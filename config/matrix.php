<?php

return array(
    'get' => array(
        '1' => array('start', 'Старт'),
        '2' => array('auto', 'Авто'),
        '3' => array('main', 'Основная'),
        '4' => array('acummulative', 'Накопительная'),
        '5' => array('acummulative_plus', 'Накопительная +'),
        '6' => array('vip', 'Vip'),
        '7' => array('fast', 'Fast'),
        '8' => array('bonus', 'Bonus'),
    ),
);
