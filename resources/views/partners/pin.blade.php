<?php 
	use Illuminate\Support\Facades\Session;
?>

@extends('layouts.app')


@section('breadcrumbs')
	<li class="breadcrumb-item active">Заявки на пин-код </li>
@endsection

@section('content')


				@if(Session::has('message'))
                  <div class="card-header">
						<p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
                   </div>
				@endif
                <div class="card-body">
					{!! $grid !!}
                </div>



@endsection


