<div class="card">
   <form method="post" action="{{ action('PartnerController@statusupdate') }}" id="create_pin_form" class="form-horizontal" accept-charset="UTF-8">
  <div class="card-header">
    <strong>{{$model->uname}}</strong></div>
    <div class="card-body">
        @csrf

        <div class="form-group row">
          <label class="col-md-3 col-form-label" for="hf-password">Логин</label>
          <div class="col-md-9">
            <input class="form-control" name="name" value="{{$model->uname}}" type="text">
          </div>
        </div>

        <div class="form-group row">
          <label class="col-md-3 col-form-label" for="hf-password"> ПИН</label>
          <div class="col-md-9">
            <input class="form-control" name="name" value="{{$model->pin}}" type="text">
          </div>
        </div>

        <div class="form-group row">
          <label class="col-md-3 col-form-label" for="hf-password">Новый партнер</label>
          <div class="col-md-9">
            @php
            $data = json_decode($model->json);
            @endphp
            <table cellpadding="10">
              <tr>
                <td>Логин</td>
                <td><b>{{ $data->uname }}</b></td>
              </tr>
              <tr>
                <td>Имя</td>
                <td><b>{{ $data->name }}</b></td>
              </tr>
              <tr>
                <td>Фамилия</td>
                <td><b>{{ $data->secondname }}</b></td>
              </tr>
              <tr>
                <td>Отчество</td>
                <td><b>{{ $data->patronic }}</b></td>
              </tr>
              <tr>
                <td>Страна</td>
                <td><b>{{ $data->country }}</b></td>
              </tr>
              <tr>
                <td>Дата рождения</td>
                <td><b>{{ $data->birthdate }}</b></td>
              </tr>
              <tr>
                <td>Телефон</td>
                <td><b>{{ $data->phone }}</b></td>
              </tr>
              <tr>
                <td>ИИН/ИНН/№ паспорта</td>
                <td><b>{{ $data->iin }}</b></td>
              </tr>

            </table>
          </div>
        </div>


        <input type="text" name="id" value="{{$model->id}}" hidden/>
        <input type="text" name="status" id="status_id" value="1" hidden/>
        <textarea name="cancel_reason"  class="form-control" id="cancel_reason">{{$model->cancel_reason}}</textarea>
        <div class="invalid-feedback">Укажите причину отказа</div>


    </div>

    <div class="card-footer">
      <button class="btn btn-sm btn-primary" type="submit">
      <i class="fa fa-dot-circle-o" type="submit"></i> Одобрить</button>
      <button class="btn btn-sm btn-danger" onclick="reject(); return false;">
      <i class="fa fa-ban"></i> Отклонить</button>
    </div>
    </form>
</div>