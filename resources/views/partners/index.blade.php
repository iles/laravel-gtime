<?php 
	use Illuminate\Support\Facades\Session;
?>

@extends('layouts.app')


@section('breadcrumbs')
	<li class="breadcrumb-item active">Заявки {{ $prog_name }}  </li>
@endsection

@section('content')
<style type="text/css">
	.laravel-grid .grid-wrapper { 
		max-height: 60vh;
		overflow-y: auto;
	} 
</style>


				@if(Session::has('message'))
                  <div class="card-header">
						<p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
                   </div>
				@endif
                <div class="card-body">
					{!! $grid !!}
                </div>



@endsection


@section('scripts')
	<script type="text/javascript">
		$('.grid-title').html('Заявки по программе {{ $prog_name }}');
	</script>

	<script type="text/javascript">
		var i = 0;
		function addField(){
			var t = '<div class="form-group row"><div class="col-md-6"><label class="col-form-label" for="hf-password">Логин</label><input class="form-control" id="info-input" name="users['+i+'][name]" type="text"></div><div class="col-md-6"><label class="col-form-label">Программа</label><select class="form-control" name="users['+i+'][type]"><option value="1">Старт</option><option value="2">Авто</option><option value="3">Основная</option><option value="4">Накопительная</option><option value="5">Накопительная +</option><option value="6">VIP</option></select></div></div>';
			$('#create_pin_form .card-body').append(t);
			i++;
      	}

      	function reject(){
      		if($('#cancel_reason').val() == ''){
	      		$('#cancel_reason').addClass('form-control-danger').focus();
	      		$('.invalid-feedback').fadeIn();      			
      		} else {
      			$('#status_id').val(2);
      			$('#create_pin_form').submit();
      		}
      	}
	</script>
@endsection