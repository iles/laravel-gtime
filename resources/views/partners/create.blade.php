<div class="card">
   <form method="post" action="{{ action('PartnerController@add') }}" id="create_pin_form" class="form-horizontal" accept-charset="UTF-8" enctype="multipart/form-data"> 
  <div class="card-header">
    <strong>Генериравать пин</strong></div>
    <div class="card-body">
        @csrf

        <div class="form-group row">
          <label class="col-md-3 col-form-label">Логин</label>
          <div class="col-md-9">
           
            <input class="form-control" id="info-input" name="name" type="text">
          </div>
        </div>          

        <div class="form-group row">
          <label class="col-md-3 col-form-label">№ Квитанции</label>
          <div class="col-md-9">
           
            <input class="form-control" name="bill_number" type="text">
          </div>
        </div>         

        <div class="form-group row">
          <label class="col-md-3 col-form-label">ИИН/ИНН</label>
          <div class="col-md-9">
           
            <input class="form-control" name="iin" type="text">
          </div>
        </div>           

        <div class="form-group row">
          <label class="col-md-3 col-form-label">Сумма</label>
          <div class="col-md-9">
           
            <input class="form-control" name="summ" type="text">
          </div>
        </div>          

        <div class="form-group row">
          <label class="col-md-3 col-form-label">Skype</label>
          <div class="col-md-9">
           
            <input class="form-control" name="skype" type="text">
          </div>
        </div>           

        <div class="form-group row">
          <label class="col-md-3 col-form-label">Фото</label>
          <div class="col-md-9">           
            <input type="file" name="image" id="image">
          </div>
        </div>        





        
    </div>
        
    <div class="card-footer">
      <button class="btn btn-sm btn-primary" type="submit">
      <i class="fa fa-dot-circle-o"></i> Генерировать</button>
      <button class="btn btn-sm btn-danger"  data-dismiss="modal" aria-label="Close" type="reset">
      <i class="fa fa-ban"></i> Отмена</button>
      <button class="btn btn-sm btn-primary pull-right" onclick="addField(); return false;"> <i class="fa fa-plus"></i> Добавить</button>      
    </div>
    </form>
</div>

