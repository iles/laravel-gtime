<div class="card">
   <form method="post" action="{{ action('AdminController@update') }}" onsubmit="checkPasswords(event)" class="form-horizontal" accept-charset="UTF-8"> 
  <div class="card-header">
    <strong>{{$model->name}}</strong></div>
    <div class="card-body">
        @csrf

        <div class="form-group row">
          <label class="col-md-3 col-form-label" for="hf-password">Логин</label>
          <div class="col-md-9">           
            <input class="form-control" name="name" value="{{$model->name}}" type="text">
            <input class="form-control" name="id" value="{{$model->id}}" hidden >
          </div>
        </div>        

        <div class="form-group row">
          <label class="col-md-3 col-form-label" for="hf-password">Email</label>
          <div class="col-md-9">           
            <input class="form-control" name="email" value="{{$model->email}}" type="text">
          </div>
        </div>      

     

 

        <div class="form-group row">
          <label class="col-md-3 col-form-label" for="hf-password">Роль</label>
          <div class="col-md-9">
          @php
            $roles = \Spatie\Permission\Models\Role::all()->pluck('name','id');
            $active = $model->getRoleNames();
          @endphp 
          <select class="form-control" name="role">
            @foreach( $roles as $key => $role)
              @if($role == $active[0])
              <option value="{{$key}}" selected="selected">{{$role}}</option>
              @else
              <option value="{{$key}}">{{$role}}</option>
              @endif
            @endforeach
          </select>
          </div>
        </div>   

        <div class="row">
          <a style="margin: -11px 0 6px 14px;" href="#"  onclick="toglePasswords(event);"><small>Изменить пароль</small></a>
        </div>

        <div class="form-group row dis" id="change-password">
          <label class="col-md-3 col-form-label" for="hf-password">Пароль</label>
          <div class="col-md-9">           
            <input class="form-control" name="password" type="password">
          </div>
        </div>           

        <div class="form-group row dis">
          <label class="col-md-3 col-form-label" for="hf-password">Пароль</label>
          <div class="col-md-9">           
            <input class="form-control" name="password2" type="password">
          </div>
        </div>                     


        
    </div>
        
    <div class="card-footer">
      <button class="btn btn-sm btn-primary" type="submit">
      <i class="fa fa-dot-circle-o" type="submit"></i> Сохранить</button>
    </div>
    </form>
</div>