@extends('layouts.app')

@section('content')

<div class="card">
  <div class="card-body">
    <div class="alert alert-warning" style="display:none" role="alert">
      Вы действительно хотите назначить <span class="user-name"></span> лидером лестницы? 
      <br/>
      <br/>
      <a class="btn btn-info active" onclick="sendAjaxChange()" type="a" aria-pressed="true">Да</a>
      <a class="btn btn-danger active" type="a" aria-pressed="true">Отмена</a>
    </div>
          @foreach ($matrix as $m)
              <div class="level">
                @foreach ($m as $key => $level)
                    @if($level[3])
                      
                    <span data-id="{{ $level[2] }}" data-name="{{ $level[0] }}">
                      {{ $level[0] }}<br/>
                      <small>(заменен на {{ $level[3] }}</small> 
                    </span>
                    @else
                    <span data-id="{{ $level[2] }}" data-name="{{ $level[0] }}">{{ $level[0] }} </span>
                    @endif
                @endforeach
              </div>
          @endforeach
  </div>
    <style type="text/css">
      .level{width: 100%; height: auto; margin: 15px; }
      .level span {display: inline-block; width: 150px; text-align: center; font-weight: bold; padding-top: 14px; height: 60px; margin: 15px; border: 1px solid #000;}
      .level span.active {outline: 3px solid #000;}
    </style>

@endsection

@section('scripts')
  <script type="text/javascript">
    var leader;
    var new_leader_name;
    var old_leader_name;
    $('.level span').on('click', function(){
      $('.level span').removeClass('active');
      $('.alert').fadeOut();
      var name = $(this).attr('data-name');
      $('.user-name').html(name);
      $('.alert').fadeIn();
      $(this).toggleClass('active');
      leader = $(this).attr('data-id');
    })

    function sendAjaxChange(){

      $.ajax({
        url: "/change-leader",
        data: {
          program: '{{ $type }}',
          mx_id: {{ $id }},
          new_leader: leader,
          old_leader: {{ $leader }},
        },
        success: function(){
         location.reload();
        }
      })

    }
  </script>
@endsection