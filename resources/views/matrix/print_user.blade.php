<div class="card">
  <div class="card-body">
    <div class="alert alert-warning" style="display:none" role="alert">
      Вы действительно хотите назначить <span class="user-name"></span> лидером лестницы? 
      <br/>
      <br/>
      <a class="btn btn-info active" onclick="sendAjaxChange()" type="a" aria-pressed="true">Да</a>
      <a class="btn btn-danger active" type="a" aria-pressed="true">Отмена</a>
    </div>
    @php
    $c = 0;
    @endphp
          @foreach ($matrix as $m)
              <div class="level">
                @foreach ($m as $key => $level)

                @php
                $c++;
                @endphp

                    @if($level[3])
                      
                    <span data-id="{{ $level[2] }}" data-name="{{ $level[0] }}">
                      {{ $level[0] }}<br/>
                      <small>(заменен на {{ $level[3] }}</small> 
                        <span>{{ $level[4] }}</span>
                    </span>

                    @else
                    <span data-id="{{ $level[2] }}" data-name="{{ $level[0] }}">{{ $level[0] }} </span>
                    {{ $level[4] }}
                    @endif

                @endforeach
              </div>
          @endforeach
  </div>



  @if($c == 15)
    <a href="#" id="split" onclick="split({{ $id }}, {{ Session::get('programm', 1) }} ); return false;" class="btn btn-primary">Разделить</a>
  @endif

  
    <style type="text/css">
      .level{width: 100%; height: auto; margin: 15px; }
      .level span {display: inline-block; width: 75px; text-align: center; font-weight: bold; padding-top: 14px; height: 60px; margin: 15px; border: 1px solid #000;}
      .level span.active {outline: 3px solid #000;}
    </style>

