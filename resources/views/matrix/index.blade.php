@extends('layouts.app')

@section('content')

 <div class="card">


          @if(Session::has('message'))
                  <div class="card-header">
            <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
                   </div>
          @endif
                  <div class="card-body">
            {!! $grid !!}
                  </div>

@endsection

@section('scripts')
<script type="text/javascript">
      	function reject(){
      		if($('#cancel_reason').val() == ''){
	      		$('#cancel_reason').addClass('form-control-danger').focus();
	      		$('.invalid-feedback').fadeIn();
      		} else {
      			$('#status_id').val(2);
      			$('#swap_form').submit();
      		}
      	}
</script>
@endsection