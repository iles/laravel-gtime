@extends('layouts.app')

@section('content')

<div class="row">
<div class="col-md-8 offset-md-2">
	


<div class="card">
   <form method="post" action="{{ action('MatrixController@doswapspr') }}" id="swap_form" class="form-horizontal" accept-charset="UTF-8" enctype="multipart/form-data"> 
  <div class="card-header">
    <strong>Замена спонсора</strong>
	@if(Session::has('message'))
		<p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
	@endif

</div>
    <div class="card-body">
        @csrf

        <div class="form-group row">
          <label class="col-md-2 col-form-label">Логин:</label>
          <div class="col-md-6">           
            <input class="form-control" id="login-input-1" name="login" type="text">
           	<span class="invalid-feedback" role="alert"><strong id="message-input-1"></strong></span>
           	<br/>
           	<span id="message-input-12" role="alert"></span>
          </div>          
          <div class="col-md-1">           
			<button class="btn btn-sm btn-primary pull-right" onclick="check(1); return false;"> <i class="fa fa-plus"></i> Проверить</button>  
          </div>
        </div>           

        <div class="form-group row" id="spnsr" style="display: none;">
          <label class="col-md-2 col-form-label">Заменить на:</label>
          <div class="col-md-6">           
            <input class="form-control" id="login-input-2" name="login2" type="text">
            <span class="invalid-feedback" role="alert"><strong id="message-input-2"></strong></span>
            <span class="valid-feedback" role="alert"><strong id="message-input-22"></strong></span>
          </div>          
          <div class="col-md-1">           
			<button class="btn btn-sm btn-primary pull-right" onclick="checkuser(2); return false;"> <i class="fa fa-plus"></i> Проверить</button>  
          </div>
        </div>           
        
    </div>
        
    <div class="card-footer">
      <button class="btn btn-sm btn-primary"  type="submit" id="submit-button" style="display: none;">Заменить</button>
    </div>
    </form>
</div>

</div>
</div>

@endsection

@section('scripts')
	<script type="text/javascript">
		var p1, p2, l1, l2, m1, m2;
		function check($i){
			
			var input = $('#login-input-'+$i)
			var login = input.val();
			if(login == ''){
				$('#message-input-'+$i).html('Введите логин');
				input.removeClass('is-valid');	
				input.addClass('is-invalid');	
				return false;
			} else {
				$.blockUI({ css: { backgroundColor: '#f00', color: '#fff'} });
				$.get( "/sponsorcheck", { login: login } ).done(function(data){
					$.unblockUI();
					if(data.status == 1){
						$('#message-input-' + $i + '2').html('Спонсор: ' + data.message);
						input.removeClass('is-invalid');	
						input.addClass('is-valid');
						$('#spnsr').fadeIn();
					} else {
						$('#message-input-'+$i).html(data.message);
						input.removeClass('is-valid');	
						input.addClass('is-invalid');
					}
				});
			}
		}

		function checkuser($i){
			
			var input = $('#login-input-'+$i)
			var login = input.val();
			if(login == ''){
				$('#message-input-'+$i).html('Введите логин');
				input.removeClass('is-valid');	
				input.addClass('is-invalid');	
				return false;
			} else {
				$.blockUI({ css: { backgroundColor: '#f00', color: '#fff'} });
				$.get( "/usercheck", { login: login } ).done(function(data){
					$.unblockUI();
					if(data.status == 1){
						$('#message-input-'+$i+'2').html(data.message);
						input.removeClass('is-invalid');	
						input.addClass('is-valid');
						$('#submit-button').fadeIn();
					} else {
						$('#message-input-'+$i).html(data.message);
						input.removeClass('is-valid');	
						input.addClass('is-invalid');
					}
				});
			}
		}

	</script>
@endsection