@extends('layouts.app')

@section('content')

<div class="row">
<div class="col-md-8 offset-md-2">
	


<div class="card">
   <form method="post" action="{{ action('MatrixController@doswap') }}" id="swap_form" class="form-horizontal" accept-charset="UTF-8" enctype="multipart/form-data"> 
  <div class="card-header">
    <strong>Замена</strong>
	@if(Session::has('message'))
		<p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
	@endif

</div>
    <div class="card-body">
        @csrf

        <div class="form-group row">
          <label class="col-md-2 col-form-label">Логин:</label>
          <div class="col-md-6">           
            <input class="form-control" id="login-input-1" name="login" type="text">
           	<span class="invalid-feedback" role="alert"><strong id="message-input-1"></strong></span>
           	<span class="valid-feedback" role="alert"><strong id="message-input-12"></strong></span>
          </div>          
          <div class="col-md-1">           
			<button class="btn btn-sm btn-primary pull-right" onclick="check(1); return false;"> <i class="fa fa-plus"></i> Проверить</button>  
          </div>
        </div>            

        <div class="form-group row">
          <label class="col-md-2 col-form-label">Заменить на:</label>
          <div class="col-md-6">
           
            <input class="form-control" id="login-input-2" name="login2" type="text">
            <span class="invalid-feedback" role="alert"><strong id="message-input-2"></strong></span>
            <span class="valid-feedback" role="alert"><strong id="message-input-22"></strong></span>
          </div>          
          <div class="col-md-1">           
			<button class="btn btn-sm btn-primary pull-right" onclick="check(2); return false;"> <i class="fa fa-plus"></i> Проверить</button>  
          </div>
        </div>

        <div class="form-group row">
        	<div class="col-md-12">
        		<div class="row" style="padding-left: 15px; cursor: pointer;">
		        	<label for="PP" style="cursor: pointer;">ПП</label>
		        	<input style="margin: -5px 0 0 15px;" type="radio" value="PP"  id="PP" name="var">        		
        		</div>
        	</div>        	
        	<div class="col-md-12">
        		<div class="row" style="padding-left: 15px; cursor: pointer;">
		        	<label  for="PZ" style="cursor: pointer;">ПЗ</label>
		        	<input  style="margin: -5px 0 0 18px;" type="radio" value="PZ"  id="PZ" name="var">      		
        		</div>
        	</div>
        </div> 

      





        
    </div>
        
    <div class="card-footer">
      <button class="btn btn-sm btn-primary"  type="submit">Заменить</button>
    </div>
    </form>
</div>

</div>
</div>

@endsection

@section('scripts')
	<script type="text/javascript">
		var p1, p2, l1, l2, m1, m2;
		function check($i){
			
			var input = $('#login-input-'+$i)
			var login = input.val();
			if(login == ''){
				$('#message-input-'+$i).html('Введите логин');
				input.removeClass('is-valid');	
				input.addClass('is-invalid');	
				return false;
			} else {
				$.blockUI({ css: { backgroundColor: '#f00', color: '#fff'} });
				$.get( "/usercheck", { login: login } ).done(function(data){
					$.unblockUI();
					if(data.status == 1){
						$('#message-input-'+$i+'2').html(data.message);
						input.removeClass('is-invalid');	
						input.addClass('is-valid');
						if($i == 1){
							p1 = data.position;
							l1 = data.level;
							m1 = data.mx_id;							
						} else {
							p2 = data.position;
							l2 = data.level;
							m2 = data.mx_id;							
						}
					} else {
						$('#message-input-'+$i).html(data.message);
						input.removeClass('is-valid');	
						input.addClass('is-invalid');
					}
				});
			}
		}

		$('#swap_form').submit(function() {
			if(p1 == undefined || l1 == undefined ){
				check(1);
				return false;
			}			

			if(p2 == undefined || l2 == undefined ){
				check(2);
				return false;
			}
			
			if(l1 != l2){
				alert('Уровни участников должны совпадать!')
				return false;
			}
		});
	</script>
@endsection