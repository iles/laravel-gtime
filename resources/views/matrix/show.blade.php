<style type="text/css">
  .modal-dialog{max-width: 90%;}
</style>
<div class="card">
  <div class="card-body">
    <div class="alert alert-warning" style="display:none" role="alert">
      Вы действительно хотите назначить <span class="user-name"></span> лидером лестницы? 
      <br/>
      <br/>
      <a class="btn btn-info active" onclick="sendAjaxChange()" type="a" aria-pressed="true">Да</a>
      <a class="btn btn-danger active" type="a" aria-pressed="true">Отмена</a>
    </div>

          @foreach ($matrix as $m)
              <div class="level">
                @foreach ($m as $key => $level)
                    @if($level[3])
                      
                    <span data-id="{{ $level[2] }}" data-name="{{ $level[0] }}">
                      {{ $level[0] }}<br/>
                      <small>(заменен на {{ $level[3] }}</small> 
                    </span>
                    @else
                    <span data-id="{{ $level[2] }}" data-name="{{ $level[0] }}">{{ $level[0] }} </span>
                    @endif
                @endforeach
              </div>
          @endforeach
  </div>
    <style type="text/css">
      .level{width: 100%; height: auto; margin: 15px; }
      .level span {display: inline-block; width: 100px; text-align: center; font-weight: bold; padding-top: 14px; height: 60px; margin: 15px; border: 1px solid #000;}
      .level span.active {outline: 3px solid #000;}
    </style>

