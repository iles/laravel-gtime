<div class="card">

      {{ Form::open(['route' => 'bonus.add', 'method'=>'post', 'files' => true]) }}
    <div class="card-body">
        @csrf

{{--         <div class="form-group row">            
            {{ Form::label('program_id', 'Программа:', array('class' => 'col-md-3 col-form-label')) }}
          <div class="col-md-9">
              {{ Form::select('program_id', $progs, Input::old('program_id'), array('class' => 'form-control', 'required'=>'required')  ) }}
          </div>
        </div>    --}}           

        <div class="form-group row">            
          {{ Form::label('uname', 'Логин:', array('class' => 'col-md-3 col-form-label')) }}
          <div class="col-md-9">
            {{ Form::text('uname', Auth::user()->name,  array('class' => 'form-control', 'required'=>'required', 'readonly' => 'readonly') ) }}
          </div>
        </div>        

{{--         <div class="form-group row">            
          {{ Form::label('summ', 'Сумма:', array('class' => 'col-md-3 col-form-label')) }}
          <div class="col-md-9">
            {{ Form::number('summ', 25000,  array('class' => 'form-control', 'required'=>'required',  'readonly' => 'readonly') ) }}
          </div>
        </div> --}}

        <div class="form-group row">            
          {{ Form::label('firstname', 'Имя:', array('class' => 'col-md-3 col-form-label')) }}
          <div class="col-md-9">
            {{ Form::text('firstname', Auth::user()->firstname,  array('class' => 'form-control', 'required'=>'required') ) }}
          </div>
        </div>        

        <div class="form-group row">            
          {{ Form::label('firstname', 'Фамилия:', array('class' => 'col-md-3 col-form-label')) }}
          <div class="col-md-9">
            {{ Form::text('lastname', Auth::user()->lastname,  array('class' => 'form-control', 'required'=>'required') ) }}
          </div>
        </div>       

         <div class="form-group row">            
          {{ Form::label('patronic', 'Отчество:', array('class' => 'col-md-3 col-form-label')) }}
          <div class="col-md-9">
            {{ Form::text('patronic', Auth::user()->patronic,  array('class' => 'form-control', 'required'=>'required') ) }}
          </div>
        </div>         

        <div class="form-group row">            
          {{ Form::label('iin', 'ИИН:', array('class' => 'col-md-3 col-form-label')) }}
          <div class="col-md-9">
            {{ Form::number('iin', Input::old('iin'),  array('class' => 'form-control', 'required'=>'required') ) }}
          </div>
        </div>        

        <div class="form-group row">            
          {{ Form::label('bill_num', 'Номер счета :', array('class' => 'col-md-3 col-form-label')) }}
          <div class="col-md-9">
            {{ Form::text('bill_num', Input::old('bill_num'),  array('class' => 'form-control', 'required'=>'required') ) }}
          </div>
        </div>

        <div class="form-group row">         
          {{ Form::label('image', 'Скан документа :', array('class' => 'col-md-3 col-form-label')) }}
          <div class="col-md-9">
           {{ Form::file('image', array('class' => 'form-control', 'required'=>'required', 'multiple'=>true, 'accept'=>".jpeg, .jpg, .png, .pdf") ) }}
          </div>
        </div>      

        <div class="form-group row">            
          {{ Form::label('product', 'Продукция :', array('class' => 'col-md-3 col-form-label')) }}
          <div class="col-md-9">
            <div class="row">
              <div class="col-md-1">
                <input type="radio" name="prod" style="margin-bottom: 9px" id="prod1" val="1" /> <br/>
                <input type="radio" name="prod" style="margin-bottom: 9px" id="prod2" val="2" /> <br/>
                <input type="radio" name="prod" style="margin-bottom: 9px" id="prod3" val="3" /> <br/>
                <input type="radio" name="prod" style="margin-bottom: 9px" id="prod4" val="4" /> <br/>
                <input type="radio" name="prod" style="margin-bottom: 9px" id="prod5" val="5" /> <br/>
                <input type="radio" name="prod" style="margin-bottom: 9px" id="prod6" val="6" /> <br/>
                <input type="radio" name="prod" style="margin-bottom: 9px" id="prod7" val="7" /> <br/>
                <input type="radio" name="prod" style="margin-bottom: 9px" id="prod8" val="8" /> <br/>
                <input type="radio" name="prod" style="margin-bottom: 9px" id="prod9" val="9" /> <br/>
              </div>
              <div class="col-md-10 text-left">
                <label style="margin-bottom: 7px; cursor: pointer;" for="prod1"> 1 шт. Антицеллюлитный крем + 1шт.</label>  <br/>
                <label style="margin-bottom: 7px; cursor: pointer;" for="prod2">Крем разогревающий + 1шт. </label> <br/>
                <label style="margin-bottom: 7px; cursor: pointer;" for="prod3">Крем успокаивающий от ушибов и ссадин + 1шт.</label> <br/>
                <label style="margin-bottom: 7px; cursor: pointer;" for="prod4">Лосьон для тела + 1шт.</label> <br/>
                <label style="margin-bottom: 7px; cursor: pointer;" for="prod5">Косметический бальзам Qamqor + 1шт.</label> <br/>
                <label style="margin-bottom: 7px; cursor: pointer;" for="prod6">Эко поглотитель + 1шт.</label> <br/>
                <label style="margin-bottom: 7px; cursor: pointer;" for="prod7">Антисептический гель + 2шт.</label> <br/>
                <label style="margin-bottom: 7px; cursor: pointer;" for="prod8">Гель для душа + 1шт.</label> <br/>
                <label style="margin-bottom: 7px; cursor: pointer;" for="prod9">крем от загара + Масло для загара 1шт.</label>     <br/>            
              </div>
              
            </div>
            {{ Form::text('product', Input::old('product'),  array('class' => 'form-control', 'required'=>'required') ) }}
          </div>
        </div>          

         <div class="form-group row" style="display: none;">            
            {{ Form::text('uid', Auth::user()->user_id,  array('class' => 'form-control', 'required'=>'required') ) }}
        </div>        


     
     

        
    </div>
        
    <div class="card-footer">
      <button class="btn btn-sm btn-primary" type="submit">
        <i class="fa fa-dot-circle-o"></i> Отправить
      </button>     

   </div>
   {{ Form::close() }}

</div>

<style type="text/css">
  .cabinet_main_container .auth_cont .form-control {
    margin-top: 0 !important;
}
</style>

  <script type="text/javascript">

      $('.manage-btn').on('click', function(e){
        e.preventDefault();
        if( $(this).attr('id') == 'stat2'){
          if( $('#cancel_reason').val() == '' ){
            $('.invalid-feedback').fadeIn();
            return false;
          } else {
            $('#form2').submit();
          }
        } else {
            $('#form1').submit();          
        }

      })

  </script>
