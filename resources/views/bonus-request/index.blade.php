@extends('layouts.app')

@section('breadcrumbs')
	<li class="breadcrumb-item active">Вознаграждения</li>
@endsection

@section('content')
	@section('content')
	<div class="row">
		<div class="col-md-12">        		
	          @if(Session::has('message'))
	                  <div class="card-header">
	            			<p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
	                   </div>
	          @endif
        	</div>
		</div>
	{!! $grid !!}
	@endsection
@endsection


@section('scripts')

	<script type="text/javascript">
		var i = 0;


      	function reject(){
      		if($('#cancel_reason').val() == ''){
	      		$('#cancel_reason').addClass('form-control-danger').focus();
	      		$('.invalid-feedback').fadeIn();      			
      		} else {
      			$('#status_id').val(2);
      			$('#update_bonus_form').submit();
      		}
      	}

      		isActive = true;

	$().ready(function () {
	    //EITHER USE A GLOBAL VAR OR PLACE VAR IN HIDDEN FIELD
	    //IF FOR WHATEVER REASON YOU WANT TO STOP POLLING
	    pollServer();
	});

	function pollServer()
	{
	    if (isActive)
	    {
	        window.setTimeout(function () {
	            $.pjax.reload('#bonus-grid');
	            pollServer();
	        }, 60000);
	    }
	}
	</script>

@endsection

