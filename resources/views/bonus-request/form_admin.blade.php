<style type="text/css">
  .modal-dialog{
    max-width: 1500px;
  }
</style>
<div class="card">
      {{ Form::open(['route' => 'bonusrequest.store_admin', 'method'=>'post']) }}

  <div class="card-header">
    <strong>
Создать заявку на вознаграждение
    </strong></div>
    <div class="card-body">
        @csrf

        <div class="row">
          <div class="col-md-6">

            <div class="form-group row">            
              {{ Form::label('uname', 'Логин:', array('class' => 'col-md-3 col-form-label')) }}
              <div class="col-md-9">
                {{ Form::text('uname', Input::old('uname'), array('class' => 'form-control', 'required'=>true)  ) }}
              </div>
            </div>              

            <div class="form-group row">            
              {{ Form::label('firstname', 'Имя:', array('class' => 'col-md-3 col-form-label')) }}
              <div class="col-md-9">
                {{ Form::text('firstname', Input::old('firstname'),  array('class' => 'form-control', 'required'=>true))  }}
              </div>
            </div>               

            <div class="form-group row">            
              {{ Form::label('lastname', 'Фамилия:', array('class' => 'col-md-3 col-form-label')) }}
              <div class="col-md-9">
                {{ Form::text('lastname', Input::old('lastname'),  array('class' => 'form-control', 'required'=>true))  }}
              </div>
            </div>               

            <div class="form-group row">            
              {{ Form::label('patronic', 'Отчество:', array('class' => 'col-md-3 col-form-label')) }}
              <div class="col-md-9">
                {{ Form::text('patronic', Input::old('patronic'),  array('class' => 'form-control', 'required'=>true))  }}
              </div>
            </div>        

    



          </div>  

          <div class="col-md-6">
            
     


        <div class="form-group row">            
          {{ Form::label('program', 'Программа:', array('class' => 'col-md-3 col-form-label')) }}
          <div class="col-md-9">
            {{ Form::select('program', $progs, Input::old('program'), array('class' => 'form-control')  ) }}
          </div>
        </div>   

        <div class="form-group row">            
          {{ Form::label('summ', 'Сумма:', array('class' => 'col-md-3 col-form-label')) }}
          <div class="col-md-9">
            {{ Form::number('summ', Input::old('summ'),  array('class' => 'form-control', 'required'=>true) ) }}
          </div>
        </div>        

        <div class="form-group row">            
          {{ Form::label('iin', 'ИИН:', array('class' => 'col-md-3 col-form-label')) }}
          <div class="col-md-9">
            {{ Form::number('iin', Input::old('iin'),  array('class' => 'form-control', 'required'=>true) ) }}
          </div>
        </div>        

        <div class="form-group row">            
          {{ Form::label('country', 'Страна:', array('class' => 'col-md-3 col-form-label')) }}
          <div class="col-md-9">
            {{ Form::text('country', Input::old('country'),  array('class' => 'form-control') ) }}
          </div>
        </div>        

        <div class="form-group row">            
          {{ Form::label('city', 'Город:', array('class' => 'col-md-3 col-form-label')) }}
          <div class="col-md-9">
            {{ Form::text('city', Input::old('city'),  array('class' => 'form-control') ) }}
          </div>
        </div>

        <div class="form-group row">
          {{ Form::label('bill_num', 'Номер счета:', array('class' => 'col-md-3 col-form-label')) }}
          <div class="col-md-9">
            {{ Form::text('bill_num', Input::old('bill_num'),  array('class' => 'form-control', 'required'=>true) ) }}
          </div>
        </div>

        <div class="form-group row">
          {{ Form::label('cancel_reason', 'Причина отказа:', array('class' => 'col-md-3 col-form-label')) }}
          <div class="col-md-9">
            {{ Form::text('cancel_reason', Input::old('cancel_reason'),  array('class' => 'form-control') ) }}
            <div class="invalid-feedback">Укажите причину отказа</div>
          </div>
        </div>

          </div>
        </div>
       
    </div>
        
    <div class="card-footer">
      <button class="btn btn-sm btn-primary" type="submit">
        <i class="fa fa-dot-circle-o"></i> Сохранить
      </button>     
  
    </div>
   {{ Form::close() }}

</div>


