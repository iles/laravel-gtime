<div class="card">
  @if(isset($model))
      {{ Form::model($model, ['route' => ['bonusrequest.update', Input::old('id')], 'method' => 'put','id'=>'mForm']) }}
  @else
      {{ Form::open(['route' => 'bonusrequest.store', 'method'=>'post','id'=>'mForm']) }}
  @endif



  <div class="card-header">
    <strong>
      @if(isset($model))
        Редактировать заявку на вознаграждение
      @else
        Создать заявку на вознаграждение
      @endif
    </strong></div>
    <div class="card-body">
        @csrf

        <div class="row">
          <div class="col-md-6">

 <div class="form-group row">            
            {{ Form::label('program_id', 'Программа:', array('class' => 'col-md-3 col-form-label')) }}
          <div class="col-md-9">
              {{ Form::select('program_id', $progs, Input::old('program_id'), array('class' => 'form-control', 'required'=>'required')  ) }}
          </div>
        </div>              

        <div class="form-group row">            
          {{ Form::label('uname', 'Логин:', array('class' => 'col-md-3 col-form-label')) }}
          <div class="col-md-9">
            {{ Form::text('uname', Input::old('uname'),  array('class' => 'form-control', 'required'=>'required') ) }}
          </div>
        </div>        

        <div class="form-group row">            
          {{ Form::label('summ', 'Сумма:', array('class' => 'col-md-3 col-form-label')) }}
          <div class="col-md-9">
            {{ Form::number('summ', Input::old('summ'),  array('class' => 'form-control', 'required'=>'required') ) }}
          </div>
        </div>

        <div class="form-group row">            
          {{ Form::label('firstname', 'Имя:', array('class' => 'col-md-3 col-form-label')) }}
          <div class="col-md-9">
            {{ Form::text('firstname', Input::old('firstname'),  array('class' => 'form-control', 'required'=>'required') ) }}
          </div>
        </div>        

        <div class="form-group row">            
          {{ Form::label('firstname', 'Фамилия:', array('class' => 'col-md-3 col-form-label')) }}
          <div class="col-md-9">
            {{ Form::text('lastname', Input::old('lastname'),  array('class' => 'form-control', 'required'=>'required') ) }}
          </div>
        </div>       

         <div class="form-group row">            
          {{ Form::label('patronic', 'Отчество:', array('class' => 'col-md-3 col-form-label')) }}
          <div class="col-md-9">
            {{ Form::text('patronic', Input::old('patronic'),  array('class' => 'form-control', 'required'=>'required') ) }}
          </div>
        </div>         

        <div class="form-group row">            
          {{ Form::label('iin', 'ИИН:', array('class' => 'col-md-3 col-form-label')) }}
          <div class="col-md-9">
            {{ Form::number('iin',  Input::old('iin'),  array('class' => 'form-control', 'required'=>'required') ) }}
          </div>
        </div>        

        <div class="form-group row">            
          {{ Form::label('bill_num', 'Номер счета :', array('class' => 'col-md-3 col-form-label')) }}
          <div class="col-md-9">
            {{ Form::text('bill_num', Input::old('bill_num'),  array('class' => 'form-control', 'required'=>'required') ) }}
          </div>
        </div>           

         <div class="form-group row">            
          {{ Form::label('bank_name', 'Наименование банка :', array('class' => 'col-md-3 col-form-label')) }}
          <div class="col-md-9">
            {{ Form::text('bank_name', Input::old('bank_name'),  array('class' => 'form-control', 'required'=>'required') ) }}
          </div>
        </div>            

     
 

          </div>  

          <div class="col-md-6">
            
   <div class="form-group row">            
          {{ Form::label('BIK', 'BIK :', array('class' => 'col-md-3 col-form-label')) }}
          <div class="col-md-9">
            {{ Form::text('BIK', Input::old('BIK'),  array('class' => 'form-control', 'required'=>'required') ) }}
          </div>
        </div>          

        <div class="form-group row">            
          {{ Form::label('card_number', 'Номер карты :', array('class' => 'col-md-3 col-form-label')) }}
          <div class="col-md-9">
            {{ Form::text('card_number', Input::old('card_number'),  array('class' => 'form-control') ) }}
          </div>
        </div>        

        <div class="form-group row">            
          {{ Form::label('pens', 'Пенсионер :', array('class' => 'col-md-3 col-form-label')) }}
          <div class="col-md-9">
            {{ Form::text('pens', Input::old('pens'),  array('class' => 'form-control') ) }}
          </div>
        </div>             


		@if(isset($model))
        @if($model->inv)

        <div class="form-group row">            
          {{ Form::label('inv', 'Инвалидность :', array('class' => 'col-md-3 col-form-label')) }}
          <div class="col-md-9">
            {{ Form::text('inv', Input::old('inv'),  array('class' => 'form-control') ) }}
          </div>
        </div>      


        <div class="form-group row">            
          {{ Form::label('inv_group', 'Группа инвалидности :', array('class' => 'col-md-3 col-form-label')) }}
          <div class="col-md-9">
            {{ Form::text('inv_group', Input::old('inv_group'),  array('class' => 'form-control') ) }}
          </div>
        </div>          

         <div class="form-group row">            
          {{ Form::label('inv_start', 'Срок инвалидности :', array('class' => 'col-md-3 col-form-label')) }}
          <div class="col-md-9">
            {{ Form::text('inv_start', Input::old('inv_start'),  array('class' => 'form-control') ) }}
            {{ Form::text('inv_end', Input::old('inv_end'),  array('class' => 'form-control') ) }}
          </div>
        </div>            

        <div class="form-group row">            
          {{ Form::label('inv_timeless', 'Без срока:', array('class' => 'col-md-3 col-form-label')) }}
          <div class="col-md-9">
            {{ Form::text('inv_timeless', Input::old('inv_timeless'),  array('class' => 'form-control') ) }}
          </div>
        </div>  
                 
        @endif
        @endif
		
        <div class="form-group row">            
          {{ Form::label('notorios', 'По нотариальной доверенности:', array('class' => 'col-md-3 col-form-label')) }}
          <div class="col-md-9">
            {{ Form::text('notorios', Input::old('notorios'),  array('class' => 'form-control') ) }}
          </div>
        </div>           

         <div class="form-group row">            
          {{ Form::label('adress', 'Адрес:', array('class' => 'col-md-3 col-form-label')) }}
          <div class="col-md-9">
            {{ Form::text('adress', Input::old('adress'),  array('class' => 'form-control') ) }}
          </div>
        </div>    

         <div class="form-group row">            
          {{ Form::label('fact_adress', 'Фактический адрес:', array('class' => 'col-md-3 col-form-label')) }}
          <div class="col-md-9">
            {{ Form::text('fact_adress', Input::old('fact_adress'),  array('class' => 'form-control') ) }}
          </div>
        </div>    

         <div class="form-group row">            
          {{ Form::label('country', 'Страна/город:', array('class' => 'col-md-3 col-form-label')) }}
          <div class="col-md-9">
            {{ Form::text('country', Input::old('country'),  array('class' => 'form-control') ) }}
          </div>
        </div>    

 

        <div class="form-group row">
          {{ Form::label('cancel_reason', 'Причина отказа:', array('class' => 'col-md-3 col-form-label')) }}
          <div class="col-md-9">
            {{ Form::text('cancel_reason', Input::old('cancel_reason'),  array('class' => 'form-control') ) }}
            <div class="invalid-feedback">Укажите причину отказа</div>
          </div>
        </div>

          </div>
        </div>
       
    </div>
        
    <div class="card-footer">
  
      <a href="#" id="stat1" class="manage-btn btn btn-sm btn-success">
        <i class="fa fa-dot-circle-o"></i> Одобрить
      </a>

      <a href="#" id="stat2" class="manage-btn btn btn-sm btn-danger">
        <i class="fa fa-ban"></i>  Отклонить
      </a>


  
    </div>
   {{ Form::close() }}
   
		@if(isset($model))
          <form id="form2" style="display: none" method="POST" action="{{ route('bonusrequest.bookaprove', ['id'=>$model->id, 's'=>5]) }}"> 
            @csrf
            <input type="text" name="cancel_reason" class="cr">
          </form>
          <form id="form1" style="display: none" method="POST" action="{{ route('bonusrequest.bookaprove', ['id'=>$model->id, 's'=>4]) }}"> 
            <input type="text" name="cancel_reason" class="cr">
            @csrf
          </form> 
        @endif
</div>

  <script type="text/javascript">

    $('#cancel_reason').on('change', function(){
      $('.cr').val( $(this).val() );
    })

      $('.manage-btn').on('click', function(e){
        e.preventDefault();
        if( $(this).attr('id') == 'stat2'){
			
          if( $('#cancel_reason').val() == '' ){
            $('.invalid-feedback').fadeIn();
            return false;
          } else {
            $('#form2').submit();
          }
		  
        } else {
          // $('#mForm').submit();
          $('#form1').submit();          
        }

      })

  </script>
<style type="text/css">
  .modal-dialog{
    max-width: 1500px;
  }
</style>