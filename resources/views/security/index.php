<?php 
	use Illuminate\Support\Facades\Session;
?>

@extends('layouts.app')


@section('breadcrumbs')
	<li class="breadcrumb-item active">Заявки на передачу </li>
@endsection

@section('content')
	@section('content')
	<div class="row">
		<div class="col-md-12">        		
	          @if(Session::has('message'))
	                  <div class="card-header">
	            			<p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
	                   </div>
	          @endif
        	</div>
		</div>
	{!! $grid !!}
	@endsection
@endsection


@section('scripts')

	<script type="text/javascript">
		
		$('.grid-title').html('Заявки на передачу ');
		var i = 0;


      	function reject(){
      		if($('#cancel_reason').val() == ''){
	      		$('#cancel_reason').addClass('form-control-danger').focus();
	      		$('.invalid-feedback').fadeIn();      			
      		} else {
      			$('#status_id').val(2);
      			$('#update_bonus_form').submit();
      		}
      	}
	</script>

@endsection