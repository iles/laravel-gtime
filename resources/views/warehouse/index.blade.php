@extends('layouts.app')



@section('breadcrumbs')
	<li class="breadcrumb-item active">Склад </li>
@endsection

@section('content')



    <div class="card-body warehouse-body">
        <div class="row">
        	<div class="col-md-12">
	          @if(Session::has('message'))
	                  <div class="card-header">
	            			<p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
	                   </div>
	          @endif
        	</div>
            <div class="col-md-12 col-xs-12 col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <div class="pull-left">
                            <h4 class="grid-title">Личный кабинет регионального менеджера</h4>
                        </div>
                    </div>
                    <div class="card-body">
                    <form method="post" action="{{ action('WarehouseController@add') }}"  class="form-horizontal" accept-charset="UTF-8">
                    	@csrf
                        <div class="row">
                            <div class="col-md-4">
                            	<label>Выберите программу</label> <br/>
                            	<select class="form-control" id="program" name="program">
                            		@php
                            			$m = Config::get('matrix.get');
										$p = Session::get('programm', 1);
										$progs = [];
										foreach ($m as $key => $matrix){
											echo '<option value="'.$key.'">'.$matrix[1].'</option>';
										}
                            		@endphp
                            	</select>
                            </div>
                            <div class="col-md-4">
                            	<label>Логин</label> <br/>
                        		<input type="text" id="uname-check" required="required" name="uname" class="form-control">
                        		<input type="text" id="user_fullname" hidden="true"  required="required" name="user_fullname" class="form-control">
                            </div>
                            <div class="col-md-1">
                            	<br/>
                            	<a href="#" id="check_login" class="btn btn-warning">Проверить</a>
                            </div>
                            <div class="col-md-2">
                            	<p class="message-response"></p>
                            </div>
                        </div>
                        <br/>

                        <div class="hidden form-part">
	                        <div class="row">
	                        	<div class="col-md-4">
	                        		<label>Фамилия</label> <br/>
	                        		<input type="text" name="checkout_surname" required="required" class="form-control">
	                        	</div>
	                        	<div class="col-md-4">
	                        		<label>Имя</label> <br/>
	                        		<input type="text" name="checkout_name" required="required" class="form-control">
	                        	</div>
	                        	<div class="col-md-4">
	                        		<label>Отчество</label> <br/>
	                        		<input type="text" name="checkout_patronic" required="required" class="form-control">
	                        	</div>
	                        	<div class="col-md-4">
	                        		<label>ИИН</label> <br/>
	                        		<input type="text" name="iin" required="required" class="form-control">
	                        	</div>
	                        </div>
	                        <br/>
	                        <div class="row">
	                        	<div class="col-md-4">
	                        		<label>Выберите дату покупки продукции</label> <br/>
	                        		<input type="text" name="purchace_date" class="form-control dates" required="required">
	                        	</div>
	                        	<div class="col-md-4">
	                        		<label>Укажите дату когда забрали товар</label> <br/>
	                        		<input type="text" name="checkout_date" class="form-control dates" required="required">
	                        	</div>
	                        	<div class="col-md-4" id="opt_td">
	                            	<label>Выберите вариант</label> <br/>
	                            	<select class="form-control" name="option" id="opt-select" required="required"></select>
	                        	</div>
	                        </div>
                        <div class="row">
                        	<div class="col-md-12">
                        		<br/>
                        		<br/>
								<button class="btn btn-warning btn-lg"> Отправить </button>
                        	</div>
                        </div>

                        </div>
                    </form>
                        <br/>

                        <div class="table-responsive grid-wrapper">
                        	{!! $grid !!}
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="pull-left">
                            <b></b>
                        </div>
                        <div class="pull-right"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>



@endsection


@section('scripts')
<script type="text/javascript">
	$('.dates').daterangepicker({
		singleDatePicker: true,
	});

	$(document).ready( function() {

		$('#check_login').on('click', function(e){
			e.preventDefault();
			$('.message-response').fadeOut();
			$('.form-part').fadeOut();

			function makeSelect(p){
				$('#opt-select').html('');
				var opts;
				if(p == 4){
					opts = ['Накоп Вар А', 'Накоп Вар В']
				}else if(p == 7){
					opts = ['Фаст Вар А', 'Фаст Вар В', 'Фаст Вар Д', 'Фаст Вар Е', 'Фаст Вар С']
				}else if(p == 3){
					opts = ['Осн Вар А', 'Осн Вар В', 'Осн Вар Д', 'Осн Вар Е', 'Осн Вар С', 'Осн Вар Ф']
				}else if(p == 6){
					opts = ['Вип Вар А', 'Вип Вар В ЧАСЫ', 'Вип Вар С', 'Вип Вар Д', 'Вип Вар Е']
				}else if(p == 5){
					opts = ['Накоп Плюс Вар А', 'Накоп Плюс Вар В']
				}
				$.each( opts, function( key, value ) {
				  var opt = '<option value="'+ value +'">' + value + '</option>';
				  $('#opt-select').append(opt);
				});
				 $('#opt-select').append('<option value="other">Другое</option>');
			}
			var uname = $('#uname-check').val();
			var program = $('#program').val();

			if( uname ){
				$.get( "check_login", { name: uname, program: program} )
				  .done(function( data ) {

				  	if(data.error == 1 ){
				  		$('.message-response').html('<p class="error-message">' + data.m + '</p>' );
				  		$('.message-response').fadeIn();
				  	} else {
						$('#opt-select').show();
						$('#other-option').remove();
				  		$('.message-response').html('<p class="succses-message">' + data.m + '</p>' );
				  		$('#user_fullname').val(data.m);
				  		$('.message-response').fadeIn();
				  		$('.form-part').fadeIn();
				  		makeSelect( $('#program').val() );
				  	}
				});
			} else {
				$('#uname-check').focus();
				return true;
			}
		})


		$('#opt-select').on('change', function(){
			if( $(this).val() == 'other' ){
				$('#opt-select').hide();
				$('#opt_td').append('<input class="form-control" required="required" id="other-option" name="option" type="text">');
				$('#other-option').show();
				$('#other-option').focus();
			} else {
				$('#opt-select').show();
				$('#other-option').remove();
			}
		});
	})


</script>
@endsection