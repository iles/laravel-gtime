<div class="card">
   <form method="post" action="{{ action('PaymentController@statusupdate') }}" id="create_pin_form" class="form-horizontal" accept-charset="UTF-8"> 
  <div class="card-header">
    <strong>{{$user->name}}</strong></div>
    <div class="card-body">
        @csrf

        <div class="form-group row">
          <label class="col-md-3 col-form-label" for="hf-password">Имя</label>
          <div class="col-md-9">           
            <input class="form-control" name="name" value="{{$user->name}}" type="text">
          </div>
        </div>        
 

        <a data-fancybox="gallery" href="/uploads/payments/{{$model->photo}}"><img src="/uploads/payments/{{$model->photo}}" style="width: 80px;"></a>
        <br/>
        <br/>
        <input type="text" name="id" value="{{$model->id}}" hidden/>
        <input type="text" name="status" id="status_id" value="1" hidden/>
        <textarea name="cancel_reason"  class="form-control" id="cancel_reason">{{$model->cancel_reason}}</textarea>        
        <div class="invalid-feedback">Укажите причину отказа</div>

        
    </div>
        
    <div class="card-footer">
      <button class="btn btn-sm btn-primary" type="submit">
      <i class="fa fa-dot-circle-o" type="submit"></i> Одобрить</button>
      <button class="btn btn-sm btn-danger" onclick="reject(); return false;">
      <i class="fa fa-ban"></i> Отклонить</button>
    </div>
    </form>
</div>