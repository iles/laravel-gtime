<div class="card" style="margin-top: 70px">
    <div class="card-body">
      {{ Form::open(['route' => 'payments.store', 'method'=>'post', 'files'=>true]) }}
        <div class="form-group">
          {{ Form::label('photo', 'Прикрепите квитанцию об облате') }}            
          {{ Form::file('photo') }}
          <input type="text" hidden name="user_id" value="{{ Auth::user()->user_id }}">
          <input type="text" hidden name="program_id" value="{{ Session::get('programm', 1) }}">
        </div>          
        <div class="form-group">
          <button class="btn btn-sm btn-primary" type="submit">Отправить</button>
        </div>        
      {{ Form::close() }}
    </div>  
</div>