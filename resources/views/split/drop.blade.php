<div class="card" style="width: 100%; margin: 0 auto;">

  <div class="card-header">
    <strong></strong><br/>
  </div>


<form method="post" action="{{ action('SplitController@dropupdate') }}" id="create_pin_form" class="form-horizontal" accept-charset="UTF-8">
    <div class="card-body">
        @csrf

      <div class="form-group row">
        <label class="col-md-3 col-form-label" for="hf-password">Пин</label>
          <div class="col-md-9">
            <input class="form-control" name="program_id" type="text" hidden  value="{{ $p }}">
            <input class="form-control" name="id" type="text" hidden  value="{{ $model->id }}">
            <input class="form-control" name="fid" type="text" hidden  value="{{ $finished->id }}">
            <input class="form-control" name="screch" type="text"  value="{{ $model->screch }}">
          </div>
      </div>

        <div class="form-group row">
          <label class="col-md-3 col-form-label" for="hf-password">Логин</label>
          <div class="col-md-9">
            <input class="form-control" name="name" type="text" value="{{ $model->name }}">
          </div>
        </div>

        <div class="form-group row">
          <label class="col-md-3 col-form-label" for="hf-password">ФИО</label>
          <div class="col-md-9">
            <input class="form-control" name="familiya" type="text" value="{{ $model->familiya }}">
          </div>
        </div>

        <div class="form-group row">
          <label class="col-md-3 col-form-label" for="hf-password">Страна / Город</label>
          <div class="col-md-9">
            <input class="form-control" name="imya" type="text" value="{{ $model->imya }}">
          </div>
        </div>

        <div class="form-group row">
          <label class="col-md-3 col-form-label" for="hf-password">Дата рождения</label>
          <div class="col-md-9">
            <input class="form-control" name="otchestvo" type="text" value="{{ $model->otchestvo }}">
          </div>
        </div>

        <div class="form-group row">
          <label class="col-md-3 col-form-label" for="hf-password">Телефон</label>
          <div class="col-md-9">
            <input class="form-control" name="pol" type="text" value="{{ $model->pol }}">
          </div>
        </div>

        <div class="form-group row">
          <label class="col-md-3 col-form-label" for="hf-password">ИИН</label>
          <div class="col-md-9">
            <input class="form-control" name="strana" type="text" value="{{ $model->strana }}">
          </div>
        </div>

        <div class="form-group row">
          <label class="col-md-3 col-form-label" for="hf-password">SETR</label>
          <div class="col-md-9">
            <textarea class="form-control" name="setr">{{ $model->setr }}</textarea>
          </div>
        </div>

        <div class="form-group row">
          <label class="col-md-3 col-form-label" for="hf-password">Приписка(INFO)</label>
          <div class="col-md-9">
            <textarea class="form-control" name="INFO">&nbsp;{{ $model->INFO }}</textarea>
          </div>
        </div>

        <div class="form-group row">
          <label class="col-md-3 col-form-label" for="hf-password">Товар</label>
          <div class="col-md-9">
            <textarea class="form-control" name="TOVAR">&nbsp;{{ $model->TOVAR }}</textarea>
          </div>
        </div>

        <div class="form-group row">
          <label class="col-md-3 col-form-label" for="hf-password">Вип бонус</label>
          <div class="col-md-9">
            <textarea class="form-control" name="vip_bonus">{{ $finished->vip_bonus }}</textarea>
          </div>
        </div>


        <div class="form-group row">
          <label class="col-md-3 col-form-label" for="hf-password">Дата регистрации</label>
          <div class="col-md-9">
            <input class="form-control" name="gorod" type="text" value="{{ $model->dateout }}">
          </div>
        </div>





    </div>

    <div class="card-footer">
      <button class="btn btn-sm btn-primary" type="submit">
      <i class="fa fa-dot-circle-o" type="submit"></i> Сохранить</button>
      <button class="btn btn-sm btn-danger"  data-dismiss="modal" aria-label="Close" type="reset">
      <i class="fa fa-ban"></i> Закрыть</button>
    </div>
    </form>
</div>