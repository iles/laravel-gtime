<div class="card">
   <form method="post" action="{{ action('PaymentController@statusupdate') }}" id="create_pin_form" class="form-horizontal" accept-charset="UTF-8">
  <div class="card-header">
    <strong>Разделение лестницы #{{$split->id}}</strong>
  </div>

  <div class="card-body">
    <div class="row">
      <div class="col-md-11 col-md-offset-1" style="margin-bottom: 12px;">
          <div class="row rr">
            <h4>Основная лестница</h4>
          </div>
      @foreach( json_decode($split->main_matrix) as $level => $level_users )
          <div class="row rr">
            @foreach($level_users as $key => $user)
             <div class="node card">
                <p>{{ $user->uname }}
                    @for ($i = 0; $i < $user->stars; $i++)
                        <i class="fa fa-star"> </i>
                    @endfor
                 </p>
                <p>{{ $user->by_refer }} </p>
            </div>
            @endforeach
          </div>
      @endforeach
      </div>
      <div class="col-md-6">
          <div class="row rr">
            <h4>Первая лестница</h4>
          </div>
      @foreach( json_decode($split->first_matrix) as $level => $level_users )
          <div class="row rr">
            @foreach($level_users as $key => $user)
             <div class="node card">
                <p>{{ $user->uname }}
                    @for ($i = 0; $i < $user->stars; $i++)
                        <i class="fa fa-star"> </i>
                    @endfor
                 </p>
                <p>{{ $user->by_refer }} </p>
            </div>
            @endforeach
          </div>
      @endforeach
      </div>
      <div class="col-md-6">
          <div class="row rr">
            <h4>Вторая лестница</h4>
          </div>
      @foreach( json_decode($split->second_matrix) as $level => $level_users )
          <div class="row rr">
            @foreach($level_users as $key => $user)
             <div class="node card">
                <p>{{ $user->uname }}
                    @for ($i = 0; $i < $user->stars; $i++)
                        <i class="fa fa-star"> </i>
                    @endfor
                 </p>
                <p>{{ $user->by_refer }} </p>
            </div>
            @endforeach
          </div>
      @endforeach
      </div>
    </div>

    </div>
  </div>

  @role('superadmin')

  <div class="card-footer">
      <button class="btn btn-sm btn-danger" data-id="{{ $split->id }}" onclick="return confirmAction(this);">
      <i class="fa fa-ban"></i> Отменить Разделение</button>
  </div>

  @endrole
    </form>
</div>