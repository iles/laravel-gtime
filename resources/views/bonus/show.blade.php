
<style type="text/css">
  #bootstrap_modal .modal-dialog{
    max-width: 1200px;
  }
</style>




<div class="card">

<ul class="nav nav-pills mb-3" id="pills-tab" role="tablist" style="padding: 15px 15px 5px;">
  <li class="nav-item">
    <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#info-tab" role="tab" aria-controls="pills-home" aria-selected="true">Инфо</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#profile" role="tab" aria-controls="pills-profile" aria-selected="false">Кого пригласил</a>
  </li> 
</ul>


<div class="card">
      <div class="tab-content" id="myTabContent">


          <div class="tab-pane fade show active" style="padding: 0" id="info-tab" role="tabpanel" aria-labelledby="home-tab">
   <form method="post" action="{{ action('BonusController@statusupdate') }}" id="update_bonus_form" class="form-horizontal" accept-charset="UTF-8"> 


    <div class="card-body">
        @csrf

        <div class="row">
          <div class="col-md-6">

        <div class="form-group row">            
            {{ Form::label('program_id', 'Программа:', array('class' => 'col-md-3 col-form-label')) }}
          <div class="col-md-9">
              {{ Form::select('program_id', $progs, $model->program_id, array('class' => 'form-control', 'required'=>'required')  ) }}
          </div>
        </div>              

        <div class="form-group row">            
          {{ Form::label('uname', 'Логин:', array('class' => 'col-md-3 col-form-label')) }}
          <div class="col-md-9">
            {{ Form::text('uname', $model->uname,  array('class' => 'form-control', 'required'=>'required') ) }}
          </div>
        </div>                

    @php
     $role = Auth::guard('web')->user()->roles()->first()->name;
    @endphp

        @if($role = 'superadmin' || $role == 'admin')
        <div class="form-group row">            
          {{ Form::label('fio', 'ФИО профиля:', array('class' => 'col-md-3 col-form-label')) }}
          <div class="col-md-9">
            {{ Form::text('fio', $fio,  array('class' => 'form-control', 'required'=>'required') ) }}
          </div>
        </div>        
        @endrole

        <div class="form-group row">            
          {{ Form::label('summ', 'Сумма:', array('class' => 'col-md-3 col-form-label')) }}
          <div class="col-md-9">
            {{ Form::number('summ', $model->summ,  array('class' => 'form-control', 'required'=>'required') ) }}
          </div>
        </div>

        <div class="form-group row">            
          {{ Form::label('firstname', 'Имя:', array('class' => 'col-md-3 col-form-label')) }}
          <div class="col-md-9">
            {{ Form::text('firstname', $model->firstname,  array('class' => 'form-control', 'required'=>'required') ) }}
          </div>
        </div>        

        <div class="form-group row">            
          {{ Form::label('firstname', 'Фамилия:', array('class' => 'col-md-3 col-form-label')) }}
          <div class="col-md-9">
            {{ Form::text('lastname', $model->lastname,  array('class' => 'form-control', 'required'=>'required') ) }}
          </div>
        </div>       

         <div class="form-group row">            
          {{ Form::label('patronic', 'Отчество:', array('class' => 'col-md-3 col-form-label')) }}
          <div class="col-md-9">
            {{ Form::text('patronic', $model->patronic,  array('class' => 'form-control', 'required'=>'required') ) }}
          </div>
        </div>         

        <div class="form-group row">            
          {{ Form::label('iin', 'ИИН:', array('class' => 'col-md-3 col-form-label')) }}
          <div class="col-md-9">
            {{ Form::number('iin',  $model->iin,  array('class' => 'form-control', 'required'=>'required') ) }}
          </div>
        </div>        

        <div class="form-group row">            
          {{ Form::label('bill_num', 'Номер счета :', array('class' => 'col-md-3 col-form-label')) }}
          <div class="col-md-9">
            {{ Form::text('bill_num', $model->bill_num,  array('class' => 'form-control', 'required'=>'required') ) }}
          </div>
        </div>           

         <div class="form-group row">            
          {{ Form::label('bank_name', 'Наименование банка :', array('class' => 'col-md-3 col-form-label')) }}
          <div class="col-md-9">
            {{ Form::text('bank_name', $model->bank_name,  array('class' => 'form-control', 'required'=>'required') ) }}
          </div>
        </div>            

     
        @if($model->vip_bonus_logins)
         <div class="form-group row">            
          {{ Form::label('country', 'Логины (бонус):', array('class' => 'col-md-3 col-form-label')) }}
          <div class="col-md-9">

              @foreach($users as $k => $v)
                @if(!$v)
                Логина <b>{{ $v }}</b> не существует
                @else
                <b> {{ $v->name}} </b><br>
               {{ gmdate("Y-m-d", $v->reg_date) }} <br>
                 <span>ПИН: </span><b>{{ $v->screch}} </b>
                 <br>
                 <span>TOVAR: </span><b>{{ $v->tovar}} </b>
                 <br>
                 <br>
              <br/>
                @endif
              @endforeach
          </div>
        </div>   
        @endif 

          </div>  

          <div class="col-md-6">
            
   <div class="form-group row">            
          {{ Form::label('BIK', 'BIK :', array('class' => 'col-md-3 col-form-label')) }}
          <div class="col-md-9">
            {{ Form::text('BIK', $model->BIK,  array('class' => 'form-control', 'required'=>'required') ) }}
          </div>
        </div>          

        <div class="form-group row">            
          {{ Form::label('card_number', 'Номер карты :', array('class' => 'col-md-3 col-form-label')) }}
          <div class="col-md-9">
            {{ Form::text('card_number', $model->card_number,  array('class' => 'form-control') ) }}
          </div>
        </div>        

        <div class="form-group row">            
          {{ Form::label('pens', 'Пенсионер :', array('class' => 'col-md-3 col-form-label')) }}
          <div class="col-md-9">
            {{ Form::text('pens', $model->pens,  array('class' => 'form-control') ) }}
          </div>
        </div>             


        @if($model->inv)

        <div class="form-group row">            
          {{ Form::label('inv', 'Инвалидность :', array('class' => 'col-md-3 col-form-label')) }}
          <div class="col-md-9">
            {{ Form::text('inv', $model->inv,  array('class' => 'form-control') ) }}
          </div>
        </div>      


        <div class="form-group row">            
          {{ Form::label('inv_group', 'Группа инвалидности :', array('class' => 'col-md-3 col-form-label')) }}
          <div class="col-md-9">
            {{ Form::text('inv_group', $model->inv_group,  array('class' => 'form-control') ) }}
          </div>
        </div>          

         <div class="form-group row">            
          {{ Form::label('inv_start', 'Срок инвалидности :', array('class' => 'col-md-3 col-form-label')) }}
          <div class="col-md-9">
            {{ Form::text('inv_start', $model->inv_start,  array('class' => 'form-control') ) }}
            {{ Form::text('inv_end', $model->inv_end,  array('class' => 'form-control') ) }}
          </div>
        </div>            

        <div class="form-group row">            
          {{ Form::label('inv_timeless', 'Без срока:', array('class' => 'col-md-3 col-form-label')) }}
          <div class="col-md-9">
            {{ Form::text('inv_timeless', $model->inv_timeless,  array('class' => 'form-control') ) }}
          </div>
        </div>  
                 
        @endif

        @if( $model->notorios == 1)
        <div class="form-group row">            
          {{ Form::label('notorios', 'По нотариальной доверенности:', array('class' => 'col-md-3 col-form-label')) }}
          <div class="col-md-9">
            <div class="row">
              {{ Form::label('notorios', 'ФИО:', array('class' => 'col-md-3 col-form-label')) }}
              {{ Form::text('not_fio', $model->not_fio,  array('class' => 'col-md-9 form-control') ) }}
            </div>            
            <div class="row">
              <br/>
              {{ Form::label('notorios', 'Адрес:', array('class' => 'col-md-3 col-form-label')) }}
              {{ Form::text('not_adress', $model->not_adress,  array('class' => 'col-md-9 form-control') ) }}
            </div>
            <div class="row">
              <br/>
              {{ Form::label('notorios', 'ИИН:', array('class' => 'col-md-3 col-form-label')) }}
              {{ Form::text('not_iin', $model->not_iin,  array('class' => 'col-md-9 form-control') ) }}
            </div>            
            <div class="row">
              <br/>
              {{ Form::label('notorios', 'Пасспорт:', array('class' => 'col-md-3 col-form-label')) }}
              {{ Form::text('not_passport', $model->not_passport,  array('class' => 'col-md-9 form-control') ) }}
            </div>
          </div>
        </div>  
        @endif         

{{--         @dd(Auth::user())
        @if(Auth::user()->nonResident() ) --}}
         <div class="form-group row">            
          {{ Form::label('adress', 'Адрес:', array('class' => 'col-md-3 col-form-label')) }}
          <div class="col-md-9">
            {{ Form::text('adress', $model->adress,  array('class' => 'form-control') ) }}
          </div>
        </div>    

         <div class="form-group row">            
          {{ Form::label('fact_adress', 'Фактический адрес:', array('class' => 'col-md-3 col-form-label')) }}
          <div class="col-md-9">
            {{ Form::text('fact_adress', $model->fact_adress,  array('class' => 'form-control') ) }}
          </div>
        </div>    

         <div class="form-group row">            
          {{ Form::label('country', 'Страна/город:', array('class' => 'col-md-3 col-form-label')) }}
          <div class="col-md-9">
            {{ Form::text('country', $model->country,  array('class' => 'form-control') ) }}
          </div>
        </div>    
        {{-- @endif --}}

  

        <div class="form-group row">            
          {{ Form::label('image', 'Скан документа:', array('class' => 'col-md-3 col-form-label')) }}   
          <div class="col-md-9">

            @foreach( explode(';', $model->image) as $image)
              <a data-fancybox="gallery" href="/uploads/scans/{{$image  }}"><img src="/uploads/scans/{{$image }}" style="width: 80px;"></a>
            @endforeach
          </div>
        </div>  

        <input type="text" name="id" value="{{$model->id}}" hidden/>     
        <input type="text" name="status" id="status_id" value="1" hidden/>
        <textarea name="cancel_reason"  class="form-control" id="cancel_reason">{{$model->cancel_reason}}</textarea>        
        <div class="invalid-feedback">Укажите причину отказа</div>

          </div>
        </div>
       
    </div>
        
    <div class="card-footer">

      @if($transfer && $transfer->status_id == 0)
        Передача пин кодов не одобрена сб
      @else
      <button class="btn btn-sm btn-primary" type="submit">
      <i class="fa fa-dot-circle-o" type="submit"></i> Одобрить</button>
      <button class="btn btn-sm btn-danger" onclick="reject(); return false;">
      <i class="fa fa-ban"></i> Отклонить </button>
      @endif
    </div>
    </form>
   {{ Form::close() }}
</div>
            <div class="tab-pane fade" style="padding: 0" id="profile" role="tabpanel" aria-labelledby="profile-tab">
              <div class="card-body">

                @if(!$refers->isEmpty() )
                <table class="table table-responsive-sm">
                  <thead>
                  <tr>
                  <th>Логин</th>
                  <th>ФИО</th>
                  <th>ПИН код</th>
                  <th>Дата регистации</th>
                  </tr>
                  </thead>
                  <tbody>
                    @foreach($refers as $user)
                    <tr>
                      <td>{{ $user->name}}</td>
                      <td>{{ $user->familiya}}</td>
                      <td>{{ $user->screch}}</td>
                      <td>{{ gmdate("Y-m-d", $user->reg_date) }}</td>
                    </tr>
                    @endforeach
                  </tbody>
                  </table>
                  @else
                  Никого не пригласил
                  @endif
</div>

            </div>

</div>
</div>
</div>

<style type="text/css">
  .cabinet_main_container .auth_cont .form-control {
    margin-top: 0 !important;
}
</style>


