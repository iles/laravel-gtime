<div class="locales">
@foreach(config('app.locales') as $key => $local)
    <button type="button" class="btn btn-primary {{ ($key == $sel_local) ? 'active' : ''}}" value="{{$key}}">{{$local}}</button>
@endforeach
</div>
@push('customScripts')
	<script src="{{ asset('js/lang_switcher.js')}}"></script>
@endpush
