<?php 
	use Illuminate\Support\Facades\Session;
?>

@extends('layouts.app')


@section('content')


				@if(Session::has('message'))
                  <div class="card-header">
						<p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
                   </div>
				@endif

                <div class="card-body">
					{!! $grid !!}
                	
                </div>



@endsection

@section('scripts')
	<script type="text/javascript">
		$('.grid-title').html('Касса');

		$('.print').attr('target', '_blank');
	</script>

	<script type="text/javascript">
		var i = 0;

		function addField(){
			var t = '<div class="extra-pin col-md-6" id="ep'+i+'" style="margin-bottom: 20px;"><select class="form-control" name="users['+i+'][type]"><option value="1">Старт</option><option value="3">Основная</option><option value="4">Накопительная</option><option value="5">Накопительная +</option><option value="6">VIP</option><option value="7">Fast</option></select><a href="#" onclick="extDelete('+i+');  return false">Удалить</a></div>';
			$('.extra-pins').append(t);
			i++;
      	}

      	function pinstat(program, id, status){


				$.post( "/pin_stat", { program : program, id : id, status : status } )
				  .done(function( data ) {
				  	console.log(data);

				  	if(data.success ){
				  		$('#res'+id+'prog'+program).html(data.res);
				  		if(status == 0){
				  			$('#sign'+id+'prog'+program).html('Заблокировать');
				  			$('#sign'+id+'prog'+program).attr('onclick', 'pinstat('+program+', '+id+' , 2); return false;');
				  			$('#res'+id+'prog'+program).css("color", "#23282c");
				  		} else {
				  			$('#sign'+id+'prog'+program).html('Разблокировать');
				  			$('#sign'+id+'prog'+program).attr('onclick', 'pinstat('+program+', '+id+', 0); return false;');
				  			$('#res'+id+'prog'+program).css("color", "red");
				  		}
				  	} else {
				  		alert('произошла ошибка')

				  	}
				});	

				return false;
			}      	

			function pindelete(program, id){

				    var url = this.href;
				    var confirmText = "Вы уверены?";
				    if(confirm(confirmText)) {
						$.post( "/pin_delete", { program : program, id : id} )
						  .done(function( data ) {
						  	console.log(data);

						  	if(data.success ){
						  		$('#pin'+id+'prog'+program).fadeOut();
						  	} else {
						  		alert('произошла ошибка')

						  	}
						});	
				    }
				    return false;
			}




      	function reject(){
      		if($('#cancel_reason').val() == ''){
	      		$('#cancel_reason').addClass('form-control-danger').focus();
	      		$('.invalid-feedback').fadeIn();      			
      		} else {
      			$('#status_id').val(2);
      			$('#create_pin_form').submit();
      		}
      	}

      	function extDelete(id){
      		$('#ep'+id).remove();
      		return false;
      	}

      	function checkAvailability(e) {
      		var i = $(e).data('username');
			//$("#loaderIcon").show();
				jQuery.ajax({
					url: "/username",
					data:'username='+ $(e).val(),
					type: "get",
			success:function(data){
				if(data.error == 0){
						$('input').data('userid', i).val(data.m);
						$('span').data('userid', i).html('ok');
						pincode = 1;
			    		return;				
					}
			},
			error:function (){}
			});
			}
	</script>

@endsection