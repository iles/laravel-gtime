<style type="text/css">
  .modal-dialog{max-width: 1200px;}
</style>



<div class="card">
        @csrf
  <div class="card-header">
    <strong>{{$finished->fullname}}</strong>
  </div>

  <ul class="nav nav-tabs" id="myTab" role="tablist">
    <li class="nav-item">
      <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Инфо</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Последние логины</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Кого пригласил</a>
    </li>
  </ul>


  <div class="tab-content" id="myTabContent">
    <div class="tab-pane card-body fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
      <div class="row">

     <form action="/admin/finisheds/update/{{ $finished->id }}" id="create_pin_form" style="width: 100%;" class="form-horizontal" method="post" > 
      <div class="col-md-12">
          @csrf
          <div class="form-group row">
            <label class="col-md-3 col-form-label" for="hf-password">Логин</label>
            <div class="col-md-9">           
              <input type="hidden" value="{{$finished->program_id}}" name="program_id">
              <input class="form-control" name="login" value="{{$finished->login}}" type="text">
            </div>
          </div>                   

          <div class="form-group row">
            <label class="col-md-3 col-form-label" for="hf-password">Пин</label>
            <div class="col-md-9">           
              <input class="form-control" name="registred_pin" value="{{$finished->registred_pin}}" type="text">
            </div>
          </div>                  

          <div class="form-group row">
            <label class="col-md-3 col-form-label" for="hf-password">Дата рождения</label>
            <div class="col-md-9">           
              <input class="form-control" name="birthdate" value="{{$finished->birthdate}}" type="text">
            </div>
          </div>         

          <div class="form-group row">
            <label class="col-md-3 col-form-label" for="hf-password">ИИН</label>
            <div class="col-md-9">           
              <input class="form-control" name="iin" value="{{$finished->iin}}" type="text">
            </div>
          </div>              

           <div class="form-group row">
            <label class="col-md-3 col-form-label" for="hf-password">Страна/Город</label>
            <div class="col-md-9">           
              <input class="form-control" name="country" value="{{$finished->country}}" type="text">
            </div>
          </div>                    

          <div class="form-group row">
            <label class="col-md-3 col-form-label" for="hf-password">Телефон</label>
            <div class="col-md-9">           
              <input class="form-control" name="phone" value="{{$finished->phone}}" type="text">
            </div>
          </div>               

          <div class="form-group row">
            <label class="col-md-3 col-form-label" for="hf-password">INFO</label>
            <div class="col-md-9">           
              <input class="form-control" name="info" value="{{$finished->info}}" type="text">
            </div>
          </div>              

          <div class="form-group row">
            <input type="text" name="id"  value="{{$finished->setr}}" hidden/>
            <label class="col-md-3 col-form-label" for="hf-password">SETR</label>
            <div class="col-md-9">           
              <input class="form-control" name="setr" value="{{ $setr->setr }}" type="text">
            </div>
          </div>

          <div class="form-group row">
                                   <button class="btn btn-sm btn-primary" type="submit" style="margin: 40px 0 0 10px;">
                      <i class="fa fa-dot-circle-o" type="submit" ></i> 
                            Сохранить
                      </button>
          </div>           
        </form>

         

        </div>
 
  </div>



   
    </div>




  


  <div class="tab-pane card-body fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
  <div class="row">
              <div class="col-md-12">
              @foreach(json_decode($finished->last, true) as $user)
    <div class="row">
      <div class="col-md-5 firsts" style="border-bottom: 1px solid #c67e7e; margin-bottom: 12px;">
        <p><span style="font-weight: bold;">{{ $lastList[ $user['uid'] ]['name'] }}</span> <br/>
          <small>{{ $lastList[ $user['uid'] ]['familiya'] }};</small> <br/>
          <small>{{ $lastList[ $user['uid'] ]['screch'] }};</small> <br/>
          @if($finished->program_id == 3 || $finished->program_id == 6)
          <small><a href="#" class="watch" onclick="load(); return false;">Смотреть</a></small>
          @endif
        </p>
        <p>@if(isset($user['pin'])){{ $user['pin'] }}@endif</p>
      </div>
      <div class="col-md-7 others"></div>
    </div>
    @endforeach    
          </div>
  </div>  
  </div>
  


  <div class="tab-pane card-body fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
    <div class="row">
      <div class="col-md-12">
              <div class="card-body">

                @if( count($refs) > 0 )
                <table class="table table-responsive-sm">
                  <thead>
                  <tr>
                  <th>Логин</th>
                  <th>ФИО</th>
                  <th>ПИН код</th>
                  <th>Дата регистации</th>
                  </tr>
                  </thead>
                  <tbody>
                    @foreach($refs as $user)
                    <tr>
                      <td>{{ $user->name}}</td>
                      <td>{{ $user->familiya}}</td>
                      <td>{{ $user->screch}}</td>
                      <td>{{ gmdate("Y-m-d", $user->reg_date) }}</td>
                    </tr>
                    @endforeach
                  </tbody>
                  </table>
                  @else
                  Никого не пригласил
                  @endif
                </div>

    </div>

  </div>
</div>

                <div class="card-footer">
                  <button class="btn btn-sm btn-danger" data-dismiss="modal" aria-label="Close">      
                   <i class="fa fa-ban"></i> Закрыть</button>
                </div>



    </div>




    
</div>
