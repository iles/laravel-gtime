<div class="card">
  @if($model->status_id == 0)
   <form method="post" action="{{ action('PinsController@statusupdate') }}" id="create_pin_form" class="form-horizontal" accept-charset="UTF-8"> 
  @else
   <form method="post" action="{{ action('PinsController@update') }}" id="create_pin_form" class="form-horizontal" accept-charset="UTF-8"> 
  @endif
  <div class="card-header">
    <strong>{{$model->uname}}</strong></div>
    <div class="card-body">
        @csrf

        <div class="form-group row">
          <input type="text" name="id"  value="{{$model->id}}" hidden/>
          <label class="col-md-3 col-form-label" for="hf-password">Имя</label>
          <div class="col-md-9">           
            <input class="form-control" name="name" value="{{$model->uname}}" type="text">
          </div>
        </div>        

        <div class="form-group row">
          <label class="col-md-3 col-form-label" for="hf-password">ИИН</label>
          <div class="col-md-9">           
            <input class="form-control" name="iin" value="{{$model->iin}}" type="text">
          </div>
        </div>                

        <div class="form-group row">
          <label class="col-md-3 col-form-label" for="hf-password">Номер квитанции</label>
          <div class="col-md-9">           
            <input class="form-control" name="bill" value="{{$model->bill}}" type="text">
          </div>
        </div>            

        <div class="form-group row">
          <label class="col-md-3 col-form-label" for="hf-password">Сумма</label>
          <div class="col-md-9">           
            <input class="form-control" name="summ" value="{{$model->summ}}" type="text">
          </div>
        </div>             

        <div class="form-group row">
          <label class="col-md-3 col-form-label" for="hf-password">Skype</label>
          <div class="col-md-9">           
            <input class="form-control" name="skype" value="{{$model->skype}}" type="text">
          </div>
        </div>      

        @if($model->image != 0)
        <a data-fancybox="gallery" href="/uploads/bills/{{$model->image}}"><img src="/uploads/bills/{{$model->image}}" style="width: 80px;"></a>
        @endif

        @if($model->status_id == 0)


        <div class="form-group row">
          <input type="text" name="id" value="{{$model->id}}" hidden/>
          <input type="text" name="status" id="status_id" value="1" hidden/>
          <label class="col-md-3 col-form-label" for="hf-password">Причина отказа</label>
          <div class="col-md-9">           
            <textarea name="cancel_reason"  class="form-control" id="cancel_reason">{{$model->cancel_reason}}</textarea>
          </div>
          <div class="invalid-feedback">Укажите причину отказа</div>
        </div>          


        
        @endif

        <div class="form-group row" style="margin-top: 25px">
          @foreach($pins as $name => $prog)
            @if( count($prog) > 0 )
              <div class="col-md-12" style="margin-bottom: 25px">
                @switch($name)
                    @case('start')
                        <p>Старт</p>
                        @break
                    @case('auto')
                        <p>Авто</p>
                        @break                  

                    @case('main')
                        <p>Основная</p>
                        @break

                    @case('acummulative')
                        <p>Накопительная</p>
                        @break                  

                    @case('acummulative_plus')
                        <p>Накопительная +</p>
                        @break                  

                    @case('vip')
                        <p>VIP</p>
                        @break                  

                    @case('fast')
                        <p>FAST</p>
                        @break
                    @default
                        <p></p>
                @endswitch
                @foreach($prog as $key => $pin)
                  <div class="row" id="pin{{$pin->id}}prog{{$pin->program}}">
                    <div class="col-md-6"><b>{{ $pin->pin }}</b> <br/> 
                      
                        @if($pin->status_id == 0)
                         <pre id="res{{$pin->id}}prog{{$pin->program}}">Действителен до: <br/>{{ $pin->expired_at }}</pre>
                        @else
                         <pre style="color: red;" id="res{{$pin->id}}prog{{$pin->program}}">Заблокирован</pre>
                        @endif
                      
                    </div>
                    <div class="col-md-6">
                      @if($pin->status_id == 0)
                        <a href="#" class="pinstat" id="sign{{$pin->id}}prog{{$pin->program}}" onclick="pinstat( {{$pin->program }}, {{ $pin->id }}, 2  ); return false;" >Блокировать</a>
                      @else
                        <a href="#" class="pinstat" id="sign{{$pin->id}}prog{{$pin->program}}" onclick="pinstat( {{$pin->program }}, {{ $pin->id }}, 0  ); return false;" >Разблокировать</a>
                      @endif
                      <br/>
                      <a href="#" class="pindelete" data-trigger-confirm="1" onclick="pindelete( {{$pin->program }}, {{ $pin->id }}  ); return false;" >Удалить</a>
                      
                    </div>
                  </div>

                @endforeach
              </div>
            @endif
          @endforeach
        </div> 


        <div class="form-group row">
          <label class="col-md-3 col-form-label" for="hf-password">Приписка</label>
          <div class="col-md-9">
            <textarea name="note"  class="form-control" >{{$model->note}}</textarea>
          </div>
        </div>



        <div class="form-group row extra-pins">
          
        </div>




        
    </div>
        
    <div class="card-footer">
      <button class="btn btn-sm btn-primary" type="submit">
      <i class="fa fa-dot-circle-o" type="submit"></i> 
      @php
       echo $model->status_id == 0 ? 'Одобрить' : 'Сохранить';
      @endphp
      </button>
      @if($model->status_id == 0)
        <button class="btn btn-sm btn-danger" onclick="reject(); return false;">      
        <i class="fa fa-ban"></i> Отклонить</button>
      @endif

      <button class="btn btn-sm btn-primary pull-right" onclick="addField(); return false;"> <i class="fa fa-plus"></i> Добавить</button>
    </div>

    </form>
</div>