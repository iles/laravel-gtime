<div class="card">
   <form <form method="post" action="{{ action('PinsController@store_kassa') }}" id="create_pin_form" class="form-horizontal" accept-charset="UTF-8" enctype="multipart/form-data"> 
  <div class="card-header">
    <strong>Генериравать пин</strong></div>
    <div class="card-body">
        @csrf

        <div class="form-group row">
          <label class="col-md-3 col-form-label">Логин</label>
          <div class="col-md-9">
           
            <input class="form-control" id="info-input" name="uname"  type="text">
          </div>
        </div>          

        <div class="form-group row">
          <label class="col-md-3 col-form-label">№ Квитанции</label>
          <div class="col-md-9">
           
            <input class="form-control" name="bill" type="text" >
          </div>
        </div>         

        <div class="form-group row">
          <label class="col-md-3 col-form-label">ИИН/ИНН</label>
          <div class="col-md-9">
            <input class="form-control" name="iin" type="text" >
            <input name="user_id" hidden="hidden" value="0">
            <input name="kassa" hidden="hidden" value="1">
          </div>
        </div>           

        <div class="form-group row">
          <label class="col-md-3 col-form-label">Сумма</label>
          <div class="col-md-9">           
            <input class="form-control" name="summ" type="text">
          </div>
        </div>          

        <div class="form-group row">
          <label class="col-md-3 col-form-label">Skype</label>
          <div class="col-md-9">
           
            <input class="form-control" name="skype" type="text" >
          </div>
        </div>           

        <div class="form-group row">
          <label class="col-md-3 col-form-label">Фото</label>
          <div class="col-md-9">           
            <input type="file" name="image" id="image" >
          </div>
        </div>

        <div class="form-group row">
          <label class="col-md-3 col-form-label" for="hf-password">Примечание</label>
          <div class="col-md-9">           
            <textarea name="note"  class="form-control" ></textarea>
          </div>
        </div> 


        <div class="form-group row extra-pins">
          
        </div>



        
    </div>
        
    <div class="card-footer">
      <button class="btn btn-sm btn-primary" type="submit">
      <i class="fa fa-dot-circle-o"></i> Создать</button>
      <button class="btn btn-sm btn-danger"  data-dismiss="modal" aria-label="Close" type="reset">
      <i class="fa fa-ban"></i> Отмена</button>
      <button class="btn btn-sm btn-primary pull-right" onclick="addField(); return false;"> <i class="fa fa-plus"></i> Добавить</button>
    </div>
    </form>
</div>

