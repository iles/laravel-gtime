<style type="text/css">
  .modal-dialog{max-width: 1200px;}
</style>

  @if($model->status_id == 0)
   <form method="post" action="{{ action('PinsController@statusupdate') }}" id="create_pin_form" class="form-horizontal" accept-charset="UTF-8"> 
  @else
   <form method="post" action="{{ action('PinsController@update') }}" id="create_pin_form" class="form-horizontal" accept-charset="UTF-8"> 
  @endif

<div class="card">
        @csrf
  <div class="card-header">
    <strong>{{$model->uname}}</strong></div>
    <div class="card-body">

      <div class="row">
        
          <div class="col-md-6">
          
          <div class="form-group row">
            <input type="text" name="id"  value="{{$model->id}}" hidden/>
            <label class="col-md-3 col-form-label" for="hf-password">Логин</label>
            <div class="col-md-9">           
              <input class="form-control" name="name" value="{{$model->uname}}" type="text">
            </div>
          </div>         

          <div class="form-group row">
            <input type="text" name="id"  value="{{$model->id}}" hidden/>
            <label class="col-md-3 col-form-label" for="hf-password">ФИО</label>
            <div class="col-md-9">           
              <input class="form-control" name="name" value="{{$model->fullname}}" type="text">
            </div>
          </div>        

          <div class="form-group row">
            <label class="col-md-3 col-form-label" for="hf-password">ИИН</label>
            <div class="col-md-9">           
              <input class="form-control" name="iin" value="{{$model->iin}}" type="text">
            </div>
          </div>                

          <div class="form-group row">
            <label class="col-md-3 col-form-label" for="hf-password">Номер квитанции</label>
            <div class="col-md-9">           
              <input class="form-control" name="bill" value="{{$model->bill}}" type="text">
            </div>
          </div>            

          <div class="form-group row">
            <label class="col-md-3 col-form-label" for="hf-password">Сумма</label>
            <div class="col-md-9">           
              <input class="form-control" name="summ" value="{{$model->summ}}" type="text">
            </div>
          </div>       

          <div class="form-group row">
            <label class="col-md-3 col-form-label" for="hf-password">Валюта</label>
            <div class="col-md-9">           
              <input class="form-control" name="summ" value="{{$model->currency}}" type="text">
            </div>
          </div>             

          <div class="form-group row">
            <label class="col-md-3 col-form-label" for="hf-password">Skype</label>
            <div class="col-md-9">           
              <input class="form-control" name="skype" value="{{$model->skype}}" type="text">
            </div>
          </div>               

          <div class="form-group row">
            <label class="col-md-3 col-form-label" for="hf-password">Страна</label>
            <div class="col-md-9">           
              <input class="form-control" name="skype" value="{{$model->country}}" type="text">
            </div>
          </div>            

          <div class="form-group row">
            <label class="col-md-3 col-form-label" for="hf-password">Примечание</label>
            <div class="col-md-9">           
              <textarea class="form-control">{{$model->user_note}}</textarea>
            </div>
          </div>           

          <div class="form-group row">
            <label class="col-md-3 col-form-label" for="hf-password">Фото квитанции</label>
            <div class="col-md-9">           
                @if($model->image != 0)
                    @foreach( explode(';', $model->image) as $image)
                      <a data-fancybox="gallery" href="/uploads/bills/{{$image  }}"><img src="/uploads/bills/{{$image }}" style="width: 80px;"></a>
                    @endforeach
                @endif
            </div>
          </div>      
         
        </div>


        <div class="col-md-6">
            

            <div class="form-group row" style="margin-top: 25px">
          @foreach($pins as $name => $prog)
            @if( count($prog) > 0 )
              <div class="col-md-12" style="margin-bottom: 25px">
                @switch($name)
                    @case('1')
                        <p>Старт</p>
                        @break
                    @case('2')
                        <p>Авто</p>
                        @break                  

                    @case('3')
                        <p>Основная</p>
                        @break

                    @case('4')
                        <p>Накопительная</p>
                        @break                  

                    @case('5')
                        <p>Накопительная +</p>
                        @break                  

                    @case('6')
                        <p>VIP</p>
                        @break                  

                    @case('7')
                        <p>FAST</p>
                        @break                    
                    @case('8')
                        <p>Bonus</p>
                        @break
                    @default
                        <p></p>
                @endswitch
                @foreach($prog as $key => $pin)
                  <div class="row" id="pin{{$pin->id}}prog{{$pin->program}}">
                    <div class="col-md-6"><b>{{ $pin->pin }}</b> <br/> 
                      
                        @if($pin->status_id == 0)
                         <pre id="res{{$pin->id}}prog{{$pin->program}}">Действителен до: <br/>{{ $pin->expired_at }}</pre>
                        @elseif($pin->status_id == 2)
                         <pre style="color: red;" id="res{{$pin->id}}prog{{$pin->program}}">Заблокирован</pre>
                        @elseif($pin->status_id == 3)
                         <pre style="color: teal;" id="res{{$pin->id}}prog{{$pin->program}}">Активирован</pre>
                        @endif
                      
                    </div>
                    <div class="col-md-6">
                      @if($pin->status_id == 0)
                        <a href="#" class="pinstat" id="sign{{$pin->id}}prog{{$pin->program}}" onclick="pinstat( {{$pin->program }}, {{ $pin->id }}, 2  ); return false;" >Блокировать</a>
                      @elseif($pin->status == 1)
                        <a href="#" class="pinstat" id="sign{{$pin->id}}prog{{$pin->program}}" onclick="pinstat( {{$pin->program }}, {{ $pin->id }}, 0  ); return false;" >Разблокировать</a>
                      @elseif($pin->status == 3)

                      @endif
                      <br/>
                      <a href="#" class="pindelete" data-trigger-confirm="1" onclick="pindelete( {{$pin->program }}, {{ $pin->id }}  ); return false;" >Удалить</a>
                      
                    </div>
                  </div>

                @endforeach
              </div>
            @endif
          @endforeach
        </div> 


        <div class="form-group row">
          <label class="col-md-3 col-form-label" for="hf-password">Приписка</label>
          <div class="col-md-9">
            <textarea name="note"  class="form-control" >{{$model->note}}</textarea>
          </div>
        </div>



        <div class="form-group row extra-pins">
          
        </div>

      @if($model->program == 6 && $model->security_check !== 1)

      @else
        <div class="form-group row">
          <div class="col-md-12">
            <div class="row"> <div class="col-md-12"><p><b>Генерация пин кодов:</b></p></div> </div>
            <div class="row">
              <label class="col-md-4 col-form-label" for="hf-password">Старт</label>
              <div class="col-md-8">           
                <input type="number" min="0" name="start_pin" >
              </div>            
            </div>          
            <div class="row">
              <label class="col-md-4 col-form-label" for="hf-password">Накопительная</label>
              <div class="col-md-8">           
                <input type="number" min="0" name="bank_pin" >
              </div>            
            </div>            
        
            <div class="row">
              <label class="col-md-4 col-form-label" for="hf-password">Основная</label>
              <div class="col-md-8">           
                <input type="number" min="0" name="main_pin" >
              </div>            
            </div>           

            <div class="row">
              <label class="col-md-4 col-form-label" for="hf-password">Vip</label>
              <div class="col-md-8">           
                <input type="number" min="0" name="vip_pin" >
              </div>            
            </div>             
          

            <div class="row">
              <label class="col-md-4 col-form-label" for="hf-password">Fast</label>
              <div class="col-md-8">           
                <input type="number" min="0" name="fast_pin" >
              </div>            
            </div>

            <div class="row">
              <label class="col-md-4 col-form-label" for="hf-password">Bonus</label>
              <div class="col-md-8">           
                <input type="number" min="0" name="bonus_pin" >
              </div>            
            </div>
          </div>
        </div> 
        @endif


        @if($model->status_id == 0)
        <br><br><br>

        <div class="form-group row">
          <input type="text" name="id" value="{{$model->id}}" hidden/>
          <input type="text" name="status" id="status_id" value="1" hidden/>
          <label class="col-md-3 col-form-label" for="hf-password">Причина отказа</label>
          <div class="col-md-9">           
            <textarea name="cancel_reason"  class="form-control" id="cancel_reason">{{$model->cancel_reason}}</textarea>
          </div>
          <div class="invalid-feedback">Укажите причину отказа</div>
        </div>          

        

        </div>



      </div>

        




    


        
        @endif

        
    </div>
            </div>
    <div class="card-footer">
      
      @if($model->program == 6 && $model->security_check !== 1)
        <strong>Заявка не одобрена службой безопасности</strong>
      @else

      <button class="btn btn-sm btn-primary" type="submit">
      <i class="fa fa-dot-circle-o" type="submit" ></i> 
      @php
       echo $model->status_id == 0 ? 'Одобрить' : 'Сохранить';
      @endphp
      </button>

      @if($model->status_id == 0)
        <button class="btn btn-sm btn-danger" onclick="reject(); return false;">      
        <i class="fa fa-ban"></i> Отклонить</button>
      @endif


      @endif
</div>
</form>


    
</div>