<div class="card">@foreach($pins as $name => $prog)
            @if( count($prog) > 0 )
              <div class="col-md-12" style="margin-bottom: 25px">
                @switch($name)
                    @case('start')
                        <p>Старт</p>
                        @break
                    @case('auto')
                        <p>Авто</p>
                        @break                  

                    @case('main')
                        <p>Основная</p>
                        @break

                    @case('acummulative')
                        <p>Накопительная</p>
                        @break                  

                    @case('acummulative_plus')
                        <p>Накопительная +</p>
                        @break                  

                    @case('vip')
                        <p>VIP</p>
                        @break                  

                    @case('fast')
                        <p>FAST</p>
                        @break
                    @default
                        <p></p>
                @endswitch
                @foreach($prog as $key => $pin)
                  <div class="row" id="pin{{$pin->id}}prog{{$pin->program}}">
                    <div class="col-md-12"><b>{{ $pin->pin }}</b> <br/> 
                      
                        @if($pin->status_id == 0)
                         <pre id="res{{$pin->id}}prog{{$pin->program}}">Действителен до: <br/>{{ $pin->expired_at }}</pre>
                        @else
                         <pre style="color: red;" id="res{{$pin->id}}prog{{$pin->program}}">Заблокирован</pre>
                        @endif
                      
                    </div>
                  </div>

                @endforeach
              </div>
            @endif
          @endforeach
</div>