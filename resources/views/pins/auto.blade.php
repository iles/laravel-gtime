<?php 
	use Illuminate\Support\Facades\Session;
?>

@extends('layouts.app')


@section('breadcrumbs')
	<li class="breadcrumb-item active">Авто пин-коды </li>
@endsection

@section('content')


				@if(Session::has('message'))
					<div class="row">
				<div class="card-body">
						<div class="col-md-12 col-xs-12 col-sm-12">
						<p style="background-color: #dff0d8; border-color: #d6e9c6; color: #3c763d; padding: 15px; margin-bottom: 20px; border-radius: 4px;" class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>

                   	</div>
					</div>
				</div>
				@endif

<div class="row">
	<div class="col-lg-12">
		<div class="card">
			<div class="card-header">
				<h2>Авто пин коды</h2>
			</div>

			<div class="card-body">
				<table class="table table-responsive-sm table-bordered table-striped table-sm">
					<thead>
						<tr>
							<th>Пин код</th>
							<th>Активный</th>
							<th>Логин</th>
							<th>Дата регистрации</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						@foreach($pins as $pin)
						<tr>
							<td>{{ $pin->pin}}</td>
							<td>
								@if($pin->status_id == 0)
									<span class="badge badge-success">Свободен</span>
								@else
									<span class="badge badge-secondary">Занят</span>
								@endif
							</td>
							<td>{{ $pin->username }}</td>
							<td>
								@if($pin->status_id == 3)
									{{ $pin->updated_at }}
								@endif
							</td>
							<td>
								@if($pin->status_id == 3)
									<a class="btn btn-sm btn-ghost-success free" data-id="{{$pin->id}}" data-trigger-confirm="1" type="button">Освободить</a>
								@endif
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>

			</div>
		</div>
	</div>

</div>

@endsection

@section('scripts')

<script type="text/javascript">

	function sendFree(id) {
		jQuery.ajax({
			url: "/autofree",
			data: {
		        "_token": "{{ csrf_token() }}",
		        "id": id
	        },
			type: "POST",
			success:function(data){
				document.location.reload(true);		
			},
			error:function (){}
		});
	}

	$('.free').on('click', function(){
		var isAdmin = confirm("Вы - уверены?");
		if(isAdmin){
			sendFree( $(this).data('id') );
		}
	})

</script>

@endsection

