<div class="card">
   <form method="post" action="{{ action('SecurityController@aprove') }}" id="create_pin_form" class="form-horizontal" accept-charset="UTF-8"> 
  <div class="card-header">
    <strong>{{$model->uname}}</strong></div>
    <div class="card-body">
        @csrf

        <div class="form-group row">
          <input type="text" name="id"  value="{{$model->id}}" hidden/>
          <label class="col-md-3 col-form-label" for="hf-password">Логин</label>
          <div class="col-md-9">           
            <input class="form-control" name="name" value="{{$model->uname}}" type="text">
          </div>
        </div>         

        <div class="form-group row">
          <input type="text" name="id"  value="{{$model->id}}" hidden/>
          <label class="col-md-3 col-form-label" for="hf-password">ФИО</label>
          <div class="col-md-9">           
            <input class="form-control" name="name" value="{{$model->fullname}}" type="text">
          </div>
        </div>        

        <div class="form-group row">
          <label class="col-md-3 col-form-label" for="hf-password">ИИН</label>
          <div class="col-md-9">           
            <input class="form-control" name="iin" value="{{$model->iin}}" type="text">
          </div>
        </div>                

        <div class="form-group row">
          <label class="col-md-3 col-form-label" for="hf-password">Номер квитанции</label>
          <div class="col-md-9">           
            <input class="form-control" name="bill" value="{{$model->bill}}" type="text">
          </div>
        </div>            

        <div class="form-group row">
          <label class="col-md-3 col-form-label" for="hf-password">Сумма</label>
          <div class="col-md-9">           
            <input class="form-control" name="summ" value="{{$model->summ}}" type="text">
          </div>
        </div>             

        <div class="form-group row">
          <label class="col-md-3 col-form-label" for="hf-password">Skype</label>
          <div class="col-md-9">           
            <input class="form-control" name="skype" value="{{$model->skype}}" type="text">
          </div>
        </div>               

        <div class="form-group row">
          <label class="col-md-3 col-form-label" for="hf-password">Страна</label>
          <div class="col-md-9">           
            <input class="form-control" name="skype" value="{{$model->country}}" type="text">
          </div>
        </div>            

        <div class="form-group row">
          <label class="col-md-3 col-form-label" for="hf-password">Примечание</label>
          <div class="col-md-9">           
            <textarea class="form-control">{{$model->user_note}}</textarea>
          </div>
        </div>           

        <div class="form-group row">
          <label class="col-md-3 col-form-label" for="hf-password">Фото квитанции</label>
          <div class="col-md-9">           
              @if($model->image != 0)
                  @foreach( explode(';', $model->image) as $image)
                    <a data-fancybox="gallery" href="/uploads/bills/{{$image  }}"><img src="/uploads/bills/{{$image }}" style="width: 80px;"></a>
                  @endforeach
              @endif
          </div>
        </div>      






        <div class="form-group row">
          <label class="col-md-3 col-form-label" for="hf-password">Приписка</label>
          <div class="col-md-9">
            <textarea name="note"  class="form-control" >{{$model->note}}</textarea>
          </div>
        </div>



 


        @if($model->status_id == 0)
        <br><br><br>

        <div class="form-group row">
          <input type="text" name="id" value="{{$model->id}}" hidden/>
          <input type="text" name="status" id="status_id" value="1" hidden/>
          <label class="col-md-3 col-form-label" for="hf-password">Причина отказа</label>
          <div class="col-md-9">           
            <textarea name="cancel_reason"  class="form-control" id="cancel_reason">{{$model->cancel_reason}}</textarea>
          </div>
          <div class="invalid-feedback">Укажите причину отказа</div>
        </div>          




        
        @endif

        
    </div>
        
    <div class="card-footer">

      <button class="btn btn-sm btn-primary" type="submit">
      <i class="fa fa-dot-circle-o" type="submit"></i>Одобрить</button>
      @if($model->status_id == 0)
        <button class="btn btn-sm btn-danger" onclick="reject(); return false;">      
        <i class="fa fa-ban"></i> Отклонить</button>
      @endif




    </div>

    </form>
</div>