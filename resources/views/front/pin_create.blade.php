@extends('front.layout.main')

@section('content')


@section('content')

<style type="text/css">


a.active{
	color: #ff0200;
	text-decoration: underline;
}



.tgbl{
	display: none;
}
.tgbl.active{
	display: block;
}
</style>
<div class="cabinet_main_container">
	@include('front.layout.menu')
		<div class="body">
			<strong> </strong>
		<div class="profile_body_text">
		
		@if(Session::has('message'))
                <div class="card-header" style="margin-bottom: 25px;">
            		<p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
                </div>
        @endif

        <a href="{{ route('partner.pin') }}" >Мои пин коды</a>
        <a href="{{ route('partner.pin.requests') }}" style="margin-left: 40px;">Заявки</a>        
        <a href="{{ route('partner.pin.create') }}" class="active"  style="margin-left: 40px;">Создать заявку</a>

		


							<div class="pin-request-table">
								<table class="profile_table">
									{{ Form::open(['route' => 'partner.pin.store', 'method'=>'post', 'autocomplete'=>"off", 'files' => true]) }}
									<tbody>
						
									<tr>
										<td>{{ Form::label('bill', 'Номер квитанции') }}</td>
										<td>{{ Form::text('bill', null, ['class'=>'form-control', 'required'=>'required']) }}</td>
									</tr>									
									<tr>
										<td>{{ Form::label('country', 'Страна оплатившего') }}</td>
										<td>
											<select name="country" class="form-control" name="country" id="country-select">
													<option value="default" selected="selected">Выберите страну</option>
												@foreach ($countries as $key => $value)
													<option value="{{  $value   }}" data-id="{{ $key }}">{{ $value }}</option>
												@endforeach
											</select>
										</td>
									</tr>									
									<tr>
										<td>{{ Form::label('country', 'Город оплатившего') }}</td>
										<td id="city_td">
											<select name="city" class="form-control" name="city" id="city-select">
												<option value="default" selected="selected">Выберите город</option>
											</select>

										</td>
									</tr>	
						

									<tr id="iin-form">
										<td>{{ Form::label('iin', 'ИИН/ИНН/№ паспорта') }}</td>
										<td>{{ Form::text('iin',  null, ['class'=>'form-control', 'required'=>'required']) }}</td>
									</tr>									
									<tr>
										<td>{{ Form::label('fullname', 'ФИО оплатившего') }} <br/> <small id="text-type">(Только кириллица)</small></td>
										<td>{{ Form::text('fullname_b',  null, ['class'=>'form-control', 'id'=>'fullname_b', 'required'=>'required']) }}</td>
									</tr>									
									<tr>
										<td>{{ Form::label('date', 'Дата оплаты') }}</td>
										<td>{{ Form::text('date',  null, ['class'=>'form-control datepicker-here', 'data-position' => 'left top', 'readonly'=>'readonly', 'required'=>'required']) }}</td>
									</tr>									
									<tr>
										<td>{{ Form::label('summ', 'Сумма') }}</td>
										<td>{{ Form::number('summ', null, ['class'=>'form-control', 'required'=>'required', 'min'=>0]) }}</td>
									</tr>									
									<tr>
										<td>{{ Form::label('currency', 'Валюта') }}</td>
										<td>
											
											<select name="currency" class="form-control" name="currency">
												<option value="KZT">KZT</option>
												<option value="USD">USD</option>
												<option value="EUR">EUR</option>
												<option value="RUB">RUB</option>
											</select>
										</td>
									</tr>
									<tr>
										<td>{{ Form::label('skype', 'Skype') }}</td>
										<td>{{ Form::text('skype', null, ['class'=>'form-control',  'required'=>'required']) }}</td>
									</tr>	
									<tr>
										<td>{{ Form::label('phone', 'Телефон') }}</td>
										<td>{{ Form::text('phone', null, ['class'=>'form-control']) }}</td>
									</tr>									
									<tr>
										<td>{{ Form::label('image', 'Фото') }}</td>
										<td>{{ Form::file('image[]', array('class' => 'form-control', 'required'=>'required', 'multiple'=>true, 'accept'=>".jpeg, .jpg, .png, .pdf") ) }}</td>
									</tr>
									<tr>
										<td>{{ Form::label('user_note', 'Примечание') }}</td>
										<td>{{ Form::textarea('user_note', null, ['class'=>'form-control']) }}</td>
									</tr>
									
									<tr>
										<td><button class="btn btn-sm btn-primary" type="submit">Отправить</button> </td>
									</tr>
								</tbody>
								{{ Form::close() }}
								</table>
							</div>

						</div>
					</div>
				</div>
@endsection

@section('scripts')
<script type="text/javascript">


$(document).ready( function(){

	$('#fullname_b').inputmask({ regex: "([ а-яА-Я]{1,150})" });
	
	$('#form-click').on( 'click', function(e){
		e.preventDefault();
		$('.profile_table').toggleClass('active');
	})

	$(":input").inputmask();

	function doCIties(data){
		alert(data);
		$('#country-select').html();
		var cities = jQuery.parseJSON(data);
	}

	$('#country-select').on('change', function(){
		var c = $(this ).find('option:selected').data('id');
		$('#other-city').remove();		
		$('#city-select').html('');

		$('#city-select').show();

		if(c == 4){
			$('#city-select').html('<optgroup label="Алматинская область"><option value="Алматинская область">Алматинская область</option><option value="Алматы">Алматы</option><option value="Бурундай">Бурундай</option><option value="Есик">Есик</option><option value="Жаркент">Жаркент</option><option value="Капчагай">Капчагай</option><option value="Каскелен">Каскелен</option><option value="Сарканд">Сарканд</option><option value="Талдыкорган">Талдыкорган</option><option value="Талгар">Талгар</option><option value="Турген">Турген</option><option value="Текели">Текели</option><option value="Ушарал">Ушарал</option><option value="Уштобе">Уштобе</option><option value="Узынагаш">Узынагаш</option><option value="Чунджа">Чунджа</option><option value="Шамалган">Шамалган</option><option value="Шелек">Шелек</option></optgroup><optgroup label="Акмолинская область"><option value="Акмолинская область">Акмолинская область</option><option value="Нур-Султан">Нур-Султан(Астана)</option><option value="Астраханка">Астраханка</option><option value="Акколь">Акколь</option><option value="Алтынды">Алтынды</option><option value="Атбасар">Атбасар</option><option value="Державинск">Державинск</option><option value="Ерейментау">Ерейментау</option><option value="Кокшетау">Кокшетау</option><option value="Коянды">Коянды</option><option value="Кощи">Кощи</option><option value="Макинск">Макинск</option><option value="Щучинск">Щучинск</option><option value="Степногорск">Степногорск</option><option value="Степняк">Степняк</option><option value="Шортанды">Шортанды</option></optgroup><optgroup label="Актюбинская область"><option value="Актюбинская область">Актюбинская область</option><option value="Актобе">Актобе</option><option value="Алга">Алга</option><option value="Эмба">Эмба</option><option value="Хромтау">Хромтау</option><option value="Кандыагаш">Кандыагаш</option><option value="Шалкар">Шалкар</option><option value="Темир">Темир</option><option value="Жем">Жем</option></optgroup><optgroup label="Атырауская область"><option value="Атырауская область">Атырауская область</option><option value="Атырау">Атырау</option><option value="Алмалы">Алмалы</option><option value="Аккистау">Аккистау</option><option value="Балыкшы">Балыкшы</option><option value="Доссор">Доссор</option><option value="Кульсары">Кульсары</option><option value="Карабатан">Карабатан</option><option value="Макат">Макат</option><option value="Махамбет">Махамбет</option><option value="Тендык">Тендык</option><option value="Таскала">Таскала</option></optgroup><optgroup label="Восточно-Казахстанская область"><option value="Восточно-Казахстанская область">Восточно-Казахстанская область</option><option value="Аягуз">Аягуз</option><option value="Чарск">Чарск</option><option value="Курчатов">Курчатов</option><option value="Риддер">Риддер</option><option value="Семей">Семей</option><option value="Серебрянск">Серебрянск</option><option value="Шемонаиха">Шемонаиха</option><option value="Усть-каменогорск">Усть-каменогорск</option><option value="Урджар">Урджар</option><option value="Зайсан">Зайсан</option><option value="Зыряновск">Зыряновск</option></optgroup><optgroup label="Жамбылская область"><option value="Жамбылская область">Жамбылская область</option><option value="Каратау">Каратау</option><option value="Шу">Шу</option><option value="Тараз">Тараз</option><option value="Жанатас">Жанатас</option><option value="Мерке">Мерке</option></optgroup><optgroup label="Западно-Казахстанская область"><option value="Западно-Казахстанская область">Западно-Казахстанская область</option><option value="Уральск">Уральск</option><option value="Уральск">Аксай</option><option value="Таскала">Таскала</option><option value="Жангала">Жангала</option><option value="Жымпиты">Жымпиты</option><option value="Жанибек">Жанибек</option><option value="Каратобе">Каратобе</option><option value="Казталовка">Казталовка</option><option value="Переметное">Переметное</option><option value="Сайхин">Сайхин</option><option value="Шынгырлау">Шынгырлау</option><option value="Чапаев">Чапаев</option><option value="Федоровка">Федоровка</option></optgroup><optgroup label="Карагандинская область"><option value="Карагандинская область">Карагандинская область</option><option value="Абай">Абай</option><option value="Атасу">Атасу</option><option value="Балхаш">Балхаш</option><option value="Караганда">Караганда</option><option value="Каражал">Каражал</option><option value="Каркаралинск">Каркаралинск</option><option value="Приозерск">Приозерск</option><option value="Сарань">Сарань</option><option value="Сатпаев">Сатпаев</option><option value="Шахтинск">Шахтинск</option><option value="Темиртау">Темиртау</option><option value="Жезказган">Жезказган</option></optgroup><optgroup label="Кустанайская область"><option value="Кустанайская область">Кустанайская область</option><option value="Аркалык">Аркалык</option><option value="Костанай">Костанай</option><option value="Лисаковск">Лисаковск</option><option value="Рудный">Рудный</option><option value="Житикара">Житикара</option></optgroup><optgroup label="Кызылординская область"><option value="Кызылординская область">Кызылординская область</option><option value="Аральск">Аральск</option><option value="Байконур">Байконур</option><option value="Казалинск">Казалинск</option><option value="Кызылорда">Кызылорда</option><option value="Жанакорган">Жанакорган</option></optgroup><optgroup label="Мангистауская область"><option value="Мангистауская область">Мангистауская область</option><option value="Актау">Актау</option><option value="Форт-Шевченко">Форт-Шевченко</option><option value="Жанаозен">Жанаозен</option></optgroup><optgroup label="Павлодарская область"><option value="Павлодарская область">Павлодарская область</option><option value="Аксу">Аксу</option><option value="Экибастуз">Экибастуз</option><option value="Павлодар">Павлодар</option></optgroup><optgroup label="Северо-Казахстанская область"><option value="Северо-Казахстанская область">Северо-Казахстанская область</option><option value="Булаево">Булаево</option><option value="Мамлютка">Мамлютка</option><option value="Петропавловск">Петропавловск</option><option value="Сергеевка">Сергеевка</option><option value="Тайынша">Тайынша</option><option value="с.Новоишимка">с.Новоишимка</option></optgroup><optgroup label="Южно-Казахстанская область"><option value="Южно-Казахстанская область">Южно-Казахстанская область</option><option value="Арыс">Арыс</option><option value="Аксу">Аксу</option><option value="Жетысай">Жетысай</option><option value="Кентау">Кентау</option><option value="Казыгурт">Казыгурт</option><option value="Ленгер">Ленгер</option><option value="Шардара">Шардара</option><option value="Шымкент">Шымкент</option><option value="Шаульдер">Шаульдер</option><option value="Туркестан">Туркестан</option><option value="Сарыагаш">Сарыагаш</option></optgroup>');
				return false;	
		}

        $.ajax({
            url: 'https://api.vk.com/method/database.getCities?country_id='+ c +'&need_all=0&count=1000&access_token=24598f9424598f9424598f94462432ff5b2245924598f947942afdf2248612308b247ba&v=5.100.',
            type: 'POST',
            dataType: 'jsonp',
        }).done( function (data) {


            
            $.each( data.response.items, function( key, value ) {
	           	var opt = '<option value="' + value.title + '">' + value.title + '</option>';
				$('#city-select').append(opt);
			});
			$('#city-select').append('<option value="other">Другой</option>');

			if( c == 4 ){
				$('#iin-form').html('<td><label for="iin">ИИН/ИНН/№ паспорта</label></td><td><input class="form-control" required="required" name="iin" type="text" id="iin"></td>');
				$('#iin-form').fadeIn();
				$('#fullname_b').inputmask({ regex: "([ а-яА-Я]{1,150})" });
				$('#text-type').html('(Только кириллица)');
			} else if( c == 1 || c == 11){
				$('#fullname_b').inputmask({ regex: "([ а-яА-Я]{1,150})" });
			} else {
				$('#fullname_b').inputmask({ regex: "([ a-zA-Z]{1,150})" });
				$('#text-type').html('(Только латиница)');
				$('#iin-form').fadeOut();
				$('#iin-form').html('');
			}
			

        }).fail(function (xhr, textStatus, error) {
            var title, message;
            switch (xhr.status) {
                case 403:
                    title = xhr.responseJSON.errorSummary;
                    message = 'Please login to your server before running the test.';
                    break;
                default:
                    title = 'Invalid URL or Cross-Origin Request Blocked';
                    message = 'You must explictly add this site (' + window.location.origin + ') to the list of allowed websites in your server.';
                    break;
            }
            alert(message);
        });
	});

	$('#city-select').on('change', function(){
		if( $(this).val() == 'other' ){
			$('#city-select').hide();
			$('#city_td').append('<input class="form-control" required="required" id="other-city" name="city" type="text">');
			$('#other-city').show();
			$('#other-city').focus();
		} else {
			$('#city-select').show();
			$('#other-city').remove();			
		}
	}

)});


	// $('#country').on('change', function(){
	// 	if( $(this).val() == 'KZ' ){
	// 		$('#iin-form').html('<td><label for="iin">ИИН/ИНН/№ паспорта</label></td><td><input class="form-control" required="required" name="iin" type="text" id="iin"></td>');
	// 		$('#iin-form').fadeIn();
	// 		$('#fullname_b').inputmask({ regex: "([ а-яА-Я]{1,150})" });
	// 		$('#text-type').html('(Только кириллица)');
	// 	} else if( $(this).val() == 'RU' || $(this).val() == 'KG'){
	// 		$('#fullname_b').inputmask({ regex: "([ а-яА-Я]{1,150})" });
	// 	} else {
	// 		$('#fullname_b').inputmask({ regex: "([ a-zA-Z]{1,150})" });
	// 		$('#text-type').html('(Только латиница)');
	// 		$('#iin-form').fadeOut();
	// 		$('#iin-form').html('');
	// 	}
	// });


</script>
@endsection