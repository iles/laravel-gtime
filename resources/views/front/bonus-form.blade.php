@extends('front.layout.main')

@section('content')

<style type="text/css">


a.active{
	color: #ff0200;
	text-decoration: underline;
}



.tgbl{
	display: none;
}
.tgbl.active{
	display: block;
}
</style>
<div class="cabinet_main_container">
	@include('front.layout.menu')
		<div class="body">
			<strong> </strong>
		<div class="profile_body_text">
		
		@if(Session::has('message'))
                <div class="card-header" style="margin-bottom: 25px;">
            		<p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
                </div>
        @endif

<div class="card">


@if($invites->isEmpty())
Вы никого не пригласили
@else

@foreach($invites as $invite)
  @if(!$invite->bonus_id)
  @php
    $inv = json_decode($invite->invites);
    $reward = 10000 * count($inv);
    $fid = $invite->id;
  @endphp
    Вы пригласили:
    <br/>
    <br/>
    @foreach($inv as $i)
    <p><b>{{$i->user_name}}</b></p>
    @endforeach

    вознаграждение = {{ $reward  }}
    <br>
    <br>
    <br>
    <br>
    @include('front.finish.bonus-form', compact('reward', 'fid'))
  @else

  @php
    $bonus = \App\Models\Bonuses\StartBonusRequestModel::find($invite->bonus_id);
  @endphp

        @if($bonus)
          <div class="row" style="padding: 40px 0 0 10px;">
            @if($bonus->status_id == 0)
            Ваша заявка находится на рассмотрении
            @elseif($bonus->status_id == 1)
            Ваша заявка одобрена <br/>
            <br/>

            @elseif($bonus->status_id == 2)
            Ваша заявка отклонена <br/>
            <br/>
            <pre>
              {{ $bonus->cancel_reason }}
            </pre>
            <form action="/rebonus" method="POST">
              @csrf
              <input type="text" hidden  value="{{ $bonus->id }}" name="id">
              <button type="submit" href="#" class="btn btn-primary">Создать новую заявку</button>
            </form>
            @endif

          </div>
        @else
      asd
        @endif

<br><br><br><br><br>

  @endif
@endforeach

@endif

</div>





		</div>
					</div>
				</div>
@endsection
