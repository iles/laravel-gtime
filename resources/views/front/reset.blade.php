@extends('front.layout.main')

@section('content')

@php
  $m = Config::get('matrix.get');
    $p = Session::get('programm', 1);
@endphp

<div class="cabinet_main_container authorization">
  <div class="header">
    <h2 class="title">{{ $m[$p][1] }}</h2>
  </div>
  <div class="body">
    <div class="auth_cont">

    {{ Form::open(array('route' => 'partner.resethandle')) }}
        <p class="text">Введите ваш Email</p>
        @if(Session::has('message'))
          <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
        @endif

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        {{ Form::text('email', '', ['class'=>'form-control', 'placeholder'=>'Email']) }}

        <p class="error-message" style="color:red; font-weight: bold; display: none; margin-top: 15px;"></p>
        <input type="submit" value="Восстановить пароль" class="button" style="margin-top: 45px;">
      {{ Form::close() }}
    </div>
    </div>

        </div>
@endsection


@section('scripts')
  <script type="text/javascript">
    $(document).ready(function(){
      $('.auth_cont form').on('submit', function(e){
        e.preventDefault();
        $('error-message').html('');
        $('.error-message').fadeOut();
        var email = $( "input[name='email']").val();

        if(email == ''){
          $('.error-message').html('Введите email');
          $('.error-message').fadeIn();
          return false;
        }

        if( validateEmail(email) == false ){
          $('.error-message').html('Введите правильный email');
          $('.error-message').fadeIn();
          return false;
        }


        $.ajax({
          method: "POST",
          url: "/partner/password-reset",
          data:{
            '_token': '{{ csrf_token() }}',
            'email' : email,
          },
        }).done(function( msg ) {
          if(msg.error == 1){
            $('.error-message').html(msg.message);
            $('.error-message').fadeIn();
          }else{
              e.target.submit();
          }
        });

      })
    })

    function validateEmail(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    }
  </script>
@endsection