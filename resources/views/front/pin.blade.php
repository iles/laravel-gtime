@extends('front.layout.main')

@section('content')
<style type="text/css">
.timer div {
  display: inline-block;
  line-height: 1;
  padding: 20px;
  font-size: 40px;
}

.timer span {
  display: block;
  font-size: 20px;
  color: #000;
}

a.active{
	color: #ff0200;
	text-decoration: underline;
}

.timer #days {
  font-size: 100px;
  color: #db4844;
}
.timer #hours {
  font-size: 100px;
  color: #f07c22;
}
.timer #minutes {
  font-size: 100px;
  color: #f6da74;
}
.timer #seconds {
  font-size: 50px;
  color: #abcd58;
}

.tgbl{
	display: none;
}
.tgbl.active{
	display: block;
}
</style>
<div class="cabinet_main_container">
	@include('front.layout.menu')
		<div class="body">
			<strong> </strong>
		<div class="profile_body_text">
		
		@if(Session::has('message'))
                <div class="card-header" style="margin-bottom: 25px;">
            		<p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
                </div>
        @endif

        <a href="{{ route('partner.pin') }}" class="active">Мои пин коды</a>
        <a href="{{ route('partner.pin.requests') }}" style="margin-left: 40px;">Заявки</a>        
        <a href="{{ route('partner.pin.create') }}" style="margin-left: 40px;">Создать заявку</a>

        <div class="row" style="margin-top: 50px;">
        	<div class="col-md-12">
        		@php
        			$pins = 0;
        		@endphp
		        @foreach($pin_requests as $key => $pr)
			        


		        	@if( $pr->pins['total'] > 0 )
	        		@php
	        			$pins ++;
	        		@endphp
		        	<p><small style="font-size: 65%;">Заявка № {{ $pr->id }} от {{ $pr->created_at }}</small></p>
		        			@foreach($pr->pins as $k => $prog)


		        				@if( is_array($prog) > 0 )
			        				@foreach($prog as $p => $pin)
			        				<div class="row">
			        					<div class="col-md-4">
			        						<b>{{ $pin['pin'] }}</b>
			        					</div>
				        				<div class="col-md-8">
											@if( $pin['status_id'] == 0 )
					                    		<pre>Действителен до: <br/>{{ $pin['expired_at'] }}</pre>
					                    	@elseif($pin['status_id'] == 3 )
					                    		<pre style="color: teal;">Активирован</pre>
					                    	@else
					                    		<pre style="color: red;">Заблокирован</pre>
					                    	@endif			        					
				        				</div>			        					
			        				</div>
		        					@endforeach
		        				@endif
		        			@endforeach
		        	@endif
		        @endforeach
		        @if($pins == 0)
		        	Что бы получить пин код необходимо <a href="{{ route('partner.pin.create') }}"> оставить заявку</a>.
		        @endif      		
        	</div>
        </div>
@endsection