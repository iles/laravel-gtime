@extends('front.layout.main')

@section('content')
        @php

		$m = Config::get('matrix.get');
        $p = Session::get('programm', 1);

		$class = $m[$p][0];

		 if($p == 3 || $p == 6 || $p == 8 ){
		 	$class .= $stage == 0 ? ' st' : ' fin';
		 }

          if($p == null){
            Session::put('programm', 1);
            $p = Session::get('programm', 1);
          }

          if($p == 1 || $p == 3 ){

          	if($p == 3 && $stage == 1){
	            $pk = 3;
	          	$pt = 4;
	          	$pl = 2;
          	} else {
	          	$pk = 4;
	          	$pt = 8;
	          	$pl = 3;
          	}
      	} else {
            $pk = 3;
          	$pt = 4;
          	$pl = 2;
      }
        @endphp

<div class="cabinet_main_container">
					@include('front.layout.menu')
					<div class="body"> 
						<div class="stairs_body_text">
							<h2 class="title">{{__('Программа')}} {{ __($m[$p][1]) }}</h2>
							@if($in)
								<div class="note_info_cont"> {{__('Приветствуем новичка')}}: {{$in->name}} {{__('и желаем дальнейших успехов')}} !</div>
							@endif
							@if($out)
								<div class="note_info_cont">{{__('Поздравляем партнера')}}: {{$out->name}} {{__('с завершением полного цикла')}}</div>
							@endif
							<div class="search_cont">
								<input placeholder="Ваш логин:" class="form-control" id="login_search" type="text">
								<input value="Найти" class="button" type="submit" id="search_ladder">
								<input   type="file" hidden style="display: none;">
							</div>
							<h2 class="title">{{__('Просмотр лестницы пользователя')}} <span id="username" style="color:red;">{{ Auth::user()->name }}</span></h2>
							<div class="stairs_cont_wrap" style="width: 100%; width: 100%;overflow: auto; padding-left: 16px; box-sizing: border-box; ">
								<div class="stairs_cont" style="width: 98%; min-width: 641px;">
									@php
										$l = 0;
									@endphp

									@foreach ($res as $key => $node)
											@php
												$l = $key;
											@endphp
										<div class="row">
											@foreach($node as $v)
												<div class=" node {{ $class  }}">
													<div><span>{{ $v['uname'] }}</span>
														<div class="sponsor" > &nbsp; {{ $v['by_refer'] }}
													</div>
													<div>
														@for ($i = 0; $i < $v['stars']; $i++)
														  <img src="/front/images/matrix-star.png">
														@endfor
													</div>
													</div>
												</div>
											@endforeach
											@if($key == $pk)
												@if(count($node) < $pt )
													@php
													$c = $pt - count($node);
													@endphp
													@for ($i = 0; $i < $c; $i++)
													   <div class=" node {{ $class  }} empty"></div>
													@endfor
												@endif
											@endif
										</div>
									@endforeach

									@if($l == $pl)
									<div class="row">

										@for ($i = 0; $i < $pt; $i++)
											<div class=" node {{ $class  }} empty"></div>
										@endfor
									</div>
									@endif


								</div>
							</div>
							<div class="clearfix"></div>
						</div>
					</div>
				</div>
@endsection

@section('scripts')
	<script type="text/javascript">
		$(document).ready( function(){
			//search_ladder('{{  Auth::user()->name }}');
			$('#search_ladder').on('click', function(){
				if( $('#login_search').val() == '' ){
					alert('Введите логин');
					return false;
				} else {
					search_ladder( $('#login_search').val() );
				}
			})

			$('.node').on('click', function(){
				var name = $(this).find('span').html();
				search_ladder(name);
			})
		});

		function search_ladder(name){
			$('.stairs_cont').fadeOut();
			$('.stairs_cont').html('');
			$('#username').html(name);
			$.get( "/partner/search", { name: name })
			  .done(function( data ) {
			    if(data == 'Пользователь не найден'){
					$('.stairs_cont').html('<p style="color:red">'+ data + '</p>');
			    	$('.stairs_cont').fadeIn();
			    	return false;
			    }
			    build_ladder( data );
			  });
		}

		function build_ladder(data){
			var l = 0;
			var s = data.stage == 1;
			if(s == 1){
				st = 'fin';
			} else {
				st = 'st';

			}

			$('.stairs_cont').html('');
			$.each( data.levels, function( key, value ) {
				l = key;
				$('.stairs_cont').append('<div class="row level_ui_'+ key +'"></div> ');
				build_level(value, key, st)
			});

			if(l == {{$pl}}){
				if( {{$pl}} == 3){
				$('.stairs_cont').append('<div class="row level_ui_4"><div class="node  {{$m[$p][0]}} empty"></div><div class="node  {{$m[$p][0]}} empty"></div><div class="node  {{$m[$p][0]}} empty"></div><div class="node  {{$m[$p][0]}} empty" ></div><div class="node  {{$m[$p][0]}} empty"></div><div class="node  {{$m[$p][0]}} empty"></div><div class="node  {{$m[$p][0]}} empty"></div><div class="node  {{$m[$p][0]}} empty"></div></div> ');

				} else {
				$('.stairs_cont').append('<div class="row level_ui_3"><div class="node  {{$m[$p][0]}}  empty"></div><div class="node  {{$m[$p][0]}} empty"></div><div class="node  {{$m[$p][0]}} empty"></div><div class="node  {{$m[$p][0]}} empty"></div></div> ');
				}
			}

			$('.node').on('click', function(){
				var name = $(this).find('span').html();
				console.log(name);
				search_ladder(name);
			})


			$('.stairs_cont').fadeIn();

		}

		function build_level(value, key, st){
			$.each( value, function( keyv, valuev ) {
				$('.level_ui_'+ key ).append('<div class="node  {{ $m[$p][0]  }}  '+ st +'"><div><span>' + valuev.uname + '</span><div class="sponsor"> ' + valuev.by_refer + '</div><div>' + build_stars(valuev.stars) + '</div></div></div>');
			});

			if(key == {{ $pk }}){
				if( value.length < {{ $pt }} ){
					var c = {{ $pt }} - value.length;
					for (index = 0; index < c; ++index) {
						$('.level_ui_'+ key ).append('<div class="node  {{ $m[$p][0]  }} empty" "></div>');
					}
				}
			}
		}

		function build_stars($n){
			var stars = '';
			var i = 0;
			while (i < $n) {
			  stars = stars + '<img src="/front/images/matrix-star.png">'  ;
			  i++;
			}
			return stars;
		}
	</script>
@endsection
