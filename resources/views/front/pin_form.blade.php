@extends('front.layout.main')

@section('content')
<style type="text/css">
.timer div {
  display: inline-block;
  line-height: 1;
  padding: 20px;
  font-size: 40px;
}

.timer span {
  display: block;
  font-size: 20px;
  color: #000;
}

.timer #days {
  font-size: 100px;
  color: #db4844;
}
.timer #hours {
  font-size: 100px;
  color: #f07c22;
}
.timer #minutes {
  font-size: 100px;
  color: #f6da74;
}
.timer #seconds {
  font-size: 50px;
  color: #abcd58;
}

.tgbl{
	display: none;
}
.tgbl.active{
	display: block;
}
</style>
<div class="cabinet_main_container">
	@include('front.layout.menu')
		<div class="body">
			<strong> </strong>
		<div class="profile_body_text">
		
		@if(Session::has('message'))
                <div class="card-header" style="margin-bottom: 25px;">
            		<p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
                </div>
        @endif

        <a href="{{ route('partner.pin') }}">Мои пин коды</a>
        <a href="{{ route('partner.pin.requests') }}">Заявки</a>

		@if($pin)

			@if($pin->status_id == 0)
			<p class="pin-info"> Ваша заявка под номером <span> {{ $pin->id }}</span> от <span>{{ $pin->created_at }}</span> находится в режиме ожидания</p>
			@elseif($pin->status_id == 2)
			<p class="pin-info"> Ваша заявка под номером <span> {{ $pin->id }}</span> от <span>{{ $pin->created_at }}</span> отклонена</p>

			<p>
				<pre>{{ $pin->cancel_reason }}</pre>
			</p>

			@elseif($pin->status_id == 1)

			@foreach($pins as $name => $prog)

            @if( count($prog) > 0 )
              <div class="row" style="margin-bottom: 45px">
              <div class="col-md-12" >
                @switch($name)
                    @case('start')
                        <p>Старт</p>
                        @break
                    @case('auto')
                        <p>Авто</p>
                        @break                  

                    @case('main')
                        <p>Основная</p>
                        @break

                    @case('acummulative')
                        <p>Накопительная</p>
                        @break                  

                    @case('acummulative_plus')
                        <p>Накопительная +</p>
                        @break                  

                    @case('vip')
                        <p>VIP</p>
                        @break                  

                    @case('fast')
                        <p>FAST</p>
                        @break
                    @default
                        <p></p>
                @endswitch
                
                @php
                	$done = 0;
                	$i = count($prog);
                @endphp

                @foreach($prog as $key => $pin)
                  <div class="row">
                    <div class="col-md-4"><b>{{ $pin->pin }}</b></div>
                    <div class="col-md-6">
                    	@if( $pin->status_id == 0 )
                    	<pre>Действителен до: <br/>{{ $pin->expired_at }}</pre>
                    	@elseif($pin->status_id == 3 )
                    	<pre style="color: teal;">Активирован</pre>
                    	@else
                    	<pre style="color: red;">Заблокирован</pre>
                    	@endif
                    </div>
                  </div>
                  @php
                  	if($pin->status_id == 3){
                  		$i ++;
                  	}
                  @endphp
                @endforeach
                
              </div>

              </div>
            @endif

          @endforeach


			@endif

							
		@else
						<h4> Что бы сгеренировать пин-код, необходимо отправить запрос</h4> <br/>

							<div class="pin-request-table">
								<table class="profile_table">
									{{ Form::open(['route' => 'partner.pin.store', 'method'=>'post', 'autocomplete'=>"off", 'files' => true]) }}
									<tbody>
						

									<tr>
										<td>{{ Form::label('program', 'Программа') }}</td>
										<td>{{ $program }}</td>
									</tr>
									<tr>
										<td>{{ Form::label('bill', 'Номер квитанции') }}</td>
										<td>{{ Form::text('bill', null, ['class'=>'form-control', 'required'=>'required']) }}</td>
									</tr>									
									<tr>
										<td>{{ Form::label('country', 'Страна оплатившего') }}</td>
										<td><select name="country" class="form-control" id="country"><option value="AU">Австралия</option><option value="AT">Австрия</option><option value="AZ">Азербайджан</option><option value="AX">Аландские о-ва</option><option value="AL">Албания</option><option value="DZ">Алжир</option><option value="AS">Американское Самоа</option><option value="AI">Ангилья</option><option value="AO">Ангола</option><option value="AD">Андорра</option><option value="AQ">Антарктида</option><option value="AG">Антигуа и Барбуда</option><option value="AR">Аргентина</option><option value="AM">Армения</option><option value="AW">Аруба</option><option value="AF">Афганистан</option><option value="BS">Багамы</option><option value="BD">Бангладеш</option><option value="BB">Барбадос</option><option value="BH">Бахрейн</option><option value="BY">Беларусь</option><option value="BZ">Белиз</option><option value="BE">Бельгия</option><option value="BJ">Бенин</option><option value="BM">Бермудские о-ва</option><option value="BG">Болгария</option><option value="BO">Боливия</option><option value="BQ">Бонэйр, Синт-Эстатиус и Саба</option><option value="BA">Босния и Герцеговина</option><option value="BW">Ботсвана</option><option value="BR">Бразилия</option><option value="IO">Британская территория в Индийском океане</option><option value="BN">Бруней-Даруссалам</option><option value="BF">Буркина-Фасо</option><option value="BI">Бурунди</option><option value="BT">Бутан</option><option value="VU">Вануату</option><option value="VA">Ватикан</option><option value="GB">Великобритания</option><option value="HU">Венгрия</option><option value="VE">Венесуэла</option><option value="VG">Виргинские о-ва (Великобритания)</option><option value="VI">Виргинские о-ва (США)</option><option value="UM">Внешние малые о-ва (США)</option><option value="TL">Восточный Тимор</option><option value="VN">Вьетнам</option><option value="GA">Габон</option><option value="HT">Гаити</option><option value="GY">Гайана</option><option value="GM">Гамбия</option><option value="GH">Гана</option><option value="GP">Гваделупа</option><option value="GT">Гватемала</option><option value="GN">Гвинея</option><option value="GW">Гвинея-Бисау</option><option value="DE">Германия</option><option value="GG">Гернси</option><option value="GI">Гибралтар</option><option value="HN">Гондурас</option><option value="HK">Гонконг (САР)</option><option value="GD">Гренада</option><option value="GL">Гренландия</option><option value="GR">Греция</option><option value="GE">Грузия</option><option value="GU">Гуам</option><option value="DK">Дания</option><option value="JE">Джерси</option><option value="DJ">Джибути</option><option value="DG">Диего-Гарсия</option><option value="DM">Доминика</option><option value="DO">Доминиканская Республика</option><option value="EG">Египет</option><option value="ZM">Замбия</option><option value="EH">Западная Сахара</option><option value="ZW">Зимбабве</option><option value="IL">Израиль</option><option value="IN">Индия</option><option value="ID">Индонезия</option><option value="JO">Иордания</option><option value="IQ">Ирак</option><option value="IR">Иран</option><option value="IE">Ирландия</option><option value="IS">Исландия</option><option value="ES">Испания</option><option value="IT">Италия</option><option value="YE">Йемен</option><option value="CV">Кабо-Верде</option><option value="KZ" selected="selected">Казахстан</option><option value="KH">Камбоджа</option><option value="CM">Камерун</option><option value="CA">Канада</option><option value="IC">Канарские о-ва</option><option value="QA">Катар</option><option value="KE">Кения</option><option value="CY">Кипр</option><option value="KG">Киргизия</option><option value="KI">Кирибати</option><option value="CN">Китай</option><option value="KP">КНДР</option><option value="CC">Кокосовые о-ва</option><option value="CO">Колумбия</option><option value="KM">Коморы</option><option value="CG">Конго - Браззавиль</option><option value="CD">Конго - Киншаса</option><option value="XK">Косово</option><option value="CR">Коста-Рика</option><option value="CI">Кот-д&rsquo;Ивуар</option><option value="CU">Куба</option><option value="KW">Кувейт</option><option value="CW">Кюрасао</option><option value="LA">Лаос</option><option value="LV">Латвия</option><option value="LS">Лесото</option><option value="LR">Либерия</option><option value="LB">Ливан</option><option value="LY">Ливия</option><option value="LT">Литва</option><option value="LI">Лихтенштейн</option><option value="LU">Люксембург</option><option value="MU">Маврикий</option><option value="MR">Мавритания</option><option value="MG">Мадагаскар</option><option value="YT">Майотта</option><option value="MO">Макао (САР)</option><option value="MW">Малави</option><option value="MY">Малайзия</option><option value="ML">Мали</option><option value="MV">Мальдивы</option><option value="MT">Мальта</option><option value="MA">Марокко</option><option value="MQ">Мартиника</option><option value="MH">Маршалловы Острова</option><option value="MX">Мексика</option><option value="MZ">Мозамбик</option><option value="MD">Молдова</option><option value="MC">Монако</option><option value="MN">Монголия</option><option value="MS">Монтсеррат</option><option value="MM">Мьянма (Бирма)</option><option value="NA">Намибия</option><option value="NR">Науру</option><option value="NP">Непал</option><option value="NE">Нигер</option><option value="NG">Нигерия</option><option value="NL">Нидерланды</option><option value="NI">Никарагуа</option><option value="NU">Ниуэ</option><option value="NZ">Новая Зеландия</option><option value="NC">Новая Каледония</option><option value="NO">Норвегия</option><option value="AC">о-в Вознесения</option><option value="IM">о-в Мэн</option><option value="NF">о-в Норфолк</option><option value="CX">о-в Рождества</option><option value="SH">о-в Св. Елены</option><option value="PN">о-ва Питкэрн</option><option value="TC">о-ва Тёркс и Кайкос</option><option value="AE">ОАЭ</option><option value="OM">Оман</option><option value="KY">Острова Кайман</option><option value="CK">Острова Кука</option><option value="PK">Пакистан</option><option value="PW">Палау</option><option value="PS">Палестинские территории</option><option value="PA">Панама</option><option value="PG">Папуа &mdash; Новая Гвинея</option><option value="PY">Парагвай</option><option value="PE">Перу</option><option value="PL">Польша</option><option value="PT">Португалия</option><option value="XB">псевдо-Bidi</option><option value="XA">псевдоакценты</option><option value="PR">Пуэрто-Рико</option><option value="KR">Республика Корея</option><option value="RE">Реюньон</option><option value="RU">Россия</option><option value="RW">Руанда</option><option value="RO">Румыния</option><option value="SV">Сальвадор</option><option value="WS">Самоа</option><option value="SM">Сан-Марино</option><option value="ST">Сан-Томе и Принсипи</option><option value="SA">Саудовская Аравия</option><option value="MK">Северная Македония</option><option value="MP">Северные Марианские о-ва</option><option value="SC">Сейшельские Острова</option><option value="BL">Сен-Бартелеми</option><option value="MF">Сен-Мартен</option><option value="PM">Сен-Пьер и Микелон</option><option value="SN">Сенегал</option><option value="VC">Сент-Винсент и Гренадины</option><option value="KN">Сент-Китс и Невис</option><option value="LC">Сент-Люсия</option><option value="RS">Сербия</option><option value="EA">Сеута и Мелилья</option><option value="SG">Сингапур</option><option value="SX">Синт-Мартен</option><option value="SY">Сирия</option><option value="SK">Словакия</option><option value="SI">Словения</option><option value="US">Соединенные Штаты</option><option value="SB">Соломоновы Острова</option><option value="SO">Сомали</option><option value="SD">Судан</option><option value="SR">Суринам</option><option value="SL">Сьерра-Леоне</option><option value="TJ">Таджикистан</option><option value="TH">Таиланд</option><option value="TW">Тайвань</option><option value="TZ">Танзания</option><option value="TG">Того</option><option value="TK">Токелау</option><option value="TO">Тонга</option><option value="TT">Тринидад и Тобаго</option><option value="TA">Тристан-да-Кунья</option><option value="TV">Тувалу</option><option value="TN">Тунис</option><option value="TM">Туркменистан</option><option value="TR">Турция</option><option value="UG">Уганда</option><option value="UZ">Узбекистан</option><option value="UA">Украина</option><option value="WF">Уоллис и Футуна</option><option value="UY">Уругвай</option><option value="FO">Фарерские о-ва</option><option value="FM">Федеративные Штаты Микронезии</option><option value="FJ">Фиджи</option><option value="PH">Филиппины</option><option value="FI">Финляндия</option><option value="FK">Фолклендские о-ва</option><option value="FR">Франция</option><option value="GF">Французская Гвиана</option><option value="PF">Французская Полинезия</option><option value="TF">Французские Южные территории</option><option value="HR">Хорватия</option><option value="CF">Центрально-Африканская Республика</option><option value="TD">Чад</option><option value="ME">Черногория</option><option value="CZ">Чехия</option><option value="CL">Чили</option><option value="CH">Швейцария</option><option value="SE">Швеция</option><option value="SJ">Шпицберген и Ян-Майен</option><option value="LK">Шри-Ланка</option><option value="EC">Эквадор</option><option value="GQ">Экваториальная Гвинея</option><option value="ER">Эритрея</option><option value="SZ">Эсватини</option><option value="EE">Эстония</option><option value="ET">Эфиопия</option><option value="GS">Южная Георгия и Южные Сандвичевы о-ва</option><option value="ZA">Южно-Африканская Республика</option><option value="SS">Южный Судан</option><option value="JM">Ямайка</option><option value="JP">Япония</option></select></td>
									</tr>								

									<tr id="iin-form">
										<td>{{ Form::label('iin', 'ИИН/ИНН/№ паспорта') }}</td>
										<td>{{ Form::text('iin',  null, ['class'=>'form-control', 'required'=>'required']) }}</td>
									</tr>									
									<tr>
										<td>{{ Form::label('fullname', 'ФИО оплатившего') }}</td>
										<td>{{ Form::text('fullname',  null, ['class'=>'form-control', 'required'=>'required']) }}</td>
									</tr>									
									<tr>
										<td>{{ Form::label('date', 'Дата оплаты') }}</td>
										<td>{{ Form::text('date',  null, ['class'=>'form-control datepicker-here', 'data-position' => 'left top', 'required'=>'required']) }}</td>
									</tr>									
									<tr>
										<td>{{ Form::label('summ', 'Сумма') }}</td>
										<td>{{ Form::number('summ', null, ['class'=>'form-control', 'required'=>'required', 'min'=>'0']) }}</td>
									</tr>
									<tr>
										<td>{{ Form::label('skype', 'Skype') }}</td>
										<td>{{ Form::text('skype', null, ['class'=>'form-control']) }}</td>
									</tr>									
									<tr>
										<td>{{ Form::label('image', 'Фото') }}</td>
										<td>{{ Form::file('image[]', array('class' => 'form-control', 'required'=>'required', 'multiple'=>true, 'accept'=>".jpeg, .jpg, .png, .pdf") ) }}</td>
									</tr>
									<tr>
										<td>{{ Form::label('user_note', 'Примечание') }}</td>
										<td>{{ Form::textarea('user_note', null, ['class'=>'form-control']) }}</td>

									</tr>
									
									<tr>
										<td><button class="btn btn-sm btn-primary" type="submit">Отправить</button> </td>
									</tr>
								</tbody>
								{{ Form::close() }}
								</table>
							</div>
		@endif
						</div>
					</div>
				</div>
@endsection

@section('scripts')
<script type="text/javascript">


$(document).ready(function(){

	$('#form-click').on( 'click', function(e){
	e.preventDefault();
	$('.profile_table').toggleClass('active');
	})

	$(":input").inputmask();

	$('#country').on('change', function(){
		if( $(this).val() == 'KZ' ){
			$('#iin-form').html('<td><label for="iin">ИИН/ИНН/№ паспорта</label></td><td><input class="form-control" required="required" name="iin" type="text" id="iin"></td>');
			$('#iin-form').fadeIn();
		} else {
			$('#iin-form').fadeOut();
			$('#iin-form').html('');
		}
	});



	var isActive = true;

$().ready(function () {
    //EITHER USE A GLOBAL VAR OR PLACE VAR IN HIDDEN FIELD
    //IF FOR WHATEVER REASON YOU WANT TO STOP POLLING
    pollServer();
});

function pollServer()
{
    if (isActive)
    {
        window.setTimeout(function () {
            $.ajax({
                url: "/check/status",
                type: "POST",
                success: function (result) {
                    //SUCCESS LOGIC
                    pollServer();
                },
                error: function () {
                    //ERROR HANDLING
                    pollServer();
                }});
        }, 2500);
    }
}



});




</script>
@endsection