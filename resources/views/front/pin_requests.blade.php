@extends('front.layout.main')

@section('content')
<style type="text/css">
.timer div {
  display: inline-block;
  line-height: 1;
  padding: 20px;
  font-size: 40px;
}

.timer span {
  display: block;
  font-size: 20px;
  color: #000;
}

a.active{
	color: #ff0200;
	text-decoration: underline;
}

.timer #days {
  font-size: 100px;
  color: #db4844;
}
.timer #hours {
  font-size: 100px;
  color: #f07c22;
}
.timer #minutes {
  font-size: 100px;
  color: #f6da74;
}
.timer #seconds {
  font-size: 50px;
  color: #abcd58;
}

.tgbl{
	display: none;
}
.tgbl.active{
	display: block;
}
</style>
<div class="cabinet_main_container">
	@include('front.layout.menu')
		<div class="body">
			<strong> </strong>
		<div class="profile_body_text">
		
		@if(Session::has('message'))
                <div class="card-header" style="margin-bottom: 25px;">
            		<p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
                </div>
        @endif

        <a href="{{ route('partner.pin') }}" >Мои пин коды</a>
        <a href="{{ route('partner.pin.requests') }}" class="active" style="margin-left: 40px;">Заявки</a>        
        <a href="{{ route('partner.pin.create') }}" style="margin-left: 40px;">Создать заявку</a>

        <div class="row" style="margin: 50px 0 0px 0;">
        	@if(count($requests) > 0)
		        @foreach($requests as $key => $request)
			        <div class="row" style="margin-bottom: 25px;">	        	
			        	<div class="col-md-1">{{$key + 1}}</div>
				        <div class="col-md-11">
							Ваша заявка под номером <span style="color: teal"> {{ $request->id }}</span> от <span><small style="color: #ff7400;">{{ $request->created_at }}</small></span> 
							  @if($request->status_id == 0)
								<span class="pin-info"> находится в режиме ожидания</span>
							@elseif($request->status_id == 1)
								<span class="pin-info" style="color: teal">одобрена</span>					
							@elseif($request->status_id == 2)
								<span class="pin-info" style="color: red">отклонена</span> <br/><br>
								<pre>{{ $request->cancel_reason }}</pre>
							@endif
						</div>
			        </div>
		        @endforeach
	        @else
	        <p>У вас нет заявок. <a href="{{ route('partner.pin.create') }}">Создать заявку</a></p>
	        @endif
        </div>


@endsection

@section('scripts')
<script type="text/javascript">

@if(count($requests) > 0)

$(document).ready(function(){

 isActive = true;

$().ready(function () {
    //EITHER USE A GLOBAL VAR OR PLACE VAR IN HIDDEN FIELD
    //IF FOR WHATEVER REASON YOU WANT TO STOP POLLING
    pollServer();
});

function pollServer()
{
    if (isActive)
    {
        window.setTimeout(function () {
            $.ajax({
                url: "/check/status",
                data: {id: {{ $requests->first()->id }}, program: {{ $p }}, status_id: {{ $requests->first()->status_id }}, _token: "{{ csrf_token() }}" },
                type: "POST",
                success: function (result) {
                    if(result.change){
                    	var text;
                    	if(result.status == 2){
                    		text = 'отклонена!';
                    	} else {
                    		text = 'одобрена!';                    		
                    	}
                    	alert('Ваша заявка ' + text);
                      window.location.replace("/partner/pin");
                    }
                    pollServer();
                },
                error: function () {
                    //ERROR HANDLING
                    pollServer();
                }});
        }, 4500);
    }
}


$( ".modal" ).on('shown', function(){
    alert("I want this to appear after the modal has opened!");
});


});

@endif


</script>
@endsection