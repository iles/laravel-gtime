@extends('front.layout.main')

@section('content')

<style type="text/css">
  .transfer-form{
    display: none;
  }  

  .transfer-form.active{
    display: block;
  }
</style>

@php
 
 if(isset($finished->pin)){
                $p = json_decode($finished->pin, true);
                $pin = \App\Models\Pins\StartPin::where('id', $p['pin'])->first();
            }   else { $pin = null; }  
 
@endphp

<div class="cabinet_main_container">
		@include('front.layout.menu')

	<div class="body">
		<div class="auth_cont" style="max-width: 100%;">



<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingOne">
      <h4 class="panel-title">
        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
          Отправить заявку на получение вознаграждения
        </a>
      </h4>
    </div>
    <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
      <div class="panel-body">

        @if(isset($bonus))
          <div class="row" style="padding: 40px 0 0 10px;">
            @if($bonus->status_id == 0)
            Ваша заявка находится на рассмотрении
            @elseif($bonus->status_id == 1)
            Ваша заявка одобрена <br/>
            <br/>

            @elseif($bonus->status_id == 2)
            Ваша заявка отклонена <br/>
            <br/>
            <pre>
              {{ $bonus->cancel_reason }}
            </pre>
            <form action="/rebonus" method="POST">
              @csrf
              <input type="text" hidden  value="{{ $bonus->id }}" name="id">
              <button type="submit" href="#" class="btn btn-primary">Создать новую заявку</button>
            </form>
            @endif

          </div>
        @else


        <p style="margin: 25px 0 50px 0; font-weight: bold; font-size: 16px; color: #3e3d3c;">Поздравляем! <br/>
          Вы завершили программу «Основная старт» для получения вознаграждения заполните форму ниже</p>
          @include('front.finish.main.form_start')
          <br>
          <br>



      @endif
      <br><br>
            <h4 class="panel-title">
            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
              Генерация Пин-кода в Вип программу
            </a>
          </h4>
          
          <div class="panel-body">





@if(!$pin)



          @if(isset($transfer))
          <br>
          <br>
          <br>
            @if($transfer->status_id == 0)
              Ваша заявка находится на рассмотрении
            @elseif($transfer->status_id == 2)
              Ваша заявка отклонена
            @else($transfer->status_id == 1)
              Ваша заявка одобрена
              <br/>


            {{ Form::open(['route' => 'pin.add', 'method'=>'post']) }}
            @csrf
              <input type="text" name="program_id" value="6" hidden="hidden">
              <input type="text" name="program_type" value="3" hidden="hidden">
              <button class="btn btn-lg btn-success" type="submit" onclick="return confirm('Выу верены?')">
                <i class="fa fa-dot-circle-o"></i> Сгенерировать
              </button>  
            {{ Form::close() }}
            @endif
          @else



            {{ Form::open(['route' => 'pin.add', 'method'=>'post']) }}
            @csrf
              <input type="text" name="program_id" value="6" hidden="hidden">
              <input type="text" name="program_type" value="3" hidden="hidden">
              <button class="btn btn-lg btn-success" type="submit" onclick="return confirm('Выу верены?')">
                <i class="fa fa-dot-circle-o"></i> Сгенерировать
              </button>  
            {{ Form::close() }}          @endif   


          @if(isset($transfer))
          
          @else 



         <a href="#" style="margin: 65px 0 25px 0; display: inline-block; font-size: 16px; text-decoration: underline;" class="transfer-button">Временная передача пин-кодов </a>

      <div class="card transfer-form" id="">

      {{ Form::open(['route' => 'transfer.add', 'method'=>'post', 'files' => true, 'autocomplete' => 'off']) }}

      <div class="card-body">
        <div class="form-group row">            
          {{ Form::label('login', 'Логин:', array('class' => 'col-md-3 col-form-label')) }}
          <div class="col-md-9">
            {{ Form::text('login', Auth::user()->name,  array('class' => 'form-control', 'required'=>'required', 'readonly' => 'readonly') ) }}
          </div>
        </div>        
        <div class="form-group row">            
          {{ Form::label('firstname', 'ФИО:', array('class' => 'col-md-3 col-form-label')) }}
          <div class="col-md-9">
            {{ Form::text('fullname', Auth::user()->familiya,  array('class' => 'form-control', 'required'=>'required') ) }}
          </div>
        </div>           
        <div class="form-group row">            
          {{ Form::label('firstname', 'ФИО получателя:', array('class' => 'col-md-3 col-form-label')) }}
          <div class="col-md-9">
            {{ Form::text('fullname2', null,  array('class' => 'form-control', 'required'=>'required') ) }}
          </div>
        </div>
        <div class="form-group row">
          {{ Form::label('image', 'Скан документа :', array('class' => 'col-md-3 col-form-label')) }}
          <div class="col-md-9">
           {{ Form::file('image[]', array('class' => 'form-control', 'required'=>'required', 'multiple'=>true, 'accept'=>".jpeg, .jpg, .png, .pdf") ) }}
          </div>
        </div>
         <div class="form-group row" style="display: none;">
        @if(isset($finished))
            <input type="text" hidden value="{{ $finished->id }}" name="finished_id">
		@endif
            <input type="text" hidden value="3" name="program_id">
            {{ Form::text('uid', Auth::user()->user_id,  array('class' => 'form-control', 'required'=>'required') ) }}
        </div>
      </div>
        
      <div class="card-footer">
        <button class="btn btn-sm btn-primary" type="submit">
          <i class="fa fa-dot-circle-o"></i> Отправить
        </button>     
      </div>



      {{ Form::close() }}

      @endif










@else


      @if(!$pin)
            {{ Form::open(['route' => 'pin.add', 'method'=>'post']) }}
            @csrf
              <input type="text" name="program_id" value="6" hidden="hidden">
              <input type="text" name="program_type" value="3" hidden="hidden">
              <button class="btn btn-lg btn-success" type="submit" onclick="return confirm('Выу верены?')">
                <i class="fa fa-dot-circle-o"></i> Сгенерировать
              </button>  
            {{ Form::close() }}
      @else
                  <div class="row">
                    <div class="col-md-4"><b>{{ $pin->pin }}</b></div>
                    <div class="col-md-6">
                      @if( $pin->status_id == 0 )
                      <pre>Действителен до: <br/>{{ $pin->expired_at }}</pre>
                      @else
                      <pre style="color: red;">Заблокирован</pre>
                      @endif
                    </div>
                  </div>
       @endif
      </div>

@endif




      </div>
      </div>
      </div>
























     

    </div>
  </div>

  </div>
</div>






    </div>
    </div>

@endsection

@section('scripts')

<script type="text/javascript">
  $('.transfer-button').on('click', function(e){
    e.preventDefault();
    $('.transfer-form').toggleClass('active');
  })
</script>

@endsection