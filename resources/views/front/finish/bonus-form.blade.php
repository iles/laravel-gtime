<div class="card">

      {{ Form::open(['route' => 'bonus.bonusadd', 'method'=>'post', 'autocomplete' => 'off', 'files' => true]) }}

    <div class="card-body">
        @csrf

      

        <div class="form-group row">
          {{ Form::label('uname', 'Логин:', array('class' => 'col-md-3 col-form-label')) }}
          <div class="col-md-9">
            {{ Form::text('uname', Auth::user()->name,  array('class' => 'form-control', 'required'=>'required', 'readonly' => 'readonly') ) }}
          </div>
        </div>

        <div class="form-group row hidden">            
          {{ Form::label('summ', 'Сумма:', array('class' => 'col-md-3 col-form-label')) }}
          <div class="col-md-9">
            {{ Form::number('summ', $reward ,  array('class' => 'form-control', 'required'=>'required',  'readonly' => 'readonly') ) }}
            {{ Form::number('invite_id', $fid,  array('class' => 'form-control', 'required'=>'required',  'readonly' => 'readonly') ) }}
          </div>
        </div>

        <div class="form-group row">            
          {{ Form::label('firstname', 'Имя:', array('class' => 'col-md-3 col-form-label')) }}
          <div class="col-md-9">
            {{ Form::text('firstname', Auth::user()->firstname,  array('class' => 'form-control', 'required'=>'required') ) }}
          </div>
        </div>        

        <div class="form-group row">            
          {{ Form::label('firstname', 'Фамилия:', array('class' => 'col-md-3 col-form-label')) }}
          <div class="col-md-9">
            {{ Form::text('lastname', Auth::user()->lastname,  array('class' => 'form-control', 'required'=>'required') ) }}
          </div>
        </div>       

         <div class="form-group row">            
          {{ Form::label('patronic', 'Отчество:', array('class' => 'col-md-3 col-form-label')) }}
          <div class="col-md-9">
            {{ Form::text('patronic', Auth::user()->patronic,  array('class' => 'form-control', 'required'=>'required') ) }}
          </div>
        </div>         

        <div class="form-group row">            
          {{ Form::label('iin', 'ИИН:', array('class' => 'col-md-3 col-form-label')) }}
          <div class="col-md-9">
            {{ Form::number('iin', Input::old('iin'),  array('class' => 'form-control', 'required'=>'required') ) }}
          </div>
        </div>        

        <div class="form-group row">            
          {{ Form::label('bill_num', 'Номер счета :', array('class' => 'col-md-3 col-form-label')) }}
          <div class="col-md-9">
            {{ Form::text('bill_num', Input::old('bill_num'),  array('class' => 'form-control', 'required'=>'required') ) }}
          </div>
        </div>     

        <div class="form-group row">            
          {{ Form::label('bank_name', 'Наименование банка :', array('class' => 'col-md-3 col-form-label')) }}
          <div class="col-md-9">
            {{ Form::text('bank_name', Input::old('bank_name'),  array('class' => 'form-control', 'required'=>'required') ) }}
          </div>
        </div>        

        <div class="form-group row">            
          {{ Form::label('BIK', 'BIK :', array('class' => 'col-md-3 col-form-label')) }}
          <div class="col-md-9">
            {{ Form::text('BIK', Input::old('BIK'),  array('class' => 'form-control', 'required'=>'required') ) }}
          </div>
        </div>


        @if( Auth::user()->nonResident() )
        <div class="form-group row">            
          {{ Form::label('card_number', 'Номер карточки :', array('class' => 'col-md-3 col-form-label')) }}
          <div class="col-md-9">
            {{ Form::number('card_number', Input::old('card_number'),  array('class' => 'form-control', 'required'=>'required') ) }}
          </div>
        </div>        

        <div class="form-group row">            
          {{ Form::label('bank_account', 'Счет банка (Корреспондентский) :', array('class' => 'col-md-3 col-form-label')) }}
          <div class="col-md-9">
            {{ Form::text('bank_account', Input::old('bank_account'),  array('class' => 'form-control', 'required'=>'required') ) }}
          </div>
        </div>        

        <div class="form-group row">            
          {{ Form::label('country', 'Страна / Город:', array('class' => 'col-md-3 col-form-label')) }}
          <div class="col-md-9">
            {{ Form::text('country', Input::old('country'),  array('class' => 'form-control', 'required'=>'required') ) }}
          </div>
        </div>    

        <div class="form-group row">            
          {{ Form::label('adress', 'Адрес проживания:', array('class' => 'col-md-3 col-form-label')) }}
          <div class="col-md-9">
            {{ Form::text('adress', Input::old('adress'),  array('class' => 'form-control', 'required'=>'required') ) }}
          </div>
        </div>        

        <div class="form-group row">            
          {{ Form::label('fact_adress', 'Фактический адрес проживания:', array('class' => 'col-md-3 col-form-label')) }}
          <div class="col-md-9">
            {{ Form::text('fact_adress', Input::old('fact_adress'),  array('class' => 'form-control', 'required'=>'required') ) }}
          </div>
        </div>
        @endif 

        @if( !Auth::user()->nonResident() )

        <div class="form-group row" style="margin-top: 35px;">            
          <div class="col-md-3"></div>
          <div class="col-md-9" style="text-align: left;">
            {{ Form::label('pens', 'Пенсионер', array('class' => 'col-form-label')) }}
            {{ Form::checkbox('pens', '1') }}
            {{ Form::label('inv', 'Инвалидность', array('style'=>'margin-left: 30px;', 'class' => 'col-form-label')) }}
            {{ Form::checkbox('inv', '1') }}
          </div>
        </div>

        <div class="form-group row">  
          <div class="col-md-3"></div>          
          <div class="col-md-9" style="text-align: left;">
        
        <div class="row inv-row" style="margin: 20px 0;">
          <div class="col-md-5">
            {{ Form::label('inv_group', 'Группа инвалидности :', array('class' => 'col-form-label')) }} <br/> <br/>
            {{ Form::select('inv_group', array('1' => 'Первая группа', '2' => 'Вторая группа', '3' => 'Третья группа'), null, array('class' => 'form-control') ) }}              
          </div>
          
          <div class="col-md-7">
            <div class="row">
                <div class="col-md-12">
                  {{ Form::label('image', 'Срок инвалидности:', array('class' => 'col-form-label')) }}    <br/> <br/>               
                </div>
                <div class="col-md-6 inv-time">
                  {{ Form::text('inv_start',  null, ['class'=>'form-control datepicker-here', 'placeholder'=>'С:', 'data-position' => 'left top', 'readonly'=>'readonly', 'required'=>'required']) }}                  
                </div>                
                <div class="col-md-6 inv-time">
                  {{ Form::text('inv_end',  null, ['class'=>'form-control datepicker-here', 'placeholder'=>'ПО:', 'data-position' => 'left top', 'readonly'=>'readonly', 'required'=>'required']) }}                  
                </div>
                <div class="col-md-12" style="margin-top: 15px;">
                  {{ Form::label('inv_timeless', 'Без срока переосвидетельствования', array('style'=>'margin-right: 10px;', 'class' => 'col-form-label')) }}
                  {{ Form::checkbox('inv_timeless', '1') }}                  
                </div>
              </div>
            </div>
        </div>

          </div>
        </div>

        @endif

        <div class="form-group row">            
          {{ Form::label('notorios', 'По нотариальной доверенности', array('class' => 'col-md-3 col-form-label')) }}
          <div class="col-md-9" style="text-align: left;">
            {{ Form::checkbox('notorios', '1') }}  
          </div>
        </div> 

                <div class="form-group row not-row">  
          <div class="col-md-3"></div>          
          <div class="col-md-9" style="text-align: left;">
            <b>Укажите данные:</b>
            <br/>
            <br/>
            {{ Form::text('not_fio',  null, ['class'=>'form-control', 'placeholder'=>'ФИО:']) }}<br/>
            {{ Form::text('not_adress',  null, ['class'=>'form-control', 'placeholder'=>'Адрес:']) }}<br/>
            {{ Form::text('not_iin',  null, ['class'=>'form-control', 'placeholder'=>'ИИН:']) }}<br/>
            {{ Form::text('not_passport',  null, ['class'=>'form-control', 'placeholder'=>'№ пасспорта:']) }}<br/>

          </div>
        </div>

         
        <br/>
        <div class="form-group row">         
          {{ Form::label('image', 'Скан документа :', array('class' => 'col-md-3 col-form-label')) }}
          <div class="col-md-9">
          {{ Form::file('images[]', array('class' => 'form-control', 'required'=>'required', 'multiple'=>true, 'accept'=>".jpeg, .jpg, .png, .pdf") ) }}
          </div>
        </div>          

         <div class="form-group row" style="display: none;">            
            {{ Form::text('uid', Auth::user()->user_id,  array('class' => 'form-control', 'required'=>'required') ) }}
        </div>        

     
     

        
    </div>
        
    <div class="card-footer">
      <button class="btn btn-sm btn-primary" type="submit">
        <i class="fa fa-dot-circle-o"></i> Сохранить
      </button>     

   </div>
   {{ Form::close() }}

</div>


