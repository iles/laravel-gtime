@extends('front.layout.main')

@section('content')

<div class="cabinet_main_container authorization">
  <div class="header">
    <div class="row">
      <div class="col-md-10">
        <h2 class="title">Завершение программы</h2>        
      </div>
      <div class="col-md-2">
        <a class="link" href="#" onclick="document.getElementById('logout-form').submit(); return false;">
          <i class="icon-logout"></i>выход
        </a>
        <form id="logout-form" action="{{ route('partner.logout') }}" method="POST" style="display: none;">
          @csrf
        </form>
      </div>
    </div>

  </div>
	<div class="body">
		<div class="auth_cont" style="max-width: 100%;">   

                @if($bonus)
                    <div class="row" style="padding: 40px 0 0 10px;">
                      @if($bonus->status_id == 0)
                      Ваша заявка находится на рассмотрении
                      @elseif($bonus->status_id == 1)
                        Ваша заявка одобрена и находится на рассмотрении в бухгалтерии<br/>      
                      @elseif($bonus->status_id == 4)
                        Ваша заявка одобрена       
                      @elseif($bonus->status_id == 5)
                        Ваша заявка отклонена
                        <br/>
                        <form action="/partner/rebonus" method="POST">
                          @csrf
                          <input type="text" hidden  value="{{ $bonus->id }}" name="id">
                          <button type="submit" href="#" class="btn btn-primary">Создать новую заявку</button>
                        </form>  
                  
                      @elseif($bonus->status_id == 2)
                      Ваша заявка отклонена <br/>
                      <br/>
                      <pre>
                        {{ $bonus->cancel_reason }}
                      </pre>
                      <form action="/partner/rebonus" method="POST">
                        @csrf
                        <input type="text" hidden  value="{{ $bonus->id }}" name="id">
                        <button type="submit" href="#" class="btn btn-primary">Создать новую заявку</button>
                      </form>
                      @endif
                    </div>
                  @else
                    <p style="margin: 25px 0 50px 0; font-weight: bold; font-size: 16px; color: #3e3d3c;">
                    -Вознаграждение в денежном выражении 80 000тенге*.
                    + и варианты выбора продукции, входящих в пакет «Накопительная»</p>
                    @include('front.finish.bns.formC')
                  @endif


  <p style="margin: 65px 0 25px 0; font-weight: bold; font-size: 16px; color: #3e3d3c;">Пинкод для регистрации в Накопительную программу (с номинальной стоимостью 15 000тг)</p>
            <p style="margin: 25px 0 25px 0; font-weight: bold; font-size: 16px; color: #3e3d3c;">2 (два) пинкода для регистрации в FAST программу (с общей номинальной стоимостью 120 000тг), и варианты выбора продукции, входящих в пакет «FAST»</p>
@if(!$pin)



          @if($transfer)
          <br>
          <br>
          <br>
            @if($transfer->status_id == 0)
              Ваша заявка находится на рассмотрении
            @elseif($transfer->status_id == 2)
              Ваша заявка отклонена
            @else($transfer->status_id == 1)
              Ваша заявка одобрена
              <br/>

          <p style="margin: 65px 0 25px 0; font-weight: bold; font-size: 16px; color: #3e3d3c;">Пинкод для регистрации в Накопительную программу (с номинальной стоимостью 15 000тг), </p>
          <p style="margin: 25px 0 25px 0; font-weight: bold; font-size: 16px; color: #3e3d3c;">2 (два) пинкода для регистрации в FAST программу (с общей номинальной стоимостью 120 000тг), и варианты выбора продукции, входящих в пакет «FAST»</p>

          {{ Form::open(['route' => 'pin.add', 'method'=>'post']) }}
            @csrf
            <input type="text" name="triple" value="g" hidden="hidden">
            <input type="text" name="program_id" value="5" hidden="hidden">

            <button class="btn btn-lg btn-success" type="submit" onclick="return confirm('Вы уверены?')">
              <i class="fa fa-dot-circle-o"></i> Сгенерировать
            </button>  
          {{ Form::close() }}
            @endif
          @else

          <p style="margin: 65px 0 25px 0; font-weight: bold; font-size: 16px; color: #3e3d3c;">Пинкод для регистрации в Накопительную программу (с номинальной стоимостью 15 000тг), </p>
          <p style="margin: 25px 0 25px 0; font-weight: bold; font-size: 16px; color: #3e3d3c;">2 (два) пинкода для регистрации в FAST программу (с общей номинальной стоимостью 120 000тг), и варианты выбора продукции, входящих в пакет «FAST»</p>

          {{ Form::open(['route' => 'pin.add', 'method'=>'post']) }}
            @csrf
            <input type="text" name="triple" value="g" hidden="hidden">
            <input type="text" name="program_id" value="5" hidden="hidden">

            <button class="btn btn-lg btn-success" type="submit" onclick="return confirm('Вы уверены?')">
              <i class="fa fa-dot-circle-o"></i> Сгенерировать
            </button>  
          {{ Form::close() }}
          @endif   


          @if($transfer)
          
          @else 

          <a href="#" style="margin: 65px 0 25px 0; display: inline-block; font-size: 16px; text-decoration: underline;" id="transfer-button">Временная передача пин-кодов </a>

      <div class="card" id="transfer-form">

      {{ Form::open(['route' => 'transfer.add', 'method'=>'post', 'files' => true, 'autocomplete' => 'off']) }}

      <div class="card-body">
        <div class="form-group row">            
          {{ Form::label('login', 'Логин:', array('class' => 'col-md-3 col-form-label')) }}
          <div class="col-md-9">
            {{ Form::text('login', Auth::user()->name,  array('class' => 'form-control', 'required'=>'required', 'readonly' => 'readonly') ) }}
          </div>
        </div>        
        <div class="form-group row">            
          {{ Form::label('firstname', 'ФИО:', array('class' => 'col-md-3 col-form-label')) }}
          <div class="col-md-9">
            {{ Form::text('fullname', Auth::user()->familiya,  array('class' => 'form-control', 'required'=>'required') ) }}
          </div>
        </div>           
        <div class="form-group row">            
          {{ Form::label('firstname', 'ФИО получателя:', array('class' => 'col-md-3 col-form-label')) }}
          <div class="col-md-9">
            {{ Form::text('fullname2', null,  array('class' => 'form-control', 'required'=>'required') ) }}
          </div>
        </div>
        <div class="form-group row">
          {{ Form::label('image', 'Скан документа :', array('class' => 'col-md-3 col-form-label')) }}
          <div class="col-md-9">
           {{ Form::file('image[]', array('class' => 'form-control', 'required'=>'required', 'multiple'=>true, 'accept'=>".jpeg, .jpg, .png, .pdf") ) }}
          </div>
        </div>
         <div class="form-group row" style="display: none;">
            <input type="text" hidden value="{{ $finished->id }}" name="finished_id">
            <input type="text" hidden value="5" name="program_id">
            <input type="text" hidden value="3" name="variant">
            {{ Form::text('uid', Auth::user()->user_id,  array('class' => 'form-control', 'required'=>'required') ) }}
        </div>
      </div>
        
      <div class="card-footer">
        <button class="btn btn-sm btn-primary" type="submit">
          <i class="fa fa-dot-circle-o"></i> Отправить
        </button>     
      </div>


      {{ Form::close() }}

      @endif










@else

                 <div class="row">
                   <div class="col-md-4"><b>{{ $pin->pin }}</b></div>
                    <div class="col-md-6">
                      @if( $pin->status_id == 0 )
                      <pre>Действителен до: <br/>{{ $pin->expired_at }}</pre>
                      @else
                      <pre style="color: red;">Заблокирован</pre>
                      @endif
                    </div>
                  </div>
 @endif


  
  @if($pin2)
                   <div class="row">
                   <div class="col-md-4"><b>{{ $pin2->pin }}</b></div>
                    <div class="col-md-6">
                      @if( $pin2->status_id == 0 )
                      <pre>Действителен до: <br/>{{ $pin2->expired_at }}</pre>
                      @else
                      <pre style="color: red;">Заблокирован</pre>
                      @endif
                    </div>
                  </div>
  @endif  

  @if($pin3)
                   <div class="row">
                   <div class="col-md-4"><b>{{ $pin3->pin }}</b></div>
                    <div class="col-md-6">
                      @if( $pin3->status_id == 0 )
                      <pre>Действителен до: <br/>{{ $pin3->expired_at }}</pre>
                      @else
                      <pre style="color: red;">Заблокирован</pre>
                      @endif
                    </div>
                  </div>
  @endif
  




		</div>
	</div>

</div>
@endsection

@section('scripts')
<script type="text/javascript">


$(document).ready(function(){
  $('#form-click').on( 'click', function(e){
  e.preventDefault();
  $('.profile_table').toggleClass('active');
  })

  $(":input").inputmask();


});




</script>
@endsection
