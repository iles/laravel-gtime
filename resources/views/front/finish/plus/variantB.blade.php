@extends('front.layout.main')

@section('content')



<div class="cabinet_main_container authorization">
  <div class="header">
    <div class="row">
      <div class="col-md-10">
        <h2 class="title">Завершение программы</h2>        
      </div>
      <div class="col-md-2">
        <a class="link" href="#" onclick="document.getElementById('logout-form').submit(); return false;">
          <i class="icon-logout"></i>выход
        </a>
        <form id="logout-form" action="{{ route('partner.logout') }}" method="POST" style="display: none;">
          @csrf
        </form>
      </div>
    </div>

  </div>
	<div class="body">
		<div class="auth_cont" style="max-width: 100%;">   

                @if($bonus)
                    <div class="row" style="padding: 40px 0 0 10px;">
                      @if($bonus->status_id == 0)
                      Ваша заявка находится на рассмотрении
                      @elseif($bonus->status_id == 1)
                        Ваша заявка одобрена и находится на рассмотрении в бухгалтерии<br/>      
                      @elseif($bonus->status_id == 4)
                        Ваша заявка одобрена       
                      @elseif($bonus->status_id == 5)
                        Ваша заявка отклонена
                        <br/>
                        <form action="/partner/rebonus" method="POST">
                          @csrf
                          <input type="text" hidden  value="{{ $bonus->id }}" name="id">
                          <button type="submit" href="#" class="btn btn-primary">Создать новую заявку</button>
                        </form>  
                  
                      @elseif($bonus->status_id == 2)
                      Ваша заявка отклонена <br/>
                      <br/>
                      <pre>
                        {{ $bonus->cancel_reason }}
                      </pre>
                      <form action="/partner/rebonus" method="POST">
                        @csrf
                        <input type="text" hidden  value="{{ $bonus->id }}" name="id">
                        <button type="submit" href="#" class="btn btn-primary">Создать новую заявку</button>
                      </form>
                      @endif
                    </div>
                  @else
                  <p style="margin: 25px 0 50px 0; font-weight: bold; font-size: 16px; color: #3e3d3c;">
                  -Вознаграждение в денежном выражении 80 000тенге*.
                  + и варианты выбора продукции, входящих в пакет «Накопительная»</p>>
                  @include('front.finish.plus.formB')
                  @endif



  <p style="margin: 65px 0 25px 0; font-weight: bold; font-size: 16px; color: #3e3d3c;">Пинкод для регистрации в Накопительную программу (с номинальной стоимостью 15 000тг)</p>
  @if(!$pin)
          {{ Form::open(['route' => 'pin.add', 'method'=>'post']) }}
            @csrf
            <input type="text" name="program_id" value="4" hidden="hidden">
            <input type="text" name="program_type" value="3" hidden="hidden">
            <input type="text" name="variant" value="2" hidden="hidden">
            <button class="btn btn-lg btn-success" type="submit" onclick="return confirm('Вы уверены?')">
              <i class="fa fa-dot-circle-o"></i> Сгенерировать
            </button>  
          {{ Form::close() }}
 @else

                  <div class="row">
                    <div class="col-md-4"><b>{{ $pin->pin }}</b></div>
                    <div class="col-md-6">
                      @if( $pin->status_id == 0 )
                      <pre>Действителен до: <br/>{{ $pin->expired_at }}</pre>
                      @else
                      <pre style="color: red;">Заблокирован</pre>
                      @endif
                    </div>
                  </div>

 @endif





		</div>
	</div>

</div>
@endsection

@section('scripts')
<script type="text/javascript">


$(document).ready(function(){
  $('#form-click').on( 'click', function(e){
  e.preventDefault();
  $('.profile_table').toggleClass('active');
  })

  $(":input").inputmask();

  @if( isset($pin) )

  function makeTimer() {

  //    var endTime = new Date("29 April 2018 9:56:00 GMT+01:00");  
    var endTime = new Date("{{ $pin->expired_at }}");     
      endTime = (Date.parse(endTime) / 1000);

      var now = new Date();
      now = (Date.parse(now) / 1000);

      var timeLeft = endTime - now;

      var days = Math.floor(timeLeft / 86400); 
      var hours = Math.floor((timeLeft - (days * 86400)) / 3600);
      var minutes = Math.floor((timeLeft - (days * 86400) - (hours * 3600 )) / 60);
      var seconds = Math.floor((timeLeft - (days * 86400) - (hours * 3600) - (minutes * 60)));
  
      if (hours < "10") { hours = "0" + hours; }
      if (minutes < "10") { minutes = "0" + minutes; }
      if (seconds < "10") { seconds = "0" + seconds; }

      $("#days").html(days + "<span>Дня</span>");
      $("#hours").html(hours + "<span>Часов</span>");
      $("#minutes").html(minutes + "<span>Минут</span>");
      $("#seconds").html(seconds + "<span>Секунд</span>");    

  }

  setInterval(function() { makeTimer(); }, 1000);

  @endif;

});




</script>
@endsection