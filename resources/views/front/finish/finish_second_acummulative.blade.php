@extends('front.layout.main')

@section('content')


<style type="text/css">
.timer div {
  display: inline-block;
  line-height: 1;
  padding: 20px;
  font-size: 40px;
}

.timer span {
  display: block;
  font-size: 20px;
  color: #000;
}

.timer #days {
  font-size: 100px;
  color: #db4844;
}
.timer #hours {
  font-size: 100px;
  color: #f07c22;
}
.timer #minutes {
  font-size: 100px;
  color: #f6da74;
}
.timer #seconds {
  font-size: 50px;
  color: #abcd58;
}

.tgbl{
  display: none;
}
.tgbl.active{
  display: block;
}
</style>

<div class="cabinet_main_container authorization">
  <div class="header">
    <div class="row">
      <div class="col-md-10">
        <h2 class="title">Завершение программы</h2>        
      </div>
      <div class="col-md-2">
        <a class="link" href="#" onclick="document.getElementById('logout-form').submit(); return false;">
          <i class="icon-logout"></i>выход
        </a>
        <form id="logout-form" action="{{ route('partner.logout') }}" method="POST" style="display: none;">
          @csrf
        </form>
      </div>
    </div>

  </div>
	<div class="body">
		<div class="auth_cont" style="max-width: 100%;">   
	
	@if($bonus->status_id == 0)
		<p class="pin-info"> Ваша заявка на вознаграждение под номером <span> {{ $bonus->id }}</span> от <span>{{ $bonus->created_at }}</span> находится в режиме ожидания</p>
	@elseif($bonus->status_id == 1)
		<p class="pin-info"> Ваша заявка на вознаграждение под номером <span> {{ $bonus->id }}</span> от <span>{{ $bonus->created_at }}</span> одобрена</p>      
	@elseif($bonus->status_id == 2)
		<p class="pin-info"> Ваша заявка на вознаграждение под номером <span> {{ $bonus->id }}</span> от <span>{{ $bonus->created_at }}</span> отклонена</p>
		
		\
		<p>
			<pre>{{ $bonus->cancel_reason }}</pre>
		</p>

		<form method="post" action="{{ action('FrontController@rebonus') }}" id="update_bonus_form" class="form-horizontal" accept-charset="UTF-8">
			@csrf
			<input hidden="hidden" name="bonus_id" value="{{ $bonus->id }}">
			<p style="margin-top: 35px;"><button  type="submit" class="btn dtn-success"> Отравить повторно </button></p>	
		<form>
	@endif




    <div style="margin-top: 90px">
	@if($bonus->secondPin)
    @php  
     $pin  = \App\Models\Pins\Acummulative_plusPin::where('id', $bonus->secondPin )->first();
    @endphp

    @if($pin)
        <p class="pin-info"> Ваш пин-код</p>
        <p style="font-weight: bold; font-size: 28px;">
          {{ $pin->pin  }}
        </p>
        <p class="pin-info"> Действителен до:</p>
        <p><pre>{{ $pin->expired_at }}</pre></p>
      
        <div class="timer">
          <div id="days"></div>
          <div id="hours"></div>
          <div id="minutes"></div>
          <div id="seconds"></div>
        </div>
    @else
    <h4>Пин код в програму "Основная"</h4>    	
        {{ Form::open(['route' => 'pin.add', 'method'=>'post']) }}
        @csrf
          <input type="text" name="program_id" value="3" hidden="hidden">
          <input type="text" name="bonus_id" value="{{ $bonus->id }}" hidden="hidden">
          <input type="text" name="bonus_program_id" value="5" hidden="hidden">
          
          <button class="btn btn-lg btn-success" type="submit" onclick="return confirm('Выу верены?')">
            <i class="fa fa-dot-circle-o"></i> Сгенерировать
          </button>  
        {{ Form::close() }}
    </div>

    @endif
  @endif


		</div>
	</div>

</div>
@endsection

@section('scripts')
<script type="text/javascript">


$(document).ready(function(){
  $('#form-click').on( 'click', function(e){
  e.preventDefault();
  $('.profile_table').toggleClass('active');
  })

  $(":input").inputmask();

  @if( isset($pin) )

  function makeTimer() {

  //    var endTime = new Date("29 April 2018 9:56:00 GMT+01:00");  
    var endTime = new Date("{{ $pin->expired_at }}");     
      endTime = (Date.parse(endTime) / 1000);

      var now = new Date();
      now = (Date.parse(now) / 1000);

      var timeLeft = endTime - now;

      var days = Math.floor(timeLeft / 86400); 
      var hours = Math.floor((timeLeft - (days * 86400)) / 3600);
      var minutes = Math.floor((timeLeft - (days * 86400) - (hours * 3600 )) / 60);
      var seconds = Math.floor((timeLeft - (days * 86400) - (hours * 3600) - (minutes * 60)));
  
      if (hours < "10") { hours = "0" + hours; }
      if (minutes < "10") { minutes = "0" + minutes; }
      if (seconds < "10") { seconds = "0" + seconds; }

      $("#days").html(days + "<span>Дня</span>");
      $("#hours").html(hours + "<span>Часов</span>");
      $("#minutes").html(minutes + "<span>Минут</span>");
      $("#seconds").html(seconds + "<span>Секунд</span>");    

  }

  setInterval(function() { makeTimer(); }, 1000);

  @endif;

});




</script>
@endsection