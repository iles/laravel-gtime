@extends('front.layout.main')

@section('content')

<div class="cabinet_main_container authorization">
	<div class="header">
    <div class="row">
      <div class="col-md-10">
		    <h2 class="title">Завершение программы</h2>        
      </div>
      <div class="col-md-2">
        <a class="link" href="#" onclick="document.getElementById('logout-form').submit(); return false;">
          <i class="icon-logout"></i>выход
        </a>
        <form id="logout-form" action="{{ route('partner.logout') }}" method="POST" style="display: none;">
          @csrf
        </form>
      </div>
    </div>

	</div>
	<div class="body">
		<div class="auth_cont" style="max-width: 100%;">



<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingOne">
      <h4 class="panel-title">
        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
          Отправить заявку на получение вознаграждения
        </a>
      </h4>
    </div>
    <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
      <div class="panel-body">
        <p style="margin: 25px 0 50px 0; font-weight: bold; font-size: 16px; color: #3e3d3c;">Поздравляем! <br/>
                Вы завершили программу «Старт»  для получения вознаграждения заполните форму ниже.</p>
 
                @if($bonus)
                    <div class="row" style="padding: 40px 0 0 10px;">
                      @if($bonus->status_id == 0)
                      Ваша заявка находится на рассмотрении
                      @elseif($bonus->status_id == 1)
                        Ваша заявка одобрена и находится на рассмотрении в бухгалтерии<br/>      
                      @elseif($bonus->status_id == 4)
                        Ваша заявка одобрена       
                      @elseif($bonus->status_id == 5)
                        Ваша заявка отклонена
                        <br/>
                        <form action="/partner/rebonus" method="POST">
                          @csrf
                          <input type="text" hidden  value="{{ $bonus->id }}" name="id">
                          <button type="submit" href="#" class="btn btn-primary">Создать новую заявку</button>
                        </form>  
                  
                      @elseif($bonus->status_id == 2)
                      Ваша заявка отклонена <br/>
                      <br/>
                      <pre>
                        {{ $bonus->cancel_reason }}
                      </pre>
                      <form action="/partner/rebonus" method="POST">
                        @csrf
                        <input type="text" hidden  value="{{ $bonus->id }}" name="id">
                        <button type="submit" href="#" class="btn btn-primary">Создать новую заявку</button>
                      </form>
                      @endif
                    </div>
                  @else
					  
			             @include('bonus-request.form_2', ['progs' => $progs])
                  @endif
      </div>
    </div>
  </div>

{{--   <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingThree">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
          Генерация Пин-кода в Накопительную программу
        </a>
      </h4>
    </div>
    <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
      <div class="panel-body">
      	{{ Form::open(['route' => 'pin.add', 'method'=>'post']) }}
        @csrf
          <input type="text" name="program_id" value="4" hidden="hidden">
      		<button class="btn btn-lg btn-success" type="submit" onclick="return confirm('Выу верены?')">
				<i class="fa fa-dot-circle-o"></i> Сгенерировать
			</button>  
      	{{ Form::close() }}
      </div>
    </div>
  </div> --}}
</div>






		</div>
		</div>

				</div>
@endsection

@section('scripts')


@endsection