@extends('front.layout.main')

@section('content')


<style type="text/css">
  .transfer-form{
    display: none;
  }  

  .transfer-form.active{
    display: block;
  }
</style>

<div class="cabinet_main_container authorization">
  <div class="header">
    <div class="row">
      <div class="col-md-10">
        <h2 class="title">Завершение программы</h2>        
      </div>
      <div class="col-md-2">
        <a class="link" href="#" onclick="document.getElementById('logout-form').submit(); return false;">
          <i class="icon-logout"></i>выход
        </a>
        <form id="logout-form" action="{{ route('partner.logout') }}" method="POST" style="display: none;">
          @csrf
        </form>
      </div>
    </div>

  </div>
  <div class="body">









<!-- Nav tabs -->
<ul class="nav nav-tabs" id="myTab" role="tablist">
  <li class="nav-item">
    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Вознаграждение(завершение)</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Вознаграждение (бонусы)</a>
  </li>
</ul>

<!-- Tab panes -->
<div class="tab-content">
  <div class="tab-pane active" id="home" role="tabpanel" aria-labelledby="home-tab">
    
 <div class="auth_cont" style="max-width: 100%;">

<p style="margin: 25px 0 50px 0; font-weight: bold; font-size: 16px; color: #3e3d3c;">Поздравляем Вы завершили программу «Бонус» для получения вознаграждения выберите подходящий вариант и заполните соответствующую форму ниже:</p>

<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingOne">
      <h4 class="panel-title">
        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
          Вариант А
        </a>
      </h4>
    </div>
    <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
      <div class="panel-body">
        <p style="margin: 25px 0 50px 0; font-weight: bold; font-size: 16px; color: #3e3d3c;">
          -Вознаграждение в денежном выражении 160 000тенге*..</p>
          @include('front.finish.bns.formA')




</div>
      </div>
    </div>
  </div>

  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingThree">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
            Вариант Б
        </a>
      </h4>
    </div>
    <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
      <div class="panel-body">

          <p style="margin: 65px 0 25px 0; font-weight: bold; font-size: 16px; color: #3e3d3c;">Пинкод для регистрации в Основную программу (с номинальной стоимостью 160 000тг)</p>

          {{ Form::open(['route' => 'pin.add', 'method'=>'post']) }}
            @csrf
            <input type="text" name="program_id" value="3" hidden="hidden">
            <input type="text" name="program_type" value="3" hidden="hidden">
            <input type="text" name="variant" value="2" hidden="hidden">
            <button class="btn btn-lg btn-success" type="submit" onclick="return confirm('Вы уверены?')">
              <i class="fa fa-dot-circle-o"></i> Сгенерировать
            </button>  
          {{ Form::close() }}

          </div>
        <br/>
        <br/>
      </div>

    </div>

  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingThree">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
            Вариант С
        </a>
      </h4>
    </div>
    <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
      <div class="panel-body">
          <p style="margin: 25px 0 50px 0; font-weight: bold; font-size: 16px; color: #3e3d3c;">Поздравляем! <br/>
          -Вознаграждение в денежном выражении 100 000тенге*.
          </p>

          @include('front.finish.bns.formC')


          <p style="margin: 65px 0 25px 0; font-weight: bold; font-size: 16px; color: #3e3d3c;">Пинкод для регистрации в Fast программу (с номинальной стоимостью 60 000тг)</p>

          {{ Form::open(['route' => 'pin.add', 'method'=>'post']) }}
            @csrf
            <input type="text" name="program_id" value="4" hidden="hidden">
            <input type="text" name="program_type" value="7" hidden="hidden">
            <input type="text" name="variant" value="3" hidden="hidden">
            <button class="btn btn-lg btn-success" type="submit" onclick="return confirm('Вы уверены?')">
              <i class="fa fa-dot-circle-o"></i> Сгенерировать
            </button>  
          {{ Form::close() }}

          </div>
        <br/>
        <br/>
      </div>

    </div>
	
  </div>  

  </div>
  <div class="tab-pane" id="profile" role="tabpanel" aria-labelledby="profile-tab">
    
    <style type="text/css">


a.active{
  color: #ff0200;
  text-decoration: underline;
}



.tgbl{
  display: none;
}

ul li:before {
    content: '';
    float: left;
    margin-left: -1.25em;
}
.tgbl.active{
  display: block;
}
</style>
<div class="cabinet_main_container">

    <div class="body">
      <strong> </strong>
    <div class="profile_body_text">
    
    @if(Session::has('message'))
                <div class="card-header" style="margin-bottom: 25px;">
                <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
                </div>
        @endif

<div class="card">


@if($invites->isEmpty())
Вы никого не пригласили
@else

@foreach($invites as $invite)
  @if(!$invite->bonus_id)
  @php
    $inv = json_decode($invite->invites);
    $reward = 10000 * count($inv);
    $fid = $invite->id;
  @endphp
    Вы пригласили:
    <br/>
    <br/>
    @foreach($inv as $i)
    <p><b>{{$i->user_name}}</b></p>
    @endforeach

    вознаграждение = {{ $reward  }}
    <br>
    <br>
    <br>
    <br>
    @include('front.finish.bonus-form', compact('reward', 'fid'))
  @else

  @php
    $bonus = \App\Models\Bonuses\StartBonusRequestModel::find($invite->bonus_id);
  @endphp

        @if($bonus)
          <div class="row" style="padding: 40px 0 0 10px;">
            @if($bonus->status_id == 0)
            Ваша заявка находится на рассмотрении
            @elseif($bonus->status_id == 1)
            Ваша заявка одобрена <br/>
            <br/>

            @elseif($bonus->status_id == 2)
            Ваша заявка отклонена <br/>
            <br/>
            <pre>
              {{ $bonus->cancel_reason }}
            </pre>
            <form action="/rebonus" method="POST">
              @csrf
              <input type="text" hidden  value="{{ $bonus->id }}" name="id">
              <button type="submit" href="#" class="btn btn-primary">Создать новую заявку</button>
            </form>
            @endif

          </div>
        @else
      asd
        @endif

<br><br><br><br><br>

  @endif
@endforeach

@endif

</div>





    </div>
          </div>
        </div>
  </div>

</div>















































   






















</div>






    </div>
    </div>

        </div>
@endsection


@section('scripts')

<script type="text/javascript">
  $('.transfer-button').on('click', function(e){
    e.preventDefault();
    $('.transfer-form').toggleClass('active');
  })
</script>

@endsection