@extends('front.layout.main')

@section('content')



<div class="cabinet_main_container authorization">

  <div class="header">
    <div class="row">
      <div class="col-md-10">
        <h2 class="title">Завершение программы</h2>        
      </div>
      <div class="col-md-2">
        <a class="link" href="#" onclick="document.getElementById('logout-form').submit(); return false;">
          <i class="icon-logout"></i>выход
        </a>
        <form id="logout-form" action="{{ route('partner.logout') }}" method="POST" style="display: none;">
          @csrf
        </form>
      </div>
    </div>

  </div>

<div class="body">
  <div class="auth_cont" style="max-width: 100%;">

<p style="margin: 25px 0 50px 0; font-weight: bold; font-size: 16px; color: #3e3d3c;">Поздравляем Вы завершили программу «Fast» для получения вознаграждения выберите подходящий вариант и заполните соответствующую форму ниже:</p>

<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">



  @if( ($fc && $fc->done2 !== null ) || ($fc2 && $fc2->done1 !== null) )
   <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingOne">
      <h4 class="panel-title">
        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
          Вариант А
        </a>
      </h4>
    </div>
    <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
      <div class="panel-body">
        <p style="margin: 25px 0 50px 0; font-weight: bold; font-size: 16px; color: #3e3d3c;">
          -Вознаграждение в денежном выражении 160 000 тенге*.
         </p>
          @include('front.finish.fast.formA')
      </div>
    </div>
  </div> 
  @else


  @if(!$fc && !$fc2)
     <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingOne">
      <h4 class="panel-title">
        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
          Вариант А
        </a>
      </h4>
    </div>
    <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
      <div class="panel-body">
        <p style="margin: 25px 0 50px 0; font-weight: bold; font-size: 16px; color: #3e3d3c;">
          -Вознаграждение в денежном выражении 160 000 тенге*.
        </p>
          @include('front.finish.fast.formA')
      </div>
    </div>
  </div> 
  @endif

  @endif

  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingThree">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
            Вариант Б
        </a>
      </h4>
    </div>
    <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
      <div class="panel-body">
          <p style="margin: 25px 0 50px 0; font-weight: bold; font-size: 16px; color: #3e3d3c;">Поздравляем! <br/>
            Получить пин код - бизнес место в программу «Основной»
          </p>



          <p style="margin: 65px 0 25px 0; font-weight: bold; font-size: 16px; color: #3e3d3c;">Пинкод для регистрации в "Основную программу", </p>
          {{ Form::open(['route' => 'pin.add', 'method'=>'post']) }}
            @csrf
            <input type="text" name="program_id" value="3" hidden="hidden">
            <input type="text" name="program_type" value="3" hidden="hidden">
            <button class="btn btn-lg btn-success" type="submit" onclick="return confirm('Вы уверены?')">
              <i class="fa fa-dot-circle-o"></i> Сгенерировать
            </button>  
          {{ Form::close() }}


</div>

        <br/>
        <br/>
      </div>

    </div>
  </div>  
</div>


    </div>
    </div>

        </div>
@endsection
