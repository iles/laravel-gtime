@extends('front.layout.main')

@section('content')

  <style type="text/css">
    

a.active{
  color: #ff0200;
  text-decoration: underline;
}



.tgbl{
  display: none;
}

ul li:before {
    content: '';
    float: left;
    margin-left: -1.25em;
}
.tgbl.active{
  display: block;
}
</style>
  </style>

<div class="cabinet_main_container authorization">
  <div class="header">
    <div class="row">
      <div class="col-md-10">
        <h2 class="title">Завершение программы</h2>        
      </div>
      <div class="col-md-2">
        <a class="link" href="#" onclick="document.getElementById('logout-form').submit(); return false;">
          <i class="icon-logout"></i>выход
        </a>
        <form id="logout-form" action="{{ route('partner.logout') }}" method="POST" style="display: none;">
          @csrf
        </form>
      </div>
    </div>

  </div>
	<div class="body">



<!-- Nav tabs -->
<ul class="nav nav-tabs" id="myTab" role="tablist">
  <li class="nav-item">
    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Вознаграждение(завершение)</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Вознаграждение (бонусы)</a>
  </li>
</ul>







<div class="tab-content">
  <div class="tab-pane active" id="home" role="tabpanel" aria-labelledby="home-tab">



		<div class="auth_cont" style="max-width: 100%;">   

  <p style="margin: 65px 0 25px 0; font-weight: bold; font-size: 16px; color: #3e3d3c;">Пинкод для регистрации в Накопительную программу (с номинальной стоимостью 15 000тг)</p>
  @if(!$pin)
          {{ Form::open(['route' => 'pin.add', 'method'=>'post']) }}
            @csrf
            <input type="text" name="program_id" value="4" hidden="hidden">
            <input type="text" name="program_type" value="3" hidden="hidden">
            <input type="text" name="variant" value="2" hidden="hidden">
            <button class="btn btn-lg btn-success" type="submit" onclick="return confirm('Вы уверены?')">
              <i class="fa fa-dot-circle-o"></i> Сгенерировать
            </button>  
          {{ Form::close() }}
 @else

                  <div class="row">
                    <div class="col-md-4"><b>{{ $pin->pin }}</b></div>
                    <div class="col-md-6">
                      @if( $pin->status_id == 0 )
                      <pre>Действителен до: <br/>{{ $pin->expired_at }}</pre>
                      @else
                      <pre style="color: red;">Заблокирован</pre>
                      @endif
                    </div>
                  </div>

 @endif



		</div>
    </div>


  <div class="tab-pane" id="profile" role="tabpanel" aria-labelledby="profile-tab">
    
  
<div class="card">


@if($invites->isEmpty())
Вы никого не пригласили
@else

@foreach($invites as $invite)
  @if(!$invite->bonus_id)
  @php
    $inv = json_decode($invite->invites);
    $reward = 10000 * count($inv);
    $fid = $invite->id;
  @endphp
    Вы пригласили:
    <br/>
    <br/>
    @foreach($inv as $i)
    <p><b>{{$i->user_name}}</b></p>
    @endforeach

    вознаграждение = {{ $reward  }}
    <br>
    <br>
    <br>
    <br>
    @include('front.finish.bonus-form', compact('reward', 'fid'))
  @else

  @php
    $bonus = \App\Models\Bonuses\StartBonusRequestModel::find($invite->bonus_id);
  @endphp

        @if($bonus)
          <div class="row" style="padding: 40px 0 0 10px;">
            @if($bonus->status_id == 0)
            Ваша заявка находится на рассмотрении
            @elseif($bonus->status_id == 1)
            Ваша заявка одобрена <br/>
            <br/>

            @elseif($bonus->status_id == 2)
            Ваша заявка отклонена <br/>
            <br/>
            <pre>
              {{ $bonus->cancel_reason }}
            </pre>
            <form action="/rebonus" method="POST">
              @csrf
              <input type="text" hidden  value="{{ $bonus->id }}" name="id">
              <button type="submit" href="#" class="btn btn-primary">Создать новую заявку</button>
            </form>
            @endif

          </div>
        @else
      asd
        @endif

<br><br><br><br><br>

  @endif
@endforeach

@endif

</div>

</div>





    </div>
          </div>
        </div>
  </div>

    </div>
	</div>

</div>
@endsection

@section('scripts')
<script type="text/javascript">


$(document).ready(function(){
  $('#form-click').on( 'click', function(e){
  e.preventDefault();
  $('.profile_table').toggleClass('active');
  })

  $(":input").inputmask();

  @if( isset($pin) )

  function makeTimer() {

  //    var endTime = new Date("29 April 2018 9:56:00 GMT+01:00");  
    var endTime = new Date("{{ $pin->expired_at }}");     
      endTime = (Date.parse(endTime) / 1000);

      var now = new Date();
      now = (Date.parse(now) / 1000);

      var timeLeft = endTime - now;

      var days = Math.floor(timeLeft / 86400); 
      var hours = Math.floor((timeLeft - (days * 86400)) / 3600);
      var minutes = Math.floor((timeLeft - (days * 86400) - (hours * 3600 )) / 60);
      var seconds = Math.floor((timeLeft - (days * 86400) - (hours * 3600) - (minutes * 60)));
  
      if (hours < "10") { hours = "0" + hours; }
      if (minutes < "10") { minutes = "0" + minutes; }
      if (seconds < "10") { seconds = "0" + seconds; }

      $("#days").html(days + "<span>Дня</span>");
      $("#hours").html(hours + "<span>Часов</span>");
      $("#minutes").html(minutes + "<span>Минут</span>");
      $("#seconds").html(seconds + "<span>Секунд</span>");    

  }

  setInterval(function() { makeTimer(); }, 1000);

  @endif;

});




</script>
@endsection