@extends('front.layout.main')

@section('content')

<div class="cabinet_main_container authorization">
	<div class="header">
		<h2 class="title">Накопительный+</h2>
	</div>
	<div class="body">
		<div class="auth_cont">

		{{ Form::open(array('route' => 'partner.loginPartner')) }}
				<p class="text">Введите ваши регистрационные данные для входа в личный кабинет2</p>
				@if(Session::has('message'))
					<p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
				@endif

				@if ($errors->any())
				    <div class="alert alert-danger">
				        <ul>
				            @foreach ($errors->all() as $error)
				                <li>{{ $error }}</li>
				            @endforeach
				        </ul>
				    </div>
				@endif
			
				{{ Form::text('username', '', ['class'=>'form-control', 'placeholder'=>'Логин']) }}
				{{ Form::password('password', ['class'=>'form-control', 'placeholder'=>'Пароль']) }}

				<div class="link_cont"><a href="#" class="link">восстановить пароль</a></div>
				<input type="submit" value="авторизоваться" class="button">
			{{ Form::close() }}
			<input type="file" style="display: none;" name="">
		</div>
		</div>

				</div>
@endsection