        @php
          $m = Config::get('matrix.get');
          $p = Session::get('programm', 1);
          if($p == null){
            Session::put('programm', 1);
            $p = Session::get('programm', 1);
          }
        @endphp

					<div class="header">
                    <div class="row">
                        <div class="col-md-12">
                            <h2 class="title">{{ __($m[$p][1]) }}</h2>

                            <p class="pull-right">
                                                                              @if (Auth::guard('partner')->check())
                                                    <a class="link" href="#" onclick="document.getElementById('logout-form').submit(); return false;">
                                                        <i class="icon-logout"></i>
                                                        {{__('выход')}}
                                                    </a>
                                                    <form id="logout-form" action="{{ route('partner.logout') }}" method="POST" style="display: none;">
                                                     @csrf
                                                    </form>
                                                @else
                                                    <a href="#" class="link">{{__('вход')}}</a>
                                                @endif
                            </p>
                        </div>
                        <div class="col-md-12">
                            <div class="nav_cont">

            									<a href="{{ route('partner.registration') }}" class="link {{ request()->is('partner/registration') ? 'active' : '' }}">{{__('Регистрация')}}</a>
            									<a href="{{ route('partner.profile') }}" class="link {{ request()->is('partner/profile') ? 'active' : '' }}">{{__('Мой профиль')}}</a>
            									<a href="{{ route('ladder')}}" class="link {{ request()->is('partner/ladder') ? 'active' : '' }}">{{__('Моя лестница')}}</a>
{{--             									<a href="{{ route('partner.bonus') }}" class="link {{ request()->is('partner/bonus') ? 'active' : '' }}">Вознаграждения</a> --}}
            									<a href="{{ route('partner.structure') }}" class="link {{ request()->is('partner/bonus') ? 'active' : '' }}">{{__('Моя структура')}}</a>
 
                                                @if($p == 5 || $p == 2)
                                                @else
            									<a href="{{ route('partner.pin') }}" class="link {{ request()->is('partner/pin') ? 'active' : '' }}">{{__('Пин-код')}}</a>				
                                                @endif		
            									<a href="{{ route('partner.marketing') }}" class="link {{ request()->is('partner/marketing') ? 'active' : '' }}">{{__('Маркетинг')}}</a>
                                                @if( \App\Models\Finished::where('user_id',  Auth::user()->user_id )->where('program_id', 3)->where('stage', 1)->first() );
                                                    <a href="/partner/main/finish" class="link {{ request()->is('partner/main/finish') ? 'active' : '' }}">{{__('Вознаграждение')}}</a>
                                                @endif
                                                @if( \App\Models\Finished::where('user_id',  Auth::user()->user_id )->where('program_id', 6)->where('stage', 1)->first() );
                                                    <a href="/partner/vip/finish" class="link {{ request()->is('partner/vip/finish') ? 'active' : '' }}">{{__('Вознаграждение')}}</a>
                                                @endif   
                                           
                                                @if( $p == 6 );
                                                    <a href="/partner/vipbonus" class="link {{ request()->is('partner/main/finish') ? 'active' : '' }}">{{__('Вознаграждение Бонус')}}</a>
                                                @endif                                                
                                                @if( $p == 8 );
                                                    <a href="/partner/bonus-form" class="link {{ request()->is('partner/bonus-program/finish') ? 'active' : '' }}">{{__('Вознаграждение (Bonus)')}}</a>
                                                @endif

                            </div>
                        </div>
                    </div>
                </div>