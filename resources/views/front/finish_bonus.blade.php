@extends('front.layout.main')

@section('content')

<div class="cabinet_main_container authorization">
	<div class="header">
	    <div class="row">
	      <div class="col-md-10">
			    <h2 class="title">Завершение программы</h2>        
	      </div>
	      <div class="col-md-2">
	        <a class="link" href="#" onclick="document.getElementById('logout-form').submit(); return false;">
	          <i class="icon-logout"></i>выход
	        </a>
	        <form id="logout-form" action="{{ route('partner.logout') }}" method="POST" style="display: none;">
	          @csrf
	        </form>
	      </div>
	    </div>
	</div>
	<div class="body">
		<div class="auth_cont" style="max-width: 100%;">

			@if($bonus->status_id == 5)
			   <p class="pin-info"> Ваша заявка под номером <span> {{ $bonus->id }}</span> от <span>{{ $bonus->created_at }}</span> одобренна</p>
			   <p>Вознаграждение выплачено</p>
		      @elseif($bonus->status_id == 0 || $bonus->status_id == 1)
		      	<p class="pin-info"> Ваша заявка под номером <span> {{ $bonus->id }}</span> от <span>{{ $bonus->created_at }}</span> находится в режиме ожидания</p>
   
		      @elseif($bonus->status_id == 2 || $bonus->status_id == 6)
		      	<p class="pin-info"> Ваша заявка под номером <span> {{ $bonus->id }}</span> от <span>{{ $bonus->created_at }}</span> отклонена</p>
		      	<p>
		      		<pre>{{ $bonus->cancel_reason }}</pre>
		      	</p>
					<form method="post" action="{{ action('FrontController@rebonus') }}" id="update_bonus_form" class="form-horizontal" accept-charset="UTF-8">
						@csrf
						<input hidden="hidden" name="bonus_id" value="{{ $bonus->id }}">
						<p style="margin-top: 35px;"><button  type="submit" class="btn dtn-success"> Отравить повторно </button></p>	
					</form>
		      @endif
		</div>
	</div>

</div>
@endsection

@section('scripts')


@endsection