@extends('front.layout.main')

@section('content')
<div class="cabinet_main_container">
	@include('front.layout.menu')
<div class="body">


    <div id="dle-content">
    	<form method="post" name="userinfo" id="userinfo" enctype="multipart/form-data" action="http://test.gtime.kz/start/index.php?subaction=userinfo&amp;user=user">

    <div class="profile_body_text">


		<table class="profile_table">
			<tbody>
				<tr>
					<td>{{__('Пользователь')}}</td>
					<td>{{ $user->name }}</td>
				</tr>
				<tr>
					<td>{{__('Группа')}}:</td>
					<td>{{__('Главные редакторы')}}</td>
				</tr>
				<tr>
					<td>{{__('Дата регистрации')}}:</td>
					<td>{{ gmdate("Y-m-d", $user->reg_date)  }} <br/> {{ gmdate("H:i:s", $user->reg_date)  }} </td>
				</tr>
				<tr>
					<td>{{__('Последнее посещение')}}:</td>
					<td>{{ gmdate("Y-m-d", $user->lastdate)  }} <br/> {{ gmdate("H:i:s", $user->lastdate)  }} </td>
				</tr>
				<tr>
					<td>{{__('Ф.И.О')}}:</td>
					<td>{{ $user->familiya }}</td>
				</tr>
				<tr>
					<td>{{__('Страна и город')}}:</td>
					<td>{{ $user->imya }}</td>
				</tr>
				<tr>
					<td>{{__('Дата рождения')}}:</td>
					<td></td>
				</tr>
				<tr>
					<td>{{__('Номер телефона')}}:</td>
					<td>{{ $user->pol }}</td>
				</tr>
				<tr>
					<td>{{__('ИИН')}}:</td>
					<td>{{ $user->strana }}</td>
				</tr>
				<tr>
					<td>{{__('Прописка')}}:</td>
					<td>
						@php
							$info = explode(';', $user->info)
						@endphp
						{{ $info[0] }}
					</td>
				</tr>
			</tbody>
		</table>

		@if( in_array(Session::get('programm', 1), [2,3,5,6,7]  ) )		
    		<a class="btn btn-primary btn-large" href="/partner/swapuser" style="margin-top: 35px;display: inline-block;">{{__('Оставить заявку на замену')}}</a>
    	@endif

	</div>




		<input type="hidden" name="doaction" value="adduserinfo">
		<input type="hidden" name="id" value="4339">
		<input type="hidden" name="dle_allow_hash" value="953be8b65524a94088fc8844ec8e5934">
		</form><div class="berrors"><div class="berrors">
	<b></b><br>

</div></div></div>
                </div>
				</div>
@endsection