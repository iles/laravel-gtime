        @php
          $m = Config::get('matrix.get');
          $p = Session::get('programm', 1);
          if($p == null){
            Session::put('programm', 1);
            $p = Session::get('programm', 1);
          }
        @endphp

@extends('front.layout.main')

@section('content')
<div class="cabinet_main_container">
	@include('front.layout.menu')
	<div class="body">
		<h2 class="title">
			Запрос на вознаграждение	
		</h2>

			@if($bonus)

				@if($bonus->status_id == 0)
					<p class="pin-info"> Ваша заявка под номером <span> {{ $bonus->id }}</span> от <span>{{ $bonus->created_at }}</span> находится в режиме ожидания</p>
				@elseif($bonus->status_id == 2)
					<p class="pin-info"> Ваша заявка под номером <span> {{ $bonus->id }}</span> от <span>{{ $bonus->created_at }}</span> отклонена</p>
					<p>
						<pre>{{ $pin->cancel_reason }}</pre>
					</p>
					<a href="#" id="form-click">Отправить запрос повторно</a>
				@endif
			@else
						<div class="row">
							<table class="profile_table">
								{{ Form::open(array('route' => 'partner.bonus.store', 'files' => true)) }}
								<tbody>
								<tr>
									<td>{{ Form::label('program', 'Программа') }}</td>
									<td>{{  $m[$p][1] }}</td>
								</tr>
							
								<tr>
									<td>{{ Form::label('uname', 'Логин') }}</td>
									<td>{{ Auth::user()->name }}</td>
								</tr>									

								<tr>
									<td>{{ Form::label('summ', 'Сумма ') }}</td>
									<td>15 000</td>
								</tr>									
								<tr>
									<td>{{ Form::label('second_name', 'Фамилия') }}</td>
									<td>{{ Form::text('name', null, ['class'=>'form-control', 'required'=>'required']) }}</td>
								</tr>								
								<tr>
									<td>{{ Form::label('name', 'Имя

								') }}</td>
									<td>{{ Form::text('second_name', null, ['class'=>'form-control', 'required'=>'required']) }}</td>
								</tr>								
								<tr>
									<td>{{ Form::label('patronic', 'Отчество') }}</td>
									<td>{{ Form::text('patronic', null, ['class'=>'form-control', 'required'=>'required']) }}</td>
								</tr>								
								<tr>
									<td>{{ Form::label('iin', 'ИИН') }}</td>
									<td>{{ Form::text('iin', null, ['data-inputmask' => '"mask": "999999999999"', 'class'=>'form-control', 'required'=>'required']) }}</td>
								</tr>
								
								<tr>
									<td>{{ Form::label('bill_number', 'Счет получателя') }}</td>
									<td>{{ Form::text('bill_number', null, ['data-inputmask' => '"mask": "999999999999"', 'class'=>'form-control', 'required'=>'required']) }}</td>
								</tr>
								<tr>
									<td><button class="btn btn-sm btn-primary" type="submit">Отправить</button> </td>
								</tr>
							</tbody>
							{{ Form::close() }}
						</table>
						</div>
			@endif			
					</div>
				</div>
@endsection

@section('scripts')
	<script type="text/javascript">
		$(document).ready( function(){
			$(":input").inputmask();			
		});
	</script>
@endsection
