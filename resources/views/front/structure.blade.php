@extends('front.layout.main')

@section('content')
        @php
          $m = Config::get('matrix.get');
          $p = Session::get('programm', 1);
          if($p == null){
            Session::put('programm', 1);
            $p = Session::get('programm', 1);
          }
        @endphp

<div class="cabinet_main_container">
					@include('front.layout.menu')
					<div class="body">
						<div class="stairs_body_text">

							<div class="stairs_cont">

							</div>
							<div class="clearfix"></div>
						</div>
					</div>
				</div>
@endsection

@section('scripts')
	<script type="text/javascript">
		$(document).ready( function(){
			search_ladder('{{  Auth::user()->name }}');
		});

		function search_ladder(name){
			$('.stairs_cont').html('');
			$.get( "/partner/search", { name: name } )
			  .done(function( data ) {
			    if(data == 'Пользователь не найден'){
					$('.stairs_cont').html('<p style="color:red">'+ data + '</p>');
			    	$('.stairs_cont').fadeIn();
			    	return false;
			    }
			    build_ladder( data );
			  });
		}

		function build_ladder(data){
			$('.stairs_cont').html('');
			$.each( data, function( key, value ) {
				$('.stairs_cont').append('<ul class="level_ui_'+ key +'"></ul> ');
				build_level(value, key)
			});

			$('.stairs_cont').fadeIn();
		}

		function build_level(value, key){
			$.each( value, function( keyv, valuev ) {
				$('.level_ui_'+ key ).append('<div class="node"><div>' + valuev.uname + '<div class="sponsor">' + valuev.by_refer + '</div><div>' + build_stars(valuev.stars) + '</div></div></div>');
			});
		}

		function build_stars($n){
			var stars = '';
			var i = 0;
			while (i < $n) {
			  stars = stars + '<img src="/front/images/matrix-star.png">'  ;
			  i++;
			}
			return stars;
		}
	</script>
@endsection