@extends('front.layout.main')

@section('content')


@section('content')

<style type="text/css">


a.active{
	color: #ff0200;
	text-decoration: underline;
}



.tgbl{
	display: none;
}
.tgbl.active{
	display: block;
}
</style>

<div class="cabinet_main_container">
	@include('front.layout.menu')
		<div class="body">
			<strong> </strong>
		<div class="profile_body_text">

		@if(Session::has('message'))
                <div class="card-header" style="margin-bottom: 25px;">
            		<p class="alert succsess">{{ Session::get('message') }}</p>
                </div>
        @endif

		<a href="/partner/profile">&#8592; Назад</a>

		@if($swap)
		<div class="row" style="padding: 40px 0 0 10px;">
			@if($swap->status_id == 0)
			Ваша заявка находится на рассмотрении
			@elseif($swap->status_id == 1)
			Ваша заявка одобрена <br/>
			<br/>
			<form action="/reswap" method="POST">
				@csrf
				<input type="text" hidden  value="{{ $swap->id }}" name="id">
				<button type="submit" href="#" class="btn btn-primary">Создать новую заявку</button>
			</form>			
			@elseif($swap->status_id == 2)
			Ваша заявка отклонена <br/>
			<br/>
			<pre>
				{{ $swap->cancel_reason }}
			</pre>
			<form action="/reswap" method="POST">
				@csrf
				<input type="text" hidden  value="{{ $swap->id }}" name="id">
				<button type="submit" href="#" class="btn btn-primary">Создать новую заявку</button>
			</form>
			@endif
		</div>

		@else


							<div class="pin-request-table">
								<table class="profile_table">
									{{ Form::open(['route' => 'partner.swap', 'method'=>'post', 'files'=>true, 'autocomplete'=>"off"]) }}
									<tbody>


									<tr>
										<td>{{ Form::label('login', 'Замена') }}</td>
										<td>
											<select name="type" id="type-select" class="form-control">
												<option value="1">По заявлению</option>
												<option value="2">По приказу №7</option>
												@if( in_array(Session::get('programm', 1), [3,6]  ) )	
													<option value="3">Спонсора</option>
												@endif
											</select>
										</td>
									</tr>

									<tr>
										<td>{{ Form::label('login', 'Логин') }}</td>
										<td>{{ Form::text('login', null, ['class'=>'form-control', 'required'=>'required']) }}</td>
									</tr>

									<tr>
										<td>{{ Form::label('login2', 'Заменить на') }}</td>
										<td>{{ Form::text('login2', null, ['class'=>'form-control', 'required'=>'required']) }}</td>
									</tr>

									<tr>
										<td>{{ Form::label('images', 'Скан документа') }}</td>
										<td>{{ Form::file('images[]', array('class' => 'form-control', 'required'=>'required', 'multiple'=>true, 'accept'=>".jpeg, .jpg, .png, .pdf") ) }}</td>
									</tr>




									<tr>
										<td><button class="btn btn-sm btn-primary" type="submit">Отправить</button> </td>
									</tr>
									</tbody>
									{{ Form::close() }}
								</table>
							</div>
		@endif
						</div>
					</div>
				</div>
@endsection

@section('scripts')
<script type="text/javascript">


$(document).ready( function(){




	// $('#type-select').on('change', function() {
	//   alert( this.value );
	// });



});



</script>
@endsection