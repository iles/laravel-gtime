@extends('front.layout.main')

@section('content')

<div class="cabinet_main_container">
    @include('front.layout.menu')

  <div class="body">
    <div class="auth_cont" style="max-width: 100%;">



<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingOne">
      <h4 class="panel-title">
        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
          Отправить заявку на получение вознаграждения
        </a>
      </h4>
    </div>
    <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
      <div class="panel-body">
        <p style="margin: 25px 0 50px 0; font-weight: bold; font-size: 16px; color: #3e3d3c;">Поздравляем! <br/>
          Вы завершили программу «BONUS старт» для получения вознаграждения заполните форму ниже</p>
          @include('front.finish.bonus.form')
      </div>
    </div>
  </div>

{{--   <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingThree">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
          Генерация Пин-кода в Накопительную программу
        </a>
      </h4>
    </div>
    <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
      <div class="panel-body">
        {{ Form::open(['route' => 'pin.add', 'method'=>'post']) }}
        @csrf
          <input type="text" name="program_id" value="4" hidden="hidden">
          <button class="btn btn-lg btn-success" type="submit" onclick="return confirm('Выу верены?')">
        <i class="fa fa-dot-circle-o"></i> Сгенерировать
      </button>  
        {{ Form::close() }}
      </div>
    </div>
  </div> --}}
</div>






    </div>
    </div>

@endsection

@section('scripts')


@endsection