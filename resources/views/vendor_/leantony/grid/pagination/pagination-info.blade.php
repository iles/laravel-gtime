@if($grid->wantsPagination() && !$grid->gridNeedsSimplePagination())
    <div class="pull-{{ $direction }}">
        <b>
            @if($grid->getData()->total() <= $grid->getData()->perpage())
                @if(!isset($atFooter))
                    Показанно от {{ ($grid->getData()->currentpage() - 1 ) * $grid->getData()->perpage() + 1 }}
                    до {{ $grid->getData()->total() }}
                    из {{ $grid->getData()->total() }} записей.
                @endif
            @else
                Показанно от {{ ($grid->getData()->currentpage() - 1 ) * $grid->getData()->perpage() + 1 }}
                до {{ $grid->getData()->currentpage() * $grid->getData()->perpage() }}
                из {{ $grid->getData()->total() }} записей.
            @endif
        </b>
    </div>
@else
    @if(isset($atFooter))
        @if($grid->getData()->count() >= $grid->getData()->perpage())
            <div class="pull-{{ $direction }}">
                <b>
                    Showing {{ $grid->getData()->count() }} records for this page.
                </b>
            </div>
        @endif
    @else
        <div class="pull-{{ $direction }}">
            <b>
                Showing {{ $grid->getData()->count() }} records for this page.
            </b>
        </div>
    @endif
@endif