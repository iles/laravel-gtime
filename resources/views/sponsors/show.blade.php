        @php
          $m = Config::get('matrix.get');
          $p = Session::get('programm', 1);
          if($p == null){
            Session::put('programm', 1);
            $p = Session::get('programm', 1);
          }
        @endphp

<div class="card" style="width: 100%; margin: 0 auto;">

  <div class="card-header">
    <strong>{{$model->name}}</strong><br/>
    <a href="#" id="change_button" onClick="switch_form();return false;">Поменять пароль</a>
  </div>

  <form method="post" action="{{ action('SponsorsController@passchange') }}" id="passchange_form" class="form-horizontal" accept-charset="UTF-8">
    @csrf
    <div class="card-body">
      <div class="form-group row">
        <label class="col-md-3 col-form-label" for="hf-password">Пароль</label>
          <div class="col-md-9">
            <input class="" name="user_id" type="text" hidden value="{{ $model->user_id }}">
            <input class="form-control" name="password"  minlength="8" type="password" >
          </div>
      </div>
      <div class="form-group row">
        <label class="col-md-3 col-form-label" for="hf-password">Подтверждение Пароля</label>
          <div class="col-md-9">
            <input class="form-control" name="password_confirmation"  minlength="8" type="password">
          </div>
      </div>
    </div>

    <div class="card-footer">
      <button class="btn btn-sm btn-primary" type="submit">
      <i class="fa fa-dot-circle-o" type="submit"></i> Сохранить</button>
    </div>

  </form>

<form method="post" action="{{ action('SponsorsController@update') }}" id="create_pin_form" class="form-horizontal" accept-charset="UTF-8">
    <div class="card-body">
        @csrf

        <input class="form-control" name="user_id" type="text" hidden value="{{ $model->user_id }}">
        <div class="form-group row">
          <label class="col-md-3 col-form-label" for="hf-password">Логин</label>
          <div class="col-md-9">
            <input class="form-control" name="name" type="text" value="{{ $model->name }}">
          </div>
        </div>



        <div class="form-group row">
          <label class="col-md-3 col-form-label" for="hf-password">ФИО</label>
          <div class="col-md-9">
            <input class="form-control" name="familiya" type="text" value="{{$model->familiya }}">
          </div>
        </div>

        <div class="form-group row">
          <label class="col-md-3 col-form-label" for="hf-password">Страна / Город</label>
          <div class="col-md-9">
            <input class="form-control" name="imya " type="text" value="{{$model->imya }}">
          </div>
        </div>

        <div class="form-group row">
          <label class="col-md-3 col-form-label" for="hf-password">Дата рождения</label>
          <div class="col-md-9">
            <input class="form-control" name="otchestvo" type="text" value="{{$model->otchestvo }}">
          </div>
        </div>

        <div class="form-group row">
          <label class="col-md-3 col-form-label" for="hf-password">Телефон</label>
          <div class="col-md-9">
            <input class="form-control" name="pol" type="text" value="{{$model->pol }}">
          </div>
        </div>

        <div class="form-group row">
          <label class="col-md-3 col-form-label" for="hf-password">ИИН</label>
          <div class="col-md-9">
            <input class="form-control" name="strana" type="text" value="{{$model->strana }}">
          </div>
        </div>



        <div class="form-group row">
          <label class="col-md-3 col-form-label" for="hf-password">Звезды</label>
          <div class="col-md-9">
            <input class="form-control" name="ref_num" type="text" value="{{$model->ref_num }}">
          </div>
        </div>

        <div class="form-group row">
          <label class="col-md-3 col-form-label" for="hf-password">Спонсор</label>
          <div class="col-md-9">
            <input class="form-control" name="ref_num" type="text" value="{{$model->by_refer }}">
          </div>
        </div>

        <div class="form-group row">
          <label class="col-md-3 col-form-label" for="hf-password">Skype</label>
          <div class="col-md-9">
            <input class="form-control" name="gorod" type="text" value="{{$model->skype }}">
          </div>
        </div>

        <div class="form-group row">
          <label class="col-md-3 col-form-label" for="hf-password">Дата регистрации</label>
          <div class="col-md-9">
            <input class="form-control" name="gorod" type="text" value="{{ gmdate("Y-m-d", $model->reg_date) }}">
          </div>
        </div>

        <div class="form-group row">
          <label class="col-md-3 col-form-label" for="hf-password">Приписка (signature)</label>
          <div class="col-md-9">
            <textarea class="form-control" name="signature">{{ $model->signature }}</textarea>
          </div>
        </div>


        <div class="form-group row">
          <label class="col-md-3 col-form-label" for="hf-password">Приписка(info)</label>
          <div class="col-md-9">
            <textarea class="form-control" name="info">{{ $model->info }}</textarea>
          </div>
        </div>

        <div class="form-group row">
          <label class="col-md-3 col-form-label" for="hf-password">Товар</label>
          <div class="col-md-9">
            <textarea class="form-control" name="info">{{ $model->tovar }}</textarea>
          </div>
        </div>

        @if ($p == 6)
        <div class="form-group row">
          <label class="col-md-3 col-form-label" for="hf-password">Вип бонус</label>
          <div class="col-md-9">
            <textarea class="form-control" name="signature">{{ $model->vip_bonus }}</textarea>
          </div>
        </div>
        @endif

{{--         <div class="form-group row">
          <label class="col-md-3 col-form-label" for="hf-password">Примечание</label>
          <div class="col-md-9">
            <textarea class="form-control" name="signature">{{ $model->info }}</textarea>
          </div>
        </div>   --}}



    </div>

    <div class="card-footer">
      <button class="btn btn-sm btn-primary" type="submit">
      <i class="fa fa-dot-circle-o" type="submit"></i> Сохранить</button>
      <button class="btn btn-sm btn-danger"  data-dismiss="modal" aria-label="Close" type="reset">
      <i class="fa fa-ban"></i> Закрыть</button>
    </div>
    </form>
</div>