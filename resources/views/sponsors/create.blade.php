<div class="card">
   <form <form method="post" action="{{ action('SponsorsController@store') }}" id="create_pin_form" class="form-horizontal" accept-charset="UTF-8"> 
  <div class="card-header">

    <div class="card-body">
        @csrf

        <div class="form-group row">
          <label class="col-md-3 col-form-label" for="hf-password">Логин</label>
          <div class="col-md-9">           
            <input class="form-control" name="name" type="text">
          </div>
        </div>          

        <div class="form-group row">
          <label class="col-md-3 col-form-label" for="hf-password">Email</label>
          <div class="col-md-9">           
            <input class="form-control" name="email" type="text"> 
          </div>
        </div>          

        <div class="form-group row">
          <label class="col-md-3 col-form-label" for="hf-password">Фамилия</label>
          <div class="col-md-9">           
            <input class="form-control" name="familiya" type="text">
          </div>
        </div>  

        <div class="form-group row">
          <label class="col-md-3 col-form-label" for="hf-password">Имя</label>
          <div class="col-md-9">           
            <input class="form-control" name="imya " type="text">
          </div>
        </div>         

        <div class="form-group row">
          <label class="col-md-3 col-form-label" for="hf-password">Отчество</label>
          <div class="col-md-9">           
            <input class="form-control" name="otchestvo" type="text">
          </div>
        </div>          

        <div class="form-group row">
          <label class="col-md-3 col-form-label" for="hf-password">Пол</label>
          <div class="col-md-9">           
            <input class="form-control" name="pol" type="text">
          </div>
        </div>         

        <div class="form-group row">
          <label class="col-md-3 col-form-label" for="hf-password">Страна</label>
          <div class="col-md-9">           
            <input class="form-control" name="strana" type="text">
          </div>
        </div>          

        <div class="form-group row">
          <label class="col-md-3 col-form-label" for="hf-password">Город</label>
          <div class="col-md-9">           
            <input class="form-control" name="gorod" type="text">
          </div>
        </div>         

        <div class="form-group row">
          <label class="col-md-3 col-form-label" for="hf-password">Skype</label>
          <div class="col-md-9">           
            <input class="form-control" name="gorod" type="text">
          </div>
        </div>          

        <div class="form-group row">
          <label class="col-md-3 col-form-label" for="hf-password">Примечание</label>
          <div class="col-md-9">           
            <input class="form-control" name="info" type="text">
          </div>
        </div>  


        
    </div>
        
    <div class="card-footer">
      <button class="btn btn-sm btn-primary" type="submit">
      <i class="fa fa-dot-circle-o" type="submit"></i> Сохранить</button>
      <button class="btn btn-sm btn-danger"  data-dismiss="modal" aria-label="Close" type="reset">
      <i class="fa fa-ban"></i> Закрыть</button>
    

    </div>
    </form>
</div>