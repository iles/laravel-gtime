<div class="card">

<div class="card-header">
  Последние пользователи  
</div>

<div class="card-body">
    @foreach($data as $user)
    <div class="row">
      <div class="col-md-5 firsts" style="border-bottom: 1px solid #c67e7e; margin-bottom: 12px;">
        <p><span style="font-weight: bold;">{{ $user['uname'] }}</span> <small>@if(isset($user['fullname'])){{ $user['fullname'] }}@endif;</small>
        	@if($p == 3 || $p == 6)
        	<small><a href="#" class="watch" onclick="load(); return false;">Смотреть</a></small>
        	@endif
        </p>
        <p>@if(isset($user['pin'])){{ $user['pin'] }}@endif</p>
      </div>
      <div class="col-md-7 others"></div>
    </div>
    @endforeach    
</div>

</div>