@extends('layouts.app')

@section('content')
<div class="card shadow mb-4">
  <div class="card-header"> <strong> Переводы </strong>  
  </div>
  <div class="card-body">
    <div class="row">
      <div class="col-12"> 
        <form action="{{url('admin/translation')}}" method="post">
           @csrf
          <button class="btn btn-danger pull-right copy" type="button" >+ Добавить</button>
          <button class="btn btn-success" >Сохранить</button>

          <table class="table table-striped" id="table2">
            <thead>
              <tr>
                <th>Ключ</th>
                @foreach($langlist as $key => $lang)
                <th>
                  {{$lang}}
                </th>
                @endforeach
              </tr>
            </thead>
            <tbody>
              @foreach($list as $key1 => $item)
              <tr>
                <td>
                  <input type="text" name="keys[]" value="{{$key1}}" class="form-control">
                </td>
                @foreach($langlist as $key2 => $lang)
                <td>
                  <input type="text" name="{{$key2}}[]" value="{{$item[$key2]}}" class="form-control">
                </td>
                @endforeach
              </tr>
              @endforeach
              <tr style="display: none" class="shablon">
                <td>
                  <input type="text" name="keys[]" value="" class="form-control">
                </td>
                @foreach($langlist as $key2 => $lang)
                <td>
                  <input type="text" name="{{$key2}}[]" class="form-control">
                </td>
                @endforeach
              </tr>
            </tbody>
          </table>
        </form>
      </div>
        
    </div>
  </div>
</div>
@endsection

@push('scripts')
    <script type="text/javascript">
      $('.copy').on('click',function(){
        console.log('asa');
        $tr = $('tr.shablon')
        var $clone = $tr.clone();
        $clone.find(':text').val('');
        $clone.removeClass('shablon').show();
        $tr.after($clone);
      })
    </script>
@endpush
