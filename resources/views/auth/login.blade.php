@extends('front.layout.main')

@section('content')

@php
  $m = Config::get('matrix.get');
    $p = Session::get('programm', 1);
@endphp

<div class="cabinet_main_container authorization">
  <div class="header">
    <h2 class="title">{{ __($m[$p][1]) }}</h2>
  </div>
  <div class="body">
    <div class="auth_cont">

    {{ Form::open(array('route' => 'partner.loginPartner')) }}
        <p class="text">{{__('Введите ваши регистрационные данные для входа в личный кабинет')}}</p>
        @if(Session::has('message'))
          <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
        @endif

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        {{ Form::text('username', '', ['class'=>'form-control', 'placeholder'=>__('Логин')]) }}
        {{ Form::password('password', ['class'=>'form-control', 'placeholder'=>__('Пароль')]) }}

        <div class="link_cont"><a href="/partner/reset" class="link">{{__('восстановить пароль')}}</a></div>
        <p class="error-message" style="color:red; font-weight: bold; display: none;">ываываываыва</p>
        <input type="submit" value="{{__('авторизоваться')}}" class="button">
      {{ Form::close() }}
        <input type="file" style="display: none;">
    </div>
    </div>

        </div>
@endsection


@section('scripts')
  <script type="text/javascript">
    $(document).ready(function(){
      $('.auth_cont form').on('submit', function(e){
        e.preventDefault();
        $('error-message').html('');
          $('.error-message').fadeOut();
        var name = $( "input[name='username']").val();
        var password = $( "input[name='password']").val();
        if(name == ''){
          $('.error-message').html('Введите логин');
          $('.error-message').fadeIn();
          return false;
        }        
        if(password == ''){
          $('.error-message').html('Введите пароль');
          $('.error-message').fadeIn();
          return false;
        }

        $.ajax({
          method: "POST",
          headers: {          
            Accept: "text/plain; charset=utf-8",         
          },
          url: "/auth-validate",
          data:{
            '_token': '{{ csrf_token() }}',
            'name' : name,
            'password' : password,
          },
        }).done(function( msg ) {
          if(msg.error == 1){
            $('.error-message').html(msg.message);
            $('.error-message').fadeIn();
          }else{
              e.target.submit();
          }
        });

      })
    })
  </script>
@endsection