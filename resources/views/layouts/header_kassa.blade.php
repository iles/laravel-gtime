    <header class="app-header navbar">
      <button class="navbar-toggler sidebar-toggler d-lg-none mr-auto" type="button" data-toggle="sidebar-show">
        <span class="navbar-toggler-icon"></span>
      </button>
      <a class="navbar-brand" href="#">
      	<span class="navbar-brand-full">GTIME</span>
      	<span class="navbar-brand-minimized">GTIME</span>
      </a>




      <ul class="nav navbar-nav ml-auto" style="margin-right: 30px;">
        <li class="nav-item d-md-down-none">

        @php
          $m = Config::get('matrix.get');
          $p = Session::get('programm', 1);
          if($p == null){
            Session::put('programm', 1);
            $p = Session::get('programm', 1);
          }
        @endphp


            <div class="btn-group dropleft" id="mtype">
              <button class="btn btn-warning btn-lg dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                {{ $m[$p][1] }}
              </button>

              <div class="dropdown-menu">
                      @foreach ($m as $key => $matrix)
                        @if ($key == $p)
                             <a class="dropdown-item active" href="" data-id="{{ $key }}">{{ $matrix[1] }}</a>
                             @continue
                        @endif
                              <a class="dropdown-item" href="" data-id="{{ $key }}">{{ $matrix[1] }}</a>
                      @endforeach
              </div>

              <form action="/change" id="change-programm-form" style="display: none;" method="post">
                @csrf
                <input type="text" hidden name="id" id="change-programm-id">
              </form>

            </div>

        </li>
      </ul>
    </header>

