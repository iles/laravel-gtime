    <header class="app-header navbar">
      <button class="navbar-toggler sidebar-toggler d-lg-none mr-auto" type="button" data-toggle="sidebar-show">
        <span class="navbar-toggler-icon"></span>
      </button>
      <a class="navbar-brand" href="#">
      	<span class="navbar-brand-full">GTIME</span>
      	<span class="navbar-brand-minimized">GTIME</span>
      </a>


      <ul class="nav navbar-nav d-md-down-none">
        <li class="nav-item dropdown px-3" class="dropdown-menu">
          <div class="dropdown">
            <a class="btn dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Пин коды
            </a>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
              <a class="dropdown-item" href="/admin/pins">Заявки на пин коды</a>
              <a class="dropdown-item" href="/pincodes">Переходные пин коды</a>
              <a class="dropdown-item" href="/pincodes/cancel">Анулирование</a>
            </div>
          </div>

        </li>


        <li class="nav-item px-3">
          <a class="nav-link" href="/admin/partners/">Партнеры</a>
        </li>

        <li class="nav-item px-3">
          <a class="nav-link" href="{{ route('bonusrequest.index') }}">Вознаграждения</a>
        </li>

        <li class="nav-item px-3">
          <a class="nav-link" href="{{ route('partners.finished') }}">Завершившие</a>
        </li>

        <li class="nav-item px-3">
          <a class="nav-link" href="/admin/splits">Разделения</a>
        </li>
      </ul>


      <ul class="nav navbar-nav ml-auto" style="margin-right: 30px;">
        <li class="nav-item d-md-down-none">

        @php
          $m = Config::get('matrix.get');
          $p = Session::get('programm', 1);
          if($p == null){
            Session::put('programm', 1);
            $p = Session::get('programm', 1);
          }
        @endphp


            <div class="btn-group dropleft" id="mtype">
              <button class="btn btn-warning btn-lg dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                {{ $m[$p][1] }}
              </button>

              <div class="dropdown-menu">
                <a class="dropdown-item active" href="" data-id="1">Старт</a>
                <a class="dropdown-item" href="" data-id="3">Основная</a>
                <a class="dropdown-item" href="" data-id="4">Накопительная</a>
                <a class="dropdown-item" href="" data-id="5">Накопительная +</a>
                <a class="dropdown-item" href="" data-id="6">Vip</a>
                <a class="dropdown-item" href="" data-id="7">Fast</a>
                <a class="dropdown-item" href="" data-id="8">Bonus</a>
              </div>

              <form action="/change" id="change-programm-form" style="display: none;" method="post">
                @csrf
                <input type="text" hidden name="id" id="change-programm-id">
              </form>

            </div>

        </li>
      </ul>
    </header>

