<div class="card">
   <form <form method="post" action="{{ action('PartnerController@statusupdate') }}" id="create_pin_form" class="form-horizontal" accept-charset="UTF-8"> 
  <div class="card-header">
    <strong>{{$model->uname}}</strong></div>
    <div class="card-body">
        @csrf

        <p>{{$model->pin}}</p>
        <p>{{$model->created_at}}</p>
        <input type="text" name="id" value="{{$model->id}}" hidden />
        <input type="text" name="status" value="1" hidden />
        <textarea name="cancel_reason">
        	
        </textarea>

        
    </div>
        
    <div class="card-footer">
      <button class="btn btn-sm btn-primary" type="submit">
      <i class="fa fa-dot-circle-o" type="submit"></i> Одобрить</button>
      <button class="btn btn-sm btn-danger"  data-dismiss="modal" aria-label="Close" type="reset">
      <i class="fa fa-ban"></i> Отклонить</button>
    </div>
    </form>
</div>