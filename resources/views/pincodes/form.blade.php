<div class="card">
  @if(isset($model))
      {{ Form::model($model, ['route' => ['bonusrequest.update', $model->id], 'method' => 'put']) }}
  @else
      {{ Form::open(['route' => 'bonusrequest.store', 'method'=>'post']) }}
  @endif


  <div class="card-header">
    <strong>
      @if(isset($model))
        Редактировать заявку на вознаграждение
      @else
        Создать заявку на вознаграждение
      @endif
    </strong></div>
    <div class="card-body">
        @csrf

        <div class="row">
          <div class="col-md-6">

            <div class="form-group row">            
              {{ Form::label('uname', 'Логин:', array('class' => 'col-md-3 col-form-label')) }}
              <div class="col-md-9">
                {{ Form::text('uname', Input::old('uname'), array('class' => 'form-control') ,  array('class' => 'form-control') ) }}
              </div>
            </div>              

            <div class="form-group row">            
              {{ Form::label('name', 'ФИО:', array('class' => 'col-md-3 col-form-label')) }}
              <div class="col-md-9">
                {{ Form::text('name', Input::old('name'),  array('class' => 'form-control') ) }}
              </div>
            </div>        

            <div class="form-group row">            
              {{ Form::label('exist_pin', 'Существующий пин-код:', array('class' => 'col-md-3 col-form-label')) }}
              <div class="col-md-9">
                {{ Form::text('exist_pin', Input::old('exist_pin'),  array('class' => 'form-control') ) }}
              </div>
            </div>

            <div class="form-group row">            
              {{ Form::label('registration_date', 'Дата регистрации логина:', array('class' => 'col-md-3 col-form-label')) }}
              <div class="col-md-9">
                {{ Form::text('registration_date', Input::old('registration_date'),  array('class' => 'form-control') ) }}
              </div>
            </div>        

            <div class="form-group row">            
              {{ Form::label('payment_date', 'Дата оплаты за текущий логин:', array('class' => 'col-md-3 col-form-label')) }}
              <div class="col-md-9">
                {{ Form::text('payment_date', Input::old('payment_date'),  array('class' => 'form-control') ) }}
              </div>
            </div>       

             <div class="form-group row">            
              {{ Form::label('fact_date', 'Дата фактической отправки вознаграждения:', array('class' => 'col-md-3 col-form-label')) }}
              <div class="col-md-9">
                {{ Form::text('fact_date', Input::old('fact_date'),  array('class' => 'form-control') ) }}
              </div>
            </div>         

            <div class="form-group row">            
              {{ Form::label('bank', 'Банк отправителя:', array('class' => 'col-md-3 col-form-label')) }}
              <div class="col-md-9">
                {{ Form::text('bank', Input::old('bank'),  array('class' => 'form-control') ) }}
              </div>
            </div>   

          </div>  

          <div class="col-md-6">
            
            <div class="form-group row">            
          {{ Form::label('checkout_date', 'Дата выдачи продукции:', array('class' => 'col-md-3 col-form-label')) }}
          <div class="col-md-9">
            {{ Form::text('checkout_date', Input::old('checkout_date'),  array('class' => 'form-control') ) }}
          </div>
        </div>        


        <div class="form-group row">            
          {{ Form::label('program', 'Программа:', array('class' => 'col-md-3 col-form-label')) }}
          <div class="col-md-9">
            {{ Form::select('program', $progs, Input::old('program'), array('class' => 'form-control')  ) }}
          </div>
        </div>   

        <div class="form-group row">            
          {{ Form::label('summ', 'Сумма:', array('class' => 'col-md-3 col-form-label')) }}
          <div class="col-md-9">
            {{ Form::text('summ', Input::old('summ'),  array('class' => 'form-control') ) }}
          </div>
        </div>        

        <div class="form-group row">            
          {{ Form::label('iin', 'ИИН:', array('class' => 'col-md-3 col-form-label')) }}
          <div class="col-md-9">
            {{ Form::text('iin', Input::old('iin'),  array('class' => 'form-control') ) }}
          </div>
        </div>        

        <div class="form-group row">            
          {{ Form::label('country', 'Страна:', array('class' => 'col-md-3 col-form-label')) }}
          <div class="col-md-9">
            {{ Form::text('country', Input::old('country'),  array('class' => 'form-control') ) }}
          </div>
        </div>        

        <div class="form-group row">            
          {{ Form::label('city', 'Город:', array('class' => 'col-md-3 col-form-label')) }}
          <div class="col-md-9">
            {{ Form::text('city', Input::old('city'),  array('class' => 'form-control') ) }}
          </div>
        </div>

        <div class="form-group row">
          {{ Form::label('bill_number', 'Номер счета:', array('class' => 'col-md-3 col-form-label')) }}
          <div class="col-md-9">
            {{ Form::text('bill_number', Input::old('bill_number'),  array('class' => 'form-control') ) }}
          </div>
        </div>

        <div class="form-group row">
          {{ Form::label('cancel_reason', 'Причина отказа:', array('class' => 'col-md-3 col-form-label')) }}
          <div class="col-md-9">
            {{ Form::text('cancel_reason', Input::old('cancel_reason'),  array('class' => 'form-control') ) }}
            <div class="invalid-feedback">Укажите причину отказа</div>
          </div>
        </div>

          </div>
        </div>
       
    </div>
        
    <div class="card-footer">
      <button class="btn btn-sm btn-primary" type="submit">
        <i class="fa fa-dot-circle-o"></i> Сохранить
      </button>     


      <a href="#" id="stat2" class="manage-btn btn btn-sm btn-danger pull-right">
        <i class="fa fa-ban"></i>  Отклонить


      </a>

      <a href="#" id="stat1" class="manage-btn btn btn-sm btn-success pull-right">

        <i class="fa fa-dot-circle-o"></i> Одобрить


      </a>
  
    </div>
   {{ Form::close() }}
          <form id="form2" style="display: none" method="POST" action="{{ route('bonusrequest.bookaprove', ['id'=>$model->id, 's'=>2]) }}"> 
            @csrf
          </form>
          <form id="form1" style="display: none" method="POST" action="{{ route('bonusrequest.bookaprove', ['id'=>$model->id, 's'=>1]) }}"> 
            @csrf
          </form> 
</div>

  <script type="text/javascript">

      $('.manage-btn').on('click', function(e){
        e.preventDefault();
        if( $(this).attr('id') == 'stat2'){
          if( $('#cancel_reason').val() == '' ){
            $('.invalid-feedback').fadeIn();
            return false;
          } else {
            $('#form2').submit();
          }
        } else {
            $('#form1').submit();          
        }

      })

  </script>
