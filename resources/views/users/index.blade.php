@extends('layouts.app')


@section('breadcrumbs')
  <li class="breadcrumb-item active">Пользователи</li>
@endsection

@section('content')

<style type="text/css">
	.dis{display: none;	}
	.dis.active{display: flex;	}
</style>

				@if(Session::has('message'))
                  <div class="card-header">
						<p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
                   </div>
				@endif
asd
{!! $grid !!}
@endsection


@section('scripts')
  <script type="text/javascript">
			function checkPasswords( event ) {
				var x = document.getElementsByName('password')[0].value,
			        y = document.getElementsByName('password2')[0].value;

			    

			    if( $('#change-password').hasClass('active') ){
				    if(x == "" || y == "" ){
						event.preventDefault();
				    	alert('Введите пароль и подтверждение');
				    }			    	
			    }

			    if (x === y) {
					return;
			    }
			 	
				event.preventDefault();
			    alert('Пароли не совпадают!');
			};

			function toglePasswords(e) {
			   	e.preventDefault();
				$('.dis').toggleClass('active');	
			}
  </script>
@endsection