@extends('layouts.app')

@section('content')
    <section class="content-header">
      <h1>
        Пользователи
        <small>it all starts here</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Examples</a></li>
        <li class="active">Blank page</li>
      </ol>
    </section>

    <section class="content">


      <!-- Default box -->
      <div class="box">
       <div class="box-body">

          <table id="example2" class="table table-bordered table-hover">
            <thead>
              <tr>
                <th>id</th>
                <th>Имя</th>
                <th>Email</th>
                <th>создан</th>
                <th>хеш</th>
              </tr>
            </thead>
            
            <tbody>
              @foreach ($users as $user)
                <tr>
                  <td>{{ $user->id }}</td> 
                  <td>{{ $user->name }}</td> 
                  <td>{{ $user->email }}</td> 
                  <td>{{ $user->created_at }}</td> 
                  <td>{{ $user->password }}</td> 
                </tr>
              @endforeach
            </tbody>
          </table>
           {{ $users->links() }}
          </div>
        </div>
        <!-- /.box-body -->
      <!-- /.box -->




    </section>
@endsection