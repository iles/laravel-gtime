<div class="card">
   <form method="post" action="{{ action('TransferController@aprove') }}" id="create_pin_form" class="form-horizontal" accept-charset="UTF-8"> 
  <div class="card-header">
    <strong>{{ $transfer->login }}</strong></div>
    <div class="card-body">
        @csrf

      
        <div class="form-group row">
          <label class="col-md-3 col-form-label" for="hf-password">Логин</label>
          <div class="col-md-9">           
            <input class="form-control" name="skype" value="{{ $transfer->login }}" type="text">
            <input hidden name="id" value="{{ $transfer->id }}" type="text">
          </div>
        </div>                  

        <div class="form-group row">
          <label class="col-md-3 col-form-label" for="hf-password">ФИО</label>
          <div class="col-md-9">           
            <input class="form-control" name="skype" value="{{ $transfer->fullname }}" type="text">
          </div>
        </div>      

        <div class="form-group row">
          <label class="col-md-3 col-form-label" for="hf-password">ФИО получателя</label>
          <div class="col-md-9">           
            <input class="form-control" name="skype" value="{{ $transfer->fullname2 }}" type="text">
          </div>
        </div>            

     

        <div class="form-group row">
          <label class="col-md-3 col-form-label" for="hf-password">Документ</label>
          <div class="col-md-9">           
              @if($transfer->image != 0)
                  @foreach( explode(';', $transfer->image) as $image)
                    <a data-fancybox="gallery" href="/uploads/transfers/{{$image  }}"><img src="/uploads/transfers/{{$image }}" style="width: 80px;"></a>
                  @endforeach
              @endif
          </div>
        </div>      
        
    </div>
        
    <div class="card-footer">


      <button class="btn btn-sm btn-primary" type="submit">
      <i class="fa fa-dot-circle-o" type="submit"></i> Одобрить
      </button>
        <button class="btn btn-sm btn-danger" onclick="reject(); return false;">      
        <i class="fa fa-ban"></i> Отклонить</button>

    </div>

    </form>
</div>