
<style type="text/css">
  #bootstrap_modal .modal-dialog{
    max-width: 800px;
  }
</style>

@php
  $m = Config::get('matrix.get');
  $p = Session::get('programm', 1);
@endphp



<div class="card">

   <form method="post" action="{{ action('SwapController@make') }}" id="swap_form" class="form-horizontal" accept-charset="UTF-8"> 


    <div class="card-body">
        @csrf



        <div class="form-group row">
          <label class="col-md-3 col-form-label">Программа</label>
          <div class="col-md-9">
            <select name="programm" class="form-control">
              <optgroup>
                      @foreach ($m as $key => $matrix)
                             <option value="{{ $key }}" {{ $key == $swap->program_id ? 'selected' : ''  }}>{{ $matrix[1] }}</option>
                      @endforeach
              </optgroup>
            </select>
          </div>
        </div>     
    

        <div class="form-group row">            
          {{ Form::label('login', 'Логин', array('class' => 'col-md-3 col-form-label')) }}
          <div class="col-md-7">
            {{ Form::text('login', $swap->login,  array('class' => 'form-control', 'id' => 'login-input-1') ) }}
            <span class="valid-feedback" role="alert"><strong id="message-input-12"></strong></span>
          </div>
          
          <div class="col-md-2">           
            <button class="btn btn-sm btn-primary pull-right" onclick="check(1); return false;"> <i class="fa fa-plus"></i> Проверить</button>  
          </div>

        </div>      

        <div class="form-group row">            
          {{ Form::label('login', 'Заменить на', array('class' => 'col-md-3 col-form-label')) }}
          <div class="col-md-7">
            {{ Form::text('login', $swap->login2,  array('class' => 'form-control', 'id' => 'login-input-2') ) }}
            <span class="valid-feedback" role="alert"><strong id="message-input-22"></strong></span>
          </div>
          <div class="col-md-2">           
            <button class="btn btn-sm btn-primary pull-right" onclick="check(2); return false;"> <i class="fa fa-plus"></i> Проверить</button>  
          </div>
        </div>          


        <div class="form-group row">
          <label class="col-md-3 col-form-label" for="hf-password">Фото квитанции</label>
          <div class="col-md-9">           
              @if($swap->image != 0)
                  @foreach( explode(';', $swap->image) as $image)
                    <a data-fancybox="gallery" href="/uploads/applications/{{$image  }}"><img src="/uploads/applications/{{$image }}" style="width: 80px;"></a>
                  @endforeach
              @endif
          </div>
        </div>      

      

        <input type="text" name="id" value="{{$swap->id}}" hidden/>     
        <input type="text" name="status" id="status_id" value="1" hidden/>
        <textarea name="cancel_reason"  class="form-control" id="cancel_reason">{{$swap->cancel_reason}}</textarea>        
        <div class="invalid-feedback">Укажите причину отказа</div>

       
    </div>
        
    <div class="card-footer">


      @if($swap->type == 2)
        @if($p == 3 || $p == 6)
          @if(!$swap->sec_check)
            Заявка не одобрена сб
          @else
                <button class="btn btn-sm btn-primary" type="submit">
            <i class="fa fa-dot-circle-o" type="submit"></i> Одобрить</button>
          @endif
        @else
              <button class="btn btn-sm btn-primary" type="submit">
        <i class="fa fa-dot-circle-o" type="submit"></i> Одобрить</button>
        @endif
      @else  
            <button class="btn btn-sm btn-primary" type="submit">
      <i class="fa fa-dot-circle-o" type="submit"></i> Одобрить</button>
      @endif
      <button class="btn btn-sm btn-danger" onclick="reject(); return false;">
      <i class="fa fa-ban"></i> Отклонить </button>
    </div>


    </form>
   {{ Form::close() }}

</div>

  


