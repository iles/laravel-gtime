
<style type="text/css">
  #bootstrap_modal .modal-dialog{
    max-width: 800px;
  }
</style>

@php
  $m = Config::get('matrix.get');
  $p = Session::get('programm', 1);
@endphp



<div class="card">

   <form method="post" action="{{ action('SwapController@make') }}" id="swap_form" class="form-horizontal" accept-charset="UTF-8"> 


    <div class="card-body">
        @csrf



    

        <div class="form-group row">            
          {{ Form::label('login', 'Логин', array('class' => 'col-md-3 col-form-label')) }}
          <div class="col-md-9">
            {{ Form::text('login', $swap->login,  array('class' => 'form-control', 'id' => 'login-input-1') ) }}
            <span class="valid-feedback" role="alert"><strong id="message-input-12"></strong></span>
          </div>
        </div>      

        <div class="form-group row">            
          {{ Form::label('login', 'Заменить на', array('class' => 'col-md-3 col-form-label')) }}
          <div class="col-md-7">
            <div class="row">
              <div class=col-md-4>
                Логин:<br/>
                <br>
                ФИО:<br/>                
                <br>
                Телефон:<br/>                
                <br>
                Страна/Город:<br/>
              </div>
              <div class="col-md-6">
                {{$swap->login2}}        <br/>     
                <br>   
                {{$user->familiya}}      <br/>                        
                <br>   
                {{$user->pol}}      <br/>                          
                <br>   
                {{$user->imya}}      <br/>          
              </div>
            </div>
          </div>
        </div>          


    

      

        <input type="text" name="id" value="{{$swap->id}}" hidden/>     
        <input type="text" name="status" id="status_id" value="1" hidden/>
        <textarea name="cancel_reason"  class="form-control" id="cancel_reason">{{$swap->cancel_reason}}</textarea>        
        <div class="invalid-feedback">Укажите причину отказа</div>

       
    </div>
        
    <div class="card-footer">


      @if($swap->type == 2)
        @if($p == 3 || $p == 6)
          @if(!$swap->sec_check)
            Заявка не одобрена сб
          @else
                <button class="btn btn-sm btn-primary" type="submit">
            <i class="fa fa-dot-circle-o" type="submit"></i> Одобрить</button>
          @endif
        @else
              <button class="btn btn-sm btn-primary" type="submit">
        <i class="fa fa-dot-circle-o" type="submit"></i> Одобрить</button>
        @endif
      @else  
            <button class="btn btn-sm btn-primary" type="submit">
      <i class="fa fa-dot-circle-o" type="submit"></i> Одобрить</button>
      @endif
      <button class="btn btn-sm btn-danger" onclick="reject(); return false;">
      <i class="fa fa-ban"></i> Отклонить </button>
    </div>


    </form>
   {{ Form::close() }}

</div>

  


