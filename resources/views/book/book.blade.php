@extends('layouts.app')


@section('content')

	<div class="row">
		<div class="col-md-12">        		
	          @if(Session::has('message'))
	                  <div class="card-header">
	            			<p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
	                   </div>
	          @endif
        	</div>
		</div>
		<div class="row">

				 <ul class="nav navbar-nav ml-auto" style="margin-right: 30px;">
        <li class="nav-item d-md-down-none">

        @php
          $m = Config::get('matrix.get');
          $p = Session::get('programm', 1);
          if($p == null){
            Session::put('programm', 1);
            $p = Session::get('programm', 1);
          }
        @endphp


            <div class="btn-group dropleft" id="mtype">
              <button class="btn btn-warning btn-lg dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                {{ $m[$p][1] }}
              </button>

              <div class="dropdown-menu">
                      @foreach ($m as $key => $matrix)
                        @if ($key == $p)
                             <a class="dropdown-item active" href="" data-id="{{ $key }}">{{ $matrix[1] }}</a>
                             @continue
                        @endif
                              <a class="dropdown-item" href="" data-id="{{ $key }}">{{ $matrix[1] }}</a>
                      @endforeach
              </div>

              <form action="/change" id="change-programm-form" style="display: none;" method="post">
                @csrf
                <input type="text" hidden name="id" id="change-programm-id">
              </form>

            </div>

        </li>
      </ul>
      
			<div class="col-md-12" style="text-align: right; padding: 6px 48px 35px 0;">        		
				<a href="/admin/bookkeeping" style="color: #2575ce; font-weight: bold; margin-right: 15px;">Вознаграждения</a>
				<a href="/admin/bookkeeping/warehouse" style="color: #00a195; font-weight: bold; margin-right: 15px;">Склад</a>
				<a href="/admin/bookkeeping/credit" style="color: #00a195; font-weight: bold;">Поступления</a>
			</div>
		</div>
<style type="text/css">
	#bootstrap_modal .modal-dialog{
		max-width: 1200px;
	}
</style>
	{!! $grid !!}

@endsection

