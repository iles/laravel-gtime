@extends('layouts.app')


@section('content')
	@section('content')
	<div class="row">
		<div class="col-md-12">        		
	          @if(Session::has('message'))
	                  <div class="card-header">
	            			<p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
	                   </div>
	          @endif
        	</div>
		</div>
		<div class="row">
			<div class="col-md-12" style="text-align: right; padding: 6px 48px 35px 0;">        		
				<a href="/admin/bookkeeping" style="color: #00a195; font-weight: bold;  margin-right: 15px;">Вознаграждения</a>
				<a href="/admin/bookkeeping/warehouse" style="color: #00a195; font-weight: bold; margin-right: 15px;">Склад</a>
				<a href="/admin/bookkeeping/credit"  style="color: #2575ce; font-weight: bold; margin-right: 15px;" >Поступления</a>
			</div>
		</div>
<style type="text/css">
	#bootstrap_modal .modal-dialog{
		max-width: 1200px;
	}
</style>
	{!! $grid !!}
	@endsection
@endsection

