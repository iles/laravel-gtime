<div class="card">
  <?php if(isset($model)): ?>
      <?php echo e(Form::model($model, ['route' => ['bonusrequest.update', Input::old('id')], 'method' => 'put','id'=>'mForm'])); ?>

  <?php else: ?>
      <?php echo e(Form::open(['route' => 'bonusrequest.store', 'method'=>'post','id'=>'mForm'])); ?>

  <?php endif; ?>



  <div class="card-header">
    <strong>
      <?php if(isset($model)): ?>
        Редактировать заявку на вознаграждение
      <?php else: ?>
        Создать заявку на вознаграждение
      <?php endif; ?>
    </strong></div>
    <div class="card-body">
        <?php echo csrf_field(); ?>

        <div class="row">
          <div class="col-md-6">

 <div class="form-group row">            
            <?php echo e(Form::label('program_id', 'Программа:', array('class' => 'col-md-3 col-form-label'))); ?>

          <div class="col-md-9">
              <?php echo e(Form::select('program_id', $progs, Input::old('program_id'), array('class' => 'form-control', 'required'=>'required')  )); ?>

          </div>
        </div>              

        <div class="form-group row">            
          <?php echo e(Form::label('uname', 'Логин:', array('class' => 'col-md-3 col-form-label'))); ?>

          <div class="col-md-9">
            <?php echo e(Form::text('uname', Input::old('uname'),  array('class' => 'form-control', 'required'=>'required') )); ?>

          </div>
        </div>        

        <div class="form-group row">            
          <?php echo e(Form::label('summ', 'Сумма:', array('class' => 'col-md-3 col-form-label'))); ?>

          <div class="col-md-9">
            <?php echo e(Form::number('summ', Input::old('summ'),  array('class' => 'form-control', 'required'=>'required') )); ?>

          </div>
        </div>

        <div class="form-group row">            
          <?php echo e(Form::label('firstname', 'Имя:', array('class' => 'col-md-3 col-form-label'))); ?>

          <div class="col-md-9">
            <?php echo e(Form::text('firstname', Input::old('firstname'),  array('class' => 'form-control', 'required'=>'required') )); ?>

          </div>
        </div>        

        <div class="form-group row">            
          <?php echo e(Form::label('firstname', 'Фамилия:', array('class' => 'col-md-3 col-form-label'))); ?>

          <div class="col-md-9">
            <?php echo e(Form::text('lastname', Input::old('lastname'),  array('class' => 'form-control', 'required'=>'required') )); ?>

          </div>
        </div>       

         <div class="form-group row">            
          <?php echo e(Form::label('patronic', 'Отчество:', array('class' => 'col-md-3 col-form-label'))); ?>

          <div class="col-md-9">
            <?php echo e(Form::text('patronic', Input::old('patronic'),  array('class' => 'form-control', 'required'=>'required') )); ?>

          </div>
        </div>         

        <div class="form-group row">            
          <?php echo e(Form::label('iin', 'ИИН:', array('class' => 'col-md-3 col-form-label'))); ?>

          <div class="col-md-9">
            <?php echo e(Form::number('iin',  Input::old('iin'),  array('class' => 'form-control', 'required'=>'required') )); ?>

          </div>
        </div>        

        <div class="form-group row">            
          <?php echo e(Form::label('bill_num', 'Номер счета :', array('class' => 'col-md-3 col-form-label'))); ?>

          <div class="col-md-9">
            <?php echo e(Form::text('bill_num', Input::old('bill_num'),  array('class' => 'form-control', 'required'=>'required') )); ?>

          </div>
        </div>           

         <div class="form-group row">            
          <?php echo e(Form::label('bank_name', 'Наименование банка :', array('class' => 'col-md-3 col-form-label'))); ?>

          <div class="col-md-9">
            <?php echo e(Form::text('bank_name', Input::old('bank_name'),  array('class' => 'form-control', 'required'=>'required') )); ?>

          </div>
        </div>            

     
 

          </div>  

          <div class="col-md-6">
            
   <div class="form-group row">            
          <?php echo e(Form::label('BIK', 'BIK :', array('class' => 'col-md-3 col-form-label'))); ?>

          <div class="col-md-9">
            <?php echo e(Form::text('BIK', Input::old('BIK'),  array('class' => 'form-control', 'required'=>'required') )); ?>

          </div>
        </div>          

        <div class="form-group row">            
          <?php echo e(Form::label('card_number', 'Номер карты :', array('class' => 'col-md-3 col-form-label'))); ?>

          <div class="col-md-9">
            <?php echo e(Form::text('card_number', Input::old('card_number'),  array('class' => 'form-control') )); ?>

          </div>
        </div>        

        <div class="form-group row">            
          <?php echo e(Form::label('pens', 'Пенсионер :', array('class' => 'col-md-3 col-form-label'))); ?>

          <div class="col-md-9">
            <?php echo e(Form::text('pens', Input::old('pens'),  array('class' => 'form-control') )); ?>

          </div>
        </div>             


		<?php if(isset($model)): ?>
        <?php if($model->inv): ?>

        <div class="form-group row">            
          <?php echo e(Form::label('inv', 'Инвалидность :', array('class' => 'col-md-3 col-form-label'))); ?>

          <div class="col-md-9">
            <?php echo e(Form::text('inv', Input::old('inv'),  array('class' => 'form-control') )); ?>

          </div>
        </div>      


        <div class="form-group row">            
          <?php echo e(Form::label('inv_group', 'Группа инвалидности :', array('class' => 'col-md-3 col-form-label'))); ?>

          <div class="col-md-9">
            <?php echo e(Form::text('inv_group', Input::old('inv_group'),  array('class' => 'form-control') )); ?>

          </div>
        </div>          

         <div class="form-group row">            
          <?php echo e(Form::label('inv_start', 'Срок инвалидности :', array('class' => 'col-md-3 col-form-label'))); ?>

          <div class="col-md-9">
            <?php echo e(Form::text('inv_start', Input::old('inv_start'),  array('class' => 'form-control') )); ?>

            <?php echo e(Form::text('inv_end', Input::old('inv_end'),  array('class' => 'form-control') )); ?>

          </div>
        </div>            

        <div class="form-group row">            
          <?php echo e(Form::label('inv_timeless', 'Без срока:', array('class' => 'col-md-3 col-form-label'))); ?>

          <div class="col-md-9">
            <?php echo e(Form::text('inv_timeless', Input::old('inv_timeless'),  array('class' => 'form-control') )); ?>

          </div>
        </div>  
                 
        <?php endif; ?>
        <?php endif; ?>
		
        <div class="form-group row">            
          <?php echo e(Form::label('notorios', 'По нотариальной доверенности:', array('class' => 'col-md-3 col-form-label'))); ?>

          <div class="col-md-9">
            <?php echo e(Form::text('notorios', Input::old('notorios'),  array('class' => 'form-control') )); ?>

          </div>
        </div>           

         <div class="form-group row">            
          <?php echo e(Form::label('adress', 'Адрес:', array('class' => 'col-md-3 col-form-label'))); ?>

          <div class="col-md-9">
            <?php echo e(Form::text('adress', Input::old('adress'),  array('class' => 'form-control') )); ?>

          </div>
        </div>    

         <div class="form-group row">            
          <?php echo e(Form::label('fact_adress', 'Фактический адрес:', array('class' => 'col-md-3 col-form-label'))); ?>

          <div class="col-md-9">
            <?php echo e(Form::text('fact_adress', Input::old('fact_adress'),  array('class' => 'form-control') )); ?>

          </div>
        </div>    

         <div class="form-group row">            
          <?php echo e(Form::label('country', 'Страна/город:', array('class' => 'col-md-3 col-form-label'))); ?>

          <div class="col-md-9">
            <?php echo e(Form::text('country', Input::old('country'),  array('class' => 'form-control') )); ?>

          </div>
        </div>    

 

        <div class="form-group row">
          <?php echo e(Form::label('cancel_reason', 'Причина отказа:', array('class' => 'col-md-3 col-form-label'))); ?>

          <div class="col-md-9">
            <?php echo e(Form::text('cancel_reason', Input::old('cancel_reason'),  array('class' => 'form-control') )); ?>

            <div class="invalid-feedback">Укажите причину отказа</div>
          </div>
        </div>

          </div>
        </div>
       
    </div>
        
    <div class="card-footer">
  
      <a href="#" id="stat1" class="manage-btn btn btn-sm btn-success">
        <i class="fa fa-dot-circle-o"></i> Одобрить
      </a>

      <a href="#" id="stat2" class="manage-btn btn btn-sm btn-danger">
        <i class="fa fa-ban"></i>  Отклонить
      </a>


  
    </div>
   <?php echo e(Form::close()); ?>

   
		<?php if(isset($model)): ?>
          <form id="form2" style="display: none" method="POST" action="<?php echo e(route('bonusrequest.bookaprove', ['id'=>$model->id, 's'=>5])); ?>"> 
            <?php echo csrf_field(); ?>
            <input type="text" name="cancel_reason" class="cr">
          </form>
          <form id="form1" style="display: none" method="POST" action="<?php echo e(route('bonusrequest.bookaprove', ['id'=>$model->id, 's'=>4])); ?>"> 
            <input type="text" name="cancel_reason" class="cr">
            <?php echo csrf_field(); ?>
          </form> 
        <?php endif; ?>
</div>

  <script type="text/javascript">

    $('#cancel_reason').on('change', function(){
      $('.cr').val( $(this).val() );
    })

      $('.manage-btn').on('click', function(e){
        e.preventDefault();
        if( $(this).attr('id') == 'stat2'){
          if( $('#cancel_reason').val() == '' ){
            $('.invalid-feedback').fadeIn();
            return false;
          } else {
            $('#form2').submit();
          }
        } else {
          $('#mForm').submit();
          // $('#form1').submit();          
        }

      })

  </script>
<style type="text/css">
  .modal-dialog{
    max-width: 1500px;
  }
</style>