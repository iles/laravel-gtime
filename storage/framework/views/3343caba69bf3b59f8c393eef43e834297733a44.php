<?php 
	use Illuminate\Support\Facades\Session;
?>




<?php $__env->startSection('breadcrumbs'); ?>
	<li class="breadcrumb-item active">Авто пин-коды </li>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>


				<?php if(Session::has('message')): ?>
					<div class="row">
				<div class="card-body">
						<div class="col-md-12 col-xs-12 col-sm-12">
						<p style="background-color: #dff0d8; border-color: #d6e9c6; color: #3c763d; padding: 15px; margin-bottom: 20px; border-radius: 4px;" class="alert <?php echo e(Session::get('alert-class', 'alert-info')); ?>"><?php echo e(Session::get('message')); ?></p>

                   	</div>
					</div>
				</div>
				<?php endif; ?>

<div class="row">
	<div class="col-lg-12">
		<div class="card">
			<div class="card-header">
				<h2>Авто пин коды</h2>
			</div>

			<div class="card-body">
				<table class="table table-responsive-sm table-bordered table-striped table-sm">
					<thead>
						<tr>
							<th>Пин код</th>
							<th>Активный</th>
							<th>Логин</th>
							<th>Дата регистрации</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						<?php $__currentLoopData = $pins; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $pin): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
						<tr>
							<td><?php echo e($pin->pin); ?></td>
							<td>
								<?php if($pin->status_id == 0): ?>
									<span class="badge badge-success">Свободен</span>
								<?php else: ?>
									<span class="badge badge-secondary">Занят</span>
								<?php endif; ?>
							</td>
							<td><?php echo e($pin->username); ?></td>
							<td>
								<?php if($pin->status_id == 3): ?>
									<?php echo e($pin->updated_at); ?>

								<?php endif; ?>
							</td>
							<td>
								<?php if($pin->status_id == 3): ?>
									<a class="btn btn-sm btn-ghost-success free" data-id="<?php echo e($pin->id); ?>" data-trigger-confirm="1" type="button">Освободить</a>
								<?php endif; ?>
							</td>
						</tr>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					</tbody>
				</table>

			</div>
		</div>
	</div>

</div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>

<script type="text/javascript">

	function sendFree(id) {
		jQuery.ajax({
			url: "/autofree",
			data: {
		        "_token": "<?php echo e(csrf_token()); ?>",
		        "id": id
	        },
			type: "POST",
			success:function(data){
				document.location.reload(true);		
			},
			error:function (){}
		});
	}

	$('.free').on('click', function(){
		var isAdmin = confirm("Вы - уверены?");
		if(isAdmin){
			sendFree( $(this).data('id') );
		}
	})

</script>

<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>