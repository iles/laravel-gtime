        <?php
          $m = Config::get('matrix.get');
          $p = Session::get('programm', 1);
          if($p == null){
            Session::put('programm', 1);
            $p = Session::get('programm', 1);
          }
        ?>

<div class="card" style="width: 100%; margin: 0 auto;">

  <div class="card-header">
    <strong><?php echo e($model->name); ?></strong><br/>
  </div>



<ul class="nav nav-pills mb-3" id="pills-tab" role="tablist" style="padding: 15px 15px 5px;">
  <li class="nav-item">
    <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#info-tab" role="tab" aria-controls="pills-home" aria-selected="true">Инфо</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#profile" role="tab" aria-controls="pills-profile" aria-selected="false">Кого пригласил</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#change-pass" role="tab" aria-controls="pills-contact" aria-selected="false">Поменять пароль</a>
  </li>
</ul>


    <div class="card">
      <div class="tab-content" id="myTabContent">
          <div class="tab-pane fade show active" style="padding: 0" id="info-tab" role="tabpanel" aria-labelledby="home-tab">
            <form method="post" action="<?php echo e(action('SponsorsController@update')); ?>" id="create_pin_form" class="form-horizontal" accept-charset="UTF-8">
    <div class="card-body">
        <?php echo csrf_field(); ?>

        <input class="form-control" name="user_id" type="text" hidden value="<?php echo e($model->user_id); ?>">
        <div class="form-group row">
          <label class="col-md-3 col-form-label" for="hf-password">Логин</label>
          <div class="col-md-9">
            <input class="form-control" name="name" type="text" value="<?php echo e($model->name); ?>">
          </div>
        </div>



        <div class="form-group row">
          <label class="col-md-3 col-form-label" for="hf-password">ФИО</label>
          <div class="col-md-9">
            <input class="form-control" name="familiya" type="text" value="<?php echo e($model->familiya); ?>">
          </div>
        </div>

        <div class="form-group row">
          <label class="col-md-3 col-form-label" for="hf-password">Страна / Город</label>
          <div class="col-md-9">
            <input class="form-control" name="imya " type="text" value="<?php echo e($model->imya); ?>">
          </div>
        </div>

        <div class="form-group row">
          <label class="col-md-3 col-form-label" for="hf-password">Дата рождения</label>
          <div class="col-md-9">
            <input class="form-control" name="otchestvo" type="text" value="<?php echo e($model->otchestvo); ?>">
          </div>
        </div>

        <div class="form-group row">
          <label class="col-md-3 col-form-label" for="hf-password">Телефон</label>
          <div class="col-md-9">
            <input class="form-control" name="pol" type="text" value="<?php echo e($model->pol); ?>">
          </div>
        </div>

        <div class="form-group row">
          <label class="col-md-3 col-form-label" for="hf-password">ИИН</label>
          <div class="col-md-9">
            <input class="form-control" name="strana" type="text" value="<?php echo e($model->strana); ?>">
          </div>
        </div>



        <div class="form-group row">
          <label class="col-md-3 col-form-label" for="hf-password">Звезды</label>
          <div class="col-md-9">
            <input class="form-control" name="ref_num" type="text" value="<?php echo e($model->ref_num); ?>">
          </div>
        </div>

        <div class="form-group row">
          <label class="col-md-3 col-form-label" for="hf-password">Спонсор</label>
          <div class="col-md-9">
            <input class="form-control" name="ref_num" type="text" value="<?php echo e($model->by_refer); ?>">
          </div>
        </div>

        <div class="form-group row">
          <label class="col-md-3 col-form-label" for="hf-password">Skype</label>
          <div class="col-md-9">
            <input class="form-control" name="gorod" type="text" value="<?php echo e($model->skype); ?>">
          </div>
        </div>

        <div class="form-group row">
          <label class="col-md-3 col-form-label" for="hf-password">Дата регистрации</label>
          <div class="col-md-9">
            <input class="form-control" name="gorod" type="text" value="<?php echo e(gmdate("Y-m-d", $model->reg_date)); ?>">
          </div>
        </div>

        <div class="form-group row">
          <label class="col-md-3 col-form-label" for="hf-password">Приписка (signature)</label>
          <div class="col-md-9">
            <textarea class="form-control" name="signature"><?php echo e($model->signature); ?></textarea>
          </div>
        </div>


        <div class="form-group row">
          <label class="col-md-3 col-form-label" for="hf-password">Приписка(info)</label>
          <div class="col-md-9">
            <textarea class="form-control" name="info"><?php echo e($model->info); ?></textarea>
          </div>
        </div>

        <div class="form-group row">
          <label class="col-md-3 col-form-label" for="hf-password">Товар</label>
          <div class="col-md-9">
            <textarea class="form-control" name="tovar"><?php echo e($model->tovar); ?></textarea>
          </div>
        </div>

        <?php if($p == 6): ?>
        <div class="form-group row">
          <label class="col-md-3 col-form-label" for="hf-password">Вип бонус</label>
          <div class="col-md-9">
            <textarea class="form-control" name="signature"><?php echo e($model->vip_bonus); ?></textarea>
          </div>
        </div>
        <?php endif; ?>





    </div>

    <div class="card-footer">
      <button class="btn btn-sm btn-primary" type="submit">
      <i class="fa fa-dot-circle-o" type="submit"></i> Сохранить</button>
      <button class="btn btn-sm btn-danger"  data-dismiss="modal" aria-label="Close" type="reset">
      <i class="fa fa-ban"></i> Закрыть</button>
    </div>
    </form>
          </div>
            <div class="tab-pane fade" style="padding: 0" id="profile" role="tabpanel" aria-labelledby="profile-tab">
              <div class="card-body">

                <?php if(!$refers->isEmpty() ): ?>
                <table class="table table-responsive-sm">
                  <thead>
                  <tr>
                  <th>Логин</th>
                  <th>ФИО</th>
                  <th>ПИН код</th>
                  <th>Дата регистации</th>
                  </tr>
                  </thead>
                  <tbody>
                    <?php $__currentLoopData = $refers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <tr>
                      <td><?php echo e($user->name); ?></td>
                      <td><?php echo e($user->familiya); ?></td>
                      <td><?php echo e($user->screch); ?></td>
                      <td><?php echo e(gmdate("Y-m-d", $user->reg_date)); ?></td>
                    </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                  </tbody>
                  </table>
                  <?php else: ?>
                  Никого не пригласил
                  <?php endif; ?>
</div>

            </div>


            <div class="tab-pane fade" style="padding: 0" id="change-pass" role="tabpanel" aria-labelledby="contact-tab">
            
            <form method="post" action="<?php echo e(action('SponsorsController@passchange')); ?>" id="passchange_form" style="display:block" class="form-horizontal" accept-charset="UTF-8">
            <div class="card-body">
            <?php echo csrf_field(); ?>
              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="hf-password">Пароль</label>
                  <div class="col-md-9">
                    <input class="" name="user_id" type="text" hidden value="<?php echo e($model->user_id); ?>">
                    <input class="form-control" name="password"  minlength="8" type="password" >
                  </div>
              </div>
              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="hf-password">Подтверждение Пароля</label>
                  <div class="col-md-9">
                    <input class="form-control" name="password_confirmation"  minlength="8" type="password">
                  </div>
              </div>
            </div>


            <div class="card-footer">
              <button class="btn btn-sm btn-primary" type="submit">
              <i class="fa fa-dot-circle-o" type="submit"></i> Сохранить</button>
            </div>
             </form>

            </div>
      </div>


    </div>



    </form>
</div>
