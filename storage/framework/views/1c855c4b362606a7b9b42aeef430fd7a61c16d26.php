<?php $__env->startSection('content'); ?>

<div class="cabinet_main_container authorization">
	<div class="header">
    <div class="row">
      <div class="col-md-10">
		    <h2 class="title">Завершение программы</h2>        
      </div>
      <div class="col-md-2">
        <a class="link" href="#" onclick="document.getElementById('logout-form').submit(); return false;">
          <i class="icon-logout"></i>выход
        </a>
        <form id="logout-form" action="<?php echo e(route('partner.logout')); ?>" method="POST" style="display: none;">
          <?php echo csrf_field(); ?>
        </form>
      </div>
    </div>

	</div>
	<div class="body">
		<div class="auth_cont" style="max-width: 100%;">



<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingOne">
      <h4 class="panel-title">
        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
          Отправить заявку на получение вознаграждения
        </a>
      </h4>
    </div>
    <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
      <div class="panel-body">
        <p style="margin: 25px 0 50px 0; font-weight: bold; font-size: 16px; color: #3e3d3c;">Поздравляем! <br/>
                Вы завершили программу «Старт»  для получения вознаграждения заполните форму ниже.</p>
 
                <?php if($bonus): ?>
                    <div class="row" style="padding: 40px 0 0 10px;">
                      <?php if($bonus->status_id == 0): ?>
                      Ваша заявка находится на рассмотрении
                      <?php elseif($bonus->status_id == 1): ?>
                        Ваша заявка одобрена и находится на рассмотрении в бухгалтерии<br/>      
                      <?php elseif($bonus->status_id == 4): ?>
                        Ваша заявка одобрена       
                      <?php elseif($bonus->status_id == 5): ?>
                        Ваша заявка отклонена
                        <br/>
                        <form action="/partner/rebonus" method="POST">
                          <?php echo csrf_field(); ?>
                          <input type="text" hidden  value="<?php echo e($bonus->id); ?>" name="id">
                          <button type="submit" href="#" class="btn btn-primary">Создать новую заявку</button>
                        </form>  
                  
                      <?php elseif($bonus->status_id == 2): ?>
                      Ваша заявка отклонена <br/>
                      <br/>
                      <pre>
                        <?php echo e($bonus->cancel_reason); ?>

                      </pre>
                      <form action="/partner/rebonus" method="POST">
                        <?php echo csrf_field(); ?>
                        <input type="text" hidden  value="<?php echo e($bonus->id); ?>" name="id">
                        <button type="submit" href="#" class="btn btn-primary">Создать новую заявку</button>
                      </form>
                      <?php endif; ?>
                    </div>
                  <?php else: ?>
					  
			             <?php echo $__env->make('bonus-request.form_2', ['progs' => $progs], \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                  <?php endif; ?>
      </div>
    </div>
  </div>


</div>






		</div>
		</div>

				</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>


<?php $__env->stopSection(); ?>
<?php echo $__env->make('front.layout.main', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>