<?php $__env->startSection('content'); ?>

<?php
  $m = Config::get('matrix.get');
    $p = Session::get('programm', 1);
?>

<div class="cabinet_main_container authorization">
  <div class="header">
    <h2 class="title"><?php echo e($m[$p][1]); ?></h2>
  </div>
  <div class="body">
    <div class="auth_cont">

    <?php echo e(Form::open(array('route' => 'partner.resethandle'))); ?>

        <p class="text">Введите ваш Email</p>
        <?php if(Session::has('message')): ?>
          <p class="alert <?php echo e(Session::get('alert-class', 'alert-info')); ?>"><?php echo e(Session::get('message')); ?></p>
        <?php endif; ?>

        <?php if($errors->any()): ?>
            <div class="alert alert-danger">
                <ul>
                    <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <li><?php echo e($error); ?></li>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </ul>
            </div>
        <?php endif; ?>

        <?php echo e(Form::text('email', '', ['class'=>'form-control', 'placeholder'=>'Email'])); ?>


        <p class="error-message" style="color:red; font-weight: bold; display: none; margin-top: 15px;"></p>
        <input type="submit" value="Восстановить пароль" class="button" style="margin-top: 45px;">
      <?php echo e(Form::close()); ?>

    </div>
    </div>

        </div>
<?php $__env->stopSection(); ?>


<?php $__env->startSection('scripts'); ?>
  <script type="text/javascript">
    $(document).ready(function(){
      $('.auth_cont form').on('submit', function(e){
        e.preventDefault();
        $('error-message').html('');
        $('.error-message').fadeOut();
        var email = $( "input[name='email']").val();

        if(email == ''){
          $('.error-message').html('Введите email');
          $('.error-message').fadeIn();
          return false;
        }

        if( validateEmail(email) == false ){
          $('.error-message').html('Введите правильный email');
          $('.error-message').fadeIn();
          return false;
        }


        $.ajax({
          method: "POST",
          url: "/partner/password-reset",
          data:{
            '_token': '<?php echo e(csrf_token()); ?>',
            'email' : email,
          },
        }).done(function( msg ) {
          if(msg.error == 1){
            $('.error-message').html(msg.message);
            $('.error-message').fadeIn();
          }else{
              e.target.submit();
          }
        });

      })
    })

    function validateEmail(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    }
  </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('front.layout.main', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>