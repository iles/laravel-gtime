<style type="text/css">
  .modal-dialog{max-width: 90%;}
</style>
<div class="card">
  <div class="card-body">
    <div class="alert alert-warning" style="display:none" role="alert">
      Вы действительно хотите назначить <span class="user-name"></span> лидером лестницы? 
      <br/>
      <br/>
      <a class="btn btn-info active" onclick="sendAjaxChange()" type="a" aria-pressed="true">Да</a>
      <a class="btn btn-danger active" type="a" aria-pressed="true">Отмена</a>
    </div>

          <?php $__currentLoopData = $matrix; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $m): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
              <div class="level">
                <?php $__currentLoopData = $m; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $level): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <?php if($level[3]): ?>
                      
                    <span data-id="<?php echo e($level[2]); ?>" data-name="<?php echo e($level[0]); ?>">
                      <?php echo e($level[0]); ?><br/>
                      <small>(заменен на <?php echo e($level[3]); ?></small> 
                    </span>
                    <?php else: ?>
                    <span data-id="<?php echo e($level[2]); ?>" data-name="<?php echo e($level[0]); ?>"><?php echo e($level[0]); ?> </span>
                    <?php endif; ?>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
              </div>
          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
  </div>
    <style type="text/css">
      .level{width: 100%; height: auto; margin: 15px; }
      .level span {display: inline-block; width: 100px; text-align: center; font-weight: bold; padding-top: 14px; height: 60px; margin: 15px; border: 1px solid #000;}
      .level span.active {outline: 3px solid #000;}
    </style>

