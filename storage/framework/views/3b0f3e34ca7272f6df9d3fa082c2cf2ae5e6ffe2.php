<div class="card">
  <?php echo e(Form::model($model, array('route' => array('warehouse.update', $model->id), 'method'=>'put'))); ?>


  <div class="card-header">
    <strong>Редактировать заявку</strong></div>
    <div class="card-body">
        <?php echo csrf_field(); ?>



        <div class="form-group row">            
          <?php echo e(Form::label('program', 'Программа:', array('class' => 'col-md-3 col-form-label'))); ?>

          <div class="col-md-9">
            <?php echo e(Form::text('program', Input::old('program'), array('class' => 'form-control') ,  array('class' => 'form-control') )); ?>

          </div>
        </div>              



        <div class="form-group row">            
          <?php echo e(Form::label('uname', 'Логин:', array('class' => 'col-md-3 col-form-label'))); ?>

          <div class="col-md-9">
            <?php echo e(Form::text('uname', Input::old('uname'), array('class' => 'form-control') ,  array('class' => 'form-control') )); ?>

          </div>
        </div>     

        <div class="form-group row">            
          <?php echo e(Form::label('user_fullname', 'ФИО:', array('class' => 'col-md-3 col-form-label'))); ?>

          <div class="col-md-9">
            <?php echo e(Form::text('user_fullname', Input::old('user_fullname'), array('class' => 'form-control') ,  array('class' => 'form-control') )); ?>

          </div>
        </div>             

        <div class="form-group row">            
          <?php echo e(Form::label('purchace_date', 'Дата покупки:', array('class' => 'col-md-3 col-form-label'))); ?>

          <div class="col-md-9">
            <?php echo e(Form::text('purchace_date', Input::old('purchace_date'), array('class' => 'form-control') ,  array('class' => 'form-control') )); ?>

          </div>
        </div>         


        <div class="form-group row">            
          <?php echo e(Form::label('purchace_date', 'Набор:', array('class' => 'col-md-3 col-form-label'))); ?>

          <div class="col-md-9">
            <?php echo e(Form::select('option', $options, $model->option, array('class' => 'form-control') )); ?>

          </div>
        </div>              


        <div class="form-group row">            
          <?php echo e(Form::label('checkout_name', 'Имя:', array('class' => 'col-md-3 col-form-label'))); ?>

          <div class="col-md-9">
            <?php echo e(Form::text('checkout_name', Input::old('checkout_name'), array('class' => 'form-control') ,  array('class' => 'form-control') )); ?>

          </div>
        </div>


       <div class="form-group row">            
          <?php echo e(Form::label('checkout_surname', 'Фамилия:', array('class' => 'col-md-3 col-form-label'))); ?>

          <div class="col-md-9">
            <?php echo e(Form::text('checkout_surname', Input::old('checkout_surname'), array('class' => 'form-control') ,  array('class' => 'form-control') )); ?>

          </div>
        </div>         

        <div class="form-group row">            
          <?php echo e(Form::label('checkout_patronic', 'Отчество:', array('class' => 'col-md-3 col-form-label'))); ?>

          <div class="col-md-9">
            <?php echo e(Form::text('checkout_patronic', Input::old('checkout_patronic'), array('class' => 'form-control') ,  array('class' => 'form-control') )); ?>

          </div>
        </div>           

        <div class="form-group row">            
          <?php echo e(Form::label('checkout_date', 'Дата покупки:', array('class' => 'col-md-3 col-form-label'))); ?>

          <div class="col-md-9">
            <?php echo e(Form::text('checkout_date', Input::old('checkout_date'), array('class' => 'form-control') ,  array('class' => 'form-control') )); ?>

          </div>
        </div>         

        
    </div>
        
    <div class="card-footer">
      <button class="btn btn-sm btn-primary" type="submit">
      <i class="fa fa-dot-circle-o"></i> Сохранить </button>
      <button class="btn btn-sm btn-danger"  data-dismiss="modal" aria-label="Close" type="reset">
      <i class="fa fa-ban"></i> Отмена</button>

    </div>
    </form>
</div>

