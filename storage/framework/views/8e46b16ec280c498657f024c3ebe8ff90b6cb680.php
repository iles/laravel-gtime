<!DOCTYPE html>
<html lang="ru">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title></title>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">

<link rel="stylesheet" href="/front/css/bootstrap.min.css">
<link href="/front/css/styles.css" rel="stylesheet" type="text/css"/>
<link href="https://fonts.googleapis.com/css?family=Ubuntu:300,400,400i,500,700,700i&amp;subset=cyrillic" rel="stylesheet">

<script src="/front/js/jquery-3.3.1.min.js"></script>
<script src="/front/js/jquery.easing.min.js"></script>
<script src="/front/js/bootstrap.min.js"></script>
<script src="/front/js/inputmask/inputmask.min.js"></script>
<script src="/front/js/inputmask/inputmask.extensions.min.js"></script>
<script src="/front/js/inputmask/jquery.inputmask.min.js"></script>
</head>
<body>
	<div class="top_cont">
		<div class="container-fluid">
			<div class="flex_container">
				<div class="flex_item social_cont">
					<ul>

					</ul>
				</div>
				<div class="flex_item tel_cont">
					<ul>
						<li><a href="tel:+77474747474" class="tel">+7(747)474 74 74</a></li>
						<li><a href="tel:+77474747474" class="tel">+7(747)474 74 74</a></li>
					</ul>
				</div>
				<div class="flex_item logo_cont"><a href="#"></a></div>
				<div class="flex_item icons_cont">
					<div class="icon_cont lang_cont">
						<a class="link collapsed" role="button" data-toggle="collapse" href="#lang_collapse" aria-expanded="false" aria-controls="lang_collapse">Ru</a>
						<div class="collapse" id="lang_collapse">
							<div class="cont">
								<ul>
									<li><a href="#" class="link">En</a></li>
									<li><a href="#" class="link">kz</a></li>
								</ul>
							</div>
						</div>
					</div>
					<div class="icon_cont cart_cont">
						<a class="link collapsed" role="button" data-toggle="collapse" href="#cart_collapse" aria-expanded="false" aria-controls="cart_collapse">корзина</a>
						<div class="collapse" id="cart_collapse">
							<div class="cont">
								<div class="text"><b>2</b> товара на сумму<br><b>991 250 000</b> тг.</div>
								<a href="#" class="button">В корзину</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="menu_cont">
		<div class="container-fluid">
			<div class="flex_container">
				<nav class="navbar navbar-default">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">Меню</button>
					</div>
					<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
						<ul class="nav navbar-nav">
							<li class="active"><a href="#">главная</a></li>
							<li><a href="#">продукция</a></li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span>о компании</span></a>
								<ul class="dropdown-menu">
									<li><a href="#">Action</a></li>
									<li><a href="#">Another action</a></li>
									<li><a href="#">Something else here</a></li>
									<li><a href="#">Separated link</a></li>
									<li><a href="#">One more separated link</a></li>
								</ul>
							</li>
							<li><a href="#">новости</a></li>
							<li><a href="#">для партнеров</a></li>
							<li><a href="#">галерея</a></li>
							<li><a href="#">обучение</a></li>
							<li><a href="#">контакты</a></li>
						</ul>
					</div>
				</nav>
				<div class="search_cont">
					<input type="text" placeholder="поиск" class="form-control">
					<input type="submit" value="" class="button">
				</div>
			</div>
		</div>
	</div>
	<div class="container-fluid">
		<ol class="breadcrumb">
			<li><a href="#">главная</a></li>
			<li class="active"><a href="#">для партнеров</a></li>
		</ol>

        <?php
          $m = Config::get('matrix.get');
          $p = Session::get('programm', 1);
        ?>

		<div class="row">
			<div class="col-xs-12 col-sm-4 col-lg-3 left_column">
				<div class="collapse_menu_cont left_menu">
					<button class="button collapsed" type="button" data-toggle="collapse" data-target="#collapse_menu" aria-expanded="false" aria-controls="collapse_menu">вход в программу</button>
					<div class="collapse in" id="collapse_menu">
						<div class="collapse_text_cont">
							<ul>
			                      <?php $__currentLoopData = $m; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $matrix): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
			                        <?php if($key == $p): ?>
			                        	<li><a href="#" data-id="<?php echo e($key); ?>" class="link active"><?php echo e($matrix[1]); ?></a></li>
			                            <?php continue; ?>
			                        <?php endif; ?>
			                            <li><a href="#" data-id="<?php echo e($key); ?>" class="link"><?php echo e($matrix[1]); ?></a></li>
			                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
							</ul>
						</div>
					</div>
				</div>
				<div class="counter_cont">
					<h3 class="title">партнеров с нами</h3>
					<div class="numbers_cont">
						<div class="line"></div>
						<div class="number">0</div>
						<div class="line"></div>
						<div class="number">0</div>
						<div class="line"></div>
						<div class="number">0</div>
						<div class="line"></div>
						<div class="number">2</div>
						<div class="line"></div>
						<div class="number">0</div>
						<div class="line"></div>
						<div class="number">1</div>
						<div class="line"></div>
						<div class="number">8</div>
						<div class="line"></div>
					</div>
				</div>
				<div class="left_col_text_cont">
					<h3 class="title">Lorem ipsum dolor</h3>
					<p class="text">sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore.</p>
					<a href="#" class="link">читать дальше</a>
				</div>
			</div>
			<div class="col-xs-12 col-sm-8 col-lg-9 right_column">
					<?php echo $__env->yieldContent('content'); ?>
			</div>
		</div>
	</div>
	<div class="footer_cont">
		<div class="footer">
			<div class="container-fluid">
				<div class="row">
					<div class="col-xs-12 col-lg-3">
						<div class="menu_cont">
							<nav class="navbar navbar-default">
								<div class="navbar-header">
									<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2" aria-expanded="false">Меню</button>
								</div>
								<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
									<ul class="nav navbar-nav">
										<li class="active"><a href="#">главная</a></li>
										<li><a href="#">продукция</a></li>
										<li class="dropdown">
											<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span>о компании</span></a>
											<ul class="dropdown-menu">
												<li><a href="#">Action</a></li>
												<li><a href="#">Another action</a></li>
												<li><a href="#">Something else here</a></li>
												<li><a href="#">Separated link</a></li>
												<li><a href="#">One more separated link</a></li>
											</ul>
										</li>
										<li><a href="#">новости</a></li>
										<li><a href="#">для партнеров</a></li>
										<li><a href="#">галерея</a></li>
										<li><a href="#">обучение</a></li>
										<li><a href="#">контакты</a></li>
									</ul>
								</div>
							</nav>
						</div>
					</div>
					<div class="col-xs-12 col-sm-7 col-lg-6">
						<div class="address_cont">
							<div class="flex_container">
								<div class="col-xs-12 col-sm-6 flex_item">
									<div class="address">Республика Казахстан,<br>г. Алматы, ул. Достык 2<br>(Квартал), 2-этаж, Блок С</div>
								</div>
								<div class="col-xs-12 col-sm-6 flex_item">
									<div class="tel_cont">
										<ul>
											<li><a href="tel:+77474747474" class="tel">+7(747)474 74 74</a></li>
											<li><a href="tel:+77474747474" class="tel">+7(747)474 74 74</a></li>
										</ul>
									</div>
								</div>
								<div class="col-xs-12 col-sm-6 flex_item">
									<div class="link_cont">
										<div class="name">g-time travel</div>
										<a href="http://www.gtimetravel.kz" class="link">www.gtimetravel.kz</a>
									</div>
								</div>
								<div class="col-xs-12 col-sm-6 flex_item">
									<div class="social_cont">
										<ul>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-xs-12 col-sm-5 col-lg-3">
						<div class="kurs_cont">
							<table>
								<tr>
									<td class="th1">курс валют</td>
									<td class="th2">покупка</td>
									<td class="th2">продажа</td>
								</tr>
								<tr>
									<td>usd</td>
									<td>0.00</td>
									<td>0.00</td>
								</tr>
								<tr>
									<td>eur</td>
									<td>0.00</td>
									<td>0.00</td>
								</tr>
								<tr>
									<td>rur</td>
									<td>0.00</td>
									<td>0.00</td>
								</tr>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="bottom">
			<div class="container-fluid">
				<div class="row">
					<div class="col-xs-12 col-sm-6 copyright">© 2012-2018 g-time corporation </div>
					<div class="col-xs-12 col-sm-6 make"><a href="#" class="link">сайт разработан компанией</a></div>
				</div>
			</div>
		</div>
	</div>
</body>

<script type="text/javascript">
	$(document).ready( function(){
		var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
		$('#collapse_menu .link').on('click', function(e){
			e.preventDefault();
			$.post( "/change", {_token: CSRF_TOKEN, id:  $(this).data('id') } ).done( function(){
            	window.location.reload('/login'); 
            });
		})
	})
</script>

      <script>
         $(function(){
            $("input[type = 'submit']").click(function(){
               var $fileUpload = $("input[type='file']");
               if (parseInt($fileUpload.get(0).files.length) > 3){
                  alert("You are only allowed to upload a maximum of 3 files");
               }
            });
         });
      </script>
      
<?php echo $__env->yieldContent('scripts'); ?>
</html>