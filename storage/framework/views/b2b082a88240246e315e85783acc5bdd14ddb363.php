<div class="card">
   <form method="post" action="<?php echo e(action('SecurityController@aprove')); ?>" id="create_pin_form" class="form-horizontal" accept-charset="UTF-8"> 
  <div class="card-header">
    <strong><?php echo e($model->uname); ?></strong></div>
    <div class="card-body">
        <?php echo csrf_field(); ?>

        <div class="form-group row">
          <input type="text" name="id"  value="<?php echo e($model->id); ?>" hidden/>
          <label class="col-md-3 col-form-label" for="hf-password">Логин</label>
          <div class="col-md-9">           
            <input class="form-control" name="name" value="<?php echo e($model->uname); ?>" type="text">
          </div>
        </div>         

        <div class="form-group row">
          <input type="text" name="id"  value="<?php echo e($model->id); ?>" hidden/>
          <label class="col-md-3 col-form-label" for="hf-password">ФИО</label>
          <div class="col-md-9">           
            <input class="form-control" name="name" value="<?php echo e($model->fullname); ?>" type="text">
          </div>
        </div>        

        <div class="form-group row">
          <label class="col-md-3 col-form-label" for="hf-password">ИИН</label>
          <div class="col-md-9">           
            <input class="form-control" name="iin" value="<?php echo e($model->iin); ?>" type="text">
          </div>
        </div>                

        <div class="form-group row">
          <label class="col-md-3 col-form-label" for="hf-password">Номер квитанции</label>
          <div class="col-md-9">           
            <input class="form-control" name="bill" value="<?php echo e($model->bill); ?>" type="text">
          </div>
        </div>            

        <div class="form-group row">
          <label class="col-md-3 col-form-label" for="hf-password">Сумма</label>
          <div class="col-md-9">           
            <input class="form-control" name="summ" value="<?php echo e($model->summ); ?>" type="text">
          </div>
        </div>             

        <div class="form-group row">
          <label class="col-md-3 col-form-label" for="hf-password">Skype</label>
          <div class="col-md-9">           
            <input class="form-control" name="skype" value="<?php echo e($model->skype); ?>" type="text">
          </div>
        </div>               

        <div class="form-group row">
          <label class="col-md-3 col-form-label" for="hf-password">Страна</label>
          <div class="col-md-9">           
            <input class="form-control" name="skype" value="<?php echo e($model->country); ?>" type="text">
          </div>
        </div>            

        <div class="form-group row">
          <label class="col-md-3 col-form-label" for="hf-password">Примечание</label>
          <div class="col-md-9">           
            <textarea class="form-control"><?php echo e($model->user_note); ?></textarea>
          </div>
        </div>           

        <div class="form-group row">
          <label class="col-md-3 col-form-label" for="hf-password">Фото квитанции</label>
          <div class="col-md-9">           
              <?php if($model->image != 0): ?>
                  <?php $__currentLoopData = explode(';', $model->image); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $image): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <a data-fancybox="gallery" href="/uploads/bills/<?php echo e($image); ?>"><img src="/uploads/bills/<?php echo e($image); ?>" style="width: 80px;"></a>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
              <?php endif; ?>
          </div>
        </div>      






        <div class="form-group row">
          <label class="col-md-3 col-form-label" for="hf-password">Приписка</label>
          <div class="col-md-9">
            <textarea name="note"  class="form-control" ><?php echo e($model->note); ?></textarea>
          </div>
        </div>



 


        <?php if($model->status_id == 0): ?>
        <br><br><br>

        <div class="form-group row">
          <input type="text" name="id" value="<?php echo e($model->id); ?>" hidden/>
          <input type="text" name="status" id="status_id" value="1" hidden/>
          <label class="col-md-3 col-form-label" for="hf-password">Причина отказа</label>
          <div class="col-md-9">           
            <textarea name="cancel_reason"  class="form-control" id="cancel_reason"><?php echo e($model->cancel_reason); ?></textarea>
          </div>
          <div class="invalid-feedback">Укажите причину отказа</div>
        </div>          




        
        <?php endif; ?>

        
    </div>
        
    <div class="card-footer">

      <button class="btn btn-sm btn-primary" type="submit">
      <i class="fa fa-dot-circle-o" type="submit"></i>Одобрить</button>
      <?php if($model->status_id == 0): ?>
        <button class="btn btn-sm btn-danger" onclick="reject(); return false;">      
        <i class="fa fa-ban"></i> Отклонить</button>
      <?php endif; ?>




    </div>

    </form>
</div>