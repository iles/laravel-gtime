<?php $__env->startSection('breadcrumbs'); ?>
	<li class="breadcrumb-item active">Вознаграждения</li>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
	<?php $__env->startSection('content'); ?>
	<div class="row">
		<div class="col-md-12">        		
	          <?php if(Session::has('message')): ?>
	                  <div class="card-header">
	            			<p class="alert <?php echo e(Session::get('alert-class', 'alert-info')); ?>"><?php echo e(Session::get('message')); ?></p>
	                   </div>
	          <?php endif; ?>
        	</div>
		</div>
	<?php echo $grid; ?>

	<?php $__env->stopSection(); ?>
<?php $__env->stopSection(); ?>


<?php $__env->startSection('scripts'); ?>

	<script type="text/javascript">
		var i = 0;


      	function reject(){
      		if($('#cancel_reason').val() == ''){
	      		$('#cancel_reason').addClass('form-control-danger').focus();
	      		$('.invalid-feedback').fadeIn();      			
      		} else {
      			$('#status_id').val(2);
      			$('#update_bonus_form').submit();
      		}
      	}

      		isActive = true;

	$().ready(function () {
	    //EITHER USE A GLOBAL VAR OR PLACE VAR IN HIDDEN FIELD
	    //IF FOR WHATEVER REASON YOU WANT TO STOP POLLING
	    pollServer();
	});

	function pollServer()
	{
	    if (isActive)
	    {
	        window.setTimeout(function () {
	            $.pjax.reload('#bonus-grid');
	            pollServer();
	        }, 60000);
	    }
	}
	</script>

<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>