<?php $__env->startSection('breadcrumbs'); ?>
  <li class="breadcrumb-item active">Пользователи</li>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

<style type="text/css">
	.dis{display: none;	}
	.dis.active{display: flex;	}
</style>

				<?php if(Session::has('message')): ?>
                  <div class="card-header">
						<p class="alert <?php echo e(Session::get('alert-class', 'alert-info')); ?>"><?php echo e(Session::get('message')); ?></p>
                   </div>
				<?php endif; ?>
asd
<?php echo $grid; ?>

<?php $__env->stopSection(); ?>


<?php $__env->startSection('scripts'); ?>
  <script type="text/javascript">
			function checkPasswords( event ) {
				var x = document.getElementsByName('password')[0].value,
			        y = document.getElementsByName('password2')[0].value;

			    

			    if( $('#change-password').hasClass('active') ){
				    if(x == "" || y == "" ){
						event.preventDefault();
				    	alert('Введите пароль и подтверждение');
				    }			    	
			    }

			    if (x === y) {
					return;
			    }
			 	
				event.preventDefault();
			    alert('Пароли не совпадают!');
			};

			function toglePasswords(e) {
			   	e.preventDefault();
				$('.dis').toggleClass('active');	
			}
  </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>