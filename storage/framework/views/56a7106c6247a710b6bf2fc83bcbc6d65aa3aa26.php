<?php 
	use Illuminate\Support\Facades\Session;
?>




<?php $__env->startSection('breadcrumbs'); ?>
	<li class="breadcrumb-item active">Заявки на передачу </li>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
	<?php $__env->startSection('content'); ?>
	<div class="row">
		<div class="col-md-12">        		
	          <?php if(Session::has('message')): ?>
	                  <div class="card-header">
	            			<p class="alert <?php echo e(Session::get('alert-class', 'alert-info')); ?>"><?php echo e(Session::get('message')); ?></p>
	                   </div>
	          <?php endif; ?>
        	</div>
	</div>
	<?php echo $grid; ?>

	<?php $__env->stopSection(); ?>
<?php $__env->stopSection(); ?>


<?php $__env->startSection('scripts'); ?>

	<script type="text/javascript">
		
		$('.grid-title').html('Заявки на передачу ');
		var i = 0;


      	function reject(){
      		if($('#cancel_reason').val() == ''){
	      		$('#cancel_reason').addClass('form-control-danger').focus();
	      		$('.invalid-feedback').fadeIn();      			
      		} else {
      			$('#status_id').val(2);
      			$('#update_bonus_form').submit();
      		}
      	}
	</script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>