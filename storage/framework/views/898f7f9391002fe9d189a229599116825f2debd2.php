<?php if($grid->wantsPagination() && !$grid->gridNeedsSimplePagination()): ?>
    <div class="pull-<?php echo e($direction); ?>">
        <b>
            <?php if($grid->getData()->total() <= $grid->getData()->perpage()): ?>
                <?php if(!isset($atFooter)): ?>
                    Showing <?php echo e(($grid->getData()->currentpage() - 1 ) * $grid->getData()->perpage() + 1); ?>

                    to <?php echo e($grid->getData()->total()); ?>

                    of <?php echo e($grid->getData()->total()); ?> entries.
                <?php endif; ?>
            <?php else: ?>
                Showing <?php echo e(($grid->getData()->currentpage() - 1 ) * $grid->getData()->perpage() + 1); ?>

                to <?php echo e($grid->getData()->currentpage() * $grid->getData()->perpage()); ?>

                of <?php echo e($grid->getData()->total()); ?> entries.
            <?php endif; ?>
        </b>
    </div>
<?php else: ?>
    <?php if(isset($atFooter)): ?>
        <?php if($grid->getData()->count() >= $grid->getData()->perpage()): ?>
            <div class="pull-<?php echo e($direction); ?>">
                <b>
                    Showing <?php echo e($grid->getData()->count()); ?> records for this page.
                </b>
            </div>
        <?php endif; ?>
    <?php else: ?>
        <div class="pull-<?php echo e($direction); ?>">
            <b>
                Showing <?php echo e($grid->getData()->count()); ?> records for this page.
            </b>
        </div>
    <?php endif; ?>
<?php endif; ?>