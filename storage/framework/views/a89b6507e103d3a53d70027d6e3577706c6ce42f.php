<?php $__env->startSection('content'); ?>



<div class="cabinet_main_container authorization">
  <div class="header">
    <div class="row">
      <div class="col-md-10">
        <h2 class="title">Завершение программы</h2>        
      </div>
      <div class="col-md-2">
        <a class="link" href="#" onclick="document.getElementById('logout-form').submit(); return false;">
          <i class="icon-logout"></i>выход
        </a>
        <form id="logout-form" action="<?php echo e(route('partner.logout')); ?>" method="POST" style="display: none;">
          <?php echo csrf_field(); ?>
        </form>
      </div>
    </div>

  </div>
	<div class="body">
		<div class="auth_cont" style="max-width: 100%;">   

                <?php if($bonus): ?>
                    <div class="row" style="padding: 40px 0 0 10px;">
                      <?php if($bonus->status_id == 0): ?>
                      Ваша заявка находится на рассмотрении
                      <?php elseif($bonus->status_id == 1): ?>
                        Ваша заявка одобрена и находится на рассмотрении в бухгалтерии<br/>      
                      <?php elseif($bonus->status_id == 4): ?>
                        Ваша заявка одобрена       
                      <?php elseif($bonus->status_id == 5): ?>
                        Ваша заявка отклонена
                        <br/>
                        <form action="/partner/rebonus" method="POST">
                          <?php echo csrf_field(); ?>
                          <input type="text" hidden  value="<?php echo e($bonus->id); ?>" name="id">
                          <button type="submit" href="#" class="btn btn-primary">Создать новую заявку</button>
                        </form>  
                  
                      <?php elseif($bonus->status_id == 2): ?>
                      Ваша заявка отклонена <br/>
                      <br/>
                      <pre>
                        <?php echo e($bonus->cancel_reason); ?>

                      </pre>
                      <form action="/partner/rebonus" method="POST">
                        <?php echo csrf_field(); ?>
                        <input type="text" hidden  value="<?php echo e($bonus->id); ?>" name="id">
                        <button type="submit" href="#" class="btn btn-primary">Создать новую заявку</button>
                      </form>
                      <?php endif; ?>
                    </div>
                  <?php else: ?>
                  <p style="margin: 25px 0 50px 0; font-weight: bold; font-size: 16px; color: #3e3d3c;">
                  -Вознаграждение в денежном выражении 20 000тенге*.
                  + варианты выбора продукции, входящих в пакет «Основной».</p>
                  <?php echo $__env->make('front.finish.plus.formA', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                  <?php endif; ?>



  <p style="margin: 65px 0 25px 0; font-weight: bold; font-size: 16px; color: #3e3d3c;">Пинкод для регистрации в Основную программу (с номинальной стоимостью 160 000тг)</p>
  <?php if(!$pin): ?>
      <?php echo e(Form::open(['route' => 'pin.add', 'method'=>'post'])); ?>

        <?php echo csrf_field(); ?>
        <input type="text" name="program_id" value="3" hidden="hidden">
        <input type="text" name="program_type" value="3" hidden="hidden">
        <button class="btn btn-lg btn-success" type="submit" onclick="return confirm('Вы уверены?')">
          <i class="fa fa-dot-circle-o"></i> Сгенерировать
        </button>  
      <?php echo e(Form::close()); ?>  
 <?php else: ?>

                  <div class="row">
                    <div class="col-md-4"><b><?php echo e($pin->pin); ?></b></div>
                    <div class="col-md-6">
                      <?php if( $pin->status_id == 0 ): ?>
                      <pre>Действителен до: <br/><?php echo e($pin->expired_at); ?></pre>
                      <?php else: ?>
                      <pre style="color: red;">Заблокирован</pre>
                      <?php endif; ?>
                    </div>
                  </div>

 <?php endif; ?>





		</div>
	</div>

</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
<script type="text/javascript">


$(document).ready(function(){
  $('#form-click').on( 'click', function(e){
  e.preventDefault();
  $('.profile_table').toggleClass('active');
  })

  $(":input").inputmask();

  <?php if( isset($pin) ): ?>

  function makeTimer() {

  //    var endTime = new Date("29 April 2018 9:56:00 GMT+01:00");  
    var endTime = new Date("<?php echo e($pin->expired_at); ?>");     
      endTime = (Date.parse(endTime) / 1000);

      var now = new Date();
      now = (Date.parse(now) / 1000);

      var timeLeft = endTime - now;

      var days = Math.floor(timeLeft / 86400); 
      var hours = Math.floor((timeLeft - (days * 86400)) / 3600);
      var minutes = Math.floor((timeLeft - (days * 86400) - (hours * 3600 )) / 60);
      var seconds = Math.floor((timeLeft - (days * 86400) - (hours * 3600) - (minutes * 60)));
  
      if (hours < "10") { hours = "0" + hours; }
      if (minutes < "10") { minutes = "0" + minutes; }
      if (seconds < "10") { seconds = "0" + seconds; }

      $("#days").html(days + "<span>Дня</span>");
      $("#hours").html(hours + "<span>Часов</span>");
      $("#minutes").html(minutes + "<span>Минут</span>");
      $("#seconds").html(seconds + "<span>Секунд</span>");    

  }

  setInterval(function() { makeTimer(); }, 1000);

  <?php endif; ?>;

});




</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('front.layout.main', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>