<?php $__env->startSection('content'); ?>

<div class="cabinet_main_container authorization">
  <div class="header">
    <div class="row">
      <div class="col-md-10">
        <h2 class="title">Завершение программы</h2>        
      </div>
      <div class="col-md-2">
        <a class="link" href="#" onclick="document.getElementById('logout-form').submit(); return false;">
          <i class="icon-logout"></i>выход
        </a>
        <form id="logout-form" action="<?php echo e(route('partner.logout')); ?>" method="POST" style="display: none;">
          <?php echo csrf_field(); ?>
        </form>
      </div>
    </div>

  </div>
	<div class="body">
		<div class="auth_cont" style="max-width: 100%;">



<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingOne">
      <h4 class="panel-title">
        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
          Получить вознаграждение (25 000 тг.)
        </a>
      </h4>
    </div>
    <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
      <div class="panel-body">

      <div style="text-align: left;">
              Уважаемые партнёры, согласно Налоговому Кодексу РК партнерам для получения вознаграждения 800 000 и 3 000 000 тенге необходимо так же предоставить СЧЕТ- ФАКТУРУ за реализацию товара !
              <br/>
              Внимание счет-фактура нужна только для граждан Республики Казахстан ! 
              <br/>
              счет-фактуру заполнить в печатной форме. Ручкой нельзя.
              <br/>
              Если, у вас ИП без НДС то указываете без НДС и поставить печать ИП, если имеется. Если, у вас ИП с НДС то указываете 12% и поставить печать ИП если имеется .
              <br/>
              Счет-фактура, Акт Выполненных работ, Договор и Акт Сверки предоставить в оригинале.
              <br/>
              Наличными деньги выдаются при наличии фискального чека с кассового аппарат. Для перечисление вознаграждение счет должен быть оформлен на ИП.
              <br/><br/>
              <br/><br/>
              Образец Счет Фактуры <a href="#">скачать</a>
              <br/><br/>
              Счет Фактура <a href="#">скачать</a>
              <br/><br/>
              Образец   Акт Выполненных Работ для граждан Казахстан <a href="#">скачать</a>
              Образец   Акт Выполненных Работ для не граждан РК (Нерезидент РК)  <a href="#">скачать</a>
              <br/><br/>
              Акт Выполненных Работ для граждан Казахстан <a href="#">скачать</a>
              <br/><br/>
              Акт Выполненных Работ для граждан России <a href="#">скачать</a>
              <br/><br/>
              Договор <a href="#">скачать</a>
              Расписка <a href="#">скачать</a>
              <br/><br/>
              <br/><br/>
      </div>

			<?php echo $__env->make('bonus-request.form_2', ['progs' => $progs], \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      </div>
    </div>
  </div>

  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingThree">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
          Форма на вознаграждение + регистрация в Авто программу 
        </a>
      </h4>
    </div>
    <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
      <div class="panel-body">

        <?php echo $__env->make('bonus-request.form_2', ['progs' => $progs], \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>

      </div>
    </div>
  </div>
</div>






		</div>
		</div>

				</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>


<?php $__env->stopSection(); ?>
<?php echo $__env->make('front.layout.main', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>