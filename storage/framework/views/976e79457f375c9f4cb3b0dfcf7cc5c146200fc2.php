<?php $__env->startSection('breadcrumbs'); ?>
	<li class="breadcrumb-item active">Склад </li>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
		<div class="row">
			<div class="col-md-12" style="text-align: right; padding: 6px 48px 35px 0;">        		
				<a href="/admin/bookkeeping/warehouse" style="color: #00a195; font-weight: bold; margin-right: 15px;">Вознаграждения</a>
				<a href="/admin/bookkeeping" style="color: #2575ce; font-weight: bold; margin-right: 15px;">Склад</a>
				<a href="/admin/bookkeeping/credit" style="color: #00a195; font-weight: bold;">Поступления</a>
			</div>
		</div>

    <div class="card-body warehouse-body">
        <div class="row">
        	<div class="col-md-12">        		
	          <?php if(Session::has('message')): ?>
	                  <div class="card-header">
	            <p class="alert <?php echo e(Session::get('alert-class', 'alert-info')); ?>"><?php echo e(Session::get('message')); ?></p>
	                   </div>
	          <?php endif; ?>
        	</div>
            <div class="col-md-12 col-xs-12 col-sm-12">
                <div class="card">
                    <div class="card-body">

                        <div class="table-responsive grid-wrapper">
                        	<?php echo $grid; ?>

                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="pull-left">
                            <b></b>
                        </div>
                        <div class="pull-right"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>



<?php $__env->stopSection(); ?>


<?php $__env->startSection('scripts'); ?>
<script type="text/javascript">
	$('.dates').daterangepicker({
		singleDatePicker: true,
	});

	$(document).ready( function() {

		$('#check_login').on('click', function(e){
			e.preventDefault();
			$('.message-response').fadeOut();
			$('.form-part').fadeOut();

			var uname = $('#uname-check').val();
			var program = $('#program').val();

			if( uname ){
				$.get( "check_login", { name: uname, program: program} )
				  .done(function( data ) {
				  	console.log(data);

				  	if(data.error == 1 ){
				  		$('.message-response').html('<p class="error-message">' + data.m + '</p>' );
				  		$('.message-response').fadeIn();
				  	} else {
				  		$('.message-response').html('<p class="succses-message">' + data.m + '</p>' );
				  		$('#user_fullname').val(data.m);
				  		$('.message-response').fadeIn();
				  		$('.form-part').fadeIn();

				  	}
				});				
			} else {
				$('#uname-check').focus();
				return true;
			}
		})
	})


</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>