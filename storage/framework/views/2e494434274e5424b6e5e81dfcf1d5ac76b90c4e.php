<?php $__env->startSection('content'); ?>

 <div class="card">


          <?php if(Session::has('message')): ?>
                  <div class="card-header">
            <p class="alert <?php echo e(Session::get('alert-class', 'alert-info')); ?>"><?php echo e(Session::get('message')); ?></p>
                   </div>
          <?php endif; ?>
                  <div class="card-body">
            <?php echo $grid; ?>

                  </div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
<script type="text/javascript">
      	function reject(){
      		if($('#cancel_reason').val() == ''){
	      		$('#cancel_reason').addClass('form-control-danger').focus();
	      		$('.invalid-feedback').fadeIn();
      		} else {
      			$('#status_id').val(2);
      			$('#swap_form').submit();
      		}
      	}
</script>

  <script type="text/javascript">
    var p1, p2, l1, l2, m1, m2;
    function check($i){
      
      var input = $('#login-input-'+$i)
      var login = input.val();
      if(login == ''){
        $('#message-input-'+$i).html('Введите логин');
        input.removeClass('is-valid');  
        input.addClass('is-invalid'); 
        return false;
      } else {
        $.blockUI({ css: { backgroundColor: '#f00', color: '#fff'} });
        $.get( "/usercheck", { login: login } ).done(function(data){
          $.unblockUI();
          if(data.status == 1){
            $('#message-input-'+$i+'2').html(data.message);
            input.removeClass('is-invalid');  
            input.addClass('is-valid');
            if($i == 1){
              p1 = data.position;
              l1 = data.level;
              m1 = data.mx_id;              
            } else {
              p2 = data.position;
              l2 = data.level;
              m2 = data.mx_id;              
            }
          } else {
            $('#message-input-'+$i).html(data.message);
            input.removeClass('is-valid');  
            input.addClass('is-invalid');
          }
        });
      }
    }

    $('#swap_form').submit(function() {
      if(p1 == undefined || l1 == undefined ){
        check(1);
        return false;
      }     

      if(p2 == undefined || l2 == undefined ){
        check(2);
        return false;
      }
      
      if(l1 != l2){
        alert('Уровни участников должны совпадать!')
        return false;
      }
    });
  </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>