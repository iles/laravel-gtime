<style type="text/css">
  .modal-dialog{max-width: 1200px;}
</style>

  <?php if($model->status_id == 0): ?>
   <form method="post" action="<?php echo e(action('PinsController@statusupdate')); ?>" id="create_pin_form" class="form-horizontal" accept-charset="UTF-8"> 
  <?php else: ?>
   <form method="post" action="<?php echo e(action('PinsController@update')); ?>" id="create_pin_form" class="form-horizontal" accept-charset="UTF-8"> 
  <?php endif; ?>

<div class="card">
        <?php echo csrf_field(); ?>
  <div class="card-header">
    <strong><?php echo e($model->uname); ?></strong></div>
    <div class="card-body">

      <div class="row">
        
          <div class="col-md-6">
          
          <div class="form-group row">
            <input type="text" name="id"  value="<?php echo e($model->id); ?>" hidden/>
            <label class="col-md-3 col-form-label" for="hf-password">Логин</label>
            <div class="col-md-9">           
              <input class="form-control" name="name" value="<?php echo e($model->uname); ?>" type="text">
            </div>
          </div>         

          <div class="form-group row">
            <input type="text" name="id"  value="<?php echo e($model->id); ?>" hidden/>
            <label class="col-md-3 col-form-label" for="hf-password">ФИО</label>
            <div class="col-md-9">           
              <input class="form-control" name="name" value="<?php echo e($model->fullname); ?>" type="text">
            </div>
          </div>        

          <div class="form-group row">
            <label class="col-md-3 col-form-label" for="hf-password">ИИН</label>
            <div class="col-md-9">           
              <input class="form-control" name="iin" value="<?php echo e($model->iin); ?>" type="text">
            </div>
          </div>                

          <div class="form-group row">
            <label class="col-md-3 col-form-label" for="hf-password">Номер квитанции</label>
            <div class="col-md-9">           
              <input class="form-control" name="bill" value="<?php echo e($model->bill); ?>" type="text">
            </div>
          </div>            

          <div class="form-group row">
            <label class="col-md-3 col-form-label" for="hf-password">Сумма</label>
            <div class="col-md-9">           
              <input class="form-control" name="summ" value="<?php echo e($model->summ); ?>" type="text">
            </div>
          </div>       

          <div class="form-group row">
            <label class="col-md-3 col-form-label" for="hf-password">Валюта</label>
            <div class="col-md-9">           
              <input class="form-control" name="summ" value="<?php echo e($model->currency); ?>" type="text">
            </div>
          </div>             

          <div class="form-group row">
            <label class="col-md-3 col-form-label" for="hf-password">Skype</label>
            <div class="col-md-9">           
              <input class="form-control" name="skype" value="<?php echo e($model->skype); ?>" type="text">
            </div>
          </div>               

          <div class="form-group row">
            <label class="col-md-3 col-form-label" for="hf-password">Страна</label>
            <div class="col-md-9">           
              <input class="form-control" name="skype" value="<?php echo e($model->country); ?>" type="text">
            </div>
          </div>            

          <div class="form-group row">
            <label class="col-md-3 col-form-label" for="hf-password">Примечание</label>
            <div class="col-md-9">           
              <textarea class="form-control"><?php echo e($model->user_note); ?></textarea>
            </div>
          </div>           

          <div class="form-group row">
            <label class="col-md-3 col-form-label" for="hf-password">Фото квитанции</label>
            <div class="col-md-9">           
                <?php if($model->image != 0): ?>
                    <?php $__currentLoopData = explode(';', $model->image); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $image): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                      <a data-fancybox="gallery" href="/uploads/bills/<?php echo e($image); ?>"><img src="/uploads/bills/<?php echo e($image); ?>" style="width: 80px;"></a>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <?php endif; ?>
            </div>
          </div>      
         
        </div>


        <div class="col-md-6">
            

            <div class="form-group row" style="margin-top: 25px">
          <?php $__currentLoopData = $pins; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $name => $prog): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <?php if( count($prog) > 0 ): ?>
              <div class="col-md-12" style="margin-bottom: 25px">
                <?php switch($name):
                    case ('1'): ?>
                        <p>Старт</p>
                        <?php break; ?>
                    <?php case ('2'): ?>
                        <p>Авто</p>
                        <?php break; ?>                  

                    <?php case ('3'): ?>
                        <p>Основная</p>
                        <?php break; ?>

                    <?php case ('4'): ?>
                        <p>Накопительная</p>
                        <?php break; ?>                  

                    <?php case ('5'): ?>
                        <p>Накопительная +</p>
                        <?php break; ?>                  

                    <?php case ('6'): ?>
                        <p>VIP</p>
                        <?php break; ?>                  

                    <?php case ('7'): ?>
                        <p>FAST</p>
                        <?php break; ?>                    
                    <?php case ('8'): ?>
                        <p>Bonus</p>
                        <?php break; ?>
                    <?php default: ?>
                        <p></p>
                <?php endswitch; ?>
                <?php $__currentLoopData = $prog; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $pin): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                  <div class="row" id="pin<?php echo e($pin->id); ?>prog<?php echo e($pin->program); ?>">
                    <div class="col-md-6"><b><?php echo e($pin->pin); ?></b> <br/> 
                      
                        <?php if($pin->status_id == 0): ?>
                         <pre id="res<?php echo e($pin->id); ?>prog<?php echo e($pin->program); ?>">Действителен до: <br/><?php echo e($pin->expired_at); ?></pre>
                        <?php elseif($pin->status_id == 2): ?>
                         <pre style="color: red;" id="res<?php echo e($pin->id); ?>prog<?php echo e($pin->program); ?>">Заблокирован</pre>
                        <?php elseif($pin->status_id == 3): ?>
                         <pre style="color: teal;" id="res<?php echo e($pin->id); ?>prog<?php echo e($pin->program); ?>">Активирован</pre>
                        <?php endif; ?>
                      
                    </div>
                    <div class="col-md-6">
                      <?php if($pin->status_id == 0): ?>
                        <a href="#" class="pinstat" id="sign<?php echo e($pin->id); ?>prog<?php echo e($pin->program); ?>" onclick="pinstat( <?php echo e($pin->program); ?>, <?php echo e($pin->id); ?>, 2  ); return false;" >Блокировать</a>
                      <?php elseif($pin->status == 1): ?>
                        <a href="#" class="pinstat" id="sign<?php echo e($pin->id); ?>prog<?php echo e($pin->program); ?>" onclick="pinstat( <?php echo e($pin->program); ?>, <?php echo e($pin->id); ?>, 0  ); return false;" >Разблокировать</a>
                      <?php elseif($pin->status == 3): ?>

                      <?php endif; ?>
                      <br/>
                      <a href="#" class="pindelete" data-trigger-confirm="1" onclick="pindelete( <?php echo e($pin->program); ?>, <?php echo e($pin->id); ?>  ); return false;" >Удалить</a>
                      
                    </div>
                  </div>

                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
              </div>
            <?php endif; ?>
          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </div> 


        <div class="form-group row">
          <label class="col-md-3 col-form-label" for="hf-password">Приписка</label>
          <div class="col-md-9">
            <textarea name="note"  class="form-control" ><?php echo e($model->note); ?></textarea>
          </div>
        </div>



        <div class="form-group row extra-pins">
          
        </div>

      <?php if($model->program == 6 && $model->security_check !== 1): ?>

      <?php else: ?>
        <div class="form-group row">
          <div class="col-md-12">
            <div class="row"> <div class="col-md-12"><p><b>Генерация пин кодов:</b></p></div> </div>
            <div class="row">
              <label class="col-md-4 col-form-label" for="hf-password">Старт</label>
              <div class="col-md-8">           
                <input type="number" min="0" name="start_pin" >
              </div>            
            </div>          
            <div class="row">
              <label class="col-md-4 col-form-label" for="hf-password">Накопительная</label>
              <div class="col-md-8">           
                <input type="number" min="0" name="bank_pin" >
              </div>            
            </div>            
        
            <div class="row">
              <label class="col-md-4 col-form-label" for="hf-password">Основная</label>
              <div class="col-md-8">           
                <input type="number" min="0" name="main_pin" >
              </div>            
            </div>           

            <div class="row">
              <label class="col-md-4 col-form-label" for="hf-password">Vip</label>
              <div class="col-md-8">           
                <input type="number" min="0" name="vip_pin" >
              </div>            
            </div>             
          

            <div class="row">
              <label class="col-md-4 col-form-label" for="hf-password">Fast</label>
              <div class="col-md-8">           
                <input type="number" min="0" name="fast_pin" >
              </div>            
            </div>

            <div class="row">
              <label class="col-md-4 col-form-label" for="hf-password">Bonus</label>
              <div class="col-md-8">           
                <input type="number" min="0" name="bonus_pin" >
              </div>            
            </div>
          </div>
        </div> 
        <?php endif; ?>


        <?php if($model->status_id == 0): ?>
        <br><br><br>

        <div class="form-group row">
          <input type="text" name="id" value="<?php echo e($model->id); ?>" hidden/>
          <input type="text" name="status" id="status_id" value="1" hidden/>
          <label class="col-md-3 col-form-label" for="hf-password">Причина отказа</label>
          <div class="col-md-9">           
            <textarea name="cancel_reason"  class="form-control" id="cancel_reason"><?php echo e($model->cancel_reason); ?></textarea>
          </div>
          <div class="invalid-feedback">Укажите причину отказа</div>
        </div>          

        

        </div>



      </div>

        




    


        
        <?php endif; ?>

        
    </div>
            </div>
    <div class="card-footer">
      
      <?php if($model->program == 6 && $model->security_check !== 1): ?>
        <strong>Заявка не одобрена службой безопасности</strong>
      <?php else: ?>

      <button class="btn btn-sm btn-primary" type="submit">
      <i class="fa fa-dot-circle-o" type="submit" ></i> 
      <?php
       echo $model->status_id == 0 ? 'Одобрить' : 'Сохранить';
      ?>
      </button>

      <?php if($model->status_id == 0): ?>
        <button class="btn btn-sm btn-danger" onclick="reject(); return false;">      
        <i class="fa fa-ban"></i> Отклонить</button>
      <?php endif; ?>


      <?php endif; ?>
</div>
</form>


    
</div>