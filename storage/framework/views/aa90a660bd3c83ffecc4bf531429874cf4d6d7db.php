
<style type="text/css">
  #bootstrap_modal .modal-dialog{
    max-width: 1200px;
  }
</style>




<div class="card">

<ul class="nav nav-pills mb-3" id="pills-tab" role="tablist" style="padding: 15px 15px 5px;">
  <li class="nav-item">
    <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#info-tab" role="tab" aria-controls="pills-home" aria-selected="true">Инфо</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#profile" role="tab" aria-controls="pills-profile" aria-selected="false">Кого пригласил</a>
  </li> 
</ul>


<div class="card">
      <div class="tab-content" id="myTabContent">


          <div class="tab-pane fade show active" style="padding: 0" id="info-tab" role="tabpanel" aria-labelledby="home-tab">
   <form method="post" action="<?php echo e(action('BonusController@statusupdate')); ?>" id="update_bonus_form" class="form-horizontal" accept-charset="UTF-8"> 


    <div class="card-body">
        <?php echo csrf_field(); ?>

        <div class="row">
          <div class="col-md-6">

        <div class="form-group row">            
            <?php echo e(Form::label('program_id', 'Программа:', array('class' => 'col-md-3 col-form-label'))); ?>

          <div class="col-md-9">
              <?php echo e(Form::select('program_id', $progs, $model->program_id, array('class' => 'form-control', 'required'=>'required')  )); ?>

          </div>
        </div>              

        <div class="form-group row">            
          <?php echo e(Form::label('uname', 'Логин:', array('class' => 'col-md-3 col-form-label'))); ?>

          <div class="col-md-9">
            <?php echo e(Form::text('uname', $model->uname,  array('class' => 'form-control', 'required'=>'required') )); ?>

          </div>
        </div>                

    <?php
     $role = Auth::guard('web')->user()->roles()->first()->name;
    ?>

        <?php if($role = 'superadmin' || $role == 'admin'): ?>
        <div class="form-group row">            
          <?php echo e(Form::label('fio', 'ФИО профиля:', array('class' => 'col-md-3 col-form-label'))); ?>

          <div class="col-md-9">
            <?php echo e(Form::text('fio', $fio,  array('class' => 'form-control', 'required'=>'required') )); ?>

          </div>
        </div>        
        <?php endif; ?>

        <div class="form-group row">            
          <?php echo e(Form::label('summ', 'Сумма:', array('class' => 'col-md-3 col-form-label'))); ?>

          <div class="col-md-9">
            <?php echo e(Form::number('summ', $model->summ,  array('class' => 'form-control', 'required'=>'required') )); ?>

          </div>
        </div>

        <div class="form-group row">            
          <?php echo e(Form::label('firstname', 'Имя:', array('class' => 'col-md-3 col-form-label'))); ?>

          <div class="col-md-9">
            <?php echo e(Form::text('firstname', $model->firstname,  array('class' => 'form-control', 'required'=>'required') )); ?>

          </div>
        </div>        

        <div class="form-group row">            
          <?php echo e(Form::label('firstname', 'Фамилия:', array('class' => 'col-md-3 col-form-label'))); ?>

          <div class="col-md-9">
            <?php echo e(Form::text('lastname', $model->lastname,  array('class' => 'form-control', 'required'=>'required') )); ?>

          </div>
        </div>       

         <div class="form-group row">            
          <?php echo e(Form::label('patronic', 'Отчество:', array('class' => 'col-md-3 col-form-label'))); ?>

          <div class="col-md-9">
            <?php echo e(Form::text('patronic', $model->patronic,  array('class' => 'form-control', 'required'=>'required') )); ?>

          </div>
        </div>         

        <div class="form-group row">            
          <?php echo e(Form::label('iin', 'ИИН:', array('class' => 'col-md-3 col-form-label'))); ?>

          <div class="col-md-9">
            <?php echo e(Form::number('iin',  $model->iin,  array('class' => 'form-control', 'required'=>'required') )); ?>

          </div>
        </div>        

        <div class="form-group row">            
          <?php echo e(Form::label('bill_num', 'Номер счета :', array('class' => 'col-md-3 col-form-label'))); ?>

          <div class="col-md-9">
            <?php echo e(Form::text('bill_num', $model->bill_num,  array('class' => 'form-control', 'required'=>'required') )); ?>

          </div>
        </div>           

         <div class="form-group row">            
          <?php echo e(Form::label('bank_name', 'Наименование банка :', array('class' => 'col-md-3 col-form-label'))); ?>

          <div class="col-md-9">
            <?php echo e(Form::text('bank_name', $model->bank_name,  array('class' => 'form-control', 'required'=>'required') )); ?>

          </div>
        </div>            

     
        <?php if($model->vip_bonus_logins): ?>
         <div class="form-group row">            
          <?php echo e(Form::label('country', 'Логины (бонус):', array('class' => 'col-md-3 col-form-label'))); ?>

          <div class="col-md-9">

              <?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $k => $v): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <?php if(!$v): ?>
                Логина <b><?php echo e($v); ?></b> не существует
                <?php else: ?>
                <b> <?php echo e($v->name); ?> </b><br>
               <?php echo e(gmdate("Y-m-d", $v->reg_date)); ?> <br>
                 <span>ПИН: </span><b><?php echo e($v->screch); ?> </b>
                 <br>
                 <span>TOVAR: </span><b><?php echo e($v->tovar); ?> </b>
                 <br>
                 <br>
              <br/>
                <?php endif; ?>
              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
          </div>
        </div>   
        <?php endif; ?> 

          </div>  

          <div class="col-md-6">
            
   <div class="form-group row">            
          <?php echo e(Form::label('BIK', 'BIK :', array('class' => 'col-md-3 col-form-label'))); ?>

          <div class="col-md-9">
            <?php echo e(Form::text('BIK', $model->BIK,  array('class' => 'form-control', 'required'=>'required') )); ?>

          </div>
        </div>          

        <div class="form-group row">            
          <?php echo e(Form::label('card_number', 'Номер карты :', array('class' => 'col-md-3 col-form-label'))); ?>

          <div class="col-md-9">
            <?php echo e(Form::text('card_number', $model->card_number,  array('class' => 'form-control') )); ?>

          </div>
        </div>        

        <div class="form-group row">            
          <?php echo e(Form::label('pens', 'Пенсионер :', array('class' => 'col-md-3 col-form-label'))); ?>

          <div class="col-md-9">
            <?php echo e(Form::text('pens', $model->pens,  array('class' => 'form-control') )); ?>

          </div>
        </div>             


        <?php if($model->inv): ?>

        <div class="form-group row">            
          <?php echo e(Form::label('inv', 'Инвалидность :', array('class' => 'col-md-3 col-form-label'))); ?>

          <div class="col-md-9">
            <?php echo e(Form::text('inv', $model->inv,  array('class' => 'form-control') )); ?>

          </div>
        </div>      


        <div class="form-group row">            
          <?php echo e(Form::label('inv_group', 'Группа инвалидности :', array('class' => 'col-md-3 col-form-label'))); ?>

          <div class="col-md-9">
            <?php echo e(Form::text('inv_group', $model->inv_group,  array('class' => 'form-control') )); ?>

          </div>
        </div>          

         <div class="form-group row">            
          <?php echo e(Form::label('inv_start', 'Срок инвалидности :', array('class' => 'col-md-3 col-form-label'))); ?>

          <div class="col-md-9">
            <?php echo e(Form::text('inv_start', $model->inv_start,  array('class' => 'form-control') )); ?>

            <?php echo e(Form::text('inv_end', $model->inv_end,  array('class' => 'form-control') )); ?>

          </div>
        </div>            

        <div class="form-group row">            
          <?php echo e(Form::label('inv_timeless', 'Без срока:', array('class' => 'col-md-3 col-form-label'))); ?>

          <div class="col-md-9">
            <?php echo e(Form::text('inv_timeless', $model->inv_timeless,  array('class' => 'form-control') )); ?>

          </div>
        </div>  
                 
        <?php endif; ?>

        <?php if( $model->notorios == 1): ?>
        <div class="form-group row">            
          <?php echo e(Form::label('notorios', 'По нотариальной доверенности:', array('class' => 'col-md-3 col-form-label'))); ?>

          <div class="col-md-9">
            <div class="row">
              <?php echo e(Form::label('notorios', 'ФИО:', array('class' => 'col-md-3 col-form-label'))); ?>

              <?php echo e(Form::text('not_fio', $model->not_fio,  array('class' => 'col-md-9 form-control') )); ?>

            </div>            
            <div class="row">
              <br/>
              <?php echo e(Form::label('notorios', 'Адрес:', array('class' => 'col-md-3 col-form-label'))); ?>

              <?php echo e(Form::text('not_adress', $model->not_adress,  array('class' => 'col-md-9 form-control') )); ?>

            </div>
            <div class="row">
              <br/>
              <?php echo e(Form::label('notorios', 'ИИН:', array('class' => 'col-md-3 col-form-label'))); ?>

              <?php echo e(Form::text('not_iin', $model->not_iin,  array('class' => 'col-md-9 form-control') )); ?>

            </div>            
            <div class="row">
              <br/>
              <?php echo e(Form::label('notorios', 'Пасспорт:', array('class' => 'col-md-3 col-form-label'))); ?>

              <?php echo e(Form::text('not_passport', $model->not_passport,  array('class' => 'col-md-9 form-control') )); ?>

            </div>
          </div>
        </div>  
        <?php endif; ?>         


         <div class="form-group row">            
          <?php echo e(Form::label('adress', 'Адрес:', array('class' => 'col-md-3 col-form-label'))); ?>

          <div class="col-md-9">
            <?php echo e(Form::text('adress', $model->adress,  array('class' => 'form-control') )); ?>

          </div>
        </div>    

         <div class="form-group row">            
          <?php echo e(Form::label('fact_adress', 'Фактический адрес:', array('class' => 'col-md-3 col-form-label'))); ?>

          <div class="col-md-9">
            <?php echo e(Form::text('fact_adress', $model->fact_adress,  array('class' => 'form-control') )); ?>

          </div>
        </div>    

         <div class="form-group row">            
          <?php echo e(Form::label('country', 'Страна/город:', array('class' => 'col-md-3 col-form-label'))); ?>

          <div class="col-md-9">
            <?php echo e(Form::text('country', $model->country,  array('class' => 'form-control') )); ?>

          </div>
        </div>    
        

  

        <div class="form-group row">            
          <?php echo e(Form::label('image', 'Скан документа:', array('class' => 'col-md-3 col-form-label'))); ?>   
          <div class="col-md-9">

            <?php $__currentLoopData = explode(';', $model->image); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $image): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
              <a data-fancybox="gallery" href="/uploads/scans/<?php echo e($image); ?>"><img src="/uploads/scans/<?php echo e($image); ?>" style="width: 80px;"></a>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
          </div>
        </div>  

        <input type="text" name="id" value="<?php echo e($model->id); ?>" hidden/>     
        <input type="text" name="status" id="status_id" value="1" hidden/>
        <textarea name="cancel_reason"  class="form-control" id="cancel_reason"><?php echo e($model->cancel_reason); ?></textarea>        
        <div class="invalid-feedback">Укажите причину отказа</div>

          </div>
        </div>
       
    </div>
        
    <div class="card-footer">

      <?php if($transfer && $transfer->status_id == 0): ?>
        Передача пин кодов не одобрена сб
      <?php else: ?>
      <button class="btn btn-sm btn-primary" type="submit">
      <i class="fa fa-dot-circle-o" type="submit"></i> Одобрить</button>
      <button class="btn btn-sm btn-danger" onclick="reject(); return false;">
      <i class="fa fa-ban"></i> Отклонить </button>
      <?php endif; ?>
    </div>
    </form>
   <?php echo e(Form::close()); ?>

</div>
            <div class="tab-pane fade" style="padding: 0" id="profile" role="tabpanel" aria-labelledby="profile-tab">
              <div class="card-body">

                <?php if(!$refers->isEmpty() ): ?>
                <table class="table table-responsive-sm">
                  <thead>
                  <tr>
                  <th>Логин</th>
                  <th>ФИО</th>
                  <th>ПИН код</th>
                  <th>Дата регистации</th>
                  </tr>
                  </thead>
                  <tbody>
                    <?php $__currentLoopData = $refers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <tr>
                      <td><?php echo e($user->name); ?></td>
                      <td><?php echo e($user->familiya); ?></td>
                      <td><?php echo e($user->screch); ?></td>
                      <td><?php echo e(gmdate("Y-m-d", $user->reg_date)); ?></td>
                    </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                  </tbody>
                  </table>
                  <?php else: ?>
                  Никого не пригласил
                  <?php endif; ?>
</div>

            </div>

</div>
</div>
</div>

<style type="text/css">
  .cabinet_main_container .auth_cont .form-control {
    margin-top: 0 !important;
}
</style>


