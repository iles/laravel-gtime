<div class="card">

      <?php echo e(Form::open(['route' => 'bonus.add', 'method'=>'post', 'autocomplete' => 'off', 'files' => true])); ?>


    <div class="card-body">
        <?php echo csrf_field(); ?>

        <div class="form-group row">
          <?php echo e(Form::label('uname', 'Логин:', array('class' => 'col-md-3 col-form-label'))); ?>

          <div class="col-md-9">
            <?php echo e(Form::text('uname', Auth::user()->name,  array('class' => 'form-control', 'required'=>'required', 'readonly' => 'readonly') )); ?>

          </div>
        </div>

        <div class="form-group row hidden">            
          <?php echo e(Form::label('summ', 'Сумма:', array('class' => 'col-md-3 col-form-label'))); ?>

          <div class="col-md-9">
            <?php echo e(Form::number('summ', 80000,  array('class' => 'form-control', 'required'=>'required',  'readonly' => 'readonly') )); ?>

          </div>
        </div>

        <div class="form-group row">            
          <?php echo e(Form::label('firstname', 'Имя:', array('class' => 'col-md-3 col-form-label'))); ?>

          <div class="col-md-9">
            <?php echo e(Form::text('firstname', Auth::user()->firstname,  array('class' => 'form-control', 'required'=>'required') )); ?>

          </div>
        </div>        

        <div class="form-group row">            
          <?php echo e(Form::label('firstname', 'Фамилия:', array('class' => 'col-md-3 col-form-label'))); ?>

          <div class="col-md-9">
            <?php echo e(Form::text('lastname', Auth::user()->lastname,  array('class' => 'form-control', 'required'=>'required') )); ?>

          </div>
        </div>       

         <div class="form-group row">            
          <?php echo e(Form::label('patronic', 'Отчество:', array('class' => 'col-md-3 col-form-label'))); ?>

          <div class="col-md-9">
            <?php echo e(Form::text('patronic', Auth::user()->patronic,  array('class' => 'form-control', 'required'=>'required') )); ?>

          </div>
        </div>         

        <div class="form-group row">            
          <?php echo e(Form::label('iin', 'ИИН:', array('class' => 'col-md-3 col-form-label'))); ?>

          <div class="col-md-9">
            <?php echo e(Form::number('iin', Input::old('iin'),  array('class' => 'form-control', 'required'=>'required') )); ?>

          </div>
        </div>        

        <div class="form-group row">            
          <?php echo e(Form::label('bill_num', 'Номер счета :', array('class' => 'col-md-3 col-form-label'))); ?>

          <div class="col-md-9">
            <?php echo e(Form::text('bill_num', Input::old('bill_num'),  array('class' => 'form-control', 'required'=>'required') )); ?>

          </div>
        </div>     

        <div class="form-group row">            
          <?php echo e(Form::label('bank_name', 'Наименование банка :', array('class' => 'col-md-3 col-form-label'))); ?>

          <div class="col-md-9">
            <?php echo e(Form::text('bank_name', Input::old('bank_name'),  array('class' => 'form-control', 'required'=>'required') )); ?>

          </div>
        </div>        

        <div class="form-group row">            
          <?php echo e(Form::label('BIK', 'BIK :', array('class' => 'col-md-3 col-form-label'))); ?>

          <div class="col-md-9">
            <?php echo e(Form::text('BIK', Input::old('BIK'),  array('class' => 'form-control', 'required'=>'required') )); ?>

          </div>
        </div>


        <?php if( Auth::user()->nonResident() ): ?>
        <div class="form-group row">            
          <?php echo e(Form::label('card_number', 'Номер карточки :', array('class' => 'col-md-3 col-form-label'))); ?>

          <div class="col-md-9">
            <?php echo e(Form::number('card_number', Input::old('card_number'),  array('class' => 'form-control', 'required'=>'required') )); ?>

          </div>
        </div>        

        <div class="form-group row">            
          <?php echo e(Form::label('bank_account', 'Счет банка (Корреспондентский) :', array('class' => 'col-md-3 col-form-label'))); ?>

          <div class="col-md-9">
            <?php echo e(Form::text('bank_account', Input::old('bank_account'),  array('class' => 'form-control', 'required'=>'required') )); ?>

          </div>
        </div>        

        <div class="form-group row">            
          <?php echo e(Form::label('country', 'Страна / Город:', array('class' => 'col-md-3 col-form-label'))); ?>

          <div class="col-md-9">
            <?php echo e(Form::text('country', Input::old('country'),  array('class' => 'form-control', 'required'=>'required') )); ?>

          </div>
        </div>    

        <div class="form-group row">            
          <?php echo e(Form::label('adress', 'Адрес проживания:', array('class' => 'col-md-3 col-form-label'))); ?>

          <div class="col-md-9">
            <?php echo e(Form::text('adress', Input::old('adress'),  array('class' => 'form-control', 'required'=>'required') )); ?>

          </div>
        </div>        

        <div class="form-group row">            
          <?php echo e(Form::label('adress', 'Фактический адрес проживания:', array('class' => 'col-md-3 col-form-label'))); ?>

          <div class="col-md-9">
            <?php echo e(Form::text('fact_adress', Input::old('fact_adress'),  array('class' => 'form-control', 'required'=>'required') )); ?>

          </div>
        </div>
        <?php endif; ?> 

        <?php if( !Auth::user()->nonResident() ): ?>
        <div class="form-group row" style="margin-top: 35px;">            
          <div class="col-md-3"></div>
          <div class="col-md-9" style="text-align: left;">
            <?php echo e(Form::label('pens', 'Пенсионер', array('class' => 'col-form-label'))); ?>

            <?php echo e(Form::checkbox('pens', '1')); ?>

            <?php echo e(Form::label('inv2', 'Инвалидность', array('style'=>'margin-left: 30px;', 'class' => 'col-form-label'))); ?> 
			<?php echo e(Form::checkbox('inv',1,null, array('id'=>'inv2'))); ?>

          </div>
        </div>

        <div class="form-group row">  
          <div class="col-md-3"></div>          
          <div class="col-md-9" style="text-align: left;">
        
        <div class="row inv2-row" style="margin: 20px 0;">
          <div class="col-md-5">
            <?php echo e(Form::label('inv_group', 'Группа инвалидности :', array('class' => 'col-form-label'))); ?> <br/> <br/>
            <?php echo e(Form::select('inv_group', array('1' => 'Первая группа', '2' => 'Вторая группа', '3' => 'Третья группа'), null, array('class' => 'form-control') )); ?>              
          </div>
          
          <div class="col-md-7">
            <div class="row">
                <div class="col-md-12">
                  <?php echo e(Form::label('image', 'Срок инвалидности:', array('class' => 'col-form-label'))); ?>    <br/> <br/>               
                </div>
                <div class="col-md-6 inv-time">
                  <?php echo e(Form::text('inv_start',  null, ['class'=>'form-control datepicker-here', 'placeholder'=>'С:', 'data-position' => 'left top', 'readonly'=>'readonly', 'required'=>'required'])); ?>                  
                </div>                
                <div class="col-md-6 inv-time">
                  <?php echo e(Form::text('inv_end',  null, ['class'=>'form-control datepicker-here', 'placeholder'=>'ПО:', 'data-position' => 'left top', 'readonly'=>'readonly', 'required'=>'required'])); ?>                  
                </div>
                <div class="col-md-12" style="margin-top: 15px;">
                  <?php echo e(Form::label('inv_timeless', 'Без срока переосвидетельствования', array('style'=>'margin-right: 10px;', 'class' => 'col-form-label'))); ?>

                  <?php echo e(Form::checkbox('inv_timeless', '1')); ?>                  
                </div>
              </div>
            </div>
        </div>

          </div>
        </div>
        <?php endif; ?>
        <div class="form-group row">            
          <?php echo e(Form::label('notorios', 'По нотариальной доверенности', array('class' => 'col-md-3 col-form-label'))); ?>

          <div class="col-md-9" style="text-align: left;">
            <?php echo e(Form::checkbox('notorios', '1')); ?>  
          </div>
        </div>         

          
        <div class="form-group row not-row">  
          <div class="col-md-3"></div>          
          <div class="col-md-9" style="text-align: left;">
            <b>Укажите данные:</b>
            <br/>
            <br/>
            <?php echo e(Form::text('not_fio',  null, ['class'=>'form-control', 'placeholder'=>'ФИО:'])); ?><br/>
            <?php echo e(Form::text('not_adress',  null, ['class'=>'form-control', 'placeholder'=>'Адрес:'])); ?><br/>
            <?php echo e(Form::text('not_iin',  null, ['class'=>'form-control', 'placeholder'=>'ИИН:'])); ?><br/>
            <?php echo e(Form::text('not_passport',  null, ['class'=>'form-control', 'placeholder'=>'№ пасспорта:'])); ?><br/>

          </div>
        </div>


         
        <br/>
        <div class="form-group row">         
          <?php echo e(Form::label('image', 'Скан документа :', array('class' => 'col-md-3 col-form-label'))); ?>

          <div class="col-md-9">
          <?php echo e(Form::file('images[]', array('class' => 'form-control', 'required'=>'required', 'multiple'=>true, 'accept'=>".jpeg, .jpg, .png, .pdf") )); ?>

          </div>
        </div>          

         <div class="form-group row" style="display: none;">            
            <?php echo e(Form::text('uid', Auth::user()->user_id,  array('class' => 'form-control', 'required'=>'required') )); ?>

        </div>        

     
     

        
    </div>
        
    <div class="card-footer">
      <button class="btn btn-sm btn-primary" type="submit">
        <i class="fa fa-dot-circle-o"></i> Сохранить
      </button>     

   </div>
   <?php echo e(Form::close()); ?>


</div>

<style type="text/css">
  .cabinet_main_container .auth_cont .form-control {
    margin-top: 0 !important;
}
</style>

  <script type="text/javascript">

      $('.manage-btn').on('click', function(e){
        e.preventDefault();
        if( $(this).attr('id') == 'stat2'){
          if( $('#cancel_reason').val() == '' ){
            $('.invalid-feedback').fadeIn();
            return false;
          } else {
            $('#form2').submit();
          }
        } else {
            $('#form1').submit();          
        }

      })

  </script>
