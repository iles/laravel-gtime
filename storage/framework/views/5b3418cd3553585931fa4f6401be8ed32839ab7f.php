<?php if(call_user_func($renderIf) === true): ?>
    <div class="btn-group pull-right grid-export-button" role="group" title="<?php echo e($title); ?>">
        <button id="export-button" type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown"
                aria-haspopup="true" aria-expanded="false">
            <i class="fa <?php echo e($icon); ?>"></i>&nbsp;<?php echo e($name); ?>

        </button>
        <div class="dropdown-menu" aria-labelledby="export-button">
            <?php $__currentLoopData = $exportOptions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $k => $v): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <a href="<?php echo e($v['url']); ?>" class="dropdown-item" title="<?php echo e($v['title']); ?>" aria-labelledby="<?php echo e($k); ?>">
                    <i class="fa fa-<?php echo e($v['icon']); ?>"></i>&nbsp;<?php echo e($v['name']); ?>

                </a>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </div>
    </div>
<?php endif; ?>