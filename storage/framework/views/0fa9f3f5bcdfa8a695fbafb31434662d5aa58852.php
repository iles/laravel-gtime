<div class="card">
   <form method="post" action="<?php echo e(action('AdminController@store')); ?>" onsubmit="checkPasswords(event)" class="form-horizontal" accept-charset="UTF-8"> 
  <div class="card-header">
    <strong>Добавить пользователя</strong></div>
    <div class="card-body">
        <?php echo csrf_field(); ?>

        <div class="form-group row">
          <label class="col-md-3 col-form-label" for="hf-password">Логин</label>
          <div class="col-md-9">           
            <input class="form-control" name="name" required="required" type="text">
          </div>
        </div>        

        <div class="form-group row">
          <label class="col-md-3 col-form-label" for="hf-password">EMAIL</label>
          <div class="col-md-9">           
            <input class="form-control" name="email" required="required" type="text">
          </div>
        </div>            

        <div class="form-group row">
          <label class="col-md-3 col-form-label" for="hf-password">Пароль</label>
          <div class="col-md-9">           
            <input class="form-control" name="password" required="required" type="password">
          </div>
        </div>            

        <div class="form-group row">
          <label class="col-md-3 col-form-label" for="hf-password">Повторите Пароль</label>
          <div class="col-md-9">           
            <input class="form-control" name="password2" required="required" type="password">
          </div>
        </div>             

        <div class="form-group row">
          <label class="col-md-3 col-form-label" for="hf-password">Роль</label>
          <div class="col-md-9">
          <?php
            $roles = \Spatie\Permission\Models\Role::all()->pluck('name','id');
          ?> 
          <select class="form-control" name="role">
            <?php $__currentLoopData = $roles; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $role): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
              <option value="<?php echo e($role); ?>"><?php echo e($role); ?></option>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
          </select>
          </div>
        </div>                


        
    </div>
        
    <div class="card-footer">
      <button class="btn btn-sm btn-primary" type="submit">
      <i class="fa fa-dot-circle-o" type="submit"></i> Сохранить</button>
    </div>
    </form>
</div>
