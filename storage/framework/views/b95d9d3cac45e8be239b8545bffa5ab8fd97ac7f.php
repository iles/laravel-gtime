<style type="text/css">
  .modal-dialog{max-width: 1200px;}
</style>



<div class="card">
        <?php echo csrf_field(); ?>
  <div class="card-header">
    <strong><?php echo e($finished->fullname); ?></strong>
  </div>

  <ul class="nav nav-tabs" id="myTab" role="tablist">
    <li class="nav-item">
      <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Инфо</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Последние логины</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Кого пригласил</a>
    </li>
  </ul>


  <div class="tab-content" id="myTabContent">
    <div class="tab-pane card-body fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
      <div class="row">

     <form action="/admin/finisheds/update/<?php echo e($finished->id); ?>" id="create_pin_form" style="width: 100%;" class="form-horizontal" method="post" > 
      <div class="col-md-12">
          <?php echo csrf_field(); ?>
          <div class="form-group row">
            <label class="col-md-3 col-form-label" for="hf-password">Логин</label>
            <div class="col-md-9">           
              <input type="hidden" value="<?php echo e($finished->program_id); ?>" name="program_id">
              <input class="form-control" name="login" value="<?php echo e($finished->login); ?>" type="text">
            </div>
          </div>                   

          <div class="form-group row">
            <label class="col-md-3 col-form-label" for="hf-password">Пин</label>
            <div class="col-md-9">           
              <input class="form-control" name="registred_pin" value="<?php echo e($finished->registred_pin); ?>" type="text">
            </div>
          </div>                  

          <div class="form-group row">
            <label class="col-md-3 col-form-label" for="hf-password">Дата рождения</label>
            <div class="col-md-9">           
              <input class="form-control" name="birthdate" value="<?php echo e($finished->birthdate); ?>" type="text">
            </div>
          </div>         

          <div class="form-group row">
            <label class="col-md-3 col-form-label" for="hf-password">ИИН</label>
            <div class="col-md-9">           
              <input class="form-control" name="iin" value="<?php echo e($finished->iin); ?>" type="text">
            </div>
          </div>              

           <div class="form-group row">
            <label class="col-md-3 col-form-label" for="hf-password">Страна/Город</label>
            <div class="col-md-9">           
              <input class="form-control" name="country" value="<?php echo e($finished->country); ?>" type="text">
            </div>
          </div>                    

          <div class="form-group row">
            <label class="col-md-3 col-form-label" for="hf-password">Телефон</label>
            <div class="col-md-9">           
              <input class="form-control" name="phone" value="<?php echo e($finished->phone); ?>" type="text">
            </div>
          </div>               

          <div class="form-group row">
            <label class="col-md-3 col-form-label" for="hf-password">INFO</label>
            <div class="col-md-9">           
              <input class="form-control" name="info" value="<?php echo e($finished->info); ?>" type="text">
            </div>
          </div>              

          <div class="form-group row">
            <input type="text" name="id"  value="<?php echo e($finished->setr); ?>" hidden/>
            <label class="col-md-3 col-form-label" for="hf-password">SETR</label>
            <div class="col-md-9">           
              <input class="form-control" name="setr" value="<?php echo e($setr->setr); ?>" type="text">
            </div>
          </div>

          <div class="form-group row">
                                   <button class="btn btn-sm btn-primary" type="submit" style="margin: 40px 0 0 10px;">
                      <i class="fa fa-dot-circle-o" type="submit" ></i> 
                            Сохранить
                      </button>
          </div>           
        </form>

         

        </div>
 
  </div>



   
    </div>




  


  <div class="tab-pane card-body fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
  <div class="row">
              <div class="col-md-12">
              <?php $__currentLoopData = json_decode($finished->last, true); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <div class="row">
      <div class="col-md-5 firsts" style="border-bottom: 1px solid #c67e7e; margin-bottom: 12px;">
        <p><span style="font-weight: bold;"><?php echo e($lastList[ $user['uid'] ]['name']); ?></span> <br/>
          <small><?php echo e($lastList[ $user['uid'] ]['familiya']); ?>;</small> <br/>
          <small><?php echo e($lastList[ $user['uid'] ]['screch']); ?>;</small> <br/>
          <?php if($finished->program_id == 3 || $finished->program_id == 6): ?>
          <small><a href="#" class="watch" onclick="load(); return false;">Смотреть</a></small>
          <?php endif; ?>
        </p>
        <p><?php if(isset($user['pin'])): ?><?php echo e($user['pin']); ?><?php endif; ?></p>
      </div>
      <div class="col-md-7 others"></div>
    </div>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>    
          </div>
  </div>  
  </div>
  


  <div class="tab-pane card-body fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
    <div class="row">
      <div class="col-md-12">
              <div class="card-body">

                <?php if( count($refs) > 0 ): ?>
                <table class="table table-responsive-sm">
                  <thead>
                  <tr>
                  <th>Логин</th>
                  <th>ФИО</th>
                  <th>ПИН код</th>
                  <th>Дата регистации</th>
                  </tr>
                  </thead>
                  <tbody>
                    <?php $__currentLoopData = $refs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <tr>
                      <td><?php echo e($user->name); ?></td>
                      <td><?php echo e($user->familiya); ?></td>
                      <td><?php echo e($user->screch); ?></td>
                      <td><?php echo e(gmdate("Y-m-d", $user->reg_date)); ?></td>
                    </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                  </tbody>
                  </table>
                  <?php else: ?>
                  Никого не пригласил
                  <?php endif; ?>
                </div>

    </div>

  </div>
</div>

                <div class="card-footer">
                  <button class="btn btn-sm btn-danger" data-dismiss="modal" aria-label="Close">      
                   <i class="fa fa-ban"></i> Закрыть</button>
                </div>



    </div>




    
</div>
