        <?php
          $m = Config::get('matrix.get');
          $p = Session::get('programm', 1);
          if($p == null){
            Session::put('programm', 1);
            $p = Session::get('programm', 1);
          }
        ?>

					<div class="header">
                    <div class="row">
                        <div class="col-md-12">
                            <h2 class="title"><?php echo e(__($m[$p][1])); ?></h2>

                            <p class="pull-right">
                                                                              <?php if(Auth::guard('partner')->check()): ?>
                                                    <a class="link" href="#" onclick="document.getElementById('logout-form').submit(); return false;">
                                                        <i class="icon-logout"></i>
                                                        <?php echo e(__('выход')); ?>

                                                    </a>
                                                    <form id="logout-form" action="<?php echo e(route('partner.logout')); ?>" method="POST" style="display: none;">
                                                     <?php echo csrf_field(); ?>
                                                    </form>
                                                <?php else: ?>
                                                    <a href="#" class="link"><?php echo e(__('вход')); ?></a>
                                                <?php endif; ?>
                            </p>
                        </div>
                        <div class="col-md-12">
                            <div class="nav_cont">

            									<a href="<?php echo e(route('partner.registration')); ?>" class="link <?php echo e(request()->is('partner/registration') ? 'active' : ''); ?>"><?php echo e(__('Регистрация')); ?></a>
            									<a href="<?php echo e(route('partner.profile')); ?>" class="link <?php echo e(request()->is('partner/profile') ? 'active' : ''); ?>"><?php echo e(__('Мой профиль')); ?></a>
            									<a href="<?php echo e(route('ladder')); ?>" class="link <?php echo e(request()->is('partner/ladder') ? 'active' : ''); ?>"><?php echo e(__('Моя лестница')); ?></a>

            									<a href="<?php echo e(route('partner.structure')); ?>" class="link <?php echo e(request()->is('partner/bonus') ? 'active' : ''); ?>"><?php echo e(__('Моя структура')); ?></a>
 
                                                <?php if($p == 5 || $p == 2): ?>
                                                <?php else: ?>
            									<a href="<?php echo e(route('partner.pin')); ?>" class="link <?php echo e(request()->is('partner/pin') ? 'active' : ''); ?>"><?php echo e(__('Пин-код')); ?></a>				
                                                <?php endif; ?>		
            									<a href="<?php echo e(route('partner.marketing')); ?>" class="link <?php echo e(request()->is('partner/marketing') ? 'active' : ''); ?>"><?php echo e(__('Маркетинг')); ?></a>
                                                <?php if( \App\Models\Finished::where('user_id',  Auth::user()->user_id )->where('program_id', 3)->where('stage', 1)->first() ): ?>;
                                                    <a href="/partner/main/finish" class="link <?php echo e(request()->is('partner/main/finish') ? 'active' : ''); ?>"><?php echo e(__('Вознаграждение')); ?></a>
                                                <?php endif; ?>
                                                <?php if( \App\Models\Finished::where('user_id',  Auth::user()->user_id )->where('program_id', 6)->where('stage', 1)->first() ): ?>;
                                                    <a href="/partner/vip/finish" class="link <?php echo e(request()->is('partner/vip/finish') ? 'active' : ''); ?>"><?php echo e(__('Вознаграждение')); ?></a>
                                                <?php endif; ?>   
                                           
                                                <?php if( $p == 6 ): ?>;
                                                    <a href="/partner/vipbonus" class="link <?php echo e(request()->is('partner/main/finish') ? 'active' : ''); ?>"><?php echo e(__('Вознаграждение Бонус')); ?></a>
                                                <?php endif; ?>                                                
                                                <?php if( $p == 8 ): ?>;
                                                    <a href="/partner/bonus-form" class="link <?php echo e(request()->is('partner/bonus-program/finish') ? 'active' : ''); ?>"><?php echo e(__('Вознаграждение (Bonus)')); ?></a>
                                                <?php endif; ?>

                            </div>
                        </div>
                    </div>
                </div>