<?php $__env->startSection('content'); ?>


<?php $__env->startSection('content'); ?>

<style type="text/css">


a.active{
	color: #ff0200;
	text-decoration: underline;
}



.tgbl{
	display: none;
}
.tgbl.active{
	display: block;
}
</style>

<div class="cabinet_main_container">
	<?php echo $__env->make('front.layout.menu', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
		<div class="body">
			<strong> </strong>
		<div class="profile_body_text">

		<?php if(Session::has('message')): ?>
                <div class="card-header" style="margin-bottom: 25px;">
            		<p class="alert succsess"><?php echo e(Session::get('message')); ?></p>
                </div>
        <?php endif; ?>

		<a href="/partner/profile">&#8592; Назад</a>

		<?php if($swap): ?>
		<div class="row" style="padding: 40px 0 0 10px;">
			<?php if($swap->status_id == 0): ?>
			Ваша заявка находится на рассмотрении
			<?php elseif($swap->status_id == 1): ?>
			Ваша заявка одобрена <br/>
			<br/>
			<form action="/reswap" method="POST">
				<?php echo csrf_field(); ?>
				<input type="text" hidden  value="<?php echo e($swap->id); ?>" name="id">
				<button type="submit" href="#" class="btn btn-primary">Создать новую заявку</button>
			</form>			
			<?php elseif($swap->status_id == 2): ?>
			Ваша заявка отклонена <br/>
			<br/>
			<pre>
				<?php echo e($swap->cancel_reason); ?>

			</pre>
			<form action="/reswap" method="POST">
				<?php echo csrf_field(); ?>
				<input type="text" hidden  value="<?php echo e($swap->id); ?>" name="id">
				<button type="submit" href="#" class="btn btn-primary">Создать новую заявку</button>
			</form>
			<?php endif; ?>
		</div>

		<?php else: ?>


							<div class="pin-request-table">
								<table class="profile_table">
									<?php echo e(Form::open(['route' => 'partner.swap', 'method'=>'post', 'files'=>true, 'autocomplete'=>"off"])); ?>

									<tbody>


									<tr>
										<td><?php echo e(Form::label('login', 'Замена')); ?></td>
										<td>
											<select name="type" id="type-select" class="form-control">
												<option value="1">По заявлению</option>
												<option value="2">По приказу №7</option>
												<?php if( in_array(Session::get('programm', 1), [3,6]  ) ): ?>	
													<option value="3">Спонсора</option>
												<?php endif; ?>
											</select>
										</td>
									</tr>

									<tr>
										<td><?php echo e(Form::label('login', 'Логин')); ?></td>
										<td><?php echo e(Form::text('login', null, ['class'=>'form-control', 'required'=>'required'])); ?></td>
									</tr>

									<tr>
										<td><?php echo e(Form::label('login2', 'Заменить на')); ?></td>
										<td><?php echo e(Form::text('login2', null, ['class'=>'form-control', 'required'=>'required'])); ?></td>
									</tr>

									<tr>
										<td><?php echo e(Form::label('images', 'Скан документа')); ?></td>
										<td><?php echo e(Form::file('images[]', array('class' => 'form-control', 'required'=>'required', 'multiple'=>true, 'accept'=>".jpeg, .jpg, .png, .pdf") )); ?></td>
									</tr>




									<tr>
										<td><button class="btn btn-sm btn-primary" type="submit">Отправить</button> </td>
									</tr>
									</tbody>
									<?php echo e(Form::close()); ?>

								</table>
							</div>
		<?php endif; ?>
						</div>
					</div>
				</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
<script type="text/javascript">


$(document).ready( function(){




	// $('#type-select').on('change', function() {
	//   alert( this.value );
	// });



});



</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('front.layout.main', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>