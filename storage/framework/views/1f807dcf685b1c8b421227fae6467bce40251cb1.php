<?php if($type === 'toolbar'): ?>

    <?php if(call_user_func($renderIf) === true): ?>
        <a href="<?php echo e(is_callable($url) ? $url() : $url); ?>"
           title="<?php echo e($title); ?>"
           class="<?php echo e($class); ?>"
           <?php $__currentLoopData = $dataAttributes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $k => $v): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
           data-<?php echo e($k); ?>=<?php echo e($v); ?>

                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        >
            <?php if($icon): ?>
                <i class="fa <?php echo e($icon); ?>"></i>
            <?php endif; ?>
            <?php echo e($name); ?>

        </a>
    <?php endif; ?>
<?php else: ?>

    <?php if(call_user_func($renderIf, $gridName, $gridItem) === true): ?>
        <a href="<?php echo e(is_callable($url) ? call_user_func($url, $gridName, $gridItem) : $url); ?>"
           title="<?php echo e($title); ?>"
           class="<?php echo e($class); ?>"
           <?php $__currentLoopData = $dataAttributes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $k => $v): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
           data-<?php echo e($k); ?>=<?php echo e($v); ?>

                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        >
            <?php if($icon): ?>
                <i class="fa <?php echo e($icon); ?>"></i>
            <?php endif; ?>
            <?php echo e($name); ?>

        </a>
    <?php endif; ?>
<?php endif; ?>
