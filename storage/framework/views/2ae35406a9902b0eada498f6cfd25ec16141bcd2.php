<?php $__env->startSection('content'); ?>

<div class="cabinet_main_container authorization">
  <div class="header">
    <div class="row">
      <div class="col-md-10">
        <h2 class="title">Завершение программы</h2>        
      </div>
      <div class="col-md-2">
        <a class="link" href="#" onclick="document.getElementById('logout-form').submit(); return false;">
          <i class="icon-logout"></i>выход
        </a>
        <form id="logout-form" action="<?php echo e(route('partner.logout')); ?>" method="POST" style="display: none;">
          <?php echo csrf_field(); ?>
        </form>
      </div>
    </div>

  </div>
  <div class="body">
    <div class="auth_cont" style="max-width: 100%;">



<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingOne">
      <h4 class="panel-title">
        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">

        </a>
      </h4>
    </div>
    <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
      <div class="panel-body">
        <p style="margin: 25px 0 50px 0; font-weight: bold; font-size: 16px; color: #3e3d3c;">Поздравляем! <br/>
        Вы завершили «Накопительную» программу для получения Пин-кода воспользуйтесь формой ниже</p>
        <?php echo e(Form::open(['route' => 'pin.add', 'method'=>'post'])); ?>

        <?php echo csrf_field(); ?>
          <input type="text" name="program_id" value="5" hidden="hidden">
          <input type="text" name="program_type" value="1" hidden="hidden">
          <button class="btn btn-lg btn-success" type="submit" onclick="return confirm('Вы уверены?')">
            <i class="fa fa-dot-circle-o"></i> Сгенерировать
           </button>  
        <?php echo e(Form::close()); ?>

        <br>
        <br>

          <div class="form-group row"> 
          <div class="col-md-2"></div>           
          <div class="col-md-10">
            <p style="text-align: left;">Варианты выбора продукции от G-Time CORPORATION по Накопительной программе:</p>
            <p style="text-align: left;">Вариант А</p>
            <p style="text-align: left;">-1 упаковка мыло + 1 шт. крем (для рук или ног, на выбор)</p>
            <p style="text-align: left;">Вариант В</p>
            <p style="text-align: left;">-1 упаковка мыло с черным тмином + 1 упаковка мыло + 1шт. гель антисептик</p>            
          </div>           

          </div>     



      </div>
    </div>
  </div>


</div>






    </div>
    </div>

        </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>


<?php $__env->stopSection(); ?>
<?php echo $__env->make('front.layout.main', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>