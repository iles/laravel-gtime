<?php $__env->startSection('content'); ?>

        <?php
          $m = Config::get('matrix.get');
          $p = Session::get('programm', 1);
          if($p == null){
            Session::put('programm', 1);
            $p = Session::get('programm', 1);
          }
        ?>

        		<style>
			.title {
				margin-bottom: 30px;
				font-size: 1rem;
				letter-spacing: 0.1em;
				text-transform: none;
			}
		    .form-group{
		        display: block;
		        margin: 20px 0;
		    }		    
		    .succsess{
		    	color: green !important
		    }
		    #result-registration, #result-pin{ 
		    	color:red; 
		    	margin-top: 6px; 
		    	font-size: 14px;
		    	display: none;
		    }		    
		    #result-registration, #result-sponsor{ 
		    	color:red; 
		    	margin-top: 6px; 
		    	font-size: 14px;
		    	display: none;
		    }
		    label{
		        font-weight: 400;
		    }
		    select{
		        width:100%;
		        padding: 10px;
		    }

			@media (max-width: 768px) {
				.button {
					margin-top: 20px;m
				}
			}
		</style>

<div class="cabinet_main_container">
	<?php echo $__env->make('front.layout.menu', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
	
	<div class="body">
    
    <div id="dle-content">


<div class="marketing_body_text">

		<?php if(Session::has('message')): ?>
        	<div class="card-header" style="margin-bottom: 45px;">
            	<p class="alert <?php echo e(Session::get('alert-class', 'alert-info')); ?>"><?php echo e(Session::get('message')); ?></p>
            </div>
        <?php endif; ?>

<h2 class="title">
	Регистрация нового партнера
	
</h2>

					
    <p><b>Внимание! Перед регистрацией просим учесть, что:   </b></p>
    <p>* Все данные, которые Вы вносите во время регистрации должны быть достоверны. </p>
    <p>* Правильно вносите электронную почту. </p>
    <p>* Будьте внимательны при заполнении логина. Логин нужно заполнить латинскими буквами. Логин Вы будете использовать для дальнейшей работы.
    </p><p>* Пароль вы сможете восстановить при необходимости.  </p>
    <p>* В случае не заполнения всех ячеек, регистрация отменяется.</p>
    <p><br><b>Поля со знаком <font color="red">*</font> - для обязательного заполнения.  </b><br><br></p>
    <p><b></b></p><div style="color:red"><b> Для получение денежного вознаграждение <br>
							необходимо корректно вводить паспортные данные</b></div>
					<br><br>
					
					
</div>
			
			<div class="row">


			<?php echo Form::open(array('route' => 'partner.register_partner','files'=> true, 'id' => 'reg-form', 'autocomplete'=>'off')); ?>


                <div class="form-group">
                    <div class="col-md-3 col-xs-12 col-sm-3">
                        <label for="pin">Пин:  <b><span style="color:red">*</span></b></label>
                    </div>
                    <div class="col-md-6 col-xs-12 col-sm-6">
                    	<?php echo e(Form::text('pin', null, ['class'=>'form-control', 'required'=>'required'])); ?>

                    	<div id="result-pin"></div>
                    </div>
                    <div class="col-md-3 col-xs-12 col-sm-3">
                        <button class="button" id="pin-check">Проверить</button>
                    </div>
                </div>                
                <div class="clearfix"></div>
                <div class="form-group">
                    <div class="col-md-3 col-xs-12 col-sm-3">
                        <label for="name">Логин: <b><span style="color:red">*</span></b></label>
                    </div>
                    <div class="col-md-6 col-xs-12 col-sm-6">
                    	<?php echo e(Form::text('json[uname]', null, ['data-inputmask' => '"mask": "*", "repeat": 29', 'class'=>'form-control', 'required'=>'required', 'minlength' => 4])); ?>

                        <div id="result-registration"></div>
                    </div>
                    <div class="col-md-3 col-xs-12 col-sm-3">
                        <button class="button" id="login-check">Проверить</button>
                    </div>
                </div>   
                <div class="clearfix"></div>                             
                <div class="form-group">
                    <div class="col-md-3 col-xs-12 col-sm-3">
                        <label for="name">Логин спонсора: <b><span style="color:red">*</span></b></label>
                    </div>
                    <div class="col-md-6 col-xs-12 col-sm-6">
                    	<?php if($p == 8): ?>
                    	<?php echo e(Form::text('json[sponsor]', null, ['class'=>'form-control', 'minlength' => 2, 'autocomplete' => 'off'])); ?>

                    	<?php else: ?>
                    	<?php echo e(Form::text('json[sponsor]', null, ['class'=>'form-control', 'minlength' => 2, 'autocomplete' => 'off'])); ?>

                    	<?php endif; ?>
                        <div id="result-sponsor"></div>
                    </div>
                    <div class="col-md-3 col-xs-12 col-sm-3">
                        <button class="button" id="sponosor-check">Проверить</button>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="form-group">
                    <div class="col-md-3 col-xs-12 col-sm-3">
                        <label for="password">Пароль: <b><span style="color:red">*</span></b></label>
                    </div>
                    <div class="col-md-6 col-xs-12 col-sm-6">
                          <?php echo e(Form::password('json[password]', ['class'=>'form-control', 'disabled'=>'disabled', 'required'=>'required', 'autocomplete'=>'off'])); ?>

                    </div>
                    <div class="col-md-3 col-xs-12 col-sm-3">
                    </div>
                </div>                   

                <div class="clearfix"></div>
                <div class="form-group">
                    <div class="col-md-3 col-xs-12 col-sm-3">
                        <label for="password2">Повторите пароль: <b><span style="color:red">*</span></b></label>
                    </div>
                    <div class="col-md-6 col-xs-12 col-sm-6">
                        <?php echo e(Form::password('json[password2]', ['class'=>'form-control', 'required'=>'required', 'autocomplete'=>'off'])); ?>

                    </div>
                    <div class="col-md-3 col-xs-12 col-sm-3">
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="form-group">
                    <div class="col-md-3 col-xs-12 col-sm-3">
                        <label for="familiya">Фамилия: <b><span style="color:red">*</span></b></label>
                    </div>
                    <div class="col-md-6 col-xs-12 col-sm-6">
                    	<?php echo e(Form::text('json[secondname]', 'dsfdsfdsfdsfds', ['class'=>'form-control', 'required'=>'required'])); ?>

                    </div>
                    <div class="col-md-3 col-xs-12 col-sm-3">
                    </div>
                </div>
                <div class="clearfix"></div>                
                <div class="form-group">
                    <div class="col-md-3 col-xs-12 col-sm-3">
                        <label for="familiya">Имя: <b><span style="color:red">*</span></b></label>
                    </div>
                    <div class="col-md-6 col-xs-12 col-sm-6">
                        <?php echo e(Form::text('json[name]', 'dsfdsfdsfdsfds', ['class'=>'form-control', 'required'=>'required'])); ?>


                    </div>
                    <div class="col-md-3 col-xs-12 col-sm-3">
                    </div>
                </div>
                <div class="clearfix"></div>                
                <div class="form-group">
                    <div class="col-md-3 col-xs-12 col-sm-3">
                        <label for="familiya">Отчество: <b><span style="color:red">*</span></b></label>
                    </div>
                    <div class="col-md-6 col-xs-12 col-sm-6">
                    	<?php echo e(Form::text('json[patronic]', 'dsfdsfdsfdsfds', ['class'=>'form-control', 'required'=>'required'])); ?>

                    </div>
                    <div class="col-md-3 col-xs-12 col-sm-3">
                    </div>
                </div>
                <div class="clearfix"></div>



                <div class="form-group">
                    <div class="col-md-3 col-xs-12 col-sm-3">
                        <label>Страна и город: <b><span style="color:red">*</span></b></label>
                    </div>
                    <div class="col-md-6 col-xs-12 col-sm-6">
						<select name="json[country]" class="selectcity" required="required">
							<optgroup label="Алматинская область">
								<option value="Алматинская область">Алматинская область</option>
								<option value="Алматы">Алматы</option>
								<option value="Бурундай">Бурундай</option>
								<option value="Есик">Есик</option>
								<option value="Жаркент">Жаркент</option>
								<option value="Капчагай">Капчагай</option>
								<option value="Каскелен">Каскелен</option>
								<option value="Сарканд">Сарканд</option>
								<option value="Талдыкорган">Талдыкорган</option>
								<option value="Талгар">Талгар</option>
								<option value="Турген">Турген</option>
								<option value="Текели">Текели</option>
								<option value="Ушарал">Ушарал</option>
								<option value="Уштобе">Уштобе</option>
								<option value="Узынагаш">Узынагаш</option>
								<option value="Чунджа">Чунджа</option>
								<option value="Шамалган">Шамалган</option>
								<option value="Шелек">Шелек</option>
							</optgroup>
							<optgroup label="Акмолинская область">
								<option value="Акмолинская область">Акмолинская область</option>
								<option value="Астана">Астана</option>
								<option value="Астраханка">Астраханка</option>
								<option value="Акколь">Акколь</option>
								<option value="Алтынды">Алтынды</option>
								<option value="Атбасар">Атбасар</option>
								<option value="Державинск">Державинск</option>
								<option value="Ерейментау">Ерейментау</option>
								<option value="Кокшетау">Кокшетау</option>
								<option value="Коянды">Коянды</option>
								<option value="Кощи">Кощи</option>
								<option value="Макинск">Макинск</option>
								<option value="Щучинск">Щучинск</option>
								<option value="Степногорск">Степногорск</option>
								<option value="Степняк">Степняк</option>
								<option value="Шортанды">Шортанды</option>
							</optgroup>
							<optgroup label="Актюбинская область">
								<option value="Актюбинская область">Актюбинская область</option>
								<option value="Актобе">Актобе</option>
								<option value="Алга">Алга</option>
								<option value="Эмба">Эмба</option>
								<option value="Хромтау">Хромтау</option>
								<option value="Кандыагаш">Кандыагаш</option>
								<option value="Шалкар">Шалкар</option>
								<option value="Темир">Темир</option>
								<option value="Жем">Жем</option>
							</optgroup>
							<optgroup label="Атырауская область">
								<option value="Атырауская область">Атырауская область</option>
								<option value="Атырау">Атырау</option>
								<option value="Алмалы">Алмалы</option>
								<option value="Аккистау">Аккистау</option>
								<option value="Балыкшы">Балыкшы</option>
								<option value="Доссор">Доссор</option>
								<option value="Кульсары">Кульсары</option>
								<option value="Карабатан">Карабатан</option>
								<option value="Макат">Макат</option>
								<option value="Махамбет">Махамбет</option>
								<option value="Тендык">Тендык</option>
								<option value="Таскала">Таскала</option>
							</optgroup>
							<optgroup label="Восточно-Казахстанская область">
								<option value="Восточно-Казахстанская область">Восточно-Казахстанская область</option>
								<option value="Аягуз">Аягуз</option>
								<option value="Чарск">Чарск</option>
								<option value="Курчатов">Курчатов</option>
								<option value="Риддер">Риддер</option>
								<option value="Семей">Семей</option>
								<option value="Серебрянск">Серебрянск</option>
								<option value="Шемонаиха">Шемонаиха</option>
								<option value="Усть-каменогорск">Усть-каменогорск</option>
								<option value="Урджар">Урджар</option>
								<option value="Зайсан">Зайсан</option>
								<option value="Зыряновск">Зыряновск</option>
							</optgroup>
							<optgroup label="Жамбылская область">
								<option value="Жамбылская область">Жамбылская область</option>
								<option value="Каратау">Каратау</option>
								<option value="Шу">Шу</option>
								<option value="Тараз">Тараз</option>
								<option value="Жанатас">Жанатас</option>
								<option value="Мерке">Мерке</option>
							</optgroup>
							<optgroup label="Западно-Казахстанская область">
								<option value="Западно-Казахстанская область">Западно-Казахстанская область</option>
								<option value="Уральск">Уральск</option>
								<option value="Уральск">Аксай</option>
								<option value="Таскала">Таскала</option>
								<option value="Жангала">Жангала</option>
								<option value="Жымпиты">Жымпиты</option>
								<option value="Жанибек">Жанибек</option>
								<option value="Каратобе">Каратобе</option>
								<option value="Казталовка">Казталовка</option>
								<option value="Переметное">Переметное</option>
								<option value="Сайхин">Сайхин</option>
								<option value="Шынгырлау">Шынгырлау</option>
								<option value="Чапаев">Чапаев</option>
								<option value="Федоровка">Федоровка</option>
							</optgroup>
							<optgroup label="Карагандинская область">
								<option value="Карагандинская область">Карагандинская область</option>
								<option value="Абай">Абай</option>
								<option value="Атасу">Атасу</option>
								<option value="Балхаш">Балхаш</option>
								<option value="Караганда">Караганда</option>
								<option value="Каражал">Каражал</option>
								<option value="Каркаралинск">Каркаралинск</option>
								<option value="Приозерск">Приозерск</option>
								<option value="Сарань">Сарань</option>
								<option value="Сатпаев">Сатпаев</option>
								<option value="Шахтинск">Шахтинск</option>
								<option value="Темиртау">Темиртау</option>
								<option value="Жезказган">Жезказган</option>
							</optgroup>
							<optgroup label="Кустанайская область">
								<option value="Кустанайская область">Кустанайская область</option>
								<option value="Аркалык">Аркалык</option>
								<option value="Костанай">Костанай</option>
								<option value="Лисаковск">Лисаковск</option>
								<option value="Рудный">Рудный</option>
								<option value="Житикара">Житикара</option>
							</optgroup>
							<optgroup label="Кызылординская область">
								<option value="Кызылординская область">Кызылординская область</option>
								<option value="Аральск">Аральск</option>
								<option value="Байконур">Байконур</option>
								<option value="Казалинск">Казалинск</option>
								<option value="Кызылорда">Кызылорда</option>
								<option value="Жанакорган">Жанакорган</option>
							</optgroup>
							<optgroup label="Мангистауская область">
								<option value="Мангистауская область">Мангистауская область</option>
								<option value="Актау">Актау</option>
								<option value="Форт-Шевченко">Форт-Шевченко</option>
								<option value="Жанаозен">Жанаозен</option>
							</optgroup>
							<optgroup label="Павлодарская область">
								<option value="Павлодарская область">Павлодарская область</option>
								<option value="Аксу">Аксу</option>
								<option value="Экибастуз">Экибастуз</option>
								<option value="Павлодар">Павлодар</option>
							</optgroup>
							<optgroup label="Северо-Казахстанская область">
								<option value="Северо-Казахстанская область">Северо-Казахстанская область</option>
								<option value="Булаево">Булаево</option>
								<option value="Мамлютка">Мамлютка</option>
								<option value="Петропавловск">Петропавловск</option>
								<option value="Сергеевка">Сергеевка</option>
								<option value="Тайынша">Тайынша</option>
								<option value="с.Новоишимка">с.Новоишимка</option>
							</optgroup>
							<optgroup label="Южно-Казахстанская область">
								<option value="Южно-Казахстанская область">Южно-Казахстанская область</option>
								<option value="Арыс">Арыс</option>
								<option value="Аксу">Аксу</option>
								<option value="Жетысай">Жетысай</option>
								<option value="Кентау">Кентау</option>
								<option value="Казыгурт">Казыгурт</option>
								<option value="Ленгер">Ленгер</option>
								<option value="Шардара">Шардара</option>
								<option value="Шымкент">Шымкент</option>
								<option value="Шаульдер">Шаульдер</option>
								<option value="Туркестан">Туркестан</option>
								<option value="Сарыагаш">Сарыагаш</option>
							</optgroup>
							<optgroup label="Ближнее и дальнее зарубежье">
								<option value="Россия">Россия</option>
								<option value="Татарстан">Татарстан</option>
								<option value="Беларусь">Беларусь</option>
								<option value="Киргизия">Киргизия</option>
								<option value="Таджикистан">Таджикистан</option>
								<option value="Узбекистан">Узбекистан</option>
								<option value="Украина">Украина</option>
								<option value="Туркмения">Туркмения</option>
								<option value="Abkhazia"> Abkhazia</option>
								<option value="Australia"> Australia</option>
								<option value="Austria"> Austria</option>
								<option value="Azerbaijan"> Azerbaijan</option>
								<option value="Albania"> Albania</option>
								<option value="Algeria"> Algeria</option>
								<option value="Argentina"> Argentina</option>
								<option value="Armenia"> Armenia</option>
								<option value="Afghanistan"> Afghanistan</option>
								<option value="Bangladesh"> Bangladesh</option>
								<option value="Bahrain"> Bahrain</option>
								<option value="Belgium"> Belgium</option>
								<option value="Bulgaria"> Bulgaria</option>
								<option value="Bolivia"> Bolivia</option>
								<option value="Brazil"> Brazil</option>
								<option value="Great Britain"> United Kingdom</option>
								<option value="Hungary"> Hungary</option>
								<option value="Venezuela"> Venezuela</option>
								<option value="Vietnam"> Vietnam</option>
								<option value="Germany"> Germany</option>
								<option value="Hong Kong"> Hong Kong</option>
								<option value="Greenland"> Greenland</option>
								<option value="Greece"> Greece</option>
								<option value="Georgia"> Georgia</option>
								<option value="Denmark"> Denmark</option>
								<option value="Egypt"> Egypt</option>
								<option value="Israel"> Israel</option>
								<option value="India"> India</option>
								<option value="Indonesia"> Indonesia</option>
								<option value="Jordan"> Jordan</option>
								<option value="Iraq"> Iraq</option>
								<option value="Iran"> Iran</option>
								<option value="Ireland"> Ireland</option>
								<option value="Spain"> Spain</option>
								<option value="Iceland"> Iceland</option>
								<option value="Italy"> Italy</option>
								<option value="Cameroon"> Cameroon</option>
								<option value="Canada"> Canada</option>
								<option value="Kenya"> Kenya</option>
								<option value="Cyprus"> Cyprus</option>
								<option value="China"> China</option>
								<option value="Columbia"> Columbia</option>
								<option value="Korea (South)"> Korea (South)</option>
								<option value="Costa Rica"> Costa Rica</option>
								<option value="Kuwait"> Kuwait</option>
								<option value="Latvia"> Latvia</option>
								<option value="Lebanon"> Lebanon</option>
								<option value="Lithuania"> Lithuania</option>
								<option value="Malaysia"> Malaysia</option>
								<option value="Morocco"> Morocco</option>
								<option value="Mexico"> Mexico</option>
								<option value="Moldova"> Moldova</option>
								<option value="Mongolia"> Mongolia</option>
								<option value="Nepal"> Nepal</option>
								<option value="Nigeria"> Nigeria</option>
								<option value="Netherlands"> Netherlands</option>
								<option value="New Zealand"> New Zealand</option>
								<option value="Norway"> Norway</option>
								<option value="Pakistan"> Pakistan</option>
								<option value="Palestine"> Palestine</option>
								<option value="Panama"> Panama</option>
								<option value="Paraguay"> Paraguay</option>
								<option value="Peru"> Peru</option>
								<option value="Poland"> Poland</option>
								<option value="Portugal"> Portugal</option>
								<option value="Salvador"> Salvador</option>
								<option value="Saudi Arabia"> Saudi Arabia</option>
								<option value="Serbia"> Serbia</option>
								<option value="Syria"> Syria</option>
								<option value="Slovakia"> Slovakia</option>
								<option value="Slovenia"> Slovenia</option>
								<option value="United States"> United States</option>
								<option value="Thailand"> Thailand</option>
								<option value="Taiwan"> Taiwan</option>
								<option value="Turkey"> Turkey</option>
								<option value="Philippines"> Philippines</option>
								<option value="Finland"> Finland</option>
								<option value="France"> France</option>
								<option value="Croatia"> Croatia</option>
								<option value="Czech Republic"> Czech Republic</option>
								<option value="Switzerland"> Switzerland</option>
								<option value="Sweden"> Sweden</option>
								<option value="Scotia"> Scotia</option>
								<option value="Ecuador"> Ecuador</option>
								<option value="Estonia"> Estonia</option>
								<option value="Ethiopia"> Ethiopia</option>
								<option value="South Africa"> South Africa</option>
								<option value="Jamaica"> Jamaica</option>
								<option value="Japan"> Japan</option>
							</optgroup>
						</select>
                    </div>
                    <div class="col-md-3 col-xs-12 col-sm-3">
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="form-group">
                    <div class="col-md-3 col-xs-12 col-sm-3">
                        <label for="datedd">Дата рождения: <b><span style="color:red">*</span></b></label>
                    </div>
                    <div class="col-md-6 col-xs-12 col-sm-6">
                    	<?php echo e(Form::text('json[birthdate]', '12-12-1212', ['data-inputmask' => '"mask": "99-99-9999"', 'class'=>'form-control', 'required'=>'required'])); ?>


                    </div>
                    <div class="col-md-3 col-xs-12 col-sm-3">
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="form-group">
                    <div class="col-md-3 col-xs-12 col-sm-3">
                        <label for="phone">Телефон: <b><span style="color:red">*</span></b></label>
                    </div>
                    <div class="col-md-6 col-xs-12 col-sm-6">
                        <?php echo e(Form::text('json[phone]', '"+7(999)-999-99-99', ['data-inputmask' => '"mask": "+7(999)-999-99-99"', 'class'=>'form-control', 'required'=>'required'])); ?>

                    </div>
                    <div class="col-md-3 col-xs-12 col-sm-3">
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="form-group">
                    <div class="col-md-3 col-xs-12 col-sm-3">
                        <label for="iin">ИИН, ИНН (Pasport ID): <b><span style="color:red">*</span></b></label>
                    </div>
                    <div class="col-md-6 col-xs-12 col-sm-6">
                    <?php echo e(Form::text('json[iin]', '999999999999', ['data-inputmask' => '"mask": "999999999999"', 'class'=>'form-control', 'required'=>'required'])); ?>

                    <?php echo e(Form::text('json[state]', null, ['hidden'=>'hidden'])); ?>

                    </div>
                    <div class="col-md-3 col-xs-12 col-sm-3">
                    </div>
                </div>
                <div class="clearfix"></div>

			</div>
			

            <div style="width:100%; margin-top:20px; text-align: center">
                <button name="submit" class="button" type="submit" style="width:200px;">  Зарегистрироваться </button>
            </div>


<?php echo e(Form::close()); ?>

		</div>
	</div>
</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
	<script type="text/javascript">

	var login = 0;
	var pincode = 0;
	var name;
	var sp = 0;

	$(document).ready( function(){

		$('input[name="json[uname]"').inputmask("[a-zA-Z0-9]"); //mask with dynamic syntax
		//$('input[name="json[sponsor]"').inputmask("a{1,33}"); //mask with dynamic syntax

		function getRidOffAutocomplete(){               
		    var timer = window.setTimeout( function(){
		        $('input[name="json[password]"').prop('disabled',false);
		            clearTimeout(timer);
		        }, 800);
	    }

		// Invoke the function
		getRidOffAutocomplete();

		$('#sponosor-check').on('click', function(e){
			e.preventDefault();
			CheckSponsor();				
		})

		// $('input[name="json[uname]"').on('blur', function(){
		// 	CheckLogin()
		// 	name = $(this).val();
		// } );			

		// $('input[name="pin"').on('change', function(){
		// 	CheckPin()
		// } );			

		$('.selectcity').on('change', function(){
			var selected = $(':selected', this);
			$('input[name="json[state]"').val( selected.closest('optgroup').attr('label') )
		} );

		$('#reg-form').submit(function( event ) {
			var sponsor = $('input[name="json[sponsor]"').val();
			if(login == 0){
				alert('Введите уникальный логин');
			    CheckLogin();
				event.preventDefault();
			}				

			if(name.length < 4){
				alert('В логине должно быть минимум 4 символа');
			    CheckLogin();
				event.preventDefault();
			}				

			if(pin == 0){
				alert('Введите действительный пин код');
			    CheckPin();
				event.preventDefault();
			}			

			if(pin == 0 && sponsor.length > 0){
				alert('Введите действительный логин спонсора');
				event.preventDefault();
			}

			var x = document.getElementsByName('json[password]')[0].value,
			        y = document.getElementsByName('json[password2]')[0].value;

			if (x === y) {
				return;
			}
			 	
			alert('Пароли не совпадают!');
			
			event.preventDefault();

		});

			$(":input").inputmask();
			$('#login-check').on('click', function(e){
				e.preventDefault();
				CheckLogin();				
			});			

			$('#pin-check').on('click', function(e){
				e.preventDefault();
				CheckPin();				
			});
		});

		function validate() {
				if(login == 0){
					CheckLogin();
					return false;
				}				

				if(pincode == 0){
					CheckPin();
					return false;
				}

			    var x = document.getElementsByName('json[password]')[0].value,
			        y = document.getElementsByName('json[password2]')[0].value;

			    if (x === y) {
			        return true;
			    }

			    alert('Пароли не совпадают!');
			    return false;
		}


		function CheckLogin(){
				$('#result-registration').fadeOut();
				$('#result-registration').html('');
				$('#result-registration').removeClass('succsess');
				name = $('input[name="json[uname]"').val();
				if(!name){
			    	$('#result-registration').html('Введите логин').fadeIn(300);
			    	return false;
			    }
			    if(name.length < 4){
			    	$('#result-registration').html('В логине должно быть минимум 4 символа').fadeIn(300)
			    	return false;
				}		

				var program = <?php echo e(Session::get('programm', 1)); ?>;

				$.get( "/partner/check-login", { name: name, program : program} ).done(function( data ) {
					if(data.error == 0){
			    		$('#result-registration').addClass('succsess');	
			    		$('#result-registration').html(data.m).fadeIn(300);		
						login = 1;
			    		return;				
					}
			    	$('#result-registration').html(data.m).fadeIn(300);						
			  	});
		}

		function CheckSponsor(){
				var sponsor = $('input[name="json[sponsor]"').val();
				console.log(sponsor);
				$('#result-sponsor').fadeOut();
				$('#result-sponsor').html('');
				$('#result-sponsor').removeClass('succsess');
				if(!sponsor){
			    	$('#result-sponsor').html('Введите логин').fadeIn(300);
			    	return false;
			    }
			    if(sponsor.length < 4){
			    	$('#result-sponsor').html('В логине должно быть минимум 4 символа').fadeIn(300)
			    	return false;
				}		

				var program = <?php echo e(Session::get('programm', 1)); ?>;

				$.get( "/partner/check-sponsor", { name: sponsor, program : program} ).done(function( data ) {
					if(data.error == 0){
			    		$('#result-sponsor').addClass('succsess');	
			    		$('#result-sponsor').html(data.m).fadeIn(300);		
						sp = 1;
			    		return;				
					}
					sp = 0;
			    	$('#result-sponsor').html(data.m).fadeIn(300);						
			  	});
		}		

		function CheckPin(){
				$('#result-pin').fadeOut();
				$('#result-pin').html('');
				$('#result-pin').removeClass('succsess');
				var pin = $( "input[name='pin']" ).val();
				if(!pin){
			    	$('#result-pin').html('Введите пин').fadeIn(300);
			    	return false;
			    }

				var program = <?php echo e(Session::get('programm', 1)); ?>;

				$.get( "/partner/check-pin", { pin: pin, program : program} ).done(function( data ) {
					if(data.error == 0){
			    		$('#result-pin').addClass('succsess');	
			    		$('#result-pin').html(data.m).fadeIn(300);		
						pincode = 1;
			    		return;				
					}
			    	$('#result-pin').html(data.m).fadeIn(300);						
			  	});
		}



	</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('front.layout.main', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>