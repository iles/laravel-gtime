<select name="<?php echo e($name); ?>" id="<?php echo e($id); ?>" form="<?php echo e($formId); ?>" class="<?php echo e($class); ?>">
    <option value=""></option>
    <?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $k => $v): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <?php if(request($name) !== null && request($name) == $k): ?>
            <option value="<?php echo e($k); ?>" selected><?php echo e($v); ?></option>
        <?php else: ?>
            <option value="<?php echo e($k); ?>"><?php echo e($v); ?></option>
        <?php endif; ?>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
</select>