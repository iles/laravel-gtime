<?php $__env->startSection('content'); ?>

<div class="cabinet_main_container authorization">
	<div class="header">
		<h2 class="title">Накопительный+</h2>
	</div>
	<div class="body">
		<div class="auth_cont">

		<?php echo e(Form::open(array('route' => 'partner.loginPartner'))); ?>

				<p class="text">Введите ваши регистрационные данные для входа в личный кабинет2</p>
				<?php if(Session::has('message')): ?>
					<p class="alert <?php echo e(Session::get('alert-class', 'alert-info')); ?>"><?php echo e(Session::get('message')); ?></p>
				<?php endif; ?>

				<?php if($errors->any()): ?>
				    <div class="alert alert-danger">
				        <ul>
				            <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
				                <li><?php echo e($error); ?></li>
				            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
				        </ul>
				    </div>
				<?php endif; ?>
			
				<?php echo e(Form::text('username', '', ['class'=>'form-control', 'placeholder'=>'Логин'])); ?>

				<?php echo e(Form::password('password', ['class'=>'form-control', 'placeholder'=>'Пароль'])); ?>


				<div class="link_cont"><a href="#" class="link">восстановить пароль</a></div>
				<input type="submit" value="авторизоваться" class="button">
			<?php echo e(Form::close()); ?>

			<input type="file" style="display: none;" name="">
		</div>
		</div>

				</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('front.layout.main', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>