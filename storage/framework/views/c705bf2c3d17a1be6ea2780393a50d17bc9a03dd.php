<div class="col-md-<?php echo e($colSize); ?>">
    <form method="GET" id="<?php echo e($id); ?>" action="<?php echo e($action); ?>"
          <?php $__currentLoopData = $dataAttributes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $k => $v): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
          data-<?php echo e($k); ?>=<?php echo e($v); ?>

            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    >
        <div class="input-group mb-12">
            <input type="text" class="form-control" name="<?php echo e($name); ?>"
                   placeholder="<?php echo e($placeholder); ?>" value="<?php echo e(request($name)); ?>" required="required"
                   aria-label="search">
            <div class="input-group-append">
                <button class="btn btn-outline-primary" type="submit"><i class="fa fa-search"></i></button>
            </div>
        </div>
    </form>
</div>