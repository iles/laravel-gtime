<?php $__env->startSection('breadcrumbs'); ?>
	<li class="breadcrumb-item active">Заявки на пин-коды </li>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<style type="text/css">
	.form-top{display: none;}
	.gen-pin{
		margin-bottom: 12px;
	}
	.gen-pin label{
		margin: 0 15px;
		cursor: pointer;
	}

	.gen-pin label input{
		margin-right: 6px;
	}

	.laravel-grid .grid-wrapper {
		margin-top: 20px;
		max-height: 60vh;
		overflow-y: auto;
	} 
</style>

				<?php if(Session::has('message')): ?>
					<div class="row">
				<div class="card-body">
						<div class="col-md-12 col-xs-12 col-sm-12">
						<p style="background-color: #dff0d8; border-color: #d6e9c6; color: #3c763d; padding: 15px; margin-bottom: 20px; border-radius: 4px;" class="alert <?php echo e(Session::get('alert-class', 'alert-info')); ?>"><?php echo e(Session::get('message')); ?></p>

                   	</div>
					</div>
				</div>
				<?php endif; ?>

                <div class="card-body">
					<?php echo $grid; ?>


                </div>



<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
	<script type="text/javascript">
		$('.grid-title').html('Заявки на ПИН коды');


	</script>

	<script type="text/javascript">
		var i = 0;

		function addField(){
			var t = '<div class="extra-pin col-md-6" id="ep'+i+'" style="margin-bottom: 20px;"><select class="form-control" name="users['+i+'][type]"><option value="1">Старт</option><option value="3">Основная</option><option value="4">Накопительная</option><option value="6">VIP</option><option value="7">Fast</option></select><a href="#" onclick="extDelete('+i+');  return false">Удалить</a></div>';
			$('.extra-pins').append(t);
			i++;
      	}

      	function pinstat(program, id, status){


				$.post( "/pin_stat", { program : program, id : id, status : status } )
				  .done(function( data ) {
				  	console.log(data);

				  	if(data.success ){
				  		$('#res'+id+'prog'+program).html(data.res);
				  		if(status == 0){
				  			$('#sign'+id+'prog'+program).html('Заблокировать');
				  			$('#sign'+id+'prog'+program).attr('onclick', 'pinstat('+program+', '+id+' , 2); return false;');
				  			$('#res'+id+'prog'+program).css("color", "#23282c");
				  		} else {
				  			$('#sign'+id+'prog'+program).html('Разблокировать');
				  			$('#sign'+id+'prog'+program).attr('onclick', 'pinstat('+program+', '+id+', 0); return false;');
				  			$('#res'+id+'prog'+program).css("color", "red");
				  		}
				  	} else {
				  		alert('произошла ошибка')

				  	}
				});

				return false;
			}

			function pindelete(program, id){

				    var url = this.href;
				    var confirmText = "Вы уверены?";
				    if(confirm(confirmText)) {
						$.post( "/pin_delete", { program : program, id : id} )
						  .done(function( data ) {
						  	console.log(data);

						  	if(data.success ){
						  		$('#pin'+id+'prog'+program).fadeOut();
						  	} else {
						  		alert('произошла ошибка')

						  	}
						});
				    }
				    return false;
			}




      	function reject(){
      		if($('#cancel_reason').val() == ''){
	      		$('#cancel_reason').addClass('form-control-danger').focus();
	      		$('.invalid-feedback').fadeIn();
      		} else {
      			$('#status_id').val(2);
      			$('#create_pin_form').submit();
      		}
      	}

      	function extDelete(id){
      		$('#ep'+id).remove();
      		return false;
      	}


     	function switch_form(){

			//e.preventDefault();
			if( $('#switch_button').hasClass('active') ){
				$('.form-top').fadeOut();
				$('#switch_button').removeClass('active');
				$('#switch_button').html('Показать дополнительные поля');
			} else {
				$('.form-top').fadeIn();
				$('.form-top').css('display', 'flex');
				$('#switch_button').addClass('active');
				$('#switch_button').html('Скрыть дополнительные поля');
			}
		}


      	function checkAvailability(e) {
      		var i = $(e).data('username');
			//$("#loaderIcon").show();
				jQuery.ajax({
					url: "/username",
					data:'username='+ $(e).val(),
					type: "get",
			success:function(data){
				if(data.error == 0){
						$('input').data('userid', i).val(data.m);
						$('span').data('userid', i).html('ok');
						pincode = 1;
			    		return;
					}
			},
			error:function (){}
			});
			}

 isActive = true;

$().ready(function () {
    //EITHER USE A GLOBAL VAR OR PLACE VAR IN HIDDEN FIELD
    //IF FOR WHATEVER REASON YOU WANT TO STOP POLLING
    pollServer();


});

function pollServer()
{
    if (isActive)
    {
        window.setTimeout(function () {
            $.pjax.reload('#pin-grid');
            pollServer();
        }, 60000);
    }
}


	</script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>