<?php $__env->startSection('content'); ?>

<style type="text/css">


a.active{
	color: #ff0200;
	text-decoration: underline;
}



.tgbl{
	display: none;
}
.tgbl.active{
	display: block;
}
</style>
<div class="cabinet_main_container">
	<?php echo $__env->make('front.layout.menu', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
		<div class="body">
			<strong> </strong>
		<div class="profile_body_text">
		
		<?php if(Session::has('message')): ?>
                <div class="card-header" style="margin-bottom: 25px;">
            		<p class="alert <?php echo e(Session::get('alert-class', 'alert-info')); ?>"><?php echo e(Session::get('message')); ?></p>
                </div>
        <?php endif; ?>

<div class="card">


<?php if($invites->isEmpty()): ?>
Вы никого не пригласили
<?php else: ?>

<?php $__currentLoopData = $invites; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $invite): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
  <?php if(!$invite->bonus_id): ?>
  <?php
    $inv = json_decode($invite->invites);
    $reward = 10000 * count($inv);
    $fid = $invite->id;
  ?>
    Вы пригласили:
    <br/>
    <br/>
    <?php $__currentLoopData = $inv; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $i): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <p><b><?php echo e($i->user_name); ?></b></p>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

    вознаграждение = <?php echo e($reward); ?>

    <br>
    <br>
    <br>
    <br>
    <?php echo $__env->make('front.finish.bonus-form', compact('reward', 'fid'), \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <?php else: ?>

  <?php
    $bonus = \App\Models\Bonuses\StartBonusRequestModel::find($invite->bonus_id);
  ?>

        <?php if($bonus): ?>
          <div class="row" style="padding: 40px 0 0 10px;">
            <?php if($bonus->status_id == 0): ?>
            Ваша заявка находится на рассмотрении
            <?php elseif($bonus->status_id == 1): ?>
            Ваша заявка одобрена <br/>
            <br/>

            <?php elseif($bonus->status_id == 2): ?>
            Ваша заявка отклонена <br/>
            <br/>
            <pre>
              <?php echo e($bonus->cancel_reason); ?>

            </pre>
            <form action="/rebonus" method="POST">
              <?php echo csrf_field(); ?>
              <input type="text" hidden  value="<?php echo e($bonus->id); ?>" name="id">
              <button type="submit" href="#" class="btn btn-primary">Создать новую заявку</button>
            </form>
            <?php endif; ?>

          </div>
        <?php else: ?>
      asd
        <?php endif; ?>

<br><br><br><br><br>

  <?php endif; ?>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

<?php endif; ?>

</div>





		</div>
					</div>
				</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('front.layout.main', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>