<?php $__env->startSection('content'); ?>

<style type="text/css">
	.mxinfo{
		color: #00232a;
		font-weight: bold;
	}	

	.mxinfo small{
		color: #626262;
	}

	h4 {margin-bottom: 15px;}

	.rr{
		padding-left: 25px;
	}
	.modal-dialog{
		max-width: 90%;
	}

	.node{
		margin: 0 15px 15px 0;
		padding: 5px;
	}
	.node p{
		margin: 0;
	}	

	.node i{
		color: #c0b926;
		font-size: 7px;
	}

	.node p:nth-child(2) {
	 	font-size: 11px;
		color: #6f3f3f;
	}
 
	.laravel-grid .grid-wrapper { 
		max-height: 60vh;
		overflow-y: auto;
	} 
</style>

 <div class="card">


          <?php if(Session::has('message')): ?>
                  <div class="card-header">
            <p class="alert <?php echo e(Session::get('alert-class', 'alert-info')); ?>"><?php echo e(Session::get('message')); ?></p>
                   </div>
          <?php endif; ?>
                  <div class="card-body">
            <?php echo $grid; ?>

                  </div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
<script type="text/javascript">
	function confirmAction(obj) {
		if (confirm("Вы уверены?")) {
			doRevert(obj);
			return true;
		} else {
			return false;
		}
	}

	function doRevert(id){
		$('.modal').modal('hide');
		$.blockUI({ css: { backgroundColor: '#f00', color: '#fff'} });
		$.post( "/admin/ladder/revert", { id: $(id).data('id') } ).done(function(data){
					$.unblockUI();
					if(data.status == 1){
						$('#message-input-'+$i+'2').html(data.message);
						input.removeClass('is-invalid');	
						input.addClass('is-valid');
						$('#submit-button').fadeIn();
					} else {
						$('#message-input-'+$i).html(data.message);
						input.removeClass('is-valid');	
						input.addClass('is-invalid');
					}
				});
	}

</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>