<?php $__env->startSection('breadcrumbs'); ?>
	<li class="breadcrumb-item active">Завершившие</li>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<style type="text/css">
	.modal-dialog{max-width: 1200px;}
	#passchange_form{display: none;}
</style>
					<?php if(Session::has('message')): ?>
	                  	<div class="card-header">
							<p class="alert <?php echo e(Session::get('alert-class', 'alert-info')); ?>"><?php echo e(Session::get('message')); ?></p>
					<?php endif; ?>

					<?php if($errors->any()): ?>
	                   </div><div class="card-header">
							<p class="alert danger">
							<?php echo implode('', $errors->all('<div>:message</div>')); ?>

						</p>
	                   </div>

					<?php endif; ?>

                  <div class="card-body">
						<?php echo $grid; ?>

                  </div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
	<script type="text/javascript">
		$('.grid-title').html('Завршившие');


		function switch_form(){

			//e.preventDefault();
			if( $('#passchange_form').hasClass('active') ){
				$('#passchange_form').hide();
				$('#create_pin_form').fadeIn();
				$('#passchange_form').removeClass('active');
				$('#change_button').html('Поменять пароль');
			} else {
				$('#passchange_form').addClass('active');
				$('#create_pin_form').hide();
				$('#passchange_form').fadeIn();
				$('#change_button').html('Назад');
			}
		}

		$('#split').on('click', function(e) {
			e.preventDefault();
			$.post( "/manual/split", { id: $(this).data('id'), p: $(this).data('pr') } ).done(function(data){
					$.unblockUI();
					if(data.status == 1){
						$('#message-input-'+$i+'2').html(data.message);
						input.removeClass('is-invalid');	
						input.addClass('is-valid');
						$('#submit-button').fadeIn();
					} else {
						$('#message-input-'+$i).html(data.message);
						input.removeClass('is-valid');	
						input.addClass('is-invalid');
					}
				});
		});

		function split(id, pr){


            $.ajax({
                url: "/manual/split",
                data: { id: id, pr: pr },
                type: "POST",
                success: function (result) {
                    if(result.change){
                    	var text;
                    	if(result.status == 2){
                    		text = 'отклонена!';
                    	} else {
                    		text = 'одобрена!';                    		
                    	}
                    	alert('Ваша заявка ' + text);
                      window.location.replace("/partner/pin");
                    }
                    pollServer();
                },
                error: function () {
                    //ERROR HANDLING
                    pollServer();
                }});

		}

		function load(){
			$('.others').html();
				$('.others').html( $('.firsts').html() );
		}
	</script>
<?php $__env->stopSection(); ?>



<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>