<?php $__env->startSection('content'); ?>

	<div class="row">
		<div class="col-md-12">        		
	          <?php if(Session::has('message')): ?>
	                  <div class="card-header">
	            			<p class="alert <?php echo e(Session::get('alert-class', 'alert-info')); ?>"><?php echo e(Session::get('message')); ?></p>
	                   </div>
	          <?php endif; ?>
        	</div>
		</div>
		<div class="row">

				 <ul class="nav navbar-nav ml-auto" style="margin-right: 30px;">
        <li class="nav-item d-md-down-none">

        <?php
          $m = Config::get('matrix.get');
          $p = Session::get('programm', 1);
          if($p == null){
            Session::put('programm', 1);
            $p = Session::get('programm', 1);
          }
        ?>


            <div class="btn-group dropleft" id="mtype">
              <button class="btn btn-warning btn-lg dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <?php echo e($m[$p][1]); ?>

              </button>

              <div class="dropdown-menu">
                      <?php $__currentLoopData = $m; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $matrix): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php if($key == $p): ?>
                             <a class="dropdown-item active" href="" data-id="<?php echo e($key); ?>"><?php echo e($matrix[1]); ?></a>
                             <?php continue; ?>
                        <?php endif; ?>
                              <a class="dropdown-item" href="" data-id="<?php echo e($key); ?>"><?php echo e($matrix[1]); ?></a>
                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
              </div>

              <form action="/change" id="change-programm-form" style="display: none;" method="post">
                <?php echo csrf_field(); ?>
                <input type="text" hidden name="id" id="change-programm-id">
              </form>

            </div>

        </li>
      </ul>
      
			<div class="col-md-12" style="text-align: right; padding: 6px 48px 35px 0;">        		
				<a href="/admin/bookkeeping" style="color: #2575ce; font-weight: bold; margin-right: 15px;">Вознаграждения</a>
				<a href="/admin/bookkeeping/warehouse" style="color: #00a195; font-weight: bold; margin-right: 15px;">Склад</a>
				<a href="/admin/bookkeeping/credit" style="color: #00a195; font-weight: bold;">Поступления</a>
			</div>
		</div>
<style type="text/css">
	#bootstrap_modal .modal-dialog{
		max-width: 1200px;
	}
</style>
	<?php echo $grid; ?>


<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>