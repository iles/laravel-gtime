<?php $__env->startSection('content'); ?>
        <?php

		$m = Config::get('matrix.get');
        $p = Session::get('programm', 1);

		$class = $m[$p][0];

		 if($p == 3 || $p == 6 || $p == 8 ){
		 	$class .= $stage == 0 ? ' st' : ' fin';
		 }

          if($p == null){
            Session::put('programm', 1);
            $p = Session::get('programm', 1);
          }

          if($p == 1 || $p == 3 ){

          	if($p == 3 && $stage == 1){
	            $pk = 3;
	          	$pt = 4;
	          	$pl = 2;
          	} else {
	          	$pk = 4;
	          	$pt = 8;
	          	$pl = 3;
          	}
      	} else {
            $pk = 3;
          	$pt = 4;
          	$pl = 2;
      }
        ?>

<div class="cabinet_main_container">
					<?php echo $__env->make('front.layout.menu', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
					<div class="body"> 
						<div class="stairs_body_text">
							<h2 class="title"><?php echo e(__('Программа')); ?> <?php echo e(__($m[$p][1])); ?></h2>
							<?php if($in): ?>
								<div class="note_info_cont"> <?php echo e(__('Приветствуем новичка')); ?>: <?php echo e($in->name); ?> <?php echo e(__('и желаем дальнейших успехов')); ?> !</div>
							<?php endif; ?>
							<?php if($out): ?>
								<div class="note_info_cont"><?php echo e(__('Поздравляем партнера')); ?>: <?php echo e($out->name); ?> <?php echo e(__('с завершением полного цикла')); ?></div>
							<?php endif; ?>
							<div class="search_cont">
								<input placeholder="Ваш логин:" class="form-control" id="login_search" type="text">
								<input value="Найти" class="button" type="submit" id="search_ladder">
								<input   type="file" hidden style="display: none;">
							</div>
							<h2 class="title"><?php echo e(__('Просмотр лестницы пользователя')); ?> <span id="username" style="color:red;"><?php echo e(Auth::user()->name); ?></span></h2>
							<div class="stairs_cont_wrap" style="width: 100%; width: 100%;overflow: auto; padding-left: 16px; box-sizing: border-box; ">
								<div class="stairs_cont" style="width: 98%; min-width: 641px;">
									<?php
										$l = 0;
									?>

									<?php $__currentLoopData = $res; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $node): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
											<?php
												$l = $key;
											?>
										<div class="row">
											<?php $__currentLoopData = $node; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $v): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
												<div class=" node <?php echo e($class); ?>">
													<div><span><?php echo e($v['uname']); ?></span>
														<div class="sponsor" > &nbsp; <?php echo e($v['by_refer']); ?>

													</div>
													<div>
														<?php for($i = 0; $i < $v['stars']; $i++): ?>
														  <img src="/front/images/matrix-star.png">
														<?php endfor; ?>
													</div>
													</div>
												</div>
											<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
											<?php if($key == $pk): ?>
												<?php if(count($node) < $pt ): ?>
													<?php
													$c = $pt - count($node);
													?>
													<?php for($i = 0; $i < $c; $i++): ?>
													   <div class=" node <?php echo e($class); ?> empty"></div>
													<?php endfor; ?>
												<?php endif; ?>
											<?php endif; ?>
										</div>
									<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

									<?php if($l == $pl): ?>
									<div class="row">

										<?php for($i = 0; $i < $pt; $i++): ?>
											<div class=" node <?php echo e($class); ?> empty"></div>
										<?php endfor; ?>
									</div>
									<?php endif; ?>


								</div>
							</div>
							<div class="clearfix"></div>
						</div>
					</div>
				</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
	<script type="text/javascript">
		$(document).ready( function(){
			//search_ladder('<?php echo e(Auth::user()->name); ?>');
			$('#search_ladder').on('click', function(){
				if( $('#login_search').val() == '' ){
					alert('Введите логин');
					return false;
				} else {
					search_ladder( $('#login_search').val() );
				}
			})

			$('.node').on('click', function(){
				var name = $(this).find('span').html();
				search_ladder(name);
			})
		});

		function search_ladder(name){
			$('.stairs_cont').fadeOut();
			$('.stairs_cont').html('');
			$('#username').html(name);
			$.get( "/partner/search", { name: name })
			  .done(function( data ) {
			    if(data == 'Пользователь не найден'){
					$('.stairs_cont').html('<p style="color:red">'+ data + '</p>');
			    	$('.stairs_cont').fadeIn();
			    	return false;
			    }
			    build_ladder( data );
			  });
		}

		function build_ladder(data){
			var l = 0;
			var s = data.stage == 1;
			if(s == 1){
				st = 'fin';
			} else {
				st = 'st';

			}

			$('.stairs_cont').html('');
			$.each( data.levels, function( key, value ) {
				l = key;
				$('.stairs_cont').append('<div class="row level_ui_'+ key +'"></div> ');
				build_level(value, key, st)
			});

			if(l == <?php echo e($pl); ?>){
				if( <?php echo e($pl); ?> == 3){
				$('.stairs_cont').append('<div class="row level_ui_4"><div class="node  <?php echo e($m[$p][0]); ?> empty"></div><div class="node  <?php echo e($m[$p][0]); ?> empty"></div><div class="node  <?php echo e($m[$p][0]); ?> empty"></div><div class="node  <?php echo e($m[$p][0]); ?> empty" ></div><div class="node  <?php echo e($m[$p][0]); ?> empty"></div><div class="node  <?php echo e($m[$p][0]); ?> empty"></div><div class="node  <?php echo e($m[$p][0]); ?> empty"></div><div class="node  <?php echo e($m[$p][0]); ?> empty"></div></div> ');

				} else {
				$('.stairs_cont').append('<div class="row level_ui_3"><div class="node  <?php echo e($m[$p][0]); ?>  empty"></div><div class="node  <?php echo e($m[$p][0]); ?> empty"></div><div class="node  <?php echo e($m[$p][0]); ?> empty"></div><div class="node  <?php echo e($m[$p][0]); ?> empty"></div></div> ');
				}
			}

			$('.node').on('click', function(){
				var name = $(this).find('span').html();
				console.log(name);
				search_ladder(name);
			})


			$('.stairs_cont').fadeIn();

		}

		function build_level(value, key, st){
			$.each( value, function( keyv, valuev ) {
				$('.level_ui_'+ key ).append('<div class="node  <?php echo e($m[$p][0]); ?>  '+ st +'"><div><span>' + valuev.uname + '</span><div class="sponsor"> ' + valuev.by_refer + '</div><div>' + build_stars(valuev.stars) + '</div></div></div>');
			});

			if(key == <?php echo e($pk); ?>){
				if( value.length < <?php echo e($pt); ?> ){
					var c = <?php echo e($pt); ?> - value.length;
					for (index = 0; index < c; ++index) {
						$('.level_ui_'+ key ).append('<div class="node  <?php echo e($m[$p][0]); ?> empty" "></div>');
					}
				}
			}
		}

		function build_stars($n){
			var stars = '';
			var i = 0;
			while (i < $n) {
			  stars = stars + '<img src="/front/images/matrix-star.png">'  ;
			  i++;
			}
			return stars;
		}
	</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('front.layout.main', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>