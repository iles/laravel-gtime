<?php $__env->startSection('content'); ?>

<div class="cabinet_main_container authorization">
  <div class="header">
    <div class="row">
      <div class="col-md-10">
        <h2 class="title">Завершение программы</h2>        
      </div>
      <div class="col-md-2">
        <a class="link" href="#" onclick="document.getElementById('logout-form').submit(); return false;">
          <i class="icon-logout"></i>выход
        </a>
        <form id="logout-form" action="<?php echo e(route('partner.logout')); ?>" method="POST" style="display: none;">
          <?php echo csrf_field(); ?>
        </form>
      </div>
    </div>

  </div>
	<div class="body">
		<div class="auth_cont" style="max-width: 100%;">   

                <?php if($bonus): ?>
                    <div class="row" style="padding: 40px 0 0 10px;">
                      <?php if($bonus->status_id == 0): ?>
                      Ваша заявка находится на рассмотрении
                      <?php elseif($bonus->status_id == 1): ?>
                        Ваша заявка одобрена и находится на рассмотрении в бухгалтерии<br/>      
                      <?php elseif($bonus->status_id == 4): ?>
                        Ваша заявка одобрена       
                      <?php elseif($bonus->status_id == 5): ?>
                        Ваша заявка отклонена
                        <br/>
                        <form action="/partner/rebonus" method="POST">
                          <?php echo csrf_field(); ?>
                          <input type="text" hidden  value="<?php echo e($bonus->id); ?>" name="id">
                          <button type="submit" href="#" class="btn btn-primary">Создать новую заявку</button>
                        </form>  
                  
                      <?php elseif($bonus->status_id == 2): ?>
                      Ваша заявка отклонена <br/>
                      <br/>
                      <pre>
                        <?php echo e($bonus->cancel_reason); ?>

                      </pre>
                      <form action="/partner/rebonus" method="POST">
                        <?php echo csrf_field(); ?>
                        <input type="text" hidden  value="<?php echo e($bonus->id); ?>" name="id">
                        <button type="submit" href="#" class="btn btn-primary">Создать новую заявку</button>
                      </form>
                      <?php endif; ?>
                    </div>
                  <?php else: ?>
                    <p style="margin: 25px 0 50px 0; font-weight: bold; font-size: 16px; color: #3e3d3c;">
                    -Вознаграждение в денежном выражении 80 000тенге*.
                    + и варианты выбора продукции, входящих в пакет «Накопительная»</p>
                    <?php echo $__env->make('front.finish.plus.formC', compact($finished), \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                  <?php endif; ?>


  <p style="margin: 65px 0 25px 0; font-weight: bold; font-size: 16px; color: #3e3d3c;">Пинкод для регистрации в Накопительную программу (с номинальной стоимостью 15 000тг)</p>
            <p style="margin: 25px 0 25px 0; font-weight: bold; font-size: 16px; color: #3e3d3c;">2 (два) пинкода для регистрации в FAST программу (с общей номинальной стоимостью 120 000тг), и варианты выбора продукции, входящих в пакет «FAST»</p>
<?php if(!$pin): ?>



          <?php if($transfer): ?>
          <br>
          <br>
          <br>
            <?php if($transfer->status_id == 0): ?>
              Ваша заявка находится на рассмотрении
            <?php elseif($transfer->status_id == 2): ?>
              Ваша заявка отклонена
            <?php else: ?>
              Ваша заявка одобрена
              <br/>

          <p style="margin: 65px 0 25px 0; font-weight: bold; font-size: 16px; color: #3e3d3c;">Пинкод для регистрации в Накопительную программу (с номинальной стоимостью 15 000тг), </p>
          <p style="margin: 25px 0 25px 0; font-weight: bold; font-size: 16px; color: #3e3d3c;">2 (два) пинкода для регистрации в FAST программу (с общей номинальной стоимостью 120 000тг), и варианты выбора продукции, входящих в пакет «FAST»</p>

          <?php echo e(Form::open(['route' => 'pin.add', 'method'=>'post'])); ?>

            <?php echo csrf_field(); ?>
            <input type="text" name="triple" value="g" hidden="hidden">
            <input type="text" name="program_id" value="5" hidden="hidden">

            <button class="btn btn-lg btn-success" type="submit" onclick="return confirm('Вы уверены?')">
              <i class="fa fa-dot-circle-o"></i> Сгенерировать
            </button>  
          <?php echo e(Form::close()); ?>

            <?php endif; ?>
          <?php else: ?>

          <p style="margin: 65px 0 25px 0; font-weight: bold; font-size: 16px; color: #3e3d3c;">Пинкод для регистрации в Накопительную программу (с номинальной стоимостью 15 000тг), </p>
          <p style="margin: 25px 0 25px 0; font-weight: bold; font-size: 16px; color: #3e3d3c;">2 (два) пинкода для регистрации в FAST программу (с общей номинальной стоимостью 120 000тг), и варианты выбора продукции, входящих в пакет «FAST»</p>

          <?php echo e(Form::open(['route' => 'pin.add', 'method'=>'post'])); ?>

            <?php echo csrf_field(); ?>
            <input type="text" name="triple" value="g" hidden="hidden">
            <input type="text" name="program_id" value="5" hidden="hidden">

            <button class="btn btn-lg btn-success" type="submit" onclick="return confirm('Вы уверены?')">
              <i class="fa fa-dot-circle-o"></i> Сгенерировать
            </button>  
          <?php echo e(Form::close()); ?>

          <?php endif; ?>   


          <?php if($transfer): ?>
          
          <?php else: ?> 

          <a href="#" style="margin: 65px 0 25px 0; display: inline-block; font-size: 16px; text-decoration: underline;" id="transfer-button">Временная передача пин-кодов </a>

      <div class="card" id="transfer-form">

      <?php echo e(Form::open(['route' => 'transfer.add', 'method'=>'post', 'files' => true, 'autocomplete' => 'off'])); ?>


      <div class="card-body">
        <div class="form-group row">            
          <?php echo e(Form::label('login', 'Логин:', array('class' => 'col-md-3 col-form-label'))); ?>

          <div class="col-md-9">
            <?php echo e(Form::text('login', Auth::user()->name,  array('class' => 'form-control', 'required'=>'required', 'readonly' => 'readonly') )); ?>

          </div>
        </div>        
        <div class="form-group row">            
          <?php echo e(Form::label('firstname', 'ФИО:', array('class' => 'col-md-3 col-form-label'))); ?>

          <div class="col-md-9">
            <?php echo e(Form::text('fullname', Auth::user()->familiya,  array('class' => 'form-control', 'required'=>'required') )); ?>

          </div>
        </div>           
        <div class="form-group row">            
          <?php echo e(Form::label('firstname', 'ФИО получателя:', array('class' => 'col-md-3 col-form-label'))); ?>

          <div class="col-md-9">
            <?php echo e(Form::text('fullname2', null,  array('class' => 'form-control', 'required'=>'required') )); ?>

          </div>
        </div>
        <div class="form-group row">
          <?php echo e(Form::label('image', 'Скан документа :', array('class' => 'col-md-3 col-form-label'))); ?>

          <div class="col-md-9">
           <?php echo e(Form::file('image[]', array('class' => 'form-control', 'required'=>'required', 'multiple'=>true, 'accept'=>".jpeg, .jpg, .png, .pdf") )); ?>

          </div>
        </div>
         <div class="form-group row" style="display: none;">
            <input type="text" hidden value="<?php echo e($finished->id); ?>" name="finished_id">
            <input type="text" hidden value="5" name="program_id">
            <input type="text" hidden value="3" name="variant">
            <?php echo e(Form::text('uid', Auth::user()->user_id,  array('class' => 'form-control', 'required'=>'required') )); ?>

        </div>
      </div>
        
      <div class="card-footer">
        <button class="btn btn-sm btn-primary" type="submit">
          <i class="fa fa-dot-circle-o"></i> Отправить
        </button>     
      </div>


      <?php echo e(Form::close()); ?>


      <?php endif; ?>










<?php else: ?>

                 <div class="row">
                   <div class="col-md-4"><b><?php echo e($pin->pin); ?></b></div>
                    <div class="col-md-6">
                      <?php if( $pin->status_id == 0 ): ?>
                      <pre>Действителен до: <br/><?php echo e($pin->expired_at); ?></pre>
                      <?php else: ?>
                      <pre style="color: red;">Заблокирован</pre>
                      <?php endif; ?>
                    </div>
                  </div>
 <?php endif; ?>


  
  <?php if($pin2): ?>
                   <div class="row">
                   <div class="col-md-4"><b><?php echo e($pin2->pin); ?></b></div>
                    <div class="col-md-6">
                      <?php if( $pin2->status_id == 0 ): ?>
                      <pre>Действителен до: <br/><?php echo e($pin2->expired_at); ?></pre>
                      <?php else: ?>
                      <pre style="color: red;">Заблокирован</pre>
                      <?php endif; ?>
                    </div>
                  </div>
  <?php endif; ?>  

  <?php if($pin3): ?>
                   <div class="row">
                   <div class="col-md-4"><b><?php echo e($pin3->pin); ?></b></div>
                    <div class="col-md-6">
                      <?php if( $pin3->status_id == 0 ): ?>
                      <pre>Действителен до: <br/><?php echo e($pin3->expired_at); ?></pre>
                      <?php else: ?>
                      <pre style="color: red;">Заблокирован</pre>
                      <?php endif; ?>
                    </div>
                  </div>
  <?php endif; ?>
  




		</div>
	</div>

</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
<script type="text/javascript">


$(document).ready(function(){
  $('#form-click').on( 'click', function(e){
  e.preventDefault();
  $('.profile_table').toggleClass('active');
  })

  $(":input").inputmask();


});




</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('front.layout.main', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>