<?php $__env->startSection('content'); ?>
<div class="card shadow mb-4">
  <div class="card-header"> <strong> Переводы </strong>  
  </div>
  <div class="card-body">
    <div class="row">
      <div class="col-12"> 
        <form action="<?php echo e(url('admin/translation')); ?>" method="post">
           <?php echo csrf_field(); ?>
          <button class="btn btn-danger pull-right copy" type="button" >+ Добавить</button>
          <button class="btn btn-success" >Сохранить</button>

          <table class="table table-striped" id="table2">
            <thead>
              <tr>
                <th>Ключ</th>
                <?php $__currentLoopData = $langlist; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $lang): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <th>
                  <?php echo e($lang); ?>

                </th>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
              </tr>
            </thead>
            <tbody>
              <?php $__currentLoopData = $list; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key1 => $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
              <tr>
                <td>
                  <input type="text" name="keys[]" value="<?php echo e($key1); ?>" class="form-control">
                </td>
                <?php $__currentLoopData = $langlist; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key2 => $lang): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <td>
                  <input type="text" name="<?php echo e($key2); ?>[]" value="<?php echo e($item[$key2]); ?>" class="form-control">
                </td>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
              </tr>
              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
              <tr style="display: none" class="shablon">
                <td>
                  <input type="text" name="keys[]" value="" class="form-control">
                </td>
                <?php $__currentLoopData = $langlist; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key2 => $lang): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <td>
                  <input type="text" name="<?php echo e($key2); ?>[]" class="form-control">
                </td>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
              </tr>
            </tbody>
          </table>
        </form>
      </div>
        
    </div>
  </div>
</div>
<?php $__env->stopSection(); ?>

<?php $__env->startPush('scripts'); ?>
    <script type="text/javascript">
      $('.copy').on('click',function(){
        console.log('asa');
        $tr = $('tr.shablon')
        var $clone = $tr.clone();
        $clone.find(':text').val('');
        $clone.removeClass('shablon').show();
        $tr.after($clone);
      })
    </script>
<?php $__env->stopPush(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>