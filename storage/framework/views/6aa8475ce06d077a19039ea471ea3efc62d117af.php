<div class="card">
    <form method="post" action="<?php echo e(action('PinsController@store')); ?>" id="create_pin_form" class="form-horizontal" accept-charset="UTF-8" enctype="multipart/form-data"> 
  <div class="card-header">
    <strong>Создать заявку</strong>
  </div>
    <div class="card-body">
        <?php echo csrf_field(); ?>
        <a href="#" id="switch_button" onclick="switch_form(); return false;">Показать дополнительные поля</a>
        <br/>
        <br/>
        <div class="form-group form-top row">
          <label class="col-md-3 col-form-label" for="hf-password">Логин</label>
          <div class="col-md-9">           
            <input class="form-control" name="name"type="text">
          </div>
        </div>         

        <div class="form-group form-top row">
          <label class="col-md-3 col-form-label" for="hf-password">ФИО</label>
          <div class="col-md-9">           
            <input class="form-control" name="name" type="text">
          </div>
        </div>        

        <div class="form-group form-top row">
          <label class="col-md-3 col-form-label" for="hf-password">ИИН</label>
          <div class="col-md-9">           
            <input class="form-control" name="iin"  type="text">
          </div>
        </div>                

        <div class="form-group form-top row">
          <label class="col-md-3 col-form-label" for="hf-password">Номер квитанции</label>
          <div class="col-md-9">           
            <input class="form-control" name="bill" type="text">
          </div>
        </div>            

        <div class="form-group form-top row">
          <label class="col-md-3 col-form-label" for="hf-password">Сумма</label>
          <div class="col-md-9">           
            <input class="form-control" name="summ"  type="text">
          </div>
        </div>             

        <div class="form-group form-top row">
          <label class="col-md-3 col-form-label" for="hf-password">Skype</label>
          <div class="col-md-9">           
            <input class="form-control" name="skype"  type="text">
          </div>
        </div>               

        <div class="form-group form-top row">
          <label class="col-md-3 col-form-label" for="hf-password">Страна</label>
          <div class="col-md-9">           
            <input class="form-control" name="skype" type="text">
          </div>
        </div>            

        <div class="form-group form-top row">
          <label class="col-md-3 col-form-label" for="hf-password">Примечание</label>
          <div class="col-md-9">           
            <textarea class="form-control" name="user_name"></textarea>
          </div>
        </div>           

        <div class="form-group form-top row">
          <label class="col-md-3 col-form-label" for="hf-password">Фото квитанции</label>
          <div class="col-md-9">           
              <input class="form-control"  multiple="" accept=".jpeg, .jpg, .png, .pdf" name="image[]" type="file">
          </div>
        </div>      







        <div class="form-group form-top row">
          <label class="col-md-3 col-form-label" for="hf-password">Приписка</label>
          <div class="col-md-9">
            <textarea name="note"  class="form-control" name="note"></textarea>
          </div>
        </div>





        <div class="form-group row">
          <div class="col-md-12">
            <div class="row"> <div class="col-md-12"><p><b>Генерация пин кодов:</b></p></div> </div>
            <div class="row gen-pin">
              <label class="col-md-12 col-form-label" for="hf-password">Старт</label>
              <div class="col-md-12">           
                <input type="number" min="0" name="start_pin">
                
                <label class="col-form-label" for="start_pin_kassa">
                  <input type="checkbox" id="start_pin_kassa" name="start_pin_kassa">Касса
                </label>                
                <label class="col-form-label" for="start_pin_timeless">
                  <input type="checkbox" id="start_pin_timeless" name="start_pin_timeless">Бессрочный
                </label>
              </div>            
            </div>          
            <div class="row gen-pin">
              <label class="col-md-12 col-form-label" for="hf-password">Накопительная</label>
              <div class="col-md-12">           
                <input type="number" min="0" name="bank_pin">
                
                <label class="col-form-label" for="bank_pin_kassa">
                  <input type="checkbox" id="bank_pin_kassa" name="bank_pin_kassa">Касса
                </label>                
                <label class="col-form-label" for="bank_pin_timeless">
                  <input type="checkbox" id="bank_pin_timeless" name="bank_pin_timeless">Бессрочный
                </label>
              </div>            
            </div>               

            <div class="row gen-pin">
              <label class="col-md-12 col-form-label" for="hf-password">Накопительная +</label>
              <div class="col-md-12">           
                <input type="number" min="0" name="plus_pin">
                
                <label class="col-form-label" for="plus_pin_kassa">
                  <input type="checkbox" id="plus_pin_kassa" name="plus_pin_kassa">Касса
                </label>                
                <label class="col-form-label" for="plus_pin_timeless">
                  <input type="checkbox" id="plus_pin_timeless" name="plus_pin_timeless">Бессрочный
                </label>
              </div>            
            </div>            
        
            <div class="row gen-pin">
              <label class="col-md-12 col-form-label" for="hf-password">Основная</label>
              <div class="col-md-12">           
                <input type="number" min="0" name="main_pin">
                
                <label class="col-form-label" for="main_pin_kassa">
                  <input type="checkbox" id="main_pin_kassa" name="main_pin_kassa">Касса
                </label>                
                <label class="col-form-label" for="main_pin_timeless">
                  <input type="checkbox" id="main_pin_timeless" name="main_pin_timeless">Бессрочный
                </label>
              </div>            
            </div>           
            <div class="row gen-pin">
              <label class="col-md-12 col-form-label" for="hf-password">Vip</label>
              <div class="col-md-12">           
                <input type="number" min="0" name="vip_pin">
                
                <label class="col-form-label" for="vip_pin_kassa">
                  <input type="checkbox" id="vip_pin_kassa" name="vip_pin_kassa">Касса
                </label>                
                <label class="col-form-label" for="vip_pin_timeless">
                  <input type="checkbox" id="vip_pin_timeless" name="vip_pin_timeless">Бессрочный
                </label>
              </div>            
            </div>   
            <div class="row gen-pin">
              <label class="col-md-12 col-form-label" for="hf-password">Fast</label>
              <div class="col-md-12">           
                <input type="number" min="0" name="fast_pin">
                
                <label class="col-form-label" for="fast_pin_kassa">
                  <input type="checkbox" id="fast_pin_kassa" name="fast_pin_kassa">Касса
                </label>                
                <label class="col-form-label" for="fast_pin_timeless">
                  <input type="checkbox" id="fast_pin_timeless" name="fast_pin_timeless">Бессрочный
                </label>
              </div>            
            </div>            
             <div class="row gen-pin">
              <label class="col-md-12 col-form-label" for="hf-password">Bonus</label>
              <div class="col-md-12">           
                <input type="number" min="0" name="bonus_pin">
                
                <label class="col-form-label" for="bonus_pin_kassa">
                  <input type="checkbox" id="bonus_pin_kassa" name="bonus_pin_kassa">Касса
                </label>                
                <label class="col-form-label" for="bonus_pin_timeless">
                  <input type="checkbox" id="bonus_pin_timeless" name="bonus_pin_timeless">Бессрочный
                </label>
              </div>            
            </div>              

          </div>
        </div> 
 



        
    </div>
        
    <div class="card-footer">
      <button class="btn btn-sm btn-primary" type="submit">
      <i class="fa fa-dot-circle-o" type="submit"></i> 
      Сохранить
      </button>
    </div>

    </form>
</div>