<?php $__env->startSection('breadcrumbs'); ?>
	<li class="breadcrumb-item active">Аннулирование</li>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

<style type="text/css">
	.act{display: none;}
</style>
<div class="row">
<div class="col-md-12">
	


<div class="card">
   <form method="post" action="<?php echo e(action('PincodesController@docancel')); ?>" id="swap_form" class="form-horizontal" accept-charset="UTF-8" enctype="multipart/form-data"> 

	<div class="card-header">
	    <strong>Анулирование</strong>
		<?php if(Session::has('message')): ?>
			<p class="alert <?php echo e(Session::get('alert-class', 'alert-info')); ?>"><?php echo e(Session::get('message')); ?></p>
		<?php endif; ?>
	</div>

    <div class="card-body">
        <?php echo csrf_field(); ?>

        <div class="form-group row">
          <label class="col-md-1 col-form-label">Пин код:</label>
          <div class="col-md-6">           
            <input class="form-control" id="login-input-1" name="pin" type="text">
           	<span class="invalid-feedback" role="alert"><strong id="message-input-1"></strong></span>
           	<span class="valid-feedback" role="alert"><strong id="message-input-12"></strong></span>
          </div>          
          <div class="col-md-1">           
			<button class="btn btn-sm btn-primary pull-right" onclick="check(1); return false;"> <i class="fa fa-plus"></i> Проверить</button>  
          </div>
        </div>  

        <div class="form-group row act">
          <label class="col-md-1 col-form-label">Логин:</label>
          <div class="col-md-6">           
           <strong id="d-login"></strong>
          </div>          
        </div>           

        <div class="form-group row act">
          <label class="col-md-1 col-form-label">ФИО:</label>
          <div class="col-md-6">           
            <strong id="d-fullname"></strong>
          </div>          
        </div>           

        <div class="form-group row act">
          <label class="col-md-1 col-form-label">Логин спонсора:</label>
          <div class="col-md-6">           
           <strong id="d-sponsor_login"></strong>
          </div>          
        </div>           

        <div class="form-group row act">
          <label class="col-md-1 col-form-label">ФИО спонсора:</label>
          <div class="col-md-6">           
            <strong id="d-sponsor_fullname"></strong>
          </div>          
        </div>         

        <div class="form-group row act">
          <label class="col-md-1 col-form-label">Дата регистрации:</label>
          <div class="col-md-6">           
            <strong id="cr_date"></strong>
          </div>          
        </div>           

        <div class="form-group row act">
          <label class="col-md-1 col-form-label">Позиция</label>
          <div class="col-md-6">           
            <strong id="cr_pos"></strong>
          </div>          
        </div>            
        
    </div>
        
    <div class="card-footer act">
      <button class="btn btn-sm btn-primary"  type="submit">Анулировать</button>
    </div>

    </form>
</div>

</div>
</div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
	<script type="text/javascript">
		var p1, p2, l1, l2, m1, m2;
		function check($i){
			
			var input = $('#login-input-'+$i)
			var login = input.val();
			if(login == ''){
				$('#message-input-'+$i).html('Введите логин');
				input.removeClass('is-valid');	
				input.addClass('is-invalid');	
				return false;
			} else {
				$.blockUI({ css: { backgroundColor: '#f00', color: '#fff'} });
				$.get( "/pincodecheck", { pin: login } ).done(function(data){
					$.unblockUI();
					var c = data.c;
          var p = data.p;
					if(c.error == 'true'){
						alert('Пин код не действителен');
					} else {
						$('.act').fadeIn();
            $('.act').css('display', '  flex');
						$('#d-login').html(c.login);
						$('#d-fullname').html(c.fullname);
						$('#d-sponsor_login').html(c.sponsor_login);
						$('#d-sponsor_fullname').html(c.sponsor_fullname);
						$('#cr_date').html(c.created_at);
            $('#cr_pos').html('№ лестницы:' + p.mx_id + ';   Уровень:' + p.level + ';   Позиция:' + p.position);
					}
				});
			}
		}

	</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>