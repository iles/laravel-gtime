<?php $__env->startSection('content'); ?>

 <div class="card">


          <?php if(Session::has('message')): ?>
                  <div class="card-header">
            <p class="alert <?php echo e(Session::get('alert-class', 'alert-info')); ?>"><?php echo e(Session::get('message')); ?></p>
                   </div>
          <?php endif; ?>
                  <div class="card-body">
            <?php echo $grid; ?>

                  </div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
<script type="text/javascript">
      	function reject(){
      		if($('#cancel_reason').val() == ''){
	      		$('#cancel_reason').addClass('form-control-danger').focus();
	      		$('.invalid-feedback').fadeIn();
      		} else {
      			$('#status_id').val(2);
      			$('#swap_form').submit();
      		}
      	}
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>