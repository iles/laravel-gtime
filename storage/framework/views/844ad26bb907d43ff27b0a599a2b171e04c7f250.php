<?php $__env->startSection('content'); ?>


<style type="text/css">
  .transfer-form{
    display: none;
  }  

  .transfer-form.active{
    display: block;
  }
</style>

<div class="cabinet_main_container authorization">
  <div class="header">
    <div class="row">
      <div class="col-md-10">
        <h2 class="title">Завершение программы</h2>        
      </div>
      <div class="col-md-2">
        <a class="link" href="#" onclick="document.getElementById('logout-form').submit(); return false;">
          <i class="icon-logout"></i>выход
        </a>
        <form id="logout-form" action="<?php echo e(route('partner.logout')); ?>" method="POST" style="display: none;">
          <?php echo csrf_field(); ?>
        </form>
      </div>
    </div>

  </div>
  <div class="body">
    <div class="auth_cont" style="max-width: 100%;">

<p style="margin: 25px 0 50px 0; font-weight: bold; font-size: 16px; color: #3e3d3c;">Поздравляем Вы завершили программу «Накопительная +» для получения вознаграждения выберите подходящий вариант и заполните соответствующую форму ниже:</p>

<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingOne">
      <h4 class="panel-title">
        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
          Вариант А
        </a>
      </h4>
    </div>
    <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
      <div class="panel-body">
        <p style="margin: 25px 0 50px 0; font-weight: bold; font-size: 16px; color: #3e3d3c;">
          -Вознаграждение в денежном выражении 20 000тенге*.
          + варианты выбора продукции, входящих в пакет «Основной».</p>
          <?php echo $__env->make('front.finish.plus.formA', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>

          <p style="margin: 65px 0 25px 0; font-weight: bold; font-size: 16px; color: #3e3d3c;">Пинкод для регистрации в Основную программу (с номинальной стоимостью 160 000тг)</p>
          <?php echo e(Form::open(['route' => 'pin.add', 'method'=>'post'])); ?>

            <?php echo csrf_field(); ?>
            <input type="text" name="program_id" value="3" hidden="hidden">
            <input type="text" name="program_type" value="3" hidden="hidden">
            <input type="text" name="variant" value="1" hidden="hidden">
            <button class="btn btn-lg btn-success" type="submit" onclick="return confirm('Вы уверены?')">
            <i class="fa fa-dot-circle-o"></i> Сгенерировать
            </button>  
          <?php echo e(Form::close()); ?>


                    <a href="#" style="margin: 65px 0 25px 0; display: inline-block; font-size: 16px; text-decoration: underline;" class="transfer-button">Временная передача пин-кодов </a>

      <div class="card transfer-form" id="">

      <?php echo e(Form::open(['route' => 'transfer.add', 'method'=>'post', 'files' => true, 'autocomplete' => 'off'])); ?>


      <div class="card-body">
        <div class="form-group row">            
          <?php echo e(Form::label('login', 'Логин:', array('class' => 'col-md-3 col-form-label'))); ?>

          <div class="col-md-9">
            <?php echo e(Form::text('login', Auth::user()->name,  array('class' => 'form-control', 'required'=>'required', 'readonly' => 'readonly') )); ?>

          </div>
        </div>        
        <div class="form-group row">            
          <?php echo e(Form::label('firstname', 'ФИО:', array('class' => 'col-md-3 col-form-label'))); ?>

          <div class="col-md-9">
            <?php echo e(Form::text('fullname', Auth::user()->familiya,  array('class' => 'form-control', 'required'=>'required') )); ?>

          </div>
        </div>           
        <div class="form-group row">            
          <?php echo e(Form::label('firstname', 'ФИО получателя:', array('class' => 'col-md-3 col-form-label'))); ?>

          <div class="col-md-9">
            <?php echo e(Form::text('fullname2', null,  array('class' => 'form-control', 'required'=>'required') )); ?>

          </div>
        </div>
        <div class="form-group row">
          <?php echo e(Form::label('image', 'Скан документа :', array('class' => 'col-md-3 col-form-label'))); ?>

          <div class="col-md-9">
           <?php echo e(Form::file('image[]', array('class' => 'form-control', 'required'=>'required', 'multiple'=>true, 'accept'=>".jpeg, .jpg, .png, .pdf") )); ?>

          </div>
        </div>
         <div class="form-group row" style="display: none;">
            <input type="text" hidden value="<?php echo e($finished->id); ?>" name="finished_id">
            <input type="text" hidden value="5" name="program_id">
            <input type="text" hidden value="1" name="variant">
            <?php echo e(Form::text('uid', Auth::user()->user_id,  array('class' => 'form-control', 'required'=>'required') )); ?>

        </div>
      </div>
        
      <div class="card-footer">
        <button class="btn btn-sm btn-primary" type="submit">
          <i class="fa fa-dot-circle-o"></i> Отправить
        </button>     
      </div>


      <?php echo e(Form::close()); ?>


</div>
      </div>
    </div>
  </div>

  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingThree">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
            Вариант Б
        </a>
      </h4>
    </div>
    <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
      <div class="panel-body">
          <p style="margin: 25px 0 50px 0; font-weight: bold; font-size: 16px; color: #3e3d3c;">Поздравляем! <br/>
          -Вознаграждение в денежном выражении 80 000тенге*.
          + и варианты выбора продукции, входящих в пакет «Накопительная»
          </p>

          <?php echo $__env->make('front.finish.plus.formB', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>


          <p style="margin: 65px 0 25px 0; font-weight: bold; font-size: 16px; color: #3e3d3c;">Пинкод для регистрации в Накопительную программу (с номинальной стоимостью 15 000тг), </p>

          <?php echo e(Form::open(['route' => 'pin.add', 'method'=>'post'])); ?>

            <?php echo csrf_field(); ?>
            <input type="text" name="program_id" value="4" hidden="hidden">
            <input type="text" name="program_type" value="3" hidden="hidden">
            <input type="text" name="variant" value="2" hidden="hidden">
            <button class="btn btn-lg btn-success" type="submit" onclick="return confirm('Вы уверены?')">
              <i class="fa fa-dot-circle-o"></i> Сгенерировать
            </button>  
          <?php echo e(Form::close()); ?>


          <a href="#" style="margin: 65px 0 25px 0; display: inline-block; font-size: 16px; text-decoration: underline;" class="transfer-button">Временная передача пин-кодов </a>

      <div class="card transfer-form" id="">

      <?php echo e(Form::open(['route' => 'transfer.add', 'method'=>'post', 'files' => true, 'autocomplete' => 'off'])); ?>


      <div class="card-body">
        <div class="form-group row">            
          <?php echo e(Form::label('login', 'Логин:', array('class' => 'col-md-3 col-form-label'))); ?>

          <div class="col-md-9">
            <?php echo e(Form::text('login', Auth::user()->name,  array('class' => 'form-control', 'required'=>'required', 'readonly' => 'readonly') )); ?>

          </div>
        </div>        
        <div class="form-group row">            
          <?php echo e(Form::label('firstname', 'ФИО:', array('class' => 'col-md-3 col-form-label'))); ?>

          <div class="col-md-9">
            <?php echo e(Form::text('fullname', Auth::user()->familiya,  array('class' => 'form-control', 'required'=>'required') )); ?>

          </div>
        </div>           
        <div class="form-group row">            
          <?php echo e(Form::label('firstname', 'ФИО получателя:', array('class' => 'col-md-3 col-form-label'))); ?>

          <div class="col-md-9">
            <?php echo e(Form::text('fullname2', null,  array('class' => 'form-control', 'required'=>'required') )); ?>

          </div>
        </div>
        <div class="form-group row">
          <?php echo e(Form::label('image', 'Скан документа :', array('class' => 'col-md-3 col-form-label'))); ?>

          <div class="col-md-9">
           <?php echo e(Form::file('image[]', array('class' => 'form-control', 'required'=>'required', 'multiple'=>true, 'accept'=>".jpeg, .jpg, .png, .pdf") )); ?>

          </div>
        </div>
         <div class="form-group row" style="display: none;">
            <input type="text" hidden value="<?php echo e($finished->id); ?>" name="finished_id">
            <input type="text" hidden value="5" name="program_id">
            <input type="text" hidden value="2" name="variant">
            <?php echo e(Form::text('uid', Auth::user()->user_id,  array('class' => 'form-control', 'required'=>'required') )); ?>

        </div>
      </div>
        
      <div class="card-footer">
        <button class="btn btn-sm btn-primary" type="submit">
          <i class="fa fa-dot-circle-o"></i> Отправить
        </button>     
      </div>


      <?php echo e(Form::close()); ?>


</div>

          <div>
            <p style="margin: 60px 0 25px 0;; font-weight: bold; font-size: 14px; color: #3e3d3c;">
              продукция компании, входящих в пакет, предназначенный для данного варианта с номинальной стоимостью 85 000тг:            
            </p>
            <p><b>• Вариант A</b> </p>
            <p>   - 1 шт. Антицеллюлитный крем </p>
            <p>   - 1шт. Крем разогревающий </p>
            <p>   - 1шт. Крем успокаивающий от ушибов и ссадин </p>
            <p>   - 1шт. Лосьон для тела</p>
            <p>   - 1шт. Косметический бальзам Qamqor </p>
            <p>   - 1шт. Эко поглотитель</p> 
            <p>   - 1шт Антисептический гель + 2шт.</p>
            <p>   - Гель для душа + 1шт. </p>
            <p>   - крем от загара </p>
            <p>   - Масло для загара 1шт.</p>
            <p><b>• Вариант B</b></p>
            <p>- 12 упаковка мыло </p>
            <p>-1шт Антисептический гель.</p>
          </div>
        <br/>
        <br/>
      </div>

    </div>
  </div>  

  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingOne">
      <h4 class="panel-title">
        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="true" aria-controls="collapseThree">
          Вариант С
        </a>
      </h4>
    </div>
    <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
      <div class="panel-body">
        <p style="margin: 25px 0 50px 0; font-weight: bold; font-size: 16px; color: #3e3d3c;">
          -Вознаграждение в денежном выражении 45 000тенге*.
          + варианты выбора продукции, входящих в пакет «Основной».</p>
           <?php echo $__env->make('front.finish.plus.formC', compact($finished), \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>







          <p style="margin: 65px 0 25px 0; font-weight: bold; font-size: 16px; color: #3e3d3c;">Пинкод для регистрации в Накопительную программу (с номинальной стоимостью 15 000тг), </p>
          <p style="margin: 25px 0 25px 0; font-weight: bold; font-size: 16px; color: #3e3d3c;">2 (два) пинкода для регистрации в FAST программу (с общей номинальной стоимостью 120 000тг), и варианты выбора продукции, входящих в пакет «FAST»</p>

          <?php echo e(Form::open(['route' => 'pin.add', 'method'=>'post'])); ?>

            <?php echo csrf_field(); ?>
            <input type="text" name="triple" value="g" hidden="hidden">
            <input type="text" name="program_id" value="5" hidden="hidden">

            <button class="btn btn-lg btn-success" type="submit" onclick="return confirm('Вы уверены?')">
              <i class="fa fa-dot-circle-o"></i> Сгенерировать
            </button>  
          <?php echo e(Form::close()); ?>  




          <a href="#" style="margin: 65px 0 25px 0; display: inline-block; font-size: 16px; text-decoration: underline;" class="transfer-button">Временная передача пин-кодов </a>

      <div class="card transfer-form" id="">

      <?php echo e(Form::open(['route' => 'transfer.add', 'method'=>'post', 'files' => true, 'autocomplete' => 'off'])); ?>


      <div class="card-body">
        <div class="form-group row">            
          <?php echo e(Form::label('login', 'Логин:', array('class' => 'col-md-3 col-form-label'))); ?>

          <div class="col-md-9">
            <?php echo e(Form::text('login', Auth::user()->name,  array('class' => 'form-control', 'required'=>'required', 'readonly' => 'readonly') )); ?>

          </div>
        </div>        
        <div class="form-group row">            
          <?php echo e(Form::label('firstname', 'ФИО:', array('class' => 'col-md-3 col-form-label'))); ?>

          <div class="col-md-9">
            <?php echo e(Form::text('fullname', Auth::user()->familiya,  array('class' => 'form-control', 'required'=>'required') )); ?>

          </div>
        </div>           
        <div class="form-group row">            
          <?php echo e(Form::label('firstname', 'ФИО получателя:', array('class' => 'col-md-3 col-form-label'))); ?>

          <div class="col-md-9">
            <?php echo e(Form::text('fullname2', null,  array('class' => 'form-control', 'required'=>'required') )); ?>

          </div>
        </div>
        <div class="form-group row">
          <?php echo e(Form::label('image', 'Скан документа :', array('class' => 'col-md-3 col-form-label'))); ?>

          <div class="col-md-9">
           <?php echo e(Form::file('image[]', array('class' => 'form-control', 'required'=>'required', 'multiple'=>true, 'accept'=>".jpeg, .jpg, .png, .pdf") )); ?>

          </div>
        </div>
         <div class="form-group row" style="display: none;">
            <input type="text" hidden value="<?php echo e($finished->id); ?>" name="finished_id">
            <input type="text" hidden value="5" name="program_id">
            <input type="text" hidden value="3" name="variant">
            <?php echo e(Form::text('uid', Auth::user()->user_id,  array('class' => 'form-control', 'required'=>'required') )); ?>

        </div>
      </div>
        
      <div class="card-footer">
        <button class="btn btn-sm btn-primary" type="submit">
          <i class="fa fa-dot-circle-o"></i> Отправить
        </button>     
      </div>


      <?php echo e(Form::close()); ?>


</div>

          




      </div>
    </div>
  </div>

</div>






    </div>
    </div>

        </div>
<?php $__env->stopSection(); ?>


<?php $__env->startSection('scripts'); ?>

<script type="text/javascript">
  $('.transfer-button').on('click', function(e){
    e.preventDefault();
    $('.transfer-form').toggleClass('active');
  })
</script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('front.layout.main', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>