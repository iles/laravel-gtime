<?php $__env->startSection('content'); ?>

<div class="cabinet_main_container authorization">
  <div class="header">
    <div class="row">
      <div class="col-md-10">
        <h2 class="title">Завершение программы</h2>        
      </div>
      <div class="col-md-2">
        <a class="link" href="#" onclick="document.getElementById('logout-form').submit(); return false;">
          <i class="icon-logout"></i>выход
        </a>
        <form id="logout-form" action="<?php echo e(route('partner.logout')); ?>" method="POST" style="display: none;">
          <?php echo csrf_field(); ?>
        </form>
      </div>
    </div>

  </div>
	<div class="body">
		<div class="auth_cont" style="max-width: 100%;">



<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingOne">
      <h4 class="panel-title">
        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
          Получить вознаграждение (25 000 тг.)
        </a>
      </h4>
    </div>
    <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
      <div class="panel-body">
			<?php echo $__env->make('bonus-request.form_2', ['progs' => $progs], \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      </div>
    </div>
  </div>

  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingThree">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
          Генерация Пин-кода в Накопительную программу
        </a>
      </h4>
    </div>
    <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
      <div class="panel-body">
      	<?php echo e(Form::open(['route' => 'pin.add', 'method'=>'post'])); ?>

        <?php echo csrf_field(); ?>
          <input type="text" name="program" value="1">
      		<button class="btn btn-lg btn-success" type="submit" onclick="return confirm('Выу верены?')">
				<i class="fa fa-dot-circle-o"></i> Сгенерировать
			</button>  
      	<?php echo e(Form::close()); ?>

      </div>
    </div>
  </div>
</div>






		</div>
		</div>

				</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>


<?php $__env->stopSection(); ?>
<?php echo $__env->make('front.layout.main', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>