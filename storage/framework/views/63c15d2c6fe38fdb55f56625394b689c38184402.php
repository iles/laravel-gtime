<?php $__env->startSection('content'); ?>
<style type="text/css">
.timer div {
  display: inline-block;
  line-height: 1;
  padding: 20px;
  font-size: 40px;
}

.timer span {
  display: block;
  font-size: 20px;
  color: #000;
}

a.active{
	color: #ff0200;
	text-decoration: underline;
}

.timer #days {
  font-size: 100px;
  color: #db4844;
}
.timer #hours {
  font-size: 100px;
  color: #f07c22;
}
.timer #minutes {
  font-size: 100px;
  color: #f6da74;
}
.timer #seconds {
  font-size: 50px;
  color: #abcd58;
}

.tgbl{
	display: none;
}
.tgbl.active{
	display: block;
}
</style>
<div class="cabinet_main_container">
	<?php echo $__env->make('front.layout.menu', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
		<div class="body">
			<strong> </strong>
		<div class="profile_body_text">
		
		<?php if(Session::has('message')): ?>
                <div class="card-header" style="margin-bottom: 25px;">
            		<p class="alert <?php echo e(Session::get('alert-class', 'alert-info')); ?>"><?php echo e(Session::get('message')); ?></p>
                </div>
        <?php endif; ?>

        <a href="<?php echo e(route('partner.pin')); ?>" class="active">Мои пин коды</a>
        <a href="<?php echo e(route('partner.pin.requests')); ?>" style="margin-left: 40px;">Заявки</a>        
        <a href="<?php echo e(route('partner.pin.create')); ?>" style="margin-left: 40px;">Создать заявку</a>

        <div class="row" style="margin-top: 50px;">
        	<div class="col-md-12">
        		<?php
        			$pins = 0;
        		?>
		        <?php $__currentLoopData = $pin_requests; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $pr): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
			        


		        	<?php if( $pr->pins['total'] > 0 ): ?>
	        		<?php
	        			$pins ++;
	        		?>
		        	<p><small style="font-size: 65%;">Заявка № <?php echo e($pr->id); ?> от <?php echo e($pr->created_at); ?></small></p>
		        			<?php $__currentLoopData = $pr->pins; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $k => $prog): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>


		        				<?php if( is_array($prog) > 0 ): ?>
			        				<?php $__currentLoopData = $prog; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $p => $pin): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
			        				<div class="row">
			        					<div class="col-md-4">
			        						<b><?php echo e($pin['pin']); ?></b>
			        					</div>
				        				<div class="col-md-8">
											<?php if( $pin['status_id'] == 0 ): ?>
					                    		<pre>Действителен до: <br/><?php echo e($pin['expired_at']); ?></pre>
					                    	<?php elseif($pin['status_id'] == 3 ): ?>
					                    		<pre style="color: teal;">Активирован</pre>
					                    	<?php else: ?>
					                    		<pre style="color: red;">Заблокирован</pre>
					                    	<?php endif; ?>			        					
				        				</div>			        					
			        				</div>
		        					<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
		        				<?php endif; ?>
		        			<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
		        	<?php endif; ?>
		        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
		        <?php if($pins == 0): ?>
		        	Что бы получить пин код необходимо <a href="<?php echo e(route('partner.pin.create')); ?>"> оставить заявку</a>.
		        <?php endif; ?>      		
        	</div>
        </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('front.layout.main', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>