<?php if($grid->wantsPagination()): ?>
    <div class="pull-right">
        <?php echo e($grid->getData()->appends(request()->query())->links($grid->getGridPaginationView(), ['pjaxTarget' => $grid->getId()])); ?>

    </div>
<?php endif; ?>