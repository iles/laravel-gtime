<?php $__currentLoopData = $columns; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <?php if(!$row->filter): ?>
        <th></th>
    <?php elseif(!$row->filter->enabled): ?>
        <th></th>
    <?php else: ?>
        <th>
            <?php echo $row->filter; ?>

        </th>
    <?php endif; ?>
    <?php if($loop->last): ?>
        <th class="<?php echo e($grid->getGridFilterFieldColumnClass()); ?>">
            <div class="pull-right">
                <button type="submit"
                        class="btn btn-outline-primary grid-filter-button"
                        title="filter data"
                        form="<?php echo e($formId); ?>">Filter&nbsp;<i class="fa fa-filter"></i>
                </button>
            </div>
        </th>
    <?php endif; ?>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
