<!DOCTYPE html>

<html lang="en">
  <head>
    <base href="./">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <meta name="description" content="CoreUI - Open Source Bootstrap Admin Template">
    <meta name="author" content="Łukasz Holeczek">
    <meta name="keyword" content="Bootstrap,Admin,Template,Open,Source,jQuery,CSS,HTML,RWD,Dashboard">
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">
    <title>GTIME.KZ</title>
    <!-- Icons-->
    <link href="/css/coreui-icons.min.css" rel="stylesheet">
    <link href="/css/flag-icon.min.css" rel="stylesheet">
    <link href="/css/font-awesome.min.css" rel="stylesheet">
    <link href="/css/simple-line-icons.css" rel="stylesheet">
    <link href="/css/nprogress.css" rel="stylesheet">
    <link href="/css/daterangepicker.css" rel="stylesheet">
    <link href="/css/jquery.fancybox.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('vendor/leantony/grid/css/grid.css')); ?>" />
    <!-- Main styles for this application-->
    <link href="/css/style.css" rel="stylesheet">
    <link href="/css/pace.min.css" rel="stylesheet">
    <?php echo $__env->yieldContent('styles'); ?>
  </head>
  <?php
    $p = Session::get('programm', 1);
    $c = '';
    switch ($p) {
        case 1:
            $c = '#d4ffd4';
            break;
        case 2:
            $c = '#d4fffd';
            break;
        case 3:
            $c = '#ffebd4';
            break;
        case 4:
            $c = '#ccd4ff';
            break;
        case 5:
            $c = '#ccd4ff';
            break;
        case 6:
            $c = '#ffded9';
            break;

        default:
            # code...
            break;
    }
  ?>
  <body  class="app header-fixed aside-menu-fixed" style="background: <?php echo e($c); ?>;">
<style type="text/css">
	.laravel-grid .grid-wrapper { 
		max-height: 60vh;
		overflow-y: auto;
	} 
</style>
    <?php
     $role = Auth::guard('web')->user()->roles()->first()->name;
    ?>

    <?php if($role == 'admin'): ?>:
        <?php echo $__env->make('layouts.header', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php endif; ?>

    <?php if($role == 'superadmin'): ?>:
        <?php echo $__env->make('layouts.header_super', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php endif; ?>

    <?php if($role == 'kassa'): ?>:
        <?php echo $__env->make('layouts.header_kassa', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php endif; ?>

    <?php if($role == 'service'): ?>:
        <?php echo $__env->make('layouts.header_service', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php endif; ?>

    <?php if($role == 'warehouse_admin'): ?>:
        <?php echo $__env->make('layouts.header_warehouse', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php endif; ?>

    <?php if($role == 'security'): ?>:
        <?php echo $__env->make('layouts.header_security', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php endif; ?>


    <div class="app-body">
      <main class="main">
        <ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="/pins">Главная</a>
          </li>
          <?php echo $__env->yieldContent('breadcrumbs'); ?>
          <!-- Breadcrumb Menu-->
          <li class="breadcrumb-menu d-md-down-none">

            <div class="btn-group" role="group" aria-label="Button group">
              <a class="btn" onclick="document.getElementById('logout-form').submit();">
                <i class="icon-logout"></i>  Выход</a>
                  <form id="logout-form" action="<?php echo e(route('admin.logout')); ?>" method="POST" style="display: none;">
                  <?php echo csrf_field(); ?>
                </form>
            </div>
          </li>
        </ol>
        <?php echo $__env->make('leantony::modal.container', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>

        <?php echo $__env->yieldContent('content'); ?>
      </main>
</div>




    <!-- CoreUI and necessary plugins-->
    <script src="/js/jquery.min.js"></script>
    <script src="/js/moment.min.js"></script>
    <script src="/js/nprogress.min.js"></script>
    <script src="/js/popper.min.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/jquery.pjax.min.js"></script>
    <script src="/js/pace.min.js"></script>
    <script src="/js/daterangepicker.min.js"></script>
    <script src="/js/perfect-scrollbar.min.js"></script>
    <script src="/js/jquery.blockUI.js"></script>
    <script src="/js/jquery.fancybox.min.js"></script>
    <script src="/js/coreui.min.js"></script>
    <script src="<?php echo e(asset('vendor/leantony/grid/js/grid.js')); ?>"></script>
    <script>
        // send csrf token (see https://laravel.com/docs/5.6/csrf#csrf-x-csrf-token) - this is required
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        // for the progress bar (required for progress bar functionality)
        $(document).on('pjax:start', function () {
            //$.blockUI({ css: { backgroundColor: '#f00', color: '#fff'} });
            NProgress.start();
        });
        $(document).on('pjax:end', function () {
            NProgress.done();
            //$.unblockUI();

        });

            $('#mtype .dropdown-item').on('click', function (e) {
                e.preventDefault();
                $.blockUI({ css: { backgroundColor: '#f00', color: '#fff'} });
                $.post( "/change", { id:  $(this).data('id') } ).done( function(){
                    window.location.reload(false);
                });



            })



    </script>
    <!-- entry point for all scripts injected by the generated grids (required) -->
    <?php echo $__env->yieldContent('scripts'); ?>
    <?php echo $__env->yieldPushContent('grid_js'); ?>
  </body>
</html>
