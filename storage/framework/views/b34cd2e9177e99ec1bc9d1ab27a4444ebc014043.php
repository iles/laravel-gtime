<div class="row laravel-grid" id="<?php echo e($grid->getId()); ?>">
    <div class="col-md-12 col-xs-12 col-sm-12">
        <div class="card">
            <div class="card-header">
                <div class="pull-left">
                    <h4 class="grid-title"><?php echo e($grid->renderTitle()); ?></h4>
                </div>
                <?php echo $grid->renderPaginationInfoAtHeader(); ?>

            </div>
            <div class="card-body">
                <?php echo $__env->yieldContent('data'); ?>
            </div>
            <div class="card-footer">
                <?php echo $grid->renderPaginationInfoAtFooter(); ?>

                <?php echo $grid->renderPaginationLinksSection(); ?>

            </div>
        </div>
    </div>
</div>