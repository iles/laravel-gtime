<div class="card">
   <form method="post" action="<?php echo e(action('TransferController@aprove')); ?>" id="create_pin_form" class="form-horizontal" accept-charset="UTF-8"> 
  <div class="card-header">
    <strong><?php echo e($transfer->login); ?></strong></div>
    <div class="card-body">
        <?php echo csrf_field(); ?>

      
        <div class="form-group row">
          <label class="col-md-3 col-form-label" for="hf-password">Логин</label>
          <div class="col-md-9">           
            <input class="form-control" name="skype" value="<?php echo e($transfer->login); ?>" type="text">
            <input hidden name="id" value="<?php echo e($transfer->id); ?>" type="text">
          </div>
        </div>                  

        <div class="form-group row">
          <label class="col-md-3 col-form-label" for="hf-password">ФИО</label>
          <div class="col-md-9">           
            <input class="form-control" name="skype" value="<?php echo e($transfer->fullname); ?>" type="text">
          </div>
        </div>      

        <div class="form-group row">
          <label class="col-md-3 col-form-label" for="hf-password">ФИО получателя</label>
          <div class="col-md-9">           
            <input class="form-control" name="skype" value="<?php echo e($transfer->fullname2); ?>" type="text">
          </div>
        </div>            

     

        <div class="form-group row">
          <label class="col-md-3 col-form-label" for="hf-password">Документ</label>
          <div class="col-md-9">           
              <?php if($transfer->image != 0): ?>
                  <?php $__currentLoopData = explode(';', $transfer->image); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $image): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <a data-fancybox="gallery" href="/uploads/transfers/<?php echo e($image); ?>"><img src="/uploads/transfers/<?php echo e($image); ?>" style="width: 80px;"></a>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
              <?php endif; ?>
          </div>
        </div>      
        
    </div>
        
    <div class="card-footer">


      <button class="btn btn-sm btn-primary" type="submit">
      <i class="fa fa-dot-circle-o" type="submit"></i> Одобрить
      </button>
        <button class="btn btn-sm btn-danger" onclick="reject(); return false;">      
        <i class="fa fa-ban"></i> Отклонить</button>

    </div>

    </form>
</div>