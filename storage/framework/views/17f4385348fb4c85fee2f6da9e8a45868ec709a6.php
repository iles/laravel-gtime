<!DOCTYPE html>
<html lang="ru">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title></title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">

    <link rel="stylesheet" href="/front/css/bootstrap.min.css">
    <link href="/front/css/datepicker.min.css" rel="stylesheet" type="text/css"/>
    <link href="/front/css/styles.css" rel="stylesheet" type="text/css"/>
    <link href="https://fonts.googleapis.com/css?family=Ubuntu:300,400,400i,500,700,700i&amp;subset=cyrillic"
          rel="stylesheet">
    <script src="/front/js/jquery-3.3.1.min.js"></script>
    <link rel="shortcut icon" href="/images/favicon.png" type="image/png">

</head>

<?php echo $__env->yieldContent('head'); ?>
<body>
<div class="top_cont">
    <div class="container-fluid">
        <div class="flex_container">
            <div class="flex_item social_cont">
                <ul>
                    <li><a href="https://www.instagram.com/shungite_organic_line/" class="link" target="blank"><img
                                    src="/images/1.1.png" alt="" class="pic"></a></li>
                    <li><a href="https://www.youtube.com/channel/UCttYxkxbpxyGs4a3g0Lbdeg" class="link"
                           target="blank"><img src="/images/1.2.png" alt="" class="pic"></a></li>
                    <li>
                        <a href="https://www.facebook.com/shungite.beautyline.7/about?section=bio&info_surface=intro_card&pnref=about&lst=100023014900540%3A100023014900540%3A1510817312"
                           class="link" target="blank"><img src="/images/1.3.png" alt="" class="pic"></a>
                    </li>
                    <li><a href="https://vk.com/gtime_gtime" class="link" target="blank"><img
                                    src="/images/Без имени-3.png" alt="" class="pic"></a></li>
                </ul>
            </div>
            <div class="flex_item tel_cont">
                <ul>
                    <li><a href="tel:+77273115184" class="tel">+7 (727) 311-51-84</a></li>
                    <li><a href="tel:+77074656554" class="tel">+7 (707) 465-65-54</a></li>
                    <li>c 10:00 до 20:00</li>
                </ul>
            </div>
            <div class="flex_item logo_cont">
                <a href="/">
                    <img src="/images/3.png" alt="" class="pic">
                </a>
            </div>
            <div class="flex_item icons_cont">
                <div class="icon_cont lang_cont">
                    <a class="link collapsed" role="button" data-toggle="collapse" href="#lang_collapse"
                       aria-expanded="false" aria-controls="lang_collapse"><?php echo e(App::getLocale()); ?></a>
                    <div class="collapse" id="lang_collapse">
                        <div class="cont">

                            <ul>
                                <?php $__currentLoopData = config('app.locales'); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $local): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <?php if(!App::isLocale($key)): ?>
                                        <li>
                                            <a href="<?php echo e("/$key$page_url"); ?>" class="link"><?php echo e($key); ?></a>
                                        </li>
                                <?php endif; ?>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <!-- 									<li><a href="#" class="link">En</a></li>
									<li><a href="#" class="link">kz</a></li> -->
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="icon_cont cart_cont" style="min-width: 114px">









                </div>
                <div class="icon_cont enter_cont">
                    <a href="/" class="link">Бизнес</a>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="menu_cont">
    <div class="container-fluid">
        <div class="flex_container">
            <nav class="navbar navbar-default">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                            data-target="#bs-example-navbar-collapse-1" aria-expanded="false"><?php echo e(__('Меню')); ?></button>
                </div>
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li class="first"><a href="/"><?php echo e(__('главная')); ?></a></li>
                        <li><a href="http://gtime.kz/produkcziya/"><?php echo e(__('продукция')); ?></a></li>
                        <li><a href="http://gtime.kz/o-kompanii.html"><?php echo e(__('о компании')); ?></a></li>
                        <li><a href="http://gtime.kz/novosti/"><?php echo e(__('новости')); ?></a></li>
                        <li class=" dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"
                                                 role="button" aria-haspopup="true"
                                                 aria-expanded="false"><span><?php echo e(__('для партнеров')); ?></span></a>
                            <ul class="dropdown-menu">
                                <ul class="nav navbar-nav">
                                    <li class="first"><a
                                                href="http://gtime.kz/dlya-partnerov/pravila/"><?php echo e(__("orders")); ?></a></li>
                                    <li><a href="http://gtime.kz/dlya-partnerov/pravila.html"><?php echo e(__("rules")); ?></a></li>
                                    <li>
                                        <a href="http://gtime.kz/dlya-partnerov/rekvizityi.html"><?php echo e(__("requisites")); ?></a>
                                    </li>
                                    <li class="last"><a
                                                href="http://gtime.kz/dlya-partnerov/obuchenie.html"><?php echo e(__("educating")); ?> </a>
                                    </li>
                                </ul>
                            </ul>
                        </li>
                        <li><a href="http://gtime.kz/galereya/"><?php echo e(__('галерея')); ?></a></li>
                        <li><a href="http://gtime.kz/kontaktyi.html"><?php echo e(__('контакты')); ?></a></li>
                        <li class="last"><a href="http://gtime.kz/lideryi-kompanii/"><?php echo e(__('leaders')); ?></a></li>
                    </ul>
                </div>
            </nav>
            <div class="search_cont">
                <input type="text" placeholder="<?php echo e(__('поиск')); ?>" class="form-control">
                <input type="submit" value="" class="button">
            </div>
        </div>
    </div>
</div>
<div class="container-fluid">
    <ol class="breadcrumb">
        <li><a href="#"><?php echo e(__('главная')); ?></a></li>
        <li class="active"><a href="#"><?php echo e(__('для партнеров')); ?></a></li>
    </ol>

    <?php
        $m = Config::get('matrix.get');
        $p = Session::get('programm', 1);
    ?>

    <div class="row">
        <div class="col-xs-12 col-sm-4 col-lg-3 left_column">
            <div class="collapse_menu_cont left_menu">
                <button class="button collapsed" type="button" data-toggle="collapse" data-target="#collapse_menu"
                        aria-expanded="false" aria-controls="collapse_menu"><?php echo e(__('вход в программу')); ?></button>
                <div class="collapse in" id="collapse_menu">
                    <div class="collapse_text_cont">
                        <ul>
                            <?php $__currentLoopData = $m; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $matrix): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <?php if($key == $p): ?>
                                    <li><a href="#" data-id="<?php echo e($key); ?>" class="link active"><?php echo e(__($matrix[1])); ?></a>
                                    </li>
                                    <?php continue; ?>
                                <?php endif; ?>
                                <li><a href="#" data-id="<?php echo e($key); ?>" class="link"><?php echo e(__($matrix[1])); ?></a></li>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="counter_cont">
                <h3 class="title"><?php echo e(__('партнеров с нами')); ?>

                    <?php
                        $m = App\Models\User::getTotalCount();
                        $l = strlen($m);
                    ?>
                </h3>
                <div class="numbers_cont">
                    <?php for( $i=0; $i < 7; $i++): ?>
                        <div class="line"></div>
                        <div class="number"><?php echo e($m[$i]); ?></div>
                    <?php endfor; ?>
                    <div class="line"></div>
                </div>
            </div>
            <div class="left_col_text_cont">
                <h3 class="title" style="    margin-bottom: 15px; font-size: 1.125rem; text-transform: uppercase;"><?php echo e(__('news')); ?></h3>
                <div class="orders_page" style="margin-bottom: 0; padding: 0;">

                    <?= __('newsContent')?>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-8 col-lg-9 right_column">
            <?php echo $__env->yieldContent('content'); ?>
        </div>
    </div>
</div>
<div class="footer_cont">
    <div class="footer">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-12 col-lg-3">
                    <div class="menu_cont">
                        <nav class="navbar navbar-default">
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                                        data-target="#bs-example-navbar-collapse-2" aria-expanded="false">Меню
                                </button>
                            </div>
                            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
                                <ul class="nav navbar-nav">
                                    <li class="first"><a href="http://gtime.kz/"><?php echo e(__('главная')); ?></a></li>
                                    <li><a href="http://gtime.kz/produkcziya/"><?php echo e(__('продукция')); ?></a></li>
                                    <li><a href="http://gtime.kz/o-kompanii.html"><?php echo e(__('о компании')); ?></a></li>
                                    <li><a href="http://gtime.kz/novosti/"><?php echo e(__('новости')); ?></a></li>
                                    <li><a href="http://gtime.kz/dlya-partnerov/"><?php echo e(__('для партнеров')); ?></a></li>
                                    <li><a href="http://gtime.kz/galereya/"><?php echo e(__('галерея')); ?></a></li>
                                    <li><a href="http://gtime.kz/kontaktyi.html"><?php echo e(__('контакты')); ?></a></li>
                                    <li class="last"><a href="http://gtime.kz/lideryi-kompanii/"><?php echo e(__('leaders')); ?></a>
                                    </li>
                                </ul>
                            </div>
                        </nav>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-7 col-lg-6">
                    <div class="address_cont">
                        <div class="flex_container">
                            <div class="col-xs-12 col-sm-6 flex_item">
                                <div class="address"><?=__('Республика Казахстан,<br>г. Алматы, ул. Достык 2<br>(Квартал), 2-этаж, Блок С')  ?>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6 flex_item">
                                <div class="tel_cont">
                                    <ul>
                                        <li><a href="tel:+7 (727) 311-51-84 " class="tel">+7 (727) 311-51-84 </a>
                                        </li>
                                        <li><a href="tel:+7 (707) 465 65 54 " class="tel">+7 (707) 465 65 54 </a>
                                        </li>
                                        <li><a href="tel:С 10:00 до 20:00" class="tel">С 10:00 до 20:00</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6 flex_item">
                                <div class="link_cont">
                                    <div class="name">g-time travel</div>
                                    <a href="http://www.gtimetravel.kz" class="link">www.gtimetravel.kz</a>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6 flex_item">
                                <div class="social_cont">
                                    <ul>
                                        <li><a href="https://www.instagram.com/shungite_organic_line/" class="link"
                                               target="blank"><img src="/images/9.1.png" alt=""
                                                                   class="pic"></a></li>
                                        <li><a href="https://www.youtube.com/channel/UCttYxkxbpxyGs4a3g0Lbdeg"
                                               class="link" target="blank"><img src="/images/9.2.png"
                                                                                alt="" class="pic"></a></li>
                                        <li>
                                            <a href="https://www.facebook.com/shungite.beautyline.7/about?section=bio&info_surface=intro_card&pnref=about&lst=100023014900540%3A100023014900540%3A1510817312"
                                               class="link" target="blank"><img src="/images/9.3.png"
                                                                                alt="" class="pic"></a></li>
                                        <li><a href="https://vk.com/gtime_gtime" class="link" target="blank"><img
                                                        src="/images/Vk.png" alt="" class="pic"></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-5 col-lg-3">
                    <div class="kurs_cont">


                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-12 col-sm-6 copyright">© 2012-<?php echo e(date('Y')); ?> g-time corporation</div>
            </div>
        </div>
    </div>
</div>


<script src="/front/js/jquery.easing.min.js"></script>
<script src="/front/js/bootstrap.min.js"></script>
<script src="/front/js/inputmask/inputmask.min.js"></script>
<script src="/front/js/inputmask/inputmask.extensions.min.js"></script>
<script src="/front/js/inputmask/jquery.inputmask.min.js"></script>
<script src="/front/js/datepicker.min.js"></script>
<script src="/front/js/main.js"></script>

<script type="text/javascript">
    $(document).ready(function () {
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        $('#collapse_menu .link').on('click', function (e) {
            e.preventDefault();
            $.post("/change", {_token: CSRF_TOKEN, id: $(this).data('id')}).done(function () {
                window.location.reload('/login');
            });
        })


        $("#notorios").click(function () {
            if ($(this).is(':checked')) {
                $('.not-row').show();
            } else {
                $('.not-row').hide();
            }
        });
    })
</script>

<script>
    $(function () {
        $("input[type = 'submit']").click(function () {
            var $fileUpload = $("input[type='file']");
            if (parseInt($fileUpload.get(0).files.length) > 10) {
                alert("You are only allowed to upload a maximum of 10 files");
            }
        });
    });
</script>
</body>


<?php echo $__env->yieldContent('scripts'); ?>
</html>