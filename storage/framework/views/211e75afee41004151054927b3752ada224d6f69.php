<?php $__env->startSection('content'); ?>
<style type="text/css">
.timer div {
  display: inline-block;
  line-height: 1;
  padding: 20px;
  font-size: 40px;
}

.timer span {
  display: block;
  font-size: 20px;
  color: #000;
}

a.active{
	color: #ff0200;
	text-decoration: underline;
}

.timer #days {
  font-size: 100px;
  color: #db4844;
}
.timer #hours {
  font-size: 100px;
  color: #f07c22;
}
.timer #minutes {
  font-size: 100px;
  color: #f6da74;
}
.timer #seconds {
  font-size: 50px;
  color: #abcd58;
}

.tgbl{
	display: none;
}
.tgbl.active{
	display: block;
}
</style>
<div class="cabinet_main_container">
	<?php echo $__env->make('front.layout.menu', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
		<div class="body">
			<strong> </strong>
		<div class="profile_body_text">
		
		<?php if(Session::has('message')): ?>
                <div class="card-header" style="margin-bottom: 25px;">
            		<p class="alert <?php echo e(Session::get('alert-class', 'alert-info')); ?>"><?php echo e(Session::get('message')); ?></p>
                </div>
        <?php endif; ?>

        <a href="<?php echo e(route('partner.pin')); ?>" >Мои пин коды</a>
        <a href="<?php echo e(route('partner.pin.requests')); ?>" class="active" style="margin-left: 40px;">Заявки</a>        
        <a href="<?php echo e(route('partner.pin.create')); ?>" style="margin-left: 40px;">Создать заявку</a>

        <div class="row" style="margin: 50px 0 0px 0;">
        	<?php if(count($requests) > 0): ?>
		        <?php $__currentLoopData = $requests; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $request): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
			        <div class="row" style="margin-bottom: 25px;">	        	
			        	<div class="col-md-1"><?php echo e($key + 1); ?></div>
				        <div class="col-md-11">
							Ваша заявка под номером <span style="color: teal"> <?php echo e($request->id); ?></span> от <span><small style="color: #ff7400;"><?php echo e($request->created_at); ?></small></span> 
							  <?php if($request->status_id == 0): ?>
								<span class="pin-info"> находится в режиме ожидания</span>
							<?php elseif($request->status_id == 1): ?>
								<span class="pin-info" style="color: teal">одобрена</span>					
							<?php elseif($request->status_id == 2): ?>
								<span class="pin-info" style="color: red">отклонена</span> <br/><br>
								<pre><?php echo e($request->cancel_reason); ?></pre>
							<?php endif; ?>
						</div>
			        </div>
		        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
	        <?php else: ?>
	        <p>У вас нет заявок. <a href="<?php echo e(route('partner.pin.create')); ?>">Создать заявку</a></p>
	        <?php endif; ?>
        </div>


<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
<script type="text/javascript">

<?php if(count($requests) > 0): ?>

$(document).ready(function(){

 isActive = true;

$().ready(function () {
    //EITHER USE A GLOBAL VAR OR PLACE VAR IN HIDDEN FIELD
    //IF FOR WHATEVER REASON YOU WANT TO STOP POLLING
    pollServer();
});

function pollServer()
{
    if (isActive)
    {
        window.setTimeout(function () {
            $.ajax({
                url: "/check/status",
                data: {id: <?php echo e($requests->first()->id); ?>, program: <?php echo e($p); ?>, status_id: <?php echo e($requests->first()->status_id); ?>, _token: "<?php echo e(csrf_token()); ?>" },
                type: "POST",
                success: function (result) {
                    if(result.change){
                    	var text;
                    	if(result.status == 2){
                    		text = 'отклонена!';
                    	} else {
                    		text = 'одобрена!';                    		
                    	}
                    	alert('Ваша заявка ' + text);
                      window.location.replace("/partner/pin");
                    }
                    pollServer();
                },
                error: function () {
                    //ERROR HANDLING
                    pollServer();
                }});
        }, 4500);
    }
}


$( ".modal" ).on('shown', function(){
    alert("I want this to appear after the modal has opened!");
});


});

<?php endif; ?>


</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('front.layout.main', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>