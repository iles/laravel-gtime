<div class="card">
   <form method="post" action="<?php echo e(action('PaymentController@statusupdate')); ?>" id="create_pin_form" class="form-horizontal" accept-charset="UTF-8">
  <div class="card-header">
    <strong>Разделение лестницы #<?php echo e($split->id); ?></strong>
  </div>

  <div class="card-body">
    <div class="row">
      <div class="col-md-11 col-md-offset-1" style="margin-bottom: 12px;">
          <div class="row rr">
            <h4>Основная лестница</h4>
          </div>
      <?php $__currentLoopData = json_decode($split->main_matrix); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $level => $level_users): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
          <div class="row rr">
            <?php $__currentLoopData = $level_users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
             <div class="node card">
                <p><?php echo e($user->uname); ?>

                    <?php for($i = 0; $i < $user->stars; $i++): ?>
                        <i class="fa fa-star"> </i>
                    <?php endfor; ?>
                 </p>
                <p><?php echo e($user->by_refer); ?> </p>
            </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
          </div>
      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
      </div>
      <div class="col-md-6">
          <div class="row rr">
            <h4>Первая лестница</h4>
          </div>
      <?php $__currentLoopData = json_decode($split->first_matrix); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $level => $level_users): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
          <div class="row rr">
            <?php $__currentLoopData = $level_users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
             <div class="node card">
                <p><?php echo e($user->uname); ?>

                    <?php for($i = 0; $i < $user->stars; $i++): ?>
                        <i class="fa fa-star"> </i>
                    <?php endfor; ?>
                 </p>
                <p><?php echo e($user->by_refer); ?> </p>
            </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
          </div>
      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
      </div>
      <div class="col-md-6">
          <div class="row rr">
            <h4>Вторая лестница</h4>
          </div>
      <?php $__currentLoopData = json_decode($split->second_matrix); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $level => $level_users): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
          <div class="row rr">
            <?php $__currentLoopData = $level_users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
             <div class="node card">
                <p><?php echo e($user->uname); ?>

                    <?php for($i = 0; $i < $user->stars; $i++): ?>
                        <i class="fa fa-star"> </i>
                    <?php endfor; ?>
                 </p>
                <p><?php echo e($user->by_refer); ?> </p>
            </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
          </div>
      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
      </div>
    </div>

    </div>
  </div>

  <?php if(auth()->check() && auth()->user()->hasRole('superadmin')): ?>

  <div class="card-footer">
      <button class="btn btn-sm btn-danger" data-id="<?php echo e($split->id); ?>" onclick="return confirmAction(this);">
      <i class="fa fa-ban"></i> Отменить Разделение</button>
  </div>

  <?php endif; ?>
    </form>
</div>