<?php $__env->startSection('content'); ?>

<style type="text/css">
  .transfer-form{
    display: none;
  }  

  .transfer-form.active{
    display: block;
  }
</style>

<?php
/*
 if($finished->pin){
                $p = json_decode($finished->pin, true);
                $pin = \App\Models\Pins\StartPin::where('id', $p['pin'])->first();
            }   else { $pin = null; }  
*/
?>

<div class="cabinet_main_container">
		<?php echo $__env->make('front.layout.menu', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>

	<div class="body">
		<div class="auth_cont" style="max-width: 100%;">



<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingOne">
      <h4 class="panel-title">
        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
          Отправить заявку на получение вознаграждения
        </a>
      </h4>
    </div>
    <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
      <div class="panel-body">

        <?php if($bonus): ?>
          <div class="row" style="padding: 40px 0 0 10px;">
            <?php if($bonus->status_id == 0): ?>
            Ваша заявка находится на рассмотрении
            <?php elseif($bonus->status_id == 1): ?>
            Ваша заявка одобрена <br/>
            <br/>

            <?php elseif($bonus->status_id == 2): ?>
            Ваша заявка отклонена <br/>
            <br/>
            <pre>
              <?php echo e($bonus->cancel_reason); ?>

            </pre>
            <form action="/rebonus" method="POST">
              <?php echo csrf_field(); ?>
              <input type="text" hidden  value="<?php echo e($bonus->id); ?>" name="id">
              <button type="submit" href="#" class="btn btn-primary">Создать новую заявку</button>
            </form>
            <?php endif; ?>

          </div>
        <?php else: ?>


        <p style="margin: 25px 0 50px 0; font-weight: bold; font-size: 16px; color: #3e3d3c;">Поздравляем! <br/>
          Вы завершили программу «Основная старт» для получения вознаграждения заполните форму ниже</p>
          <?php echo $__env->make('front.finish.main.form_start', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
          <br>
          <br>



      <?php endif; ?>
      <br><br>
            <h4 class="panel-title">
            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
              Генерация Пин-кода в Вип программу
            </a>
          </h4>
          
          <div class="panel-body">





<?php if(!$pin): ?>



          <?php if($transfer): ?>
          <br>
          <br>
          <br>
            <?php if($transfer->status_id == 0): ?>
              Ваша заявка находится на рассмотрении
            <?php elseif($transfer->status_id == 2): ?>
              Ваша заявка отклонена
            <?php else: ?>
              Ваша заявка одобрена
              <br/>


            <?php echo e(Form::open(['route' => 'pin.add', 'method'=>'post'])); ?>

            <?php echo csrf_field(); ?>
              <input type="text" name="program_id" value="6" hidden="hidden">
              <input type="text" name="program_type" value="3" hidden="hidden">
              <button class="btn btn-lg btn-success" type="submit" onclick="return confirm('Выу верены?')">
                <i class="fa fa-dot-circle-o"></i> Сгенерировать
              </button>  
            <?php echo e(Form::close()); ?>

            <?php endif; ?>
          <?php else: ?>



            <?php echo e(Form::open(['route' => 'pin.add', 'method'=>'post'])); ?>

            <?php echo csrf_field(); ?>
              <input type="text" name="program_id" value="6" hidden="hidden">
              <input type="text" name="program_type" value="3" hidden="hidden">
              <button class="btn btn-lg btn-success" type="submit" onclick="return confirm('Выу верены?')">
                <i class="fa fa-dot-circle-o"></i> Сгенерировать
              </button>  
            <?php echo e(Form::close()); ?>          <?php endif; ?>   


          <?php if($transfer): ?>
          
          <?php else: ?> 



         <a href="#" style="margin: 65px 0 25px 0; display: inline-block; font-size: 16px; text-decoration: underline;" class="transfer-button">Временная передача пин-кодов </a>

      <div class="card transfer-form" id="">

      <?php echo e(Form::open(['route' => 'transfer.add', 'method'=>'post', 'files' => true, 'autocomplete' => 'off'])); ?>


      <div class="card-body">
        <div class="form-group row">            
          <?php echo e(Form::label('login', 'Логин:', array('class' => 'col-md-3 col-form-label'))); ?>

          <div class="col-md-9">
            <?php echo e(Form::text('login', Auth::user()->name,  array('class' => 'form-control', 'required'=>'required', 'readonly' => 'readonly') )); ?>

          </div>
        </div>        
        <div class="form-group row">            
          <?php echo e(Form::label('firstname', 'ФИО:', array('class' => 'col-md-3 col-form-label'))); ?>

          <div class="col-md-9">
            <?php echo e(Form::text('fullname', Auth::user()->familiya,  array('class' => 'form-control', 'required'=>'required') )); ?>

          </div>
        </div>           
        <div class="form-group row">            
          <?php echo e(Form::label('firstname', 'ФИО получателя:', array('class' => 'col-md-3 col-form-label'))); ?>

          <div class="col-md-9">
            <?php echo e(Form::text('fullname2', null,  array('class' => 'form-control', 'required'=>'required') )); ?>

          </div>
        </div>
        <div class="form-group row">
          <?php echo e(Form::label('image', 'Скан документа :', array('class' => 'col-md-3 col-form-label'))); ?>

          <div class="col-md-9">
           <?php echo e(Form::file('image[]', array('class' => 'form-control', 'required'=>'required', 'multiple'=>true, 'accept'=>".jpeg, .jpg, .png, .pdf") )); ?>

          </div>
        </div>

      </div>
        
      <div class="card-footer">
        <button class="btn btn-sm btn-primary" type="submit">
          <i class="fa fa-dot-circle-o"></i> Отправить
        </button>     
      </div>



      <?php echo e(Form::close()); ?>


      <?php endif; ?>










<?php else: ?>


      <?php if(!$pin): ?>
            <?php echo e(Form::open(['route' => 'pin.add', 'method'=>'post'])); ?>

            <?php echo csrf_field(); ?>
              <input type="text" name="program_id" value="6" hidden="hidden">
              <input type="text" name="program_type" value="3" hidden="hidden">
              <button class="btn btn-lg btn-success" type="submit" onclick="return confirm('Выу верены?')">
                <i class="fa fa-dot-circle-o"></i> Сгенерировать
              </button>  
            <?php echo e(Form::close()); ?>

      <?php else: ?>
                  <div class="row">
                    <div class="col-md-4"><b><?php echo e($pin->pin); ?></b></div>
                    <div class="col-md-6">
                      <?php if( $pin->status_id == 0 ): ?>
                      <pre>Действителен до: <br/><?php echo e($pin->expired_at); ?></pre>
                      <?php else: ?>
                      <pre style="color: red;">Заблокирован</pre>
                      <?php endif; ?>
                    </div>
                  </div>
       <?php endif; ?>
      </div>

<?php endif; ?>




























     

    </div>
  </div>

  </div>
</div>






    </div>
    </div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>

<script type="text/javascript">
  $('.transfer-button').on('click', function(e){
    e.preventDefault();
    $('.transfer-form').toggleClass('active');
  })
</script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('front.layout.main', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>