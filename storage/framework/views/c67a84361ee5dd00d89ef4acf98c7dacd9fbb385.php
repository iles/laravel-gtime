
<style type="text/css">
  #bootstrap_modal .modal-dialog{
    max-width: 800px;
  }
</style>

<?php
  $m = Config::get('matrix.get');
  $p = Session::get('programm', 1);
?>



<div class="card">

   <form method="post" action="<?php echo e(action('SwapController@make')); ?>" id="swap_form" class="form-horizontal" accept-charset="UTF-8"> 


    <div class="card-body">
        <?php echo csrf_field(); ?>



    

        <div class="form-group row">            
          <?php echo e(Form::label('login', 'Логин', array('class' => 'col-md-3 col-form-label'))); ?>

          <div class="col-md-9">
            <?php echo e(Form::text('login', $swap->login,  array('class' => 'form-control', 'id' => 'login-input-1') )); ?>

            <span class="valid-feedback" role="alert"><strong id="message-input-12"></strong></span>
          </div>
        </div>      

        <div class="form-group row">            
          <?php echo e(Form::label('login', 'Заменить на', array('class' => 'col-md-3 col-form-label'))); ?>

          <div class="col-md-7">
            <div class="row">
              <div class=col-md-4>
                Логин:<br/>
                <br>
                ФИО:<br/>                
                <br>
                Телефон:<br/>                
                <br>
                Страна/Город:<br/>
              </div>
              <div class="col-md-6">
                <?php echo e($swap->login2); ?>        <br/>     
                <br>   
                <?php echo e($user->familiya); ?>      <br/>                        
                <br>   
                <?php echo e($user->pol); ?>      <br/>                          
                <br>   
                <?php echo e($user->imya); ?>      <br/>          
              </div>
            </div>
          </div>
        </div>          


    

      

        <input type="text" name="id" value="<?php echo e($swap->id); ?>" hidden/>     
        <input type="text" name="status" id="status_id" value="1" hidden/>
        <textarea name="cancel_reason"  class="form-control" id="cancel_reason"><?php echo e($swap->cancel_reason); ?></textarea>        
        <div class="invalid-feedback">Укажите причину отказа</div>

       
    </div>
        
    <div class="card-footer">


      <?php if($swap->type == 2): ?>
        <?php if($p == 3 || $p == 6): ?>
          <?php if(!$swap->sec_check): ?>
            Заявка не одобрена сб
          <?php else: ?>
                <button class="btn btn-sm btn-primary" type="submit">
            <i class="fa fa-dot-circle-o" type="submit"></i> Одобрить</button>
          <?php endif; ?>
        <?php else: ?>
              <button class="btn btn-sm btn-primary" type="submit">
        <i class="fa fa-dot-circle-o" type="submit"></i> Одобрить</button>
        <?php endif; ?>
      <?php else: ?>  
            <button class="btn btn-sm btn-primary" type="submit">
      <i class="fa fa-dot-circle-o" type="submit"></i> Одобрить</button>
      <?php endif; ?>
      <button class="btn btn-sm btn-danger" onclick="reject(); return false;">
      <i class="fa fa-ban"></i> Отклонить </button>
    </div>


    </form>
   <?php echo e(Form::close()); ?>


</div>

  


