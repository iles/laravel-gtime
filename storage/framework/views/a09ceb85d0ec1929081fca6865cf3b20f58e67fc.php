<?php $__env->startSection('content'); ?>

<div class="cabinet_main_container authorization">
	<div class="header">
	    <div class="row">
	      <div class="col-md-10">
			    <h2 class="title">Завершение программы</h2>        
	      </div>
	      <div class="col-md-2">
	        <a class="link" href="#" onclick="document.getElementById('logout-form').submit(); return false;">
	          <i class="icon-logout"></i>выход
	        </a>
	        <form id="logout-form" action="<?php echo e(route('partner.logout')); ?>" method="POST" style="display: none;">
	          <?php echo csrf_field(); ?>
	        </form>
	      </div>
	    </div>
	</div>
	<div class="body">
		<div class="auth_cont" style="max-width: 100%;">

			<?php if($bonus->status_id == 5): ?>
			   <p class="pin-info"> Ваша заявка под номером <span> <?php echo e($bonus->id); ?></span> от <span><?php echo e($bonus->created_at); ?></span> одобренна</p>
			   <p>Вознаграждение выплачено</p>
		      <?php elseif($bonus->status_id == 0 || $bonus->status_id == 1): ?>
		      	<p class="pin-info"> Ваша заявка под номером <span> <?php echo e($bonus->id); ?></span> от <span><?php echo e($bonus->created_at); ?></span> находится в режиме ожидания</p>
   
		      <?php elseif($bonus->status_id == 2 || $bonus->status_id == 6): ?>
		      	<p class="pin-info"> Ваша заявка под номером <span> <?php echo e($bonus->id); ?></span> от <span><?php echo e($bonus->created_at); ?></span> отклонена</p>
		      	<p>
		      		<pre><?php echo e($bonus->cancel_reason); ?></pre>
		      	</p>
					<form method="post" action="<?php echo e(action('FrontController@rebonus')); ?>" id="update_bonus_form" class="form-horizontal" accept-charset="UTF-8">
						<?php echo csrf_field(); ?>
						<input hidden="hidden" name="bonus_id" value="<?php echo e($bonus->id); ?>">
						<p style="margin-top: 35px;"><button  type="submit" class="btn dtn-success"> Отравить повторно </button></p>	
					</form>
		      <?php endif; ?>
		</div>
	</div>

</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>


<?php $__env->stopSection(); ?>
<?php echo $__env->make('front.layout.main', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>