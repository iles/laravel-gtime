<?php $__env->startSection('data'); ?>
    <div class="row">
        <?php if($grid->shouldRenderSearchForm()): ?>
            <?php echo $grid->renderSearchForm(); ?>

        <?php endif; ?>

        <?php if($grid->hasButtons('toolbar')): ?>
            <div class="col-md-<?php echo e($grid->getGridToolbarSize()[1]); ?>">
                <div class="pull-right">
                    <?php $__currentLoopData = $grid->getButtons('toolbar'); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $button): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php echo $button->render(); ?>

                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </div>
            </div>
        <?php endif; ?>

    </div>
    <form action="<?php echo e($grid->getSearchUrl()); ?>" method="GET" id="<?php echo e($grid->getFilterFormId()); ?>"></form>
    <div class="table-responsive grid-wrapper">
        <table class="<?php echo e($grid->getClass()); ?>">
            <thead class="<?php echo e($grid->getHeaderClass()); ?>">
            <tr class="filter-header">
                <?php $__currentLoopData = $columns; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $column): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                    <?php if($loop->first): ?>

                        <?php if($column->isSortable): ?>
                            <th scope="col"
                                class="<?php echo e(is_callable($column->columnClass) ? call_user_func($column->columnClass) : $column->columnClass); ?>"
                                title="click to sort by <?php echo e($column->key); ?>">
                                <a data-trigger-pjax="1" class="data-sort"
                                   href="<?php echo e($grid->getSortUrl($column->key, $grid->getSelectedSortDirection())); ?>">
                                    <?php if($column->useRawHtmlForLabel): ?>
                                        <?php echo $column->name; ?>

                                    <?php else: ?>
                                        <?php echo e($column->name); ?>

                                    <?php endif; ?>
                                </a>
                            </th>
                        <?php else: ?>
                            <th class="<?php echo e(is_callable($column->columnClass) ? call_user_func($column->columnClass) : $column->columnClass); ?>">
                                <?php if($column->useRawHtmlForLabel): ?>
                                    <?php echo $column->name; ?>

                                <?php else: ?>
                                    <?php echo e($column->name); ?>

                                <?php endif; ?>
                            </th>
                        <?php endif; ?>
                    <?php else: ?>
                        <?php if($column->isSortable): ?>
                            <th scope="col" title="click to sort by <?php echo e($column->key); ?>"
                                class="<?php echo e(is_callable($column->columnClass) ? call_user_func($column->columnClass) : $column->columnClass); ?>">
                                <a data-trigger-pjax="1" class="data-sort"
                                   href="<?php echo e($grid->getSortUrl($column->key, $grid->getSelectedSortDirection())); ?>">
                                    <?php if($column->useRawHtmlForLabel): ?>
                                        <?php echo $column->name; ?>

                                    <?php else: ?>
                                        <?php echo e($column->name); ?>

                                    <?php endif; ?>
                                </a>
                            </th>
                        <?php else: ?>
                            <th scope="col"
                                class="<?php echo e(is_callable($column->columnClass) ? call_user_func($column->columnClass) : $column->columnClass); ?>">
                                <?php if($column->useRawHtmlForLabel): ?>
                                    <?php echo $column->name; ?>

                                <?php else: ?>
                                    <?php echo e($column->name); ?>

                                <?php endif; ?>
                            </th>
                        <?php endif; ?>
                    <?php endif; ?>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <th></th>
            </tr>
            <?php if($grid->shouldRenderGridFilters()): ?>
                <tr>
                    <?php echo $grid->renderGridFilters(); ?>

                </tr>
            <?php endif; ?>
            </thead>
            <tbody>
            <?php if($grid->hasItems()): ?>
                <?php if($grid->warnIfEmpty()): ?>
                    <div class="alert alert-warning" role="alert">
                        <strong><i class="fa fa-exclamation-triangle"></i>&nbsp;No data present!.</strong>
                    </div>
                <?php endif; ?>
            <?php else: ?>
                <?php $__currentLoopData = $grid->getData(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <?php if($grid->allowsLinkableRows()): ?>
                        <?php
                            $callback = call_user_func($grid->getLinkableCallback(), $grid->transformName(), $item);
                        ?>
                        <?php
                            $trClassCallback = call_user_func($grid->getRowCssStyle(), $grid->transformName(), $item);
                        ?>
                        <tr class="<?php echo e(trim("linkable " . $trClassCallback)); ?>" data-url="<?php echo e($callback); ?>">
                    <?php else: ?>
                        <?php
                            $trClassCallback = call_user_func($grid->getRowCssStyle(), $grid->transformName(), $item);
                        ?>
                        <tr class="<?php echo e($trClassCallback); ?>">
                            <?php endif; ?>
                            <?php $__currentLoopData = $columns; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $column): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <?php if(is_callable($column->data)): ?>
                                    <?php if($column->useRawFormat): ?>
                                        <td class="<?php echo e($column->rowClass); ?>">
                                            <?php echo call_user_func($column->data, $item, $column->key); ?>

                                        </td>
                                    <?php else: ?>
                                        <td class="<?php echo e($column->rowClass); ?>">
                                            <?php echo e(call_user_func($column->data , $item, $column->key)); ?>

                                        </td>
                                    <?php endif; ?>
                                <?php else: ?>
                                    <?php if($column->useRawFormat): ?>
                                        <td class="<?php echo e($column->rowClass); ?>">
                                            <?php echo $item->{$column->key}; ?>

                                        </td>
                                    <?php else: ?>
                                        <td class="<?php echo e($column->rowClass); ?>">
                                            <?php echo e($item->{$column->key}); ?>

                                        </td>
                                    <?php endif; ?>
                                <?php endif; ?>
                                <?php if($loop->last && $grid->hasButtons('rows')): ?>
                                    <td>
                                        <div class="pull-right">
                                            <?php $__currentLoopData = $grid->getButtons('rows'); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $button): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <?php if(call_user_func($button->renderIf, $grid->transformName(), $item)): ?>
                                                    <?php echo $button->render(['gridName' => $grid->transformName(), 'gridItem' => $item]); ?>

                                                <?php else: ?>
                                                    <?php continue; ?>
                                                <?php endif; ?>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </div>
                                    </td>
                                <?php endif; ?>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <?php if($grid->shouldShowFooter()): ?>
                            <tr class="<?php echo e($grid->getGridFooterClass()); ?>">
                                <?php $__currentLoopData = $columns; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $column): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <?php if($column->footer === null): ?>
                                        <td></td>
                                    <?php else: ?>
                                        <td>
                                            <b><?php echo e(call_user_func($column->footer)); ?></b>
                                        </td>
                                    <?php endif; ?>
                                    <?php if($loop->last): ?>
                                        <td></td>
                                    <?php endif; ?>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </tr>
                        <?php endif; ?>
                    <?php endif; ?>
            </tbody>
        </table>
    </div>
<?php $__env->stopSection(); ?>
<?php $__env->startPush('grid_js'); ?>
    <script>
      (function($) {
        var grid = "<?php echo e('#' . $grid->getId()); ?>";
        var filterForm = "<?php echo e('#' . $grid->getFilterFormId()); ?>";
        var searchForm = "<?php echo e('#' . $grid->getSearchFormId()); ?>";
        _grids.grid.init({
          id: grid,
          filterForm: filterForm,
          dateRangeSelector: '.date-range',
          searchForm: searchForm,
          pjax: {
            pjaxOptions: {
              scrollTo: false,
            },
            // what to do after a PJAX request. Js plugins have to be re-intialized
            afterPjax: function(e) {
              _grids.init();
            },
          },
        });
      })(jQuery);
    </script>
<?php $__env->stopPush(); ?>

<?php echo $__env->make($grid->getRenderingTemplateToUse(), \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>