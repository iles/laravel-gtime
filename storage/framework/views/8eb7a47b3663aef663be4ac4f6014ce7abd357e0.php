
<style type="text/css">
  #bootstrap_modal .modal-dialog{
    max-width: 800px;
  }
</style>

<?php
  $m = Config::get('matrix.get');
  $p = Session::get('programm', 1);
?>



<div class="card">

   <form method="post" action="<?php echo e(action('SwapController@make')); ?>" id="swap_form" class="form-horizontal" accept-charset="UTF-8"> 


    <div class="card-body">
        <?php echo csrf_field(); ?>



        <div class="form-group row">
          <label class="col-md-3 col-form-label">Программа</label>
          <div class="col-md-9">
            <select name="programm" class="form-control">
              <optgroup>
                      <?php $__currentLoopData = $m; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $matrix): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                             <option value="<?php echo e($key); ?>" <?php echo e($key == $swap->program_id ? 'selected' : ''); ?>><?php echo e($matrix[1]); ?></option>
                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
              </optgroup>
            </select>
          </div>
        </div>     
    

        <div class="form-group row">            
          <?php echo e(Form::label('login', 'Логин', array('class' => 'col-md-3 col-form-label'))); ?>

          <div class="col-md-7">
            <?php echo e(Form::text('login', $swap->login,  array('class' => 'form-control', 'id' => 'login-input-1') )); ?>

            <span class="valid-feedback" role="alert"><strong id="message-input-12"></strong></span>
          </div>
          
          <div class="col-md-2">           
            <button class="btn btn-sm btn-primary pull-right" onclick="check(1); return false;"> <i class="fa fa-plus"></i> Проверить</button>  
          </div>

        </div>      

        <div class="form-group row">            
          <?php echo e(Form::label('login', 'Заменить на', array('class' => 'col-md-3 col-form-label'))); ?>

          <div class="col-md-7">
            <?php echo e(Form::text('login', $swap->login2,  array('class' => 'form-control', 'id' => 'login-input-2') )); ?>

            <span class="valid-feedback" role="alert"><strong id="message-input-22"></strong></span>
          </div>
          <div class="col-md-2">           
            <button class="btn btn-sm btn-primary pull-right" onclick="check(2); return false;"> <i class="fa fa-plus"></i> Проверить</button>  
          </div>
        </div>          


        <div class="form-group row">
          <label class="col-md-3 col-form-label" for="hf-password">Фото квитанции</label>
          <div class="col-md-9">           
              <?php if($swap->image != 0): ?>
                  <?php $__currentLoopData = explode(';', $swap->image); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $image): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <a data-fancybox="gallery" href="/uploads/applications/<?php echo e($image); ?>"><img src="/uploads/applications/<?php echo e($image); ?>" style="width: 80px;"></a>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
              <?php endif; ?>
          </div>
        </div>      

      

        <input type="text" name="id" value="<?php echo e($swap->id); ?>" hidden/>     
        <input type="text" name="status" id="status_id" value="1" hidden/>
        <textarea name="cancel_reason"  class="form-control" id="cancel_reason"><?php echo e($swap->cancel_reason); ?></textarea>        
        <div class="invalid-feedback">Укажите причину отказа</div>

       
    </div>
        
    <div class="card-footer">


      <?php if($swap->type == 2): ?>
        <?php if($p == 3 || $p == 6): ?>
          <?php if(!$swap->sec_check): ?>
            Заявка не одобрена сб
          <?php else: ?>
                <button class="btn btn-sm btn-primary" type="submit">
            <i class="fa fa-dot-circle-o" type="submit"></i> Одобрить</button>
          <?php endif; ?>
        <?php else: ?>
              <button class="btn btn-sm btn-primary" type="submit">
        <i class="fa fa-dot-circle-o" type="submit"></i> Одобрить</button>
        <?php endif; ?>
      <?php else: ?>  
            <button class="btn btn-sm btn-primary" type="submit">
      <i class="fa fa-dot-circle-o" type="submit"></i> Одобрить</button>
      <?php endif; ?>
      <button class="btn btn-sm btn-danger" onclick="reject(); return false;">
      <i class="fa fa-ban"></i> Отклонить </button>
    </div>


    </form>
   <?php echo e(Form::close()); ?>


</div>

  


