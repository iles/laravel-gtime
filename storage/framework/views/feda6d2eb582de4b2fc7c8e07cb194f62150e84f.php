<?php $__env->startSection('content'); ?>
<style type="text/css">
.timer div {
  display: inline-block;
  line-height: 1;
  padding: 20px;
  font-size: 40px;
}

.timer span {
  display: block;
  font-size: 20px;
  color: #000;
}

.timer #days {
  font-size: 100px;
  color: #db4844;
}
.timer #hours {
  font-size: 100px;
  color: #f07c22;
}
.timer #minutes {
  font-size: 100px;
  color: #f6da74;
}
.timer #seconds {
  font-size: 50px;
  color: #abcd58;
}

.tgbl{
  display: none;
}
.tgbl.active{
  display: block;
}
</style>
<div class="cabinet_main_container authorization">
  <div class="header">
      <div class="row">
        <div class="col-md-10">
          <h2 class="title">Завершение программы</h2>        
        </div>
        <div class="col-md-2">
          <a class="link" href="#" onclick="document.getElementById('logout-form').submit(); return false;">
            <i class="icon-logout"></i>выход
          </a>
          <form id="logout-form" action="<?php echo e(route('partner.logout')); ?>" method="POST" style="display: none;">
            <?php echo csrf_field(); ?>
          </form>
        </div>
      </div>
  </div>
	<div class="body">
		<div class="auth_cont" style="max-width: 100%;">

      
      <?php if($pin): ?>

          <p class="pin-info"> Ваш пин-код</p>
          <p style="font-weight: bold; font-size: 28px;">
            <?php echo e($pin->pin); ?>

          </p>

      <?php if($pin->status_id==0): ?>
          <p class="pin-info"> Действителен до:</p>
          <p><pre><?php echo e($pin->expired_at); ?></pre></p>
      
      
        <div class="timer">
          <div id="days"></div>
          <div id="hours"></div>
          <div id="minutes"></div>
          <div id="seconds"></div>
        </div>
      <?php else: ?>
          <p class="pin-info"> Активирован</p>
      <?php endif; ?>

      <?php endif; ?>




		</div>
		</div>

				</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
<script type="text/javascript">


$(document).ready(function(){
  $('#form-click').on( 'click', function(e){
  e.preventDefault();
  $('.profile_table').toggleClass('active');
  })

  $(":input").inputmask();

  <?php if($pin): ?>

  function makeTimer() {

  //    var endTime = new Date("29 April 2018 9:56:00 GMT+01:00");  
    var endTime = new Date("<?php echo e($pin->expired_at); ?>");     
      endTime = (Date.parse(endTime) / 1000);

      var now = new Date();
      now = (Date.parse(now) / 1000);

      var timeLeft = endTime - now;

      var days = Math.floor(timeLeft / 86400); 
      var hours = Math.floor((timeLeft - (days * 86400)) / 3600);
      var minutes = Math.floor((timeLeft - (days * 86400) - (hours * 3600 )) / 60);
      var seconds = Math.floor((timeLeft - (days * 86400) - (hours * 3600) - (minutes * 60)));
  
      if (hours < "10") { hours = "0" + hours; }
      if (minutes < "10") { minutes = "0" + minutes; }
      if (seconds < "10") { seconds = "0" + seconds; }

      $("#days").html(days + "<span>Дня</span>");
      $("#hours").html(hours + "<span>Часов</span>");
      $("#minutes").html(minutes + "<span>Минут</span>");
      $("#seconds").html(seconds + "<span>Секунд</span>");    

  }

  setInterval(function() { makeTimer(); }, 1000);

  <?php endif; ?>

});




</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('front.layout.main', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>