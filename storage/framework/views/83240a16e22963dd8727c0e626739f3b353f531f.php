<?php $__env->startSection('content'); ?>

<?php
  $m = Config::get('matrix.get');
    $p = Session::get('programm', 1);
?>

<div class="cabinet_main_container authorization">
  <div class="header">
    <h2 class="title"><?php echo e($m[$p][1]); ?></h2>
  </div>
  <div class="body">
    <div class="auth_cont">

    <?php echo e(Form::open(array('route' => 'partner.loginPartner'))); ?>

        <p class="text">Please enter your login information to log in your personal account.</p>
        <?php if(Session::has('message')): ?>
          <p class="alert <?php echo e(Session::get('alert-class', 'alert-info')); ?>"><?php echo e(Session::get('message')); ?></p>
        <?php endif; ?>

        <?php if($errors->any()): ?>
            <div class="alert alert-danger">
                <ul>
                    <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <li><?php echo e($error); ?></li>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </ul>
            </div>
        <?php endif; ?>

        <?php echo e(Form::text('username', '', ['class'=>'form-control', 'placeholder'=>'Логин'])); ?>

        <?php echo e(Form::password('password', ['class'=>'form-control', 'placeholder'=>'Пароль'])); ?>


        <div class="link_cont"><a href="/partner/reset" class="link">восстановить пароль</a></div>
        <p class="error-message" style="color:red; font-weight: bold; display: none;">ываываываыва</p>
        <input type="submit" value="авторизоваться" class="button">
      <?php echo e(Form::close()); ?>

    </div>
    </div>

        </div>
<?php $__env->stopSection(); ?>


<?php $__env->startSection('scripts'); ?>
  <script type="text/javascript">
    $(document).ready(function(){
      $('.auth_cont form').on('submit', function(e){
        e.preventDefault();
        $('error-message').html('');
          $('.error-message').fadeOut();
        var name = $( "input[name='username']").val();
        var password = $( "input[name='password']").val();
        if(name == ''){
          $('.error-message').html('Введите логин');
          $('.error-message').fadeIn();
          return false;
        }        
        if(password == ''){
          $('.error-message').html('Введите пароль');
          $('.error-message').fadeIn();
          return false;
        }

        $.ajax({
          method: "POST",
          url: "/auth-validate",
          data:{
            '_token': '<?php echo e(csrf_token()); ?>',
            'name' : name,
            'password' : password,
          },
        }).done(function( msg ) {
          if(msg.error == 1){
            $('.error-message').html(msg.message);
            $('.error-message').fadeIn();
          }else{
              e.target.submit();
          }
        });

      })
    })
  </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('front.layout.main', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>