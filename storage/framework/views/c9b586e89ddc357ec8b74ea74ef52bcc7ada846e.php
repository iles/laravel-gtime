<?php $__env->startSection('content'); ?>
<style type="text/css">


a.active{
	color: #ff0200;
	text-decoration: underline;
}



.tgbl{
	display: none;
}
.tgbl.active{
	display: block;
}
</style>
<div class="cabinet_main_container">
	<?php echo $__env->make('front.layout.menu', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
		<div class="body">
			<strong> </strong>
		<div class="profile_body_text">
		
		<?php if(Session::has('message')): ?>
                <div class="card-header" style="margin-bottom: 25px;">
            		<p class="alert <?php echo e(Session::get('alert-class', 'alert-info')); ?>"><?php echo e(Session::get('message')); ?></p>
                </div>
        <?php endif; ?>

<div class="card">

  <?php if($bonus): ?>
    <div class="row" style="padding: 40px 0 0 10px;">
      <?php if($bonus->status_id == 0): ?>
      Ваша заявка находится на рассмотрении
      <?php elseif($bonus->status_id == 1): ?>
        Ваша заявка одобрена и находится на рассмотрении в бухгалтерии<br/>      
      <?php elseif($bonus->status_id == 4): ?>
        Ваша заявка одобрена       
      <?php elseif($bonus->status_id == 5): ?>
        Ваша заявка отклонена
        <br/>
        <form action="/partner/rebonusvip" method="POST">
          <?php echo csrf_field(); ?>
          <input type="text" hidden  value="<?php echo e($bonus->id); ?>" name="id">
          <button type="submit" href="#" class="btn btn-primary">Создать новую заявку</button>
        </form>  
  
      <?php elseif($bonus->status_id == 2): ?>
      Ваша заявка отклонена <br/>
      <br/>
      <pre>
        <?php echo e($bonus->cancel_reason); ?>

      </pre>
      <form action="/partner/rebonusvip" method="POST">
        <?php echo csrf_field(); ?>
        <input type="text" hidden  value="<?php echo e($bonus->id); ?>" name="id">
        <button type="submit" href="#" class="btn btn-primary">Создать новую заявку</button>
      </form>
      <?php endif; ?>
    </div>
  <?php else: ?>
<?php echo e(Form::open(['route' => 'vipbonus.add', 'method'=>'post', 'autocomplete' => 'off', 'files' => true])); ?>


    <div class="card-body">
        <?php echo csrf_field(); ?>

        <div class="form-group row">
          <?php echo e(Form::label('uname', 'Логин:', array('class' => 'col-md-3 col-form-label'))); ?>

          <div class="col-md-9">
            <?php echo e(Form::text('uname', Auth::user()->name,  array('class' => 'form-control', 'required'=>'required', 'readonly' => 'readonly') )); ?>

          </div>
        </div>

        <div class="form-group row hidden">            
          <?php echo e(Form::label('summ', 'Сумма:', array('class' => 'col-md-3 col-form-label'))); ?>

          <div class="col-md-9">
            <?php echo e(Form::number('summ', 25000,  array('class' => 'form-control', 'required'=>'required',  'readonly' => 'readonly') )); ?>

          </div>
        </div>

        <div class="form-group row">            
          <?php echo e(Form::label('firstname', 'Имя:', array('class' => 'col-md-3 col-form-label'))); ?>

          <div class="col-md-9">
            <?php echo e(Form::text('firstname', Auth::user()->firstname,  array('class' => 'form-control', 'required'=>'required') )); ?>

          </div>
        </div>        

        <div class="form-group row">            
          <?php echo e(Form::label('firstname', 'Фамилия:', array('class' => 'col-md-3 col-form-label'))); ?>

          <div class="col-md-9">
            <?php echo e(Form::text('lastname', Auth::user()->lastname,  array('class' => 'form-control', 'required'=>'required') )); ?>

          </div>
        </div>       

         <div class="form-group row">            
          <?php echo e(Form::label('patronic', 'Отчество:', array('class' => 'col-md-3 col-form-label'))); ?>

          <div class="col-md-9">
            <?php echo e(Form::text('patronic', Auth::user()->patronic,  array('class' => 'form-control', 'required'=>'required') )); ?>

          </div>
        </div>         

        <div class="form-group row">            
          <?php echo e(Form::label('iin', 'ИИН:', array('class' => 'col-md-3 col-form-label'))); ?>

          <div class="col-md-9">
            <?php echo e(Form::number('iin', Input::old('iin'),  array('class' => 'form-control', 'required'=>'required') )); ?>

          </div>
        </div>        

        <div class="form-group row">            
          <?php echo e(Form::label('bill_num', 'Номер счета :', array('class' => 'col-md-3 col-form-label'))); ?>

          <div class="col-md-9">
            <?php echo e(Form::text('bill_num', Input::old('bill_num'),  array('class' => 'form-control', 'required'=>'required') )); ?>

          </div>
        </div>     

        <div class="form-group row">            
          <?php echo e(Form::label('bank_name', 'Наименование банка :', array('class' => 'col-md-3 col-form-label'))); ?>

          <div class="col-md-9">
            <?php echo e(Form::text('bank_name', Input::old('bank_name'),  array('class' => 'form-control', 'required'=>'required') )); ?>

          </div>
        </div>        

        <div class="form-group row">            
          <?php echo e(Form::label('BIK', 'BIK :', array('class' => 'col-md-3 col-form-label'))); ?>

          <div class="col-md-9">
            <?php echo e(Form::text('BIK', Input::old('BIK'),  array('class' => 'form-control', 'required'=>'required') )); ?>

          </div>
        </div>


        <?php if( Auth::user()->nonResident() ): ?>
        <div class="form-group row">            
          <?php echo e(Form::label('card_number', 'Номер карточки :', array('class' => 'col-md-3 col-form-label'))); ?>

          <div class="col-md-9">
            <?php echo e(Form::number('card_number', Input::old('card_number'),  array('class' => 'form-control', 'required'=>'required') )); ?>

          </div>
        </div>        

        <div class="form-group row">            
          <?php echo e(Form::label('bank_account', 'Счет банка (Корреспондентский) :', array('class' => 'col-md-3 col-form-label'))); ?>

          <div class="col-md-9">
            <?php echo e(Form::text('bank_account', Input::old('bank_account'),  array('class' => 'form-control', 'required'=>'required') )); ?>

          </div>
        </div>        

        <div class="form-group row">            
          <?php echo e(Form::label('country', 'Страна / Город:', array('class' => 'col-md-3 col-form-label'))); ?>

          <div class="col-md-9">
            <?php echo e(Form::text('country', Input::old('country'),  array('class' => 'form-control', 'required'=>'required') )); ?>

          </div>
        </div>    

        <div class="form-group row">            
          <?php echo e(Form::label('adress', 'Адрес проживания:', array('class' => 'col-md-3 col-form-label'))); ?>

          <div class="col-md-9">
            <?php echo e(Form::text('adress', Input::old('adress'),  array('class' => 'form-control', 'required'=>'required') )); ?>

          </div>
        </div>        

        <div class="form-group row">            
          <?php echo e(Form::label('adress', 'Фактический адрес проживания:', array('class' => 'col-md-3 col-form-label'))); ?>

          <div class="col-md-9">
            <?php echo e(Form::text('fact_adress', Input::old('fact_adress'),  array('class' => 'form-control', 'required'=>'required') )); ?>

          </div>
        </div>
        <?php endif; ?> 

        <div class="form-group row" style="margin-top: 35px;">            
          <div class="col-md-3"></div>
          <div class="col-md-9" style="text-align: left;">
            <?php echo e(Form::label('pens', 'Пенсионер', array('class' => 'col-form-label'))); ?>

            <?php echo e(Form::checkbox('pens', '1')); ?>

            <?php echo e(Form::label('inv', 'Инвалидность', array('style'=>'margin-left: 30px;', 'class' => 'col-form-label'))); ?>

            <?php echo e(Form::checkbox('inv', '1')); ?>

          </div>
        </div>

        <div class="form-group row">  
          <div class="col-md-3"></div>          
          <div class="col-md-9" style="text-align: left;">
        
        <div class="row inv-row" style="margin: 20px 0;">
          <div class="col-md-5">
            <?php echo e(Form::label('inv_group', 'Группа инвалидности :', array('class' => 'col-form-label'))); ?> <br/> <br/>
            <?php echo e(Form::select('inv_group', array('1' => 'Первая группа', '2' => 'Вторая группа', '3' => 'Третья группа'), null, array('class' => 'form-control') )); ?>              
          </div>
          
          <div class="col-md-7">
            <div class="row">
                <div class="col-md-12">
                  <?php echo e(Form::label('image', 'Срок инвалидности:', array('class' => 'col-form-label'))); ?>    <br/> <br/>               
                </div>
                <div class="col-md-6 inv-time">
                  <?php echo e(Form::text('inv_start',  null, ['class'=>'form-control datepicker-here', 'placeholder'=>'С:', 'data-position' => 'left top', 'readonly'=>'readonly', 'required'=>'required'])); ?>                  
                </div>                
                <div class="col-md-6 inv-time">
                  <?php echo e(Form::text('inv_end',  null, ['class'=>'form-control datepicker-here', 'placeholder'=>'ПО:', 'data-position' => 'left top', 'readonly'=>'readonly', 'required'=>'required'])); ?>                  
                </div>
                <div class="col-md-12" style="margin-top: 15px;">
                  <?php echo e(Form::label('inv_timeless', 'Без срока переосвидетельствования', array('style'=>'margin-right: 10px;', 'class' => 'col-form-label'))); ?>

                  <?php echo e(Form::checkbox('inv_timeless', '1')); ?>                  
                </div>
              </div>
            </div>
        </div>

          </div>
        </div>

        <div class="form-group row">            
          <?php echo e(Form::label('notorios', 'По нотариальной доверенности', array('class' => 'col-md-3 col-form-label'))); ?>

          <div class="col-md-9" style="text-align: left;">
            <?php echo e(Form::checkbox('notorios', '1')); ?>  
          </div>
        </div>         

          
        <div class="form-group row not-row">  
          <div class="col-md-3"></div>          
          <div class="col-md-9" style="text-align: left;">
            <b>Укажите данные:</b>
            <br/>
            <br/>
            <?php echo e(Form::text('not_fio',  null, ['class'=>'form-control', 'placeholder'=>'ФИО:'])); ?><br/>
            <?php echo e(Form::text('not_adress',  null, ['class'=>'form-control', 'placeholder'=>'Адрес:'])); ?><br/>
            <?php echo e(Form::text('not_iin',  null, ['class'=>'form-control', 'placeholder'=>'ИИН:'])); ?><br/>
            <?php echo e(Form::text('not_passport',  null, ['class'=>'form-control', 'placeholder'=>'№ пасспорта:'])); ?><br/>

          </div>
        </div>


         
        <br/>
        <div class="form-group row">         
          <?php echo e(Form::label('image', 'Скан документа :', array('class' => 'col-md-3 col-form-label'))); ?>

          <div class="col-md-9">
          <?php echo e(Form::file('images[]', array('class' => 'form-control', 'required'=>'required', 'multiple'=>true, 'accept'=>".jpeg, .jpg, .png, .pdf") )); ?>

          </div>
        </div>          

         <div class="form-group row" style="display: none;">            
            <?php echo e(Form::text('uid', Auth::user()->user_id,  array('class' => 'form-control', 'required'=>'required') )); ?>

        </div>        

        <div class="form-group row">            
          <?php echo e(Form::label('vip_bonus_logins', 'Логины (через запятую)', array('class' => 'col-md-3 col-form-label'))); ?>

          <div class="col-md-9">
            <?php echo e(Form::text('vip_bonus_logins', Input::old('vip_bonus_logins'),  array('class' => 'form-control', 'required'=>'required') )); ?>

            <input type="tet" name="vip_bonus" hidden>
          </div>
        </div>        
     
     
     

        
    </div>
        
    <div class="card-footer">
      <button class="btn btn-sm btn-primary" type="submit">
        <i class="fa fa-dot-circle-o"></i> Сохранить
      </button>     

   </div>
   <?php echo e(Form::close()); ?>


<?php endif; ?>

</div>





		</div>
					</div>
				</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('front.layout.main', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>