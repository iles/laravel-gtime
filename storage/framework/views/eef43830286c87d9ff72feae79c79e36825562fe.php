        <?php
          $m = Config::get('matrix.get');
          $p = Session::get('programm', 1);
          if($p == null){
            Session::put('programm', 1);
            $p = Session::get('programm', 1);
          }
        ?>

<div class="card" style="width: 100%; margin: 0 auto;">

  <div class="card-header">
    <strong>Просмотр записи</strong>
  </div>


<form method="post" action="<?php echo e(action('WarehouseController@update_admin')); ?>" id="create_pin_form" class="form-horizontal" accept-charset="UTF-8">
    <div class="card-body">
        <?php echo csrf_field(); ?>

        <input class="form-control" name="user_id" type="text" hidden value="<?php echo e($model->user_id); ?>">
        <div class="form-group row">
          <label class="col-md-3 col-form-label" for="hf-password">Менеджер</label>
          <div class="col-md-9">
            <input class="form-control" name="id" hidden="" type="text" value="<?php echo e($model->id); ?>">
            <input class="form-control" name="name" type="text" value="<?php echo e($manager->name); ?>">
          </div>
        </div>

        <div class="form-group row">
          <label class="col-md-3 col-form-label" for="hf-password">Программа</label>
          <div class="col-md-9">
            <select name="program" id="grid-filter-program" form="" class="form-control grid-filter">
                                <option value="1" <?php if($model->program == 1): ?> selected="" <?php endif; ?>>Старт</option>
                                <option value="2" <?php if($model->program == 2): ?> selected="" <?php endif; ?>>Авто</option>
                                <option value="3" <?php if($model->program == 3): ?> selected="" <?php endif; ?>>Основная</option>
                                <option value="4" <?php if($model->program == 4): ?> selected="" <?php endif; ?>>Накопительная</option>
                                <option value="5" <?php if($model->program == 5): ?> selected="" <?php endif; ?>>Накопительная +</option>
                                <option value="6" <?php if($model->program == 6): ?> selected="" <?php endif; ?>>Vip</option>
                                <option value="7" <?php if($model->program == 7): ?> selected="" <?php endif; ?>>Fast</option>
            </select>
          </div>
        </div>

        <div class="form-group row">
          <label class="col-md-3 col-form-label" for="hf-password">Логин</label>
          <div class="col-md-9">
            <input class="form-control" name="uname" type="text" value="<?php echo e($model->uname); ?>">
          </div>
        </div>

        <div class="form-group row">
          <label class="col-md-3 col-form-label" for="hf-password">ФИО</label>
          <div class="col-md-9">
            <input class="form-control" name="user_fullname" type="text" value="<?php echo e($model->user_fullname); ?>">
          </div>
        </div>

        <div class="form-group row">
          <label class="col-md-3 col-form-label" for="hf-password">ИИН</label>
          <div class="col-md-9">
            <input class="form-control" name="iin" type="text" value="<?php echo e($model->iin); ?>">
          </div>
        </div>

        <div class="form-group row">
          <label class="col-md-3 col-form-label" for="hf-password">Дата покупки</label>
          <div class="col-md-9">
            <input class="form-control" name="purchace_date" type="text" value="<?php echo e($model->purchace_date); ?>">
          </div>
        </div>

        <div class="form-group row">
          <label class="col-md-3 col-form-label" for="hf-password">Имя</label>
          <div class="col-md-9">
            <input class="form-control" name="checkout_name" type="text" value="<?php echo e($model->checkout_name); ?>">
          </div>
        </div>

        <div class="form-group row">
          <label class="col-md-3 col-form-label" for="hf-password">Фамилия</label>
          <div class="col-md-9">
            <input class="form-control" name="checkout_surname" type="text" value="<?php echo e($model->checkout_surname); ?>">
          </div>
        </div>

        <div class="form-group row">
          <label class="col-md-3 col-form-label" for="hf-password">Отчество</label>
          <div class="col-md-9">
            <input class="form-control" name="checkout_patronic" type="text" value="<?php echo e($model->checkout_patronic); ?>">
          </div>
        </div>

        <div class="form-group row">
          <label class="col-md-3 col-form-label" for="hf-password">Когда забрал</label>
          <div class="col-md-9">
            <input class="form-control" name="checkout_date" type="text" value="<?php echo e($model->checkout_date); ?>">
          </div>
        </div>

         <div class="form-group row">
          <label class="col-md-3 col-form-label" for="hf-password">Набор</label>
          <div class="col-md-9">
            <input class="form-control" name="option" type="text" value="<?php echo e($model->option); ?>">
          </div>
        </div>








    </div>

    <div class="card-footer">
      <button class="btn btn-sm btn-primary" type="submit">
      <i class="fa fa-dot-circle-o" type="submit"></i> Сохранить</button>
      <button class="btn btn-sm btn-danger"  data-dismiss="modal" aria-label="Close" type="reset">
      <i class="fa fa-ban"></i> Закрыть</button>
    </div>
    </form>
</div>