<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccumulativePlusBonusRequestTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accumulative_plus_bonus_request', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('summ');
            $table->integer('uid');
            $table->integer('program_id')->default(5);
            $table->string('uname');
            $table->string('firstname');
            $table->string('lastname');
            $table->string('image')->nullable();
            $table->integer('secondBonus')->nullable();
            $table->integer('secondPin')->nullable();
            $table->string('patronic');
            $table->timestamp('fact_date')->nullable();
            $table->bigInteger('iin');
            $table->string('bill_num', 50);
            $table->string('product', 450)->nullable();
            $table->integer('status_id')->default(0);
            $table->integer('book_status_id')->default(0);
            $table->integer('card_number')->nullable();
            $table->string('bank_name')->nullable();
            $table->integer('BIK')->nullable();
            $table->string('bank_account')->nullable();
            $table->string('country')->nullable();
            $table->string('adress')->nullable();
            $table->text('cancel_reason')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accumulative_plus_bonus_request');
    }
}
