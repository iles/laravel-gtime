<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransfersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transfers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('program_id');
            $table->text('login');
            $table->text('fullname');
            $table->text('fullname2');
            $table->text('image');
            $table->integer('status_id')->default(0);
            $table->integer('finished_id')->nullable();
            $table->integer('bonus_id')->nullable();
            $table->integer('variant')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transfers');
    }
}
