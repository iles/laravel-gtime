<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccumulativeBonusRequestTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accumulative_bonus_request', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('summ');
            $table->integer('uid');
            $table->integer('program_id')->default(4);
            $table->string('uname');
            $table->string('firstname');
            $table->string('lastname');
            $table->string('patronic');
            $table->string('image')->nullable();
            $table->bigInteger('iin');
            $table->string('bill_num', 50);
            $table->timestamp('fact_date')->nullable();
            $table->integer('status_id')->default(0);
            $table->integer('book_status_id')->default(0);
            $table->integer('card_number')->nullable();
            $table->string('bank_name')->nullable();
            $table->integer('BIK')->nullable();
            $table->string('bank_account')->nullable();
            $table->string('country')->nullable();
            $table->string('adress')->nullable();
            $table->text('cancel_reason')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accumulative_bonus_request');
    }
}
