<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAutoPinsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('auto_pins', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('username')->nullable();
            $table->integer('request_id');
            $table->integer('request_program_id');
            $table->string('pin');
            $table->integer('program');
            $table->integer('status_id')->default(0);
            $table->integer('kassa')->nullable();
            $table->dateTime('expired_at')->nullable();
            $table->dateTime('active_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('auto_pins');
    }
}
