<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGainsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gains', function (Blueprint $table) {
            $table->increments('id');
            $table->date('date');
            $table->date('date_fact');
            $table->date('date_prod');
            $table->integer('program');
            $table->integer('sum');
            $table->string('login');
            $table->string('fullname');
            $table->string('iin');
            $table->string('city');
            $table->string('country');
            $table->string('status_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gains');
    }
}
