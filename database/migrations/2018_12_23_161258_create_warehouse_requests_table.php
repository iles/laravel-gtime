<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWarehouseRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('warehouse_requests', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('status_id')->default('0');
            $table->integer('manager_id')->nullable();
            $table->string('program');
            $table->string('uname');
            $table->string('user_fullname')->nullable();
            $table->string('iin')->nullable();
            $table->string('purchace_date');
            $table->string('checkout_name');
            $table->string('checkout_surname');
            $table->string('checkout_patronic');
            $table->string('checkout_date');
            $table->string('option');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('warehouse_requests');
    }
}
