<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FastRequestUsersNewPin extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('fast_request_users_new_pin', function (Blueprint $table) {//рисунок 3
            $table->increments('id');
            $table->integer('program_id')->default(1);
            $table->text('json')->nullable();
            $table->integer('user_id');
            $table->integer('status');
            $table->string('uname')->nullable();
            $table->string('iin')->nullable();
            $table->string('bill_number')->nullable();
            $table->string('summ')->nullable();
            $table->string('skype')->nullable();
            $table->string('image')->nullable();
            $table->text('cancel_reason')->nullable();
            $table->string('pin')->unique()->nullable();
            $table->datetime('expires_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fast_request_users_new_pin');
    }
}
