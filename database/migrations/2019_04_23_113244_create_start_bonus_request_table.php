<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStartBonusRequestTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('start_bonus_request', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('summ');
            $table->integer('view')->default(0);
            $table->integer('uid');
            $table->integer('program_id')->default(1);
            $table->string('uname');
            $table->string('fullname')->nullable();
            $table->string('firstname');
            $table->string('lastname');
            $table->string('patronic');
            $table->string('image')->nullable();
            $table->bigInteger('iin');
            $table->timestamp('fact_date')->nullable();
            $table->string('bill_num', 50);
            $table->integer('status_id')->default(0);
            $table->integer('pens')->nullable();
            $table->integer('inv')->nullable();
            $table->integer('inv_timeless')->nullable();
            $table->integer('inv_group')->nullable();
            $table->integer('notorios')->nullable();
            $table->integer('transfer')->nullable();
            $table->integer('finished_id')->nullable();
            $table->string('inv_start')->nullable();
            $table->string('inv_end')->nullable();
            $table->integer('book_status_id')->default(0);
            $table->bigInteger('card_number')->nullable();
            $table->string('bank_name')->nullable();
            $table->text('BIK')->nullable();
            $table->string('bank_account')->nullable();
            $table->string('country')->nullable();
            $table->string('adress')->nullable();
            $table->string('fact_adress')->nullable();
            $table->text('cancel_reason')->nullable();
            $table->integer('vip_bonus')->nullable();
            $table->text('vip_bonus_logins')->nullable();
            $table->text('not_fio')->nullable();
            $table->text('not_adress')->nullable();
            $table->text('not_iin')->nullable();
            $table->text('not_passport')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('start_bonus_request');
    }
}