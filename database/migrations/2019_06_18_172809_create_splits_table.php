<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSplitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('splits', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('status_id')->default(0);
            $table->integer('program_id');
            $table->integer('main_matrix_id');
            $table->integer('first_matrix_id');
            $table->integer('leader_id');
            $table->integer('second_matrix_id');
            $table->integer('sponsor_id');
            $table->integer('new_id');
            $table->text('main_matrix');
            $table->text('first_matrix');
            $table->text('second_matrix');
            $table->text('leader');
            $table->text('sponsor');
            $table->text('new');
            $table->integer('matrix_stage');
            $table->string('matrix_dateout');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('splits');
    }
}