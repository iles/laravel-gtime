<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChangeLeaderHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('change_leader_histories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('programm');
            $table->integer('mx_id');
            $table->integer('old_leader');
            $table->integer('new_leader');
            $table->string('old_leader_name')->nullable();
            $table->string('new_leader_name')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('change_leader_histories');
    }
}
