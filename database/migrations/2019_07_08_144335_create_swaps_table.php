<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSwapsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('swaps', function (Blueprint $table) {
            $table->increments('id');
            $table->text('login');
            $table->text('login2');
            $table->integer('type');
            $table->integer('uid');
            $table->integer('uid2')->nullable();
            $table->integer('program_id');
            $table->integer('reason')->nullable();
            $table->integer('sec_check')->nullable();
            $table->integer('status_id')->default(0);
            $table->integer('view')->default(0);
            $table->text('cancel_reason')->nullable();
            $table->text('image')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('swaps');
    }
}
