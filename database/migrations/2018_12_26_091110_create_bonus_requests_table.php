<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBonusRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bonus_requests', function (Blueprint $table) {
            $table->increments('id');
            $table->string('uname')->nullable();
            $table->string('name')->nullable();
            $table->string('second_name')->nullable();
            $table->string('patronic')->nullable();
            $table->string('exist_pin')->nullable();
            $table->string('registration_date')->nullable();
            $table->string('payment_date')->nullable();
            $table->string('fact_date')->nullable();
            $table->string('bank')->nullable();
            $table->string('checkout_date')->nullable();
            $table->integer('program')->nullable();
            $table->string('programm_date')->nullable();
            $table->string('summ')->nullable();
            $table->string('iin')->nullable();
            $table->string('country')->nullable();
            $table->string('city')->nullable();
            $table->string('bill_number')->nullable();
            $table->string('cancel_reason')->nullable();
            $table->integer('status_id')->default('0');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bonus_requests');
    }
}
