<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStartRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('start_requests', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('image',90);
            $table->string('bill' ,190);
            $table->string('iin',190);
            $table->integer('kassa')->nullable();
            $table->string('uname',190)->nullable();
            $table->string('fullname',190)->nullable();
            $table->string('fullname_b',190)->nullable();
            $table->string('summ',190);
            $table->string('currency',190)->nullable();
            $table->string('skype',190)->nullable();
            $table->integer('status_id')->default(0);
            $table->string('cancel_reason', 590)->nullable();
            $table->text('note')->nullable();
            $table->string('exist_pin')->nullable();
            $table->string('registration_date')->nullable();
            $table->timestamp('aprove_at')->nullable();
            $table->string('payment_date')->nullable();
            $table->string('fact_date')->nullable();
            $table->string('bank')->nullable();
            $table->string('checkout_date')->nullable();
            $table->integer('program')->nullable();
            $table->string('programm_date')->nullable();
            $table->integer('security_check')->nullable();
            $table->integer('transitive')->nullable();
            $table->string('date');
            $table->string('country');
            $table->string('city')->nullable();
            $table->text('user_note')->nullable();

            $table->timestamps(); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('start_requests');
    }
}
