<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStartPinsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('start_pins', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('username')->nullable();
            $table->integer('request_id')->unsigned()->nullable();
            $table->integer('request_program_id');
            $table->integer('program');
            $table->integer('transitive')->nullable();
            $table->integer('timeless')->nullable();
            $table->string('pin');
            $table->integer('kassa')->nullable();
            $table->integer('status_id')->default(0);
            $table->dateTime('expired_at')->nullable();
            $table->dateTime('active_at')->nullable();
            $table->timestamps();
            $table->foreign('request_id')->references('id')->on('start_requests')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('start_pins');
    }
}
