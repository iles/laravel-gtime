<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePinsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pins', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('program');
            $table->string('image',90);
            $table->string('bill' ,190);
            $table->string('iin',190);
            $table->string('uname',190)->nullable();
            $table->string('summ',190);
            $table->string('skype',190)->nullable();
            $table->integer('status_id')->default(0);
            $table->integer('expired')->default(0);
            $table->dateTime('expired_at')->nullable();
            $table->dateTime('active_at')->nullable();
            $table->string('pin',90)->nullable();
            $table->string('cancel_reason', 590)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pins');
    }
}
