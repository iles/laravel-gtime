<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFinishedsTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('finisheds', function (Blueprint $table) {
			$table->increments('id');
			$table->string('program_id');
			$table->string('user_id');
			$table->string('login');
			$table->string('fullname');
			$table->string('birthdate');
			$table->string('iin');
			$table->string('registred_pin');
			$table->string('country');
			$table->string('info')->nullable();
			$table->integer('transfer')->nullable();
			$table->string('tovar')->nullable();
			$table->string('phone')->nullable();
			$table->integer('variant')->nullable();
			$table->string('pin')->nullable();
			$table->string('pin2')->nullable();
			$table->string('pin3')->nullable();
			$table->string('setr')->nullable();
			$table->integer('bonus')->nullable();
			$table->integer('stage')->default(0);
			$table->string('vip_bonus')->nullable();
			$table->text('last')->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists('finisheds');
	}
}
