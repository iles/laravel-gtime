<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCancellationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cancellations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('program_id');
            $table->string('pin');
            $table->string('login');
            $table->string('fullname');
            $table->integer('uid');
            $table->integer('mx_id');
            $table->integer('sponsor_id');
            $table->integer('status_id')->default(0);
            $table->string('sponsor_login');
            $table->string('sponsor_fullname');
            $table->timestamp('reg_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cancellations');
    }
}
