<?php

use Illuminate\Database\Seeder;

class DleUsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('dle_users')->delete();
        
        \DB::table('dle_users')->insert(array (
            0 => 
            array (
           'user_id' => 1,
           'screch' => 'asdas',
            'tovar' => 'asdas',
            'password'  => '9db06bcff9248837f86d1a6bcf41c9e7',
            'email' => 'user@mail.com',
            'name' => 'user',
            'news_nums' => 1,
            'user_group' => 1,
            'comm_num'  => 1,
            'lastdate'  => 1,
            'reg_date' => 1,
            'banned'=> 1,
            'allow_mail' => 1,
            'info' => 1,
            'signature' => 1,
            'foto'  => 'asdas', 
            'fullname' => 1,
            'land' => 1,
            'icq' => 1,
            'favorites' => 1,
            'pm_all'  => 1,
            'pm_unread'  => 1,
            'time_limit' => 1,
            'allowed_ip' => 'asdas',
            'hash' => 'asdas',
            'xfields' => 1,
            'logged_ip'  => 1,
           'restricted' => 1,
            'restricted_days' => 1,
            'restricted_date' => 'asdas',
           	'stage' => 1,
            'matrix_id' => 1,
            'ref_num' => '1',
            'by_refer' => 2,
            'active'=> 1,
            'balance' => 0,
            'balance_user' => 0,
            'get_out' => 1,
            'familiya' => 'asdas',
            'imya' => 'asdas',
            'otchestvo' => 'asdas',
            'pol' => 'asdas',
            'strana' => 'asdas',
            'gorod' => 'asdas',
            'skype' => 'asdas'
            ),
        ));
    }
}
