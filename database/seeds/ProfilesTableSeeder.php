<?php

use Illuminate\Database\Seeder;

class ProfilesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('profiles')->delete();
        
        \DB::table('profiles')->insert(array (
            0 => 
            array (
                'id' => 1,
                'user_id' => 1,
                'info' => NULL,
                'logo' => NULL,
                'is_online' => false,
                'created_at' => '2018-10-03 05:26:22',
                'updated_at' => '2018-10-03 05:26:22',
            ),
            1 => 
            array (
                'id' => 2,
                'user_id' => 2,
                'info' => NULL,
                'logo' => NULL,
                'is_online' => false,
                'created_at' => '2018-10-06 09:05:09',
                'updated_at' => '2018-10-06 09:05:09',
            ),
        ));
        
        
    }
}