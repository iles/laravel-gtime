<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('roles')->delete();
        
        \DB::table('roles')->insert(array (
            array (
                'id' => 1,
                'name' => 'superadmin',
                'guard_name' => 'web',
                'created_at' => '2018-10-03 05:25:22',
                'updated_at' => '2018-10-03 05:25:22',
            ),
            array (
                'id' => 2,
                'name' => 'admin',
                'guard_name' => 'web',
                'created_at' => '2018-10-06 10:36:04',
                'updated_at' => '2018-10-06 10:36:04',
            ),
            array (
                'id' => 3,
                'name' => 'moderator',
                'guard_name' => 'web',
                'created_at' => '2018-10-06 10:42:33',
                'updated_at' => '2018-10-06 10:42:33',
            ),
            array (
                'id' => 4,
                'name' => 'content-manager',
                'guard_name' => 'web',
                'created_at' => '2018-10-06 10:42:51',
                'updated_at' => '2018-10-06 10:42:51',
            ),            
            array (
                'id' => 5,
                'name' => 'warehouse',
                'guard_name' => 'web',
                'created_at' => '2018-10-06 10:42:51',
                'updated_at' => '2018-10-06 10:42:51',
            ),            
            array (
                'id' => 6,
                'name' => 'warehouse_admin',
                'guard_name' => 'web',
                'created_at' => '2018-10-06 10:42:51',
                'updated_at' => '2018-10-06 10:42:51',
            ),            
            array (
                'id' => 7,
                'name' => 'bookkeeping',
                'guard_name' => 'web',
                'created_at' => '2018-10-06 10:42:51',
                'updated_at' => '2018-10-06 10:42:51',
            ),            
            array (
                'id' => 8,
                'name' => 'kassa',
                'guard_name' => 'web',
                'created_at' => '2018-10-06 10:42:51',
                'updated_at' => '2018-10-06 10:42:51',
            ),            
            array (
                'id' => 9,
                'name' => 'service',
                'guard_name' => 'web',
                'created_at' => '2018-10-06 10:42:51',
                'updated_at' => '2018-10-06 10:42:51',
            ),            
            array (
                'id' => 10,
                'name' => 'security',
                'guard_name' => 'web',
                'created_at' => '2018-10-06 10:42:51',
                'updated_at' => '2018-10-06 10:42:51',
            ),
        ));
        
        
    }
}