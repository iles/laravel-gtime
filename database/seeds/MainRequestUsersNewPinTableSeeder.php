<?php

use Illuminate\Database\Seeder;

class MainRequestUsersNewPinTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('main_request_users_new_pin')->delete();
        
        \DB::table('main_request_users_new_pin')->insert(array (
            0 => 
            array (
                'id' => 1,
                'json' => NULL,
                'user_id' => 3,
                'status' => 0,
                'uname' => NULL,
                'cancel_reason' => NULL,
                'pin' => '31231872987601W',
                'expires_at' => '2018-10-28 15:24:01',
                'created_at' => '2018-10-28 15:24:01',
                'updated_at' => '2018-10-28 15:24:01',
            ),
        ));
        
        
    }
}