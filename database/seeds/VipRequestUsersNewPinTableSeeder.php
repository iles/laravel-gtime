<?php

use Illuminate\Database\Seeder;

class VipRequestUsersNewPinTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('vip_request_users_new_pin')->delete();
        
        \DB::table('vip_request_users_new_pin')->insert(array (
            0 => 
            array (
                'id' => 1,
                'json' => NULL,
                'user_id' => 68,
                'status' => 0,
                'uname' => 'Akgula',
                'cancel_reason' => NULL,
                'pin' => 'I39800664701908',
                'expires_at' => '2018-10-28 16:07:11',
                'created_at' => '2018-10-28 16:07:11',
                'updated_at' => '2018-10-28 16:07:11',
            ),
            1 => 
            array (
                'id' => 2,
                'json' => NULL,
                'user_id' => 482,
                'status' => 0,
                'uname' => 'Арай',
                'cancel_reason' => NULL,
                'pin' => '2H3836254703279',
                'expires_at' => '2018-10-28 16:08:02',
                'created_at' => '2018-10-28 16:08:02',
                'updated_at' => '2018-10-28 16:08:02',
            ),
            2 => 
            array (
                'id' => 3,
                'json' => NULL,
                'user_id' => 575,
                'status' => 0,
                'uname' => 'Forever',
                'cancel_reason' => NULL,
                'pin' => '561V77158287924',
                'expires_at' => '2018-10-28 16:08:19',
                'created_at' => '2018-10-28 16:08:19',
                'updated_at' => '2018-10-28 16:08:19',
            ),
        ));
        
        
    }
}