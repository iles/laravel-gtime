<?php

use Illuminate\Database\Seeder;

class PinsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('pins')->delete();
        
        \DB::table('pins')->insert(array (
            0 => 
            array (
                'id' => 1,
                'user_id' => 51597,
                'program' => 1,
                'image' => '1555998695.jpg',
                'bill' => '2313123123',
                'iin' => '123123123',
                'uname' => 'Виталик',
                'summ' => '1231232',
                'skype' => 'aasdw',
                'status_id' => 3,
                'expired' => 0,
                'expired_at' => '2019-04-25 11:52:44',
                'active_at' => '2019-04-23 11:52:44',
                'pin' => '898Q46904875096',
                'cancel_reason' => NULL,
                'created_at' => '2019-04-23 11:51:35',
                'updated_at' => '2019-04-23 11:53:28',
            ),
        ));
        
        
    }
}