<?php

use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('permissions')->delete();
        
        \DB::table('permissions')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => '',
                'guard_name' => 'web',
                'created_at' => '2018-10-03 05:25:22',
                'updated_at' => '2018-10-03 05:25:22',
            ),
        ));
        
        
    }
}