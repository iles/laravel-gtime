<?php

use Illuminate\Database\Seeder;

class StartRequestUsersNewPinTableSeeder extends Seeder {

	/**
	 * Auto generated seed file
	 *
	 * @return void
	 */
	public function run() {

		\DB::table('start_request_users_new_pin')->delete();

		\DB::table('start_request_users_new_pin')->insert(array(
			0 => array(
				'bill_number' => NULL,
				'cancel_reason' => NULL,
				'created_at' => '2019-03-17 01:38:19',
				'expires_at' => NULL,
				'id' => 111,
				'iin' => NULL,
				'image' => '1552765099.jpg',
				'json' => '{"uname":"user2","password2":"111111","secondname":"Second","name":"User","patronic":"Super","country":"\\u0410\\u043b\\u043c\\u0430\\u0442\\u0438\\u043d\\u0441\\u043a\\u0430\\u044f \\u043e\\u0431\\u043b\\u0430\\u0441\\u0442\\u044c","birthdate":"22-12-1192","phone":"+7(123)-123-12-31","iin":"231231231231"}',
				'pin' => 'X38189358650522',
				'skype' => NULL,
				'status' => 0,
				'summ' => NULL,
				'uname' => 'зуля',
				'updated_at' => '2019-03-17 01:38:19',
				'user_id' => 4527,
			),
		));

	}
}