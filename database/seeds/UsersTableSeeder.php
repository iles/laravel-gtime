<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('users')->delete();
        
        \DB::table('users')->insert(array (
            0 => 
            array (
                'id' => 2,
                'name' => 'admin',
                'email' => 'admin@mail.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$piJldAp.kLp5gjj8GZkpp.jGjYPhulISyTItxNLAJkvHazsgjLBve',
                'remember_token' => NULL,
                'created_at' => '2018-10-06 09:05:08',
                'updated_at' => '2018-10-06 09:05:08',
            ),
            1 => 
            array (
                'id' => 1,
                'name' => 'superadmin',
                'email' => 'superadmin@mail.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$N.3RNoBhC4D7tLXAD98IiusMNU7Av6iTT9Ch.ZQvPqepcP/.aM2Fy',
                'remember_token' => 'myxXWD396TwTdq3I6QSn2noSA1tcfPYYQVvSTCpGhj3a7rjjqZq8dBV8QQdF',
                'created_at' => '2018-10-03 05:26:22',
                'updated_at' => '2018-10-03 05:26:22',
            ),
            array (
                'id' => 3,
                'name' => 'warehouse',
                'email' => 'warehouse@mail.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$N.3RNoBhC4D7tLXAD98IiusMNU7Av6iTT9Ch.ZQvPqepcP/.aM2Fy',
                'remember_token' => 'myxXWD396TwTdq3I6QSn2noSA1tcfPYYQVvSTCpGhj3a7rjjqZq8dBV8QQdF',
                'created_at' => '2018-10-03 05:26:22',
                'updated_at' => '2018-10-03 05:26:22',
            ),             
            array (
                'id' => 5,
                'name' => 'warehouse2',
                'email' => 'warehouse2@mail.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$N.3RNoBhC4D7tLXAD98IiusMNU7Av6iTT9Ch.ZQvPqepcP/.aM2Fy',
                'remember_token' => 'myxXWD396TwTdq3I6QSn2noSA1tcfPYYQVvSTCpGhj3a7rjjqZq8dBV8QQdF',
                'created_at' => '2018-10-03 05:26:22',
                'updated_at' => '2018-10-03 05:26:22',
            ),            
            array (
                'id' => 4,
                'name' => 'warehouse_admin',
                'email' => 'warehouse_admin@mail.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$N.3RNoBhC4D7tLXAD98IiusMNU7Av6iTT9Ch.ZQvPqepcP/.aM2Fy',
                'remember_token' => 'myxXWD396TwTdq3I6QSn2noSA1tcfPYYQVvSTCpGhj3a7rjjqZq8dBV8QQdF',
                'created_at' => '2018-10-03 05:26:22',
                'updated_at' => '2018-10-03 05:26:22',
            ),            
            array (
                'id' => 6,
                'name' => 'bookkeeping',
                'email' => 'bookkeeping@mail.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$N.3RNoBhC4D7tLXAD98IiusMNU7Av6iTT9Ch.ZQvPqepcP/.aM2Fy',
                'remember_token' => 'myxXWD396TwTdq3I6QSn2noSA1tcfPYYQVvSTCpGhj3a7rjjqZq8dBV8QQdF',
                'created_at' => '2018-10-03 05:26:22',
                'updated_at' => '2018-10-03 05:26:22',
            ),            
            array (
                'id' => 7,
                'name' => 'kassa',
                'email' => 'kassa@mail.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$N.3RNoBhC4D7tLXAD98IiusMNU7Av6iTT9Ch.ZQvPqepcP/.aM2Fy',
                'remember_token' => 'myxXWD396TwTdq3I6QSn2noSA1tcfPYYQVvSTCpGhj3a7rjjqZq8dBV8QQdF',
                'created_at' => '2018-10-03 05:26:22',
                'updated_at' => '2018-10-03 05:26:22',
            ),            
            array (
                'id' => 8,
                'name' => 'service',
                'email' => 'service@mail.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$N.3RNoBhC4D7tLXAD98IiusMNU7Av6iTT9Ch.ZQvPqepcP/.aM2Fy',
                'remember_token' => 'myxXWD396TwTdq3I6QSn2noSA1tcfPYYQVvSTCpGhj3a7rjjqZq8dBV8QQdF',
                'created_at' => '2018-10-03 05:26:22',
                'updated_at' => '2018-10-03 05:26:22',
            ),           
            array (
                'id' => 9,
                'name' => 'security',
                'email' => 'security@mail.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$N.3RNoBhC4D7tLXAD98IiusMNU7Av6iTT9Ch.ZQvPqepcP/.aM2Fy',
                'remember_token' => 'myxXWD396TwTdq3I6QSn2noSA1tcfPYYQVvSTCpGhj3a7rjjqZq8dBV8QQdF',
                'created_at' => '2018-10-03 05:26:22',
                'updated_at' => '2018-10-03 05:26:22',
            ),
        ));
        
        
    }
}