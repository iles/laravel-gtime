<?php

use Illuminate\Database\Seeder;

class AutoPinsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('start_pins')->delete();
        
        \DB::table('start_pins')->insert(array (
            0 => 
            array (
                'id' => 1,
                'user_id' => 0,
                'request_id' => null,
                'request_program_id' => 0,
                'pin' => 'RG098677479993RG',
                'program' => 2,
                'status_id' => 0,
                'timeless' => 1,
                'kassa' => 0,
                'expired_at' => NULL,
                'active_at' => NULL,
                'created_at' => NULL,
                'updated_at' => '2019-06-05 12:29:55',
            ),
            1 => 
            array (
                'id' => 2,
                'user_id' => 0,
                'request_id' => null,
                'request_program_id' => 0,
                'pin' => 'PL026117246912RG',
                'program' => 2,
                'status_id' => 0,
                'timeless' => 1,
                'kassa' => 0,
                'expired_at' => NULL,
                'active_at' => NULL,
                'created_at' => NULL,
                'updated_at' => '2019-06-05 11:26:16',
            ),
            2 => 
            array (
                'id' => 3,
                'user_id' => 0,
                'request_id' => null,
                'request_program_id' => 0,
                'pin' => 'RG053677479993RG',
                'program' => 2,
                'status_id' => 0,
                'timeless' => 1,
                'kassa' => 0,
                'expired_at' => NULL,
                'active_at' => NULL,
                'created_at' => NULL,
                'updated_at' => '2019-06-05 11:26:16',
            ),
            3 => 
            array (
                'id' => 4,
                'user_id' => 0,
                'request_id' => null,
                'request_program_id' => 0,
                'pin' => 'PL602857329613RG',
                'program' => 2,
                'status_id' => 0,
                'timeless' => 1,
                'kassa' => 0,
                'expired_at' => NULL,
                'active_at' => NULL,
                'created_at' => NULL,
                'updated_at' => '2019-06-05 11:26:17',
            ),
            4 => 
            array (
                'id' => 5,
                'user_id' => 0,
                'request_id' => null,
                'request_program_id' => 0,
                'pin' => 'PL993596445717RG',
                'program' => 2,
                'status_id' => 0,
                'timeless' => 1,
                'kassa' => 0,
                'expired_at' => NULL,
                'active_at' => NULL,
                'created_at' => NULL,
                'updated_at' => '2019-06-05 11:26:17',
            ),
        ));
        
        
    }
}