<?php

use Illuminate\Database\Seeder;

class AutoRequestUsersNewPinTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('auto_request_users_new_pin')->delete();
        
        \DB::table('auto_request_users_new_pin')->insert(array (
            0 => 
            array (
                'id' => 1,
                'json' => NULL,
                'user_id' => 784,
                'status' => 0,
                'uname' => 'Адиет10',
                'cancel_reason' => NULL,
                'pin' => 'E19529244493713',
                'expires_at' => '2018-10-28 16:09:00',
                'created_at' => '2018-10-28 16:09:00',
                'updated_at' => '2018-10-28 16:09:00',
            ),
        ));
        
        
    }
}