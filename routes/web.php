<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */
Route::get('/test/{p}/{t}', 'programs\ProgramMainController@test');
Route::post('/auth-validate', ['as' => 'authValidate', 'uses' => 'HomeController@authValidate']);
Route::get('/roll', 'FrontController@roll');
Route::get('/username', 'FrontController@username');
Route::get('/admin/check_login', ['as' => 'warehouse.check', 'uses' => 'WarehouseController@checkLogin']);
Route::get('/export', ['as' => 'warehouse.export', 'uses' => 'WarehouseController@export']);
Route::get('check_login_kassa', ['as' => 'kass.check', 'uses' => 'KassaController@checkLogin']);
Route::get('/getcities', ['as' => 'kass.check', 'uses' => 'FrontController@getCities']);

Route::post('/pin_stat', ['as' => 'pins.stat', 'uses' => 'PinsController@pin_stat']);
Route::post('/pin_delete', ['as' => 'pins.stat_delete', 'uses' => 'PinsController@pin_delete']);

Route::post('/change', ['as' => 'change', 'uses' => 'HomeController@change']);
Route::post('/check/status', ['as' => 'check.status', 'uses' => 'PinsController@checkStatus']);

Route::post('/autofree', ['as' => 'auto.free', 'uses' => 'PincodesController@free_auto']);
Route::get('/testclear', 'HomeController@test');


// Route::group(
// 	[
// 		'prefix' => LaravelLocalization::setLocale(),
// 		'middleware' => ['localeSessionRedirect', 'localizationRedirect', 'localeViewPath'],
// 	],
// 	function () {

		Route::group(['middleware' => ['web']], function () {
			Route::get('/', 'FrontController@ladder');
		});

		Route::group(['middleware' => ['auth:web', 'role:superadmin']], function () {
			Route::prefix('admin')->group(function () {
				Route::get('index', ['as' => 'admins.index', 'uses' => 'AdminController@index']);
				Route::get('show', ['as' => 'admins.show', 'uses' => 'AdminController@show']);
				Route::get('create', ['as' => 'admins.create', 'uses' => 'AdminController@create']);
				Route::post('store', ['as' => 'admins.store', 'uses' => 'AdminController@store']);
				Route::post('update', ['as' => 'admins.update', 'uses' => 'AdminController@update']);
				Route::delete('destroy', ['as' => 'admins.destroy', 'uses' => 'AdminController@destroy']);

				Route::post('/ladder/revert', ['as' => 'split.revert', 'uses' => 'SplitController@revert']);
				Route::get('/mu_swap', ['as' => 'matrix.mu_swap', 'uses' => 'MatrixController@mu_swap']);
				Route::post('/do/swap', ['as' => 'matrix.do_mu_swap', 'uses' => 'MatrixController@doswapmu']);
			});
		});

		Route::group(['middleware' => ['auth:web', 'role:superadmin|admin']], function () {
			Route::prefix('admin')->group(function () {
				Route::resource('splits', 'SplitController');
				//Route::get('/splits', ['as' => 'splits.index', 'uses' => 'SplitController@index']);
			});
		});

		Route::group(['middleware' => ['auth:web', 'role:security']], function () {
			Route::get('/sb', ['as' => 'security.index', 'uses' => 'SecurityController@index']);
			Route::get('/sec/show', ['as' => 'security.show', 'uses' => 'SecurityController@show']);
			Route::post('/sec/aprove', ['as' => 'security.aprove', 'uses' => 'SecurityController@aprove']);
			Route::get('/sb/swap', ['as' => 'security.swap', 'uses' => 'SecurityController@swap']);
		});

		Route::prefix('partner')->group(function () {
			Route::get('login', 'Auth\PartnerLoginController@login')->name('partner.login');
			Route::get('reset', 'Auth\PartnerLoginController@reset')->name('partner.reset');
			Route::post('pass_reset', 'Auth\PartnerLoginController@resethandle')->name('partner.resethandle');
			Route::post('login', 'Auth\PartnerLoginController@loginPartner')->name('partner.loginPartner');
			Route::get('ladder', 'FrontController@ladder')->name('ladder');
			Route::get('swapuser', 'FrontController@swapuser')->name('swapuser');
			Route::post('swapuser', 'FrontController@doswapuser')->name('partner.swap');
			Route::post('logout', 'Auth\PartnerLoginController@logout')->name('partner.logout');
			Route::get('search', 'FrontController@search');
			Route::get('structure', 'FrontController@structure')->name('partner.structure');
			Route::get('registration', 'FrontController@registration_rules')->name('partner.registration_rules');
			Route::post('registration', 'FrontController@registration')->name('partner.registration');
			Route::post('register_partner', 'FrontController@register_partner')->name('partner.register_partner');
			Route::get('marketing', 'FrontController@marketing')->name('partner.marketing');
			Route::get('bonus', 'FrontController@bonus')->name('partner.bonus');
			Route::post('bonus', 'FrontController@bonusstore')->name('partner.bonus.store');
			Route::post('rebonus', 'FrontController@rebonus')->name('partner.rebonus');
			Route::post('rebonusvip', 'FrontController@rebonusvip')->name('partner.rebonusvip');
			Route::get('pin', 'FrontController@pin')->name('partner.pin');
			Route::get('pin/requests', 'FrontController@pin_requests')->name('partner.pin.requests');
			Route::get('pin/requests/create', 'FrontController@create_request')->name('partner.pin.create');
			Route::get('profile', 'FrontController@profile')->name('partner.profile');
			Route::post('pin', 'FrontController@pinstore')->name('partner.pin.store');
			Route::get('check-login', ['as' => 'front.check', 'uses' => 'FrontController@checkLogin']);
			Route::get('check-pin', ['as' => 'front.checpin', 'uses' => 'FrontController@checkPin']);
			Route::get('check-sponsor', ['as' => 'front.checksponosr', 'uses' => 'FrontController@checkSponsor']);
			Route::get('finish', ['as' => 'front.finish', 'uses' => 'FrontController@finish']);
			Route::get('finish_bonus', ['as' => 'front.finish_bonus', 'uses' => 'FrontController@finish_bonus']);
			Route::get('finish_pin', ['as' => 'front.finish_pin', 'uses' => 'FrontController@finish_pin']);
			Route::get('main/finish', ['as' => 'front.main_finish', 'uses' => 'FrontController@main_finish']);
			Route::get('vip/finish', ['as' => 'front.vip_finish', 'uses' => 'FrontController@vip_finish']);
			Route::get('bonus-program/finish', ['as' => 'front.bonus_finish', 'uses' => 'FrontController@bonus_finish']);
			Route::get('vipbonus', ['as' => 'vipbonus', 'uses' => 'FrontController@vipbonus']);
			Route::get('bonus-form', ['as' => 'bonus-form', 'uses' => 'FrontController@bonusform']);

			Route::get('/bonus/', ['as' => 'bonus.index', 'uses' => 'BonusController@index']);
			Route::get('/partner/bonus/show/{id}', ['as' => 'bonus.show', 'uses' => 'BonusController@show']);
			Route::get('/bonus/create', ['as' => 'bonus.create', 'uses' => 'BonusController@create']);
			Route::delete('/bonus/destroy/{id}', ['as' => 'bonus.destroy', 'uses' => 'BonusController@destroy']);
			Route::post('/bonus/add', ['as' => 'bonus.bonusadd', 'uses' => 'FrontController@bonusadd']);
			Route::post('/bonusbonus/bonusadd', ['as' => 'bonus.bonusadd', 'uses' => 'FrontController@bonusbonusadd']);
			Route::post('/bonus/add', ['as' => 'bonus.add', 'uses' => 'FrontController@bonusadd']);
			Route::post('/bonus/add', ['as' => 'vipbonus.add', 'uses' => 'FrontController@vipbonusadd']);
			Route::post('/pin/add', ['as' => 'pin.add', 'uses' => 'FrontController@pinadd']);
			Route::post('/pin/plus/add', ['as' => 'pin.plus.add', 'uses' => 'programs\ProgramAcummulative_plusController@pinadd']);
		});
		
		Route::post('/bonus/add', ['as' => 'bonus.add', 'uses' => 'FrontController@bonusadd']);

		Route::prefix('admin')->group(function () {
			
			Route::get('login', 'Auth\AdminLoginController@login')->name('admin.login');
			Route::post('login', 'Auth\AdminLoginController@loginAdmin')->name('admin.loginAdmin');
			Route::post('logout', 'Auth\AdminLoginController@logout')->name('admin.logout');
			
		   	Route::get('/translation','TranlationController@index')->name('admin.translation.index');
		   	Route::post('/translation','TranlationController@save')->name('admin.translation.save');


			Route::get('pins', ['as' => 'admin.pins', 'uses' => 'PinsController@index']);
			Route::get('pins/export', ['as' => 'admin.pins.export', 'uses' => 'PinsController@export']);
			Route::delete('/pins/destroy/{id}', ['as' => 'pins.destroy', 'uses' => 'PinsController@destroy']);
			
			Route::get('pins/block/{id}', ['as' => 'admin.pins.block', 'uses' => 'PincodesController@block']);
			Route::get('pins/unblock/{id}', ['as' => 'admin.pins.unblock', 'uses' => 'PincodesController@unblock']);
						
			Route::get('finished', ['as' => 'partners.finished', 'uses' => 'SponsorsController@finished']);
						
			Route::get('requests', ['as' => 'requests.index', 'uses' => 'PartnerController@index']);
			Route::delete('/pincode/destroy', ['as' => 'pincode.destroy', 'uses' => 'PinsController@pin_destroy']);
			Route::resource('bonusrequest', 'BonusRequestController');
			 
			//Route::get('bonusrequest/statusupdate/', ['as' => 'bonusrequest.update', 'uses' => 'BonusRequestController@statusupdate']);
			//Route::post('bonusrequest/aprove/{id}/{s}',  [ 'as' => 'bonusrequest.aprove', 'uses' => 'BonusRequestController@aprove'] );
			
			//Route::match(array('GET', 'POST'), 'bonusrequest/aprove/{id}/{s}', ['as' => 'bonusrequest.bookaprove', 'uses' => 'BookkeepingController@aprove']);
			
			Route::match(array('GET', 'POST'), 'bonusrequest/aprove/{id}/{s}', ['as' => 'bonusrequest.bookaprove', 'uses' => 'BookkeepingController@aprove']);

			Route::get('partners/', ['as' => 'sponsors.index', 'uses' => 'SponsorsController@index']);
			Route::post('partners/passchange', ['as' => 'sponsors.passchange', 'uses' => 'SponsorsController@passchange']);

			Route::get('/finisheds/show/{id}', ['as' => 'finisheds.show', 'uses' => 'SponsorsController@finished_show']);
			Route::get('/finisheds/show_pin/{id}', ['as' => 'finisheds.show_pin', 'uses' => 'SponsorsController@finished_show_pin']);
			Route::get('/finisheds/create', ['as' => 'finisheds.create', 'uses' => 'SponsorsController@create']);
			Route::delete('/finisheds/destroy/{id}', ['as' => 'finisheds.destroy', 'uses' => 'SponsorsController@destroy']);
			Route::post('/finisheds/add', ['as' => 'finisheds.add', 'uses' => 'SponsorsController@add']);
			Route::post('/finisheds/update/{id}', ['as' => 'finisheds.update', 'uses' => 'SponsorsController@finished_update']);
			Route::post('/finisheds/dropupdate', ['as' => 'finisheds.dropupdate', 'uses' => 'SplitController@dropupdate']);
			Route::get('/finisheds/viewdrop', ['as' => 'finisheds.viewDrop', 'uses' => 'SplitController@viewDrop']);

		});

		Route::group(['prefix' => 'admin'], function () {
			Route::resource('warehouse', 'WarehouseController');

			// Route::get('warehouses', [ 'as' => 'warehouse.index', 'uses' => 'WarehouseController@index']);

			Route::post('admin-request-add', ['as' => 'warehouse.add', 'uses' => 'WarehouseController@add']);
			Route::get('/ware', ['as' => 'warehouse.show', 'uses' => 'WarehouseController@show']);
			// Route::post('/warehouse/update/{$id}', ['as' => 'warehouse.update', 'uses' => 'WarehouseController@update']);
		});

		Route::group(['prefix' => 'admin'], function () {
			Route::get('warehouse-admin', ['as' => 'warehouse.admin_index', 'uses' => 'WarehouseController@index_admin']);
			Route::get('warehouse-admin/view', ['as' => 'warehouse.admin_view', 'uses' => 'WarehouseController@view_admin']);
			Route::post('warehouse-admin/updatea', ['as' => 'warehouse.admin_update', 'uses' => 'WarehouseController@update_admin']);
		});

		Route::group(['prefix' => 'admin'], function () {
			Route::get('/kassa', ['as' => 'kassa.index', 'uses' => 'KassaController@index']);
		});

		Route::group(['prefix' => 'admin'], function () {
			Route::get('bookkeeping', ['as' => 'bookkeeping.index', 'uses' => 'BookkeepingController@index']);
			Route::get('bookkeeping/warehouse', ['as' => 'bookkeeping.warehouse', 'uses' => 'BookkeepingController@warehouse']);
			Route::get('bookkeeping/credit', ['as' => 'bookkeeping.warehouse', 'uses' => 'BookkeepingController@credit']);
			Route::get('bookinput', ['as' => 'bookkeeping.input', 'uses' => 'BookkeepingController@inout']);
		});

		Route::get('/migration/run', [
			'uses' => 'MigrationController@run',
		]);

		Route::get('/migration/seed', [
			'uses' => 'MigrationController@seed',
		]);

		Route::get('/migration/clear', [
			'uses' => 'MigrationController@clear',
		]);

		Route::get('/create/', ['as' => 'startpinrequests.create', 'uses' => 'PartnerController@create']);
		Route::get('/show/', ['as' => 'startpinrequests.show', 'uses' => 'PartnerController@show']);
		Route::get('/destroy/', ['as' => 'startpinrequests.destroy', 'uses' => 'PartnerController@destroy']);

		Route::get('users/create/', ['as' => 'users.create', 'uses' => 'PartnerController@create']);
		Route::get('users/show/', ['as' => 'users.show', 'uses' => 'PartnerController@show']);
		Route::get('users/index/', ['as' => 'users.index', 'uses' => 'PartnerController@show']);
		Route::get('users/destroy/', ['as' => 'users.destroy', 'uses' => 'PartnerController@destroy']);

		Route::group(['middleware' => ['auth:web', 'role:admin']], function () {
			// Route::get('/', 'HomeController@index');

			//Route::get('/matrix/{type}/{id}', 'MatrixController@print');
			// Route::get('/partners/new', 'PartnerController@create');
			Route::post('/partners/add', 'PartnerController@add');
			// Route::get('/partners/all', 'PartnerController@index')->name('startgrid');;

		});

		Route::post('auth/register', 'Auth\AuthController@postRegister');

		Auth::routes();

		Route::get('/create', ['as' => 'partners.create', 'uses' => 'PartnerController@create']);
		Route::get('/show', ['as' => 'partners.show', 'uses' => 'PartnerController@show']);
		Route::get('/partners/', ['uses' => 'PartnerController@index']);
		Route::delete('/partners/destroy', ['as' => 'partners.destroy', 'uses' => 'PartnerController@destroy']);

		Route::get('/partners/all', ['as' => 'partners.index', 'uses' => 'PartnerController@index']);
		//Route::post('/partners/update', ['as' => 'partners.statusupdate', 'uses' => 'PartnerController@statusupdate']);

		Route::get('/change-leader', ['as' => 'matrix.change', 'uses' => 'MatrixController@change_leader']);

		Route::get('/history', ['as' => 'history.show', 'uses' => 'HistoryController@index']);
		Route::get('/history/create', ['as' => 'history.create', 'uses' => 'HistoryController@create']);
		Route::get('/history/all', ['as' => 'history.index', 'uses' => 'HistoryController@index']);
		Route::get('/history/destroy', ['as' => 'history.destroy', 'uses' => 'HistoryController@destroy']);

		Route::get('/matrix/', ['uses' => 'MatrixController@index']);
		Route::get('/matrix/all', ['as' => 'matrix.index', 'uses' => 'MatrixController@index']);
		Route::get('/matrix/show', ['as' => 'matrix.show', 'uses' => 'MatrixController@showfull']);
		Route::get('/matrix/user', ['as' => 'matrix.user', 'uses' => 'MatrixController@user_print']);
		Route::get('/matrix/create', ['as' => 'matrix.create', 'uses' => 'MatrixController@create']);
		Route::delete('/matrix/destroy', ['as' => 'matrix.destroy', 'uses' => 'MatrixController@destroy']);
		Route::get('/matrix/block', ['as' => 'matrix.block', 'uses' => 'MatrixController@block']);
		Route::get('/matrix/unblock', ['as' => 'matrix.unblock', 'uses' => 'MatrixController@unblock']);

		Route::get('/partners/show', ['as' => 'sponsors.show', 'uses' => 'SponsorsController@show']);
		Route::get('/partners/create', ['as' => 'sponsors.create', 'uses' => 'SponsorsController@create']);
		Route::post('/partners/update', ['as' => 'sponsors.update', 'uses' => 'SponsorsController@update']);
		Route::post('/partners/store', ['as' => 'sponsors.store', 'uses' => 'SponsorsController@store']);
		Route::delete('/partners/destroy', ['as' => 'sponsors.destroy', 'uses' => 'SponsorsController@destroy']);

		Route::get('/payments/', ['as' => 'payments.index', 'uses' => 'PaymentController@index']);
		Route::get('/payments/show', ['as' => 'payments.show', 'uses' => 'PaymentController@show']);
		Route::get('/payments/create', ['as' => 'payments.create', 'uses' => 'PaymentController@create']);
		Route::post('/payments/update', ['as' => 'payments.update', 'uses' => 'PaymentController@update']);
		Route::post('/payments/store', ['as' => 'payments.store', 'uses' => 'PaymentController@store']);
		Route::delete('/payments/destroy', ['as' => 'payments.destroy', 'uses' => 'PaymentController@destroy']);
		Route::post('/payments/statusupdate', ['as' => 'payments.statusupdate', 'uses' => 'PaymentController@statusupdate']);

		Route::post('/reswap', ['as' => 'reswap', 'uses' => 'FrontController@reswap']);
		
		Route::get('/pins/', ['as' => 'pins.index', 'uses' => 'PinsController@index']);
		Route::get('/pins/print', ['as' => 'pins.print', 'uses' => 'PinsController@print']);
		Route::get('/pins/show', ['as' => 'pins.show', 'uses' => 'PinsController@show']);
		Route::get('/pins/show/kassa', ['as' => 'pins.show.kassa', 'uses' => 'PinsController@show_kassa']);
		Route::get('/pins/create', ['as' => 'pins.create', 'uses' => 'PinsController@create']);

		Route::get('/store/kassa', ['as' => 'pins.create.kassa', 'uses' => 'PinsController@create_kassa']);

		Route::post('/store/kassa/store_kassa', ['as' => 'pins.store.kassa', 'uses' => 'PinsController@store_kassa']);

		Route::post('/pins/update', ['as' => 'pins.update', 'uses' => 'PinsController@update']);
		Route::post('/pins/store', ['as' => 'pins.store', 'uses' => 'PinsController@store']);

		Route::post('/pins/statusupdate', ['as' => 'pins.statusupdate', 'uses' => 'PinsController@statusupdate']);
		Route::post('/bonus/statusupdate', ['as' => 'bonuses.statusupdate', 'uses' => 'BonusController@statusupdate']);

		Route::get('/bonusrequest/', ['as' => 'bonusrequests.index', 'uses' => 'BonusRequestController@index']);
		Route::get('/bonusrequest/show/{id}', ['as' => 'bonusrequests.show', 'uses' => 'BonusRequestController@show']);
		Route::get('/bonusrequest/create', ['as' => 'bonusrequests.create', 'uses' => 'BonusRequestController@create']);
		Route::get('/bonusrequest/create_admin', ['as' => 'bonusrequests.create_admin', 'uses' => 'BonusRequestController@create_admin']);
		Route::delete('/bonusrequest/destroy/{id}', ['as' => 'bonusrequests.destroy', 'uses' => 'BonusRequestController@destroy']);
		Route::post('/bonusrequest/add', ['as' => 'bonusrequests.add', 'uses' => 'BonusRequestController@add']);
		Route::post('bonusrequest/store_admin', ['as' => 'bonusrequest.store_admin', 'uses' => 'BonusRequestController@store_Admin']);		 
		
		Route::match(array('GET', 'POST'), 'bonusrequest/statusupdate/{id}', ['as' => 'bonusrequest.update', 'uses' => 'BonusRequestController@statusupdate']);
 
		Route::get('/bonuses/', ['as' => 'bonuses.index', 'uses' => 'BonusController@index']);
		Route::get('/bonuses/show/{id}', ['as' => 'bonuses.show', 'uses' => 'BonusController@show']);
		Route::get('/bonuses/create', ['as' => 'bonuses.create', 'uses' => 'BonusController@create']);
		Route::delete('/bonuses/destroy/{id}', ['as' => 'bonuses.destroy', 'uses' => 'BonusController@destroy']);
		Route::post('/bonuses/add', ['as' => 'bonuses.add', 'uses' => 'BonusController@add']);
		Route::post('/bonuses/update', ['as' => 'bonuses.update', 'uses' => 'BonusController@update']);

		Route::get('/pincodes', ['as' => 'pincodes.index', 'uses' => 'PincodesController@index']);
		Route::get('/pincodes/show/{id}', ['as' => 'pincodes.show', 'uses' => 'PincodesController@show']);
		Route::get('/pincodes/create', ['as' => 'pincodes.create', 'uses' => 'PincodesController@create']);
		Route::delete('/pincodes/destroy/{id}', ['as' => 'pincodes.destroy', 'uses' => 'PincodesController@destroy']);
		Route::post('/pincodes/add', ['as' => 'pincodes.add', 'uses' => 'PincodesController@add']);
		Route::post('/pincodes/update', ['as' => 'pincodes.update', 'uses' => 'PincodesController@update']);
		Route::get('/pincodes/auto', ['as' => 'pincodes.auto', 'uses' => 'PincodesController@auto']);
		Route::get('/pincodes/cancel', ['as' => 'pincodes.cancel', 'uses' => 'PincodesController@cancel']);
		Route::post('/pins/cancel', ['as' => 'pincodes.cancel', 'uses' => 'PincodesController@docancel']);

		Route::get('/warehouserequests/create', ['as' => 'warehouserequests.create', 'uses' => 'WarehouseController@create']);
		Route::get('/warehouserequests/destroy', ['as' => 'warehouserequests.destroy', 'uses' => 'WarehouseController@destroy']);
		Route::post('/warehouserequests/add', ['as' => 'warehouserequests.add', 'uses' => 'WarehouseController@add']);

		Route::get('/warehouseAdminrequests/show', ['as' => 'warehouse-admin.show', 'uses' => 'WarehouseController@print']);
		Route::get('/warehouseAdminrequests/create', ['as' => 'warehouse-admin.create', 'uses' => 'WarehouseController@create']);
		Route::get('/warehouseAdminrequests/destroy', ['as' => 'warehouse-admin.destroy', 'uses' => 'WarehouseController@destroy']);
		Route::post('/warehouseAdminrequests/add', ['as' => 'warehouse-admin.add', 'uses' => 'WarehouseController@add']);
		Route::get('/warehouse/aprove/{id}', ['as' => 'warehouse-admin.aprove', 'uses' => 'WarehouseController@aprove']);

		Route::resource('swaps', 'SwapController');
		Route::get('/swap', ['as' => 'service.swap', 'uses' => 'MatrixController@swap']);
		Route::get('/swapspr', ['as' => 'service.swapspr', 'uses' => 'MatrixController@swapspr']);
		Route::get('/swapldr', ['as' => 'service.swapldr', 'uses' => 'MatrixController@swapldr']);
		Route::post('/doswapspr', ['as' => 'service.doswapspr', 'uses' => 'MatrixController@doswapspr']);
		Route::get('/usercheck', ['as' => 'usercheck', 'uses' => 'MatrixController@usercheck']);
		Route::get('/pincodecheck', ['as' => 'usercheck', 'uses' => 'PincodesController@pinscheck']);
		Route::get('/sponsorcheck', ['as' => 'sponsorcheck', 'uses' => 'MatrixController@sponsorcheck']);
		Route::post('/doswap', ['as' => 'doswap', 'uses' => 'MatrixController@doswap']);
		Route::post('/service/doswap', ['as' => 'swap.make', 'uses' => 'SwapController@make']);


		Route::post('/manual/split', ['as' => 'manual.split', 'uses' => 'ManualController@split']);

		Route::post('/transfer/add', ['as' => 'transfer.add', 'uses' => 'FrontController@addTransfer']);


		Route::get('/sb/transfer', ['as' => 'transfer.sv', 'uses' => 'SecurityController@transfer']);

		Route::post('/transfer/aprove', ['as' => 'transfer.aprove', 'uses' => 'TransferController@aprove']);

		Route::resource('transfers', 'TransferController');

// Route::group(['prefix' => 'partners', 'before' => 'auth'], function () {
		//     Route::get('/create', [ 'as' => 'partners.create', 'uses' => 'PartnerController@create']);
		//     Route::get('partners/login', [ 'as' => 'partners.login', 'uses' => 'PartnerController@getLogin']);
		//     Route::post('partners/login', [ 'as' => 'partners.login', 'uses' => 'PartnerController@postSignin']);
		//     Route::get('partners/logout', [ 'as' => 'partners.logout', 'uses' => 'PartnerController@getLogout']);
		//     Route::resource('/','UserController');
		// });

	// });
