<?php

namespace App\Exports;

use App\Models\WarehouseRequest;
use Maatwebsite\Excel\Concerns\FromCollection;

class WarehouseExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return WarehouseRequest::all();
    }
}
