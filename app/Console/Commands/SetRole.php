<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;

class SetRole extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'role:set {user} {--role=}';
    //php artisan role:set 1 --role=superadmin
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $user = User::find($this->argument('user'));
        return $user->assignRole($this->option('role'));
    }
}
