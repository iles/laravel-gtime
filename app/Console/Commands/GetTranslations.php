<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use App\Models\Translations;

class GetTranslations extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'translations:build';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get Translation string to database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @param  \App\DripEmailer  $drip
     * @return mixed
     */
    public function handle()
    {
        $directory = resource_path('views');
        // echo $directory;
        $files = Storage::disk('resourses')->allFiles('views');
        // print_r($files);
        foreach ($files as $key => $file) {
            // echo $file;
            $contents = Storage::disk('resourses')->get($file);
            // $contents = Storage::disk('resourses')->get('views/front/layout/main.blade.php');
            // echo $contents;
            // echo $file;
            preg_match_all("/__\(\s*'(.*?)'\s*\)/x",
                $contents, $translates);
            if(!empty($translates) && count($translates) > 1)
                foreach ($translates[1] as $key => $value) {
                    preg_match("/^\w+\.\w+$/x", $value, $hasFileTanslation);
                    // echo $value;
                    // print_r($hasFileTanslation);
                    if(empty($hasFileTanslation)){
                        Translations::saveFileWord($value);
                    }
                }
            // break;
        }
        //
    }
}

