<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Schema;
use DB;

class ImportTables extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'database:import';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //Schema::getConnection()->getDoctrineSchemaManager()->dropDatabase('start_old');
        //  DB::statement("DROP DATABASE IF EXISTS `start_old`");
        //  DB::statement('CREATE DATABASE `start_old`');
        //  DB::unprepared(file_get_contents('database/dumps/start.sql'));
        echo 'dropped';
    }
}
