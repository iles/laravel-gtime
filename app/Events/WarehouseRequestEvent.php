<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

use App\Models\WarehouseRequest;



class WarehouseRequestEvent
{
    use SerializesModels;

    public $warehouseRequest;
    /**
     * Create a new event instance.
     *
     * @return void
     */

    public function __construct(WarehouseRequest $warehouseRequest)
    {
        $this->warehouseRequest = $warehouseRequest;
    }
    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */

}
