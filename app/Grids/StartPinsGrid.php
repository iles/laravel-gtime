<?php

namespace App\Grids;

use Closure;
use Leantony\Grid\Grid;
use App\Models\User;
use Config;
use Session;


class StartPinsGrid extends Grid implements StartPinsGridInterface
{
    /**
     * The name of the grid
     *
     * @var string
     */
    protected $name = 'StartPins';

    /**
     * List of buttons to be generated on the grid
     *
     * @var array
     */
    protected $buttonsToGenerate = [
        'create',
        'view',
        'delete',
        'ubblock',
        'refresh',
        'export'
    ];

    /**
     * Specify if the rows on the table should be clicked to navigate to the record
     *
     * @var bool
     */
    protected $linkableRows = false;

    /**
    * Set the columns to be displayed.
    *
    * @return void
    * @throws \Exception if an error occurs during parsing of the data
    */
    public function setColumns()
    {
    	
    	$progs = [];        
        foreach (Config::get('matrix.get') as $key => $matrix){
            $progs[$key] = $matrix[1];
        }
        $statusesf = [0 => 'Новый', 1 => 'Одобрен', 2 => 'Заблокирован', 3 => 'Активирован' ];
    
     $statuses = [0 => '<span class="badge badge-success">Новый</span>', 1 => '<span class="badge badge-primary">Одобрен</span>', 2 => '<span class="badge badge-danger">Заблокирован</span>', 3 => '<span class="badge badge-secondary">Активирован</span>' ];

        $this->columns = [
		    "id" => [
		        "label" => "ID",
		        "filter" => [
		            "enabled" => true,
		            "operator" => "="
		        ],
		        "styles" => [
		            "column" => "grid-w-10"
		        ]
		    ],
		   	"program" => [
		    	"label" => 'Программа',
                "filter" => [
                    "enabled" => true,
                    'type' => 'select',
                    'data' => $progs
                ],
                "data" => function ($columnData, $columnName) use ($progs) {
                    return $progs[$columnData->program];
                },
		        "export" => false
		    ],
            "user_id" => [
                "label" => 'Логин (кому выдали)',
                "data" => function ($columnData, $columnName) {
                    if($columnData->user_id == 0){
                        return 0;
                    }
                    $m = Config::get('matrix.get');
                    $p = $columnData->request_program_id;
                    $connection = 'mysql_prod_'.$m[$p][0];
                    $user = User::on($connection)->where('user_id', $columnData->user_id)->first();
					
					$uname = '';
					
					if(isset($user->name))
						$uname = $user->name;
					
                    return $uname;

 
               },
                "export" => false
            ],
		    "status_id" => [
		    	"label" => 'Статус',
                "raw" => true,
                "filter" => [
                    "enabled" => true,
                    'type' => 'select',
                    'data' => $statusesf
                ],
                "data" => function ($columnData, $columnName) use ($statuses) {
                    return $statuses[$columnData->status_id];
                },
		        "export" => false
		    ],
		    "pin" => [
		        "search" => [
		            "enabled" => true
		        ],
		        "filter" => [
		            "enabled" => true,
		            "operator" => "="
		        ]
		    ],

            "username" => [
                "label"=> "Логин (кто зарегестрировался)",
                "sort" => false,
                "raw" => true,
                "filter" => [
                    "enabled" => true,
                    "operator" => "=",
                ],
                "data" => function ($columnData, $columnName){
                    return $columnData->username;
                }
            ],
		    "expired_at" => [
		    	"label"=> "Действтелен до",
		        "sort" => false,
		        "date" => "true",
		        "filter" => [
		            "enabled" => true,
		            "type" => "date",
		            "operator" => "<="
		        ],
		        "data" => function ($columnData, $columnName){
                    return $columnData->expired_at;
                },
		    ],
		    "created_at" => [
		    	"label"=> "Создан",
		        "sort" => false,
		        "date" => "true",
		        "filter" => [
		            "enabled" => true,
		            "type" => "date",
		            "operator" => "<="
		        ],
		        "data" => function ($columnData, $columnName){
                    return $columnData->created_at;
                },
		    ]
		];
    }

    /**
     * Set the links/routes. This are referenced using named routes, for the sake of simplicity
     *
     * @return void
     */
    public function setRoutes()
    {
        // searching, sorting and filtering
        $this->setIndexRouteName('pincodes.index');

        // crud support
        $this->setCreateRouteName('pincodes.create');
        $this->setViewRouteName('pincodes.show');
        $this->setDeleteRouteName('pincodes.destroy');

        // default route parameter
        $this->setDefaultRouteParameter('id');
    }

    /**
    * Return a closure that is executed per row, to render a link that will be clicked on to execute an action
    *
    * @return Closure
    */
    public function getLinkableCallback(): Closure
    {
        return function ($gridName, $item) {
            return route($this->getViewRouteName(), [$gridName => $item->id]);
        };
    }

    /**
    * Configure rendered buttons, or add your own
    *
    * @return void
    */
    public function configureButtons()
    {
        // call `addRowButton` to add a row button
        // call `addToolbarButton` to add a toolbar button
        // call `makeCustomButton` to do either of the above, but passing in the button properties as an array

        // call `editToolbarButton` to edit a toolbar button
        // call `editRowButton` to edit a row button
        // call `editButtonProperties` to do either of the above. All the edit functions accept the properties as an array
        $this->editToolbarButton('refresh', [
            'name' => 'Обновить',
        ]);        

        $this->editRowButton('view', [
            'name' => 'Заблокировать',
            'renderIf' => function ($gridName, $item){
                return $item->status_id == 0;
            },
            'showModal' => false,
            'url' => function ($gridName, $item) {
                return '/admin/pins/block/'.$item->id;
            },
        ]);


        $this->makeCustomButton([
            'name' => 'ubblock',
            'label' => 'разблокировать',
            'icon' => 'fa-eye',
            'position' => 1,
            'class' => 'btn btn-outline-primary btn-sm grid-row-button',
            'showModal' => false,
            'gridId' => $this->getId(),
            'renderIf' => function($gridName, $item){
                return $item->status_id == 2;
            },
            'title' => 'разблокировать',
            'url' => function($gridName, $gridItem) {
                return route('admin.pins.unblock', ['id' => $gridItem->id]);
            },
        ], static::$TYPE_ROW);



        $this->editRowButton('delete', [
            'name' => 'Удалить',
            'renderIf' => function($gridName, $item){
                return $item->status_id !== 3;
            },
            'url' => function ($gridName, $item) {
                return '/admin/pincode/destroy?id='.$item->id;
            },
        ]);        

        $this->editToolbarButton('create', [
            'name' => 'Создать',
        ]); 
    }

    /**
    * Returns a closure that will be executed to apply a class for each row on the grid
    * The closure takes two arguments - `name` of grid, and `item` being iterated upon
    *
    * @return Closure
    */
    public function getRowCssStyle(): Closure
    {
        return function ($gridName, $item) {
            // e.g, to add a success class to specific table rows;
            // return $item->id % 2 === 0 ? 'table-success' : '';
            return "";
        };
    }
}
