<?php

namespace App\Grids;

use App\Models\Finished;
use App\Models\User;
use Closure;
use Config;
use Leantony\Grid\Grid;

class BonusesGrid extends Grid implements BonusesGridInterface {
	/**
	 * The name of the grid
	 *
	 * @var string
	 */
	protected $name = 'bonuses';

	/**
	 * List of buttons to be generated on the grid
	 *
	 * @var array
	 */
	protected $buttonsToGenerate = [
		'create',
		'view',
		'delete',
		'refresh',
		'export',
	];

	/**
	 * Specify if the rows on the table should be clicked to navigate to the record
	 *
	 * @var bool
	 */
	protected $linkableRows = false;

	/**
	 * Set the columns to be displayed.
	 *
	 * @return void
	 * @throws \Exception if an error occurs during parsing of the data
	 */
	public function setColumns() {

		$progs = [];
		$statuses = [0 => 'Новый', 1 => 'Одобрен', 2 => 'Отклонен', 5 => 'Отклонен (бух)', 4 => 'Оплачен(бух)'];
		$stages = [1 => 'Старт', 0 => 'Финиш'];
		$options = ['nabor1' => 'nabor1', 'nabor2' => 'nabor2', 'nabor3' => 'nabor3', 'nabor4' => 'nabor4'];

		foreach (Config::get('matrix.get') as $key => $matrix) {
			$progs[$key] = $matrix[1];
		}
		 
		$this->columns = [
			"id" => [
				"label" => "ID",
				"filter" => [
					"enabled" => true,
					"operator" => "=",
				],
			],
			//   "program_id" => [
			// "label" => "Программа",
			//       "search" => [
			//           "enabled" => true
			//       ],
			//       "filter" => [
			//           "enabled" => true,
			//                 'type' => 'select',
			//                 'data' => $progs
			//       ],
			// "data" => function ($columnData, $columnName) use ($progs) {
			//    	if ($columnData->program_id == null){
			//    		return '';
			//    	}
			//    	return $progs[$columnData->program_id];
			// },
			//   ],
			"summ" => [
				"label" => "Сумма",
				// "search" => [
				// 	"enabled" => true,
				// ],
				"filter" => [
					"enabled" => true,
					"operator" => "=",
				],
			],			
			"program_id" => [
				"label" => 'Программа',
				"export" => false,
				"filter" => [
					"enabled" => true,
					'type' => 'select',
					'data' => $progs,
				],
				"data" => function ($columnData, $columnName) use ($progs) {
					return $progs[$columnData->program_id];
				},

			],			
//			"stage" => [
//				"label" => 'Лестница',
//				"export" => false,
//				"filter" => [
//					"enabled" => true,
//					'type' => 'select',
//					'data' => $stages,
//				],
//				"data" => function ($columnData, $columnName) use ($stages) {
//					if($columnData->program_id == 3 || $columnData->program_id == 6){
//
//						$f = Finished::where('user_id', $columnData->uid)->where('program_id', $columnData->program_id)->first();
//
//						if($f){
//							return $stages[$f->stage];
//						} else {
//							return '';
//						}
//					} else {
//						return '';
//					}
//				},
//
//			],
			"uname" => [
				"label" => "Логин",
				// "search" => [
				// 	"enabled" => true,
				// ],
				"filter" => [
					"enabled" => true,
					"operator" => "=",
				],
			],
//			"exist_pin" => [
//				"label" => "Существующий ПИН",
//				// "search" => [
//				// 	"enabled" => true,
//				// ],
//				"filter" => [
//					"enabled" => false,
//				],
//				"data" => function ($columnData, $columnName) {
//					/*251119
//					$m = Config::get('matrix.get');
//					$type = $m[$columnData->program_id][0];
//					$connection = 'mysql_prod_' . $type;
//					$user = User::on($connection)->find($columnData->uid);
//					return $user->screch;*/
//				},
//			],
//			"registration_date" => [
//				"label" => "Дата регистрации логина",
//				"filter" => [
//					"enabled" => false,
//				],
//				"data" => function ($columnData, $columnName) {
//					/*251119
//					$m = Config::get('matrix.get');
//					$type = $m[$columnData->program_id][0];
//					$connection = 'mysql_prod_' . $type;
//					$user = User::on($connection)->find($columnData->uid);
//					return gmdate("Y-m-d", $user->reg_date);*/
//				},
//			],
			"firstname" => [
				"label" => "Имя",
				// "search" => [
				// 	"enabled" => true,
				// ],
				"filter" => [
					"enabled" => true,
					"operator" => "=",
				],
			],
			"lastname" => [
				"label" => "Фамилия",
				// "search" => [
				// 	"enabled" => true,
				// ],
				"filter" => [
					"enabled" => true,
					"operator" => "=",
				],
			],
			"patronic" => [
				"label" => "Отчество",
				// "search" => [
				// 	"enabled" => true,
				// ],
				"filter" => [
					"enabled" => true,
					"operator" => "=",
				],
			],
			"iin" => [
				"label" => "ИНН",
				// "search" => [
				// 	"enabled" => true,
				// ],
				"filter" => [
					"enabled" => true,
					"operator" => "=",
				],
			],
			"bank_name" => [
				"label" => "Наименование банка",
				// "search" => [
				// 	"enabled" => true,
				// ],
				"filter" => [
					"enabled" => true,
					"operator" => "=",
				],
			],
			"BIK" => [
				"label" => "BIK",
				// "search" => [
				// 	"enabled" => true,
				// ],
				"filter" => [
					"enabled" => true,
					"operator" => "=",
				],
			],
			"bill_num" => [
				"label" => "Номер Счета",
				// "search" => [
				// 	"enabled" => true,
				// ],
				"filter" => [
					"enabled" => true,
					"operator" => "=",
				],
			],
//			"pins" => [
//				"label" => "Переходные пин коды",
//				"raw" => true,
//				"filter" => [
//					"enabled" => true,
//					'type' => 'select',
//					'data' => $statuses,
//				],
//				"data" => function ($columnData, $columnName) {
//					$f = Finished::where('user_id', $columnData->uid)->where('program_id', $columnData->program_id)->first();
//					$str = '';
//					$m = Config::get('matrix.get');
//
//					if(!$f){
//						return '';
//					}
//
//					if ($f->pin) {
//						$data = json_decode($f->pin, true);
//						$pin = \App\Models\Pins\StartPin::where('id', $data['pin'])->first();
//						$type = $m[$pin->program][0];
//						$connection = 'mysql_prod_' . $type;
//						//$user = User::on($connection)->where('screch', $pin->pin)->first();
//						$user = User::on($connection)->where('screch', 0)->first();
//						$str .= $pin->pin;
//						if ($user) {
//							$str .= '<pre style="background: #c9c9c9; padding: 5px;">';
//							$str .= 'Логин: ' . $user->name . '<br/>';
//							$str .= 'ФИО: ' . $user->fullname . '<br/>';
//							$str .= 'Дата регистрации: ' . gmdate("Y-m-d", $user->reg_date) . '<br/>';
//							$str .= '</pre>';
//						}
//					}
//
//					if ($f->pin2) {
//						$data = json_decode($f->pin2, true);
//						$pin = \App\Models\Pins\StartPin::where('id', $data['pin'])->first();
//
//						$type = $m[$data['program_id']][0];
//						$connection = 'mysql_prod_' . $type;
//						//$user = User::on($connection)->where('screch', $pin->pin)->first();
//						$user = User::on($connection)->where('screch', 0)->first();
//						$str .= '<br/>' . $pin->pin;
//						if ($user) {
//							$str .= '<pre style="background: #c9c9c9; padding: 5px;">';
//							$str .= '<br/> Логин: ' . $user->name . '<br/>';
//							$str .= '<br/> ФИО: ' . '<b>' . $user->fullname . '</b><br/>';
//							$str .= '<br/> Дата регистрации: ' . '<b>' . gmdate("Y-m-d", $user->reg_date) . '</b><br/>';
//							$str .= '</pre>';
//						}
//					}
//
//					if ($f->pin3) {
//						$data = json_decode($f->pin3, true);
//						$pin = \App\Models\Pins\StartPin::where('id', $data['pin'])->first();
//						$type = $m[$data['program_id']][0];
//						$connection = 'mysql_prod_' . $type;
//						//$user = User::on($connection)->where('screch', $pin->pin)->first();
//						$user = User::on($connection)->where('screch', 0)->first();
//						$str .= '<br/>' . $pin->pin;
//						if ($user) {
//							$str .= '<pre style="background: #c9c9c9; padding: 5px;">';
//							$str .= '<br/> Логин: ' . $user->name . '<br/>';
//							$str .= '<br/> ФИО: ' . '<b>' . $user->fullname . '</b><br/>';
//							$str .= '<br/> Дата регистрации: ' . '<b>' . gmdate("Y-m-d", $user->reg_date) . '</b><br/>';
//							$str .= '</pre>';
//						}
//					}
//					return $str;
//				},
//			],
			"fact_date" => [
				"label" => "Дата фактической отправки вознаграждения",
				"filter" => [
					"enabled" => true,
					"type" => "date",
					"operator" => "<=",
				],
			],
			"created_at" => [
				"label" => "Создана",
				"sort" => false,
				"date" => "true",
				"filter" => [
					"enabled" => true,
					"type" => "date",
					"operator" => "<=",
				],
			],
			"status_id" => [
				"label" => "Статус",
				"raw" => true,
				"filter" => [
					"enabled" => true,
					'type' => 'select',
					'data' => $statuses,
				],
				"data" => function ($columnData, $columnName) use ($statuses) {
					return $statuses[$columnData->status_id];
				},
			],

		];
	}

	/**
	 * Set the links/routes. This are referenced using named routes, for the sake of simplicity
	 *
	 * @return void
	 */
	public function setRoutes() {
		// searching, sorting and filtering
		$this->setIndexRouteName('bonuses.index');

		// crud support
		$this->setCreateRouteName('bonusrequests.create_admin');
		$this->setViewRouteName('bonuses.show');
		$this->setDeleteRouteName('bonuses.destroy');

		// default route parameter
		$this->setDefaultRouteParameter('id');
	}

	/**
	 * Return a closure that is executed per row, to render a link that will be clicked on to execute an action
	 *
	 * @return Closure
	 */
	public function getLinkableCallback(): Closure {
		return function ($gridName, $item) {
			return route($this->getViewRouteName(), [$gridName => $item->id]);
		};
	}

	/**
	 * Configure rendered buttons, or add your own
	 *
	 * @return void
	 */
	public function configureButtons() {
		$this->editRowButton('delete', [
			'name' => 'Удалить',
			'pjaxEnabled' => false,
			'position' => 4,
			'url' => function ($gridName, $gridItem) {
				return route('bonus.destroy', ['id' => $gridItem->id]);
			},
			'dataAttributes' => [
				'method' => 'delete',
				'trigger-confirm' => true,
			],
		]);

		$this->editToolbarButton('create', [
			'name' => 'Создать',
		]);

		$this->editToolbarButton('refresh', [
			'name' => 'Обновить',
		]);

		$this->editRowButton('view', [
			'name' => 'Смотреть',
			'url' => function ($gridName, $gridItem) {
				return route('bonus.show', ['id' => $gridItem->id]);
			},
		]);
	}

	/**
	 * Returns a closure that will be executed to apply a class for each row on the grid
	 * The closure takes two arguments - `name` of grid, and `item` being iterated upon
	 *
	 * @return Closure
	 */
	public function getRowCssStyle(): Closure {
		return function ($gridName, $item) {
			// e.g, to add a success class to specific table rows;
			// return $item->id % 2 === 0 ? 'table-success' : '';
			return "";
		};
	}
}
