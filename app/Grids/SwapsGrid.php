<?php

namespace App\Grids;

use Closure;
use Leantony\Grid\Grid;
use Config;

class SwapsGrid extends Grid implements SwapsGridInterface
{
    /**
     * The name of the grid
     *
     * @var string
     */
    protected $name = 'Swaps';

    /**
     * List of buttons to be generated on the grid
     *
     * @var array
     */
    protected $buttonsToGenerate = [
        'view',
        'delete',
        'refresh',
    ];

    /**
     * Specify if the rows on the table should be clicked to navigate to the record
     *
     * @var bool
     */
    protected $linkableRows = false;

    /**
    * Set the columns to be displayed.
    *
    * @return void
    * @throws \Exception if an error occurs during parsing of the data
    */
    public function setColumns()
    {
    	$progs = [];

		foreach (Config::get('matrix.get') as $key => $matrix) {
			$progs[$key] = $matrix[1];
		}
		$statuses = [0 => 'Новый', 1 => 'Одобрен', 2 => 'Отклонен'];
		$statuses_badges = [0 => '<span class="badge badge-success">Новый</span>', 1 => '<span class="badge badge-primary">Одобрен</span>', 2 => '<span class="badge badge-danger">Отклонен</span>'];
		$types = [1 => 'По заявлению', 2 => 'По приказу №7', 3 => 'Замена спонсора'];
		$reasons = [1 => 'По приказу', 2 => 'По заявлению', '' => ''];


        $this->columns = [
		    "id" => [
		        "label" => "ID",
		        "filter" => [
		            "enabled" => true,
		            "operator" => "="
		        ],
		        "styles" => [
		            "column" => "grid-w-10"
		        ]
		    ],
		    "login" => [
				"label" => 'Логин',
		        "search" => [
		            "enabled" => true
		        ],
		        "filter" => [
		            "enabled" => true,
		            "operator" => "="
		        ]
		    ],
		    "program_id" => [
				"label" => 'Программа',
				"export" => false,
				"filter" => [
					"enabled" => true,
					'type' => 'select',
					'data' => $progs,
				],
				"data" => function ($columnData, $columnName) use ($progs) {
					return $progs[$columnData->program_id];
				},
		    ],
		    "login2" => [
				"label" => 'Заменить на',
		        "search" => [
		            "enabled" => true
		        ],
		        "filter" => [
		            "enabled" => true,
		            "operator" => "="
		        ]
		    ],
		    "type" => [
				"label" => 'Тип',
		        "search" => [
		            "enabled" => true
		        ],
				"filter" => [
					"enabled" => true,
					'type' => 'select',
					'data' => $types,
				],
		        "data" => function ($columnData, $columnName) use ($types) {
					return $types[$columnData->type];
				},
		    ],
		    "reason" => [
		    	"label" => 'Тип',
		        "search" => [
		            "enabled" => true
		        ],
				"filter" => [
					"enabled" => true,
					'type' => 'select',
					'data' => $reasons,
				],
		        "data" => function ($columnData, $columnName) use ($reasons) {
					return $reasons[$columnData->reason];
				},
		    ],
		    "status_id" => [
		    	"label" => 'Статус',
		    	"raw" => true,
				"filter" => [
					"enabled" => true,
					'type' => 'select',
					'data' => $statuses,
				],
		        "data" => function ($columnData, $columnName) use ($statuses_badges) {
					return $statuses_badges[$columnData->status_id];
				},
		        "export" => false
		    ],
		    "created_at" => [
		    	"label" => 'Создана',
		        "sort" => false,
		        "date" => "true",
		        "filter" => [
		            "enabled" => true,
		            "type" => "date",
		            "operator" => "<="
		        ],
		        "data" => function ($columnData, $columnName) use ($statuses) {
					return $columnData->created_at;
				},
		    ]
		];
    }

    /**
     * Set the links/routes. This are referenced using named routes, for the sake of simplicity
     *
     * @return void
     */
    public function setRoutes()
    {
        // searching, sorting and filtering
        $this->setIndexRouteName('swaps.index');

        // crud support
        $this->setCreateRouteName('swaps.create');
        $this->setViewRouteName('swaps.show');
        $this->setDeleteRouteName('swaps.destroy');

        // default route parameter
        $this->setDefaultRouteParameter('id');
    }

    /**
    * Return a closure that is executed per row, to render a link that will be clicked on to execute an action
    *
    * @return Closure
    */
    public function getLinkableCallback(): Closure
    {
        return function ($gridName, $item) {
            return route($this->getViewRouteName(), [$gridName => $item->id]);
        };
    }

    /**
    * Configure rendered buttons, or add your own
    *
    * @return void
    */
    public function configureButtons()
    {
        // call `addRowButton` to add a row button
        // call `addToolbarButton` to add a toolbar button
        // call `makeCustomButton` to do either of the above, but passing in the button properties as an array

        // call `editToolbarButton` to edit a toolbar button
        // call `editRowButton` to edit a row button
        // call `editButtonProperties` to do either of the above. All the edit functions accept the properties as an array
		$this->editToolbarButton('refresh', [
            'name' => 'Обновить',
        ]);        

        $this->editRowButton('view', [
            'name' => 'Смотреть',
        ]);   
    }

    /**
    * Returns a closure that will be executed to apply a class for each row on the grid
    * The closure takes two arguments - `name` of grid, and `item` being iterated upon
    *
    * @return Closure
    */
    public function getRowCssStyle(): Closure
    {
        return function ($gridName, $item) {
            // e.g, to add a success class to specific table rows;
            // return $item->id % 2 === 0 ? 'table-success' : '';
            return "";
        };
    }
}