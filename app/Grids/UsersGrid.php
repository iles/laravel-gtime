<?php

namespace App\Grids;

use Closure;
use Leantony\Grid\Grid;

class UsersGrid extends Grid implements UsersGridInterface
{
    /**
     * The name of the grid
     *
     * @var string
     */
    protected $name = 'Patrners';

    /**
     * List of buttons to be generated on the grid
     *
     * @var array
     */
    protected $buttonsToGenerate = [
        'view',
        'delete',
        'refresh',
        'ladder',
    ];

    /**
     * Specify if the rows on the table should be clicked to navigate to the record
     *
     * @var bool
     */
    protected $linkableRows = false;

    /**
    * Set the columns to be displayed.
    *
    * @return void
    * @throws \Exception if an error occurs during parsing of the data
    */
    public function setColumns()
    {
        $this->columns = [

            "user_id" => [
                "label" => 'ID',
                "search" => [
                    "enabled" => true
                ],
                "filter" => [
                    "enabled" => true,
                    "operator" => "="
                ]
            ],                
            "name" => [
                "label" => 'Логин',
                "search" => [
                    "enabled" => true
                ],
                "filter" => [
                    "enabled" => true,
                    "operator" => "="
                ]
            ],            
            "screch" => [
                "label" => 'Пин код',
                "search" => [
                    "enabled" => true
                ],
                "filter" => [
                    "enabled" => true,
                    "operator" => "like"
                ]
            ],		    
            "familiya" => [
                "label" => 'ФИО',
                "search" => [
                    "enabled" => true
                ],
                "filter" => [
                    "enabled" => true,
                    "operator" => "like"
                ]
            ],             
            "imya" => [
                "label" => 'Страна/Город',
                "search" => [
                    "enabled" => true
                ],
                "filter" => [
                    "enabled" => true,
                    "operator" => "like"
                ]
            ],             
            "otchestvo" => [
                "label" => 'Дата рождения',
                "search" => [
                    "enabled" => true
                ],
                "filter" => [
                    "enabled" => true,
                    "operator" => "like"
                ]
            ],             
            "strana" => [
                "label" => 'ИИН',
                "search" => [
                    "enabled" => true
                ],
                "filter" => [
                    "enabled" => true,
                    "operator" => "="
                ]
            ],              
            "pol" => [
                "label" => 'Телефон',
                "search" => [
                    "enabled" => true
                ],
                "filter" => [
                    "enabled" => true,
                    "operator" => "="
                ]
            ],             
            "ref_num" => [
                "label" => 'Звезды',
                "search" => [
                    "enabled" => true
                ],
                "filter" => [
                    "enabled" => true,
                    "operator" => "="
                ]
            ],            
            "signature" => [
                "label" => 'Приписка',
		        "search" => [
		            "enabled" => true
		        ],
		        "filter" => [
		            "enabled" => true,
		            "operator" => "="
		        ]
		    ],            
		];
    }

    /**
     * Set the links/routes. This are referenced using named routes, for the sake of simplicity
     *
     * @return void
     */
    public function setRoutes()
    {
        // searching, sorting and filtering
        $this->setIndexRouteName('sponsors.index');

        // crud support
        $this->setCreateRouteName('sponsors.create');
        $this->setViewRouteName('sponsors.show');
        $this->setDeleteRouteName('sponsors.destroy');

        // default route parameter
        $this->setDefaultRouteParameter('id');
    }

    /**
    * Return a closure that is executed per row, to render a link that will be clicked on to execute an action
    *
    * @return Closure
    */
    public function getLinkableCallback(): Closure
    {
        return function ($gridName, $item) {
            return route($this->getViewRouteName(), [$gridName => $item->id]);
        };
    }

    /**
    * Configure rendered buttons, or add your own
    *
    * @return void
    */
    public function configureButtons()
    {
        // call `addRowButton` to add a row button
        // call `addToolbarButton` to add a toolbar button
        // call `makeCustomButton` to do either of the above, but passing in the button properties as an array

        // call `editToolbarButton` to edit a toolbar button
        // call `editRowButton` to edit a row button
        // call `editButtonProperties` to do either of the above. All the edit functions accept the properties as an array
        $this->editRowButton('view', [
            'name' => 'Смотреть',
            'url' => function ($gridName, $item) {
                return $this->getViewUrl([
                    'id' => $item->user_id, 
                ]);             
            },
        ]);          

        $this->makeCustomButton([
            'name' => 'ladder',
            'label' => 'лестница',
            'icon' => 'fa-eye',
            'position' => 2,
            'class' => 'btn btn-outline-primary btn-sm grid-row-button',
            'showModal' => true,
            'gridId' => $this->getId(),
            'renderIf' => function(){
                return true;
            },
            'title' => 'edit record',
            'url' => function($gridName, $gridItem) {
                return route('matrix.user', ['id' => $gridItem->matrix_id]);
            },
        ], static::$TYPE_ROW);


        $this->editToolbarButton('refresh', [
            'name' => 'Обновить',
        ]);            



        $this->editRowButton('delete', [
            'name' => 'Удалить',
            'url' => function ($gridName, $item) {
                return $this->getDeleteUrl([
                    'id' => $item->user_id, 
                ]);             
            },
        ]); 

        $this->editRowButton('ladder', [
            'name' => 'Лестница',
        ]);  
    }

    /**
    * Returns a closure that will be executed to apply a class for each row on the grid
    * The closure takes two arguments - `name` of grid, and `item` being iterated upon
    *
    * @return Closure
    */
    public function getRowCssStyle(): Closure
    {
        return function ($gridName, $item) {
            // e.g, to add a success class to specific table rows;
            // return $item->id % 2 === 0 ? 'table-success' : '';
            return "";
        };
    }
}
