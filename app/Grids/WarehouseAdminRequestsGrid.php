<?php

namespace App\Grids;

use App\User;
use Closure;
use Config;
use Leantony\Grid\Grid;

class WarehouseAdminRequestsGrid extends Grid implements WarehouseRequestsGridInterface {
	/**
	 * The name of the grid
	 *
	 * @var string
	 */
	protected $name = 'Личный кабинет администрации склада';

	/**
	 * List of buttons to be generated on the grid
	 *
	 * @var array
	 */
	protected $buttonsToGenerate = [
		'custom', 'view', 'refresh',
	];

	/**
	 * Specify if the rows on the table should be clicked to navigate to the record
	 *
	 * @var bool
	 */
	protected $linkableRows = false;

	/**
	 * Set the columns to be displayed.
	 *
	 * @return void
	 * @throws \Exception if an error occurs during parsing of the data
	 */
	public function setColumns() {
		$progs = [];

		foreach (Config::get('matrix.get') as $key => $matrix) {
			$progs[$key] = $matrix[1];
		}

		$options = ['nabor1' => 'nabor1', 'nabor2' => 'nabor2', 'nabor3' => 'nabor3', 'nabor4' => 'nabor4'];
		$statuses = [0 => 'Новый', 1 => 'Одобрен'];

		$managers = User::role('warehouse')->get()->pluck('name', 'id');

		$this->columns = [
			// "checks" => [
			// 	"label" => "",
			// 	"data" => function ($columnData, $columnName) {
			// 		return '<input type="checkbox" value="' . $columnData->id . '" />';
			// 	},
			// 	"raw" => true,
			// ],
			"id" => [
				"label" => "ID",
				"filter" => [
					"enabled" => true,
					"operator" => "=",
				],
			],
			"manager_id" => [
				"label" => "Региональный менеджер",
				"filter" => [
					"enabled" => true,
					'type' => 'select',
					'data' => $managers,
				],
				"data" => function ($columnData, $columnName) use ($managers) {
					if ($columnData->manager_id == null) {
						return '';
					}
                   if (isset($managers[$columnData->manager_id])){
					return $managers[$columnData->manager_id];
                   }
                   return '';
				},
			],
			"program" => [
				"label" => "Программа",
				"search" => [
					"enabled" => true,
				],
				"filter" => [
					"enabled" => true,
					'type' => 'select',
					'data' => $progs,
				],
				"data" => function ($columnData, $columnName) use ($progs) {
					if ($columnData->program == null) {
						return '';
					}
					return $progs[$columnData->program];
				},
			],
			"uname" => [
				"label" => "Логин",
				"search" => [
					"enabled" => true,
				],
				"filter" => [
					"enabled" => true,
					"operator" => "=",
				],
			],
			"user_fullname" => [
				"label" => "ФИО",
				"search" => [
					"enabled" => true,
				],
				"filter" => [
					"enabled" => true,
					"operator" => "=",
				],
			],
			"purchace_date" => [
				"label" => "Дата Покупки",
				"filter" => [
					"enabled" => true,
					'type' => 'date',
					"operator" => "=",
				],
			],
			"checkout_name" => [
				"label" => "Имя",
				"search" => [
					"enabled" => true,
				],
				"filter" => [
					"enabled" => true,
					"operator" => "=",
				],
			],
			"checkout_surname" => [
				"label" => "Фамилия",
				"search" => [
					"enabled" => true,
				],
				"filter" => [
					"enabled" => true,
					"operator" => "=",
				],
			],
			"checkout_patronic" => [
				"label" => "Отчество",
				"search" => [
					"enabled" => true,
				],
				"filter" => [
					"enabled" => true,
					"operator" => "=",
				],
			],
			"checkout_date" => [
				"label" => "Когда забрал",
				"filter" => [
					"enabled" => true,
					'type' => 'date',
					"operator" => "=",
				],
			],
			"option" => [
				"label" => "Набор",
				"search" => [
					"enabled" => true,
				],
				"filter" => [
					"enabled" => true,
					'type' => 'select',
					'data' => $options,
				],
			],
			"created_at" => [
				"label" => 'Создан',
				"sort" => false,
				"date" => "true",
				"filter" => [
					"enabled" => true,
					"type" => "date",
					"operator" => "<=",
				],
			],
			"status_id" => [
				"label" => "Статус",
				"filter" => [
					"enabled" => true,
					'type' => 'select',
					'data' => $statuses,
				],
				"data" => function ($columnData, $columnName) use ($statuses) {
					return $statuses[$columnData->status_id];
				},
			],
		];
    }

	/**
	 * Set the links/routes. This are referenced using named routes, for the sake of simplicity
	 *
	 * @return void
	 */
	public function setRoutes() {
		// searching, sorting and filtering
		$this->setIndexRouteName('warehouse.admin_index');

		// crud support
		$this->setCreateRouteName('warehouse-admin.create');
		$this->setViewRouteName('warehouse-admin.show');
		$this->setDeleteRouteName('warehouse-admin.destroy');

		// default route parameter
		$this->setDefaultRouteParameter('id');
	}

	/**
	 * Return a closure that is executed per row, to render a link that will be clicked on to execute an action
	 *
	 * @return Closure
	 */
	public function getLinkableCallback(): Closure {
		return function ($gridName, $item) {
			return route($this->getViewRouteName(), [$gridName => $item->id]);
		};
	}

	/**
	 * Configure rendered buttons, or add your own
	 *
	 * @return void
	 */
	public function configureButtons() {
		// call `addRowButton` to add a row button
		// call `addToolbarButton` to add a toolbar button
		// call `makeCustomButton` to do either of the above, but passing in the button properties as an array

		$this->editRowButton('view', [
			'name' => 'Одобрить',
			'icon' => 'fa-user',
			'pjaxEnabled' => false,
			'showModal' => false,
			'renderIf' => function ($gridName, $item) {
				// assuming there exists a `user_id` field on your data
				return $item->status_id === 0;
			},
			'url' => function ($gridName, $item) {
				return route('warehouse-admin.aprove', ['id' => $item->id]);
			},
		]);

		$this->makeCustomButton([
			'name' => 'check',
			'label' => 'лестница',
			'icon' => 'fa-eye',
			'position' => 2,
			'class' => 'btn btn-outline-primary btn-sm grid-row-button',
			'showModal' => true,
			'gridId' => $this->getId(),
			'renderIf' => function () {
				return true;
			},
			'title' => 'edit record',
			'url' => function ($gridName, $gridItem) {
				return route('warehouse.admin_view', ['id' => $gridItem->id]);
			},
		], static::$TYPE_ROW);

		// $this->makeCustomButton([
		//     'icon' => 'fa-user',
		//     'name' => 'custom',
		//     'class' => 'btn btn-info',
		//     'url' => function($gridName, $gridItem) {
		//         return route('warehouse-admin.aprove', [$gridName => $gridItem->id]);
		//     },
		// ]);

		// call `editToolbarButton` to edit a toolbar button
		// call `editRowButton` to edit a row button
		// call `editButtonProperties` to do either of the above. All the edit functions accept the properties as an array
	}

	/**
	 * Returns a closure that will be executed to apply a class for each row on the grid
	 * The closure takes two arguments - `name` of grid, and `item` being iterated upon
	 *
	 * @return Closure
	 */
	public function getRowCssStyle(): Closure {
		return function ($gridName, $item) {
			// e.g, to add a success class to specific table rows;
			// return $item->id % 2 === 0 ? 'table-success' : '';
			return "";
		};
	}
}