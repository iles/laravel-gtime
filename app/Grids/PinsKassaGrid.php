<?php

namespace App\Grids;

use Closure;
use Leantony\Grid\Grid;
use Config;

class PinsKassaGrid extends Grid implements PinsGridInterface
{
    /**
     * The name of the grid
     *
     * @var string
     */
    protected $name = 'Pins';

    /**
     * List of buttons to be generated on the grid
     *
     * @var array
     */
    protected $buttonsToGenerate = [
        'create',
        'view',
        'delete',
        'refresh',
        'print',
    ];

    /**
     * Specify if the rows on the table should be clicked to navigate to the record
     *
     * @var bool
     */
    protected $linkableRows = false;

    /**
    * Set the columns to be displayed.
    *
    * @return void
    * @throws \Exception if an error occurs during parsing of the data
    */
    public function setColumns()
    {

        $progs = [];
        
        foreach (Config::get('matrix.get') as $key => $matrix){
            $progs[$key] = $matrix[1];
        }
            $statuses = [0 => 'Новый', 1 => 'Одобрен', 2 => 'Отклонен', 3 => 'Активирован' ];


        $this->columns = [
            "id" => [
                "label" => "ID",
                "filter" => [
                    "enabled" => true,
                    "operator" => "="
                ],
            ],
            "uname" => [
                "label" => "Имя",
                "sort" => false,
                "filter" => [
                    "enabled" => true,
                    "operator" => '='

                ]
            ],            
            "bill" => [
                "label" => "Номер квитанции",
                "sort" => false,
                "filter" => [
                    "enabled" => true,
                    "operator" => '='

                ]
            ],            
            "iin" => [
                "label" => "ИИН",
                "sort" => false,
                "filter" => [
                    "enabled" => true,
                    "operator" => '='

                ]
            ],            
            "summ" => [
                "label" => "Сумма",
                "sort" => false,
                "filter" => [
                    "enabled" => true,
                    "operator" => '='

                ]
            ],            
            "skype" => [
                "label" => "Skype",
                "sort" => false,
                "filter" => [
                    "enabled" => true,
                    "operator" => '='

                ]
            ],            
            "pin" => [
                "label" => "Пин",
                "sort" => false,
                "raw" => true,
                "filter" => [
                    "enabled" => false,
                ],
                "data" => function ($columnData, $columnName) {
                    $startPins = \App\Models\Pins\StartPin::where('request_id', $columnData->id)->get();
                    $autoPins = \App\Models\Pins\AutoPin::where('request_id', $columnData->id)->get();
                    $mainPins = \App\Models\Pins\MainPin::where('request_id', $columnData->id)->get(); 
                    $AcummulativePins = \App\Models\Pins\AcummulativePin::where('request_id', $columnData->id)->get(); 
                    $AcummulativePlusPins = \App\Models\Pins\Acummulative_plusPin::where('request_id', $columnData->id)->get(); 
                    $VipPins = \App\Models\Pins\VipPin::where('request_id', $columnData->id)->get(); 
                    $FastPins = \App\Models\Pins\FastPin::where('request_id', $columnData->id)->get(); 
                    $str = '';
                    if( count($startPins) ) {
                        $str .= '<b>Старт:</b><br/>';
                        foreach ($startPins as $key => $value) {
                            $str .= $value->pin.'<br/>'; 
                        }
                    }                    
                    if( count($autoPins) ){
                        $str .= '<b>Авто:</b><br/>';
                        foreach ($autoPins as $key => $value) {
                            $str .= $value->pin.'<br/>'; 
                        }
                    }                    
                    if( count($AcummulativePins) ){
                        $str .= '<b>Накопительная:</b><br/>';
                        foreach ($AcummulativePins as $key => $value) {
                            $str .= $value->pin.'<br/>'; 
                        }
                    }                    
                    if( count($AcummulativePlusPins) ){
                        $str .= '<b>Накопительная +:</b><br/>';
                        foreach ($AcummulativePlusPins as $key => $value) {
                            $str .= $value->pin.'<br/>'; 
                        }
                    }                    
                    if( count($VipPins) ){
                        $str .= '<b>VIP:</b><br/>';
                        foreach ($VipPins as $key => $value) {
                            $str .= $value->pin.'<br/>'; 
                        }
                    }                    
                    if( count($FastPins) ){
                        $str .= '<b>Fast:</b><br/>';   
                        foreach ($FastPins as $key => $value) {
                            $str .= $value->pin.'<br/>'; 
                        }
                    }
                    return $str;
                },
            ],
            "status_id" => [
                "label" => "Статус заявки ",
                "filter" => [
                    "enabled" => true,
                    'type' => 'select',
                    'data' => $statuses
                ],
                "data" => function ($columnData, $columnName) use ($statuses) {
                    return $statuses[$columnData->status_id];
                },
            ],            
            "created_at" => [
                "label" => "Создана",
                "sort" => false,
                "date" => "true",
                "filter" => [
                    "enabled" => true,
                    "type" => "date",
                    "operator" => "<="
                ],
                "data" => function ($columnData, $columnName){
                    return $columnData->created_at;
                },
            ],               
            "note" => [
                "label" => "Примечание",
                "sort" => false,
                "filter" => [
                    "enabled" => false,
                ],
            ],                  
            // "active_at" => [
            //     "label" => "Активирован",
            //     "sort" => false,
            //     "date" => "true",
            //     "data" => function ($columnData, $columnName) use ($statuses) {
            //         return $columnData->active_at;
            //     },
            //     "filter" => [
            //         "enabled" => true,
            //         "type" => "date",
            //         "operator" => "<="
            //     ]
            // ],             
            // "expired_at" => [
            //     "label" => "Действителен до",
            //     "sort" => false,
            //     "date" => "true",
            // "data" => function ($columnData, $columnName) use ($statuses) {
            //         return $columnData->expired_at;
            //     },
            //     "filter" => [
            //         "enabled" => true,
            //         "type" => "date",
            //         "operator" => "<="
            //     ]
            // ],      
        ];
    }

    /**
     * Set the links/routes. This are referenced using named routes, for the sake of simplicity
     *
     * @return void
     */
    public function setRoutes()
    {
        // searching, sorting and filtering
        $this->setIndexRouteName('kassa.index');

        // crud support
        $this->setCreateRouteName('pins.create.kassa');
        $this->setViewRouteName('pins.show.kassa');
        $this->setDeleteRouteName('pins.destroy');
        // default route parameter
        $this->setDefaultRouteParameter('id');
    }

    /**
    * Return a closure that is executed per row, to render a link that will be clicked on to execute an action
    *
    * @return Closure
    */
    public function getLinkableCallback(): Closure
    {
        return function ($gridName, $item) {
            return route($this->getViewRouteName(), [$gridName => $item->id]);
        };
    }

    /**
    * Configure rendered buttons, or add your own
    *
    * @return void
    */
    public function configureButtons()
    {    
        $this->editToolbarButton('refresh', [
            'name' => 'Обновить',
        ]);        

        $this->editRowButton('view', [
            'name' => 'Смотреть',
        ]);        

        $this->editRowButton('delete', [
            'name' => 'Удалить',
            'url' => function ($gridName, $item) {
                return '/admin/pins/destroy/'.$item->id;
            },
        ]);        

        $this->editToolbarButton('create', [
            'name' => 'Создать',
        ]); 

        $this->makeCustomButton([
            'name' => 'print',
            'label' => 'лестница',
            'icon' => 'fa-eye',
            'position' => 2,
            'class' => 'btn btn-outline-primary btn-sm grid-row-button print',
            'showModal' => false,
            'gridId' => $this->getId(),
            'renderIf' => function(){
                return true;
            },
            'title' => 'edit record',
            'url' => function($gridName, $gridItem) {
                return route('pins.print', ['pin' => $gridItem->id]);
            },
        ], static::$TYPE_ROW);

        $this->editRowButton('print', [
            'name' => 'Печать',
        ]);  

        // call `addRowButton` to add a row button
        // call `addToolbarButton` to add a toolbar button
        // call `makeCustomButton` to do either of the above, but passing in the button properties as an array

        // call `editToolbarButton` to edit a toolbar button
        // call `editRowButton` to edit a row button
        // call `editButtonProperties` to do either of the above. All the edit functions accept the properties as an array
    }

    /**
    * Returns a closure that will be executed to apply a class for each row on the grid
    * The closure takes two arguments - `name` of grid, and `item` being iterated upon
    *
    * @return Closure
    */
    public function getRowCssStyle(): Closure
    {
        return function ($gridName, $item) {
            // e.g, to add a success class to specific table rows;
            // return $item->id % 2 === 0 ? 'table-success' : '';
            return "";
        };
    }
}