<?php

namespace App\Grids;

use Closure;
use Config;
use Leantony\Grid\Grid;

class BonusRequestsGrid extends Grid implements BonusRequestsGridInterface {
	/**
	 * The name of the grid
	 *
	 * @var string
	 */
	protected $name = 'Заявки на вознаграждение';

	/**
	 * List of buttons to be generated on the grid
	 *
	 * @var array
	 */
	protected $buttonsToGenerate = [
		'create',
		'view',
		'delete',
		'refresh',
		'edit',
	];

	/**
	 * Specify if the rows on the table should be clicked to navigate to the record
	 *
	 * @var bool
	 */
	protected $linkableRows = false;

	/**
	 * Set the columns to be displayed.
	 *
	 * @return void
	 * @throws \Exception if an error occurs during parsing of the data
	 */
	public function setColumns() {
		$progs = [];
		$statuses = [1 => 'Новый', 1 => 'Оплачен', 4 => 'Неоплачен'];
		$options = ['nabor1' => 'nabor1', 'nabor2' => 'nabor2', 'nabor3' => 'nabor3', 'nabor4' => 'nabor4'];

		foreach (Config::get('matrix.get') as $key => $matrix) {
			$progs[$key] = $matrix[1];
		}

		$this->columns = [
			"id" => [
				"label" => "ID",
				"filter" => [
					"enabled" => true,
					"operator" => "=",
				],
			],
			"uname" => [
				"label" => "Логин",
				"search" => [
					"enabled" => true,
				],
				"filter" => [
					"enabled" => true,
					"operator" => "=",
				],
			],
			"firstname" => [
				"label" => "Имя",
				"search" => [
					"enabled" => true,
				],
				"filter" => [
					"enabled" => true,
					"operator" => "=",
				],
			],
			"lastname" => [
				"label" => "Фамилия",
				"search" => [
					"enabled" => true,
				],
				"filter" => [
					"enabled" => true,
					"operator" => "=",
				],
			],
			"patronic" => [
				"label" => "Отчество",
				"search" => [
					"enabled" => true,
				],
				"filter" => [
					"enabled" => true,
					"operator" => "=",
				],
			],
			"fact_date" => [
				"label" => "Дата фактической отправки вознаграждения",
				"filter" => [
					"enabled" => true,
					"type" => "date",
					"operator" => "<=",
				],
			],
			"checkout_date" => [
				"label" => "Дата выдачи продукции",
				"filter" => [
					"enabled" => true,
					"operator" => "=",
				],
			],
			"summ" => [
				"label" => "Сумма",
				"search" => [
					"enabled" => true,
				],
				"filter" => [
					"enabled" => true,
					"operator" => "=",
				],
			],
			"iin" => [
				"label" => "ИИН",
				"search" => [
					"enabled" => true,
				],
				"filter" => [
					"enabled" => true,
					"operator" => "=",
				],
			],
			"country" => [
				"label" => "Страна",
				"search" => [
					"enabled" => true,
				],
				"filter" => [
					"enabled" => true,
					"operator" => "=",
				],
			],
			"bank_name" => [
				"label" => "Наименование банка",
				"search" => [
					"enabled" => true,
				],
				"filter" => [
					"enabled" => true,
					"operator" => "=",
				],
			],
			"BIK" => [
				"label" => "BIK",
				"search" => [
					"enabled" => true,
				],
				"filter" => [
					"enabled" => true,
					"operator" => "=",
				],
			],
			"bill_num" => [
				"label" => "Номер счета",
				"search" => [
					"enabled" => true,
				],
				"filter" => [
					"enabled" => true,
					"operator" => "=",
				],
			],
			"status_id" => [
				"label" => "Статус",
				"filter" => [
					"enabled" => true,
					'type' => 'select',
					'data' => $statuses,
				],
				"data" => function ($columnData, $columnName) use ($statuses) {
					return $statuses[$columnData->status_id];
				},
			],
			"created_at" => [
				"label" => "Создана",
				"sort" => false,
				"date" => "true",
				"filter" => [
					"enabled" => true,
					"type" => "date",
					"operator" => "<=",
				],
				"data" => function ($columnData, $columnName) use ($statuses) {
					return $columnData->created_at;
				},
			],
		];
	}

	/**
	 * Set the links/routes. This are referenced using named routes, for the sake of simplicity
	 *
	 * @return void
	 */
	public function setRoutes() {
		// searching, sorting and filtering
		$this->setIndexRouteName('bonusrequest.index');

		// crud support
		$this->setCreateRouteName('bonusrequests.create_admin');
		$this->setViewRouteName('bonusrequests.show');
		$this->setDeleteRouteName('bonusrequest.destroy');

		// default route parameter
		$this->setDefaultRouteParameter('id');
	}

	/**
	 * Return a closure that is executed per row, to render a link that will be clicked on to execute an action
	 *
	 * @return Closure
	 */
	public function getLinkableCallback(): Closure {
		return function ($gridName, $item) {
			return route($this->getViewRouteName(), [$gridName => $item->id]);
		};
	}

	/**
	 * Configure rendered buttons, or add your own
	 *
	 * @return void
	 */
	public function configureButtons() {
		$this->editRowButton('delete', [
			'name' => 'Удалить',
			'pjaxEnabled' => false,
			'position' => 4,
			'url' => function ($gridName, $gridItem) {
				return route('bonusrequests.destroy', ['id' => $gridItem->id]);
			},
			'dataAttributes' => [
				'method' => 'delete',
				'trigger-confirm' => true,
			],
		]);

		$this->editToolbarButton('create', [
			'name' => 'Создать',
		]);

		$this->editToolbarButton('refresh', [
			'name' => 'Обновить',
		]);

		$this->editRowButton('view', [
			'name' => 'Смотреть',
			'url' => function ($gridName, $gridItem) {
				return route('bonusrequests.show', ['id' => $gridItem->id]);
			},
		]);

		// call `editButtonProperties` to do either of the above. All the edit functions accept the properties as an array
	}

	/**
	 * Returns a closure that will be executed to apply a class for each row on the grid
	 * The closure takes two arguments - `name` of grid, and `item` being iterated upon
	 *
	 * @return Closure
	 */
	public function getRowCssStyle(): Closure {
		return function ($gridName, $item) {
			// e.g, to add a success class to specific table rows;
			// return $item->id % 2 === 0 ? 'table-success' : '';
			return "";
		};
	}
}