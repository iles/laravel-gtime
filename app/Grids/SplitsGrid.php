<?php

namespace App\Grids;

use Closure;
use Leantony\Grid\Grid;
use Config;

class SplitsGrid extends Grid implements SplitsGridInterface
{
    /**
     * The name of the grid
     *
     * @var string
     */
    protected $name = 'Splits';

    /**
     * List of buttons to be generated on the grid
     *
     * @var array
     */
    protected $buttonsToGenerate = [
        'view',
        'refresh',
    ];

    /**
     * Specify if the rows on the table should be clicked to navigate to the record
     *
     * @var bool
     */
    protected $linkableRows = false;

    /**
    * Set the columns to be displayed.
    *
    * @return void
    * @throws \Exception if an error occurs during parsing of the data
    */
    public function setColumns()
    {

    	$progs = [];
        
        foreach (Config::get('matrix.get') as $key => $matrix){
            $progs[$key] = $matrix[1];
        }

        $this->columns = [
		    "id" => [
		        "label" => "ID",
		        "filter" => [
		            "enabled" => true,
		            "operator" => "="
		        ],
		    ],
            "program_id" => [
                "label" => 'Программа',
                "filter" => [
                    "enabled" => true,
                    'type' => 'select',
                    'data' => $progs
                ],
                "data" => function ($columnData, $columnName) use ($progs) {
                    return $progs[$columnData->program_id];
                },
                "export" => false
            ],
		    "leader" => [
                "label" => 'Лидер',
		        "search" => [
		            "enabled" => true
		        ],
		        "filter" => [
		            "enabled" => true,
		            "operator" => "like"
		        ],
                "data" => function ($columnData, $columnName){
                    $l = json_decode($columnData->leader);
                    return $l->name;
                },
		    ],
		    "main_matrix_id" => [
		    	"label" => 'Старая лестница',
		        "raw" => true,
		        "filter" => [
		            "enabled" => true,
		            "operator" => "="
		        ],
				"styles" => [
		            "class" => "mxinfo"
		        ],
		        "export" => false,
		        "data" => function ($columnData, $columnName) use ($progs) {
		        	$l = json_decode($columnData->leader);
                    $str = '<p class="mxinfo">';
                    $str .= '<small>ID:</small><br/>';
                    $str .= '<span>';
                    $str .= $columnData->main_matrix_id;
                    $str .= '</span>';
                    $str .= '</p>';                    
                    $str .= '<p class="mxinfo">';
                    $str .= '<small>Лидер:</small><br/>';
                    $str .= '<span>';
                    $str .= $l->name;
                    $str .= '</span>';
                    $str .= '</p>';                    
                    $str .= '<p class="mxinfo">';
                    $str .= '<small>ФИО:</small><br/>';
                    $str .= '<span>';
                    $str .= $l->familiya;
                    $str .= '</span>';
                    $str .= '</p>';
                    return $str;
                },
		    ],
		    // "first_matrix" => [
		    //     "search" => [
		    //         "enabled" => true
		    //     ],
		    //     "filter" => [
		    //         "enabled" => true,
		    //         "operator" => "="
		    //     ]
		    // ],
		    "first_matrix_id" => [
		    	"label" => 'Первая лестница',
				"raw" => true,
		        "filter" => [
		            "enabled" => true,
		            "operator" => "="
		        ],
				"styles" => [
		            "class" => "mxinfo"
		        ],
		        "export" => false,
					"data" => function ($columnData, $columnName) use ($progs) {
		        	$l = json_decode($columnData->first_matrix, true);

                    $c = 0;
                    foreach ($l as $key => $value) {
                        foreach ($value as $k => $v) {
                            $c ++;
                        }
                    }
		        	
 					$str = '<p class="mxinfo">';
                    $str .= '<small>ID:</small><br/>';
                    $str .= '<span>';
                    $str .= $columnData->first_matrix_id;
                    $str .= '</span>';
                    $str .= '</p>';                    
                    $str .= '<p class="mxinfo">';
                    $str .= '<small>Лидер:</small><br/>';
                    $str .= '<span>';
                    $str .= $leader = $l[1][0]['uname'];
                    $str .= '</span>';
                    $str .= '</p>';                    
                    $str .= '<p class="mxinfo">';
                    $str .= '<small>Партнеров:</small><br/>';
                    $str .= '<span>';
                    $str .= $c;
                    $str .= '</span>';
                    $str .= '</p>';
                    return $str;
                },
		    ],		    
		    "second_matrix_id" => [
		    	"label" => 'Вторая лестница',
				"raw" => true,
		        "filter" => [
		            "enabled" => true,
		            "operator" => "="
		        ],
				"styles" => [
		            "class" => "mxinfo"
		        ],
		        "export" => false,
					"data" => function ($columnData, $columnName) use ($progs) {
		        	 $c = 0;
                     $l = json_decode($columnData->second_matrix, true);
                    foreach ($l as $key => $value) {
                        foreach ($value as $k => $v) {
                            $c ++;
                        }
                    }
 					$str = '<p class="mxinfo">';
                    $str .= '<small>ID:</small><br/>';
                    $str .= '<span>';
                    $str .= $columnData->second_matrix_id;
                    $str .= '</span>';
                    $str .= '</p>';                    
                    $str .= '<p class="mxinfo">';
                    $str .= '<small>Лидер:</small><br/>';
                    $str .= '<span>';
                    $str .= $l[1][0]['uname'];;
                    $str .= '</p>';        
                    $str .= '</span>';            
                    $str .= '<p class="mxinfo">';
                    $str .= '<small>Партнеров:</small><br/>';
                    $str .= '<span>';
                    $str .= $c;
                    $str .= '</span>';
                    $str .= '</p>';
                    return $str;
                },
		    ],

		    "created_at" => [
		    	"label" => 'Дата разделения',
		        "sort" => false,
		        "date" => "true",
		        "filter" => [
		            "enabled" => true,
		            "type" => "date",
		            "operator" => "<="
		        ],
				"data" => function ($columnData, $columnName) use ($progs) {
					return $columnData->created_at;
				}
		    ]
		];
    }

    /**
     * Set the links/routes. This are referenced using named routes, for the sake of simplicity
     *
     * @return void
     */
    public function setRoutes()
    {
        // searching, sorting and filtering
        $this->setIndexRouteName('splits.index');

        // crud support
        $this->setCreateRouteName('splits.create');
        $this->setViewRouteName('splits.show');
        $this->setDeleteRouteName('splits.destroy');

        // default route parameter
        $this->setDefaultRouteParameter('id');
    }

    /**
    * Return a closure that is executed per row, to render a link that will be clicked on to execute an action
    *
    * @return Closure
    */
    public function getLinkableCallback(): Closure
    {
        return function ($gridName, $item) {
            return route($this->getViewRouteName(), [$gridName => $item->id]);
        };
    }

    /**
    * Configure rendered buttons, or add your own
    *
    * @return void
    */
    public function configureButtons()
    {
        // call `addRowButton` to add a row button
        // call `addToolbarButton` to add a toolbar button
        // call `makeCustomButton` to do either of the above, but passing in the button properties as an array

        // call `editToolbarButton` to edit a toolbar button
        // call `editRowButton` to edit a row button
        // call `editButtonProperties` to do either of the above. All the edit functions accept the properties as an array
    }

    /**
    * Returns a closure that will be executed to apply a class for each row on the grid
    * The closure takes two arguments - `name` of grid, and `item` being iterated upon
    *
    * @return Closure
    */
    public function getRowCssStyle(): Closure
    {
        return function ($gridName, $item) {
            // e.g, to add a success class to specific table rows;
            // return $item->id % 2 === 0 ? 'table-success' : '';
            return "";
        };
    }
}