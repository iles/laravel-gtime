<?php

namespace App\Grids;

use Closure;
use Leantony\Grid\Grid;

class StartRequestUsersNewPinGrid extends Grid implements StartRequestUsersNewPinGridInterface
{
    /**
     * The name of the grid
     *
     * @var string
     */
    protected $name = 'Заявки';

    /**
     * List of buttons to be generated on the grid
     *
     * @var array
     */
    protected $buttonsToGenerate = [
        'create',
        'view',
        'delete',
        'refresh',
        // 'export'
    ];

    /**
     * Specify if the rows on the table should be clicked to navigate to the record
     *
     * @var bool
     */
    protected $linkableRows = false;

    /**
    * Set the columns to be displayed.
    *
    * @return void
    * @throws \Exception if an error occurs during parsing of the data
    */
    public function setColumns()
    {
        $this->columns = [
		    "id" => [
                "sort" => true,
                "search" => ["enabled" => true],
                "filter" => ["enabled" => true, "operator" => "="],
            ],
		    "pin" => [
                "label" =>'Пин',
                "sort" => true,
                "search" => ["enabled" => true],
                "filter" => ["enabled" => true, "operator" => "like"],
		    ],
            "uname" => [
                "label" =>'Логин',
                "sort" => true,
                "search" => ["enabled" => true],
                "filter" => ["enabled" => true, "operator" => "like"],
            ],		    
            "status" => [
                "label" =>'Статус',
                "sort" => true,
                "search" => ["enabled" => false],
                'presenter' => function ($columnData, $columnName) {
                    switch ($columnData->status) {
                        case 0:
                            return 'Новая';
                            break;                        
                        case 1:
                            return 'Одобренная';
                            break;                        
                        case 2:
                            return 'Отклоненная';
                            break;
                        
                        default:
                            return 'Новая';
                            break;
                    }
                },
                "filter" => [
                    "enabled" => true,
                    "type" => "select",
                    "data" => [
                        '0'=> 'Новая',
                        '1'=> 'Одобренная',
                        '2'=> 'Отклоненная',
                    ]
                ],
		    ],
		];
    }

    /**
     * Set the links/routes. This are referenced using named routes, for the sake of simplicity
     *
     * @return void
     */
    public function setRoutes()
    {
        // searching, sorting and filtering
        $this->setIndexRouteName('partners.index');

        // crud support
        $this->setCreateRouteName('partners.create');
        $this->setViewRouteName('partners.show');
        $this->setDeleteRouteName('partners.destroy');

        // default route parameter
        $this->setDefaultRouteParameter('id');
    }

    /**
    * Return a closure that is executed per row, to render a link that will be clicked on to execute an action
    *
    * @return Closure
    */
    public function getLinkableCallback(): Closure
    {
        return function ($gridName, $item) {
            return route($this->getViewRouteName(), [$gridName => $item->id]);
        };
    }

    /**
    * Configure rendered buttons, or add your own
    *
    * @return void
    */
    public function configureButtons()
    {
        $this->editToolbarButton('refresh', [
            'name' => 'Обновить',
        ]);        

        $this->editRowButton('view', [
            'name' => 'Смотреть',
        ]);        

        $this->editRowButton('delete', [
            'name' => 'Удалить',
            'url' => function ($gridName, $item) {
                return $this->getDeleteUrl([
                    'id' => $item->id, 
                ]);             
            },
        ]);        

        $this->editToolbarButton('create', [
            'name' => 'Создать',
        ]);  

        // call `addRowButton` to add a row button
        // call `addToolbarButton` to add a toolbar button
        // call `makeCustomButton` to do either of the above, but passing in the button properties as an array

        // call `editToolbarButton` to edit a toolbar button
        // call `editRowButton` to edit a row button
        // call `editButtonProperties` to do either of the above. All the edit functions accept the properties as an array
    }


    /**
    * Returns a closure that will be executed to apply a class for each row on the grid
    * The closure takes two arguments - `name` of grid, and `item` being iterated upon
    *
    * @return Closure
    */
    public function getRowCssStyle(): Closure
    {
        return function ($gridName, $item) {
            // e.g, to add a success class to specific table rows;
            // return $item->id % 2 === 0 ? 'table-success' : '';
            return "";
        };
    }
}