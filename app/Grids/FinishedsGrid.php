<?php

namespace App\Grids;

use App\Models\User;
use Closure;
use Config;
use Leantony\Grid\Grid;

class FinishedsGrid extends Grid implements FinishedsGridInterface {
	/**
	 * The name of the grid
	 *
	 * @var string
	 */
	protected $name = 'Завершившие';

	/**
	 * List of buttons to be generated on the grid
	 *
	 * @var array
	 */
	protected $buttonsToGenerate = [
		'refresh',
		'view',
		'info',
	];

	/**
	 * Specify if the rows on the table should be clicked to navigate to the record
	 *
	 * @var bool
	 */
	protected $linkableRows = false;

	/**
	 * Set the columns to be displayed.
	 *
	 * @return void
	 * @throws \Exception if an error occurs during parsing of the data
	 */
	public function setColumns() {

		$progs = [];
		$stages = [1 => 'Старт', 0 => 'Финиш'];

		foreach (Config::get('matrix.get') as $key => $matrix) {
			$progs[$key] = $matrix[1];
		}

		$this->columns = [
			"id" => [
				"label" => "ID",
				"filter" => [
					"enabled" => true,
					"operator" => "=",
				],
			],

			"program_id" => [
				"label" => 'Программа',
				"filter" => [
					"enabled" => true,
					'type' => 'select',
					'data' => $progs,
				],
				"data" => function ($columnData, $columnName) use ($progs) {
					return $progs[$columnData->program_id];
				},
				"export" => false,
			],
			"stage" => [
				"label" => 'Лестница',
				"export" => false,
				"filter" => [
					"enabled" => true,
					'type' => 'select',
					'data' => $stages,
				],
				"data" => function ($columnData, $columnName) use ($stages) {
					if($columnData->program_id == 3 || $columnData->program_id == 6 || $columnData->program_id == 8){
						return $stages[$columnData->stage];
					} else {
						return '';
					}
				},

			],
			"registred_pin" => [
				"label" => "Пинкод",
				"search" => [
					"enabled" => true,
				],
				"filter" => [
					"enabled" => true,
					"operator" => "=",
				],
			],
			"created_at" => [
				"label" => "Дата закрытия",
				"sort" => false,
				"date" => "true",
				"filter" => [
					"enabled" => true,
					"type" => "date",
					"operator" => "<=",
				],
				"data" => function ($columnData, $columnName) {
					return $columnData->created_at;
				},
			],
			"login" => [
				"label" => "Логин",
				"search" => [
					"enabled" => true,
				],
				"filter" => [
					"enabled" => true,
					"operator" => "=",
				],
			],
			"fullname" => [
				"label" => "ФИО",
				"search" => [
					"enabled" => true,
				],
				"filter" => [
					"enabled" => true,
					"operator" => "=",
				],
			],
			"birthdate" => [
				"label" => "Дата рождения",
				"search" => [
					"enabled" => true,
				],
				"filter" => [
					"enabled" => true,
					"operator" => "=",
				],
			],
			"iin" => [
				"label" => "ИИН",
				"search" => [
					"enabled" => true,
				],
				"filter" => [
					"enabled" => true,
					"operator" => "=",
				],
			],
			"phone" => [
				"label" => "Телефон",
				"search" => [
					"enabled" => true,
				],
				"filter" => [
					"enabled" => true,
					"operator" => "=",
				],
			],
			"pins" => [
				"label" => "Переходные пин коды",
				"raw" => true,
				"filter" => [
					"enabled" => false,
				],
				"data" => function ($columnData, $columnName) {
					$str = '';
					$m = Config::get('matrix.get');

					if ($columnData->pin) {
						$data = json_decode($columnData->pin, true);
						$pin = \App\Models\Pins\StartPin::where('id', $data['pin'])->first();
						$type = $m[$pin->program][0];
						$connection = 'mysql_prod_' . $type;
						$user = User::on($connection)->where('screch', $pin->pin)->first();
						$str .= $pin->pin;
						if ($user) {
							$str .= '<pre style="background: #c9c9c9; padding: 5px;">';
							$str .= '<br/> Логин: ' . $user->name . '<br/>';
							$str .= '<br/> ФИО: ' . '<b>' . $user->fullname . '</b><br/>';
							$str .= '<br/> Дата регистрации: ' . '<b>' . gmdate("Y-m-d", $user->reg_date) . '</b><br/>';
							$str .= '</pre>';
						}
					}

					if ($columnData->pin2) {
						$data = json_decode($columnData->pin2, true);
						$pin = \App\Models\Pins\StartPin::where('id', $data['pin'])->first();

						$type = $m[$data['program_id']][0];
						$connection = 'mysql_prod_' . $type;
						$user = User::on($connection)->where('screch', $pin->pin)->first();
						$str .= '<br/>' . $pin->pin;
						if ($user) {
							$str .= '<pre style="background: #c9c9c9; padding: 5px;">';
							$str .= '<br/> Логин: ' . $user->name . '<br/>';
							$str .= '<br/> ФИО: ' . '<b>' . $user->fullname . '</b><br/>';
							$str .= '<br/> Дата регистрации: ' . '<b>' . gmdate("Y-m-d", $user->reg_date) . '</b><br/>';
							$str .= '</pre>';
						}
					}

					if ($columnData->pin3) {
						$data = json_decode($columnData->pin3, true);
						$pin = \App\Models\Pins\StartPin::where('id', $data['pin'])->first();
						$type = $m[$data['program_id']][0];
						$connection = 'mysql_prod_' . $type;
						$user = User::on($connection)->where('screch', $pin->pin)->first();
						$str .= '<br/>' . $pin->pin;
						if ($user) {
							$str .= '<pre style="background: #c9c9c9; padding: 5px;">';
							$str .= '<br/> Логин: ' . $user->name . '<br/>';
							$str .= '<br/> ФИО: ' . '<b>' . $user->fullname . '</b><br/>';
							$str .= '<br/> Дата регистрации: ' . '<b>' . gmdate("Y-m-d", $user->reg_date) . '</b><br/>';
							$str .= '</pre>';
						}
					}
					return $str;
				},
			],
			"country" => [
				"label" => "Страна/город",
				"search" => [
					"enabled" => true,
				],
				"filter" => [
					"enabled" => true,
					"operator" => "=",
				],
			],
			"info" => [
				"label" => "Приписка",
				"search" => [
					"enabled" => true,
				],
				"filter" => [
					"enabled" => true,
					"operator" => "=",
				],
			],
			"tovar" => [
				"label" => "Товар",
				"search" => [
					"enabled" => true,
				],
				"filter" => [
					"enabled" => true,
					"operator" => "=",
				],
			],
		];
	}

	/**
	 * Set the links/routes. This are referenced using named routes, for the sake of simplicity
	 *
	 * @return void
	 */
	public function setRoutes() {
		// searching, sorting and filtering
		$this->setIndexRouteName('partners.finished');

		// crud support
		$this->setCreateRouteName('finisheds.create');
		$this->setViewRouteName('finisheds.show');
		$this->setDeleteRouteName('finisheds.destroy');

		// default route parameter
		$this->setDefaultRouteParameter('id');
	}

	/**
	 * Return a closure that is executed per row, to render a link that will be clicked on to execute an action
	 *
	 * @return Closure
	 */
	public function getLinkableCallback(): Closure {
		return function ($gridName, $item) {
			return route($this->getViewRouteName(), [$gridName => $item->id]);
		};
	}

	/**
	 * Configure rendered buttons, or add your own
	 *
	 * @return void
	 */
	public function configureButtons() {
		// call `addRowButton` to add a row button
		// call `addToolbarButton` to add a toolbar button
		// call `makeCustomButton` to do either of the above, but passing in the button properties as an array

		// call `editToolbarButton` to edit a toolbar button
		// call `editRowButton` to edit a row button
		// call `editButtonProperties` to do either of the above. All the edit functions accept the properties as an array
		$this->editRowButton('view', [
			'name' => 'pins',
		]);

		// $this->makeCustomButton([
		// 	'name' => 'info',
		// 	'label' => 'лестница',
		// 	'icon' => 'fa-eye',
		// 	'position' => 2,
		// 	'class' => 'btn btn-outline-primary btn-sm grid-row-button',
		// 	'showModal' => true,
		// 	'gridId' => $this->getId(),
		// 	'renderIf' => function () {
		// 		return true;
		// 	},
		// 	'title' => 'edit record',
		// 	'url' => function ($gridName, $gridItem) {
		// 		return route('finisheds.viewDrop', ['id' => $gridItem->id]);
		// 	},
		// ], static::$TYPE_ROW);

	}

	/**
	 * Returns a closure that will be executed to apply a class for each row on the grid
	 * The closure takes two arguments - `name` of grid, and `item` being iterated upon
	 *
	 * @return Closure
	 */
	public function getRowCssStyle(): Closure {
		return function ($gridName, $item) {
			// e.g, to add a success class to specific table rows;
			// return $item->id % 2 === 0 ? 'table-success' : '';
			return "";
		};
	}
}