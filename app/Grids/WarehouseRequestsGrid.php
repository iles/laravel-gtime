<?php

namespace App\Grids;

use Closure;
use Leantony\Grid\Grid;
use Config;

class WarehouseRequestsGrid extends Grid implements WarehouseRequestsGridInterface
{
    /**
     * The name of the grid
     *
     * @var string
     */
    protected $name = '';

    /**
     * List of buttons to be generated on the grid
     *
     * @var array
     */
    protected $buttonsToGenerate = [
    	'view', 'refresh'
    ];

    /**
     * Specify if the rows on the table should be clicked to navigate to the record
     *
     * @var bool
     */
    protected $linkableRows = false;

    /**
    * Set the columns to be displayed.
    *
    * @return void
    * @throws \Exception if an error occurs during parsing of the data
    */
    public function setColumns()
    {
    	$progs = [];
    	$statuses = [0 => 'Новый', 1 => 'Одобрен' ];
    	$options = ['nabor1' => 'nabor1', 'nabor2' => 'nabor2', 'nabor3' => 'nabor3', 'nabor4' => 'nabor4'];
		
		foreach (Config::get('matrix.get') as $key => $matrix){
			$progs[$key] = $matrix[1];
		}

        $this->columns = [
		    "id" => [
		        "label" => "ID",
		        "filter" => [
		            "enabled" => true,
		            "operator" => "="
		        ],
		    ],
		    "program" => [
				"label" => "Программа",
		        "search" => [
		            "enabled" => true
		        ],
		        "filter" => [
		            "enabled" => true,
                    'type' => 'select',
                    'data' => $progs
		        ],
				"data" => function ($columnData, $columnName) use ($progs) {
			    	if ($columnData->program == null){
			    		return '';
			    	}
			    	return $progs[$columnData->program];
				},
		    ],
		    "uname" => [
				"label" => "Логин",
		        "search" => [
		            "enabled" => true
		        ],
		        "filter" => [
		            "enabled" => true,
		            "operator" => "="
		        ]
		    ],		    
		    "iin" => [
				"label" => "ИИН",
		        "search" => [
		            "enabled" => true
		        ],
		        "filter" => [
		            "enabled" => true,
		            "operator" => "="
		        ]
		    ],
		    "user_fullname" => [
				"label" => "ФИО",
		        "search" => [
		            "enabled" => true
		        ],
		        "filter" => [
		            "enabled" => true,
		            "operator" => "="
		        ]
		    ],
		    "purchace_date" => [
				"label" => "Дата Покупки",
		        "search" => [
		            "enabled" => true
		        ],
		        "filter" => [
		            "enabled" => true,
		            "operator" => "="
		        ]
		    ],
		    "checkout_name" => [
				"label" => "Имя",
		        "search" => [
		            "enabled" => true
		        ],
		        "filter" => [
		            "enabled" => true,
		            "operator" => "="
		        ]
		    ],
		    "checkout_surname" => [
				"label" => "Фамилия",
		        "search" => [
		            "enabled" => true
		        ],
		        "filter" => [
		            "enabled" => true,
		            "operator" => "="
		        ]
		    ],
		    "checkout_patronic" => [
				"label" => "Отчество",
		        "search" => [
		            "enabled" => true
		        ],
		        "filter" => [
		            "enabled" => true,
		            "operator" => "="
		        ]
		    ],
		    "checkout_date" => [
				"label" => "Дата покупки",
		        "search" => [
		            "enabled" => true
		        ],
		        "filter" => [
		            "enabled" => true,
		            "operator" => "="
		        ]
		    ],
		    "option" => [
				"label" => "Набор",
		        "search" => [
		            "enabled" => true
		        ],
		        "filter" => [
		            "enabled" => true,
                    'type' => 'select',
                    'data' => $options
		        ]
		    ],
		    "created_at" => [
				"label" => 'Дата покупки',
		        "sort" => false,
		        "date" => "true",
		        "filter" => [
		            "enabled" => true,
		            "type" => "date",
		            "operator" => "<="
		        ]
		    ],
		 	"status_id" => [
				"label" => "Статус",
		        "filter" => [
		            "enabled" => true,
                    'type' => 'select',
                    'data' => $statuses
		        ],
				"data" => function ($columnData, $columnName) use ($statuses) {
			    	return $statuses[$columnData->status_id];
				},
		    ],
		];
    }

    /**
     * Set the links/routes. This are referenced using named routes, for the sake of simplicity
     *
     * @return void
     */
    public function setRoutes()
    {
        // searching, sorting and filtering
        $this->setIndexRouteName('warehouse.index');

        // crud support
        $this->setCreateRouteName('warehouserequests.create');
        $this->setViewRouteName('warehouse.show');
        $this->setDeleteRouteName('warehouserequests.destroy');

        // default route parameter
        $this->setDefaultRouteParameter('id');
    }

    /**
    * Return a closure that is executed per row, to render a link that will be clicked on to execute an action
    *
    * @return Closure
    */
    public function getLinkableCallback(): Closure
    {
        return function ($gridName, $item) {
            return route($this->getViewRouteName(), [$gridName => $item->id]);
        };
    }

    /**
    * Configure rendered buttons, or add your own
    *
    * @return void
    */
    public function configureButtons()
    {
        // call `addRowButton` to add a row button
        // call `addToolbarButton` to add a toolbar button
        // call `makeCustomButton` to do either of the above, but passing in the button properties as an array
        $this->editRowButton('view', [
            'name' => 'Смотреть',
            'url' => function ($gridName, $item) {
            	return route('warehouse.show', ['id' => $item->id]);
            },
        ]);  

        // call `editToolbarButton` to edit a toolbar button
        // call `editRowButton` to edit a row button
        // call `editButtonProperties` to do either of the above. All the edit functions accept the properties as an array
    }

    /**
    * Returns a closure that will be executed to apply a class for each row on the grid
    * The closure takes two arguments - `name` of grid, and `item` being iterated upon
    *
    * @return Closure
    */
    public function getRowCssStyle(): Closure
    {
        return function ($gridName, $item) {
            // e.g, to add a success class to specific table rows;
            // return $item->id % 2 === 0 ? 'table-success' : '';
            return "";
        };
    }
}