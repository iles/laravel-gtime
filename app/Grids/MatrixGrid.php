<?php

namespace App\Grids;

use Closure;
use Leantony\Grid\Grid;
use App\Models\BlockedMatrix;

class MatrixGrid extends Grid implements DleMatrixGridInterface
{
    /**
     * The name of the grid
     *
     * @var string
     */
    protected $name = 'Лестницы';

    /**
     * List of buttons to be generated on the grid
     *
     * @var array
     */
    protected $buttonsToGenerate = [
        'view',
        'delete',
        'refresh',
    ];

    /**
     * Specify if the rows on the table should be clicked to navigate to the record
     *
     * @var bool
     */
    protected $linkableRows = false;

    /**
    * Set the columns to be displayed.
    *
    * @return void
    * @throws \Exception if an error occurs during parsing of the data
    */
    public function setColumns()
    {
        $this->columns = [
		    "matrix_id" => [
                "label" => 'ID',
		        "filter" => [
		            "enabled" => true,
		        ],
		        "export" => false
		    ],
		    "leader" => [
                "label" => 'Лидер',
		        "search" => [
		            "enabled" => true
		        ],
		        "filter" => [
		            "enabled" => true,
		            "operator" => "like"
		        ]
		    ],
		    "users_num" => [
                "label" => 'Кол-во человек',
		        "search" => [
		            "enabled" => true
		        ],
		        "filter" => [
		            "enabled" => true,
		            "operator" => "="
		        ]
		    ],            
            "datetout" => [
                "label" => 'datetout',
                "search" => [
                    "enabled" => true
                ],
                "filter" => [
                    "enabled" => true,
                    "operator" => "like",
                ],
            ],
		];
    }

    /**
     * Set the links/routes. This are referenced using named routes, for the sake of simplicity
     *
     * @return void
     */
    public function setRoutes()
    {
        // searching, sorting and filtering
        $this->setIndexRouteName('matrix.index');

        // crud support
        $this->setCreateRouteName('matrix.create');
        $this->setViewRouteName('matrix.show');
        $this->setDeleteRouteName('matrix.destroy');

        // default route parameter
        $this->setDefaultRouteParameter('matrix_id');
    }

    /**
    * Return a closure that is executed per row, to render a link that will be clicked on to execute an action
    *
    * @return Closure
    */
    public function getLinkableCallback(): Closure
    {
        return function ($gridName, $item) {
            return route($this->getViewRouteName(), [$gridName => $item->matrix_id]);
        };
    }

    /**
    * Configure rendered buttons, or add your own
    *
    * @return void
    */
    public function configureButtons()
    {
        $this->editToolbarButton('refresh', [
            'name' => 'Обновить',
        ]);        

        $this->editRowButton('view', [
            'name' => 'Смотреть',
            'showModal' => true,
            'url' => function ($gridName, $item) {
			return $this->getViewUrl([
				'id' => $item->{$this->getDefaultRouteParameter()}, 
				  ]);             },
        ]);


        $this->makeCustomButton([
            'name' => 'block',
            'label' => 'лестница',
            'icon' => 'fa-eye',
            'position' => 2,
            'class' => 'btn btn-outline-primary btn-sm grid-row-button',
            'showModal' => false,
            'gridId' => $this->getId(),
            'renderIf' => function ($gridName, $item)  {
                $b =  BlockedMatrix::where('matrix_id', $item->matrix_id)->first();
                if(!$b){
                    return true;
                } else {
                    return false;                    
                }
            },
            'title' => 'edit record',
            'url' => function($gridName, $gridItem) {
                return route('matrix.block', ['id' => $gridItem->matrix_id]);
            },
        ], static::$TYPE_ROW);        

        $this->makeCustomButton([
            'name' => 'unblock',
            'label' => 'лестница',
            'icon' => 'fa-eye',
            'position' => 2,
            'class' => 'btn btn-outline-primary btn-sm grid-row-button',
            'showModal' => false,
            'gridId' => $this->getId(),
            'renderIf' => function ($gridName, $item)  {
                $b =  BlockedMatrix::where('matrix_id', $item->matrix_id)->first();
                if($b){
                    return true;
                } else {
                    return false;                    
                }
            },
            'title' => 'edit record',
            'url' => function($gridName, $gridItem) {
                return route('matrix.unblock', ['id' => $gridItem->matrix_id]);
            },
        ], static::$TYPE_ROW);



        $this->editRowButton('delete', [
            'name' => 'Удалить',
        ]);

        $this->editToolbarButton('create', [
            'name' => 'Создать',
        ]);  

            



        // call `addRowButton` to add a row button
        // call `addToolbarButton` to add a toolbar button
        // call `makeCustomButton` to do either of the above, but passing in the button properties as an array

        // call `editToolbarButton` to edit a toolbar button
        // call `editRowButton` to edit a row button
        // call `editButtonProperties` to do either of the above. All the edit functions accept the properties as an array
    }

    /**
    * Returns a closure that will be executed to apply a class for each row on the grid
    * The closure takes two arguments - `name` of grid, and `item` being iterated upon
    *
    * @return Closure
    */
    public function getRowCssStyle(): Closure
    {
        return function ($gridName, $item) {
            // e.g, to add a success class to specific table rows;
            // return $item->matrix_id % 2 === 0 ? 'table-success' : '';
            return "";
        };
    }
}
