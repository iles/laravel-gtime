<?php

namespace App\Grids;

use Auth;
use Closure;
use Config;
use Leantony\Grid\Grid;

class PinsEGrid extends Grid implements PinsGridInterface {
	/**
	 * The name of the grid
	 *
	 * @var string
	 */
	protected $name = 'Pins';

	/**
	 * List of buttons to be generated on the grid
	 *
	 * @var array
	 */
	protected $buttonsToGenerate = [
		'create',
		'view',
		'delete',
		'refresh',
		'export',
		'sec',
	];

	/**
	 * Specify if the rows on the table should be clicked to navigate to the record
	 *
	 * @var bool
	 */
	protected $linkableRows = false;

	/**
	 * Set the columns to be displayed.
	 *
	 * @return void
	 * @throws \Exception if an error occurs during parsing of the data
	 */
	public function setColumns() {

		$progs = [];

		foreach (Config::get('matrix.get') as $key => $matrix) {
			$progs[$key] = $matrix[1];
		}
		$statusesf = [0 => 'Новый', 1 => 'Одобрен', 2 => 'Отклонен', 3 => 'Активирован'];
		$statuses = [0 => '<span class="badge badge-success">Новый</span>', 1 => '<span class="badge badge-primary">Одобрен</span>', 2 => '<span class="badge badge-danger">Отклонен</span>', 3 => '<span class="badge badge-secondary">Активирован</span>'];

		$this->columns = [
			"id" => [
				"label" => "ID",
				"export" => false,
				"filter" => [
					"enabled" => true,
					"operator" => "=",
				],
			],

			"summ" => [
				"label" => "Сумма",
				"sort" => false,
				"export" => true,
				"filter" => [
					"enabled" => true,
					"operator" => '=',

				],
			],
			"fullname_b" => [
				"label" => "ФИО",
				"sort" => false,
				"export" => true,
				"filter" => [
					"enabled" => true,
					"operator" => '=',

				],
			],
			"country" => [
				"label" => "Страна",
				"sort" => false,
				"export" => true,
				"filter" => [
					"enabled" => true,
					"operator" => '=',

				],
			],
			"city" => [
				"label" => "Город",
				"sort" => false,
				"export" => true,
				"filter" => [
					"enabled" => true,
					"operator" => '=',

				],
			],
			"s" => [
				"label" => "С",
				"sort" => false,
				"export" => true,
				"styles" => [
					"column" => "h",
					"row" => "h",
				],
				"filter" => [
					"enabled" => false,
				],
				"data" => function ($columnData, $columnName) {
					if(count($columnData->pins()->where('program', 1)->get())){
						return count($columnData->pins()->where('program', 1)->get());
					} else {
						return 0;
					}
				},
			],
			"n" => [
				"label" => "Н",
				"sort" => false,
				"export" => true,
				"styles" => [
					"column" => "h",
					"row" => "h",
				],
				"filter" => [
					"enabled" => false,
				],
				"data" => function ($columnData, $columnName) {
					if(count($columnData->pins()->where('program', 4)->get())){
						return count($columnData->pins()->where('program', 4)->get());
					} else {
						return 0;
					}
				},
			],
			"f" => [
				"label" => "Ф",
				"sort" => false,
				"export" => true,
				"styles" => [
					"column" => "h",
					"row" => "h",
				],
				"filter" => [
					"enabled" => false,
				],
				"data" => function ($columnData, $columnName) {
					if(count($columnData->pins()->where('program', 7)->get())){
						return count($columnData->pins()->where('program', 7)->get());
					} else {
						return 0;
					}
				},
			],
			"o" => [
				"label" => "О",
				"sort" => false,
				"export" => true,
				"styles" => [
					"column" => "h",
					"row" => "h",
				],
				"filter" => [
					"enabled" => false,
				],
				"data" => function ($columnData, $columnName) {
					if(count($columnData->pins()->where('program', 3)->get())){
						return count($columnData->pins()->where('program', 3)->get());
					} else {
						return '0';
					}
				},
			],
			"v" => [
				"label" => "В",
				"sort" => false,
				"export" => true,
				"styles" => [
					"column" => "h",
					"row" => "h",
				],
				"filter" => [
					"enabled" => false,
				],
				"data" => function ($columnData, $columnName) {
					if(count($columnData->pins()->where('program', 6)->get())){
						return count($columnData->pins()->where('program', 6)->get());
					} else {
						return 0;
					}
				},
			],
			"aprove_at" => [
				"label" => "Одобрена",
				"sort" => false,
				"export" => true,
				'renderIf' => function () {
					$auth = Auth::guard('web')->user()->roles()->first()->name;
					if ($auth == 'admin' || $auth == 'superadmin') {
						return true;
					} else {
						return false;
					}
				},
				"date" => "true",
				"filter" => [
					"enabled" => true,
					"type" => "date",
					"operator" => "<=",
				],
				"data" => function ($columnData, $columnName) {
					return $columnData->aprove_at;
				},
			],
			"pin" => [
				"label" => "Пин",
				"sort" => false,
				"raw" => false,
				"export" => true,
				"filter" => [
					"enabled" => true,
					"query" => function ($query, $columnName, $userInput) {
						return $query->join('start_pins', 'start_requests.id', '=', 'start_pins.request_id')
							->select('start_requests.id', 'start_requests.program', 'start_requests.uname', 'start_requests.bill', 'start_requests.iin', 'start_requests.summ', 'start_requests.skype', 'start_requests.status_id', 'start_requests.security_check', 'start_requests.created_at', 'start_requests.created_at', 'start_requests.created_at')
							->where('start_pins.pin', $userInput);
					},
				],
				"presenter" => function ($columnData, $columnName) {
					$pins = $columnData->pins()->get()->groupBy('program');
					$str = '';
					foreach ($pins as $key => $program) {
						$str .= '';
						switch ($key) {
						case 1:
							$prog = 'Старт';
							break;
						case 2:
							$prog = 'Авто';
							break;
						case 3:
							$prog = 'Основная';
							break;
						case 4:
							$prog = 'Накопительная';
							break;
						case 5:
							$prog = 'Накопительная +';
							break;
						case 6:
							$prog = 'VIP';
							break;
						case 7:
							$prog = 'FAST';
							break;

						default:
							$prog = 'Старт';
							break;
						}
						foreach ($program as $k => $pin) {
							$str .= $pin->pin;
							$lfcr = 'hj';
							$str .= $lfcr;
						}
					}
					return $str;
				},
			],
		];
	}

	/**
	 * Set the links/routes. This are referenced using named routes, for the sake of simplicity
	 *
	 * @return void
	 */
	public function setRoutes() {
		// searching, sorting and filtering
		$this->setIndexRouteName('admin.pins.export');

		// crud support
		$this->setCreateRouteName('pins.create');
		$this->setViewRouteName('pins.show');
		$this->setDeleteRouteName('pins.destroy');

		// default route parameter
		$this->setDefaultRouteParameter('id');
	}

	/**
	 * Return a closure that is executed per row, to render a link that will be clicked on to execute an action
	 *
	 * @return Closure
	 */
	public function getLinkableCallback(): Closure {
		return function ($gridName, $item) {
			return route($this->getViewRouteName(), [$gridName => $item->id]);
		};
	}

	/**
	 * Configure rendered buttons, or add your own
	 *
	 * @return void
	 */
	public function configureButtons() {
		$this->editToolbarButton('refresh', [
			'name' => 'Обновить',
		]);

		$this->editRowButton('view', [
			'name' => 'Смотреть',
			'renderIf' => function () {
				$auth = Auth::guard('web')->user()->roles()->first()->name;
				if ($auth == 'admin' || $auth == 'superadmin') {
					return true;
				} else {
					return false;
				}
			},
		]);

		$this->editRowButton('delete', [
			'renderIf' => function () {
				$auth = Auth::guard('web')->user()->roles()->first()->name;
				if ($auth == 'admin' || $auth == 'superadmin') {
					return true;
				} else {
					return false;
				}
			},
			'name' => 'Удалить',
			'url' => function ($gridName, $item) {
				return '/admin/pins/destroy/' . $item->id;
			},
		]);

		$this->editToolbarButton('create', [
			'renderIf' => function () {
				$auth = Auth::guard('web')->user()->roles()->first()->name;
				if ($auth == 'admin' || $auth == 'superadmin') {
					return true;
				} else {
					return false;
				}
			},
			'name' => 'Создать',
		]);

		$this->makeCustomButton([
			'name' => 'sec',
			'icon' => 'fa-eye',
			'renderIf' => function () {
				$auth = Auth::guard('web')->user()->roles()->first()->name;
				if ($auth == 'security') {
					return true;
				} else {
					return false;
				}
			},
			'position' => 2,
			'class' => 'btn btn-outline-primary btn-sm grid-row-button',
			'showModal' => true,
			'gridId' => $this->getId(),
			'title' => 'edit record',
			'url' => function ($gridName, $gridItem) {
				return route('security.show', ['id' => $gridItem->id]);
			},
		], static::$TYPE_ROW);

		$this->editRowButton('sec', [
			'name' => 'Смотреть',
		]);
		// call `addRowButton` to add a row button
		// call `addToolbarButton` to add a toolbar button
		// call `makeCustomButton` to do either of the above, but passing in the button properties as an array

		// call `editToolbarButton` to edit a toolbar button
		// call `editRowButton` to edit a row button
		// call `editButtonProperties` to do either of the above. All the edit functions accept the properties as an array
	}

	/**
	 * Returns a closure that will be executed to apply a class for each row on the grid
	 * The closure takes two arguments - `name` of grid, and `item` being iterated upon
	 *
	 * @return Closure
	 */
	public function getRowCssStyle(): Closure {
		return function ($gridName, $item) {
			// e.g, to add a success class to specific table rows;
			// return $item->id % 2 === 0 ? 'table-success' : '';
			return "";
		};
	}
}