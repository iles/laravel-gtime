<?php

namespace App\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Events\WarehouseRequestEvent;
use Auth;

class WarehouseRequestListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(WarehouseRequestEvent $event)
    {
        $event->warehouseRequest->manager_id = Auth::user->getId();
    }
}
