<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Grids\StartRequestUsersNewPinGrid;
use App\Grids\StartRequestUsersNewPinGridInterface;

use App\Grids\ChangeLeaderHistoriesGrid;
use App\Grids\ChangeLeaderHistoriesGridInterface;
use App\Models\Setting;
use Illuminate\Support\Facades\View;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        date_default_timezone_set('Asia/Almaty');
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        View::share('page_url', Setting::getPageUrl());
        $this->app->bind(StartRequestUsersNewPinGridInterface::class, StartRequestUsersNewPinGrid::class);

    }
}
