<?php


namespace App\Http\Middleware;
use Closure;
use Illuminate\Contracts\Auth\Guard;
use Auth;

class RedirectIfAuthenticated
{
    /**
     * The Guard implementation.
     *
     * @var Guard
     */
    protected $auth;

    /**
     * Create a new filter instance.
     *
     * @param  Guard  $auth
     * @return void
     */
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        


        // if ( strpos($this->auth->getName(), 'web')  ) {
        //     dd($this->auth->getName());
        //     $auth = Austh::user()->roles()->first();

        //     switch ($auth->role) {
        //         case 'warehouse':
        //                 return  redirect()->route('adgvmin');    
        //             break;
        //         case 'superadmin':
        //                 return  redirect()->route('supervgadmin'); 
        //             break;
        //         case 'user':
        //                 return  redirect()->route('usvger');  
        //             break;

        //         default:
        //             # code...
        //             return  redirect()->route('usgver');  
        //             break;
        //     }   

        //  }

        return $next($request);
    }
}