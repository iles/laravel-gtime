<?php

namespace App\Http\Middleware;
use DB;
use Auth;   
use Closure;
use App\Models\Finished;

class DropMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $finished = Finished::where('user_id',  Auth::user()->user_id )->where('stage', 0)->first(); 
        
        if($finished) {
            return redirect('partner/finish');
        }

        return $next($request);
    }
}
