<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


use App\Models\User;
use App\Models\ChangeLeaderHistory;
use App\Grids\ChangeLeaderHistoriesGrid;
use App\Grids\ChangeLeaderHistoriesGridInterface;
use Illuminate\Http\JsonResponse;

class HistoryController extends Controller
{




    public function index(ChangeLeaderHistoriesGrid $historyGrid, Request $request)
    {

        // // define query for roles
        // $sprQuery = ChangeLeaderHistory::query();



        // // define users grid
        // $spr = $sprGrid->create(['query' => $sprQuery, 'request' => $request]);
        // // define roles grid


        // return view('partners.index', compact('spr'));

        // render the grid on the welcome view


            return $historyGrid
                    ->create(['query' => ChangeLeaderHistory::query(), 'request' => $request])
                    ->renderOn('history.index'); // render the grid on the welcome view
    
    }      

    public function create()
    {
        return view('partners.create');
    }


    public function add(Request $request)
    {


        // $data = $this->validate($request, [
        //     'title'=> 'required',
        //     'info'=>'required',
        //     'coordinates'=>'required',
        //     'is_online'=>'integer',
        // ]);
        
        switch ($request->type) {
            case 0:
                $t = 'start';
                break;            
            case 1:
                $t = 'auto';
                break;            
            case 2:
                $t = 'main';
                break;            
            case 3:
                $t = 'vip';
                break;
            
            default:
                # code...
                break;
        }

        //get_table name by program
        //get user_id or get from request
        //while not exeption thrown upon insert, do"
        //generate code, try to insert.
        $connection = 'mysql_prod_'.$t;
        $user = User::on($connection)->whereName($request->name)->first();

        if(!$user){
          return new JsonResponse([
                'success' => false,
                'message' => 'Юзера с ником ' . $request->name . ' не существует.'
            ]);
        }

        $startPinRequest = new StartPinRequest;
        $startPinRequest->user_id = $user->user_id;
        $startPinRequest->status = 0;
        $startPinRequest->pin = $this->generatePIN();
        $startPinRequest->expires_at = date('Y-m-d H:i:s');

        while (true) {
            try {
                $startPinRequest->save(); 
                break;
            } catch (\Illuminate\Database\QueryException $e) {
                $startPinRequest->pin = $this->generatePIN();
                continue;
            }
        };



        return redirect('/partners/all');
    }

    private function generatePIN(){
        // Available alpha caracters
        $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';

        // generate a pin based on 2 * 7 digits + a random character
        $pin = mt_rand(1000000, 9999999)
            . mt_rand(1000000, 9999999)
            . $characters[rand(0, strlen($characters) - 1)];

        // shuffle the result
        return str_shuffle($pin);
    }


}
