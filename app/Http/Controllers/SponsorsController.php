<?php

namespace App\Http\Controllers;

use App\Grids\FinishedsGrid;
use App\Grids\UsersGrid;
use App\Models\Finished;
use App\Models\User;
use Config;
use Illuminate\Http\JsonResponse;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Session;

class SponsorsController extends Controller {

	public function index(UsersGrid $usersGrid, Request $request) {

		$m = Config::get('matrix.get');
		$p = Session::get('programm', 1);

		$type = $m[$p][0];

		$connection = 'mysql_prod_start';
 
		$query = User::query();

		//$query = User::whereIn('user_id', $ids);

		//select * from User where id in (select ref_by from user where ref_by not null)
		return $usersGrid
			->create(['query' => $query, 'request' => $request])
			->renderOn('sponsors.index'); // render the grid on the welcome view

	}

	public function finished_show($id) {
		$finished = Finished::findOrFail($id);
		$last_users = json_decode($finished->last, true);
		$lastList = [];
		$m = Config::get('matrix.get');
		$p = $finished->program_id;

		$type = $m[$p][0];
		$connection = 'mysql_prod_'.$type;
		foreach ($last_users  as $key => $value) {
			$lastList[$value['uid']] = User::on($connection)->where('user_id', $value['uid'])->first();
		}

		$refs = [];
		$r = User::on($connection)->where('by_refer', $finished->user_id)->get();
		foreach ($r as $key => $value) {
		 	$refs[] = $value;
		 }

		$connection = 'mysql_prod_'.$m[$finished->program_id][0];

		switch ($finished->program_id) {
            case 1:
                $table = 'dle_drop_users_start';
                break;
            case 2:
                $table = 'dle_drop_users';
                break;
            case 3:
                if ($finished->stage == 1) {
                    $table = 'dle_drop_users80';
                } else {
                    $table = 'dle_drop_users500';
                }
                break;
            case 4:
                $table = 'dle_drop_users';
                break;
            case 5:
                $table = 'dle_drop_users';
                break;
            case 6:
                $table = 'dle_drop_users9000';
                break;
            case 7:
                $table = 'dle_drop_users';
                break;
            case 8:
                $table = 'dle_drop_users9000';
                break;
            default:
                # code...
                break;
            }

            $setr = DB::connection($connection)->table($table)->select('setr')->where('name', $finished->login)->first();
		return view('finished.show', compact('finished', 'lastList', 'refs', 'setr'));
	}	

	public function finished_show_pin($id) {
		$finished = Finished::findOrFail($id);
		$m = Config::get('matrix.get');
		$p = $finished->program_id;
		$type = $m[$p][0];
		$connection = 'mysql_prod_' . $type;

		$data = json_decode($finished->last, true);

		foreach ($data as $key => $value) {

			$user = User::on($connection)->where(['user_id' => $value['uid']])->first();
			if ($user) {
				$data[$key]['pin'] = $user->screch;
				$data[$key]['fullname'] = $user->familiya;
			}
		}
		return view('sponsors.finished', compact('data', 'p'));
	}

	public function finished(FinishedsGrid $usersGrid, Request $request) {

		$m = Config::get('matrix.get');
		$p = Session::get('programm', 1);

		$type = $m[$p][0];

		$query = Finished::query()->orderBy('created_at', 'DESC'); 
 
		//$query = User::whereIn('user_id', $ids);

		//select * from User where id in (select ref_by from user where ref_by not null)
		return $usersGrid
			->create(['query' => $query, 'request' => $request])
			->renderOn('finished.index'); // render the grid on the welcome view

	}

	public function passchange(Request $request) {

		$validator = Validator::make($request->all(), [
			'password' => ['required', 'string', 'min:8', 'confirmed'],
		]);

		if ($validator->fails()) {
			return redirect('/admin/partners/')
				->withErrors($validator)
				->withInput();
		}

		$m = Config::get('matrix.get');
		$p = Session::get('programm', 1);
		$type = $m[$p][0];
		$connection = 'mysql_prod_' . $type;

		$user = User::on($connection)->where(['user_id' => $request->user_id])->first();
		$user->password = md5(md5($request->password));
		$user->save();
		return redirect('/admin/partners/')
			->with('success', 'Пароль успешно изменен');

	}

	public function show(Request $request) {
		$m = Config::get('matrix.get');
		$p = Session::get('programm', 1);
		$type = $m[$p][0];
		$connection = 'mysql_prod_' . $type;
		$model = User::on($connection)->where(['user_id' => $request->id])->first();
		$refers = User::on($connection)->where('by_refer', $model->user_id)->get();
 
		return view('sponsors.show-bonus', compact('model','refers'));	
	}

	public function create() {
		return view('sponsors.create', compact('model'));
	}

	public function store() {
		return view('sponsors.create', compact('model'));
	}

	public function destroy(Request $request) {
		$m = Config::get('matrix.get');
		$p = Session::get('programm', 1);
		$type = $m[$p][0];
		$connection = 'mysql_prod_' . $type;
		User::on($connection)->findOrFail($request->id)->delete();
		return new JsonResponse([
			'success' => true,
			'message' => 'Спонсор удален',
		]);
	}

	public function update(Request $request) {
        
		$m = Config::get('matrix.get');
		$p = Session::get('programm', 1);

		$type = $m[$p][0];
		// All posted data except token and id
		$data = request()->except(['_token', 'user_id']);

		// Remove empty array values from the data
		$result = array_filter($data);
		$connection = 'mysql_prod_' . $type;
		
		$model = User::on($connection)->findOrFail($request->user_id);		
		$model->update($result);
		
		return redirect()->back();
	}	

	public function finished_update(Request $request,  $id) {
		$model = Finished::find($id);
		$model->login = $request->login;	
		$model->birthdate = $request->birthdate;	
		$model->iin = $request->iin;	
		$model->country = $request->country;	
		$model->phone = $request->phone;	
		$model->info = $request->info;	


		$m = Config::get('matrix.get');

		$connection = 'mysql_prod_'.$m[$model->program_id][0];

		switch ($model->program_id) {
            case 1:
                $table = 'dle_drop_users_start';
                break;
            case 2:
                $table = 'dle_drop_users';
                break;
            case 3:
                if ($model->stage == 1) {
                    $table = 'dle_drop_users80';
                } else {
                    $table = 'dle_drop_users500';
                }
                break;
            case 4:
                $table = 'dle_drop_users';
                break;
            case 5:
                $table = 'dle_drop_users';
                break;
            case 6:
                $table = 'dle_drop_users9000';
                break;
            case 7:
                $table = 'dle_drop_users';
                break;
            case 8:
                $table = 'dle_drop_users9000';
                break;
            default:
                # code...
                break;
            }

		$model->save();

		$drop = DB::connection($connection)->table($table)->where('name', $model->login)->update([
        	'setr' => $request->setr,
		]);
		return redirect()->back();
	}

}
