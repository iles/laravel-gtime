<?php

namespace App\Http\Controllers;

use App\Models\PinRequests\StartRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class StartRequestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\PinRequests\StartRequest  $startRequest
     * @return \Illuminate\Http\Response
     */
    public function show(StartRequest $startRequest)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\PinRequests\StartRequest  $startRequest
     * @return \Illuminate\Http\Response
     */
    public function edit(StartRequest $startRequest)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\PinRequests\StartRequest  $startRequest
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, StartRequest $startRequest)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\PinRequests\StartRequest  $startRequest
     * @return \Illuminate\Http\Response
     */
    public function destroy(StartRequest $startRequest)
    {
        //
    }
}
