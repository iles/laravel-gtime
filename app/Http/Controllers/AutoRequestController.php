<?php

namespace App\Http\Controllers;

use App\Models\PinRequests\AutoRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AutoRequestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\PinRequests\AutoRequest  $autoRequest
     * @return \Illuminate\Http\Response
     */
    public function show(AutoRequest $autoRequest)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\PinRequests\AutoRequest  $autoRequest
     * @return \Illuminate\Http\Response
     */
    public function edit(AutoRequest $autoRequest)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\PinRequests\AutoRequest  $autoRequest
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AutoRequest $autoRequest)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\PinRequests\AutoRequest  $autoRequest
     * @return \Illuminate\Http\Response
     */
    public function destroy(AutoRequest $autoRequest)
    {
        //
    }
}
