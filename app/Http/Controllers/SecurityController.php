<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Grids\PinsGrid;
use App\Grids\TransfersGrid;
use App\Models\Swap;
use App\Models\Transfer;
use App\Grids\SwapsGrid;
use Config;
use Illuminate\Support\Facades\Session;

class SecurityController extends Controller
{

    public function index(PinsGrid $pinGrid, Request $request)
    {
        $p = Session::get('programm', 1);
        $query = \App\Models\PinRequests\StartRequest::query()->where('program', 6)->orderBy('created_at', 'DESC');
            return $pinGrid
                    ->create(['query' => $query, 'request' => $request])
                    ->renderOn('pins.index'); // render the grid on the welcome view 
    }    

    public function transfer(TransfersGrid $transferGrid, Request $request)
    {

        $query = Transfer::query()->orderBy('created_at', 'DESC');
            return $transferGrid
                    ->create(['query' => $query, 'request' => $request])
                    ->renderOn('security.transfer'); // render the grid on the welcome view 
    }


    public function show(Request $request)
    {
        $p = Session::get('programm', 1);
        $model = \App\Models\PinRequests\StartRequest::whereId( $request->id )->first();
        $pins = [
            'start' => \App\Models\Pins\StartPin::where('request_id', $model->id)->where('request_program_id', $p)->get(),
            'auto' => \App\Models\Pins\AutoPin::where('request_id', $model->id)->where('request_program_id', $p)->get(),
            'main' => \App\Models\Pins\MainPin::where('request_id', $model->id)->where('request_program_id', $p)->get(), 
            'acummulative' => \App\Models\Pins\AcummulativePin::where('request_id', $model->id)->where('request_program_id', $p)->get(), 
            'acummulative_plus' => \App\Models\Pins\Acummulative_plusPin::where('request_id', $model->id)->where('request_program_id', $p)->get(), 
            'vip' => \App\Models\Pins\VipPin::where('request_id', $model->id)->where('request_program_id', $p)->get(), 
            'fast' => \App\Models\Pins\FastPin::where('request_id', $model->id)->where('request_program_id', $p)->get()
        ];
        return view('pins.show_sec', ['model'=>$model, 'pins' => $pins]);
    }     

    public function aprove(Request $request)
    {
 		$model = \App\Models\PinRequests\StartRequest::whereId( $request->id )->first();
		$model->security_check = $request->status; 
        $model->cancel_reason = $request->cancel_reason;
        $model->save();

        return redirect('/sb');
    }

    public function swap(SwapsGrid $grid, Request $request){

        return $grid
            ->create(['query' => Swap::query()->where('program_id', 6)->whereNull('sec_check')->orderBy('id', 'DESC'), 'request' => $request])
            ->renderOn('swap.index'); // render the grid on the welcome view

    }
}
