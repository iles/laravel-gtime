<?php

namespace App\Http\Controllers;

use App\Grids\MatrixGrid;

use App\Models\Matrix;
use App\Models\MatrixUser;
use App\Models\Partner;
use App\Models\User;
use App\Models\BlockedMatrix;
use Carbon\Carbon;
use Config;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use DB;
use Session;

class MatrixController extends Controller {



	public function index(MatrixGrid $grid, Request $request) {
		$m = Config::get('matrix.get');
		$p = Session::get('programm', 1);

		$type = $m[$p][0];

		$connection = 'mysql_prod_' . $type;
		$query = Matrix::on($connection);

		return $grid
			->create(['query' => $query, 'request' => $request])
			->renderOn('matrix.index'); // render the grid on the welcome view

	}

	public function print(Request $request) {
		$id = $request->id;
		$m = Config::get('matrix.get');
		$p = Session::get('programm', 1);

		$type = $m[$p][0];

		$connection = 'mysql_prod_' . $type;
		$matrix = MatrixUser::on($connection)->whereRaw('mx_id = ?', array($id))->orderBy('level', 'asc')->orderBy('position', 'ASC')->get();
		$res = [];
		$leader = '';
		$first = true;
		foreach ($matrix as $key => $value) {
			$res[$value->level][] = [$value->uname, $value->position, $value->uid, $value->note];
			if ($first) {
				$leader = $value->uid;
			}
			$first = false;
		}

		foreach ($res as $key => $value) {
			sort($value);
		}

		return view('matrix.print', ['matrix' => $res, 'type' => $type, 'id' => $id, 'leader' => $leader]);
	}	


	public function destroy(Request $request){

		$m = Config::get('matrix.get');
		$p = Session::get('programm', 1);

		$type = $m[$p][0];

		$connection = 'mysql_prod_' . $type;
		DB::connection($connection)->table('dle_matrix')->where('matrix_id',$request->lestnitsy)->delete();
		return new JsonResponse([
			'success' => true,
			'message' => 'Лестница удалена',
		]);
	}

	public function showfull(Request $request) {
		$id = $request->id;
		$m = Config::get('matrix.get');
		$p = Session::get('programm', 1);

		$type = $m[$p][0];

		$connection = 'mysql_prod_' . $type;
		$matrix = MatrixUser::on($connection)->whereRaw('mx_id = ?', array($id))->orderBy('level', 'asc')->orderBy('position', 'ASC')->get();
		$res = [];
		$leader = '';
		$first = true;
		foreach ($matrix as $key => $value) {
			$user = Partner::find($value->uid);
			$res[$value->level][] = [$value->uname, $value->position, $value->uid, $value->note, $user->reg_date ];
			if ($first) {
				$leader = $value->uid;
			}
			$first = false;
		}

		foreach ($res as $key => $value) {
			sort($value);
		}

		return view('matrix.show', ['matrix' => $res, 'type' => $type, 'id' => $id, 'leader' => $leader]);
	}

	public function user_print(Request $request) {

		$id = $request->id;
		$m = Config::get('matrix.get');
		$p = Session::get('programm', 1);

		$type = $m[$p][0];

		$connection = 'mysql_prod_' . $type;
		$matrix = MatrixUser::on($connection)->whereRaw('mx_id = ?', array($id))->orderBy('level', 'asc')->orderBy('position', 'ASC')->get();
		$res = [];
		$leader = '';

		$first = true;
		foreach ($matrix as $key => $value) {
			$rating = User::on($connection)->where('user_id', $value->uid)->first();
			$res[$value->level][] = [$value->uname, $value->position, $value->uid, $value->note, $rating->ref_num];
			if ($first) {
				$leader = $value->uid;
			}
			$first = false;
		}

		foreach ($res as $key => $value) {
			sort($value);
		}

		return view('matrix.print_user', ['matrix' => $res, 'type' => $type, 'id' => $id, 'leader' => $leader]);
	}

	public function swap() {
		return view('matrix.swap');
	}

	public function block(Request $request){
		$m = Config::get('matrix.get');
		$p = Session::get('programm', 1);

		$type = $m[$p][0];
		$matrix = BlockedMatrix::create([
			'matrix_id' => $request->id,
			'program_id' => $p,
		]);	

		return redirect()->back();
	}	

	public function unblock(Request $request){
		$m = Config::get('matrix.get');
		$p = Session::get('programm', 1);

		$type = $m[$p][0];
		$matrix = BlockedMatrix::where('matrix_id', $request->id );	
		$matrix ->delete();

		return redirect()->back();
	}
	public function swapspr() {
		return view('matrix.swapspr');
	}

	public function swapldr() {
		return view('matrix.swapldr');
	}

	public function doswap(Request $request) {
		$m = Config::get('matrix.get');
		$p = Session::get('programm', 1);

		$type = $m[$p][0];

		$connection = 'mysql_prod_' . $type;

		$var = $request->var == 'PP' ? 'По приказу №7' : 'По заявлению владельца';

		$partner = Partner::on($connection)->where('name', $request->login)->first();
		$partner2 = Partner::on($connection)->where('name', $request->login2)->first();
		$mid = $partner->replicate();
		$partner->info .= 'Закрывает: ' . $partner2->familiya . ';';
		$partner->info .= ' Логин: ' . $request->login2;
		$partner->info .= ' ИИН: ' . $partner2->strana;
		$partner->info .= ' Дата рождения: ' . $partner2->otchestvo;
		$partner->info .= ' Телефон: ' . $partner2->pol;
		$partner->info .= $var;
		$partner->info .= ' Дата: ' . Carbon::now();
		$partner->save();

		if($request->var == 'PZ'){
			$partner2->info .= 'Закрывает: ' . $mid->familiya . ';';
			$partner2->info .= ' Логин: ' . $request->login;
			$partner2->info .= ' ИИН: ' . $mid->strana;
			$partner2->info .= ' Дата рождения: ' . $mid->otchestvo;
			$partner2->info .= ' Телефон: ' . $mid->pol;
			$partner2->info .= $var;
			$partner2->info .= ' Дата: ' . Carbon::now();
			$partner2->save();
		}


		// $user1 = MatrixUser::on($connection)->whereRaw('uname = ?',array($request->login))->first();
		// $user2 = MatrixUser::on($connection)->whereRaw('uname = ?',array($request->login2))->first();
		// $m1 = $user1->mx_id;
		// $m2 = $user2->mx_id;
		// $user1->mx_id = $m2;
		// $user1->save();
		// $user2->mx_id = $m1;
		// $user2->save();
		Session::flash('message', 'Замена ' . $request->login . ' на ' . $request->login2 . ' прошла успешно');
		Session::flash('alert-class', 'alert-success');
		return redirect('/swap');
	}

	public function doswapmu(Request $request) {
		$m = Config::get('matrix.get');
		$p = Session::get('programm', 1);

		$type = $m[$p][0];

		$connection = 'mysql_prod_' . $type;

		$first = MatrixUser::on($connection)->where('uname', $request->login)->first();

		$second = MatrixUser::on($connection)->where('uname', $request->login2)->first();

		$partner = Partner::on($connection)->where('name', $request->login)->first();
		$mx = $partner->matrix_id;
		$partner2 = Partner::on($connection)->where('name', $request->login2)->first();

		$mid = $first->replicate();

		$first->mx_id = $second->mx_id;
		$first->level = $second->level;
		$first->position = $second->position;
		$first->save();

		$second->mx_id = $mid->mx_id;
		$second->level = $mid->level;
		$second->position = $mid->position;
		$second->save();

		$partner->matrix_id = $partner2->matrix_id;
		$partner->save();
		$partner2->matrix_id = $mx;
		$partner2->save();

		Session::flash('message', 'Замена ' . $request->login . ' на ' . $request->login2 . ' прошла успешно');
		Session::flash('alert-class', 'alert-success');
		return redirect('/swap');
	}

	public function doswapspr(Request $request) {
		$m = Config::get('matrix.get');
		$p = Session::get('programm', 1);
		$type = $m[$p][0];

		$connection = 'mysql_prod_' . $type;
		$user = User::on($connection)->whereRaw('name = ?', array($request->login))->first();
		$sponsor = User::on($connection)->whereRaw('name = ?', array($request->login2))->first();
		$user->by_refer = $sponsor->user_id;
		$user->save();
		Session::flash('message', 'Замена спонсора прошла успешно');
		Session::flash('alert-class', 'alert-success');
		return redirect('/swapspr');
	}

	public function usercheck(Request $request) {
		$m = Config::get('matrix.get');
		$p = Session::get('programm', 1);

		$type = $m[$p][0];

		$connection = 'mysql_prod_' . $type;

		$user = MatrixUser::on($connection)->whereRaw('uname = ?', array($request->login))->first();

		if ($user) {
			return ['status' => 1, 'position' => $user->position, 'level' => $user->level, 'mx_id' => $user->mx_id, 'message' => '№ лестницы - ' . $user->mx_id . '; Уровень - ' . $user->level . '; Позиция - ' . $user->position];
		} else {
			return ['status' => 0, 'message' => 'Пользовтеля с именем ' . $request->login . ' не существует'];
		}
	}

	public function sponsorcheck(Request $request) {
		$m = Config::get('matrix.get');
		$p = Session::get('programm', 1);

		$type = $m[$p][0];

		$connection = 'mysql_prod_' . $type;

		$user = User::on($connection)->whereRaw('name = ?', array($request->login))->first();
		if (!$user) {
			return ['status' => 0, 'message' => 'Пользовтеля с именем ' . $request->login . ' не существует'];
		}
		$sponsor = User::on($connection)->whereRaw('user_id = ?', array($user->by_refer))->first();

		return ['status' => 1, 'message' => $sponsor->name];

	}

	public function mu_swap() {
		return view('matrix.mu_swap');
	}

	public function change_leader(Request $request) {

		$m = Config::get('matrix.get');
		$p = Session::get('programm', 1);

		$type = $m[$p][0];

		$connection = 'mysql_prod_' . $type;
		$user = MatrixUser::on($connection)->where(['mx_id' => $request->mx_id, 'level' => 1, 'position' => 1])->first();

		$upd = MatrixUser::on($connection)->where('uid', $request->new_leader)->first();

		$user->update(['note' => $upd->uname]);

		return $upd->uname;

		// $matrix = MatrixUser::on($connection)->where(['mx_id'=>$request->mx_id])->orderBy('level', 'asc')->orderBy('position', 'ASC')->get();
		// $begin_shift = false;
		// $n = count($matrix);
		// $new_leader_name = '';
		// $old_leader_name = '';
		// foreach ($matrix as $key => $value) {

		//     if($value->level == 1 && $value->position == 1){
		//         $old_leader_name = $value->uname;
		//     }

		//     if($key == $n - 1 ){//last el
		//         break;
		//     }
		//     if($value->uid == $request->new_leader){//сдвиг только последующих элементов
		//         $begin_shift = true;
		//     }
		//     if($begin_shift){
		//         $next_user = $matrix[$key + 1];

		//         $next_update = MatrixUser::on($connection)->where('uid', $next_user->uid)->update(['level' => $value->level, 'position' => $value->position ]);
		//     }
		// }

		// $upd = MatrixUser::on($connection)->where('uid',$request->new_leader)->first();
		// $new_leader_name = $upd->uname;
		// $upd->update(['level'=>1, 'position'=>1]);

		// $old = MatrixUser::on($connection)->where(['uid'=> $request->old_leader]);
		// $old->delete();
		// //insert to matrix change log old leader, new leader, current date
		// $history = new ChangeLeaderHistory();
		// $history->programm = $request->program;
		// $history->mx_id = $request->mx_id;
		// $history->old_leader = $request->old_leader;
		// $history->new_leader = $request->new_leader;
		// $history->old_leader_name = $old_leader_name;
		// $history->new_leader_name = $new_leader_name;
		// $history->save();
		// //commit?

		return true;
	}

}
