<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Translations;

class TranlationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $list = Translations::getList();
        $langlist = config('app.locales');
        // dd($setting);
        return view('admin.translations.index', [
            'list' => $list, 
            'langlist' => $langlist
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function save(Request $request)
    {   
        // dd($request->input());
        Translations::saveItems($request->input());
        return redirect()->route('admin.translation.index')->with('success', 'Изменение успешно сохранен');;
    }
}
