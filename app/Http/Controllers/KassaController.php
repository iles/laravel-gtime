<?php

namespace App\Http\Controllers;

use App\Models\Pins\AutoPin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Grids\PinsKassaGrid;
use App\Models\BonusRequest;
use Session;
use Config;

class KassaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(PinsKassaGrid $pinGrid, Request $request)
    {
        $p = Session::get('programm', 1);
        switch ($p) {
            case 1:
                $query = \App\Models\PinRequests\StartRequest::query()->where('kassa', 1)->orderBy('id', 'DESC');
                break;            
            case 2:
                $query = \App\Models\PinRequests\AutoRequest::query()->where('kassa', 1)->orderBy('id', 'DESC');
                break;            
            case 3:
                $query = \App\Models\PinRequests\MainRequest::query()->where('kassa', 1)->orderBy('id', 'DESC');
                break;            
            case 4:
                $query = \App\Models\PinRequests\AcummulativeRequest::query()->where('kassa', 1)->orderBy('id', 'DESC');
                break;            
            case 5:
                $query = \App\Models\PinRequests\Acummulative_plusRequest::query()->where('kassa', 1)->orderBy('id', 'DESC');
                break;            
            case 6:
                $query = \App\Models\PinRequests\VipRequest::query()->where('kassa', 1)->orderBy('id', 'DESC');
                break;            
            case 7:
                $query = \App\Models\PinRequests\FastRequest::query()->where('kassa', 1)->orderBy('id', 'DESC');
                break;
            
            default:
                # code...
                break;
        }
            return $pinGrid
                    ->create(['query' => $query, 'request' => $request])
                    ->renderOn('kassa.index'); // render the grid on the welcome view
    } 

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Pins\AutoPin  $autoPin
     * @return \Illuminate\Http\Response
     */
    public function show(AutoPin $autoPin)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Pins\AutoPin  $autoPin
     * @return \Illuminate\Http\Response
     */
    public function edit(AutoPin $autoPin)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Pins\AutoPin  $autoPin
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AutoPin $autoPin)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Pins\AutoPin  $autoPin
     * @return \Illuminate\Http\Response
     */
    public function destroy(AutoPin $autoPin)
    {
        //
    }

    public function checkLogin(Request $request){
        $res = [];

        $m = Config::get('matrix.get');
        $p = Session::get('programm', 1);
        $pr = $request->program;

        $progs = [];
        $connection = 'mysql_prod_'.$m[$pr][0];
        $user = BonusRequest::where('uname', $request->name)->first();
        if( $user ){
            $res['error'] = 0;
            $res['m'] = $user->familiya;
            $res['data'] = $user;
            return $res;
        } else {
            $res['error'] = 1;
            $res['m'] = 'Пользователь не найден';
            return $res;
        }
    }
}
