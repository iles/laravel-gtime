<?php

namespace App\Http\Controllers;

use App\Models\Bonus;
use App\Models\BonusRequest;
use App\Models\Finished;
use App\Models\Matrix;
use App\Models\MatrixUser;
use App\Models\News;
use App\Models\Partner;
use App\Models\Pin;
use App\Models\StartPinRequest;
use App\Models\Swap;
use App\Models\FastCodes;
use App\Models\BlockedMatrix;
use App\Models\Invite;

use App\Models\Transfer;
use Auth;
use Carbon\Carbon;
use Config;
use DB;
use Illuminate\Http\Request;
use Session;

class FrontController extends Controller {
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct() {
		$this->middleware('auth:partner')->except('logout');
		$this->middleware('finish')->except(['finish', 'bonusadd', 'rebonus', 'finish_pin', 'addTransfer', 'finish_bonus', 'pinadd']);
	}

	public function ladder() {

		$m = Config::get('matrix.get');
		$p = Session::get('programm', 1);

		$out = News::where('type', 1)->where('program_id', $p)->orderBy('id', 'desc')->first();
		$in = News::where('type', 2)->where('program_id', $p)->orderBy('id', 'desc')->first();

		$m = Config::get('matrix.get');
		$p = Session::get('programm', 1);

		$type = $m[$p][0];
		$connection = 'mysql_prod_' . $type;

		$res = null;
		$mx_user = MatrixUser::on($connection)->where('uname', Auth::user()->name)->first();

		$stage = null;

		if ($mx_user) {
			$m = Matrix::on($connection)->where('matrix_id', $mx_user->mx_id)->first();
			$stage = $m->stage;
			$matrix = MatrixUser::on($connection)
				->whereRaw('mx_id = ?', array($mx_user->mx_id))
				->orderBy('level', 'asc')
				->leftJoin('dle_users', 'dle_matrix_users.uid', '=', 'dle_users.user_id')
				->orderBy('position', 'ASC')
				->get();
			$res = [];

			foreach ($matrix as $key => $value) {
				$res[$value->level][] = [
					'uname' => $value->uname,
					'stars' => $value->ref_num,
					'by_refer' => $this->getName($value->by_refer),
				];
			}

			foreach ($res as $key => $value) {
				sort($value);
			}

		}


		if ($res) {
			return view('front.ladder', compact('in', 'out', 'res', 'stage'));
		}

	}

	public function reswap(Request $request){
		$swap = Swap::find($request->id);
		$swap->view = 1;
		$swap->save();
		return redirect()->back();
	}

	public function swapuser() {
		$swap = Swap::where('uid', Auth::user()->user_id)->where('program_id', Session::get('programm', 1))->where('view', 0)->first();
		return view('front.swap', compact('swap'));
	}

	public function doswapuser(Request $request) {
		$m = Config::get('matrix.get');
		$p = Session::get('programm', 1);
		
		$partner1 = Partner::where('name', $request->login)->first();

		if(!$partner1){
			return redirect()->back()->with('message', 'Пользователь '.$request->login.' не найден');
		}

		$partner2 = Partner::where('name', $request->login2)->first();
		if(!$partner2){
			return redirect()->back()->with('message', 'Пользователь '.$request->login2.' не найден');
		}

		if($request->type == 3){			
			$partner2 = Partner::where('name', $request->login2)->first();
			$stage = DB::connection($connection)->table('dle_matrix')->select('stage')->where('matrix_id', $partner2->matrix_id)->first();
			if($stage->stage == 0){
				return redirect()->back()->with('message', 'Спонсор должен находится в финишной лестнице');
			}
		}

		$swap = new Swap;
		$swap->uid = Auth::user()->user_id;
		$swap->program_id = Session::get('programm', 1);
		$swap->fill($request->all());

		if ($request->hasFile('images')) {
			$imgfname = [];

			foreach ($request->images as $key => $image) {
				$imageName = time() . $key . '.' . $image->getClientOriginalExtension();
				$image->move(public_path('uploads/applications/'), $imageName);
				$imgfname[] = $imageName;
			}

			$swap->image = implode(';', $imgfname);
		}

		$swap->save();
		return redirect()->back()->with('message', 'Заявка созданна!');
	}

	public function finish() {

		$m = Config::get('matrix.get');
		$p = Session::get('programm', 1);

		$progs = [];
		foreach ($m as $key => $value) {
			$progs[$key] = $value[1];
		}
		$payment = null;

		//return view('front.finish.start', compact('progs', 'payment'));

		$finished = Finished::where('user_id', Auth::user()->user_id)->where('program_id', $p)->first();

		if (!$finished) {
			return redirect('/partner/ladder');
		};

		$m = Config::get('matrix.get');
		$p = Session::get('programm', 1);
		app('App\Http\Controllers\Programs\Program' . ucfirst($m[$p][0]) . 'Controller')->finish();
	}

	public function bonusadd(Request $request) {



		$finished = Finished::where('user_id', Auth::user()->user_id)->first();
		if ($finished) {
		$m = Config::get('matrix.get');

		$connection = 'mysql_prod_'.$m[$finished->program_id][0];

		switch ($finished->program_id) {
            case 1:
                $table = 'dle_drop_users_start';
                break;
            case 2:
                $table = 'dle_drop_users';
                break;
            case 3:
                if ($finished->stage == 1) {
                    $table = 'dle_drop_users80';
                } else {
                    $table = 'dle_drop_users500';
                }
                break;
            case 4:
                $table = 'dle_drop_users';
                break;
            case 5:
                $table = 'dle_drop_users';
                break;
            case 6:
                $table = 'dle_drop_users9000';
                break;
            case 7:
                $table = 'dle_drop_users';
                break;
            case 8:
                $table = 'dle_drop_users9000';
                break;
            default:
                # code...
                break;
            }

            $drop = DB::connection($connection)->table($table)->where('name', $finished->login)->first();
            if( $drop->setr !== 0){
            	return redirect()->back()->with('message', 'Ошибка');
            }
            

			$params = $request->all();
			$m = Config::get('matrix.get');
			$p = Session::get('programm', 1);

			$pr = null;

			$bonus = \App\Models\Bonuses\StartBonusRequestModel::create($params);
			$bonus->program_id = $p;
			$bonus->finished_id = $finished->id;
			$bonus->fullname = Auth::user()->familiya;
			foreach ($m as $key => $value) {
				$progs[$key] = $value[1];
			}

			if ($request->hasFile('images')) {
				$imgfname = [];
				foreach ($request->images as $key => $image) {
					$imageName = time() . $key . '.' . $image->getClientOriginalExtension();
					$image->move(public_path('uploads/scans/'), $imageName);
					$imgfname[] = $imageName;

				}

				$bonus->image = implode(';', $imgfname);
			}
				$bonus->save();

			$finished->bonus = $bonus->id;
			$finished->variant = $request->variant;
			$finished->save();
			if($finished->stage == 1){
				if($finished->program_id == 3){
					return redirect('/partner/main/finish');
				} else {
					return redirect('/partner/main/finish');					
				}
			}
		return redirect(route('front.finish'));
		}

	}

	public function vipbonus() {
		$bonus = \App\Models\Bonuses\StartBonusRequestModel::where('vip_bonus', 1)->where('uid', Auth::user()->user_id )->where('view', 0)->first();
		return view('front.vipbonus', compact('bonus') );
	}

	public function bonusform() {
		$invites = \App\Models\Invite::where('program_id', 8)->where('user_id', Auth::user()->user_id )->get();
		//$bonus = \App\Models\Bonuses\StartBonusRequestModel::where('vip_bonus', 1)->where('uid', Auth::user()->user_id )->where('view', 0)->first();
		return view('front.bonus-form', compact('invites') );
	}


	public function vipbonusadd(Request $request) {

		$params = $request->all();
		$m = Config::get('matrix.get');
		$p = Session::get('programm', 1);

		$pr = null;

		$bonus = \App\Models\Bonuses\StartBonusRequestModel::create($params);

		$bonus->program_id = $p;
		$bonus->vip_bonus = 1;
		$bonus->save();

		foreach ($m as $key => $value) {
			$progs[$key] = $value[1];
		}

		if ($request->hasFile('images')) {
			$imgfname = [];
			foreach ($request->images as $key => $image) {
				$imageName = time() . $key . '.' . $image->getClientOriginalExtension();
				$image->move(public_path('uploads/scans/'), $imageName);
				$imgfname[] = $imageName;

			}

			$bonus->image = implode(';', $imgfname);
			$bonus->save();
		}

		return redirect()->back();

	}

	public function bonusbonusadd(Request $request) {

		$params = $request->all();
		$bonus = \App\Models\Bonuses\StartBonusRequestModel::create($params);
		$bonus->fullname = Auth::user()->familiya;

		$bonus->program_id = 8;

		$bonus->save();

		$invite = Invite::find($request->invite_id);
		$invite->open = 1;
		$invite->bonus_id = $bonus->id;
		$invite->save();



		if ($request->hasFile('images')) {
			$imgfname = [];
			foreach ($request->images as $key => $image) {
				$imageName = time() . $key . '.' . $image->getClientOriginalExtension();
				$image->move(public_path('uploads/scans/'), $imageName);
				$imgfname[] = $imageName;

			}

			$bonus->image = implode(';', $imgfname);
			$bonus->save();
		}

		return redirect()->back();

	}

	public function rebonus(Request $request) {

		$bonus = \App\Models\Bonuses\StartBonusRequestModel::whereId($request->id)->first();

		$bonus->view = 1;
		$bonus->save();
		return redirect()->back();
	}


	public function rebonusvip(Request $request) {

		$bonus = \App\Models\Bonuses\StartBonusRequestModel::whereId($request->id)->first();

		$bonus->status_id = 3;
		$bonus->view = 1;
		$bonus->save();
		return redirect()->back();
	}

	public function pinadd(Request $request) {

		$finished = Finished::where('user_id', Auth::user()->user_id)->first();

		if ($finished) {

			$m = Config::get('matrix.get');
			$p = Session::get('programm', 1);

			$pinRequest = new \App\Models\PinRequests\StartRequest;

			$pinRequest->uname = Auth::user()->name;
			$pinRequest->bill = 0;
			$pinRequest->iin = 0;
			$pinRequest->summ = 0;
			$pinRequest->fullname = 0;
			$pinRequest->date = 0;
			$pinRequest->country = 0;
			$pinRequest->skype = 0;
			$pinRequest->note = 0;
			$pinRequest->kassa = 0;
			$pinRequest->transitive = 1;
			$pinRequest->status_id = 0;
			$pinRequest->user_id = Auth::user()->user_id;
			$pinRequest->program = 0;
			$pinRequest->image = 0;
			$pinRequest->save();

			if ($request->triple) {
				$pin1 = new \App\Models\Pins\StartPin;
				$pin1->user_id = Auth::user()->user_id;
				$pin1->request_id = $pinRequest->id;
				$pin1->request_program_id = $p;
				$pin1->transitive = 1;
				$pin1->pin = $this->generatePIN(4, 3);
				$pin1->active_at = Carbon::now()->toDateTimeString();
				$date = Carbon::now()->addDays(2)->toDateTimeString();
				$pin1->expired_at = $date;
				$pin1->program = 4;

				while (true) {
					try {
						$pin1->save();
						break;
					} catch (\Illuminate\Database\QueryException $e) {
						$pin1->pin = $this->generatePIN(4, 3);
						continue;
					}
				};

				$finished->pin = json_encode(['program_id' => 4, 'pin' => $pin1->id]);

				$pin2 = new \App\Models\Pins\StartPin;
				$pin2->user_id = Auth::user()->user_id;
				$pin2->request_id = $pinRequest->id;
				$pin2->request_program_id = $p;
				$pin2->transitive = 1;
				$pin2->pin = $this->generatePIN(7, 3);
				$pin2->active_at = Carbon::now()->toDateTimeString();
				$pin2->program = 7;
				$pin2->expired_at = $date;
				while (true) {
					try {
						$pin2->save();
						break;
					} catch (\Illuminate\Database\QueryException $e) {
						$pin2->pin = $this->generatePIN(7, 2);
						continue;
					}
				};
				$finished->pin2 = json_encode(['program_id' => 7, 'pin' => $pin2->id]);

				$pin3 = new \App\Models\Pins\StartPin;
				$pin3->user_id = Auth::user()->user_id;
				$pin3->request_id = $pinRequest->id;
				$pin3->request_program_id = $p;
				$pin3->program = 7;
				$pin3->transitive = 1;
				$pin3->pin = $this->generatePIN(7, 3);
				$pin3->active_at = Carbon::now()->toDateTimeString();
				$pin3->expired_at = $date;
				while (true) {
					try {
						$pin3->save();
						break;
					} catch (\Illuminate\Database\QueryException $e) {
						$pin3->pin = $this->generatePIN(7, 2);
						continue;
					}
				};
				$finished->pin3 = json_encode(['program_id' => 7, 'pin' => $pin3->id]);
				$finished->variant = 3;
				$finished->save();

				$fastcodes = new FastCodes;
				$fastcodes->pin1 = $pin2->pin;
				$fastcodes->pin2 = $pin3->pin;
				$fastcodes->save();

				return redirect(route('front.finish'));
			}

			$program_id = $request->program_id;
			$program_type = $request->program_type;

			$pin = new \App\Models\Pins\StartPin;

			$pin->user_id = Auth::user()->user_id;
			$pin->request_id = $pinRequest->id;
			$pin->request_program_id = $p;
			$pin->transitive = 1;
			$pin->pin = $this->generatePIN($program_id, $program_type);
			$pin->active_at = Carbon::now()->toDateTimeString();
			$date = Carbon::now()->addDays(2)->toDateTimeString();
			$pin->expired_at = $date;
			$pin->program = $program_id;
			while (true) {
				try {
					$pin->save();
					break;
				} catch (\Illuminate\Database\QueryException $e) {
					$pin->pin = $this->generatePIN($program_id, $program_type);
					continue;
				}
			};

			$m = Config::get('matrix.get');
			$progs = [];
			foreach ($m as $key => $value) {
				$progs[$key] = $value[1];
			}

			if ($request->pin_number) {
				if ($request->pin_number == 2) {
					$finished->pin2 = json_encode(['program_id' => $program_id, 'pin' => $pin->id]);
				} else {
					$finished->pin3 = json_encode(['program_id' => $program_id, 'pin' => $pin->id]);
				}
			} else {
				$finished->pin = json_encode(['program_id' => $program_id, 'pin' => $pin->id]);
			}

			if ($request->variant) {
				$finished->variant = $request->variant;
				$finished->save();
			}

			if ($request->bonus_id) {
				switch ($request->bonus_program_id) {
				case 1:
					$bonus = \App\Models\Bonuses\StartBonusRequestModel::whereId($request->bonus_id)->first();
					break;
				case 2:
					$bonus = \App\Models\Bonuses\AutoBonusRequestModel::whereId($request->bonus_id)->first();
					break;
				case 3:
					$bonus = \App\Models\Bonuses\MainBonusRequestModel::whereId($request->bonus_id)->first();
					break;
				case 6:
					$bonus = \App\Models\Bonuses\VipBonusRequestModel::whereId($request->bonus_id)->first();
					break;
				case 4:
					$bonus = \App\Models\Bonuses\AccumulativeBonusRequestModel::whereId($request->bonus_id)->first();
					break;
				case 5:
					$bonus = \App\Models\Bonuses\AccumulativePlusBonusRequestModel::whereId($request->bonus_id)->first();
					break;
				case 7:
					$bonus = \App\Models\Bonuses\FastBonusRequestModel::whereId($request->bonus_id)->first();
					break;

				default:
					$bonus = \App\Models\Bonuses\StartBonusRequestModel::whereId($request->bonus_id)->first();
					break;
				}

				$bonus->secondPin = $pin->id;
				$bonus->save();
			}

			$finished->save();
			return redirect()->back();
		}
	}

	public function registration_rules() {
		//$pin = Pin::where('user_id',  Auth::user()->user_id )->where('status_id', 1)->whereDate('expired_at', '>', Carbon::today()->toDateTimeString() )->first();

		return view('front.registration_rules');
	}

	public function registration() {
		return view('front.registration');
	}


	public function register_partner(Request $request) {


		$m = Config::get('matrix.get');
		$p = Session::get('programm', 1);

		$pin = \App\Models\Pins\StartPin::wherePin($request->pin)->first();

				$m = Config::get('matrix.get');
		$p = Session::get('programm', 1);

		$type = $m[$p][0];
		$connection = 'mysql_prod_' . $type;

		if (!$pin) {
			return false;
		} elseif ($pin->status_id == 3) {
			return false;
		} elseif ($pin->status_id == 2) {
			return false;
		}

		$pin_request = new StartPinRequest;

		$json = json_encode($request->json);

		$pin_request->json = $json;
		$pin_request->pin = $request->pin;
		$pin_request->user_id = Auth::id();
		$pin_request->status = 1;
//        $pin_request->program_id = $p;
		$pin_request->uname = Auth::user()->name;

		$pin_request->save();

	 	$data = json_decode( $pin_request->json, true );
        if( isset($data['sponsor']) ){
            $sponsor = Partner::on($connection)->where('name', $data['sponsor'] )->first();    
        } else {
            $sponsor = Partner::on($connection)->where('user_id', $model->user_id )->first();
        }

        $block = BlockedMatrix::where('matrix_id', $sponsor->matrix_id)->first();


        if($block){
            return redirect('/partner/registration')->with('message', 'Ошибка регитсрации, обратитесь в администрацию');
        }


		app('App\Http\Controllers\PartnerController')->addPartner($pin_request);

		$pin_request->status = 3;
		$pin_request->save();

		Session::flash('message', 'Заявка созданна');
		Session::flash('alert-class', 'alert-sucscess');
		return redirect('partner/ladder');

	}

	private function generatePIN($p, $t) {
		// Available alpha caracters
		$characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';

		// generate a pin based on 2 * 7 digits + a random character
		$pin = mt_rand(1000000, 9999999)
		. mt_rand(1000000, 9999999)
			. $characters[rand(0, strlen($characters) - 1)]
			. $characters[rand(0, strlen($characters) - 1)]
			. $characters[rand(0, strlen($characters) - 1)];

		// shuffle the result
		$pin = str_shuffle($pin);
		if ($p == 1) {
			if ($t == 1) {
				return 'N' . 'STR' . $str = substr($pin, 0, 12);
			} else {
				return 'STR' . substr($pin, 0, 12) . 'KSAN';
			}
		} elseif ($p == 3) {
			if ($t == 1) {
				return 'N' . 'MAIN' . $str = substr($pin, 0, 10);
			} elseif ($t == 2) {
				return 'MAIN' . substr($pin, 0, 8) . 'KSSAN';
			} else {
				return 'MAIN' . substr($pin, 0, 8) . 'WWDN';
			}
		} elseif ($p == 4) {
			if ($t == 1) {
				return 'F' . 'BNK' . $str = substr($pin, 0, 10);
			} elseif ($t == 2) {
				return 'BNK' . substr($pin, 0, 8) . 'KSAF';
			} else {
				return 'BNK' . substr($pin, 0, 8) . 'WWF';
			}
		} elseif ($p == 5) {
			if ($t == 1) {
				return 'N' . 'PLS' . $str = substr($pin, 0, 14);
			} elseif ($t == 2) {
				return 'PLS' . substr($pin, 0, 14) . 'KSAN';
			}
		} elseif ($p == 6) {
			if ($t == 1) {
				return 'N' . 'VIP' . $str = substr($pin, 0, 12);
			} elseif ($t == 2) {
				return 'VIP' . substr($pin, 0, 9) . 'KSSAN';
			} else {
				return 'VIP' . substr($pin, 0, 9) . 'WWDN';
			}
		} elseif ($p == 7) {
			if ($t == 1) {
				return 'N' . 'FAST' . $str = substr($pin, 0, 9);
			} elseif ($t == 2) {
				return 'VIP' . substr($pin, 0, 8) . 'KSSAN';
			} else {
				return 'N' . 'FAST' . $str = substr($pin, 0, 8) . 'SSDF';
			}
		} elseif ($p == 8) {
			if ($t == 1) {
				return 'N' . 'BON' . $str = substr($pin, 0, 12);
			} elseif ($t == 2) {
				return 'BON' . substr($pin, 0, 12) . 'KSSAN';
			} else {
				return 'BON' . $str = substr($pin, 0, 12) . 'WWDN';
			}
		}
	}

	/**
	 * Show the application dashboard.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		return view('home');
	}

	public function main_finish() {
		$finished = Finished::where('user_id', Auth::user()->user_id)->where('program_id', 3)->where('stage', 1)->first();
		$transfer = Transfer::where('login', Auth::user()->name )->where('program_id', 3)->where('status_id', '<>', 3)->first();
		if ($finished) {
			$bonus = \App\Models\Bonuses\StartBonusRequestModel::where('uid', Auth::user()->user_id)->first();
			return view('front.finish_main', compact('finished', 'bonus', 'transfer'));
		} else {
			return redirect(route('ladder'));
		}
	}	

	public function bonus_finish() {
		$finished = Finished::where('user_id', Auth::user()->user_id)->where('program_id', 8)->where('stage', 1)->first();
		$transfer = Transfer::where('login', Auth::user()->name )->where('program_id', 8)->where('status_id', '<>', 3)->first();
		if ($finished) {
			$bonus = \App\Models\Bonuses\StartBonusRequestModel::where('uid', Auth::user()->user_id)->first();
			return view('front.finish_bonus_program', compact('finished', 'bonus', 'transfer'));
		} else {
			return redirect(route('ladder'));
		}
	}

	public function vip_finish() {
		$mf = Finished::where('user_id', Auth::user()->user_id)->where('program_id', 6)->where('stage', 1)->first();
		if ($mf) {
			return view('front.finish_vip', compact('mf'));
		} else {
			return redirect(route('partner.ladder'));
		}
	}

	public function structure() {
		$m = Config::get('matrix.get');
		$p = Session::get('programm', 1);

		$type = $m[$p][0];
		$connection = 'mysql_prod_' . $type;

		$mx_user = MatrixUser::on($connection)->where('uname', Auth::user()->name)->first();

		if (!$mx_user) {
			return 'Пользователь не найден';
		}

		$matrix = MatrixUser::on($connection)->whereRaw('mx_id = ?', array($mx_user->mx_id))->orderBy('level', 'asc')->orderBy('position', 'ASC')->get();
		$res = [];

		foreach ($matrix as $key => $value) {
			$res[$value->level][] = [$value->uname, $value->note];
		}

		foreach ($res as $key => $value) {
			sort($value);
		}

		return view('front.structure', compact('res'));
	}

	public function checkLogin(Request $request) {
		$res = [];

		$m = Config::get('matrix.get');
		$p = Session::get('programm', 1);
		$pr = $request->program;
		$progs = [];
		$connection = 'mysql_prod_' . $m[$pr][0];
		$user = Partner::on($connection)->where('name', $request->name)->first();
		if ($user) {
			$res['error'] = 1;
			$res['m'] = 'Логин занят';
		} else {
			$res['error'] = 0;
			$res['m'] = 'Логин свободен';
		}
		return $res;
	}

	public function checkSponsor(Request $request) {
		$res = [];

		$m = Config::get('matrix.get');
		$p = Session::get('programm', 1);
		$pr = $request->program;
		$progs = [];
		$connection = 'mysql_prod_' . $m[$pr][0];
		$user = Partner::on($connection)->where('name', $request->name)->first();
		$s = 0;
		if ($p == 3 || $p == 6 || $p == 3 || $p == 8) {
			$stage = DB::connection($connection)->table('dle_matrix')->select('stage')->where('matrix_id', $user->matrix_id)->first();
			$s = $stage->stage;
		}
		if ($user) {
			$res['error'] = 0;
			$res['m'] = 'Логин действителен';
		} else {
			$res['error'] = 1;
			$res['m'] = 'Логина не существует';
		}

		if ($s == 1) {
			$res['error'] = 1;
			$res['m'] = 'Логин не действителен';
		}
		return $res;
	}

	public function username(Request $request) {
		$res = [];

		$m = Config::get('matrix.get');
		$p = Session::get('programm', 1);

		$progs = [];
		$connection = 'mysql_prod_' . $m[$p][0];
		$user = Partner::on($connection)->where('name', $request->username)->first();
		if ($user) {
			$res['error'] = 0;
			$res['m'] = $user->user_id;
		} else {
			$res['error'] = 1;
			$res['m'] = 'Логин свободен';
		}
		return $res;
	}

	public function checkPin(Request $request) {
		$p = Session::get('programm', 1);

		$res = [];

		$pin = \App\Models\Pins\StartPin::wherePin($request->pin)->first();

		if ($p != $pin->program) {
			$res['error'] = 1;
			$res['m'] = 'Пин код не действителен';
			return $res;
		}

		if ($pin->program == 2) {
			if (!$pin) {
				$res['error'] = 1;
				$res['m'] = 'Пин код не действителен';
			} elseif ($pin->status_id == 2 || $pin->status_id == 3) {
				$res['error'] = 1;
				$res['m'] = 'Пин код заблокирован';
			} else {
				$res['error'] = 0;
				$res['m'] = 'Пин код действителен';
			}
			return $res;
		}

		if (!$pin) {
			$res['error'] = 1;
			$res['m'] = 'Пин код не действителен';
		} elseif ($pin->status_id == 2) {
			$res['error'] = 1;
			$res['m'] = 'Пин код заблокирован';
		} elseif ($pin->status_id == 3) {
			$res['error'] = 1;
			$res['m'] = 'Пин код не действителен';
		} else {
			$res['error'] = 0;
			$res['m'] = 'Пин код действителен';
		}
		return $res;
	}

	public function pin() {
		$p = Session::get('programm', 1);

		$pin_requests = \App\Models\PinRequests\StartRequest::where('user_id', Auth::user()->user_id)->where('status_id', '!=', 3)->orderBy('id', 'desc')->get();

		$m = Config::get('matrix.get');
		$p = Session::get('programm', 1);
		$program = $m[$p][1];
		$code = null;

		$pins = [];
		if ($pin_requests) {
			foreach ($pin_requests as $key => $value) {
				$value->pins['start'] = \App\Models\Pins\StartPin::where('request_id', $value->id)->where('request_program_id', $p)->where('status_id', '!=', 3)->get()->toArray();
				$value->pins['auto'] = \App\Models\Pins\AutoPin::where('request_id', $value->id)->where('request_program_id', $p)->where('status_id', '!=', 3)->get()->toArray();
				$value->pins['main'] = \App\Models\Pins\MainPin::where('request_id', $value->id)->where('request_program_id', $p)->where('status_id', '!=', 3)->get()->toArray();
				$value->pins['acummulative'] = \App\Models\Pins\AcummulativePin::where('request_id', $value->id)->where('request_program_id', $p)->where('status_id', '!=', 3)->get()->toArray();
				$value->pins['acummulative_plus'] = \App\Models\Pins\Acummulative_plusPin::where('request_id', $value->id)->where('request_program_id', $p)->where('status_id', '!=', 3)->get()->toArray();
				$value->pins['vip'] = \App\Models\Pins\VipPin::where('request_id', $value->id)->where('request_program_id', $p)->where('status_id', '!=', 3)->get()->toArray();
				$value->pins['fast'] = \App\Models\Pins\FastPin::where('request_id', $value->id)->where('request_program_id', $p)->where('status_id', '!=', 3)->get()->toArray();
				$value->pins['total'] = count($value->pins['start']) + count($value->pins['auto']) + count($value->pins['main']) + count($value->pins['acummulative']) + count($value->pins['acummulative_plus']) + count($value->pins['vip']) + count($value->pins['fast']);
			}
		}

		return view('front.pin', compact('pins', 'program', 'pin_requests'));
	}

	public function pin_requests() {
		$p = Session::get('programm', 1);

		$requests = \App\Models\PinRequests\StartRequest::where('user_id', Auth::user()->user_id)->orderBy('id', 'desc')->get();

		return view('front.pin_requests', compact('requests', 'p'));
	}

	public function create_request() {
		$countries = [4 => "Казахстан", 1 => "Россия", 11 => "Кыргызстан", 2 => "Украина", 19 => "Австралия", 20 => "Австрия", 5 => "Азербайджан", 21 => "Албания", 22 => "Алжир", 23 => "Американское Самоа", 24 => "Ангилья", 25 => "Ангола", 26 => "Андорра", 27 => "Антигуа и Барбуда", 28 => "Аргентина", 6 => "Армения", 29 => "Аруба", 30 => "Афганистан", 31 => "Багамы", 32 => "Бангладеш", 33 => "Барбадос", 34 => "Бахрейн", 3 => "Беларусь", 35 => "Белиз", 36 => "Бельгия", 37 => "Бенин", 38 => "Бермуды", 39 => "Болгария", 40 => "Боливия", 235 => "Бонайре => Синт-Эстатиус и Саба", 41 => "Босния и Герцеговина", 42 => "Ботсвана", 43 => "Бразилия", 44 => "Бруней", 45 => "Буркина-Фасо", 46 => "Бурунди", 47 => "Бутан", 48 => "Вануату", 233 => "Ватикан", 49 => "Великобритания", 50 => "Венгрия", 51 => "Венесуэла", 52 => "Виргинские острова => Великобритания", 53 => "Виргинские острова => США", 54 => "Восточный Тимор", 55 => "Вьетнам", 56 => "Габон", 57 => "Гаити", 58 => "Гайана", 59 => "Гамбия", 60 => "Гана", 61 => "Гваделупа", 62 => "Гватемала", 63 => "Гвинея", 64 => "Гвинея-Бисау", 65 => "Германия", 236 => "Гернси", 66 => "Гибралтар", 67 => "Гондурас", 69 => "Гренада", 70 => "Гренландия", 71 => "Греция", 7 => "Грузия", 72 => "Гуам", 73 => "Дания", 237 => "Джерси", 231 => "Джибути", 74 => "Доминика", 75 => "Доминиканская Республика", 76 => "Египет", 77 => "Замбия", 78 => "Западная Сахара", 79 => "Зимбабве", 8 => "Израиль", 80 => "Индия", 81 => "Индонезия", 82 => "Иордания", 83 => "Ирак", 84 => "Иран", 85 => "Ирландия", 86 => "Исландия", 87 => "Испания", 88 => "Италия", 89 => "Йемен", 90 => "Кабо-Верде", 91 => "Камбоджа", 92 => "Камерун", 10 => "Канада", 93 => "Катар", 94 => "Кения", 95 => "Кипр", 96 => "Кирибати", 97 => "Китай", 98 => "Колумбия", 99 => "Коморы", 100 => "Конго", 101 => "Конго => демократическая республика", 102 => "Коста-Рика", 103 => "Кот-д'Ивуар", 104 => "Куба", 105 => "Кувейт", 138 => "Кюрасао", 106 => "Лаос", 12 => "Латвия", 107 => "Лесото", 108 => "Либерия", 109 => "Ливан", 110 => "Ливия", 13 => "Литва", 111 => "Лихтенштейн", 112 => "Люксембург", 113 => "Маврикий", 114 => "Мавритания", 115 => "Мадагаскар", 118 => "Малави", 119 => "Малайзия", 120 => "Мали", 121 => "Мальдивы", 122 => "Мальта", 123 => "Марокко", 124 => "Мартиника", 125 => "Маршалловы Острова", 126 => "Мексика", 127 => "Микронезия => федеративные штаты", 128 => "Мозамбик", 15 => "Молдова", 129 => "Монако", 130 => "Монголия", 131 => "Монтсеррат", 132 => "Мьянма", 133 => "Намибия", 134 => "Науру", 135 => "Непал", 136 => "Нигер", 137 => "Нигерия", 139 => "Нидерланды", 140 => "Никарагуа", 141 => "Ниуэ", 142 => "Новая Зеландия", 143 => "Новая Каледония", 144 => "Норвегия", 145 => "Объединенные Арабские Эмираты", 146 => "Оман", 147 => "Остров Мэн", 148 => "Остров Норфолк", 149 => "Острова Кайман", 150 => "Острова Кука", 151 => "Острова Теркс и Кайкос", 152 => "Пакистан", 153 => "Палау", 154 => "Палестинская автономия", 155 => "Панама", 156 => "Папуа — Новая Гвинея", 157 => "Парагвай", 158 => "Перу", 159 => "Питкерн", 160 => "Польша", 161 => "Португалия", 162 => "Пуэрто-Рико", 163 => "Реюньон", 164 => "Руанда", 165 => "Румыния", 9 => "США", 166 => "Сальвадор", 167 => "Самоа", 168 => "Сан-Марино", 169 => "Сан-Томе и Принсипи", 170 => "Саудовская Аравия", 171 => "Свазиленд", 172 => "Святая Елена", 173 => "Северная Корея", 117 => "Северная Македония", 174 => "Северные Марианские острова", 175 => "Сейшелы", 176 => "Сенегал", 177 => "Сент-Винсент и Гренадины", 178 => "Сент-Китс и Невис", 179 => "Сент-Люсия", 180 => "Сент-Пьер и Микелон", 181 => "Сербия", 182 => "Сингапур", 234 => "Синт-Мартен", 183 => "Сирия", 184 => "Словакия", 185 => "Словения", 186 => "Соломоновы Острова", 187 => "Сомали", 188 => "Судан", 189 => "Суринам", 190 => "Сьерра-Леоне", 16 => "Таджикистан", 191 => "Таиланд", 192 => "Тайвань", 193 => "Танзания", 194 => "Того", 195 => "Токелау", 196 => "Тонга", 197 => "Тринидад и Тобаго", 198 => "Тувалу", 199 => "Тунис", 17 => "Туркменистан", 200 => "Турция", 201 => "Уганда", 18 => "Узбекистан", 202 => "Уоллис и Футуна", 203 => "Уругвай", 204 => "Фарерские острова", 205 => "Фиджи", 206 => "Филиппины", 207 => "Финляндия", 208 => "Фолклендские острова", 209 => "Франция", 210 => "Французская Гвиана", 211 => "Французская Полинезия", 212 => "Хорватия", 213 => "Центральноафриканская Республика", 214 => "Чад", 230 => "Черногория", 215 => "Чехия", 216 => "Чили", 217 => "Швейцария", 218 => "Швеция", 219 => "Шпицберген и Ян Майен", 220 => "Шри-Ланка", 221 => "Эквадор", 222 => "Экваториальная Гвинея", 223 => "Эритрея", 14 => "Эстония", 224 => "Эфиопия", 226 => "Южная Корея", 227 => "Южно-Африканская Республика", 232 => "Южный Судан", 228 => "Ямайка", 229 => "Япония"];

		return view('front.pin_create', compact('countries'));
	}
	public function pinstore(Request $request) {
		$p = Session::get('programm', 1);
		$pinRequest = new \App\Models\PinRequests\StartRequest;

		$pinRequest->user_id = Auth::user()->user_id;
		$pinRequest->uname = Auth::user()->name;
		$pinRequest->bill = $request->bill;
		if ($request->iin) {
			$pinRequest->iin = $request->iin;
		} else {
			$pinRequest->iin = 0;
		}
		$pinRequest->fullname = Auth::user()->familiya;
		$pinRequest->fullname_b = $request->fullname_b;
		$pinRequest->user_note = $request->user_note;
		$pinRequest->date = $request->date;
		$pinRequest->country = $request->country;
		$pinRequest->summ = $request->summ;
		$pinRequest->skype = $request->skype;
		$pinRequest->city = $request->city;
		$pinRequest->currency = $request->currency;

		$img = '';

		if ($request->hasFile('image')) {
			$imgfname = [];
			foreach ($request->image as $key => $image) {
				$imageName = time() . $key . '.' . $image->getClientOriginalExtension();
				$image->move(public_path('uploads/bills/'), $imageName);
				$imgfname[] = $imageName;
			}

			$img = implode(';', $imgfname);
		}

		$pinRequest->program = $p;
		$pinRequest->image = $img;
		$pinRequest->save();

		Session::flash('message', 'Заявка созданна');
		Session::flash('alert-class', 'alert-sucscess');
		return redirect(route('partner.pin.requests'));

	}

	public function getCities(Request $request) {
		$data = [];

//         $c = $request->id;

//         $opts = array(
		//           'http'=>array(
		//             'method'=>"GET",
		//           )
		//         );
		//         $context = stream_context_create($opts);

//         header("Content-Type: text/html; charset=utf-8");
		//         $c = file_get_contents("https://vk.com/select_ajax.php?act=a_get_cities&country=".$c.'&need_all=0', false, $context);
		//         m_returnstatus(conn, identifier) mb_convert_encoding($c, "utf-8", "windows-1251");

//         $countries = curl_init();

//         curl_setopt($countries, CURLOPT_URL, 'https://vk.com/select_ajax.php?act=a_get_cities&country='.$c.'&need_all=1&count=1');
		//         curl_setopt($countries, CURLOPT_POST, true);
		//         curl_setopt($countries, CURLOPT_POSTFIELDS, 'need_all=1&count=1000');
		//         curl_setopt($countries, CURLOPT_RETURNTRANSFER, true);

//         $response1  = curl_exec($countries);

// $html_utf8 = mb_convert_encoding($response1, "utf-8", "windows-1251");

//         $headers = ['Access-Control-Allow-Origin' => '*', 'Content-Type'=>'application/json; charset=utf-8'];
		//         return response($html_utf8, 200, $headers, JSON_UNESCAPED_UNICODE );
	}

	public function bonus() {
		$bonus = BonusRequest::where('uname', Auth::user()->name)->first();
		return view('front.bonus', compact('bonus'));
	}

	public function bonusstore(Request $request) {
		$p = Session::get('programm', 1);

		$br = new BonusRequest;
		$br->fill($request->all());
		$br->uname = Auth::user()->name;
		$br->name = Auth::user()->familiya;
		$br->registration_date = Auth::user()->reg_date;
		$br->program = $p;
		$br->save();
		return redirect('partner/bonus');
	}

	public function marketing() {
		$m = Config::get('matrix.get');
		$p = Session::get('programm', 1);
		return view('front.marketing.' . $m[$p][0]);
	}

	public function addTransfer(Request $request){
		$transfer = new Transfer;


		$transfer->fill($request->all());

		if ($request->hasFile('image')) {
			$imgfname = [];

			foreach ($request->image as $key => $img) {
				$imageName = time() . $key . '.' . $img->getClientOriginalExtension();
				$img->move(public_path('uploads/transfers/'), $imageName);
				$imgfname[] = $imageName;
			}
			$transfer->image = implode(';', $imgfname);
		}
		$transfer->save();

		$finished = Finished::find($transfer->finished_id);
		$finished->transfer = $transfer->id;

		if($transfer->variant){
			$finished->variant = $transfer->variant;
		}
		$finished->save();
		return redirect()->back();
	}

	public function profile() {
		$m = Config::get('matrix.get');
		$p = Session::get('programm', 1);

		$type = $m[$p][0];
		$connection = 'mysql_prod_' . $type;
		$user = Partner::on($connection)->where('user_id', Auth::user()->user_id)->first();

		return view('front.profile', ['user' => $user]);
	}

	public function search(Request $request) {
		$m = Config::get('matrix.get');
		$p = Session::get('programm', 1);

		$type = $m[$p][0];
		$connection = 'mysql_prod_' . $type;

		$mx_user = MatrixUser::on($connection)->where('uname', $request->name)->first();


		if (!$mx_user) {
			return 'Пользователь не найден';
		}
		$max = Matrix::on($connection)->where('matrix_id', $mx_user->mx_id)->first();


		$matrix = MatrixUser::on($connection)->whereRaw('mx_id = ?', array($mx_user->mx_id))->orderBy('level', 'asc')->leftJoin('dle_users', 'dle_matrix_users.uid', '=', 'dle_users.user_id')->orderBy('position', 'ASC')->get();
		$res = [];
		$res['stage'] = $max->stage;
		foreach ($matrix as $key => $value) {
							if( $value->ref_num > 10 ){
					$ref_num = 0;
				} else {
					$ref_num = $value->ref_num;
				}
			$res['levels'][$value->level][] = [

				'uname' => $value->uname,
				'stars' => $ref_num,
				'by_refer' => $this->getName($value->by_refer),
			];
		}

		foreach ($res['levels'] as $key => $value) {
			sort($value);
		}

		return $res;
	}

	public function getName($uid) {
		$m = Config::get('matrix.get');
		$p = Session::get('programm', 1);
		$type = $m[$p][0];
		$connection = 'mysql_prod_' . $type;

		$name = Partner::on($connection)->select('name')->where('user_id', $uid)->first();
		if (!$name) {
			return '';
		}
		return 'от ' . $name->name;
	}

}
