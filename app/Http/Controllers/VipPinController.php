<?php

namespace App\Http\Controllers;

use App\Models\Pins\VipPin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class VipPinController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Pins\VipPin  $vipPin
     * @return \Illuminate\Http\Response
     */
    public function show(VipPin $vipPin)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Pins\VipPin  $vipPin
     * @return \Illuminate\Http\Response
     */
    public function edit(VipPin $vipPin)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Pins\VipPin  $vipPin
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, VipPin $vipPin)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Pins\VipPin  $vipPin
     * @return \Illuminate\Http\Response
     */
    public function destroy(VipPin $vipPin)
    {
        //
    }
}
