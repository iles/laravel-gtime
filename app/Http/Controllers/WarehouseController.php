<?php

namespace App\Http\Controllers;

use App\Exports\WarehouseExport;
use App\Grids\WarehouseAdminRequestsGrid;
use App\Grids\WarehouseRequestsGrid;
use App\Models\Partner;
use App\Models\WarehouseRequest;
use App\User;
use Config;
use Illuminate\Http\Request;
use Input;
use Maatwebsite\Excel\Facades\Excel;
use Redirect;
use Session;
use Validator;

class WarehouseController extends Controller {
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */

	public function index(WarehouseRequestsGrid $sprInterface, Request $request) {

		return $sprInterface
			->create(['query' => WarehouseRequest::query(), 'request' => $request])
			->renderOn('warehouse.index'); // render the grid on the welcome view

	}

	public function index_admin(WarehouseAdminRequestsGrid $sprInterface, Request $request) {

		return $sprInterface
			->create(['query' => WarehouseRequest::query(), 'request' => $request])
			->renderOn('warehouse.index_admin'); // render the grid on the welcome view

	}

	public function add(Request $request) {
		$warehouse = WarehouseRequest::create($request->all());
		Session::flash('message', 'Заявка создана');
		Session::flash('alert-class', 'alert-sucscess');

		$m = Config::get('matrix.get');
		$p = Session::get('programm', 1);

		$type = $m[$warehouse->program][0];

		$connection = 'mysql_prod_' . $type;
		$partner = Partner::on($connection)->where('name', $warehouse->uname)->first();
		$partner->tovar = 'Получено: ' . $warehouse->option;
		$partner->save();
		return redirect('/admin/warehouse');
	}

	public function view_admin(Request $request) {
		$model = WarehouseRequest::find($request->id);
		$manager = User::role('warehouse')->where('id', $model->manager_id)->first();
		return view('warehouse.admin_view', compact('model', 'manager'));
	}

	public function aprove($id) {
		$request = WarehouseRequest::find($id);
		$request->status_id = 1;
		$request->save();
		Session::flash('message', 'Заявка одобрена');
		Session::flash('alert-class', 'alert-sucscess');
		return redirect('/admin/warehouse-admin');
	}

	public function export() {
		// $fileContents = 'wawawaw';
		//  Storage::disk('ftp')->put('dev', $fileContents);
		Excel::store(new WarehouseExport, '/dev/dev.xlsx', 'ftp');
		//return Excel::download(new WarehouseExport, 'users.xlsx');
	}

	public function show(Request $request) {
		$m = Config::get('matrix.get');
		$progs = [];
		foreach ($m as $key => $value) {
			$progs[$key] = $value[1];
		}
		$options = [
			'nabor1' => 'Набор 1',
			'nabor2' => 'Набор 2',
			'nabor3' => 'Набор 3',
			'nabor4' => 'Набор 4',
		];
		$model = WarehouseRequest::find($request->id)->first();
		return view('warehouse.form', compact('model', 'progs', 'options'));
	}

	public function update($id) {
		// validate
		// read more on validation at http://laravel.com/docs/validation
		$rules = array(
			'uname' => 'required',
			'option' => 'required',
			'program' => 'required',
		);
		$validator = Validator::make(Input::all(), $rules);

		// process the login
		if ($validator->fails()) {
			Session::flash('message', 'Введите коректные данные');
			Session::flash('alert-class', 'alert-danger');
			return Redirect::to('/admin/warehouse')
				->withInput(Input::except('password'));
		} else {
			// store
			$wr = WarehouseRequest::find($id);
			$wr->fill(Input::all());
			$wr->save();
			Session::flash('message', 'Заявка обновлена');
			Session::flash('alert-class', 'alert-sucscess');
			return Redirect::to('/admin/warehouse');
		}
	}

	public function update_admin(Request $request) {
		$model = WarehouseRequest::find($request->id);
		$model->update($request->all());
		return redirect()->back();
	}

	public function checkLogin(Request $request) {
		$res = [];

		$m = Config::get('matrix.get');
		$p = Session::get('programm', 1);
		$pr = $request->program;
		$progs = [];
		$connection = 'mysql_prod_' . $m[$pr][0];
		$user = Partner::on($connection)->where('name', $request->name)->first();
		if ($user) {
			$res['error'] = 0;
			$res['m'] = $user->familiya;
			$res['data'] = $user;
			return $res;
		} else {
			$res['error'] = 1;
			$res['m'] = 'Пользователь не найден';
			return $res;
		}
	}
}
