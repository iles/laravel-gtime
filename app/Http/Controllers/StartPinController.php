<?php

namespace App\Http\Controllers;

use App\Models\Pins\StartPin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class StartPinController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Pins\StartPin  $startPin
     * @return \Illuminate\Http\Response
     */
    public function show(StartPin $startPin)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Pins\StartPin  $startPin
     * @return \Illuminate\Http\Response
     */
    public function edit(StartPin $startPin)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Pins\StartPin  $startPin
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, StartPin $startPin)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Pins\StartPin  $startPin
     * @return \Illuminate\Http\Response
     */
    public function destroy(StartPin $startPin)
    {
        //
    }
}
