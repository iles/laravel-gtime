<?php

namespace App\Http\Controllers;

use App\Models\FastPinRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FastPinRequestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\FastPinRequest  $fastPinRequest
     * @return \Illuminate\Http\Response
     */
    public function show(FastPinRequest $fastPinRequest)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\FastPinRequest  $fastPinRequest
     * @return \Illuminate\Http\Response
     */
    public function edit(FastPinRequest $fastPinRequest)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\FastPinRequest  $fastPinRequest
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, FastPinRequest $fastPinRequest)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\FastPinRequest  $fastPinRequest
     * @return \Illuminate\Http\Response
     */
    public function destroy(FastPinRequest $fastPinRequest)
    {
        //
    }
}
