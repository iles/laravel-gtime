<?php

namespace App\Http\Controllers;

use App\Models\Pins\Acummulative_plusPin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AcummulativePlusPinController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Pins\Acummulative_plusPin  $acummulative_plusPin
     * @return \Illuminate\Http\Response
     */
    public function show(Acummulative_plusPin $acummulative_plusPin)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Pins\Acummulative_plusPin  $acummulative_plusPin
     * @return \Illuminate\Http\Response
     */
    public function edit(Acummulative_plusPin $acummulative_plusPin)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Pins\Acummulative_plusPin  $acummulative_plusPin
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Acummulative_plusPin $acummulative_plusPin)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Pins\Acummulative_plusPin  $acummulative_plusPin
     * @return \Illuminate\Http\Response
     */
    public function destroy(Acummulative_plusPin $acummulative_plusPin)
    {
        //
    }
}
