<?php

namespace App\Http\Controllers\Programs;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Programs\ProgramController;
use App\Models\Finished;
use App\Models\VipProgramFinish;
use Image;
use Config;
use Carbon\Carbon;
use App\Models\Cancellation;
use App\Models\Split;
use DB;
use Session;
use Auth;

class ProgramVipController extends ProgramController
{
    public function insertIntoMatrix($uid, $uname, $referal_id, $insert_matrix_id = 0)
    {

        $m = Config::get('matrix.get');
        $p = 6;

        $connection = 'mysql_prod_'.$m[$p][0];

        if ($insert_matrix_id) { # Если передаём ID матрицы, не делаем лишних запросов
            $matrix_id = $insert_matrix_id;
        } else {
            $data = DB::connection($connection)->table('dle_users')->select('matrix_id')->where('user_id',
                $referal_id)->first();
            $matrix_id = intval($data->matrix_id);
        }


        $row = DB::connection($connection)->table('dle_matrix')->where('matrix_id', $matrix_id)->first();

        $leader2 = $row->leader;

        $leader_row2 = DB::connection($connection)->table('dle_users')->select('*')->where('name', $leader2)->first();

        $parent_matrix_id2 = DB::connection($connection)->table('dle_users')->select('matrix_id')->where('user_id',
            $leader_row2->by_refer)->first();

        if ($parent_matrix_id2) {
            $pm = $parent_matrix_id2->matrix_id;
        } else {
            $pm = DB::connection($connection)->table('dle_users')->select('matrix_id')->where('user_id',
                $leader_row2->by_refer)->first()->matrix_id;
        }

        $leader_row3 = DB::connection($connection)->table('dle_matrix_users')->select(DB::raw('count(mx_id) as ncnt'))->where('mx_id',
            $pm)->first();

        DB::connection($connection)->table('dle_matrix')
            ->where('matrix_id', $pm)
            ->update([
                'users_num' => $leader_row3->ncnt,
            ]);


        $new_users_num = $row->users_num + 1;

        // Вычисляем позицию

        if ($new_users_num == 7) {
            $spr = $leader_row2->by_refer;
            $sparrow = DB::connection($connection)->table('dle_users')->select('by_refer', 'user_id', 'name',
                'matrix_id', 'ref_num')->where('user_id', $spr)->first();

            /*
            if(!$sparrow){
                return false;
            }
            */

        }

        $pos = DB::connection($connection)->table('dle_matrix_users')->select(DB::raw('count(uid) as c'))->where('mx_id',
            $matrix_id)->first();
        $pos = $pos->c + 1;


        // Добавляем юзера в матрицу
        DB::connection($connection)->table('dle_matrix_users')->insert(
            [
                'uid' => $uid,
                'uname' => $uname,
                'mx_id' => $matrix_id,
                'level' => 3,
                'position' => $pos,
            ]
        );

        // Обновляем кол-во юзеров в матрице
        DB::connection($connection)->table('dle_matrix')->where('matrix_id', $matrix_id)->increment('users_num');
        DB::connection($connection)->table('dle_users')->where('user_id', $referal_id)->increment('ref_num');

        // Обновляем id матрицы у пользователя
        $this->update_user(array('matrix_id' => $matrix_id), $uid);

        $new_user = DB::connection($connection)->table('dle_users')->select('*')->where('user_id', $uid)->first();
        $sponsor_user = DB::connection($connection)->table('dle_users')->where('user_id', $referal_id)->first();

        $cancellation = Cancellation::create([
            'program_id' => 6,
            'pin' => $new_user->screch,
            'login' => $new_user->name,
            'fullname' => $new_user->fullname,
            'uid' => $new_user->user_id,
            'mx_id' => $new_user->matrix_id,
            'sponsor_id' => $sponsor_user->user_id,
            'sponsor_login' => $sponsor_user->name,
            'sponsor_fullname' => $sponsor_user->fullname,
            'reg_date' => Carbon::today()->toDateTimeString(),
        ]);

        // Если необходимо деление матриц
        if ($new_users_num > 6) {

            $last_users = DB::connection($connection)->table('dle_matrix_users')->where('mx_id',
                $matrix_id)->where('level', 3)->get()->toArray();

            $main_matrix_users = DB::connection($connection)->table('dle_matrix_users')->where('mx_id',
                $matrix_id)->get()->toArray();

            $spm = DB::connection($connection)->table('dle_matrix_users')
                ->whereRaw('mx_id = ?', array($matrix_id))
                ->orderBy('level', 'asc')
                ->leftJoin('dle_users', 'dle_matrix_users.uid', '=', 'dle_users.user_id')
                ->orderBy('position', 'ASC')
                ->get();

            $split_main_matrix = [];

            foreach ($spm as $key => $value) {
                $split_main_matrix[$value->level][] = [
                    'uid' => $value->user_id,
                    'uname' => $value->uname,
                    'stars' => $value->ref_num,
                    'by_refer' => $this->getName($value->by_refer)
                ];
            }

            foreach ($split_main_matrix as $key => $value) {
                sort($value);
            }

            $leader = $row->leader; # Лидер матрицы

            $leader_row = DB::connection($connection)->table('dle_users')->select('*')->where('name', $leader)->first();

            $split = new Split();
            $split->program_id = 6;
            $split->main_matrix_id = $matrix_id;
            $split->main_matrix = json_encode($split_main_matrix, true);
            $split->leader = json_encode($leader_row, true);
            $split->leader_id = $leader_row->user_id;
            $split->new = json_encode($new_user, true);
            $split->new_id = $new_user->user_id;
            $split->sponsor = json_encode($sponsor_user, true);
            $split->sponsor_id = $sponsor_user->user_id;
            $split->matrix_stage = $row->stage;
            $split->matrix_dateout = $row->datetout;


            // Получаем id матрицы, в которой находится тот, кто пригласил лидера

            $parent_matrix_id = DB::connection($connection)->table('dle_users')->select('matrix_id')->where('user_id',
                $leader_row->by_refer)->first();


            // Удаляем из старой матрицы

            DB::connection($connection)->table('dle_matrix_users')->where('uid', $leader_row->user_id)->delete();

            $new_stage = $row->stage + 1; # новый уровень
            $this->addNews($leader_row->name, 1, 6);

            if ($new_stage > 0) {
                $new_stage = 1;
            } # Вечно 1 stage///изменили на 1

            if ($new_stage == 0) // Если пользователь не завершает лидерскую

            {

                $this->NewMatrixByRefer($leader_row->by_refer,
                    $new_stage); # Ищем матрицу реферала, соответвующую той, что нужна нам!

                $new_matrix = current($this->MatrixList); # Получаем ID матрицы

                # Временно обнуляем кол-во рефералов у пользователя, который уходит в новую матрицу             
                $this->update_user(array('ref_num' => '0'),
                    $leader_row->user_id); ///////////////////////////////////////////////////////////////////////////

            } else {
                $GoldTemp = $leader_row;
            }


            $matrix_users = DB::connection($connection)->select('SELECT
                            dle_matrix.leader, 
                            dle_matrix.users_num,
                            dle_matrix.stage,
                            dle_matrix_users.uid,
                            dle_users.ref_num, by_refer, user_id, name

                    FROM `dle_matrix`, 
                         `dle_matrix_users`,
                         `dle_users`
                         
                    WHERE dle_matrix.matrix_id= ? AND dle_users.user_id = dle_matrix_users.uid AND  dle_users.matrix_id= ?
                    AND  dle_matrix.leader != dle_matrix_users.uname
                    ORDER BY dle_users.ref_num DESC, 
                    dle_matrix_users.level ASC, 
                    dle_matrix_users.position ASC', [$matrix_id, $matrix_id]);

            $position_id = 0;

            $_2LevelUsers = array();
            $_3LevelUsers = array();

            foreach ($matrix_users as $data) {
                $position_id++;

                if (in_array($position_id, array(1, 2))) { # Если лидеры новых матриц

                    // создаём новую матрицу////////////////////////////////////////////////

                    $new_matrix_id = $this->CREATEMatrix($data->name, 3, $row->stage);


                    if ($row->stage == 1) {

                        DB::connection($connection)->table('dle_users')
                            ->where('name', $data->name)
                            ->update([
                                'by_refer' => 0,
                            ]);

                    }


                    // добавляем лидера в матрицу (его же)
                    DB::connection($connection)->table('dle_matrix_users')
                        ->where('uid', $data->user_id)
                        ->update([
                            'mx_id' => $new_matrix_id,
                            'level' => 1,
                            'position' => 1,
                        ]);


                    // Обновляем id матрицы у пользователя
                    $this->update_user(array('matrix_id' => $new_matrix_id), $data->user_id);

                    $matrix_id_array[] = $new_matrix_id; // записываем ID новых матриц в массив

                } else { # Остальные 12 пользователя, что станут на второй и третий уровень.                    
                    $_2LevelUsers[] = array($data->user_id, $data->name);
                }
            }


            // Кидаем юзеров на 2й & 3й ряд, в новые матрицы
            $j = 0; // Общий счетчик
            $k = 0; // Вычислитель позиций
            $l = 0; // позиции самого нижнего ряда
            $l2 = 0; // самого нижнего ряда 2


            foreach ($_2LevelUsers as $user_info_array) {
                $j++;

                list($current_uid, $current_uname) = $user_info_array;

                if (!intval($current_uid)) {
                    $this->createLog('NotCID', "NOT CURRENT id ($j step)");
                    break;
                }

                if (!trim($current_uname)) {
                    $this->createLog('NotCName', "NOT CURRENT name ($j step)");
                    break;
                }


                if (in_array($j, array(1, 2))) {
                    if ($matrix_id_array[0]) { # Если сущ. id матрицы


                        DB::connection($connection)->table('dle_matrix_users')
                            ->where('uid', $current_uid)
                            ->update([
                                'mx_id' => $matrix_id_array[0],
                                'level' => 2,
                                'position' => $j,
                            ]);

                        //$this->createLog('UPDATE_MATRIX_USER', "UPDATE dle_matrix_users SET mx_id='" . $matrix_id_array[0] . "', level='2', position='" . $j . "' WHERE uid='$current_uid'");
                        $this->update_user(array('matrix_id' => $matrix_id_array[0]), $current_uid);
                    } else {
                        $this->createLog("NotCMXID1", "NOT CURRENT mxid[0] ($j step)");
                        break;
                    }
                } elseif (in_array($j, array(3, 4))) {
                    $k++;
                    if ($matrix_id_array[1]) { # Если сущ. id матрицы

                        DB::connection($connection)->table('dle_matrix_users')
                            ->where('uid', $current_uid)
                            ->update([
                                'mx_id' => $matrix_id_array[1],
                                'level' => 2,
                                'position' => $k,
                            ]);
                        //$this->createLog('UPDATE_MATRIX_USER', "UPDATE dle_matrix_users SET mx_id='" . $matrix_id_array[1] . "', level='2', position='" . $k . "' WHERE uid='$current_uid'");
                        $this->update_user(array('matrix_id' => $matrix_id_array[1]), $current_uid);
                    } else {
                        $this->createLog("NotCMXID2", "NOT CURRENT mxid[1] ($j step)");
                        break;
                    }
                } elseif ($j >= 5 && $j <= 8) {
                    $l++;

                    if ($matrix_id_array[0]) { # Если сущ. id матрицы
                        DB::connection($connection)->table('dle_matrix_users')
                            ->where('uid', $current_uid)
                            ->update([
                                'mx_id' => $matrix_id_array[0],
                                'level' => 3,
                                'position' => $l,
                            ]);
                        //$this->createLog('UPDATE_MATRIX_USER', "UPDATE dle_matrix_users SET mx_id='" . $matrix_id_array[0] . "', level='4', position='" . $l . "' WHERE uid='$current_uid'");
                        $this->update_user(array('matrix_id' => $matrix_id_array[0]), $current_uid);
                    } else {
                        $this->createLog("NotCMXID12", "NOT CURRENT mxid[0] ($l step)");
                        break;
                    }
                }
            }


            // $vip_program_finish = VipProgramFinish::create([
            //         'user_id' => $leader_row->user_id,
            // ]);

            if ($new_stage == 1) {
                $finished = Finished::create([
                    'program_id' => 6,
                    'user_id' => $leader_row->user_id,
                    'login' => $leader_row->name,
                    'fullname' => $leader_row->familiya,
                    'birthdate' => $leader_row->otchestvo,
                    'iin' => $leader_row->strana,
                    'registred_pin' => $leader_row->screch,
                    'country' => $leader_row->imya,
                    'info' => $leader_row->signature,
                    'tovar' => $leader_row->tovar,
                    'phone' => $leader_row->pol,
                    'stage' => 1,
                    'last' => json_encode($last_users, JSON_UNESCAPED_UNICODE),
                ]);

            } else {

                $finished = Finished::create([
                    'program_id' => 6,
                    'user_id' => $leader_row->user_id,
                    'login' => $leader_row->name,
                    'fullname' => $leader_row->familiya,
                    'birthdate' => $leader_row->otchestvo,
                    'iin' => $leader_row->strana,
                    'registred_pin' => $leader_row->screch,
                    'country' => $leader_row->imya,
                    'info' => $leader_row->signature,
                    'tovar' => $leader_row->tovar,
                    'phone' => $leader_row->pol,
                    'last' => json_encode($last_users, JSON_UNESCAPED_UNICODE),
                ]);
            }

            DB::connection($connection)->table('dle_matrix')->where('matrix_id', $matrix_id)->delete();


            // Если есть темп ЗОЛОТА, вставляем его в матрицу
            if (isset($GoldTemp)) {
                $leader_row = &$GoldTemp;

                if ($leader_row->by_refer == 0) {

                    $sReferal = DB::connection($connection)->table('dle_users')->select('user_id')->where('by_refer',
                        $leader_row->user_id)->orderBy('user_id', 'asc')->first();

                    $leader_row->by_refer = $sReferal->user_id;
                }

                // ИНАЧЕ::выполняем стандартные процедуры...
                $this->NewMatrixByRefer($leader_row->by_refer,
                    $new_stage); # Ищем матрицу реферала, соответвующую той, что нужна нам!

                $new_matrix = current($this->MatrixList); # Получаем ID матрицы

                //$this->createLog('NewMatrix', 'New Matrix ID:' . $new_matrix . '|| USER FOR NEW MATRIX: ' . $leader_row['name']);

                # Временно обнуляем кол-во рефералов у пользователя, который уходит в новую матрицу             
                $this->update_user(array('ref_num' => '0'), $leader_row->user_id);

                # Добавляем реферала, тому чей реферал уходит в новую матрицу 
                # (если его реферал находится в соотвественной матрице )

                // $where_referal = $this->ScanMatrixRang($leader_row['by_refer']); # &ссылаемся на ф-ю сканирования

                // if( $where_referal['stage'] == $new_stage )
                // {
                //     $this->update_user( array( 'ref_num' => 'ref_num + 1'), $leader_row['by_refer'] );
                // }
                // elseif( $where_referal['stage'] ==  $row['stage'] )  #Если его реферал, в матрице ниже ранга, отнимаем у него 1го реферала
                // {
                // //  $this->update_user( array( 'ref_num' => 'ref_num - 1'), $leader_row['by_refer'] );
                // }

                // Перемещаем юзера в ту матрицу
                // $this->insertIntoMatrix($leader_row['user_id'], $leader_row['name'], $leader_row['by_refer'], $new_matrix );

                # -----------------------------------------------------------------------------------------------

                # ПООЩЕРЕНИЕ
                // Выдаём поощерение
                $this->insertIntoMatrix($leader_row->user_id, $leader_row->name, $leader_row->by_refer, $new_matrix);

                if ($row->stage == 1 && $leader) {

                    // Set l ----------------------

                    $leader = $row->leader;


                    $leader_row = DB::connection($connection)->table('dle_users')->select('*')->where('name',
                        $leader)->first();

                    $datee = date("d.m.Y H:i:s");


                    $perekid = DB::connection($connection)->table('dle_users')->select()->where('name',
                        $leader)->first();


                    DB::connection($connection)->table('dle_drop_users9000')->insert(
                        [
                            'screch' => $perekid->screch,
                            'email' => $perekid->email,
                            'name' => $perekid->name,
                            'dateout' => $datee,
                            'balance' => 25000,
                            'familiya' => $perekid->familiya,
                            'imya' => $perekid->imya,
                            'otchestvo' => $perekid->otchestvo,
                            'pol' => $perekid->pol,
                            'strana' => $perekid->strana,
                            'gorod' => $perekid->gorod,
                            'INFO' => $perekid->info,
                            'TOVAR' => $perekid->tovar,
                        ]
                    );

                    DB::connection($connection)->table('dle_matrix_users')->where('uid',
                        $leader_row->user_id)->delete();
                    //DB::connection($connection)->table('dle_users')->select()->where('name', $leader)->delete();

                } elseif ($row->stage == 0 && $leader) {

                    DB::connection($connection)->table('dle_users')->where('user_id',
                        $leader_row->user_id)->increment('balance', 90000);


                    $leader = $row->leader;

                    $leader_row = DB::connection($connection)->table('dle_users')->select('*')->where('name',
                        $leader)->first();
                    $datee = date("d.m.Y H:i:s");


                    $perekid = DB::connection($connection)->table('dle_users')->select()->where('name',
                        $leader)->first();


                    DB::connection($connection)->table('dle_drop_users9000')->insert(
                        [
                            'screch' => $perekid->screch,
                            'email' => $perekid->email,
                            'name' => $perekid->name,
                            'dateout' => $datee,
                            'balance' => 25000,
                            'familiya' => $perekid->familiya,
                            'imya' => $perekid->imya,
                            'otchestvo' => $perekid->otchestvo,
                            'pol' => $perekid->pol,
                            'strana' => $perekid->strana,
                            'gorod' => $perekid->gorod,
                            'INFO' => $perekid->info,
                            'TOVAR' => $perekid->tovar,
                        ]
                    );


                    $fm = DB::connection($connection)->table('dle_matrix_users')
                        ->whereRaw('mx_id = ?', array($matrix_id_array[0]))
                        ->orderBy('level', 'asc')
                        ->leftJoin('dle_users', 'dle_matrix_users.uid', '=', 'dle_users.user_id')
                        ->orderBy('position', 'ASC')
                        ->get();
                    $split_first_matrix = [];

                    foreach ($fm as $key => $value) {
                        $split_first_matrix[$value->level][] = [
                            'uid' => $value->user_id,
                            'uname' => $value->uname,
                            'stars' => $value->ref_num,
                            'by_refer' => $this->getName($value->by_refer)
                        ];
                    }

                    foreach ($split_first_matrix as $key => $value) {
                        sort($value);
                    }

                    $sm = DB::connection($connection)->table('dle_matrix_users')
                        ->whereRaw('mx_id = ?', array($matrix_id_array[1]))
                        ->orderBy('level', 'asc')
                        ->leftJoin('dle_users', 'dle_matrix_users.uid', '=', 'dle_users.user_id')
                        ->orderBy('position', 'ASC')
                        ->get();
                    $split_second_matrix = [];

                    foreach ($sm as $key => $value) {
                        $split_second_matrix[$value->level][] = [
                            'uid' => $value->user_id,
                            'uname' => $value->uname,
                            'stars' => $value->ref_num,
                            'by_refer' => $this->getName($value->by_refer)
                        ];
                    }

                    foreach ($split_second_matrix as $key => $value) {
                        sort($value);
                    }


                    $split->first_matrix = json_encode($split_first_matrix, true);
                    $split->first_matrix_id = $matrix_id_array[0];
                    $split->second_matrix = json_encode($split_second_matrix, true);
                    $split->second_matrix_id = $matrix_id_array[1];
                    $split->save();

                }


            }


        }

        return true;
    }

    function ScanMatrixRang($user_id)
    {
        $connection = 'mysql_prod_main';
        $row = DB::connection($connection)->select("SELECT dle_matrix_users.mx_id, dle_matrix.stage FROM dle_matrix_users, dle_matrix WHERE dle_matrix_users.uid = ? AND dle_matrix.matrix_id = dle_matrix_users.mx_id",
            [$user_id]);
        return $row[0];
    }


    public function finish()
    {

        $m = Config::get('matrix.get');
        $p = Session::get('programm', 1);
        $progs = [];
        foreach ($m as $key => $value) {
            $progs[$key] = $value[1];
        }

        $finished = Finished::where('user_id', Auth::user()->user_id)->first();


        if ($finished->bonus) {

            $bonus = \App\Models\Bonuses\StartBonusRequestModel::where('id', $finished->bonus)->first();

            if ($bonus->status_id == 4) {
                echo view('front.finissh', ['progs' => $progs]);
            } else {
                if ($bonus->secondBonus || $bonus->secondPin) {
                    echo view('front.finish.finish_second_acummulative', ['bonus' => $bonus]);
                    return true;
                }
                echo view('front.finish.finish_second_acummulative', ['bonus' => $bonus]);
                return true;
            }
        }
        if ($finished->pin) {
            $p = json_decode($finished->pin);
            $pin = \App\Models\Pins\MainPin::where('id', $p->pin)->first();
            echo view('front.finish_pin', ['pin' => $pin]);
        }

        echo view('front.finish.vip', ['progs' => $progs]);
    }


}


