<?php

namespace App\Http\Controllers\Programs;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Image; 
use Config;
use Carbon\Carbon;
use DB;
use Session;
use Auth;
use App\Models\News;
use App\Models\User;

class ProgramController extends Controller
{

    public $stages  = array( '1', '2' );

    function NewMatrixByRefer( $referid, $need_level = 0 ){

        $m = Config::get('matrix.get');
        $p =   $p = Session::get('programm', 1);

        $connection = 'mysql_prod_'.$m[$p][0];

        $this->MatrixList = array();

        $s_result = DB::connection($connection)
            ->table('dle_users')
            ->leftJoin('dle_matrix', 'dle_users.matrix_id', '=', 'dle_matrix.matrix_id')
            ->select('dle_users.user_id', 'dle_users.by_refer', 'dle_users.matrix_id', 'dle_users.name','dle_matrix.stage')
            ->where('dle_users.user_id', $referid)->first();

        if($s_result && $s_result->matrix_id){
            $mid = $s_result->matrix_id;
        } else {
             $s_result = DB::connection($connection)
            ->table('dle_users')
            ->leftJoin('dle_matrix', 'dle_users.matrix_id', '=', 'dle_matrix.matrix_id')
            ->select('dle_users.user_id', 'dle_users.by_refer', 'dle_users.matrix_id', 'dle_users.name','dle_matrix.stage')
            ->where('dle_users.user_id', $referid)->first(); 
             $mid = $s_result->matrix_id;        
        }

        if( $mid ) {
                $curr_level = $s_result->stage;

                if( $curr_level > $need_level ) 
                {
            //      $this->SubTreeSearch( $referid, $need_level );
                }
                elseif( $curr_level < $need_level )
                {
                    $this->NewMatrixByRefer( $s_result->by_refer, $need_level);
                }
                else 
                {
                    $this->MatrixList[] = $mid;
                }
        }
    }


    public function starsComparator($user1, $user2) { 
        if( $user1['stars'] < $user2['stars'] ){
            return 1;
        } elseif($user1['stars'] > $user2['stars']) {            
            return 0;
        } elseif( $user1['level'] < $user2['level'] ){
            return 0;
        } elseif ($user1['level'] > $user2['level']){
            return 1;
        } elseif( $user1['position'] > $user2['position'] ){
            return 1;
        } elseif($user1['position'] < $user2['position']){
            return 0;
        }        
    }    

    public function positionComparator($user1, $user2) { 
        if( $user1['level'] < $user2['level'] ){
            return 0;
        } elseif ($user1['level'] > $user2['level']){
            return 1;
        } elseif( $user1['position'] > $user2['position'] ){
            return 1;
        } elseif($user1['position'] < $user2['position']){
            return 0;
        }        
    }    

    public function getStars($uid){
        $partner = User::where('user_id', $uid)->first();
        return $partner->ref_num;
    }

    public function getName($uid){
        $m = Config::get('matrix.get');
        $p = Session::get('programm', 1);
        $type = $m[$p][0];
        $connection = 'mysql_prod_'.$type;

        $name = User::on($connection)->select('name')->where('user_id', $uid)->first();
        if(!$name){
            return '';
        }
        return 'от '.$name->name;
    }


    public function CREATEMatrix( $leader, $unum, $stage ) {
        $m = Config::get('matrix.get');
        $p =   $p = Session::get('programm', 1);
        $connection = 'mysql_prod_'.$m[$p][0];
        
        $id = DB::connection($connection)->table('dle_matrix')->insertGetId(
            [
                'leader' => $leader,
                'users_num' => $unum,
                'stage' => $stage,
                'datetout' => date("d.m.Y H:i:s")
            ], 'matrix_id'
        );
        return $id;
    }


    
    public function update_user( $set_keys, $user_id ) {
        
        $m = Config::get('matrix.get');
        $p =   $p = Session::get('programm', 1);
        $connection = 'mysql_prod_'.$m[$p][0];
        
        if(is_array($set_keys) && count($set_keys)) {

            DB::connection($connection)->table('dle_users')
            ->where('user_id', $user_id)
            ->update($set_keys);

        } else {
            return "Keys Error";
        }
    }

    public function addNews($name, $type, $program){
        $out = News::create(['type' => $type, 'name' => $name, 'program_id' => $program]);
    }
}
