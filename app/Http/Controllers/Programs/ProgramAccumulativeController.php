<?php

namespace App\Http\Controllers\Programs;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Programs\ProgramController;
use App\Models\Finished;
use Image; 
use Config;
use Carbon\Carbon;
use App\Models\Cancellation;
use App\Models\Split;
use DB;
use Session;
use Auth;

class ProgramAcummulativeController extends ProgramController
{
    public function insertIntoMatrix( $uid, $uname, $referal_id ) 
    {
        
        $m = Config::get('matrix.get');
        $p = 4;

        $connection = 'mysql_prod_'.$m[$p][0];

        $matrix_user = DB::connection($connection)->table('dle_matrix_users')->select('*')->where('uid', $referal_id )->first();
        $matrix_id = $matrix_user->mx_id;


        $row = DB::connection($connection)->table('dle_matrix')->select('*')->where('matrix_id', $matrix_id)->first();

        //$row = $db->super_query( "SELECT `leader`, `users_num`, `stage` FROM " . PREFIX . "_matrix WHERE matrix_id='" . $matrix_id . "'");
        // новые кол-во юзеров в матрице!
        
        $new_users_num = $row->users_num + 1;
        
        // Вычисляем позицию


        $pos = DB::connection($connection)->table('dle_matrix_users')->select(DB::raw('count(*) as c'))->where('mx_id', $matrix_id)->first();

        // $pos = $db->super_query("SELECT COUNT(*) as c FROM `" . PREFIX . "_matrix_users` WHERE mx_id = '$matrix_id' AND level = $new_lv");
        $pos = $pos->c + 1;
        // Добавляем юзера в матрицу

        DB::connection($connection)->table('dle_matrix_users')->insert(
            [
            'uid' => $uid,
                    'uname' => $uname,
                    'mx_id' => $matrix_id,
                    'level' => 3,
                    'position' => $pos,
                ]
            );
          
            // Обновляем кол-во юзеров в матрице

            DB::connection($connection)->table('dle_matrix')->where('matrix_id', $matrix_id)
            ->increment('users_num');

            
            // Обновляем id матрицы у пользователя
            $this->update_user( array( 'matrix_id' => $matrix_id),  $uid);

             DB::connection($connection)->table('dle_users')->where('user_id', $referal_id)->increment('ref_num');

        $new_user = DB::connection($connection)->table('dle_users')->select('*')->where('user_id', $uid )->first();
        $sponsor_user = DB::connection($connection)->table('dle_users')->where('user_id', $referal_id )->first();

        $cancellation = Cancellation::create([
            'program_id' => 4,
            'pin' => $new_user->screch,
            'login' => $new_user->name,
            'fullname' => $new_user->fullname,
            'uid' => $new_user->user_id,
            'mx_id' => $new_user->matrix_id,
            'sponsor_id' => $sponsor_user->user_id,
            'sponsor_login' => $sponsor_user->name,
            'sponsor_fullname' => $sponsor_user->fullname,
            'reg_date' => Carbon::today()->toDateTimeString(),
        ]);
            
        // Если необходимо деление матриц
        if(($new_users_num > 6 ) )
        { 
            $last_users = DB::connection($connection)->table('dle_matrix_users')->where('mx_id',$matrix_id)->where('level', 3)->get()->toArray();
            $main_matrix_users = DB::connection($connection)->table('dle_matrix_users')->where('mx_id',$matrix_id)->get()->toArray();

            $spm = DB::connection($connection)->table('dle_matrix_users')
            ->whereRaw('mx_id = ?',array($matrix_id))
            ->orderBy('level', 'asc')
            ->leftJoin('dle_users', 'dle_matrix_users.uid', '=', 'dle_users.user_id')
            ->orderBy('position', 'ASC')
            ->get();

            $split_main_matrix = [];

            foreach ($spm as $key => $value) {
                $split_main_matrix[$value->level][] = [
                    'uid' => $value->user_id, 
                    'uname' => $value->uname, 
                    'stars' => $value->ref_num,
                    'by_refer' => $this->getName($value->by_refer)
                ];
            }

            foreach ($split_main_matrix as $key => $value) {
                sort($value);
            }

            $leader     = $row->leader; # Лидер матрицы
             
            $leader_row = DB::connection($connection)->table('dle_users')->select('*')->where('name', $leader)->first();

            $split = new Split;
            $split->program_id = 4;
            $split->main_matrix_id = $matrix_id;
            $split->main_matrix = json_encode($split_main_matrix, true);
            $split->leader = json_encode($leader_row, true);
            $split->leader_id = $leader_row->user_id;
            $split->new = json_encode($new_user, true);        
            $split->new_id = $new_user->user_id;        
            $split->sponsor = json_encode($sponsor_user, true);        
            $split->sponsor_id = $sponsor_user->user_id;        
            $split->matrix_stage = $row->stage;        
            $split->matrix_dateout = $row->datetout;  

            DB::connection($connection)->table('dle_matrix_users')->where('uid', $leader_row->user_id )->delete();

            $matrix_users = DB::connection($connection)->select('SELECT
                dle_matrix.leader, 
                dle_matrix.users_num,
                dle_matrix.stage,
                dle_matrix_users.uid,
                dle_users.ref_num, by_refer, user_id, name

                FROM `dle_matrix`, 
                    `dle_matrix_users`,
                    `dle_users`
                         
                    WHERE dle_matrix.matrix_id= ? AND dle_users.user_id = dle_matrix_users.uid AND  dle_users.matrix_id= ?
                    AND  dle_matrix.leader != dle_matrix_users.uname
                    ORDER BY dle_users.ref_num DESC, 
                    dle_matrix_users.level ASC, 
                    dle_matrix_users.position ASC',  [$matrix_id, $matrix_id] );

            $position_id = 0;
            
            $_2LevelUsers = array();

            foreach( $matrix_users as $data ) { 
                $position_id++;
                                            
                if( in_array($position_id, array(1, 2)) ) { # Если лидеры новых матриц
                                                        
                        // создаём новую матрицу//// 
                        $new_matrix_id = $this->CREATEMatrix($data->name, 3, $row->stage);

                        //self::createLog('createMatrix', 'Create Matrix id:' . $new_matrix_id);
                                                                    
                        // добавляем лидера в матрицу (его же)
                        DB::connection($connection)->table('dle_matrix_users')
                        ->where('uid', $data->user_id)
                        ->update([
                            'mx_id' => $new_matrix_id,
                            'level' => 1,
                            'position' => 1
                        ]);
                    
                        // Обновляем id матрицы у пользователя
                        $this->update_user( array( 'matrix_id' => $new_matrix_id),  $data->user_id);
                        
                        $matrix_id_array[] = $new_matrix_id; // записываем ID новых матриц в массив                 
                    
                } else { # Остальные 12 пользователя, что станут на второй и третий уровень.
                    $_2LevelUsers[] = array( $data->user_id, $data->name);
                }
            }

            $j = 0; // Общий счетчик
            $k = 0; // Вычислитель позиций
            $l = 0; // позиции самого нижнего ряда
            $l2 = 0; // самого нижнего ряда 2

            foreach( $_2LevelUsers as $user_info_array)
            { 
                $j++;
                
                list($current_uid, $current_uname) = $user_info_array;
                
                if( in_array( $j, array( 1, 2) )) 
                {
                                        
                    //Если сущ. id матрицы
                    if( $matrix_id_array[0] ) 
                    { 
                        DB::connection($connection)->table('dle_matrix_users')
                        ->where('uid', $current_uid)
                        ->update([
                            'mx_id' => $matrix_id_array[0],
                            'level' => 2,
                            'position' => $j
                        ]);

     
                        $this->update_user( array( 'matrix_id' => $matrix_id_array[0] ),  $current_uid);
                    }
                    else 
                    {
                        //$this->createLog("NotCMXID1", "NOT CURRENT mxid[0] ($j step)");
                        break;
                    }
                } 
                elseif( in_array( $j, Array(3, 4) ))
                {
                    $k++;
                    
                    //Если сущ. id матрицы
                    if( $matrix_id_array[1] ) 
                    {

                        DB::connection($connection)->table('dle_matrix_users')
                        ->where('uid', $current_uid)
                        ->update([
                            'mx_id' => $matrix_id_array[1],
                            'level' => 2,
                            'position' => $k
                        ]);

                        $this->update_user( array( 'matrix_id' => $matrix_id_array[1]  ),  $current_uid);
                    } 
                    else 
                    {
                        $this->createLog("NotCMXID2", "NOT CURRENT mxid[1] ($j step)");
                        break;
                    }
                } 
                elseif( $j >= 5 && $j <= 8 )
                { 
                    $l++;
                    //Если сущ. id матрицы
                    if( $matrix_id_array[0] ) 
                    { 
                        DB::connection($connection)->table('dle_matrix_users')
                        ->where('uid', $current_uid)
                        ->update([
                            'mx_id' => $matrix_id_array[0],
                            'level' => 4,
                            'position' => $l
                        ]);

     
                        $this->update_user( array( 'matrix_id' => $matrix_id_array[0] ),  $current_uid);
                    }
                    else 
                    {
                        //$this->createLog("NotCMXID1", "NOT CURRENT mxid[0] ($j step)");
                        break;
                    }
                }

            }




                // Set l ----------------------
                $leader     = $row->leader;
                    
                $leader_row = DB::connection($connection)->table('dle_users')->select('*')->where('name', $leader)->first();
                
                $datee= date("d.m.Y H:i:s");

                $perekid = DB::connection($connection)->table('dle_users')->select()->where('name', $leader)->first();

                $finished = Finished::create([
                    'program_id' => 4,
                    'user_id' => $leader_row->user_id,
                    'login' => $leader_row->name,
                    'fullname'  => $leader_row->familiya,
                    'birthdate' => $leader_row->otchestvo,
                    'iin' => $leader_row->strana,
                    'registred_pin' => $leader_row->screch,
                    'country' => $leader_row->imya,
                    'info' => $leader_row->signature,
                    'tovar' => $leader_row->tovar,
                    'phone' => $leader_row->pol,
                    'last' => json_encode($last_users, JSON_UNESCAPED_UNICODE),
                ]);
                

                
                $datee= date("d.m.Y H:i:s");

        $dd = [$matrix_users, $matrix_id_array];




        $fm = DB::connection($connection)->table('dle_matrix_users')
        ->whereRaw('mx_id = ?',array($matrix_id_array[0]))
        ->orderBy('level', 'asc')
        ->leftJoin('dle_users', 'dle_matrix_users.uid', '=', 'dle_users.user_id')
        ->orderBy('position', 'ASC')
        ->get();
        $split_first_matrix = [];

        foreach ($fm as $key => $value) {
            $split_first_matrix[$value->level][] = [
                'uid' => $value->user_id, 
                'uname' => $value->uname, 
                'stars' => $value->ref_num,
                'by_refer' => $this->getName($value->by_refer)
            ];
        }

        foreach ($split_first_matrix as $key => $value) {
            sort($value);
        }        

        $sm = DB::connection($connection)->table('dle_matrix_users')
        ->whereRaw('mx_id = ?',array($matrix_id_array[1]))
        ->orderBy('level', 'asc')
        ->leftJoin('dle_users', 'dle_matrix_users.uid', '=', 'dle_users.user_id')
        ->orderBy('position', 'ASC')
        ->get();
        $split_second_matrix = [];

        foreach ($sm as $key => $value) {
            $split_second_matrix[$value->level][] = [
                'uid' => $value->user_id, 
                'uname' => $value->uname, 
                'stars' => $value->ref_num,
                'by_refer' => $this->getName($value->by_refer)
            ];
        }

        foreach ($split_second_matrix as $key => $value) {
            sort($value);
        }


            $split->first_matrix =   json_encode($split_first_matrix ,true);
            $split->first_matrix_id =   $matrix_id_array[0];
            $split->second_matrix =  json_encode($split_second_matrix, true);
            $split->second_matrix_id = $matrix_id_array[1];
            $split->save(); 
                 
				if(!isset($perekid->info))
					$perekid->info = '';
				
				if(!isset($perekid->signature))
					$perekid->signature = '';
				
                DB::connection($connection)->table('dle_drop_users')->insert(
                    [
                        'screch' => $perekid->screch,
                        'email' => $perekid->email,
                        'name' => $perekid->name,
                        'dateout' => $datee,
                        'balance' => 25000,
                        'familiya'  => $perekid->familiya,
                        'imya'  => $perekid->imya,
                        'otchestvo'  => $perekid->otchestvo,
                        'pol'  => $perekid->pol,
                        'strana'  => $perekid->strana,
                        'gorod'  => $perekid->gorod,
                        'INFO'  => $perekid->info,
                        'TOVAR'  => $perekid->signature,
                    ]
                );
        

 
           
    }
        
        return TRUE;
    }

        public function finish(){
            $m = Config::get('matrix.get');
            $p = Session::get('programm', 1);
            $progs = [];
            foreach ($m as $key => $value) {
                $progs[$key] = $value[1];
            }

            $finished = Finished::where('user_id',  Auth::user()->user_id )->where('program_id', 4)->first(); 

          

            if($finished->pin){
                $p = json_decode($finished->pin);
                $pin = \App\Models\Pins\StartPin::whereId($p->pin )->first();
                echo view('front.finish_pin', ['pin' => $pin]);            
            }
            echo view('front.finish.acummulative', ['progs' => $progs]);
    }
    }
