<?php

namespace App\Http\Controllers\Programs;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Programs\ProgramController;
use App\Models\Finished;
use App\Models\MainProgramFinish;
use Image; 
use Config;
use Carbon\Carbon;
use DB;
use Session;
use Auth;

class ProgramMainController extends ProgramController
{
    public $MatrixList = [];

    public function NewMatrixByRefer( $referid, $need_level = 0 ){

            $m = Config::get('matrix.get');
            $p = 3;

            $connection = 'mysql_prod_'.$m[$p][0];
            
            $this->MatrixList = array();
         
            $s_result = DB::connection($connection)
            ->table('dle_users')
            ->leftJoin('dle_matrix', 'dle_users.matrix_id', '=', 'dle_matrix.matrix_id')
            ->select('dle_users.user_id', 'dle_users.by_refer', 'dle_users.matrix_id', 'dle_users.name','dle_matrix.stage')
            ->where('dle_users.user_id', $referid)->first();
             
            if(isset($s_result->matrix_id)) {
                
                $curr_level = $s_result->stage;

                if( $curr_level > $need_level ) 
                {
                    $this->SubTreeSearch( $referid, $need_level );
                }
                elseif( $curr_level < $need_level )
                {
                    $this->NewMatrixByRefer( $s_result->by_refer, $need_level);
                }
                else 
                {
                    $this->MatrixList[] = $s_result->matrix_id;           
                }
        }
    }

    public function SubTreeSearch( $user_id, $need_level )
    {
        $m = Config::get('matrix.get');
        $p = 3;

        $connection = 'mysql_prod_'.$m[$p][0];
        


         $s_result = DB::connection($connection)->table('dle_users')
            ->leftJoin('dle_matrix', 'dle_users.matrix_id', '=', 'dle_matrix.matrix_id')
            ->select('dle_users.user_id', 'dle_users.by_refer', 'dle_users.matrix_id', 'dle_users.name','dle_matrix.stage')
            ->where('dle_users.by_refer', $user_id)->get();
        
        $arr = array();

        foreach ($s_result as $key => $value) {
            //$mlevel = $this->getMatrixLevel($row['matrix_id']);
            $mlevel = $value->stage;
            
            if( $mlevel != $need_level )
            {
                $this->SubTreeSearch( $value->user_id, $need_level);
            }
            else
            {
                $this->MatrixList[] = $value->matrix_id;
                break;
            }
        }
        
    }

    public function insertIntoMatrix($uid, $uname, $referal_id, $insert_matrix_id = 0) {
        
        $m = Config::get('matrix.get');
        $p = 3;

        $connection = 'mysql_prod_'.$m[$p][0];

        if( $insert_matrix_id ) { # Если передаём ID матрицы, не делаем лишних запросов        
            $matrix_id = $insert_matrix_id;        
        } else {        
            $this->NewMatrixByRefer($referal_id, 0); # id матрицы
            $matrix_id = current( $this->MatrixList );
        }


        $row = DB::connection($connection)->table('dle_matrix')->select('leader', 'users_num', 'stage')->where('matrix_id', $matrix_id)->first();


             
        $leader2    = $row->leader; 
            
        $leader_row2 = DB::connection($connection)->table('dle_users')->select('by_refer', 'user_id', 'name', 'matrix_id', 'ref_num')->where('name', $leader2)->first();
        
        $parent_matrix_id2 = DB::connection($connection)->table('dle_users')->select('matrix_id')->where('user_id', $leader_row2->by_refer)->first();        

        if($parent_matrix_id2){
            $pm = $parent_matrix_id2->matrix_id;            
        } else {
            $pm = DB::connection($connection)->table('dle_users')->select('matrix_id')->where('user_id', $leader_row2->by_refer)->first();
        }
        //dd($pm);

        /*Проверь!!!*/
        $leader_row3 = DB::connection($connection)->table('dle_matrix_users')->select(DB::raw('count(mx_id) as ncnt'))->where('mx_id', $pm )->first();
        /*Проверь!!!*/

        DB::connection($connection)->table('dle_matrix')
                        ->where('matrix_id', $pm)
                        ->update([
                            'users_num' => $leader_row3->ncnt,
                        ]);
 

        $new_users_num = $row->users_num + 1;
            
        if(  $row->users_num < 7) 
            $new_lv = 3;
        else
            $new_lv = 4;
         

        // Вычисляем позицию
 
        $pos = DB::connection($connection)->table('dle_matrix_users')->select(DB::raw('count(*) as c'))->where('mx_id', $matrix_id)->whereLevel($new_lv)->first();
        $pos = $pos->c + 1;
		
        // Добавляем юзера в матрицу
        DB::connection($connection)->table('dle_matrix_users')->insert(
            [
                'uid' => $uid,
                'uname' => $uname,
                'mx_id' => $matrix_id,
                'level' => $new_lv,
                'position' => $pos,
            ]
        );

        // Обновляем кол-во юзеров в матрице
        DB::connection($connection)->table('dle_matrix')->where('matrix_id', $matrix_id)->increment('users_num');

        // Обновляем id матрицы у пользователя
        $this->update_user( array( 'matrix_id' => $matrix_id),  $uid);
        $ar = [
            '1' =>$new_users_num,
            '2' =>$row
        ];       

        $last_users = DB::connection($connection)->table('dle_matrix_users')->where('mx_id',$matrix_id)->where('level', $new_lv)->get()->toArray();
        DB::connection($connection)->table('dle_users')->where('user_id', $referal_id)->increment('ref_num');     
        
		
		
		/*ДЕЛЕНИЕ МАТРИЦЫ*/
        if(($row->stage == 0 && $new_users_num > 14 ) OR ($row->stage == 1 && $new_users_num > 6)) { 

            $leader = $row->leader;  
             
            $leader_row = DB::connection($connection)->table('dle_users')->select('by_refer', 'user_id', 'name', 'matrix_id', 'ref_num')->where('name', $leader)->first();

            // Получаем id матрицы, в которой находится тот, кто пригласил лидера
            //$parent_matrix_id = $db->super_query( "SELECT matrix_id FROM " . PREFIX . "_users WHERE user_id='" . $leader_row['by_refer'] . "'");
            
            /*УДАЛЯЕМ ИЗ СТАРОЙ МАТРИЦЫ*/
            DB::connection($connection)->table('dle_matrix_users')->where('uid', $leader_row->user_id )->delete();

            $new_stage = $row->stage + 1; # новый уровень
            $this->addNews($leader_row->name, 1, 3);

            if($new_stage > count($this->stages)-1) { $new_stage = count($this->stages)-1; }
 
            // Если юзер - не уходит из бронзы...
            if( $row->stage == 0 )
            {
                // ИНАЧЕ::выполняем стандартные процедуры...
                $this->NewMatrixByRefer($leader_row->by_refer, $new_stage); # Ищем матрицу реферала, соответвующую той, что нужна нам!

                $new_matrix = current( $this->MatrixList ); # Получаем ID матрицы

                //$this->createLog('NewMatrix', 'New Matrix ID:' . $new_matrix . '|| USER FOR NEW MATRIX: ' . $leader_row['name']);

                # Временно обнуляем кол-во рефералов у пользователя, который уходит в новую матрицу             
                $this->update_user( array( 'ref_num' => '0'), $leader_row->user_id );
                
                # Добавляем реферала, тому чей реферал уходит в новую матрицу 
                # (если его реферал находится в соотвественной матрице )
                     
                $where_referal = $this->ScanMatrixRang($leader_row->by_refer); # &ссылаемся на ф-ю сканирования
                 
                if( $where_referal->stage == $new_stage ){
                    DB::connection($connection)->table('dle_users')->where('user_id', $leader_row->by_refer)->increment('ref_num');
                } elseif( $where_referal->stage ==  $row->stage  ){  #Если его реферал, в матрице ниже ранга, отнимаем у него 1го реферала
                    //$this->update_user( array( 'ref_num' => 'ref_num - 1'), $leader_row['by_refer'] );
                }
                
                // Перемещаем юзера в ту матрицу                
                $this->insertIntoMatrix( $leader_row->user_id, $leader_row->name, $leader_row->by_refer, $new_matrix );
				
                # -----------------------------------------------------------------------------------------------
                if ($row->stage == 0 && $leader) {
                    DB::connection($connection)->table('dle_users')->where('user_id', $leader_row->user_id)->increment('balance', 90000);
                    $leader = $row->leader;
                    $leader_row = DB::connection($connection)->table('dle_users')->select('*')->where('name', $leader)->first();
                    $datee= date("d.m.Y H:i:s");

                    $perekid = DB::connection($connection)->table('dle_users')->select()->where('name', $leader)->first();
                    
                    $finished = Finished::create([
                        'program_id' => 3,
                        'user_id' => $leader_row->user_id,
                        'login' => $leader_row->name,
                        'fullname'  => $leader_row->familiya,
                        'birthdate' => $leader_row->otchestvo,
                        'iin' => $leader_row->strana,
                        'registred_pin' => $leader_row->screch,
                        'country' => $leader_row->imya,
                        'info' => $leader_row->signature,
                        'tovar' => $leader_row->tovar,
                        'phone' => $leader_row->pol,
                        'stage' => 1,
                        'last' => json_encode($last_users, JSON_UNESCAPED_UNICODE),
                    ]);

                    DB::connection($connection)->table('dle_drop_users80')->insert(
                        [
                            'screch' => $perekid->screch,
                            'email' => $perekid->email,
                            'name' => $perekid->name,
                            'dateout' => $datee,
                            'balance' => 25000,
                            'familiya'  => $perekid->familiya,
                            'imya'  => $perekid->imya,
                            'otchestvo'  => $perekid->otchestvo,
                            'pol'  => $perekid->pol,
                            'strana'  => $perekid->strana,
                            'gorod'  => $perekid->gorod,
                            'INFO'  => $perekid->info ? $perekid->info : 0,
                            'TOVAR'  => $perekid->signature ? $perekid->signature : 0,
                        ]
                    );                 
                }                
            } else {
                // Создаем ТЕМП...
                $GoldTemp = $leader_row;
;
            }
             
            $matrix_users = DB::connection($connection)->select('SELECT
                            dle_matrix.leader, 
                            dle_matrix.users_num,
                            dle_matrix.stage,
                            dle_matrix_users.uid,
                            dle_users.ref_num, by_refer, user_id, name

                    FROM `dle_matrix`, 
                         `dle_matrix_users`,
                         `dle_users`
                         
                    WHERE dle_matrix.matrix_id= ? AND dle_users.user_id = dle_matrix_users.uid AND  dle_users.matrix_id= ?
                    AND  dle_matrix.leader != dle_matrix_users.uname
                    ORDER BY dle_users.ref_num DESC, 
                    dle_matrix_users.level ASC, 
                    dle_matrix_users.position ASC',  [$matrix_id, $matrix_id] );

            $position_id = 0;
            
            $_2LevelUsers = array();
            $_3LevelUsers = array();
            
            foreach( $matrix_users as $data ) 
            {
                $position_id++;
                        
                if( $position_id < 3 ) 
                { # Если лидеры новых матриц
                    
                    if( $row->stage == 0 )
                        $ucount = 7;
                    else
                        $ucount = 3;
                    
                    // создаём новую матрицу
                    $new_matrix_id = $this->CREATEMatrix($data->name, $ucount, $row->stage);
                    
                    //$this->createLog('createMatrix', 'Create Matrix id:' . $new_matrix_id);
                                                                    
                    // добавляем лидера в матрицу (его же)


                    DB::connection($connection)->table('dle_matrix_users')
                    ->where('uid', $data->user_id)
                    ->update([
                        'mx_id' => $new_matrix_id,
                        'level' => 1,
                        'position' => 1
                    ]);

                   
                    // Обновляем id матрицы у пользователя
                    $this->update_user( array( 'matrix_id' => $new_matrix_id),  $data->user_id);
                    
                    $matrix_id_array[] = $new_matrix_id; // записываем ID новых матриц в массив                 
                    
                } 
                elseif( $position_id > 2 && $position_id < 7 ) 
                { 
                    // 3й ряд...
                    $_2LevelUsers[] = array( $data->user_id, $data->name);
                }
                elseif(  $position_id > 6 )
                {
                    // 4й ряд, если есть...
                    $_3LevelUsers[] = array( $data->user_id, $data->name);
                }
            }



                // Кидаем юзеров на 2й ряд, в новые матрицы
            $j = 0; // Общий счетчик
            $x = 0;
            $k = 0; // Вычислитель позиций
            
            foreach( $_2LevelUsers as $user_info_array)
            { 
                $j++;
                
                list($current_uid, $current_uname) = $user_info_array;
                
                if( in_array( $j, array( 1, 3) )) 
                {
                    $x++;
                    
                    //Если сущ. id матрицы
                    if( $matrix_id_array[0] ) 
                    { 
                        DB::connection($connection)->table('dle_matrix_users')
                        ->where('uid', $current_uid)
                        ->update([
                            'mx_id' => $matrix_id_array[0],
                            'level' => 2,
                            'position' => $x
                        ]);

     
                        $this->update_user( array( 'matrix_id' => $matrix_id_array[0] ),  $current_uid);
                    }
                    else 
                    {
                        //$this->createLog("NotCMXID1", "NOT CURRENT mxid[0] ($j step)");
                        break;
                    }
                } 
                elseif( in_array( $j, Array(2, 4) ))
                {
                    $k++;
                    
                    //Если сущ. id матрицы
                    if( $matrix_id_array[1] ) 
                    {

                        DB::connection($connection)->table('dle_matrix_users')
                        ->where('uid', $current_uid)
                        ->update([
                            'mx_id' => $matrix_id_array[1],
                            'level' => 2,
                            'position' => $k
                        ]);

                        $this->update_user( array( 'matrix_id' => $matrix_id_array[1]  ),  $current_uid);
                    } 
                    else 
                    {
                        $this->createLog("NotCMXID2", "NOT CURRENT mxid[1] ($j step)");
                        break;
                    }
                }

            }

            /* КИДАЕМ ЮЗЕРОВ НА ТРЕТИЙ РЯД */
            $j = 0; // Общий счетчик
            $x = 0;
            $k = 0; // Вычислитель позиций
            
            
            
            if(count($_3LevelUsers))
            {
                foreach( $_3LevelUsers as $user_info_array)
                {
                    $j++;
                    
                    list($current_uid, $current_uname) = $user_info_array;
                    
                    if( in_array( $j, array( 1, 3, 5, 7) )  ) 
                    { 
                        $x++;
                        // Если сущ. id матрицы
                        if( $matrix_id_array[0] ) 
                        {                        


                            DB::connection($connection)->table('dle_matrix_users')
                            ->where('uid', $current_uid)
                            ->update([
                                'mx_id' => $matrix_id_array[0],
                                'level' => 3,
                                'position' => $x
                            ]);



                            $this->update_user( array( 'matrix_id' => $matrix_id_array[0] ),  $current_uid);
                        } 
                        else 
                        {
                            $this->createLog("NotCMXID1", "NOT CURRENT mxid[0] ($j step)");
                            break;
                        }
                    } 
                    elseif(in_array( $j, array( 2, 4, 6, 8) )  )
                    {
                        $k++;
                        // Если сущ. id матрицы
                        if( $matrix_id_array[1] ) 
                        { 
     
                            DB::connection($connection)->table('dle_matrix_users')
                            ->where('uid', $current_uid)
                            ->update([
                                'mx_id' => $matrix_id_array[1],
                                'level' => 3,
                                'position' => $k
                            ]);

                            $this->update_user( array( 'matrix_id' =>  $matrix_id_array[1] ),  $current_uid);
                        } 
                        else 
                        {
                            $this->createLog("NotCMXID2", "NOT CURRENT mxid[1] ($j step)");
                            break;
                        }
                    }
                }
            }

            DB::connection($connection)->table('dle_matrix')->where('matrix_id', $matrix_id )->delete();


            // Если есть темп ЗОЛОТА, вставляем его в матрицу
            if(isset($GoldTemp) )
            {
                $leader_row = &$GoldTemp;
                
                if($leader_row->by_refer == 0 )
                {                    
                    $sReferal = DB::connection($connection)->table('dle_users')->select('user_id')->where('by_refer', $leader_row->user_id)->orderBy('user_id', 'asc')->first();
                    $leader_row_by_refer = $sReferal->user_id;
                } else {
                    $leader_row_by_refer = $leader_row->by_refer;
                }
                
                // ИНАЧЕ::выполняем стандартные процедуры...
                $this->NewMatrixByRefer($leader_row_by_refer, $new_stage); # Ищем матрицу реферала, соответвующую той, что нужна нам!

                $new_matrix = current( $this->MatrixList ); # Получаем ID матрицы

                //$this->createLog('NewMatrix', 'New Matrix ID:' . $new_matrix . '|| USER FOR NEW MATRIX: ' . $leader_row['name']);

                # Временно обнуляем кол-во рефералов у пользователя, который уходит в новую матрицу             
                $this->update_user( array( 'ref_num' => '0'), $leader_row->user_id );
                
                # Добавляем реферала, тому чей реферал уходит в новую матрицу 
                 # (если его реферал находится в соотвественной матрице )
                    
                    $where_referal = $this->ScanMatrixRang($leader_row_by_refer); # &ссылаемся на ф-ю сканирования
                    
                    if( $where_referal->stage == $new_stage ) 
                    {
                        DB::connection($connection)->table('dle_users')->where('user_id',  $leader_row_by_refer )->increment('ref_num');   
                    }
                    elseif( $where_referal->stage ==  $row->stage )  #Если его реферал, в матрице ниже ранга, отнимаем у него 1го реферала
                    {
                    //  $this->update_user( array( 'ref_num' => 'ref_num - 1'), $leader_row['by_refer'] );
                    }
                
                // Перемещаем юзера в ту матрицу
                // $this->insertIntoMatrix($leader_row['user_id'], $leader_row['name'], $leader_row['by_refer'], $new_matrix );
            
                # -----------------------------------------------------------------------------------------------
            
                # ПООЩЕРЕНИЕ
                // Выдаём поощерение
                if( $new_stage > 0 )
 
                {
                
                
                //$upsum   = $this->balance[$new_stage];
                
               // $this->update_user( array( 'balance' => "balance + '800000'"), $leader_row['user_id']); # выполняем запрос

                //DB::connection($connection)->table('dle_users')->where('user_id',  $leader_row_by_refer )->increment('ref_num'); 
                
                
            //  -------------------------------------------------
            
                    DB::connection($connection)->table('dle_users')->where('user_id', $leader_row->user_id)->increment('balance', 90000);
                    $leader = $row->leader;
                    $leader_row = DB::connection($connection)->table('dle_users')->select('*')->where('name', $leader)->first();
                    $datee= date("d.m.Y H:i:s");

                    $perekid = DB::connection($connection)->table('dle_users')->select()->where('name', $leader)->first();
                    

                    $finished = Finished::create([
                        'program_id' => 3,
                        'user_id' => $leader_row->user_id,
                        'login' => $leader_row->name,
                        'fullname'  => $leader_row->familiya,
                        'birthdate' => $leader_row->otchestvo,
                        'iin' => $leader_row->strana,
                        'registred_pin' => $leader_row->screch,
                        'country' => $leader_row->imya,
                        'info' => $leader_row->signature,
                        'tovar' => $leader_row->tovar,
                        'phone' => $leader_row->pol,
                        'last' => json_encode($last_users, JSON_UNESCAPED_UNICODE),
                    ]);

                    DB::connection($connection)->table('dle_drop_users500')->insert(
                        [
                            'screch' => $perekid->screch,
                            'email' => $perekid->email,
                            'name' => $perekid->name,
                            'dateout' => $datee,
                            'balance' => 25000,
                            'familiya'  => $perekid->familiya,
                            'imya'  => $perekid->imya,
                            'otchestvo'  => $perekid->otchestvo,
                            'pol'  => $perekid->pol,
                            'strana'  => $perekid->strana,
                            'gorod'  => $perekid->gorod,
                            'INFO'  => $perekid->info,
                            'TOVAR'  => $perekid->signature,
                        ]
                    );
                }                 
                 
            }





           
    }
        
        return TRUE;
    }

    function ScanMatrixRang( $user_id ) {
		
        $connection = 'mysql_prod_main';    
        $row = DB::connection($connection)->select( "SELECT dle_matrix_users.mx_id, dle_matrix.stage FROM dle_matrix_users, dle_matrix WHERE dle_matrix_users.uid = ? AND dle_matrix.matrix_id = dle_matrix_users.mx_id", [$user_id] );
        
		return $row[0];
		
    }   

    public function finish(){

        $m = Config::get('matrix.get');
        $p = Session::get('programm', 1);
        $progs = [];
        foreach ($m as $key => $value) {
            $progs[$key] = $value[1];
        }

        $finished = Finished::where('user_id',  Auth::user()->user_id )->where('program_id', 1)->first(); 
 
        if(isset($finished->bonus)){

            $bonus = \App\Models\Bonuses\StartBonusRequestModel::where('id', $finished->bonus)->first();
             
            if($bonus->status_id == 4){
                echo view('front.finish.start', ['progs' => $progs]);  
            } else {
                echo view('front.finish_bonus', ['bonus' => $bonus]);                
            }
        }         

        if(isset($finished->pin)){
            $p = json_decode($finished->pin);
            switch ( $p->program_id ) {
                case 1:
                    $pin = \App\Models\Pins\StartPin::whereId($p->pin )->first();
                    break;
                case 2:
                    $pin = \App\Models\Pins\AutoPin::whereId($p->pin )->first();
                    break;           
                case 3:
                    $pin = \App\Models\Pins\MainPin::whereId($p->pin )->first();
                    break;            
                case 4:
                    $pin = \App\Models\Pins\AcummulativePin::whereId($p->pin )->first();
                    break;            
                case 5:
                    $pin = \App\Models\Pins\AcummulativePin_plusPin::whereId($p->pin )->first();
                    break;            
                case 6:
                    $pin = \App\Models\Pins\VipPin::whereId($p->pin )->first();
                    break;            
                case 7:
                    $pin = \App\Models\Pins\FastPin::whereId($p->pin )->first();
                    break;
                
                default:
                    $pin = \App\Models\Pins\StartPin::whereId($p->pin )->first();
                    break;
            }
            echo view('front.finish_pin', ['pin' => $pin]);            
        }
		
        //echo view('front.finish.start', ['progs' => $progs]);
        echo view('front.finish_main', ['progs' => $progs]);
    }


    
        function test($p, $t) {
            // Available alpha caracters
            $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';

            // generate a pin based on 2 * 7 digits + a random character
            $pin = mt_rand(1000000, 9999999)
                . mt_rand(1000000, 9999999)
                . $characters[rand(0, strlen($characters) - 1)]
                . $characters[rand(0, strlen($characters) - 1)]
                . $characters[rand(0, strlen($characters) - 1)];

            // shuffle the result
            $pin = str_shuffle($pin);
            if($p == 1){
                if($t == 1){
                    return 'N'.'STR'.$str = substr($pin, 0, 12);
                } else {
                    return  'STR'.substr($pin, 0, 12).'KSAN';                    
                }
            } elseif($p ==3){
                if($t == 1){
                    return 'N'.'MAIN'.$str = substr($pin, 0, 10);
                } elseif($t == 2) {
                    return  'MAIN'.substr($pin, 0, 8).'KSSAN';                    
                } else{
                    return  'MAIN'.substr($pin, 0, 8).'WWDN';
                }
            } elseif($p ==4){
                if($t == 1){
                    return 'F'.'BNK'.$str = substr($pin, 0, 10);
                } elseif($t == 2) {
                    return  'BNK'.substr($pin, 0, 8).'KSAF';                    
                } else{
                    return  'BNK'.substr($pin, 0, 8).'WWF';
                }
            } elseif($p == 5){
                if($t == 1){
                    return 'N'.'PLS'.$str = substr($pin, 0, 14);
                } elseif($t == 2) {
                    return  'PLS'.substr($pin, 0, 14).'KSAN';                    
                }
            } elseif($p == 6){
                if($t == 1){                    
                    return 'N'.'VIP'.$str = substr($pin, 0, 12);
                } elseif($t == 2) {
                    return  'VIP'.substr($pin, 0, 9).'KSSAN';                    
                } else{
                    return  'VIP'.substr($pin, 0, 9).'WWDN';
                }
            }

            }




}
