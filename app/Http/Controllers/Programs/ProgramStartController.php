<?php

namespace App\Http\Controllers\Programs;

use App\Http\Controllers\Programs\ProgramController;
use App\Models\Cancellation;
use App\Models\Finished;
use App\Models\Split;
use Auth;
use Carbon\Carbon;
use Config;
use DB;
use Session;

class ProgramStartController extends ProgramController {

	public function insertIntoMatrix($uid, $uname, $referal_id) {



 DB::transaction(function() use ($uid, $uname, $referal_id){

		$m = Config::get('matrix.get');
		$p = 1;

		$connection = 'mysql_prod_' . $m[$p][0];

		$matrix_user = DB::connection($connection)->table('dle_matrix_users')->select('mx_id')->where('uid', $referal_id)->first();

		$matrix_id = $matrix_user->mx_id;

		$row = DB::connection($connection)->table('dle_matrix')->select('*')->where('matrix_id', $matrix_id)->first();

		//$row = $db->super_query( "SELECT `leader`, `users_num`, `stage` FROM " . PREFIX . "_matrix WHERE matrix_id='" . $matrix_id . "'");
		// новые кол-во юзеров в матрице!

		$new_users_num = $row->users_num + 1;

		if ($row->users_num < 7) {
			$new_lv = 3;
		} else {
			$new_lv = 4;
		}

		// Вычисляем позицию

		$pos = DB::connection($connection)->table('dle_matrix_users')->select(DB::raw('count(*) as c'))->where('mx_id', $matrix_id)->whereLevel($new_lv)->first();

		// $pos = $db->super_query("SELECT COUNT(*) as c FROM `" . PREFIX . "_matrix_users` WHERE mx_id = '$matrix_id' AND level = $new_lv");
		$pos = $pos->c + 1;
		
		// Добавляем юзера в матрицу

		DB::connection($connection)->table('dle_matrix_users')->insert(
			[
				'uid' => $uid,
				'uname' => $uname,
				'mx_id' => $matrix_id,
				'level' => $new_lv,
				'position' => $pos,
			]
		);

		// Обновляем кол-во юзеров в матрице

		DB::connection($connection)->table('dle_matrix')->where('matrix_id', $matrix_id)
			->increment('users_num');

		// Обновляем id матрицы у пользователя
		$this->update_user(array('matrix_id' => $matrix_id), $uid);

		DB::connection($connection)->table('dle_users')->where('user_id', $referal_id)->increment('ref_num');

		$new_user = DB::connection($connection)->table('dle_users')->select('*')->where('user_id', $uid)->first();
		$sponsor_user = DB::connection($connection)->table('dle_users')->where('user_id', $referal_id)->first();

		$cancellation = Cancellation::create([
			'program_id' => 1,
			'pin' => $new_user->screch,
			'login' => $new_user->name,
			'fullname' => $new_user->fullname,
			'uid' => $new_user->user_id,
			'mx_id' => $new_user->matrix_id,
			'sponsor_id' => $sponsor_user->user_id,
			'sponsor_login' => $sponsor_user->name,
			'sponsor_fullname' => $sponsor_user->fullname,
			'reg_date' => Carbon::today()->toDateTimeString(),
		]);

		// Если необходимо деление матриц
		if (($row->stage == 0 && $new_users_num > 14)) {
			$last_users = DB::connection($connection)->table('dle_matrix_users')->where('mx_id', $matrix_id)->where('level', $new_lv)->get()->toArray();
			$main_matrix_users = DB::connection($connection)->table('dle_matrix_users')->where('mx_id', $matrix_id)->get()->toArray();

			# Производим деление матрицы, на 2 новых, главного старой матрицы кидаем в новую, выше уровнем

			# тут не забываем про нового юзера
			// Добавляем юзера в матрицу
			//$db->query( "INSERT INTO " . PREFIX . "_matrix_users VALUES('$uid', '$uname', '$matrix_id', 3, '$pos')");

			$spm = DB::connection($connection)->table('dle_matrix_users')
				->whereRaw('mx_id = ?', array($matrix_id))
				->orderBy('level', 'asc')
				->leftJoin('dle_users', 'dle_matrix_users.uid', '=', 'dle_users.user_id')
				->orderBy('position', 'ASC')
				->get();
			$split_main_matrix = [];

			foreach ($spm as $key => $value) {
				$split_main_matrix[$value->level][] = [
					'uid' => $value->user_id,
					'uname' => $value->uname,
					'stars' => $value->ref_num,
					'by_refer' => $this->getName($value->by_refer),
				];
			}

			foreach ($split_main_matrix as $key => $value) {
				sort($value);
			}

			$leader = $row->leader; # Лидер матрицы

			$leader_row = DB::connection($connection)->table('dle_users')->select('*')->where('name', $leader)->first();

			$split = new Split;
			$split->program_id = 1;
			$split->main_matrix_id = $matrix_id;
			$split->main_matrix = json_encode($split_main_matrix, true);
			$split->leader = json_encode($leader_row, true);
			$split->leader_id = $leader_row->user_id;
			$split->new = json_encode($new_user, true);
			$split->new_id = $new_user->user_id;
			$split->sponsor = json_encode($sponsor_user, true);
			$split->sponsor_id = $sponsor_user->user_id;
			$split->matrix_stage = $row->stage;
			$split->matrix_dateout = $row->datetout;
			// Получаем id матрицы, в которой находится тот, кто пригласил лидера
			//$parent_matrix_id = $db->super_query( "SELECT matrix_id FROM " . PREFIX . "_users WHERE user_id='" . $leader_row['by_refer'] . "'");

			// Удаляем из старой матрицы

			DB::connection($connection)->table('dle_matrix_users')->where('uid', $leader_row->user_id)->delete();
			$this->addNews($leader_row->name, 1, 1);

			// Если юзер - не уходит ...

			/*

				            if( $row['stage'] == 0 )
				            {
				                // ИНАЧЕ::выполняем стандартные процедуры...
				                $this->NewMatrixByRefer($leader_row['by_refer'], $new_stage); # Ищем матрицу реферала, соответвующую той, что нужна нам!

				                $new_matrix = current( $this->MatrixList ); # Получаем ID матрицы

				                // Перемещаем юзера в ту матрицу
				                $this->insertIntoMatrix($leader_row['user_id'], $leader_row['name'], $leader_row['by_refer'], $new_matrix );

				                # -----------------------------------------------------------------------------------------------
				            }

			*/

			if ($row->stage == 0 && $leader) {
				$leader = $row->leader;

				$leader_row = DB::connection($connection)->table('dle_users')->select('*')->where('name', $leader)->first();

				$datee = date("d.m.Y H:i:s");

				$perekid = DB::connection($connection)->table('dle_users')->select()->where('name', $leader)->first();

				$finished = Finished::create([
					'program_id' => 1,
					'user_id' => $leader_row->user_id,
					'login' => $leader_row->name,
					'fullname' => $leader_row->familiya,
					'birthdate' => $leader_row->otchestvo,
					'iin' => $leader_row->strana,
					'registred_pin' => $leader_row->screch,
					'country' => $leader_row->imya,
					'info' => $leader_row->signature,
					'tovar' => $leader_row->tovar,
					'phone' => $leader_row->pol,
					'last' => json_encode($last_users, JSON_UNESCAPED_UNICODE),
				]);

				if(!isset($perekid->info))
					$perekid->info = '';
				
				if(!isset($perekid->signature))
					$perekid->signature = '';
				
				DB::connection($connection)->table('dle_drop_users_start')->insert(
					[
						'screch' => $perekid->screch,
						'email' => $perekid->email,
						'user_id' => $perekid->user_id,
						'name' => $perekid->name,
						'dateout' => $datee,
						'balance' => 25000,
						'familiya' => $perekid->familiya,
						'imya' => $perekid->imya,
						'otchestvo' => $perekid->otchestvo,
						'pol' => $perekid->pol,
						'strana' => $perekid->strana,
						'gorod' => $perekid->gorod,
						'INFO' => $perekid->info,
						'TOVAR' => $perekid->signature,
					]
				);

				// DB::connection($connection)->table('dle_users')->select()->where('name', $leader)->delete();
			}

			# -----------------------------------------------------------------------------------------------

			// Получаем всех юзеров старой матрицы

			$m_users = DB::connection($connection)
				->table('dle_matrix_users')
				->select()
				->where('mx_id', $matrix_id)->get();

			$users = [];

			foreach ($m_users as $key => $user) {
				$users[$user->uid]['uid'] = $user->uid;
				$users[$user->uid]['level'] = $user->level;
				$users[$user->uid]['position'] = $user->position;
				$users[$user->uid]['stars'] = $this->getStars($user->uid);
				$users[$user->uid]['name'] = $user->uname;
			}

			usort($users, array($this, "starsComparator"));

			$leaders = array_slice($users, 0, 2);
			unset($users[0]);
			unset($users[1]);

			// Position counter
			$position_id = 0;

			$other_users = array();
			// $_3LevelUsers = array();
			$matrix_id_array = [];

			foreach ($leaders as $key => $leader) {
				$new_matrix_id = $this->CREATEMatrix($leader['name'], 7, 0);
				DB::connection($connection)->table('dle_matrix_users')
					->where('uid', $leader['uid'])
					->update([
						'mx_id' => $new_matrix_id,
						'level' => 1,
						'position' => 1,
					]);

				$this->update_user(array('matrix_id' => $new_matrix_id), $leader['uid']);

				$matrix_id_array[] = $new_matrix_id; // записываем ID новых матриц в массив
			}

			$keys['leaders'] = $leaders;
			$keys['users'] = $users;

			$j = 0;
			$k = 0;
			$i = 0;
			$i2 = 0;
			foreach ($users as $key => $user) {
				$j++;
				if (in_array($j, array(1, 3))) {
					$p = $j;
					$l = 2;
					$m = $matrix_id_array[0];
				} elseif (in_array($j, array(2, 4))) {
					$k++;
					$p = $k;
					$l = 2;
					$m = $matrix_id_array[1];

				} elseif (in_array($j, array(5, 7, 9, 11))) {
					$i++;
					$p = $i;
					$l = 3;
					$m = $matrix_id_array[0];

				} elseif (in_array($j, array(6, 8, 10, 12))) {
					$i2++;
					$p = $i2;
					$l = 3;
					$m = $matrix_id_array[1];
				}
				DB::connection($connection)->table('dle_matrix_users')
					->where('uid', $user['uid'])
					->update([
						'mx_id' => $m,
						'level' => $l,
						'position' => $p,
					]);

				DB::connection($connection)->table('dle_users')
					->where('user_id', $user['uid'])
					->update([
						'matrix_id' => $m,
					]);

			}

			$fm = DB::connection($connection)->table('dle_matrix_users')
				->whereRaw('mx_id = ?', array($matrix_id_array[0]))
				->orderBy('level', 'asc')
				->leftJoin('dle_users', 'dle_matrix_users.uid', '=', 'dle_users.user_id')
				->orderBy('position', 'ASC')
				->get();
			$split_first_matrix = [];

			foreach ($fm as $key => $value) {
				$split_first_matrix[$value->level][] = [
					'uid' => $value->user_id,
					'uname' => $value->uname,
					'stars' => $value->ref_num,
					'by_refer' => $this->getName($value->by_refer),
				];
			}

			foreach ($split_first_matrix as $key => $value) {
				sort($value);
			}

			$sm = DB::connection($connection)->table('dle_matrix_users')
				->whereRaw('mx_id = ?', array($matrix_id_array[1]))
				->orderBy('level', 'asc')
				->leftJoin('dle_users', 'dle_matrix_users.uid', '=', 'dle_users.user_id')
				->orderBy('position', 'ASC')
				->get();
			$split_second_matrix = [];

			foreach ($sm as $key => $value) {
				$split_second_matrix[$value->level][] = [
					'uid' => $value->user_id,
					'uname' => $value->uname,
					'stars' => $value->ref_num,
					'by_refer' => $this->getName($value->by_refer),
				];
			}

			foreach ($split_second_matrix as $key => $value) {
				sort($value);
			}

			$split->first_matrix = json_encode($split_first_matrix, true);
			$split->first_matrix_id = $matrix_id_array[0];
			$split->second_matrix = json_encode($split_second_matrix, true);
			$split->second_matrix_id = $matrix_id_array[1];
			$split->save();

		}

		return 'true';
	}, 25);
	}

	public function finish() {

		$m = Config::get('matrix.get');
		$p = Session::get('programm', 1);
		$progs = [];
		foreach ($m as $key => $value) {
			$progs[$key] = $value[1];
		}

		$finished = Finished::where('user_id', Auth::user()->user_id)->where('program_id', 1)->first();
		 
		/* 
		if(!isset($finished->bonus)) 
			$finished->bonus = 2;
		*/
		
		$bonus = \App\Models\Bonuses\StartBonusRequestModel::where('id', $finished->bonus)->where('view', 0)->orderBy('id', 'ASC')->first();
		  
		echo view('front.finish.start',  compact('progs', 'bonus'));
		
        //echo view('front.finish.start', ['progs' => $bonus]);
	}

}
