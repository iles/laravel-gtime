<?php

namespace App\Http\Controllers;

use App\Grids\SplitsGrid;
use App\Http\Controllers\Controller;
use App\Models\Finished;
use App\Models\Split;
use Carbon\Carbon;
use Config;
use DB;
use Illuminate\Http\Request;

class SplitController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(SplitsGrid $grid, Request $request) {

		$query = Split::query()->where('status_id', 0)->orderBy('id', 'DESC');

		return $grid
			->create(['query' => $query, 'request' => $request])
			->renderOn('split.index'); // render the grid on the welcome view
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		//
	}

	public function revert(Request $request) {

		$split = Split::find($request->id);

		$m = Config::get('matrix.get');
		$connection = 'mysql_prod_' . $m[$split->program_id][0];

		DB::connection($connection)->table('dle_matrix')->where('matrix_id', $split->first_matrix_id)->delete();
		DB::connection($connection)->table('dle_matrix')->where('matrix_id', $split->second_matrix_id)->delete();

		DB::connection($connection)->table('dle_matrix_users')->where('mx_id', $split->first_matrix_id)->delete();
		DB::connection($connection)->table('dle_matrix_users')->where('mx_id', $split->second_matrix_id)->delete();

		$leader = json_decode($split->leader, true);

		$new = json_decode($split->new, true);

		$old_users = json_decode($split->main_matrix, true);

		DB::connection($connection)->table('dle_users')->where('user_id', $split->new_id)->delete();

		DB::connection($connection)->table('dle_matrix')->where('matrix_id', $split->main_matrix_id)->decrement('users_num');


		if($split->program_id == 6 || $split->program_id == 3){
			if($split->matrix_stage == 0){

				$ldr = DB::connection($connection)->table('dle_users')->where('user_id', $leader['user_id'])->update([
					'matrix_id' => $leader['matrix_id'],
					'by_refer' => $leader['by_refer'],
				]);
			
				DB::connection($connection)->table('dle_matrix_users')->where('uname', $leader['name'])->delete();

				DB::connection($connection)->table('dle_matrix')->insert([
					'datetout' => $split->matrix_dateout,
					'stage' => $split->matrix_stage,
					'matrix_id' => $split->main_matrix_id,
					'leader' => $leader['name'],
					'users_num'	 => count($old_users),
				]);

			} else {
				DB::connection($connection)->table('dle_users')->insert($leader);
			}
		} else {
			DB::connection($connection)->table('dle_users')->where('name', $leader['name'])->delete();
			DB::connection($connection)->table('dle_users')->insert($leader);
		}

		foreach ($old_users as $level => $users) {
			$p = 1;
			foreach ($users as $k => $user) {
				if ($user['uname'] == $new['name']) {
					continue;
				}
				DB::connection($connection)->table('dle_matrix_users')->insert([
					'uid' => $user['uid'],
					'uname' => $user['uname'],
					'mx_id' => $split->main_matrix_id,
					'level' => $level,
					'position' => $p,
				]);
				$p++;
			}
		}

		$pin = \App\Models\Pins\StartPin::where('pin', $new['screch'])->first();
		$date = Carbon::now()->addDays(2)->toDateTimeString();
		$pin->expired_at = $date;
		$pin->status_id = 0;
		$pin->status_id = 0;
		$pin->username = null;
		$pin->save();

		DB::connection($connection)->table('dle_users')->where('user_id', $split->sponsor_id)->decrement('ref_num');

		$f = Finished::where('program_id', $split->program_id)->where('user_id', $split->leader_id)->delete();
		$split->status_id = 1;
		$split->save();
		return redirect('/admin/splits');

	}

	public function viewDrop(Request $request) {
		$finished = Finished::find($request->id);
		$m = Config::get('matrix.get');
		$p = $finished->program_id;
		$connection = 'mysql_prod_' . $m[$p][0];
		switch ($finished->program_id) {
		case 1:
			$table = 'dle_drop_users_start';
			break;
		case 2:
			$table = 'dle_drop_users';
			break;
		case 3:
			if ($finished->stage == 1) {
				$table = 'dle_drop_users80';
			} else {
				$table = 'dle_drop_users500';
			}
			break;
		case 4:
			$table = 'dle_drop_users';
			break;
		case 5:
			$table = 'dle_drop_users';
			break;
		case 6:
			$table = 'dle_drop_users9000';
			break;
		case 7:
			$table = 'dle_drop_users';
			break;
		default:
			# code...
			break;
		}
		$model = DB::connection($connection)->table($table)->where('name', $finished->login)->first();
		return view('split.drop', compact('model', 'p', 'finished'));

	}

	public function dropupdate(Request $request) {
		$m = Config::get('matrix.get');
		$p = $request->program_id;
		$connection = 'mysql_prod_' . $m[$p][0];
		switch ($p) {
		case 1:
			$table = 'dle_drop_users_start';
			break;
		case 2:
			$table = 'dle_drop_users';
			break;
		case 3:
			if ($finished->stage == 0) {
				$table = 'dle_drop_users80';
			} else {
				$table = 'dle_drop_users500';
			}
			break;
		case 4:
			$table = 'dle_drop_users';
			break;
		case 5:
			$table = 'dle_drop_users';
			break;
		case 6:
			$table = 'dle_drop_users9000';
			break;
		case 7:
			$table = 'dle_drop_users';
			break;
		default:
			# code...
			break;
		}
		DB::connection($connection)->table($table)->where('id', $request->id)->update(request()->except(['_token', 'program_id', 'id', 'fid', 'vip_bonus']));
		if ($request->vip_bonus) {
			$finished = Finished::find($request->fid);
			$finished->vip_bonus = $request->vip_bonus;
			$finished->save();
		}
		return redirect()->back();
	}
	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  \App\Models\Split  $split
	 * @return \Illuminate\Http\Response
	 */
	public function show(Split $split) {
		return view('split.show', compact('split'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  \App\Models\Split  $split
	 * @return \Illuminate\Http\Response
	 */
	public function edit(Split $split) {
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \App\Models\Split  $split
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, Split $split) {
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  \App\Models\Split  $split
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(Split $split) {
		//
	}
}
