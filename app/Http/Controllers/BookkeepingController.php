<?php

namespace App\Http\Controllers;

use App\Models\BonusRequest;
use Illuminate\Http\Request;
use App\Grids\BonusRequestsGrid;
use App\Models\WarehouseRequest;
use App\Grids\PinsGrid;
use App\Grids\WarehouseAdminRequestsGrid;
use Session;
use Config;
use DB;
use Carbon\Carbon;



class BookkeepingController extends Controller
{  
    public function index(BonusRequestsGrid $brg, Request $request)
    {
        $m = Config::get('matrix.get');
        $p = Session::get('programm', 1);

        $query = \App\Models\Bonuses\StartBonusRequestModel::query()->where('status_id', 1)->orderBy('id', 'DESC');


        return $brg
            ->create(['query' =>$query, 'request' => $request])
            ->renderOn('book.book'); // render the grid on the welcome view

    }     
 
    public function aprove(Request $request, $id, $s)
    {

        $m = Config::get('matrix.get');
        $p = Session::get('programm', 1);

        $model = \App\Models\Bonuses\StartBonusRequestModel::findOrFail($id);
 
        $model->status_id = $s;

        $m = Config::get('matrix.get');
        $p = Session::get('programm', 1);
         
		$connection = 'mysql_prod_'.$m[$model->program_id][0];
 
        switch ($model->program_id) {
            case 1:
                $table = 'dle_drop_users_start';
                break;
            case 2:
                $table = 'dle_drop_users';
                break;
            case 3:
                if ($finished->stage == 1) {
                    $table = 'dle_drop_users80';
                } else {
                    $table = 'dle_drop_users500';
                }
                break;
            case 4:
                $table = 'dle_drop_users';
                break;
            case 5:
                $table = 'dle_drop_users';
                break;
            case 6:
                $table = 'dle_drop_users9000';
                break;
            case 7:
                $table = 'dle_drop_users';
                break;
            case 8:
                $table = 'dle_drop_users9000';
                break;
            default:
                # code...
                break;
            }

            if($s == 4){
				
				Session::flash('message', 'Заявка переданна в бухгалтерию'); 
                $setr = 'Выдано : '.Carbon::now();
            } else {
				
				Session::flash('message', 'Заявка отклонена'); 
                $setr = '';
            } 
			
            $drop = DB::connection($connection)->table($table)->where('name', $model->uname)->update(['setr' => $setr]);
  
        $model->cancel_reason = $request->cancel_reason;
        $model->fact_date = Carbon::now();
        $model->save();
		 
        Session::flash('alert-class', 'alert-sucscess'); 
        return redirect( route('bookkeeping.index') );
    }

    public function warehouse(WarehouseAdminRequestsGrid $sprInterface, Request $request){

        return $sprInterface
                    ->create(['query' => WarehouseRequest::query(), 'request' => $request])
                    ->renderOn('book.warehouse'); // render the grid on the welcome view

    }    

    public function credit(PinsGrid $pinGrid, Request $request)
    {
		
		$p = Session::get('programm', 1);
		$query = \App\Models\PinRequests\StartRequest::query()->whereNull('transitive')->orderBy('created_at', 'DESC');
            return $pinGrid
					->create(['query' => $query, 'request' => $request])
					->renderOn('book.credit');   
    } 
} 