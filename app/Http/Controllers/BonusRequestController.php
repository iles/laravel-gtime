<?php

namespace App\Http\Controllers;

use App\Models\BonusRequest;
use App\Models\Bonus;
use Illuminate\Http\Request;
use App\Grids\BonusRequestsGrid;
use App\Grids\BonusesGrid;
use App\Models\User;
use Session;
use Config;

use App\Http\Requests\StoreBonusRequest;

class BonusRequestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(BonusesGrid $brg, Request $request)
    {

        $m = Config::get('matrix.get');
        $p = Session::get('programm', 1);

        //$query = \App\Models\Bonuses\StartBonusRequestModel::query()->where('status_id', '<>', 2)->where('status_id', '<>', 3)->orderBy('id', 'DESC');
        $query = \App\Models\Bonuses\StartBonusRequestModel::query();
		$query->where('status_id', '<>', 2)->where('status_id', '<>', 3)->orderBy('id', 'DESC');
			
        return $brg
            ->create(['query' => $query, 'request' => $request])
            ->renderOn('bonus-request.index');  
    }

	/*обновление статуса*/
	public function statusupdate(Request $request) {
		 
        $m = Config::get('matrix.get');
        $p = Session::get('programm', 1);
		   
        $model = \App\Models\Bonuses\StartBonusRequestModel::findOrFail($request->id);

        $model->status_id = 0;
        $model->save();
        
		/* 
        $model = BonusRequest::findOrFail($id);
        $model->fill($request->all());
        
        $model->save();
        Session::flash('message', 'Заявка обновлена'); 
        Session::flash('alert-class', 'alert-sucscess'); 
        return redirect( route('bonusrequest.index') );*/
		 
		//return redirect('/admin/bookkeeping');
	}
	
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $m = Config::get('matrix.get');
        $progs = [];
        foreach ($m as $key => $value) {
            $progs[$key] = $value[1];
        }
        return view('bonus-request.form', compact('progs') );
    }    

    public function create_admin()
    {
        $m = Config::get('matrix.get');
        $progs = [];
        foreach ($m as $key => $value) {
            $progs[$key] = $value[1];
        }
        return view('bonus-request.form_admin', compact('progs') );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $br = BonusRequest::create($request->all());
        Session::flash('message', 'Заявка созданна'); 
        Session::flash('alert-class', 'alert-sucscess'); 
        return redirect( route('bonusrequest.index') );
    }   

    public function store_admin(Request $request)
    {
        $m = Config::get('matrix.get');
        $p = Session::get('programm', 1);


        $connection = 'mysql_prod_'.$m[$p][0];


        switch ( $m[$p][0] ) {
            case 'start':
                $model = new \App\Models\Bonuses\StartBonusRequestModel;
                break;
            case 'auto':
                $model = new \App\Models\Bonuses\AutoBonusRequestModel;
                break;           
            case 'main':
                $model = new \App\Models\Bonuses\MainBonusRequestModel;
                break;            
            case 'vip':
                $model = new \App\Models\Bonuses\VipBonusRequestModel;
                break;            
            case 'acummulative':
                $model = new \App\Models\Bonuses\AccumulativeBonusRequestModel;
                break;            
            case 'acummulative_plus':
                $model = new \App\Models\Bonuses\AccumulativePlusBonusRequestModel;
                break;
            case 'fast':
                $model = new \App\Models\Bonuses\FastBonusRequestModel;
                break;
            default:
                $model = new \App\Models\Bonuses\StartBonusRequestModel;
                break;
        }

        $model->fill($request->all());

         $user = User::on($connection)->whereName($request->uname)->first();
         if(!$user){
            Session::flash('message', 'Пользователя с таким ником не существует!'); 
            Session::flash('alert-class', 'alert-error'); 
            return redirect( '/admin/bonusrequest' );            
         } else {
            $model->uid = $user->user_id;
            $model->save();
            Session::flash('message', 'Заявка созданна'); 
            Session::flash('alert-class', 'alert-sucscess'); 
            return redirect( '/admin/bonusrequest' );
         }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\BonusRequest  $bonusRequest
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       $model = \App\Models\Bonuses\StartBonusRequestModel::findOrFail($id);

        $m = Config::get('matrix.get');
        $progs = [];
        foreach ($m as $key => $value) {
            $progs[$key] = $value[1];
        }
        return view('bonus-request.form', compact('progs', 'model'));
    }

    public function aprove($id, $s)
    {
        $m = Config::get('matrix.get');
        $p = Session::get('programm', 1);

        $model = \App\Models\Bonuses\StartBonusRequestModel::findOrFail($id);

        $model->status_id = $s;
        $model->save();
        
        Session::flash('message', 'Заявка переданна в бухгалтерию'); 
        Session::flash('alert-class', 'alert-sucscess'); 
        return redirect( route('bonusrequest.index') );
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\BonusRequest  $bonusRequest
     * @return \Illuminate\Http\Response
     */
    public function edit(BonusRequest $bonusRequest)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\BonusRequest  $bonusRequest
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        dd($request);
        $model = BonusRequest::findOrFail($id);
        $model->fill($request->all());
        
        $model->save();
        Session::flash('message', 'Заявка обновлена'); 
        Session::flash('alert-class', 'alert-sucscess'); 
        return redirect( route('bonusrequest.index') );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\BonusRequest  $bonusRequest
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $model = BonusRequest::findOrFail($request->id);
        $model->delete();
        return new JsonResponse([
                'success' => true,
                'message' => 'Заявка удалена'
            ]);
    }
}
