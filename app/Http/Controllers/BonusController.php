<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Bonus;
use App\Models\BonusRequest;
use App\Models\Partner;
use App\Models\Finished;
use App\Models\Transfer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Session;
use Config;
use App\Grids\BonusesGrid;

class BonusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(BonusesGrid $brg, Request $request)
    {
         $m = Config::get('matrix.get');
        $p = Session::get('programm', 1);

        $query = \App\Models\Bonuses\StartBonusRequestModel::query()
            ->whereNotIn('status_id',[2,3])
            //->where('status_id', '<>', 2)->where('status_id', '<>', 3)
            ->orderBy('id', 'DESC');

        if($request->has('q')){
            $query->where(function ($query) use ($request) {
                foreach (\App\Models\Bonuses\StartBonusRequestModel::$searchColumnList as $key => $col) {
                    $query->orWhere($col, $request->input('q'));
                }
            });
        }

        return $brg
            ->create(['query' => $query, 'request' => $request])
            ->renderOn('bonus-request.index'); // render the grid on the welcome view
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $m = Config::get('matrix.get');
        $progs = [];
        foreach ($m as $key => $value) {
            $progs[$key] = $value[1];
        }
        return view('bonus.form', compact('progs') );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }


    public function statusupdate(Request $request){


        $m = Config::get('matrix.get');
        $p =  Session::get('programm', 1);
  

        $model = \App\Models\Bonuses\StartBonusRequestModel::where('id', $request->id)->first();

        $model->update($request->all());
        $model->status_id = $request->status;
        $model->cancel_reason = $request->cancel_reason;
        $model->save();


        if($request->status == 1){
            $bonus = BonusRequest::create([
                'uname' => $model->uname,
                'name' => $model->firstname,
                'second_name' => $model->lastname,
                'patronic' => $model->patronic,
                'program' =>  $p,
                'summ' => $model->summ,
                'iin' => $model->lastname,
                'bill_number' => $model->bill_num,
            ]);
        }


        return redirect('/admin/bonusrequest');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Bonus  $bonus
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
         
    $m = Config::get('matrix.get');
    $p = Session::get('programm', 1);

    $model = \App\Models\Bonuses\StartBonusRequestModel::findOrFail($id);
 
    $transfer = null;

    $finished = Finished::find($model->finished_id);
    if(isset($finished->transfer)){
        $transfer = Transfer::find($finished->transfer);
    }
        $m = Config::get('matrix.get');
        $p = Session::get('programm', 1);
        $type = $m[$p][0];
        $connection = 'mysql_prod_' . $type;


    $fio = $model->fullname;
    $users = [];

       if($model->vip_bonus_logins){

        $m = Config::get('matrix.get');
        $p = Session::get('programm', 1);
        $type = $m[6][0];
        $connection = 'mysql_prod_' . $type;

        $l = explode(',', $model->vip_bonus_logins);


         foreach ($l as $key => $value) {
            $p = Partner::on($connection)->where('name', trim($value) )->first();
            $users[] = $p;
         }
       }
	   
	$refers = User::on($connection)->where('by_refer', $model->user_id)->get();

        $m = Config::get('matrix.get');
        $progs = [];
        foreach ($m as $key => $value) {
            $progs[$key] = $value[1];
        }
        return view('bonus.show', compact('progs', 'model', 'users', 'refers', 'transfer', 'fio'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Bonus  $bonus
     * @return \Illuminate\Http\Response
     */
    public function edit(Bonus $bonus)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Bonus  $bonus
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Bonus $bonus)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Bonus  $bonus
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $m = Config::get('matrix.get');
        $p =  Session::get('programm', 1);
  
        switch ( $m[ $p ][ 0 ] ) {
            case 'start':
                 $model = \App\Models\Bonuses\StartBonusRequestModel::findOrFail($id);
                break;
            case 'auto':
                 $model = \App\Models\Bonuses\AutoBonusRequestModel::findOrFail($id);
                break;           
            case 'main':
                 $model = \App\Models\Bonuses\MainBonusRequestModel::findOrFail($id);
                break;            
            case 'vip':
                 $model = \App\Models\Bonuses\VipBonusRequestModel::findOrFail($id);
                break;            
            case 'acummulative':
                 $model = \App\Models\Bonuses\AccumulativeBonusRequestModel::findOrFail($id);
                break;            
            case 'acummulative_plus':
                 $model = \App\Models\Bonuses\AccumulativePlusBonusRequestModel::findOrFail($id);
                break;
            case 'fast':
                 $model = \App\Models\Bonuses\FastBonusRequestModel::findOrFail($id);
                break;
            default:
                 $model = \App\Models\Bonuses\StartBonusRequestModel::findOrFail($id);
                break;
        }


        $model->delete();
        return new JsonResponse([
                'success' => true,
                'message' => 'Заявка удалена'
            ]);
    }
}
