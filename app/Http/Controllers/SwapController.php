<?php

namespace App\Http\Controllers;
use App\Grids\SwapsGrid;
use App\Models\Swap;
use Illuminate\Http\Request;
use App\Models\Partner;
use Carbon\Carbon;
use Config;
use Session;
use Auth;

class SwapController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(SwapsGrid $grid, Request $request){

        return $grid
            ->create(['query' => Swap::query()->orderBy('id', 'DESC'), 'request' => $request])
            ->renderOn('swap.index'); // render the grid on the welcome view

    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }    

    public function make(Request $request)
    {
        $swap = Swap::find($request->id);

                $auth = Auth::guard('web')->user()->roles()->first()->name;


        if($auth == 'security'){
            $swap->sec_check = 1;
            $swap->save();
            return redirect()->back();

        }

        if($request->status == 2){
            $swap->status_id = 2;
            $swap->cancel_reason = $request->cancel_reason;
            $swap->save();
            return redirect('/swaps');
        }
        if($swap->type == 1 || $swap->type == 2){
            $this->swap_profiles($swap);
        } else {
            $this->swap_spnsr($swap);            
        }
        $swap->status_id = 1;
        $swap->save();


        return redirect('/swaps');
    }


    public function swap_profiles($swap) {
        
        $m = Config::get('matrix.get');
        $type = $m[$swap->program_id][0];
        $connection = 'mysql_prod_' . $type;

        $var = $swap->reason ==  1 ? 'По приказу №7' : 'По заявлению владельца';

        $partner = Partner::on($connection)->where('name', $swap->login)->first();
        $partner2 = Partner::on($connection)->where('name', $swap->login2)->first();
        $mid = $partner->replicate();
        $partner->info .= 'Закрывает: ' . $partner2->familiya . ';';
        $partner->info .= ' Логин: ' . $swap->login2;
        $partner->info .= ' ИИН: ' . $partner2->strana;
        $partner->info .= ' Дата рождения: ' . $partner2->otchestvo;
        $partner->info .= ' Телефон: ' . $partner2->pol;
        $partner->info .= $var;
        $partner->info .= ' Дата: ' . Carbon::now();
        $partner->save();

        $partner2->info .= 'Закрывает: ' . $mid->familiya . ';';
        $partner2->info .= ' Логин: ' . $swap->login;
        $partner2->info .= ' ИИН: ' . $mid->strana;
        $partner2->info .= ' Дата рождения: ' . $mid->otchestvo;
        $partner2->info .= ' Телефон: ' . $mid->pol;
        $partner2->info .= $var;
        $partner2->info .= ' Дата: ' . Carbon::now();
        $partner2->save();

        // $user1 = MatrixUser::on($connection)->whereRaw('uname = ?',array($swap->login))->first();
        // $user2 = MatrixUser::on($connection)->whereRaw('uname = ?',array($swap->login2))->first();
        // $m1 = $user1->mx_id;
        // $m2 = $user2->mx_id;
        // $user1->mx_id = $m2;
        // $user1->save();
        // $user2->mx_id = $m1;
        // $user2->save();
        Session::flash('message', 'Замена ' . $swap->login . ' на ' . $swap->login2 . ' прошла успешно');
        Session::flash('alert-class', 'alert-success');

    }

    public function swap_spnsr($swap){
        $m = Config::get('matrix.get');
        $type = $m[$swap->program_id][0];
        $connection = 'mysql_prod_' . $type;

        $user = Partner::on($connection)->whereRaw('name = ?', array($swap->login))->first();
        $sponsor = Partner::on($connection)->whereRaw('name = ?', array($swap->login2))->first();
        $user->by_refer = $sponsor->user_id;
        $user->save();
        Session::flash('message', 'Замена спонсора прошла успешно');
        Session::flash('alert-class', 'alert-success');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Swap  $swap
     * @return \Illuminate\Http\Response
     */
    public function show(Swap $swap){

        $auth = Auth::guard('web')->user()->roles()->first()->name;

        if($auth == 'security'){

        $connection = 'mysql_prod_vip';

        $user = Partner::on($connection)->whereRaw('name = ?', array($swap->login2))->first();
            return view('swap.show_sec', compact('swap', 'user'));
        } else {
            return view('swap.show', compact('swap'));
        }
    
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Swap  $swap
     * @return \Illuminate\Http\Response
     */
    public function edit(Swap $swap)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Swap  $swap
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Swap $swap)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Swap  $swap
     * @return \Illuminate\Http\Response
     */
    public function destroy(Swap $swap)
    {
        //
    }
}
