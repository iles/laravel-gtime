<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\CronRemoves;
use Config;
use Session;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    public function change(Request $request){
        Session::put('programm', $request->id);
        Auth::logout();
    }    

    public function reset(Request $request){
        Session::put('programm', $request->id);
        Auth::logout();
    }

    public function authValidate(Request $request){

        $m = Config::get('matrix.get');
        $p = Session::get('programm', 1);
       
        $type = $m[$p][0];

        $connection = 'mysql_prod_'.$type;

        $user = User::on($connection)->where('name', $request->name )->first();
        if($user){ 
            if( md5( md5($request->password) ) == $user->password ){
                return response()->json([
                    'error' => '0', 
                ]);
            } else{
                return response()->json([
                    'error' => '1', 
                    'message' => 'Неправильный логин или пароль', 
                ]);                
            }
            
        } else{
            return response()->json([
                'error' => '1', 
                'message' => 'Неправильный логин или пароль', 
            ]);
        }
    }  

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function test(){
        CronRemoves::remove();
    }
}
