<?php

namespace App\Http\Controllers;

use App\Models\Payment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Grids\PaymentsGrid;
use App\Models\Partner;
use Config;

class PaymentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(PaymentsGrid $paymentGrid, Request $request)
    {
        $query = Payment::query()->orderBy('id', 'DESC');
        
        return $paymentGrid
                    ->create(['query' => $query, 'request' => $request])
                    ->renderOn('payments.index'); // render the grid on the welcome view 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $payment = Payment::create( $request->all() );
        if ($request->hasFile('photo')) {
            $imageName = time().'.'.$request->photo->getClientOriginalExtension();
            $request->photo->move(public_path('uploads/payments/'), $imageName);
            $payment->photo = $imageName;
            $payment->save();
        }
        return redirect('/partner/finish');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Payment  $payment
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $model = Payment::whereId($request->id)->first();

        $m = Config::get('matrix.get');
        $type = $m[$model->program_id][0];
        $connection = 'mysql_prod_'.$type;
        
        $user = Partner::on($connection)->where('user_id', $model->user_id)->first(); 
        return view('payments.show', ['model'=>$model, 'user' => $user]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Payment  $payment
     * @return \Illuminate\Http\Response
     */
    public function edit(Payment $payment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Payment  $payment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Payment $payment)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Payment  $payment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Payment $payment)
    {
        //
    }


    public function statusupdate(Request $request){
        $model = Payment::whereId( $request->id )->first();
        $model->status = $request->status;
        //$model->cancel_reason = $request->cancel_reason;
        $model->save();
        return redirect('/payments');  
    }

}
