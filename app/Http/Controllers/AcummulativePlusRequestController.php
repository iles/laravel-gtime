<?php

namespace App\Http\Controllers;

use App\Models\PinRequests\Acummulative_plusRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AcummulativePlusRequestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\PinRequests\Acummulative_plusRequest  $acummulative_plusRequest
     * @return \Illuminate\Http\Response
     */
    public function show(Acummulative_plusRequest $acummulative_plusRequest)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\PinRequests\Acummulative_plusRequest  $acummulative_plusRequest
     * @return \Illuminate\Http\Response
     */
    public function edit(Acummulative_plusRequest $acummulative_plusRequest)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\PinRequests\Acummulative_plusRequest  $acummulative_plusRequest
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Acummulative_plusRequest $acummulative_plusRequest)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\PinRequests\Acummulative_plusRequest  $acummulative_plusRequest
     * @return \Illuminate\Http\Response
     */
    public function destroy(Acummulative_plusRequest $acummulative_plusRequest)
    {
        //
    }
}
