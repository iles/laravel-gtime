<?php

namespace App\Http\Controllers;

use App\Grids\PinsGrid;
use App\Grids\PinsEGrid;
use App\Models\AccumulativePinRequest;
use App\Models\AccumulativePlusPinRequest;
use App\Models\AutoPinRequest;
use App\Models\MainPinRequest;
use App\Models\Pin;
use App\Models\StartPinRequest;
use App\Models\User;
use App\Models\VipPinRequest;
use Auth;
use Carbon\Carbon;
use Config;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Image;
use Illuminate\Support\Facades\DB;

class PinsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:web');
    }

    public function index(PinsGrid $pinGrid, Request $request)
    {

        $p = Session::get('programm', 1);

        $query = \App\Models\PinRequests\StartRequest::query();

        $query->where(function ($query) use ($request) {
            $query->whereNull('transitive');
            if ($request->has('q')) {
                $query->whereExists(function ($query) use ($request) {
                    $query->select(DB::raw(1))
                        ->from('start_pins')
                        ->whereRaw('start_requests.id = start_pins.request_id')
                        ->where('start_pins.pin', $request->input('q'));
                });
                // $query->where('uname',$request->input('q'));
            }
        });

        $query->orderBy('created_at', 'DESC');
        // dd($query->toSql());
        return $pinGrid
            ->create(['query' => $query, 'request' => $request])
            ->renderOn('pins.index');
    }

    public function export(PinsEGrid $pinGrid, Request $request)
    {
        $p = Session::get('programm', 1);
        $query = \App\Models\PinRequests\StartRequest::query()->whereNull('transitive')->orderBy('created_at', 'DESC');
        return $pinGrid
            ->create(['query' => $query, 'request' => $request])
            ->renderOn('pins.index'); // render the grid on the welcome view
    }

    public function export_create()
    {
        return 'ok';
    }

    public function export_show()
    {
        return 'ok';
    }

    public function export_destroy()
    {
        return 'ok';
    }


    public function create()
    {
        return view('pins.create');
    }

    public function create_kassa()
    {
        return view('pins.create_kassa');
    }

    public function pin_stat(Request $request)
    {
        $pin = \App\Models\Pins\StartPin::where('id', $request->id)->first();
        $pin->status_id = $request->status;
        $res = 'заблокирован';
        if ($request->status == 0) {
            $date = Carbon::now()->addDays(2)->toDateTimeString();
            $pin->expired_at = $date;
            $res = 'Действителен до: <br/>'.$pin->expired_at;
        }
        $pin->save();

        return new JsonResponse([
            'success' => true,
            'message' => 'Статус обновлен',
            'res' => $res,
        ]);
    }

    public function pin_delete(Request $request)
    {
        $pin = \App\Models\Pins\StartPin::where('id', $request->id)->first();
        $pin->delete();

        return new JsonResponse([
            'success' => true,
            'message' => 'Пин код удален',
        ]);
    }

    public function store_kassa(Request $request)
    {

        $p = Session::get('programm', 1);
        $pinRequest = new \App\Models\PinRequests\StartRequest;

        $pinRequest->uname = $request->uname ? $request->uname : Auth::user()->name;
        $pinRequest->bill = $request->bill ? $request->bill : 0;
        $pinRequest->iin = $request->iin ? $request->iin : 0;
        $pinRequest->summ = $request->summ ? $request->summ : 0;
        $pinRequest->skype = $request->skype ? $request->skype : 0;
        $pinRequest->note = $request->note;
        $pinRequest->kassa = $request->kassa;
        $pinRequest->status_id = 1;
        $pinRequest->user_id = 0;
        $imageName = time();
        $pinRequest->image = 0;
        $pinRequest->country = 0;
        $pinRequest->fullname = 0;
        $pinRequest->date = 0;
        if ($request->hasFile('image')) {
            $imageName = time().'.'.$request->image->getClientOriginalExtension();
            $request->image->move(public_path('uploads/bills/'), $imageName);
            $pinRequest->image = $imageName;
        }

        $pinRequest->save();

        $pin = new \App\Models\Pins\StartPin();
        if ($request->users) {
            foreach ($request->users as $key => $value) {
                switch ($value['type']) {

                    case 1:
                        $pin = new \App\Models\Pins\StartPin();
                        break;
                    case 2:
                        $pin = new \App\Models\Pins\AutoPin();
                        break;
                    case 3:
                        $pin = new \App\Models\Pins\MainPin();
                        break;
                    case 4:
                        $pin = new \App\Models\Pins\AcummulativePin();
                        break;
                    case 5:
                        $pin = new \App\Models\Pins\Acummulative_plusPin();
                        break;
                    case 6:
                        $pin = new \App\Models\Pins\VipPin();
                        break;
                    case 7:
                        $pin = new \App\Models\Pins\FastPin();
                        break;

                    default:
                        # code...
                        break;
                }

                $pin->user_id = 0;
                $pin->request_id = $pinRequest->id;
                $pin->request_program_id = $p;
                $pin->kassa = $request->kassa;
                $pin->pin = $this->generatePIN($value['type'], 2);
                $date = Carbon::now()->addDays(2)->toDateTimeString();
                $pin->expired_at = $date;
                while (true) {
                    try {
                        $pin->save();
                        break;
                    } catch (\Illuminate\Database\QueryException $e) {
                        $pin->pin = $this->generatePIN($value['type'], 2);
                        continue;
                    }
                };

            }
        }

        Session::flash('message', 'Заявка созданна');
        Session::flash('alert-class', 'alert-sucscess');

        return redirect('/admin/kassa');
    }

    public function checkStatus(Request $request)
    {

        $p = $request->program;

        $req = \App\Models\PinRequests\StartRequest::where('id', $request->id)->first();

        if ((int)$request->status_id !== (int)$req->status_id) {
            return new JsonResponse([
                'change' => true,
                'status' => $req->status_id,
            ]);
        } else {
            return new JsonResponse([
                'change' => false,
            ]);
        }
    }

    public function show(Request $request)
    {
        $p = Session::get('programm', 1);
        $model = \App\Models\PinRequests\StartRequest::whereId($request->pin)->first();
        $pins = $model->pins()->get()->groupBy('program');

        return view('pins.show', ['model' => $model, 'pins' => $pins]);
    }

    public function show_kassa(Request $request)
    {
        $p = Session::get('programm', 1);
        $model = \App\Models\PinRequests\StartRequest::whereId($request->pin)->first();

        $pins = [
            'start' => \App\Models\Pins\StartPin::where('request_id', $model->id)->where('request_program_id',
                $p)->get(),
            'auto' => \App\Models\Pins\AutoPin::where('request_id', $model->id)->where('request_program_id', $p)->get(),
            'main' => \App\Models\Pins\MainPin::where('request_id', $model->id)->where('request_program_id', $p)->get(),
            'acummulative' => \App\Models\Pins\AcummulativePin::where('request_id',
                $model->id)->where('request_program_id', $p)->get(),
            'acummulative_plus' => \App\Models\Pins\Acummulative_plusPin::where('request_id',
                $model->id)->where('request_program_id', $p)->get(),
            'vip' => \App\Models\Pins\VipPin::where('request_id', $model->id)->where('request_program_id', $p)->get(),
            'fast' => \App\Models\Pins\FastPin::where('request_id', $model->id)->where('request_program_id', $p)->get(),
        ];
        return view('pins.show_kassa', ['model' => $model, 'pins' => $pins]);
    }

    public function print(Request $request)
    {
        $p = Session::get('programm', 1);
        $model = \App\Models\PinRequests\StartRequest::whereId($request->pin)->first();

        $pins = [
            'start' => \App\Models\Pins\StartPin::where('request_id', $model->id)->where('request_program_id',
                $p)->get(),
            'auto' => \App\Models\Pins\AutoPin::where('request_id', $model->id)->where('request_program_id', $p)->get(),
            'main' => \App\Models\Pins\MainPin::where('request_id', $model->id)->where('request_program_id', $p)->get(),
            'acummulative' => \App\Models\Pins\AcummulativePin::where('request_id',
                $model->id)->where('request_program_id', $p)->get(),
            'acummulative_plus' => \App\Models\Pins\Acummulative_plusPin::where('request_id',
                $model->id)->where('request_program_id', $p)->get(),
            'vip' => \App\Models\Pins\VipPin::where('request_id', $model->id)->where('request_program_id', $p)->get(),
            'fast' => \App\Models\Pins\FastPin::where('request_id', $model->id)->where('request_program_id', $p)->get(),
        ];
        return view('pins.print', ['model' => $model, 'pins' => $pins]);
    }

    public function aprove($id, $s)
    {
        $model = Pin::findOrFail($id);

        $model->status_id = $s;
        $model->save();
        Session::flash('message', 'Пин код сгенерирован');
        Session::flash('alert-class', 'alert-sucscess');
        return redirect(route('bonusrequest.index'));
    }

    public function store(Request $request)
    {
        $p = Session::get('programm', 1);
        $pinRequest = new \App\Models\PinRequests\StartRequest();

        $pinRequest->uname = $request->uname ? $request->uname : Auth::user()->name;
        $pinRequest->bill = $request->bill ? $request->bill : 0;
        $pinRequest->iin = $request->iin ? $request->iin : 0;
        $pinRequest->summ = $request->summ ? $request->summ : 0;
        $pinRequest->fullname = $request->fullname ? $request->fullname : 0;
        $pinRequest->date = $request->date ? $request->date : 0;
        $pinRequest->country = $request->country ? $request->country : 0;
        $pinRequest->skype = $request->skype ? $request->skype : 0;
        $pinRequest->phone = $request->phone ? $request->phone : 0;
        $pinRequest->city = $request->city ? $request->city : 0;
        $pinRequest->currency = $request->currency ? $request->currency : 0;
        $pinRequest->note = $request->note;
        $pinRequest->kassa = $request->kassa;
        $pinRequest->status_id = 1;
        $pinRequest->user_id = 0;
        $pinRequest->program = $p;
        $imageName = time();
        $pinRequest->image = 0;

        if ($request->hasFile('image')) {
            if (is_array($request->image)) {
                $image = $request->image[0];
            } else {
                $image = $request->image;
            }
            $imageName = time().'.'.$image->getClientOriginalExtension();
            $image->move(public_path('uploads/bills/'), $imageName);
            $pinRequest->image = $imageName;
        }

        $pinRequest->save();
        $pinRequest->aprove_at = $pinRequest->created_at;
        $pinRequest->save();

        if ($request->start_pin) {
            for ($i = 0; $i < $request->start_pin; $i++) {
                $gen = 1;
                $pin = new \App\Models\Pins\StartPin();
                $pin->screch = 0;
                $pin->user_id = 0;
                $pin->request_id = $pinRequest->id;
                $pin->request_program_id = $p;
                $date = Carbon::now()->addDays(2)->toDateTimeString();
                $pin->expired_at = $date;
                $pin->program = 1;

                if ($request->start_pin_timeless) {
                    $pin->timeless = 1;
                }

                if ($request->start_pin_kassa) {
                    $pin->kassa = 1;
                    $gen = 2;
                }
                $pin->pin = $this->generatePIN(1, $gen);

                try {
                    $pin->save();
                } catch (\Illuminate\Database\QueryException $e) {
                    throw $e;
                };

            }
        }

        if ($request->bank_pin) {
            for ($i = 0; $i < $request->bank_pin; $i++) {
                $gen = 1;
                $pin = new \App\Models\Pins\StartPin();
                $pin->screch = 0;
                $pin->user_id = 0;
                $pin->request_id = $pinRequest->id;
                $pin->request_program_id = $p;
                $date = Carbon::now()->addDays(2)->toDateTimeString();
                $pin->expired_at = $date;
                $pin->program = 4;

                if ($request->bank_pin_timeless) {
                    $pin->timeless = 1;
                }

                if ($request->bank_pin_kassa) {
                    $pin->kassa = 1;
                    $gen = 2;
                }
                $pin->pin = $this->generatePIN(4, $gen);
                try {
                    $pin->save();
                } catch (\Illuminate\Database\QueryException $e) {
                    throw $e;
                }
            }
        }

        if ($request->plus_pin) {
            for ($i = 0; $i < $request->plus_pin; ++$i) {
                $gen = 1;
                $pin = new \App\Models\Pins\StartPin();
                $pin->screch = 0;
                $pin->user_id = 0;
                $pin->request_id = $pinRequest->id;
                $pin->request_program_id = $p;

                $date = Carbon::now()->addDays(2)->toDateTimeString();
                $pin->expired_at = $date;
                $pin->program = 5;

                if ($request->plus_pin_timeless) {
                    $pin->timeless = 1;
                }

                if ($request->plus_pin_kassa) {
                    $pin->kassa = 1;
                    $gen = 2;
                }
                $pin->pin = $this->generatePIN(5, $gen);

                try {
                    $pin->save();
                    break;
                } catch (\Illuminate\Database\QueryException $e) {
                    throw $e;
                }
            }
        }

        if ($request->main_pin) {
            for ($i = 0; $i < $request->main_pin; ++$i) {
                $gen = 1;
                $pin = new \App\Models\Pins\StartPin;
                $pin->user_id = 0;
                $pin->request_id = $pinRequest->id;
                $pin->request_program_id = $p;
                $date = Carbon::now()->addDays(2)->toDateTimeString();
                $pin->expired_at = $date;
                $pin->program = 3;
                $pin->screch = 0;

                if ($request->plus_pin_timeless) {
                    $pin->timeless = 1;
                }

                if ($request->plus_pin_kassa) {
                    $pin->kassa = 1;
                    $gen = 2;
                }
                $pin->pin = $this->generatePIN(3, $gen);

                try {
                    $pin->save();
                } catch (\Illuminate\Database\QueryException $e) {
                    throw $e;
                };
            }
        }

        if ($request->vip_pin) {
            for ($i = 0; $i <= $request->vip_pin; $i++) {
                $gen = 1;
                $pin = new \App\Models\Pins\StartPin();
                $pin->user_id = 0;
                $pin->screch = 0;
                $pin->request_id = $pinRequest->id;
                $pin->request_program_id = $p;
                $date = Carbon::now()->addDays(2)->toDateTimeString();
                $pin->expired_at = $date;
                $pin->program = 6;
                if ($request->plus_pin_timeless) {
                    $pin->timeless = 1;
                }

                if ($request->plus_pin_kassa) {
                    $pin->kassa = 1;
                    $gen = 2;
                }

                $pin->pin = $this->generatePIN(6, $gen);

                try {
                    $pin->save();
                } catch (\Illuminate\Database\QueryException $e) {
                    throw $e;
                }

            }
        }

        if ($request->fast_pin) {
            for ($i = 0; $i < $request->fast_pin; ++$i) {
                $gen = 1;
                $pin = new \App\Models\Pins\StartPin();
                $pin->user_id = 0;
                $pin->request_id = $pinRequest->id;
                $pin->request_program_id = $p;
                $date = Carbon::now()->addDays(2)->toDateTimeString();
                $pin->expired_at = $date;
                $pin->program = 7;
                $pin->screch = 0;


                if ($request->plus_pin_timeless) {
                    $pin->timeless = 1;
                }

                if ($request->plus_pin_kassa) {
                    $pin->kassa = 1;
                    $gen = 2;
                }
                $pin->pin = $this->generatePIN(7, $gen);
                try {
                    $pin->save();
                    break;
                } catch (\Illuminate\Database\QueryException $e) {
                    throw $e;
                }
            }
        }

        if ($request->bonus_pin) {
            for ($i = 0; $i < $request->bonus_pin; ++$i) {
                $gen = 1;
                $pin = new \App\Models\Pins\StartPin;
                $pin->user_id = 0;
                $pin->request_id = $pinRequest->id;
                $pin->request_program_id = $p;
                $date = Carbon::now()->addDays(2)->toDateTimeString();
                $pin->expired_at = $date;
                $pin->program = 8;
                $pin->screch = 0;

                if ($request->plus_pin_timeless) {
                    $pin->timeless = 1;
                }

                if ($request->plus_pin_kassa) {
                    $pin->kassa = 1;
                    $gen = 2;
                }
                $pin->pin = $this->generatePIN(8, $gen);

                try {
                    $pin->save();
                    break;
                } catch (\Illuminate\Database\QueryException $e) {
                    throw $e;
                }
            }
        }

        Session::flash('message', 'Заявка созданна');
        Session::flash('alert-class', 'alert-sucscess');

        return redirect()->route('pins.index');
    }

    public function add(Request $request)
    {

        // $data = $this->validate($request, [
        //     'title'=> 'required',
        //     'info'=>'required',
        //     'coordinates'=>'required',
        //     'is_online'=>'integer',
        // ]);

        $pname = '';

        $m = Config::get('matrix.get');
        $p = $p = Session::get('programm', 1);
        $connection = 'mysql_prod_'.$m[$p][0];

        switch ($m[$p][0]) {
            case 'start':
                $PinRequest = new StartPinRequest;
                $pname = 'Старт';
                break;
            case 'auto':
                $pname = 'Авто';
                $PinRequest = new AutoPinRequest;
                break;
            case 'main':
                $pname = 'Основная';
                $PinRequest = new MainPinRequest;
                break;
            case 'vip':
                $pname = 'Vip';
                $PinRequest = new VipPinRequest;
                break;
            case 'acummulative':
                $pname = 'Накопительная';
                $PinRequest = new AccumulativePinRequest;
                break;
            case 'acummulative_plus':
                $pname = 'Накопительная +';
                $PinRequest = new AccumulativePlusPinRequest;
                break;

            default:
                $PinRequest = new StartPinRequest;
                break;
        }

        $PinRequest->fill($request->all());
        $PinRequest->user_id = 0;

        if ($request->name) {
            $user = User::on($connection)->whereName($request->name)->first();

            if (!$user) {
                Session::flash('message', 'Юзера с ником '.$request->name.' в программе '.$pname.' не существует!');
                Session::flash('alert-class', 'alert-danger');

                return redirect()->route('partners.index', ['type' => $request->type]);
            }

            $PinRequest->user_id = $user->user_id;
            $PinRequest->uname = $request->name;

        }

        if ($request->hasFile('image')) {
            $imageName = time();
            $path = public_path('uploads/');
            $ext = $request->image->getClientOriginalExtension();
            $request->image->move($path, $imageName.'.'.$ext);
            // open and resize an image file
            $img = Image::make($path.$imageName.'.'.$ext)->resize(150, 150);
            $img->save($path.'thumb-'.$imageName.'.'.$ext, 80);
            $PinRequest->image = $imageName.'.'.$ext;
        }

        $PinRequest->pin = $this->generatePIN($p, 1);
        $PinRequest->status = 0;

        $PinRequest->expires_at = date('Y-m-d H:i:s');

        while (true) {
            try {
                $PinRequest->save();
                break;
            } catch (\Illuminate\Database\QueryException $e) {
                $PinRequest->pin = $this->generatePIN($p, 1);
                continue;
            }
        };

        if ($request->users) {
            foreach ($request->users as $key => $value) {

                switch ($m[$value['type']][0]) {
                    case 'start':
                        $PinRequest = new StartPinRequest;
                        $pname = 'Старт';
                        break;
                    case 'auto':
                        $pname = 'Авто';
                        $PinRequest = new AutoPinRequest;
                        break;
                    case 'main':
                        $pname = 'Основная';
                        $PinRequest = new MainPinRequest;
                        break;
                    case 'vip':
                        $pname = 'Vip';
                        $PinRequest = new VipPinRequest;
                        break;
                    case 'acummulative':
                        $pname = 'Накопительная';
                        $PinRequest = new AccumulativePinRequest;
                        break;
                    case 'acummulative_plus':
                        $pname = 'Накопительная +';
                        $PinRequest = new AccumulativePlusPinRequest;
                        break;

                    default:
                        $PinRequest = new StartPinRequest;
                        break;
                }

                $user = User::on($connection)->whereName($request->name)->first();

                if (!$user) {
                    Session::flash('message', 'Юзера с ником '.$request->name.' в программе '.$pname.' не существует!');
                    Session::flash('alert-class', 'alert-danger');

                    return redirect()->route('partners.index', ['type' => $request->type]);
                }

                $PinRequest->user_id = $user->user_id;
                $PinRequest->status = 0;
                $PinRequest->pin = $this->generatePIN($p, 1);
                $PinRequest->uname = $value['name'];
                $PinRequest->expires_at = date('Y-m-d H:i:s');

                while (true) {
                    try {
                        $PinRequest->save();
                        break;
                    } catch (\Illuminate\Database\QueryException $e) {
                        $PinRequest->pin = $this->generatePIN($p, 1);
                        continue;
                    }
                };

            }
        }
        Session::flash('message',
            'Пин код для юзера с ником '.$request->name.' в программе '.$pname.' успешно сгенерирован');
        Session::flash('alert-class', 'alert-success');
        return redirect()->route('partners.index');

    }

    public function pin_destroy(Request $request)
    {
        $model = \App\Models\Pins\StartPin::findOrFail($request->id);

        $model->delete();
        return new JsonResponse([
            'success' => true,
            'message' => 'Заявка удалена',
        ]);
    }

    public function destroy(Request $request)
    {
        $p = Session::get('programm', 1);
        $model = \App\Models\PinRequests\StartRequest::findOrFail($request->id);

        $pins = [
            'start' => \App\Models\Pins\StartPin::where('request_id', $model->id)->where('request_program_id',
                $p)->get(),
            'auto' => \App\Models\Pins\AutoPin::where('request_id', $model->id)->where('request_program_id', $p)->get(),
            'main' => \App\Models\Pins\MainPin::where('request_id', $model->id)->where('request_program_id', $p)->get(),
            'acummulative' => \App\Models\Pins\AcummulativePin::where('request_id',
                $model->id)->where('request_program_id', $p)->get(),
            'acummulative_plus' => \App\Models\Pins\Acummulative_plusPin::where('request_id',
                $model->id)->where('request_program_id', $p)->get(),
            'vip' => \App\Models\Pins\VipPin::where('request_id', $model->id)->where('request_program_id', $p)->get(),
            'fast' => \App\Models\Pins\FastPin::where('request_id', $model->id)->where('request_program_id', $p)->get(),
        ];

        foreach ($pins as $key => $pin) {
            foreach ($pin as $k => $p) {
                $p->delete();
            }
        }

        $model->delete();
        return new JsonResponse([
            'success' => true,
            'message' => 'Заявка удалена',
        ]);
    }

    private function generatePIN($p, $t)
    {
        // Available alpha caracters
        $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';

        // generate a pin based on 2 * 7 digits + a random character
        $pin = mt_rand(1000000, 9999999)
            .mt_rand(1000000, 9999999)
            .$characters[rand(0, strlen($characters) - 1)]
            .$characters[rand(0, strlen($characters) - 1)]
            .$characters[rand(0, strlen($characters) - 1)];

        // shuffle the result
        $pin = str_shuffle($pin);
        if ($p == 1) {
            if ($t == 1) {
                return 'N'.'STR'.$str = substr($pin, 0, 12);
            } else {
                return 'STR'.substr($pin, 0, 12).'KSAN';
            }
        } elseif ($p == 3) {
            if ($t == 1) {
                return 'N'.'MAIN'.$str = substr($pin, 0, 10);
            } elseif ($t == 2) {
                return 'MAIN'.substr($pin, 0, 8).'KSSAN';
            } else {
                return 'MAIN'.substr($pin, 0, 8).'WWDN';
            }
        } elseif ($p == 4) {
            if ($t == 1) {
                return 'F'.'BNK'.$str = substr($pin, 0, 10);
            } elseif ($t == 2) {
                return 'BNK'.substr($pin, 0, 8).'KSAF';
            } else {
                return 'BNK'.substr($pin, 0, 8).'WWF';
            }
        } elseif ($p == 5) {
            if ($t == 1) {
                return 'N'.'PLS'.$str = substr($pin, 0, 14);
            } elseif ($t == 2) {
                return 'PLS'.substr($pin, 0, 14).'KSAN';
            }
        } elseif ($p == 6) {
            if ($t == 1) {
                return 'N'.'VIP'.$str = substr($pin, 0, 12);
            } elseif ($t == 2) {
                return 'VIP'.substr($pin, 0, 9).'KSSAN';
            } else {
                return 'VIP'.substr($pin, 0, 9).'WWDN';
            }
        } elseif ($p == 7) {
            if ($t == 1) {
                return 'N'.'FAST'.$str = substr($pin, 0, 9);
            } elseif ($t == 2) {
                return 'VIP'.substr($pin, 0, 8).'KSSAN';
            } else {
                return 'N'.'FAST'.$str = substr($pin, 0, 8).'SSDF';
            }
        } elseif ($p == 8) {
            if ($t == 1) {
                return 'N'.'BON'.$str = substr($pin, 0, 12);
            } elseif ($t == 2) {
                return 'BON'.substr($pin, 0, 12).'KSSAN';
            } else {
                return 'BON'.$str = substr($pin, 0, 12).'WWDN';
            }
        }
    }

    public function update(Request $request)
    {

        $p = Session::get('programm', 1);

        $model = \App\Models\PinRequests\StartRequest::whereId($request->id)->first();
        $model->fill($request->all());
        $model->save();

        if ($request->start_pin) {
            for ($i = 0; $i < $request->start_pin; ++$i) {
                $pin = new \App\Models\Pins\StartPin;
                $pin->user_id = 0;
                $pin->request_id = $model->id;
                $pin->request_program_id = $p;
                $pin->pin = $this->generatePIN(1, 1);
                $date = Carbon::now()->addDays(2)->toDateTimeString();
                $pin->expired_at = $date;
                $pin->program = 1;
                $pin->screch = 0;

                while (true) {
                    try {
                        $pin->save();
                        break;
                    } catch (\Illuminate\Database\QueryException $e) {
                        $pin->pin = $this->generatePIN(1, 1);
                        continue;
                    }
                };
            }
        }

        if ($request->bank_pin) {
            for ($i = 0; $i < $request->bank_pin; ++$i) {

                $pin = new \App\Models\Pins\StartPin;
                $pin->user_id = 0;
                $pin->request_id = $model->id;
                $pin->request_program_id = $p;
                $pin->pin = $this->generatePIN(4, 1);
                $date = Carbon::now()->addDays(2)->toDateTimeString();
                $pin->expired_at = $date;
                $pin->program = 4;
                $pin->screch = 0;

                while (true) {
                    try {
                        $pin->save();
                        break;
                    } catch (\Illuminate\Database\QueryException $e) {
                        $pin->pin = $this->generatePIN(4, 1);
                        continue;
                    }
                };
            }
        }

        if ($request->main_pin) {
            for ($i = 0; $i < $request->main_pin; ++$i) {
                $pin = new \App\Models\Pins\StartPin;
                $pin->user_id = 0;
                $pin->request_id = $model->id;
                $pin->request_program_id = $p;
                $pin->pin = $this->generatePIN(3, 1);
                $date = Carbon::now()->addDays(2)->toDateTimeString();
                $pin->expired_at = $date;
                $pin->program = 3;
                $pin->screch = 0;

                while (true) {
                    try {
                        $pin->save();
                        break;
                    } catch (\Illuminate\Database\QueryException $e) {
                        $pin->pin = $this->generatePIN(3, 1);
                        continue;
                    }
                };
            }
        }

        if ($request->vip_pin) {
            for ($i = 0; $i < $request->vip_pin; ++$i) {
                $pin = new \App\Models\Pins\StartPin;
                $pin->user_id = 0;
                $pin->request_id = $model->id;
                $pin->request_program_id = $p;
                $pin->pin = $this->generatePIN(6, 1);
                $date = Carbon::now()->addDays(2)->toDateTimeString();
                $pin->expired_at = $date;
                $pin->program = 6;
                $pin->screch = 0;

                while (true) {
                    try {
                        $pin->save();
                        break;
                    } catch (\Illuminate\Database\QueryException $e) {
                        $pin->pin = $this->generatePIN(6, 1);
                        continue;
                    }
                };
            }
        }

        if ($request->fast_pin) {
            for ($i = 0; $i < $request->fast_pin; ++$i) {
                $pin = new \App\Models\Pins\StartPin;
                $pin->user_id = 0;
                $pin->request_id = $model->id;
                $pin->request_program_id = $p;
                $pin->pin = $this->generatePIN(7, 1);
                $date = Carbon::now()->addDays(2)->toDateTimeString();
                $pin->expired_at = $date;
                $pin->program = 7;
                $pin->screch = 0;

                while (true) {
                    try {
                        $pin->save();
                        break;
                    } catch (\Illuminate\Database\QueryException $e) {
                        $pin->pin = $this->generatePIN(7, 1);
                        continue;
                    }
                };
            }
        }

        if ($request->bonus_pin) {

            for ($i = 0; $i < $request->bonus_pin; ++$i) {

                $pin = new \App\Models\Pins\StartPin;
                $pin->user_id = 0;
                $pin->request_id = $model->id;
                $pin->request_program_id = $p;
                $pin->pin = $this->generatePIN(8, 1);
                $date = Carbon::now()->addDays(2)->toDateTimeString();
                $pin->expired_at = $date;
                $pin->program = 8;
                $pin->screch = 0;

                while (true) {
                    try {
                        $pin->save();
                        break;
                    } catch (\Illuminate\Database\QueryException $e) {
                        $pin->pin = $this->generatePIN(8, 1);
                        continue;
                    }
                };
            }

        }

        return redirect('/pins');
    }

    public function statusupdate(Request $request)
    {

        $p = Session::get('programm', 1);

        $model = \App\Models\PinRequests\StartRequest::whereId($request->id)->first();

        $model->status_id = $request->status;

        $model->bill = $request->bill;
        $model->iin = $request->iin;

        $model->cancel_reason = $request->cancel_reason;

        $model->save();

        if ($request->start_pin) {
            for ($i = 0; $i < $request->start_pin; ++$i) {
                $pin = new \App\Models\Pins\StartPin;
                $pin->user_id = 0;
                $pin->request_id = $model->id;
                $pin->request_program_id = $p;
                $pin->pin = $this->generatePIN(1, 1);
                $date = Carbon::now()->addDays(2)->toDateTimeString();
                $pin->expired_at = $date;
                $pin->program = 1;
                $pin->screch = 0;

                while (true) {
                    try {
                        $pin->save();
                        break;
                    } catch (\Illuminate\Database\QueryException $e) {
                        $pin->pin = $this->generatePIN(1, 1);
                        continue;
                    }
                };
            }
        }

        if ($request->bank_pin) {
            for ($i = 0; $i < $request->bank_pin; ++$i) {
                $pin = new \App\Models\Pins\StartPin;
                $pin->user_id = 0;
                $pin->request_id = $model->id;
                $pin->request_program_id = $p;
                $pin->pin = $this->generatePIN(4, 1);
                $date = Carbon::now()->addDays(2)->toDateTimeString();
                $pin->expired_at = $date;
                $pin->program = 4;
                $pin->screch = 0;

                while (true) {
                    try {
                        $pin->save();
                        break;
                    } catch (\Illuminate\Database\QueryException $e) {
                        $pin->pin = $this->generatePIN(4, 1);
                        continue;
                    }
                };
            }
        }

        if ($request->main_pin) {
            for ($i = 0; $i < $request->main_pin; ++$i) {
                $pin = new \App\Models\Pins\StartPin;
                $pin->user_id = 0;
                $pin->request_id = $model->id;
                $pin->request_program_id = $p;
                $pin->pin = $this->generatePIN(3, 1);
                $date = Carbon::now()->addDays(2)->toDateTimeString();
                $pin->expired_at = $date;
                $pin->program = 3;
                $pin->screch = 0;

                while (true) {
                    try {
                        $pin->save();
                        break;
                    } catch (\Illuminate\Database\QueryException $e) {
                        $pin->pin = $this->generatePIN(3, 1);
                        continue;
                    }
                };
            }
        }

        if ($request->vip_pin) {
            for ($i = 0; $i < $request->vip_pin; ++$i) {
                $pin = new \App\Models\Pins\StartPin;
                $pin->user_id = 0;
                $pin->request_id = $model->id;
                $pin->request_program_id = $p;
                $pin->pin = $this->generatePIN(6, 1);
                $date = Carbon::now()->addDays(2)->toDateTimeString();
                $pin->expired_at = $date;
                $pin->program = 6;
                $pin->screch = 0;

                while (true) {
                    try {
                        $pin->save();
                        break;
                    } catch (\Illuminate\Database\QueryException $e) {
                        $pin->pin = $this->generatePIN(6, 1);
                        continue;
                    }
                };
            }
        }

        if ($request->fast_pin) {
            for ($i = 0; $i < $request->fast_pin; ++$i) {
                $pin = new \App\Models\Pins\StartPin;
                $pin->user_id = 0;
                $pin->request_id = $model->id;
                $pin->request_program_id = $p;
                $pin->pin = $this->generatePIN(7, 1);
                $date = Carbon::now()->addDays(2)->toDateTimeString();
                $pin->expired_at = $date;
                $pin->program = 7;
                $pin->screch = 0;

                while (true) {
                    try {
                        $pin->save();
                        break;
                    } catch (\Illuminate\Database\QueryException $e) {
                        $pin->pin = $this->generatePIN(7, 1);
                        continue;
                    }
                };
            }
        }

        if ($request->bonus_pin) {
            for ($i = 0; $i < $request->bonus_pin; ++$i) {
                $pin = new \App\Models\Pins\StartPin;
                $pin->user_id = 0;
                $pin->request_id = $model->id;
                $pin->request_program_id = $p;
                $pin->pin = $this->generatePIN(8, 1);
                $date = Carbon::now()->addDays(2)->toDateTimeString();
                $pin->expired_at = $date;
                $pin->program = 8;
                $pin->screch = 0;

                while (true) {
                    try {
                        $pin->save();
                        break;
                    } catch (\Illuminate\Database\QueryException $e) {
                        $pin->pin = $this->generatePIN(8, 1);
                        continue;
                    }
                };
            }
        }

        return redirect('/admin/pins');
    }

}
