<?php

namespace App\Http\Controllers;

use App\Models\Pins\FastPin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FastPinController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Pins\FastPin  $fastPin
     * @return \Illuminate\Http\Response
     */
    public function show(FastPin $fastPin)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Pins\FastPin  $fastPin
     * @return \Illuminate\Http\Response
     */
    public function edit(FastPin $fastPin)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Pins\FastPin  $fastPin
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, FastPin $fastPin)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Pins\FastPin  $fastPin
     * @return \Illuminate\Http\Response
     */
    public function destroy(FastPin $fastPin)
    {
        //
    }
}
