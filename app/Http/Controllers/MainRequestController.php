<?php

namespace App\Http\Controllers;

use App\Models\PinRequests\MainRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MainRequestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\PinRequests\MainRequest  $mainRequest
     * @return \Illuminate\Http\Response
     */
    public function show(MainRequest $mainRequest)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\PinRequests\MainRequest  $mainRequest
     * @return \Illuminate\Http\Response
     */
    public function edit(MainRequest $mainRequest)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\PinRequests\MainRequest  $mainRequest
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MainRequest $mainRequest)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\PinRequests\MainRequest  $mainRequest
     * @return \Illuminate\Http\Response
     */
    public function destroy(MainRequest $mainRequest)
    {
        //
    }
}
