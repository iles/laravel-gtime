<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Grids\AdminsGrid;
use Illuminate\Http\JsonResponse;
use App\User;
use Hash;
use Session;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(AdminsGrid $sprInterface, Request $request)
    {
        return $sprInterface
                    ->create(['query' => User::query(), 'request' => $request])
                    ->renderOn('users.index'); // render the grid on the welcome view

    }  

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admins.create');
    }
        

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = new User;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->save();
        $user->assignRole($request->role);
        return redirect()->route('admins.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $model = User::where('id', $request->user)->first();
        return view('admins.show', ['model'=>$model]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $model = User::findOrFail($request->id);
        $model->fill($request->all());
        if($request->password){
            $model->password = Hash::make($request->password);
        }
        $model->save();
        Session::flash('message', 'Данные пользователи обновленны'); 
        Session::flash('alert-class', 'alert-sucscess'); 
        return redirect()->route('admins.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $model = User::findOrFail($request->user);
        $model->delete();
        return redirect()->route('admins.index');
        // return new JsonResponse([
        //         'success' => true,
        //         'message' => 'Заявка удалена'
        //     ]);
    }
}
