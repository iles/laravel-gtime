<?php

namespace App\Http\Controllers;

use App\Models\AutoPinRequest;
use App\Models\MainPinRequest;
use App\Models\StartPinRequest;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\VipPinRequest;


class ValidationController extends Controller
{
    public function index($type)
    {
        //
    }

    public function user_exists(Request $request){

        $connection = 'mysql_prod_'.$request->input('type');
        if(User::on($connection)->whereRaw('name = ?',array($request->input('username')))->exists()){
            return response()->json(['success' => true]);
        }
        return response()->json(['success' => false]);
    }

    public function pin_exists(Request $request){

        $connection = 'mysql_prod';


        switch ($request->type) {
            case 0:
                $t = StartPinRequest;
                break;
            case 1:
                $t = AutoPinRequest;
                break;
            case 3:
                $t = MainPinRequest;
                break;
            case 3:
                $t = VipPinRequest;
                break;
            default:
                break;
        }
        if($t == null){
            response()->json(['success' => false]);
        }
        if( $t::on($connection)->whereRaw('pin = ?',array($request->input('pin')))->exists()){
            return response()->json(['success' => true]);
        }
        return response()->json(['success' => false]);
    }
}
