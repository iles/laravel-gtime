<?php

namespace App\Http\Controllers;

use App\Grids\StartPinsGrid;
use App\Models\Bonus;
use App\Models\BonusRequest;
use App\Models\Cancellation;
use App\Models\User;
use Carbon\Carbon;
use Config;
use DB;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Session;

class PincodesController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(StartPinsGrid $brg, Request $request) {

		$query = \App\Models\Pins\StartPin::query()->whereNotNull('transitive');

		return $brg
			->create(['query' => $query, 'request' => $request])
			->renderOn('pincodes.index'); // render the grid on the welcome view
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function auto() {

		$pins = \App\Models\Pins\StartPin::where('program', 2)->get();
		return view('pins.auto', compact('pins'));
	}

	public function block($id) {
		$pin = \App\Models\Pins\StartPin::where('id', $id)->first();
		$pin->status_id = 2;
		$pin->save();
		return redirect('/pincodes');
	}

	public function unblock($id) {
		$pin = \App\Models\Pins\StartPin::where('id', $id)->first();
		$pin->status_id = 0;
		$pin->save();
		return redirect('/pincodes');
	}

	public function free_auto(Request $request) {
		$pin = \App\Models\Pins\StartPin::whereId($request->id)->first();
		$pin->user_id = 0;
		$pin->username = null;
		$pin->status_id = 0;
		$pin->expired_at =  Carbon::now()->addDays(2)->toDateTimeString();
		$pin->save();
		return 'ok';
	}

	public function create() {
		$m = Config::get('matrix.get');
		$progs = [];
		foreach ($m as $key => $value) {
			$progs[$key] = $value[1];
		}
		return view('bonus-request.form', compact('progs'));
	}

	public function create_admin() {
		$m = Config::get('matrix.get');
		$progs = [];
		foreach ($m as $key => $value) {
			$progs[$key] = $value[1];
		}
		return view('bonus-request.form_admin', compact('progs'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {

		$br = BonusRequest::create($request->all());
		Session::flash('message', 'Заявка созданна');
		Session::flash('alert-class', 'alert-sucscess');
		return redirect(route('bonusrequest.index'));
	}

	public function store_admin(Request $request) {
		$m = Config::get('matrix.get');
		$p = Session::get('programm', 1);

		$connection = 'mysql_prod_' . $m[$p][0];

		switch ($m[$p][0]) {
		case 'start':
			$model = new \App\Models\Bonuses\StartBonusRequestModel;
			break;
		case 'auto':
			$model = new \App\Models\Bonuses\AutoBonusRequestModel;
			break;
		case 'main':
			$model = new \App\Models\Bonuses\MainBonusRequestModel;
			break;
		case 'vip':
			$model = new \App\Models\Bonuses\VipBonusRequestModel;
			break;
		case 'acummulative':
			$model = new \App\Models\Bonuses\AccumulativeBonusRequestModel;
			break;
		case 'acummulative_plus':
			$model = new \App\Models\Bonuses\AccumulativePlusBonusRequestModel;
			break;
		case 'fast':
			$model = new \App\Models\Bonuses\FastBonusRequestModel;
			break;
		default:
			$model = new \App\Models\Bonuses\StartBonusRequestModel;
			break;
		}

		$model->fill($request->all());

		$user = User::on($connection)->whereName($request->uname)->first();
		if (!$user) {
			Session::flash('message', 'Пользователя с таким ником не существует!');
			Session::flash('alert-class', 'alert-error');
			return redirect('/admin/bonusrequest');
		} else {
			$model->uid = $user->user_id;
			$model->save();
			Session::flash('message', 'Заявка созданна');
			Session::flash('alert-class', 'alert-sucscess');
			return redirect('/admin/bonusrequest');
		}

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  \App\Models\BonusRequest  $bonusRequest
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {
		$model = BonusRequest::findOrFail($id);

		$m = Config::get('matrix.get');
		$progs = [];
		foreach ($m as $key => $value) {
			$progs[$key] = $value[1];
		}
		return view('bonus-request.form', compact('progs', 'model'));
	}

	public function cancel() {
		return view('pins.cancel');
	}

	public function pinscheck(Request $request) {
				$m = Config::get('matrix.get');
		$p = Session::get('programm', 1);

		$connection = 'mysql_prod_' . $m[$p][0];
		$c = Cancellation::where('pin', $request->pin)->where('status_id', 0)->first();
		$p = DB::connection($connection)->table('dle_matrix_users')->where('uname', $c->login)->first();

		if ($c) {
			return new JsonResponse([
				'c' => $c,
				'p' => $p,
			]);
		} else {
			return new JsonResponse([
				'error' => 'true',
			]);
		}
	}

	public function docancel(Request $request) {

		$m = Config::get('matrix.get');
		$p = Session::get('programm', 1);

		$connection = 'mysql_prod_' . $m[$p][0];

		$c = Cancellation::where('pin', $request->pin)->first();

		DB::connection($connection)->table('dle_users')->where('user_id', $c->sponsor_id)->decrement('ref_num');
		DB::connection($connection)->table('dle_matrix_users')->where('uid', $c->uid)->delete();
		DB::connection($connection)->table('dle_matrix')->where('matrix_id', $c->mx_id)->decrement('users_num');

		$pin = \App\Models\Pins\StartPin::where('pin', $request->pin)->first();

		$pin->status_id = 1;
		$pin->expired_at = $date = Carbon::now()->addDays(2)->toDateTimeString();
		$pin->save();

		$c->status_id = 1;
		$c->save();

		return redirect('/pincodes/cancel');

	}

	public function aprove($id, $s) {
		$m = Config::get('matrix.get');
		$p = Session::get('programm', 1);

		switch ($m[$p][0]) {
		case 'start':
			$model = \App\Models\Bonuses\StartBonusRequestModel::findOrFail($id);
			break;
		case 'auto':
			$model = \App\Models\Bonuses\AutoBonusRequestModel::findOrFail($id);
			break;
		case 'main':
			$model = \App\Models\Bonuses\MainBonusRequestModel::findOrFail($id);
			break;
		case 'vip':
			$model = \App\Models\Bonuses\VipBonusRequestModel::findOrFail($id);
			break;
		case 'acummulative':
			$model = \App\Models\Bonuses\AccumulativeBonusRequestModel::findOrFail($id);
			break;
		case 'acummulative_plus':
			$model = \App\Models\Bonuses\AccumulativePlusBonusRequestModel::findOrFail($id);
			break;
		case 'fast':
			$model = \App\Models\Bonuses\FastBonusRequestModel::findOrFail($id);
			break;
		default:
			$model = \App\Models\Bonuses\StartBonusRequestModel::findOrFail($id);
			break;
		}

		$model->status_id = $s;
		$model->save();

		Session::flash('message', 'Заявка переданна в бухгалтерию');
		Session::flash('alert-class', 'alert-sucscess');
		return redirect(route('bonusrequest.index'));
	}
	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  \App\Models\BonusRequest  $bonusRequest
	 * @return \Illuminate\Http\Response
	 */
	public function edit(BonusRequest $bonusRequest) {
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \App\Models\BonusRequest  $bonusRequest
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {
		dd($request);
		$model = BonusRequest::findOrFail($id);
		$model->fill($request->all());

		$model->save();
		Session::flash('message', 'Заявка оновлена');
		Session::flash('alert-class', 'alert-sucscess');
		return redirect(route('bonusrequest.index'));
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  \App\Models\BonusRequest  $bonusRequest
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(Request $request) {
		$model = BonusRequest::findOrFail($request->id);
		$model->delete();
		return new JsonResponse([
			'success' => true,
			'message' => 'Заявка удалена',
		]);
	}
}
