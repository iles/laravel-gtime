<?php

namespace App\Http\Controllers;

use App\Models\PinRequests\FastRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FastRequestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\PinRequests\FastRequest  $fastRequest
     * @return \Illuminate\Http\Response
     */
    public function show(FastRequest $fastRequest)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\PinRequests\FastRequest  $fastRequest
     * @return \Illuminate\Http\Response
     */
    public function edit(FastRequest $fastRequest)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\PinRequests\FastRequest  $fastRequest
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, FastRequest $fastRequest)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\PinRequests\FastRequest  $fastRequest
     * @return \Illuminate\Http\Response
     */
    public function destroy(FastRequest $fastRequest)
    {
        //
    }
}
