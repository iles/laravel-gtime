<?php

namespace App\Http\Controllers;

use App\Models\Pins\AcummulativePin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AcummulativePinController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Pins\AcummulativePin  $acummulativePin
     * @return \Illuminate\Http\Response
     */
    public function show(AcummulativePin $acummulativePin)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Pins\AcummulativePin  $acummulativePin
     * @return \Illuminate\Http\Response
     */
    public function edit(AcummulativePin $acummulativePin)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Pins\AcummulativePin  $acummulativePin
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AcummulativePin $acummulativePin)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Pins\AcummulativePin  $acummulativePin
     * @return \Illuminate\Http\Response
     */
    public function destroy(AcummulativePin $acummulativePin)
    {
        //
    }
}
