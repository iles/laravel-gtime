<?php

namespace App\Http\Controllers;

use App\Models\Pins\AutoPin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AutoPinController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Pins\AutoPin  $autoPin
     * @return \Illuminate\Http\Response
     */
    public function show(AutoPin $autoPin)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Pins\AutoPin  $autoPin
     * @return \Illuminate\Http\Response
     */
    public function edit(AutoPin $autoPin)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Pins\AutoPin  $autoPin
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AutoPin $autoPin)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Pins\AutoPin  $autoPin
     * @return \Illuminate\Http\Response
     */
    public function destroy(AutoPin $autoPin)
    {
        //
    }
}
