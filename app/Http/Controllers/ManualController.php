<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Programs\ProgramController;
use App\Models\User;
use App\Models\Finished;
use Config;
use DB;
use Session;

class ManualController extends ProgramController 
{
    public function split(Request $request){
    	$this->start_split( $request->id );
        return redirect()->back();
    }

    public function start_split($matrix_id) {

    	$m = Config::get('matrix.get');
		$p = 1;

		$connection = 'mysql_prod_' . $m[$p][0];

		$row = DB::connection($connection)->table('dle_matrix')->select('*')->where('matrix_id', $matrix_id)->first();
		
		$last_users = DB::connection($connection)->table('dle_matrix_users')->where('mx_id', $matrix_id)->where('level', 4)->get()->toArray();
		$leader = $row->leader; # Лидер матрицы

		$leader_row = DB::connection($connection)->table('dle_users')->select('*')->where('name', $leader)->first();
		
		DB::connection($connection)->table('dle_matrix_users')->where('uid', $leader_row->user_id)->delete();

			if ($row->stage == 0 && $leader) {

				$leader = $row->leader;

				$leader_row = DB::connection($connection)->table('dle_users')->select('*')->where('name', $leader)->first();

				$datee = date("d.m.Y H:i:s");

				$perekid = DB::connection($connection)->table('dle_users')->select()->where('name', $leader)->first();

				$finished = Finished::create([
					'program_id' => 1,
					'user_id' => $leader_row->user_id,
					'login' => $leader_row->name,
					'fullname' => $leader_row->familiya,
					'birthdate' => $leader_row->otchestvo,
					'iin' => $leader_row->strana,
					'registred_pin' => $leader_row->screch,
					'country' => $leader_row->imya,
					'info' => $leader_row->signature,
					'tovar' => $leader_row->tovar,
					'phone' => $leader_row->pol,
					'last' => json_encode($last_users, JSON_UNESCAPED_UNICODE),
				]);

				DB::connection($connection)->table('dle_drop_users_start')->insert(
					[
						'screch' => $perekid->screch,
						'email' => $perekid->email,
						'user_id' => $perekid->user_id,
						'name' => $perekid->name,
						'dateout' => $datee,
						'balance' => 25000,
						'familiya' => $perekid->familiya,
						'imya' => $perekid->imya,
						'otchestvo' => $perekid->otchestvo,
						'pol' => $perekid->pol,
						'strana' => $perekid->strana,
						'gorod' => $perekid->gorod,
						'INFO' => $perekid->info,
						'TOVAR' => $perekid->signature,
					]
				);

			}



			$m_users = DB::connection($connection)
				->table('dle_matrix_users')
				->select()
				->where('mx_id', $matrix_id)->get();

			$users = [];

			foreach ($m_users as $key => $user) {
				$users[$user->uid]['uid'] = $user->uid;
				$users[$user->uid]['level'] = $user->level;
				$users[$user->uid]['position'] = $user->position;
				$users[$user->uid]['stars'] = $this->getStars($user->uid);
				$users[$user->uid]['name'] = $user->uname;
			}

			usort($users, array($this, "starsComparator"));

			$leaders = array_slice($users, 0, 2);
			unset($users[0]);
			unset($users[1]);

			// Position counter
			$position_id = 0;

			$other_users = array();
			// $_3LevelUsers = array();
			$matrix_id_array = [];

			foreach ($leaders as $key => $leader) {
				$new_matrix_id = $this->CREATEMatrix($leader['name'], 7, 0);
				DB::connection($connection)->table('dle_matrix_users')
					->where('uid', $leader['uid'])
					->update([
						'mx_id' => $new_matrix_id,
						'level' => 1,
						'position' => 1,
					]);

				$this->update_user(array('matrix_id' => $new_matrix_id), $leader['uid']);

				$matrix_id_array[] = $new_matrix_id; // записываем ID новых матриц в массив
			}

			$keys['leaders'] = $leaders;
			$keys['users'] = $users;

			$j = 0;
			$k = 0;
			$i = 0;
			$i2 = 0;
			foreach ($users as $key => $user) {
				$j++;
				if (in_array($j, array(1, 3))) {
					$p = $j;
					$l = 2;
					$m = $matrix_id_array[0];
				} elseif (in_array($j, array(2, 4))) {
					$k++;
					$p = $k;
					$l = 2;
					$m = $matrix_id_array[1];

				} elseif (in_array($j, array(5, 7, 9, 11))) {
					$i++;
					$p = $i;
					$l = 3;
					$m = $matrix_id_array[0];

				} elseif (in_array($j, array(6, 8, 10, 12))) {
					$i2++;
					$p = $i2;
					$l = 3;
					$m = $matrix_id_array[1];
				}
				DB::connection($connection)->table('dle_matrix_users')
					->where('uid', $user['uid'])
					->update([
						'mx_id' => $m,
						'level' => $l,
						'position' => $p,
					]);				

				DB::connection($connection)->table('dle_users')
					->where('user_id', $user['uid'])
					->update([
						'matrix_id' => $m,
					]);
			}
	}

	public function auto_split($matrix_id){
		
		$m = Config::get('matrix.get');
		$p = 2;
		$connection = 'mysql_prod_' . $m[$p][0];

		$row = DB::connection($connection)->table('dle_matrix')->select('*')->where('matrix_id', $matrix_id)->first();

		$last_users = DB::connection($connection)->table('dle_matrix_users')->where('mx_id',$matrix_id)->where('level', 3)->get()->toArray();
		$leader     = $row->leader; # Лидер матрицы
		$leader_row = DB::connection($connection)->table('dle_users')->select('*')->where('name', $leader)->first();
		
		DB::connection($connection)->table('dle_matrix_users')->where('uid', $leader_row->user_id )->delete();

		if ($row->stage ==0 && $leader){                    
			
			$leader     = $row->leader;
            $leader_row = DB::connection($connection)->table('dle_users')->select('*')->where('name', $leader)->first();
            $datee= date("d.m.Y H:i:s");
			$perekid = DB::connection($connection)->table('dle_users')->select('*')->where('name', $leader)->first();

                
            DB::connection($connection)->table('dle_drop_users')->insert(
                [
                    'screch'=>$perekid->screch,
                    'email'=>$perekid->email,
                    'name'=>$perekid->name,
                    'user_id'=>$perekid->user_id,
                    'dateout'=>date("d.m.Y H:i:s"),
                    'balance' => 15000,
                    'familiya'=>$perekid->familiya,
                    'imya'=>$perekid->imya,
                    'otchestvo'=>$perekid->otchestvo,
                    'pol'=>$perekid->pol,
                    'strana'=>$perekid->strana,
                    'gorod'=>$perekid->gorod,
                    'setr'=>0,
                     ]
                );
            }               
                                

                  
            $m_users = DB::connection($connection)
            ->table('dle_matrix_users')
            ->select()
            ->where('mx_id', $matrix_id)->get();
            

            $matrix_users = DB::connection($connection)->select('SELECT
                            dle_matrix.leader, 
                            dle_matrix.users_num,
                            dle_matrix.stage,
                            dle_matrix_users.uid,
                            dle_users.ref_num, by_refer, user_id, name

                    FROM `dle_matrix`, 
                         `dle_matrix_users`,
                         `dle_users`
                         
                    WHERE dle_matrix.matrix_id= ? AND dle_users.user_id = dle_matrix_users.uid AND  dle_users.matrix_id= ?
                    AND  dle_matrix.leader != dle_matrix_users.uname
                    ORDER BY dle_users.ref_num DESC, 
                    dle_matrix_users.level ASC, 
                    dle_matrix_users.position ASC',  [$matrix_id, $matrix_id] );


            $position_id = 0;
            $_2LevelUsers = array();

            foreach( $matrix_users as $data ) 
            {
                $position_id++;

                if( in_array($position_id, array(1, 2)) ) { # Если лидеры новых матриц
                                                        
                        // создаём новую матрицу////////////////////////////////////////////////

                        $new_matrix_id = $this->CREATEMatrix($data->name, 3, $row->stage);

                        
                        if ($row->stage == 1) {

                            DB::connection($connection)->table('dle_users')
                                ->where('name', $data->name)
                                ->update([
                                    'by_refer' => 0,
                            ]);

                        }
                        
                        
                        
                                                                    
                        // добавляем лидера в матрицу (его же)
                            DB::connection($connection)->table('dle_matrix_users')
                                ->where('uid', $data->user_id)
                                ->update([
                                    'mx_id' => $new_matrix_id,
                                    'level' => 1,
                                    'position' => 1,
                            ]);

                    
                        // Обновляем id матрицы у пользователя
                        $this->update_user( array( 'matrix_id' => $new_matrix_id),  $data->user_id  );
                    
                        $matrix_id_array[] = $new_matrix_id; // записываем ID новых матриц в массив                 
                    
                } else { # Остальные 12 пользователя, что станут на второй и третий уровень.                    
                    $_2LevelUsers[] = array( $data->user_id, $data->name );
                }
            }
            $j = 0;
            $k = 0;
            $l = 0;
            foreach( $_2LevelUsers as $user_info_array) { 
                $j++;
                
                list($current_uid, $current_uname) = $user_info_array;
                
                if(! intval($current_uid) ) {
                    $this->createLog('NotCID', "NOT CURRENT id ($j step)");
                    break;
                }
                
                if(! trim( $current_uname ) ) {
                    $this->createLog('NotCName', "NOT CURRENT name ($j step)");
                    break;
                }
                
                
                if( in_array( $j, array( 1, 2) )  ) {
                    if( $matrix_id_array[0] ) { # Если сущ. id матрицы


                        DB::connection($connection)->table('dle_matrix_users')
                            ->where('uid', $current_uid)
                            ->update([
                                'mx_id' => $matrix_id_array[0],
                                'level' => 2,
                                'position' => $j,
                        ]);

                        //$this->createLog('UPDATE_MATRIX_USER', "UPDATE dle_matrix_users SET mx_id='" . $matrix_id_array[0] . "', level='2', position='" . $j . "' WHERE uid='$current_uid'");
                        $this->update_user( array( 'matrix_id' =>  $matrix_id_array[0] ),  $current_uid);
                    } else {
                        $this->createLog("NotCMXID1", "NOT CURRENT mxid[0] ($j step)");
                        break;
                    }
                } elseif( in_array( $j, array( 3, 4) ) ) {
                    $k++;
                    if( $matrix_id_array[1] ) { # Если сущ. id матрицы
 
                        DB::connection($connection)->table('dle_matrix_users')
                            ->where('uid', $current_uid)
                            ->update([
                                'mx_id' => $matrix_id_array[1],
                                'level' => 2,
                                'position' => $k,
                        ]);
                        //$this->createLog('UPDATE_MATRIX_USER', "UPDATE dle_matrix_users SET mx_id='" . $matrix_id_array[1] . "', level='2', position='" . $k . "' WHERE uid='$current_uid'");
                        $this->update_user( array( 'matrix_id' => $matrix_id_array[1] ),  $current_uid);
                    } else {
                        $this->createLog("NotCMXID2", "NOT CURRENT mxid[1] ($j step)");
                        break;
                    }
                }

                elseif( $j >= 5 && $j <= 8 )
                {
                    $l++;

                    if( $matrix_id_array[0] ) { # Если сущ. id матрицы
                        DB::connection($connection)->table('dle_matrix_users')
                            ->where('uid', $current_uid)
                            ->update([
                                'mx_id' => $matrix_id_array[0],
                                'level' => 3,
                                'position' => $l,
                        ]);
                        //$this->createLog('UPDATE_MATRIX_USER', "UPDATE dle_matrix_users SET mx_id='" . $matrix_id_array[0] . "', level='4', position='" . $l . "' WHERE uid='$current_uid'");
                        $this->update_user( array( 'matrix_id' => $matrix_id_array[0]),  $current_uid);
                    } else {
                        $this->createLog("NotCMXID12", "NOT CURRENT mxid[0] ($l step)");
                        break;
                    }
                }                
            }



            $leader     = $row->leader;

            $leader_row = DB::connection($connection)->table('dle_users')->select('*')->where('name', $leader)->first();
            
            $datee= date("d.m.Y H:i:s");

            $perekid = DB::connection($connection)->table('dle_users')->select()->where('name', $leader)->first();
                    

                

            DB::connection($connection)->table('dle_drop_users')->insert(
                [
                    'screch'=>$perekid->screch,
                    'email'=>$perekid->email,
                    'name'=>$perekid->name,
                    'user_id'=>$perekid->user_id,
                    'dateout'=>date("d.m.Y H:i:s"),
                    'balance' => 15000,
                    'familiya'=>$perekid->familiya,
                    'imya'=>$perekid->imya,
                    'otchestvo'=>$perekid->otchestvo,
                    'pol'=>$perekid->pol,
                    'strana'=>$perekid->strana,
                    'gorod'=>$perekid->gorod,
                    'setr'=>0,
                     ]
                );
          
                DB::connection($connection)->table('dle_matrix_users')->where('uid', $leader_row->user_id )->delete();
                //DB::connection($connection)->table('dle_users')->where('user_id', $leader_row->user_id )->delete();

        $fm = DB::connection($connection)->table('dle_matrix_users')
        ->whereRaw('mx_id = ?',array($matrix_id_array[0]))
        ->orderBy('level', 'asc')
        ->leftJoin('dle_users', 'dle_matrix_users.uid', '=', 'dle_users.user_id')
        ->orderBy('position', 'ASC')
        ->get();
        $split_first_matrix = [];

        foreach ($fm as $key => $value) {
            $split_first_matrix[$value->level][] = [
                'uid' => $value->user_id, 
                'uname' => $value->uname, 
                'stars' => $value->ref_num,
                'by_refer' => $this->getName($value->by_refer)
            ];
        }

        foreach ($split_first_matrix as $key => $value) {
            sort($value);
        }        

        $sm = DB::connection($connection)->table('dle_matrix_users')
        ->whereRaw('mx_id = ?',array($matrix_id_array[1]))
        ->orderBy('level', 'asc')
        ->leftJoin('dle_users', 'dle_matrix_users.uid', '=', 'dle_users.user_id')
        ->orderBy('position', 'ASC')
        ->get();
        $split_second_matrix = [];

        foreach ($sm as $key => $value) {
            $split_second_matrix[$value->level][] = [
                'uid' => $value->user_id, 
                'uname' => $value->uname, 
                'stars' => $value->ref_num,
                'by_refer' => $this->getName($value->by_refer)
            ];
        }

        foreach ($split_second_matrix as $key => $value) {
            sort($value);
        }


            $split->first_matrix =   json_encode($split_first_matrix ,true);
            $split->first_matrix_id =   $matrix_id_array[0];
            $split->second_matrix =  json_encode($split_second_matrix, true);
            $split->second_matrix_id = $matrix_id_array[1];
            $split->save(); 


                $finished = Finished::create([
                    'program_id' => 2,
                    'user_id' => $leader_row->user_id,
                    'login' => $leader_row->name,
                    'fullname'  => $leader_row->familiya,
                    'birthdate' => $leader_row->otchestvo,
                    'iin' => $leader_row->strana,
                    'registred_pin' => $leader_row->screch,
                    'country' => $leader_row->imya,
                    'info' => $leader_row->signature,
                    'tovar' => $leader_row->tovar,
                    'phone' => $leader_row->pol,
                    'last' => json_encode($last_users, JSON_UNESCAPED_UNICODE),
                ]);



        } 

    }





	}
}
