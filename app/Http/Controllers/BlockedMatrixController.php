<?php

namespace App\Http\Controllers;

use App\Models\BlockedMatrix;
use Illuminate\Http\Request;

class BlockedMatrixController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\BlockedMatrix  $blockedMatrix
     * @return \Illuminate\Http\Response
     */
    public function show(BlockedMatrix $blockedMatrix)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\BlockedMatrix  $blockedMatrix
     * @return \Illuminate\Http\Response
     */
    public function edit(BlockedMatrix $blockedMatrix)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\BlockedMatrix  $blockedMatrix
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, BlockedMatrix $blockedMatrix)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\BlockedMatrix  $blockedMatrix
     * @return \Illuminate\Http\Response
     */
    public function destroy(BlockedMatrix $blockedMatrix)
    {
        //
    }
}
