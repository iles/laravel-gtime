<?php
namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\Models\Partner;
use Config;
use Session;
use Auth;
use DateTime;
use DateTimeZone;

class PartnerLoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */
    use AuthenticatesUsers;
    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    // protected $redirectTo = '/home';
       // public function __construct()
       // {
       //     $this->middleware('guest:partner')->except('logout');
       // }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     protected $redirectTo = '/partner/ladder';


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }


    public function login()
    {
		return view('front.login');
    }    

    public function reset()
    {
    return view('front.reset');
    }

    public function username()
  	{
  	    return 'name';
  	}


    public function loginPartner(Request $request)
    {
      // Validate the form data
      $this->validate($request, [
        'username'   => 'required',
        'password' => 'required|min:6'
      ]);


      // Attempt to log the user in
      if (Auth::guard('partner')->attempt(['name' => $request->username, 'password' => $request->password], $request->remember)) {
        $m = Config::get('matrix.get');
        $p = Session::get('programm', 1);
        $type = $m[$p][0];        
        $connection = 'mysql_prod_'.$type;

        $user = Partner::on($connection)->where('user_id', Auth::user()->user_id)->first();

        $date = new DateTime(null, new DateTimeZone('Asia/Almaty'));
        
        $user->lastdate = $date->getTimestamp();
        $user->save();

        return redirect()->route('ladder');
      }
      // if unsuccessful, then redirect back to the login with the form data
   	  // $user = Partner::where('name', $request->username)->first();
	  // Auth::login($user, true);
      return redirect()->back()->withInput($request->only('email', 'remember'));
    }

    public function logout()
    {
        Auth::guard('partner')->logout();
        return redirect()->route('login');
    }
}