<?php
namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\Models\Partner;
use Auth;

class AdminLoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */
    use AuthenticatesUsers;
    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    // protected $redirectTo = '/home';
       // public function __construct()
       // {
       //     $this->middleware('guest:partner')->except('logout');
       // }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     protected $redirectTo = '/admin/login';


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }


    public function login()
    {
		return view('admin.login');
    }

    public function username()
  	{
  	    return 'name';
  	}


    public function loginAdmin(Request $request)
    {
      // Validate the form data
      $this->validate($request, [
        'email'   => 'required|email',
        'password' => 'required|min:6'
      ]);


      // Attempt to log the user in
      if (Auth::guard('web')->attempt(['email' => $request->email, 'password' => $request->password], $request->remember)) {
        
        $auth = Auth::guard('web')->user()->roles()->first()->name;

        if($auth == 'warehouse'){
          return redirect()->route('warehouse.index');          
        } elseif ($auth == 'warehouse_admin') {
          return redirect()->route('warehouse.admin_index');          
        } elseif ($auth == 'service') {
          return redirect()->route('swaps.index');          
        } elseif ($auth == 'bookkeeping') {
          return redirect()->route('bonusrequests.index');
        } elseif ($auth == 'kassa') {
          return redirect()->route('kassa.index');
        } elseif ($auth == 'security') {
          return redirect()->route('security.index');
        } 

        return redirect()->route('pins.index');  
      }
      // if unsuccessful, then redirect back to the login with the form data
   	  // $user = Partner::where('name', $request->username)->first();
	    // Auth::login($user, true);
      return redirect()->back()->withInput($request->only('email', 'remember'));
    }

    public function logout()
    {
      Auth::guard('web')->logout();
      return redirect()->route('admin.login');
    }
}