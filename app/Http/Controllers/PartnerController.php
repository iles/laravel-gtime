<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;



use App\Models\User;
use App\Models\Pin;
use App\Models\News;
use App\Models\NonResident;
use App\Models\StartPinRequest;
use App\Models\AutoPinRequest;
use App\Models\Partner;
use App\Models\MainPinRequest;
use App\Models\BlockedMatrix;
use App\Models\VipPinRequest;
use App\Models\AccumulativePinRequest;
use App\Models\AccumulativePlusPinRequest;
use App\Grids\StartRequestUsersNewPinGrid;
use App\Grids\StartRequestUsersNewPinGridInterface;
use Illuminate\Support\Facades\Session;
use App\Models\Finished;
use Illuminate\Http\JsonResponse;
use Image; 
use Config;
use Carbon\Carbon;
use DB;
use Auth;

class PartnerController extends Controller
{

    public function index(StartRequestUsersNewPinGridInterface $sprGrid, Request $request)
    {   
        $m = Config::get('matrix.get');
        $p = Session::get('programm', 1);

        switch ($m[$p][0]) {
            case 'start':
                $query = StartPinRequest::query()->orderBy('id', 'DESC');
                $name = 'Старт';
                break;
            case 'auto':
                $name = 'Авто';
                $query = AutoPinRequest::query()->orderBy('id', 'DESC');
                break;           
            case 'main':
                $name = 'Основная';
                $query = MainPinRequest::query()->orderBy('id', 'DESC');
                break;            
            case 'vip':
                $name = 'Vip';
                $query = VipPinRequest::query()->orderBy('id', 'DESC');
                break;            
            case 'acummulative':
                $name = 'Накопительная';
                $query = AccumulativePinRequest::query()->orderBy('id', 'DESC');
                break;            
            case 'acummulative_plus':
                $name = 'Накопительная +';
                $query = AccumulativePlusPinRequest::query()->orderBy('id', 'DESC');
                break;            
            case 'fast':
                $name = 'fast';
                $query = FastPinRequest::query()->orderBy('id', 'DESC');
                break;
            
            default:
                $query = StartPinRequest::query()->orderBy('id', 'DESC');
                break;
        }


            return $sprGrid
                    ->create(['query' => $query, 'request' => $request])
                    ->renderOn('partners.index', ['prog_name'=>$name]); // render the grid on the welcome view
    
    }      

    public function create()
    {
        return view('partners.create');
    }    

    public function show(Request $request)
    {
        $m = Config::get('matrix.get');
        $p = Session::get('programm', 1);
 
        switch ($m[$p][0]) {
            case 'start':
                $model = StartPinRequest::where('id', $request->zayavki)->first();
                $name = 'Старт';
                break;
            case 'auto':
                $name = 'Авто';
                $model = AutoPinRequest::where('id', $request->zayavki)->first();
                break;           
            case 'main':
                $name = 'Основная'
;                $model = MainPinRequest::where('id', $request->zayavki)->first();
                break;            
            case 'vip':
                $name = 'Vip';
                $model = VipPinRequest::where('id', $request->zayavki)->first();
                break;            
            case 'acummulative':
                $name = 'Накопительная';
                $model = AccumulativePinRequest::where('id', $request->zayavki)->first();
                break;            
            case 'acummulative_plus':
                $name = 'Накопительная +';
                $model = AccumulativePlusPinRequest::where('id', $request->zayavki)->first();
                break;            
            case 'fast':
                $name = 'Накопительная +';
                $model = FastPinRequest::where('id', $request->zayavki)->first();
                break;
            
            default:
                $model = StartPinRequest::where('id', $request->zayavki)->first();
                break;
        }

        return view('partners.show', ['model'=>$model]);
    }


    public function add(Request $request)
    {

        $pname = '';
        
        $m = Config::get('matrix.get');
        $p =   $p = Session::get('programm', 1);
        $connection = 'mysql_prod_'.$m[$p][0];
        
        switch ($m[$p][0]) {
            case 'start':
                $PinRequest = new StartPinRequest;
                $pname = 'Старт';
                break;
            case 'auto':
                $pname = 'Авто';
                $PinRequest = new AutoPinRequest;
                break;           
            case 'main':
                $pname = 'Основная';
                $PinRequest = new MainPinRequest;
                break;            
            case 'vip':
                $pname = 'Vip';
                $PinRequest = new VipPinRequest;
                break;            
            case 'acummulative':
                $pname = 'Накопительная';
                $PinRequest = new AccumulativePinRequest;
                break;            
            case 'acummulative_plus':
                $pname = 'Накопительная +';
                $PinRequest = new AccumulativePlusPinRequest;
                break;            
            case 'fast':
                $pname = 'Накопительная +';
                $PinRequest = new FastPinRequest;
                break;
            
            default:
                $PinRequest = new StartPinRequest;
                break;
        }


        $PinRequest->fill($request->all());
        $PinRequest->user_id = 0;

        if($request->name){
            $user = User::on($connection)->whereName($request->name)->first();

            if(!$user){
                Session::flash('message', 'Юзера с ником '.$request->name.' в программе '.$pname.' не существует!'); 
                Session::flash('alert-class', 'alert-danger'); 

                return redirect()->route('partners.index', ['type' => $request->type]);
            }

            $PinRequest->user_id = $user->user_id;
            $PinRequest->uname = $request->name;

        }          

        if ($request->hasFile('image')) {
            $imageName = time();
            $path = public_path('uploads/');
            $ext = $request->image->getClientOriginalExtension();
            $request->image->move( $path, $imageName.'.'.$ext);
            // open and resize an image file
            $img = Image::make( $path.$imageName.'.'.$ext )->resize(150, 150);
            $img->save( $path.'thumb-'.$imageName.'.'.$ext, 80);
            $PinRequest->image = $imageName.'.'.$ext;            
        }




        $PinRequest->pin = $this->generatePIN();    
        $PinRequest->status = 0;    

        $PinRequest->expires_at = date('Y-m-d H:i:s');



        while (true) {
            try {
                $PinRequest->save(); 
                break;
            } catch (\Illuminate\Database\QueryException $e) {
                $PinRequest->pin = $this->generatePIN();
                continue;
            }
        };



        
    
    if($request->users){
        foreach( $request->users as $key => $value){
    
        switch ($m[$value['type']][0]) {
            case 'start':
                $PinRequest = new StartPinRequest;
                $pname = 'Старт';
                break;
            case 'auto':
                $pname = 'Авто';
                $PinRequest = new AutoPinRequest;
                break;           
            case 'main':
                $pname = 'Основная';
                $PinRequest = new MainPinRequest;
                break;            
            case 'vip':
                $pname = 'Vip';
                $PinRequest = new VipPinRequest;
                break;            
            case 'acummulative':
                $pname = 'Накопительная';
                $PinRequest = new AccumulativePinRequest;
                break;            
            case 'acummulative_plus':
                $pname = 'Накопительная +';
                $PinRequest = new AccumulativePlusPinRequest;
                break;            
            case 'fast':
                $pname = 'fast';
                $PinRequest = new FastPinRequest;
                break;
            
            default:
                $PinRequest = new StartPinRequest;
                break;
        }

    
        $user = User::on($connection)->whereName($request->name)->first();

        if(!$user){
            Session::flash('message', 'Юзера с ником '.$request->name.' в программе '.$pname.' не существует!'); 
            Session::flash('alert-class', 'alert-danger'); 

            return redirect()->route('partners.index', ['type' => $request->type]);
        }

        
        $PinRequest->user_id = $user->user_id;
        $PinRequest->status = 0;
        $PinRequest->pin = $this->generatePIN();
        $PinRequest->uname = $value['name'];
        $PinRequest->expires_at = date('Y-m-d H:i:s');



        while (true) {
            try {
                $PinRequest->save(); 
                break;
            } catch (\Illuminate\Database\QueryException $e) {
                $PinRequest->pin = $this->generatePIN();
                continue;
            }
        };


        }
    }
        Session::flash('message', 'Пин код для юзера с ником '.$request->name.' в программе '.$pname.' успешно сгенерирован'); 
        Session::flash('alert-class', 'alert-success'); 
        return redirect()->route('partners.index');



    }


    public function destroy(Request $request){
        $m = Config::get('matrix.get');
        $p = Session::get('programm', 1);       
        switch ($m[$p][0]) {
            case 'start':
                StartPinRequest::findOrFail($request->id)->delete();
                break;
            case 'auto':
                AutoPinRequest::findOrFail($request->id)->delete();
                break;           
            case 'main':
                MainPinRequest::findOrFail($request->id)->delete();
                break;            
            case 'vip':
                VipPinRequest::findOrFail($request->id)->delete();
                break;            
            case 'acummulative':
                AccumulativePinRequest::findOrFail($request->id)->delete();
                break;            
            case 'acummulative_plus':
                AccumulativePlusPinRequest::findOrFail($request->id)->delete();
                break;            
            case 'fast':
                FastPinRequest::findOrFail($request->id)->delete();
                break;
            
            default:
                StartPinRequest::findOrFail($request->id)->delete();
                break;
        }

        return new JsonResponse([
                'success' => true,
                'message' => 'Спонсор удален'
            ]);
    }

    private function generatePIN(){
        // Available alpha caracters
        $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';

        // generate a pin based on 2 * 7 digits + a random character
        $pin = mt_rand(1000000, 9999999)
            . mt_rand(1000000, 9999999)
            . $characters[rand(0, strlen($characters) - 1)];

        // shuffle the result
        return str_shuffle($pin);
    }

    public function statusupdate(Request $request){

        $m = Config::get('matrix.get');
        $p =   $p = Session::get('programm', 1);

        switch ($m[$p][0]) {
            case 'start':
                $model = StartPinRequest::where('id', $request->id)->first();
                $name = 'Старт';
                break;
            case 'auto':
                $name = 'Авто';
                $model = AutoPinRequest::where('id', $request->id)->first();
                break;           
            case 'main':
                $name = 'Основная';
                $model = MainPinRequest::where('id', $request->id)->first();
                break;            
            case 'vip':
                $name = 'Vip';
                $model = VipPinRequest::where('id', $request->id)->first();
                break;            
            case 'acummulative':
                $name = 'Накопительная';
                $model = AccumulativePinRequest::where('id', $request->id)->first();
                break;            
            case 'acummulative_plus':
                $name = 'Накопительная +';
                $model = AccumulativePlusPinRequest::where('id', $request->id)->first();
                break;            
            case 'fast':
                $name = 'fast';
                $model = FastPinRequest::where('id', $request->id)->first();
                break;
            
            default:
                $model = StartPinRequest::where('id', $request->id)->first();
                break;
        }
         
        $model->update($request->all());

        if($request->status == 1){
            $this->addPartner($model);
        }
        return redirect('/partners');
    }

    public function addPartner($model) {  

        $m = Config::get('matrix.get');
        $p =  Session::get('programm', 1);
		
        $connection = 'mysql_prod_'.$m[$p][0];
 
        $data = json_decode($model->json, true);
        if( isset($data['sponsor']) ){
            $sponsor = Partner::on($connection)->where('name', $data['sponsor'] )->first();    
        } else {
            $sponsor = Partner::on($connection)->where('user_id', $model->user_id )->first();
        }

        $block = BlockedMatrix::find($sponsor->matrix_user);
        if($block){
            return redirect('/partner/registration')->with('message', 'Произошла ошибка, обратитесь в администрацию');
        }
		
        $new_partner = $this->register_new_partner($model);

        $matrix_user = DB::connection($connection)->table('dle_matrix_users')->select('mx_id')->where('uid', $sponsor->user_id)->first();

        app('App\Http\Controllers\Programs\Program'.ucfirst($m[ $p ][ 0 ]).'Controller')->insertIntoMatrix($new_partner->user_id, $new_partner->name, $sponsor->user_id);

        $this->updatePin($model->pin, $new_partner->user_id, $new_partner->name);
        $this->addNews($new_partner->name, 2, $p); 
    }

    private function updatePin($pin, $uid, $username){
        $m = Config::get('matrix.get');
        $p = Session::get('programm', 1);

       $pin = \App\Models\Pins\StartPin::wherePin($pin)->first();
        


        if($pin){
           $pin->status_id = 3;
            $pin->username = $username;
           $pin->save();
        }
    }

    private function addNews($name, $type, $program){
        $out = News::create(['type' => $type, 'name' => $name, 'program_id' => $program]);
    }

    private function register_new_partner($model){
        $request = json_decode( $model->json );

        $m = Config::get('matrix.get');
        $p =   $p = Session::get('programm', 1);
        $connection = 'mysql_prod_'.$m[$p][0];
        $data = json_decode( $model->json, true );

        if( isset($data['sponsor']) ){
            $sponsor = Partner::on($connection)->where('name', $data['sponsor'] )->first();    
        } else {
            $sponsor = Partner::on($connection)->where('user_id', $model->user_id )->first();
        }

        $m = Config::get('matrix.get');
        $p = Session::get('programm', 1);

        $connection = 'mysql_prod_'.$m[$p][0];

        $partner = new User;

        $partner->screch = $model->pin; 
        $partner->active = 1; 
        $partner->allow_mail = 1;
        $partner->allowed_ip = 1;
       //     'balance';
       //      'balance_user'; 
        $partner->banned = 0; 
        $partner->by_refer = $sponsor->user_id; 
        $partner->comm_num = 0; 
       //      'email'; 
        $partner->familiya = $request->name.' '.$request->secondname.' '.$request->patronic;
        $partner->fullname = $request->name.' '.$request->secondname.' '.$request->patronic;
        $partner->favorites = 0; 
     //        'foto'; 
        //$partner->fullname = $request->name.' '.$request->secondname.' '.$request->patronic; 
     //        'get_out'; 
        $partner->gorod =  $request->country;
        $partner->hash = 1; 
      //       'icq';
        $partner->imya  =  $request->country;
        $partner->pol  =  $request->phone;
      //       'info'; 
       //      'land';
      //       'lastdate';
        $partner->logged_ip = 0;
        $partner->matrix_id = 0;
        $partner->name =  $request->uname;
        $partner->news_num = 0;
        $partner->otchestvo  = $request->birthdate;
        $partner->password  = md5( md5($request->password2) );
        $partner->pm_all = 0;
        $partner->pm_unread = 0;
        $partner->ref_num = 0;
        $partner->reg_date = Carbon::now()->timestamp;
      //       'remember_token';
        $partner->restricted =0;
        $partner->restricted_date = 0;
        $partner->restricted_days = 0;
             //'signature';
        if($p == 1 || $p == 3 || $p == 6 || $p == 8 ){
            $partner->skype = 0;
        }        

        if($p == 7 ){
            $partner->spr_id = 0;
            $partner->sponsor = 0;
        }        

        if($p == 6 || $p == 3 || $p == 8 ){
            $partner->info = 0;
            $partner->signature = 0;
        }
       //      'stage'; 
        $partner->strana = $request->iin;
        $partner->time_limit = 0;
        $partner->tovar = 0;
            // 'user_group';
        $partner->xfields = 0;


        $partner->setConnection($connection);
        $partner->save();
		
        if( isset($request->state) && $request->state == 'Ближнее и дальнее зарубежье' ){
            NonResident::create([
                'program_id' => $p,
                'uid' => $partner->user_id,
            ]);
        }

        return $partner;
    }





}
