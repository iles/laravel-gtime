<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreBonusRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'uname' => 'required|string',
            'name' => 'string',
            'exist_pin' => 'string',
            'registration_date' => 'string',
            'payment_date' => 'string',
            'fact_date' => 'string',
            'bank' => 'string',
            'checkout_date' => 'string',
            'program' => 'string',
            'programm_date' => 'string',
            'summ' => 'string',
            'iin' => 'string',
            'country' => 'string',
            'city' => 'string',
            'bill_number' => 'string',
            'cancel_reason' => 'string',
            'status_id' => 'string',
        ];
    }
}
