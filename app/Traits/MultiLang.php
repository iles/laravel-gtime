<?php
namespace App\Traits;

trait MultiLang {

	public $local;
	public static $changed_input;

	public function __construct(array $attributes = array())
    {
        $this->local = config('app.fallback_locale');
		parent::__construct($attributes);
    }


	public function getTransField(){
		return self::$translate_fields;
	}

    public function getTranslate($property, $lang = null) {
        $input_line = $this->$property;
        if(is_null($lang)){
        	$lang = \App::getLocale();
        }
        $trans= '';
        if(!is_null($input_line)){
	        preg_match("/{:$lang}(.*){\/:$lang}/ius", $input_line, $matches);
	        //var_dump($matches);
	        $trans = (count($matches) == 2 ) ? $matches[1] : '';
        }
        return $trans;
    }

    public function getAllTranslates($property){
        $input_line = $this->$property;
      	$langs = config('app.locales');
				$resp = [];
				foreach ($langs as $local => $value) {
					if(!is_null($input_line)){
						preg_match("/{:$local}(.*){\/:$local}/ius", $input_line, $matches);
						//var_dump($matches);
						$resp[$local] = (count($matches) == 2 ) ? $matches[1] : '';
					}else{
						$resp[$local] = '';
					}
				}
        return $resp;
    }

    public function getDefaultTranslate($property){
    	$cur_lang = \App::getLocale();
        $trans = trim($this->getTranslate($property, $cur_lang));
        if(empty($trans)){
	        $locales = config('app.locales');
			foreach ($locales as $lang => $value) {
				if($lang != $cur_lang){
			        $trans = trim($this->getTranslate($property, $lang));
			        if(!empty($trans)){
			        	break;
			        }
				}      		
	        }    	
        }
        return $trans;
    }

    public static function union($input){
    	if(property_exists(get_called_class(), 'translate_fields')){
    		$locales = [];
	    	foreach ($input as $key => $item) {
	    		if(in_array($key, self::$translate_fields)){
	    			$input[$key] = '';
	    			foreach ($item as $local => $value) {
	    				if(trim($value)){
			    			$input[$key] .= "{:$local}".$value."{/:$local}";
			    			if( !isset($locales[$local])){
			    				$locales[$local] = '';
			    			}
	    				}
	    			}
	    		}
	    	}
	    	$input['locales'] = json_encode(array_keys($locales));
    	}
    	self::$changed_input = $input;
    	return $input;
    }

}
?>
