<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Config;
use Session;

class UserAr extends Model
{
    protected $primaryKey = ['user_id','program_'];
    public $timestamps = false;

    public $fillable = [
        'active', 
        'allow_mail',
        'allowed_ip',
        'balance',
         'balance_user', 
         'banned', 
         'by_refer', 
         'comm_num', 
         'email', 
         'familiya', 
         'favorites', 
         'foto', 
         'fullname', 
         'get_out', 
         'gorod',
         'hash', 
         'icq',
         'imya',
         'screch',
         'info', 
         'land',
         'lastdate',
         'logged_ip',
         'matrix_id',
         'name',
         'news_num',
         'otchestvo',
         'password',
         'pm_all',
         'pm_unread',
         'pol',
         'ref_num',
         'reg_date',
         'remember_token',
         'restricted',
         'restricted_date',
         'restricted_days',
         'screch',
         'signature',
         'skype',
         'stage', 
         'strana',
         'time_limit',
         'tovar',
         'user_group',
         'user_id',
         'xfields',
         'program_'
    ];

    protected $table = 'users_a';


    public static copyObj($user, $program_){
        $arch = new Usera();
        $arch->fill($user->attributesToArray());
        $arch->program_ = $program_;
        $arch->save();
    }
}
