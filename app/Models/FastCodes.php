<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FastCodes extends Model
{
     public $fillable = [
     	'pin1',
        'pin2',
        'done1',
        'done2',
    ];
}
