<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Swap extends Model
{
	public $fillable = [
		'login',
		'uid',
		'program_id',
		'login2',
		'uid2',
		'type',
		'reason',
		'image',
		'view',
		'status_id',
		'sec_check',
    ];
}
