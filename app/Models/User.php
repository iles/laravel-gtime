<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Config;
use Session;

class User extends Model
{
    public function __construct()
    {   
        $m = Config::get('matrix.get');
        $p =   $p = Session::get('programm', 1);
        $connection = 'mysql_prod_'.$m[$p][0];
        
        $this->connection =  $connection;
    }
    protected $rememberTokenName = false;
    protected $primaryKey = 'user_id';
    public $timestamps = false;

    public $fillable = [
        'active', 
        'allow_mail',
        'allowed_ip',
        'balance',
         'balance_user', 
         'banned', 
         'by_refer', 
         'comm_num', 
         'email', 
         'familiya', 
         'favorites', 
         'foto', 
         'fullname', 
         'get_out', 
         'gorod',
         'hash', 
         'icq',
         'imya',
         'screch',
         'info', 
         'land',
         'lastdate',
         'logged_ip',
         'matrix_id',
         'name',
         'news_num',
         'otchestvo',
         'password',
         'pm_all',
         'pm_unread',
         'pol',
         'ref_num',
         'reg_date',
         'remember_token',
         'restricted',
         'restricted_date',
         'restricted_days',
         'screch',
         'signature',
         'skype',
         'stage', 
         'strana',
         'time_limit',
         'tovar',
         'user_group',
         'user_id',
         'xfields'
    ];

    protected $table = 'dle_users';


    public static function getTotalCount(){
        $m = Config::get('matrix.get');
        $p = Session::get('programm', 1);
        $type = $m[$p][0];
        $connection = 'mysql_prod_'.$type;
        return str_pad(User::on($connection)->count(),7,'0',STR_PAD_LEFT);
    }
}
