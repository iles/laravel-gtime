<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Config;
use Session;
use App\Models\Usera;

class CronRemoves extends Model
{
    public $timestamps = false;

    public static function remove(){
        $items = self::whereNull('remove_time')->get();
        foreach ($items as $key => $item) {
            $connection = 'mysql_prod_'.$item->program;
            $user = User::on($connection)->where(['user_id' => $item->user_id])->first();
            if(!empty($user)){
                Usera::copyObj($user,$item->program);
                $user->delete();
            }
            $item->remove_time = date('Y-m-d H:i:s');
            $item->save();
        }
    }
}
