<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BonusRequest extends Model
{
    public $fillable = [
            'uname',
            'name',
            'second_name',
            'patronic',
            'exist_pin',
            'registration_date',
            'payment_date',
            'fact_date',
            'bank',
            'checkout_date',
            'program',
            'programm_date',
            'summ',
            'iin',
            'country',
            'city',
            'bill_number',
            'cancel_reason',
            'status_id'
    ];

    public function getRouteKeyName()
    {
        return 'bonusrequest';
    }
}