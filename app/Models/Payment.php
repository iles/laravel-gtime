<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
	public $fillable = [
            'photo',
            'user_id',
            'program_id',
            'status',
    ];
}
