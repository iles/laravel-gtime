<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserComment extends Model
{
    protected $table = 'vip_request_users_new_pin';

    public $fillable = [
        'id',
        'user_id',
        'cancel_reason',
    ];

    public function setTable($tableName){
        $table = $tableName;
    }
}
