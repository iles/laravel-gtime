<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NonResident extends Model
{
	public $fillable = [
        'program_id',
        'uid'
    ];
}