<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cancellation extends Model
{
	public $fillable = [
		'program_id',
        'pin',
        'login',
        'fullname',
        'uid',
        'mx_id',
        'sponsor_id',
        'sponsor_login',
        'sponsor_fullname',
        'reg_date',
        'status_ids',
    ];
}
