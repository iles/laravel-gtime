<?php

namespace App\Models\Bonuses;

use Illuminate\Database\Eloquent\Model;

class AutoBonusRequestModel extends Model
{
	protected $table = 'auto_bonus_request';

    public $fillable = [
            'program_id',
            'summ',
            'uid',
            'uname',
            'image',
            'firstname',
            'lastname',
            'patronic',
            'iin',
            'bill_num',
    ];
}
