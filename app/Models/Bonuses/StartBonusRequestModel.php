<?php

namespace App\Models\Bonuses;

use Illuminate\Database\Eloquent\Model;

class StartBonusRequestModel extends Model
{
      protected $table = 'start_bonus_request';

    public $fillable = [
            'program_id',
            'summ',
            'view',
            'uid',
            'uname',
            'fullname',
            'image',
            'firstname',
            'lastname',
            'patronic',
            'iin',
            'bill_num',
            'bank_name',
            'BIK',
            'pens',
            'inv',
            'finished_id',
            'inv_group',
            'inv_start',
            'inv_end',
            'inv_timeless',
            'card_number',
            'notorios',
            'adress',
            'fact_adress',
            'bank_account',
            'vip_bonus',
            'vip_bonus_logins',
            'country',
            'not_fio',
            'not_adress',
            'not_iin',
            'not_passport',
    ];

    public static $searchColumnList = [
      'summ',
      'uname',
      'firstname',
      'lastname',
      'patronic',
      'iin',
      'bank_name',
      'BIK',
      'bill_num'
    ];
}
