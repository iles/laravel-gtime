<?php

namespace App\Models\Bonuses;

use Illuminate\Database\Eloquent\Model;

class FastBonusRequestModel extends Model
{
	protected $table = 'fast_bonus_request';

    public $fillable = [
            'program_id',
            'summ',
            'uid',
            'uname',
            'image',
            'firstname',
            'lastname',
            'patronic',
            'iin',
            'bill_num',
    ];
}
