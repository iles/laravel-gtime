<?php

namespace App\Models\Bonuses;

use Illuminate\Database\Eloquent\Model;

class AccumulativePlusBonusRequestModel extends Model
{
	protected $table = 'accumulative_plus_bonus_request';

    public $fillable = [
            'program_id',
            'summ',
            'uid',
            'uname',
            'image',
            'firstname',
            'lastname',
            'patronic',
            'iin',
            'bill_num',
            'product',
            'secondBonus',
    ];
}
