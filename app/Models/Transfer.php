<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Transfer extends Model
{

    public $fillable = [
        'program_id',
        'login',
        'fullname',
        'fullname2',
        'image',
        'variant',
        'status_id',
        'finished_id',
        'bonus_id',
    ];
}
