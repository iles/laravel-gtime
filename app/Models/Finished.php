<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Finished extends Model {
	public $fillable = [
		'program_id',
		'user_id',
		'stage',
		'login',
		'fullname',
		'birthdate',
		'iin',
		'registred_pin',
		'country',
		'phone',
		'info',
		'tovar',
		'variant',
		'pin',
		'pin2',
		'pin3',
		'bonus',
		'vip_bonus',
		'last',
	];
}
