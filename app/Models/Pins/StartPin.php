<?php

namespace App\Models\Pins;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class StartPin extends Model
{
    public $fillable = [
        'user_id',
        'request_id',
        'status_id',
        'request_program_id',
        'username',
        'pin',
        'transitive',
        'program',
        'expired',
        'kassa',
        'screch',
        'timeless',
        'expired_at',
        'active_at'
    ];

   // public $program =  1;

    public function request()
    {
        return $this->belongsTo('App\Models\PinRequests\StartRequest');
    }

    public static function boot()
    {
      parent::boot();
      self::retrieved(function ($model) {
        if($model->timeless == 1){
          return true;
        }
        
      	if( Carbon::today()->toDateTimeString() > $model->expired_at  ){
      		$model->status_id = 2;
      		$model->save();
      	}


      });      
    }
}
