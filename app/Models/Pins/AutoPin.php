<?php

namespace App\Models\Pins;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class AutoPin extends Model
{
    public $fillable = [
        'user_id',
        'username',
        'request_id',
        'status_id',
        'request_program_id',
        'pin',
        'program',
        'expired',
        'kassa',
        'expired_at',
        'active_at'
    ];

}
