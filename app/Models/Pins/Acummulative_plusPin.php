<?php

namespace App\Models\Pins;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Acummulative_plusPin extends Model
{
    public $fillable = [
        'user_id',
        'request_id',
        'status_id',
        'request_program_id',
        'pin',
        'program',
        'expired',
        'kassa',
        'expired_at',
        'active_at'
    ];

   // public $program =  5;

    public static function boot()
    {
      parent::boot();
      self::retrieved(function ($model) {
      	if( Carbon::today()->toDateTimeString() > $model->expired_at  ){
      		$model->status_id = 2;
      		$model->save();
      	}
      });
    }
}
