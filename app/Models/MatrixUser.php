<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MatrixUser extends Model
{
	protected $primaryKey = 'uid';
	public $timestamps = false;

     public $fillable = [
        'level',
        'position',
        'note'
    ];
 
    protected $table = 'dle_matrix_users';
}
