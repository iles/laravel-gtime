<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Invite extends Model
{
	public $fillable = [
		'program_id',
		'user_id',
		'open',
		'bonus_id',
		'invites',
	];
}
