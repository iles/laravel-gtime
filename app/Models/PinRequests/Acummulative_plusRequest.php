<?php

namespace App\Models\PinRequests;

use Illuminate\Database\Eloquent\Model;

class Acummulative_plusRequest extends Model
{
    public $fillable = [
            'user_id',
            'image',
            'bill',
            'iin',
            'note',
            'kassa',
            'uname',
            'summ',
            'skype',
            'status_id',
            'pin',
            'cancel_reason',
            'fullname',
            'date',
            'country',
            'user_note',
            'aprove_at',
    ];

    public $program =  5;
    public $pins;
}
