<?php

namespace App\Models\PinRequests;

use Illuminate\Database\Eloquent\Model;

class StartRequest extends Model
{
    public $fillable = [
            'user_id',
            'image',
            'bill',
            'iin',
            'note',
            'uname',
            'summ',
            'kassa',
            'skype',
            'phone',
            'program',
            'transitive',
            'status_id',
            'pin',
            'cancel_reason',
            'fullname',
            'fullname_b',
            'date',
            'country',
            'city',
            'currency',
            'user_note',
            'aprove_at',
    ];

    public function pins()
    {
        return $this->hasMany('App\Models\Pins\StartPin', 'request_id');
    }

    public $pins;
}
