<?php

namespace App\Models\PinRequests;

use Illuminate\Database\Eloquent\Model;

class AutoRequest extends Model
{
    public $fillable = [
            'user_id',
            'image',
            'bill',
            'iin',
            'note',
            'uname',
            'kassa',
            'summ',
            'skype',
            'status_id',
            'pin',
            'cancel_reason',
            'fullname',
            'date',
            'country',
            'user_note',
            'aprove_at',
    ];

    public $program =  2;
    public $pins;

}
