<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pin extends Model
{
	public $fillable = [
            'program',
            'bill',
            'iin',
            'uname',
            'summ',
            'skype',
            'pin',
    ];
}
