<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MainPinRequest extends Model
{
    protected $table = 'main_request_users_new_pin';

    public $fillable = [
        'id',
        'user_id',
        'program_id',
        'created_at',
        'status',
        'image',
        'uname',
        'cancel_reason',
        'pin',
    ];

    public function getProgram(){
        return 3;
    }
}
