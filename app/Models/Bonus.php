<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Session;

class Bonus extends Model
{


	public $fillable = [
            'program_id',
            'summ',
            'uid',
            'uname',
            'firstname',
            'lastname',
            'patronic',
            'iin',
            'bill_num',
    ];

  public static function boot()
    {
      parent::boot();
      self::creating(function ($model) {
        $model->program_id = Session::get('programm', 1);;
      });
    }

}
