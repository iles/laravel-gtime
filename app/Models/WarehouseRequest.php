<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;

class WarehouseRequest extends Model
{
	protected $table = 'warehouse_requests';


    public $fillable = [
        'program',
        'uname',
        'user_fullname',
        'iin',
        'purchace_date',
        'checkout_name',
        'checkout_surname',
        'checkout_patronic',
        'checkout_date',
        'option',
    ];

    public static function boot()
    {
      parent::boot();
      self::creating(function ($model) {
        $model->manager_id =  Auth::guard('web')->user()->id ;
      });
    }
}