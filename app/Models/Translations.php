<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class Translations extends Model
{
    protected $fillable = ['key_','lang_','content'];

    public $timestamps = false;

    public function getTranslationsGrouped(){
        $grouped_langs = self::get()->groupBy('lang_');
    }

    public static function getList(){
        $list = self::get()->groupBy('key_')->toArray();  
        $langs = config('app.locales');
        // dd($list);
        $clList = [];
        foreach ($list as $key => $item) {
            foreach ($item as $key2 => $prop) {
                $clList[$key][$prop['lang_']] = $prop['content'];
            }
        }
        return $clList;
    }

    public static function saveItem($key, $lang, $content){
        if(empty($key))
            return;
        $item = self::firstOrNew(['key_' => $key,'lang_' =>$lang]);
        $item->content = $content;
        $item->save();
    }

    public static function saveItems($data){
        $langs = config('app.locales');
        self::truncate();
        $dataLang = [];
        foreach ($data['keys'] as $key1 => $name) {
            if(empty($name))
                continue;
            foreach ($langs as $key2 => $value) {
                if(!isset($dataLang[$key2]))
                    $dataLang[$key2] = [];
                self::saveItem($name,$key2, $data[$key2][$key1]);
                $dataLang[$key2][$name] = $data[$key2][$key1];
            }
        }
        // var_dump($dataLang);
        // exit();
        foreach ($langs as $key2 => $value) {
            self::saveFile($dataLang[$key2],$key2);
            // var_dump($path);
        }
        // exit();
    }

    public static function saveFile($text, $lang){
        $path = base_path('resources/lang/'.$lang.'/translation.php');
        // dd($path);
        // if (!file_exists($path)) {
        //     dd($path);
        //     // Storage::disk('resourses')($path,'');
        // }
        $var_str = var_export($text, true);
        $var = "<?php\n\n return $var_str;\n\n?>";
        Storage::disk('resourses')->put('lang/'.$lang.'.json',json_encode($text));
        // file_put_contents($path, $var);

    }

    public static function saveFileWord($word){
        $langs = config('app.locales');
        foreach ($langs as $key1 => $lang) {
            $item = self::firstOrNew(['key_' => $word,'lang_' =>$key1]);
            if(empty($item->id)){
                $item->save();
            }
        }
    }

}
