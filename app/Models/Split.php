<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Split extends Model
{
	public $fillable = [
		'program_id',
		'main_matrix',
		'main_matrix_id',
		'first_matrix',
		'first_matrix_id',
		'second_matrix',
		'second_matrix_id',
		'leader',
		'leader_id',
		'sponsor',
		'sponsor_id',
		'new',
		'new_id',
		'matrix_stage',
		'matrix_dateout',
		'status_id',
    ];


}