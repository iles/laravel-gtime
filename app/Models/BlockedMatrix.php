<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BlockedMatrix extends Model
{
     public $fillable = [
     	'program_id',
        'matrix_id',
        'date',
    ];

}
