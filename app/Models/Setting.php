<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Validator;

class Setting extends Model
{
    use \App\Traits\MultiLang;

	const SITE_NAME = 'site_name';
    const SITE_LOGO = 'site_logo';
    const COMPANY_NAME = 'company_name';
    const VCONTACTE = 'vcontact';
    const EMAIL = 'email';
    const VOTSAP = 'votsap';
    const INSTAGRAM = 'instagram';
    const VIBER = 'viber';


    public $timestamps = false;

    protected $rules = [
        'value.*'=> 'required|500',
    ];

    protected $fillable = ['value','name'];

    public static function getParams(){
		return [
			self::SITE_NAME,
            self::SITE_LOGO,
            self::COMPANY_NAME,
            self::VCONTACTE,
            self::EMAIL,
            self::VOTSAP,
            self::INSTAGRAM,
            self::VIBER
		];
    }

    public static $translate_fields = [
        self::SITE_NAME,
        self::COMPANY_NAME
    ];

    public static $file_fields = [
        self::SITE_LOGO,
    ];

    public static function getAllValues(){
        $values = [];
        $options = self::all();
        if( $options->count() != 0 ){
            $values = $options->pluck('value','name')->all();
        }
        return $values;
    }

    public static function getSiteSetting(){
        $values = [];
        $options = self::get();
        if( $options->count() != 0 ){
            foreach ($options as $key => $item) {
                if(in_array($item->name, self::$translate_fields)){
                    $saved_values[$item->name] = $item->getTranslate('value');
                }elseif(in_array($item->name, self::$file_fields)){
                    $saved_values[$item->name] = $item->getImageUrl($item->value);
                }else{
                    $saved_values[$item->name] = $item->value;
                }
            }
        }
        $params = self::getParams();
        foreach ($params as $param) {
            $values[$param] = (isset($saved_values[$param])) ?  $saved_values[$param] : '';
        }
        return $values;
    }

    public function setValues(){
        $values = self::pluck('value','name')->all();
        $params = self::getParams();
        //dd($values);
        foreach ($params as $name) {
            $this->$name = (isset($values[$name])) ? $values[$name] : '';
        }
    }

    public static function  getFormatedLang($data){
        $lang = \App::getLocale();
        $text = self::getLang($data, $lang);
        if( $text!= ''){
            $rows = explode("\r", $text);
            $text = "<p>" . implode( "</p>\n\n<p>",$rows ) . "</p>";
        }
        return $text;
    }

    public static function  getPageUrl(){
        $array = explode("/",request()->fullUrl());
        $new_url = '';
        if(count($array) >= 4){
            $num = 3;
            if(in_array($array[3], array_keys(config('app.locales') ) )){
                $num = 4;
            }
            $new_array = array_splice($array, $num);

            $new_url = (!empty($new_array)) ? '/'.implode("/", $new_array) : '';
            // dd($new_url);
        }

        return $new_url;
    }

    public function isFileInput($seting){
        return in_array($seting, self::$file_fields) ? true : false;
    }

    public function saveFile($upFile, $attr){
        if(!empty($upFile)){
            $ext = $upFile->getClientOriginalExtension();
            $path = $upFile->storeAs('setting', $attr.'.'.$ext,'public_uploads');
            // dd($path);
            $this->value = $path; 
        }
    }


    public function getImageUrl($value){
        return asset('uploads/'.$value);
    }
}
