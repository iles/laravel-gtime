<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MainProgramFinish extends Model
{
    public $fillable = [
        'user_id', 'pin', 'bonus',
    ];
}
