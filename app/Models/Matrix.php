<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Matrix extends Model
{
    protected $table = 'dle_matrix'; 
    
    protected $primaryKey = 'matrix_id';
	public $timestamps = false;
		

    public $fillable = [
        'leader',
        'users_num',
        'stage`',
        'datetout',
    ];
}
