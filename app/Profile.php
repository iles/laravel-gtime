<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
	protected $fillable = [
        'info', 'logo', 'is_online',
    ];

	public function user()
    {
    	return $this->belongsTo('App\User');
    }
}
