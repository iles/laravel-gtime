
## Установка

- Клонируешь проект;
- Переходишь в директорию проекта и ставишь пакеты композером (нужно установить composer): **composer update**;
- Устанавливаешь и настраиваешь Homestead ( https://laravel.ru/docs/v5/homestead );
- Запускаешь Homested (**vagrant up**), коннетишся по ssh к виртуальной машине (**vagrant ssh**);
- На виртуальной машине создаешь базу в postgre
- Идешь в config/database.php и массиве 'connections', в 'pgsql' прописываешь там свою базу
- Настраиваем среду: в корневой папке проекта -cp .env.example .env
- В файле .env вписываем конфиги базы:

```
DB_CONNECTION= pgsql

DB_HOST= 127.0.0.1

DB_PORT= 5432

DB_DATABASE=laravel_gtime_db

DB_USERNAME=homestead

DB_PASSWORD=secret
```
- Генерируем ключ приложения: **php artisan key:generate**;

- Миграции и сидеры запускаются с виртуальной машины:

```
php artisan migrate; 

php artisan db:seed --class=RolesTableSeeder; 

php artisan db:seed --class=UsersTableSeeder; 

php artisan db:seed;

```
